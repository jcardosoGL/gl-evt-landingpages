<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Envies 2022 Homme";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../media/LP/src/css/2021/la-beaute-est-un-geste-rose-hermes.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link rel="stylesheet" type="text/css" href="https://static.galerieslafayette.com/media/LP/src/css/2021/landing-edito.css?02">
<style type="text/css">
.header__product-list, .footer-description, .ab684263 {
    display: none;
}
.reveal-modal {
    z-index: 7005;
    border: 8px solid #000;
}
.reveal-modal-bg {
    z-index: 7000;
}
ul:not(.header-unified) {
    margin-left: 0;
}
.landing-edito dfn {
    background: url(https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/go-for-good.png) no-repeat center;
    background-size: cover;
    width: 75px;
    height: 19px;
    display: flex;
    margin: 16px auto 0;
}
@media screen and (min-width: 1250px) {
.landing-edito dfn {
    width: 115px;
    height: 32px;
   }
}
</style>
<section class="landing-edito">
    
    <div class="intro">
        <span>Mode — 01.01.22</span>  
        <h1>Toutes nos envies mode de 2022</h1>
        <p>Classiques indémodables, marques responsables et créateurs en vogue, pour 2022, on veut un nouveau dressing stylé et qui a du sens. Bonne année !
        </p> 
   </div>
   
<div class="block01 content-item">    
    <div class="bloc-visuel">
        <div class="element-sticky">
            <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/01.jpg" alt="Miser sur des soins GO FOR GOOD">
        </div>
    </div>    
    
    <div class="bloc-product">        
         <div class="text reveal">
             <span class="sub-title reveal-2">1.</span>
             <h2 class="reveal-3">De la mode responsable</h2>
             <p class="reveal-4">
              Dans une démarche de changer nos habitudes de consommation, on mise sur des pièces labellisées Go For Good pour un vestiaire eco-friendly et une mode responsable. Coton organique, matières recyclées, les marques du moment s’engagent pour un vestiaire tourné vers l’avenir.
              </p>   
         </div>                                       

         <div class="content-prod">
             <ul>
                 <li>
                   <div>
                       <a href="https://www.galerieslafayette.com/quickshop/load/76001970/125" class="js-pdt-quick-shop-link" target="_self">
                           <span class="img-prod-container"><!-- go-for-good -->
                               <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/01.jpg" alt="Huile Tonic">
                           </span>
                           <span class="description">
                               <span class="title">PATAGONIA</span>
                               <span class="texte">Casquette Lo Pro Trucker</span>
                               <dfn>&nbsp;</dfn>
                           </span>
                          
                        </a>
                  </div>
                </li>
               <li>
                   <div>
                       <a href="https://www.galerieslafayette.com/quickshop/load/74997401/85" class="js-pdt-quick-shop-link" target="_self">
                          <span class="img-prod-container">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/02.jpg" alt="Crème légère hydratante">
                          </span>
                          <span class="description">
                               <span class="title">SCOTCH&SODA</span>
                               <span  class="texte">Veste worker coton bio</span>
                               <dfn>&nbsp;</dfn>
                         </span>
                         
                        </a>
                  </div>
                </li>
                 <li>
                   <div>
                       <a href="https://www.galerieslafayette.com/quickshop/load/75448441/119" class="js-pdt-quick-shop-link" target="_self">
                          <span class="img-prod-container">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/03.jpg" alt="Huile sérum">
                          </span>
                          <span class="description">
                               <span class="title">CARHARTT WIP</span>
                               <span  class="texte">Jean Newel relaxed coton organique</span>
                               <dfn>&nbsp;</dfn>
                         </span>
                        </a>
                  </div>
                </li>
                <li>
                   <div>
                       <a href="https://www.galerieslafayette.com/quickshop/load/75150541/85" class="js-pdt-quick-shop-link" target="_self">
                          <span  class="img-prod-container">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/04.jpg" alt="Gentle Daily Peel">
                          </span>
                          <span class="description">
                               <span class="title">VEJA</span>
                               <span class="texte">Baskets basses Campo Easy</span>
                               <dfn>&nbsp;</dfn>
                          </span>
                        </a>
                  </div>
                </li>
             </ul>
        </div>        
<!--         <a href="/c/beaute" class="button white">VOIR TOUS LES PRODUITS</a>        -->
     </div>   
 </div>  
    
    
  <div class="block02 blk-right content-item">    
    <div class="bloc-visuel">
        <div class="element-sticky">
            <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/02b.jpg" alt="Se faire une crinière de rêve">
        </div>
    </div>
    <div class="bloc-product">        
            <div class="text reveal">
                <span class="sub-title reveal-2">2.</span>
                <h2 class="reveal-3">Des pièces intemporelles</h2>
                <p class="reveal-4">
                La clé d’un vestiaire qui dure ? Des bons basiques qu’on aime aujourd’hui et aussi demain. Une tenue signature, qui s’adapte à tous les styles. Parmi ces iconiques que l’on peut porter avec tout, l’incontournable blouson, qu’on aime aussi bien porté avec un chino ajusté qu’un jean clair. Côté chaussures, les mocassins sont et resteront une valeur sûre. On les aime au bureau, en week-end à la mer ou en escapade en ville. Ils ont décidément tout bon. 
                </p>   
            </div>                                       
     
            <div class="content-prod">
                <ul>
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/quickshop/load/77281405/125" class="js-pdt-quick-shop-link" target="_self">
                              <span class="img-prod-container">
                                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/05b.jpg" alt="Shampoing cheveux gras zinc & sauge">
                              </span>
                              <span class="description">
                                  <span class="title">SANDRO</span>
                                  <span class="texte">Veste droite en laine</span>
                                  <dfn>&nbsp;</dfn>
                              </span>
                           </a>
                     </div>
                   </li>
                  <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/quickshop/load/80350875/150" class="js-pdt-quick-shop-link" target="_self">
                             <span class="img-prod-container">
                                 <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/06.jpg" alt="Après-shampoing pour cheveux sec">
                             </span>
                             <span class="description">
                                  <span class="title">BALIBARIS</span>
                                  <span  class="texte">Pantalon chino Paul ajusté</span>
                            </span>
                           </a>
                     </div>
                   </li>
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/quickshop/load/300403726437/459" class="js-pdt-quick-shop-link" target="_self">
                             <span class="img-prod-container">
                                 <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/07.jpg" alt="Huile des rêves">
                             </span>
                             <span class="description">
                                  <span class="title">RAY BAN</span>
                                  <span  class="texte">Lunettes de soleil polarisées Aviator</span>
                            </span>
                           </a>
                     </div>
                   </li>
                   <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/quickshop/load/64146689/320" class="js-pdt-quick-shop-link" target="_self">
                             <span  class="img-prod-container">
                                 <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/08.jpg" alt="Masque Soin Régénérant aux Quatre Huiles Végétales">
                             </span>
                             <span class="description">
                                  <span class="title">SANDRO</span>
                                  <span class="texte">Derbies en cuir à crampons</span>
                             </span>
                           </a>
                     </div>
                   </li>
                </ul>
           </div>        
<!--         <a href="/c/beaute" class="button white">VOIR TOUS LES PRODUITS</a>        -->
        </div>   
    </div>
    
    
 <div class="block03 content-item">    
    <div class="bloc-visuel">
        <div class="element-sticky">
            <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/03.jpg" alt="Faire confiance aux compléments">
        </div>
    </div>    
    
    <div class="bloc-product">        
            <div class="text reveal">
                <span class="sub-title reveal-2">3.</span>
                <h2 class="reveal-3">Du confort toujours chic</h2>
                <p class="reveal-4">
                Après des mois d’incertitude et de confinement, le vestiaire des hommes mise sur des pièces confortables et stylées à porter bien au-delà de la salle de sport. Un pull qui tient chaud, un jogging bien coupé, une casquette en velours ou encore un sac en bandoulière dans lequel glisser ses accessoires, autant d'essentiels inspirés de la tendance leisurewear. 
                </p>   
            </div>                                       
     
            <div class="content-prod">
                <ul>
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/quickshop/load/77613468/154" class="js-pdt-quick-shop-link" target="_self">
                              <span class="img-prod-container"><!-- go-for-good -->
                                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/09.jpg" alt="Oméga Glow">
                              </span>
                              <span class="description">
                                  <span class="title">PALM ANGELS</span>
                                  <span class="texte">Casquette velours milleraies</span>
                              </span>
                           </a>
                     </div>
                   </li>
                  <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/quickshop/load/77551974/204" class="js-pdt-quick-shop-link" target="_self">
                             <span class="img-prod-container">
                                 <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/10.jpg" alt="L'Indispensable probiotiques - Probiotiques naturels">
                             </span>
                             <span class="description">
                                  <span class="title">ISABEL MARANT</span>
                                  <span  class="texte">Pul droit maille côtelée laine</span>
                            </span>
                           </a>
                     </div>
                   </li>
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/quickshop/load/76882892/232" class="js-pdt-quick-shop-link" target="_self">
                             <span class="img-prod-container">
                                 <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/11.jpg" alt="Complexe Peau Apaisée">
                             </span>
                             <span class="description">
                                  <span class="title">AMERICAN VINTAGE</span>
                                  <span  class="texte">Jogging Vetington</span>
                            </span>
                           </a>
                     </div>
                   </li>
                   <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/quickshop/load/78830268/97" class="js-pdt-quick-shop-link" target="_self">
                             <span  class="img-prod-container">
                                 <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/12.jpg" alt="Cacao Pot">
                             </span>
                             <span class="description">
                                  <span class="title">NEW BALANCE</span>
                                  <span class="texte">Baskets basses MS327LG1 bicolores</span>
                             </span>
                           </a>
                     </div>
                   </li>
                </ul>
           </div>        
<!--         <a href="/c/beaute" class="button white">VOIR TOUS LES PRODUITS</a>        -->
        </div>   
    </div>  
 
 <div class="block04 blk-right content-item">    
    <div class="bloc-visuel">
        <div class="element-sticky">
            <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/04.jpg" alt="Apprendre à lâcher prise">
        </div>
    </div>
    <div class="bloc-product">        
            <div class="text reveal">
                <span class="sub-title reveal-2">4.</span>
                <h2 class="reveal-3">Des audaces stylées</h2>
                <p class="reveal-4">
                Et si 2022 était l’année des combinaisons mode audacieuses. Exit les silhouettes un brin trop classiques, on sort de sa zone de confort en optant pour un style original mais toujours aussi élégant. Un manteau en laine vert kaki, un pantalon en cuir pour se la jouer Keanu Reeves dans Matrix et même des sandales hybrides qui feront sensation sur le macadam. A vous d’oser. 
                </p>   
            </div>                                       
     
            <div class="content-prod">
                <ul>
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/quickshop/load/79667651/263" class="js-pdt-quick-shop-link" target="_self">
                              <span class="img-prod-container">
                                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/13.jpg" alt="Sachet de lait pour le bain x10">
                              </span>
                              <span class="description">
                                  <span class="title">ETUDES</span>
                                  <span class="texte">Manteau Archeology droit long en laine</span>
                              </span>
                           </a>
                     </div>
                   </li>
                  <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/quickshop/load/300411421802/278" class="js-pdt-quick-shop-link" target="_self">
                             <span class="img-prod-container">
                                 <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/14.jpg" alt="Bougie Rose">
                             </span>
                             <span class="description">
                                  <span class="title">THE KOOPLES</span>
                                  <span  class="texte">Pantalon droit cuir</span>
                            </span>
                           </a>
                     </div>
                   </li>
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/quickshop/load/300409954326/256?version=v2" class="js-pdt-quick-shop-link" target="_self">
                             <span class="img-prod-container">
                                 <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/15.jpg" alt="Huiles essentielles ELLIA Lavande">
                             </span>
                             <span class="description">
                                  <span class="title">CAMPERLAB</span>
                                  <span  class="texte">Sandales cuir Traktori</span>
                            </span>
                           </a>
                     </div>
                   </li>
                   <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/quickshop/load/75585587/52" class="js-pdt-quick-shop-link" target="_self">
                             <span  class="img-prod-container">
                                 <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/16.jpg" alt="Rouleau de massage intensif">
                             </span>
                             <span class="description">
                                  <span class="title">JUSTINE CLENQUET</span>
                                  <span class="texte">Collier Ali</span>
                                  <dfn>&nbsp;</dfn>
                             </span>
                           </a>
                     </div>
                   </li>
                </ul>
           </div>        
<!--         <a href="/c/beaute" class="button white">VOIR TOUS LES PRODUITS</a>        -->
        </div>   
    </div>
 
 <div class="block05 content-item">    
    <div class="bloc-visuel">
        <div class="element-sticky">
            <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/05.jpg" alt="Faire du sport autrement">
        </div>
    </div>    
    
    <div class="bloc-product">        
            <div class="text reveal">
                <span class="sub-title reveal-2">5.</span>
                <h2 class="reveal-3">Des créateurs inspirants</h2>
                <p class="reveal-4">
                En 2022, le vestiaire des hommes se pare de nouvelles pièces de créateurs toutes plus originales les unes que les autres. De sweat à capuche revisité par Casablanca en passant par les nouveaux pantalons signés Jacquemus, la nouvelle allure masculine joue la carte de l’audace et c’est tant mieux. Dans le palmarès des accessoires les plus plébiscités par les garçons de la mode, les chaussures aux semelles maxi compensées et les bob revisités sont les indispensables toute saison confondue.
                </p>   
            </div>                                       
     
            <div class="content-prod">
                <ul>
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/quickshop/load/75619129/85" class="js-pdt-quick-shop-link" target="_self"> 
                              <span class="img-prod-container"><!-- go-for-good -->
                                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/17.jpg" alt="Brassière Sports Bra Coton Jersey">
                              </span>
                              <span class="description">
                                  <span class="title">CASABLANCA</span>
                                  <span class="texte">Hoodie droit signature</span>
                                  <dfn>&nbsp;</dfn>
                              </span>
                           </a>
                     </div>
                   </li>
                  <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/quickshop/load/73046133/453" class="js-pdt-quick-shop-link" target="_self">
                             <span class="img-prod-container">
                                 <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/18.jpg" alt="Legging Cotton Rib">
                             </span>
                             <span class="description">
                                  <span class="title">JACQUEMUS</span>
                                  <span  class="texte">Le Pantalon Bellu</span>
                            </span>
                           </a>
                     </div>
                   </li>
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/quickshop/load/70882492/85" class="js-pdt-quick-shop-link" target="_self">
                             <span class="img-prod-container">
                                 <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/19.jpg" alt="Tapis de yoga SISSEL et pilates bordeaux">
                             </span>
                             <span class="description">
                                  <span class="title">ROMBAUT</span>
                                  <span  class="texte">Sneakers basses Protect Hybrid cuir</span>
                            </span>
                           </a>
                     </div>
                   </li>
                   <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/quickshop/load/77366197/320" class="js-pdt-quick-shop-link" target="_self">
                             <span  class="img-prod-container">
                                 <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/envies-2022-homme/prod/20.jpg" alt="Safran Bougie Parfumée">
                             </span>
                             <span class="description">
                                  <span class="title">MARINE SERRE</span>
                                  <span class="texte">Bob signature coton denim</span>
                                  <dfn>&nbsp;</dfn>
                             </span>
                           </a>
                     </div>
                   </li>
                </ul>
           </div>        
<!--         <a href="/c/beaute" class="button white">VOIR TOUS LES PRODUITS</a>        -->
        </div>   
    </div>
      
    <div class="all-univers">
        <h2>Tout l’univers homme</h2>
        <a href="/h/homme" class="button black">Découvrir</a>
    </div>    
</section>
<script type="text/javascript" src="https://static.galerieslafayette.com/media/LP/src/js/2019/reveal.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->
<script>
    const el = document.querySelector(".element-sticky")
    const observer = new IntersectionObserver( 
      ([e]) => e.target.classList.toggle("is-pinned", e.intersectionRatio < 1),
      { threshold: [1] }
    );
    observer.observe(el);
</script>
  
<!-- build:js /media/LP/src/js/2021/prada-collection-ss21.min.v01.js 
  <script src="../../assets/js/blazy.min.js"></script>
  <script src="../src/js/2021/prada-collection-ss21.js"></script>
<!-- endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
