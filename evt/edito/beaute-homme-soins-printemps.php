<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Soins Printemps";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../media/LP/src/css/2021/la-beaute-est-un-geste-rose-hermes.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
  <link rel="stylesheet" type="text/css" href="https://static.galerieslafayette.com/media/LP/src/css/2020/edito_guide_mode02.css?02">
  <style type="text/css">
  .manteaux-hiver .block02 .with-carousel-variable-width li div {
      min-height: 496px;
  }
  .header__product-list {
      display: none;
  }
  .manteaux-hiver .all-coats h3 {
      font-size: 57px;
      line-height: 1.18;
      width: auto;
      margin: 0 auto;
      border: none;
  }
  @media screen and (min-width: 1024px) { 
      .manteaux-hiver .texte.one-line {
          height: 41px;
      }
      .manteaux-hiver .carousel-variable-width .slick-prev, .manteaux-hiver .carousel-variable-width .slick-next {
          display: none !important;
      }
      .header__product-list {
          max-width: 1250px
      }
      .manteaux-hiver .all-coats h3 {
          font-size: 104px;
          line-height: 1.18;
      }
  }
  @media screen and (min-width: 1250px) { 
      .manteaux-hiver .intro h1 {
          font-size: 123px;
          max-width: 1006px;
          margin: 0 auto;
      }
      .manteaux-hiver .text h2 {
          font-size: 51px; 
          letter-spacing: -2px;
      }
      .manteaux-hiver .with-carousel-variable-width {
          max-width: 846px;
      }
      .manteaux-hiver .with-carousel-variable-width li {
          background: #fff;
      }
      .manteaux-hiver .all-coats h3 {
         font-size: 84px;
      }  
      .manteaux-hiver .block02 .with-carousel-variable-width li div {
          min-height: auto;
      }
  }
  </style>
  <section class="manteaux-hiver">
      <div class="intro">
          <span>Beauté Homme</span>  
          <h1>Les soins à adopter pour le&nbsp;printemps</h1>
          <p>Nouvelle saison, nouvelle routine. Après des mois de frimas et de grisaille, les hommes misent sur des soins skincare calibrés pour faire face aux premiers rayons du soleil. 
          </p> 
     </div>
     
  <div class="block01 blk-left">
      <div class="situation"><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/01.jpg" alt="Faire peau neuve"></div>
      <div class="text reveal">
          <h2 class="reveal-2">Faire peau neuve</h2>
          <p class="reveal-3">Que ça soit sur le visage ou le corps, l’hiver a desséché la peau. Pour repartir de zéro, un soin qui va éliminer en douceur les peaux mortes s’impose. A répéter 2 à 3 fois par semaine selon son type de peau. On repart ensuite sur de nouvelles bases en laissant de côté sa crème hydratante trop riche au profit d’un gel qui va pénétrer plus rapidement et éviter les petites brillances trop vite arrivées. Indispensable tous les matins : une protection solaire pour protéger sa peau du vieillissement. 
          </p>
  <!--        <p class="reveal-4"></p>-->
        </div>
          <!-- CAROUSEL MOBILE  -->
        <div class="content-prod">
           <div class="with-carousel-variable-width">
                <ul class="carousel-variable-width js-carousel-variable-width">
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/p/desincrustant+visage-biotherm/75071845/378" class="js-pdt-quick-shop-link" target="_self">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/prod/01.jpg" alt="Désincrustant visage">
                              <span class="carousel-infos">
                                  <span class="title">BIOTHERM</span>
                                  <span class="texte one-line">Désincrustant visage</span>
                                  <span class="link">Voir le produit</span>  
                              </span>
                           </a>
                     </div>
                   </li>
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/p/clarins+men+gel+super+hydratant+-+soin+visage+jour+hydratant+et+fraicheur-clarins/60094824/378" class="js-pdt-quick-shop-link" target="_self">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/prod/02.jpg" alt="Hydratant - Soin visage jour hydratant et fraîcheur">
                             <span class="carousel-infos">
                                  <span class="title">CLARINS</span>
                                  <span class="texte one-line">Clarins Men Gel Super <br class="show-for-xmedium-up">Hydratant - Soin visage jour <br class="show-for-xmedium-up">hydratant et fraîcheur</span>
                                  <span class="link">Voir le produit</span>   
                             </span>
                           </a>
                     </div>
                   </li>
                  <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/p/gel+hydratant+energisant+contour+des+yeux-american+crew/62879215/378" class="js-pdt-quick-shop-link" target="_self">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/prod/03.jpg" alt="Gel Hydratant Energisant Contour des Yeux">
                             <span class="carousel-infos">
                                  <span class="title">AMERICAN CREW</span>
                                  <span  class="texte">Gel Hydratant Energisant <br class="show-for-xmedium-up">Contour des Yeux</span>
                                  <span class="link">Voir le produit</span>
                            </span>
                           </a>
                     </div>
                   </li>
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/p/hello+sunshine+protection+solaire+peaux+sensibles-pai/71932342/378" class="js-pdt-quick-shop-link" target="_self">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/prod/04.jpg" alt="Hello sunshine Protection Solaire Peaux Sensibles">
                             <span class="carousel-infos">
                                  <span class="title">PAI</span>
                                  <span  class="texte">Hello sunshine Protection <br class="show-for-xmedium-up">Solaire Peaux Sensibles</span>
                                  <span class="link">Voir le produit</span>
                            </span>
                           </a>
                     </div>
                   </li>
                </ul>
             </div>
           </div>
      </div>  
         
      <div class="block02 blk-right">
        <div class="situation"><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/02.jpg" alt="À la douche !"></div>
        <div class="text reveal">
           <h2 class="reveal-2">À la douche !</h2>
           <p class="reveal-3">Exit les soins trop riches. Alors qu’en hiver, on préfère les soins hydratants aux odeurs boisées et rassurantes, pour le printemps, on a envie de textures légères et de notes fruitées. Pour cela, on mise sur un gel douche ultra frais qui va nettoyer la peau délicatement sans pour autant la dessécher.  
          </p>
  <!--        <p class="reveal-4"></p>-->
        </div>        
           <!-- CAROUSEL MOBILE  -->
        <div class="content-prod">
           <div class="with-carousel-variable-width">
                <ul class="carousel-variable-width js-carousel-variable-width">
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/p/gel+douche+2+en+1-l+occitane+en+provence/70161748/378" class="js-pdt-quick-shop-link" target="_self">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/prod/05.jpg" alt="Gel douche 2 en 1">
                              <span class="carousel-infos">
                                  <span class="title">L’OCCITANE EN PROVENCE</span>
                                  <span class="texte one-line">Gel douche 2 en 1</span>
                                  <span class="link">Voir le produit</span>  
                             </span>
                           </a>
                     </div>
                   </li>
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/p/gel+douche+2+en+1-l+occitane+en+provence/70161754/378" class="js-pdt-quick-shop-link" target="_self">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/prod/06.jpg" alt="Gel douche 2 en 1">
                              <span class="carousel-infos">
                                  <span class="title">L’OCCITANE EN PROVENCE</span>
                                  <span class="texte one-line">Gel douche 2 en 1</span>
                                  <span class="link">Voir le produit</span>  
                              </span>
                           </a>
                     </div>
                   </li>
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/p/cycle+purete+-+gel+douche+corps+cheveux+dermo-regulateur-66+30/51997286/378" class="js-pdt-quick-shop-link" target="_self">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/prod/07.jpg" alt="Cycle Pureté - Gel Douche Corps & Cheveux Dermo-régulateur">
                             <span class="carousel-infos">
                                  <span class="title">66°30</span>
                                  <span class="texte one-line">Cycle Pureté - Gel Douche <br class="show-for-xmedium-up">Corps & Cheveux Dermo-<br class="show-for-xmedium-up">régulateur</span>
                                  <span class="link">Voir le produit</span>
                            </span>
                           </a> 
                     </div>
                   </li>
                   <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/p/eau+de+neroli+dore+gel+parfume+douche+et+bain-hermes/35601133/378" class="js-pdt-quick-shop-link" target="_self">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/prod/08.jpg" alt="Eau de néroli doré Gel parfumé douche et bain">
                             <span class="carousel-infos">
                                  <span class="title">HERMÈS</span>
                                  <span class="texte">Eau de néroli doré Gel <br class="show-for-xmedium-up">parfumé douche et bain</span>
                                  <span class="link">Voir le produit</span>
                            </span>
                           </a>
                     </div>
                   </li>
                </ul>
             </div>
           </div>
      </div>  
      
      <div class="block03 blk-left">
        <div class="situation"><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/03.jpg" alt="Entretenir sa barbe"></div>
        <div class="text reveal">
          <h2 class="reveal-2">Entretenir sa barbe</h2>
          <p class="reveal-3">Au printemps aussi la barbe nécessite une attention particulière. Pour afficher un style impeccable, la barbe doit être taillée à la tondeuse comme chez le barbier. A l’aide d’un peigne, on dompte les poils rebelles et les frisottis. Côté entretien, un nettoyant solide ou liquide permettra de retirer toutes les impuretés. Enfin, pour un toucher soyeux, on opte pour une huile qui va hydrater le poil en profondeur.
          </p>
  <!--        <p class="reveal-4"></p>-->
        </div>
          <!-- CAROUSEL MOBILE  -->
        <div class="content-prod">
           <div class="with-carousel-variable-width">
                <ul class="carousel-variable-width js-carousel-variable-width">
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/p/huile+a+barbe+5-en-1+bio-66+30/50816015/378" class="js-pdt-quick-shop-link" target="_self">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/prod/09.jpg" alt="Huile à barbe 5-en-1 BIO">
                              <span class="carousel-infos">
                                  <span class="title">66°30</span>
                                  <span class="texte one-line">Huile à barbe 5-en-1 BIO</span>
                                  <span class="link">Voir le produit</span>  
                              </span>
                           </a>
                     </div>
                   </li>
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/p/shampoing+a+barbe+bio-bivouak/50031885/378" class="js-pdt-quick-shop-link" target="_self">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/prod/10.jpg" alt="Shampoing à barbe bio">
                             <span class="carousel-infos">
                                  <span class="title">BIVOUAK</span>
                                  <span class="texte one-line">Shampoing à barbe bio</span>
                                  <span class="link">Voir le produit</span>   
                             </span>
                           </a>
                     </div>
                   </li>
                  <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/p/peigne+en+corne+pour+la+barbe-le+baigneur/53725703/378" class="js-pdt-quick-shop-link" target="_self">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/prod/11.jpg" alt="Peigne en corne pour la barbe">
                             <span class="carousel-infos">
                                  <span class="title">LE BAIGNEUR</span>
                                  <span  class="texte one-line">Peigne en corne pour la barbe</span>
                                  <span class="link">Voir le produit</span>
                            </span>
                           </a>
                     </div>
                   </li>
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/p/tondeuse+barbe+philips+one+blade+qp6620+20-philips/300405443703/378" class="js-pdt-quick-shop-link" target="_self">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/prod/12.jpg" alt="Tondeuse barbe PHILIPS One Blade QP6620/20">
                             <span class="carousel-infos">
                                  <span class="title">PHILIPS</span>
                                  <span  class="texte">Tondeuse barbe PHILIPS One <br class="show-for-xmedium-up">Blade QP6620/20</span>
                                  <span class="link">Voir le produit</span>
                            </span>
                           </a>
                     </div>
                   </li>
                </ul>
             </div>
           </div>
      </div>
      
      <div class="block04 blk-right">
        <div class="situation"><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/04.jpg" alt="Des parfums de saison"></div>
        <div class="text reveal">
             <h2 class="reveal-2">Des parfums de saison</h2>
             <p class="reveal-3">A chaque saison, sa fragrance. Le printemps est l’occasion d’opter pour des senteurs plus légères, plus fraîches. Parmi les parfums iconiques de la saison, H24, le nouveau parfum d’Hermès avec ses notes légères et sensuelles ou encore l’iconique CK All de Calvin Klein, un parfum frais et pétillant aux senteurs de mandarine.  
            </p>
    <!--        <p class="reveal-4"></p>-->
        </div>        
           <!-- CAROUSEL MOBILE  -->
        <div class="content-prod">
           <div class="with-carousel-variable-width">
                <ul class="carousel-variable-width js-carousel-variable-width">
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/p/ck+all+-+eau+de+toilette-calvin+klein/44014999/378" class="js-pdt-quick-shop-link" target="_self">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/prod/13.jpg" alt="Ck All - Eau de toilette">
                              <span class="carousel-infos">
                                  <span class="title">CALVIN KLEIN</span>
                                  <span class="texte one-line">Ck All - Eau de toilette</span>
                                  <span class="link">Voir le produit</span>  
                             </span>
                           </a>
                     </div>
                   </li>
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/p/h24+eau+de+toilette-hermes/75967316/378" class="js-pdt-quick-shop-link" target="_self">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/prod/14.jpg" alt="H24, Eau de toilette">
                              <span class="carousel-infos">
                                  <span class="title">HERMÈS</span>
                                  <span class="texte one-line">H24, Eau de toilette</span>
                                  <span class="link">Voir le produit</span>  
                              </span>
                           </a>
                     </div>
                   </li>
                    <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/p/un+jardin+sur+la+lagune+-+eau+de+toilette-hermes/61734328/378" class="js-pdt-quick-shop-link" target="_self">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/prod/15.jpg" alt="Un Jardin sur la Lagune - Eau de toilette">
                             <span class="carousel-infos">
                                  <span class="title">HERMÈS</span>
                                  <span class="texte">Un Jardin sur la Lagune - Eau <br class="show-for-xmedium-up">de toilette</span>
                                  <span class="link">Voir le produit</span>
                            </span>
                           </a> 
                     </div>
                   </li>
                   <li>
                      <div>
                          <a href="https://www.galerieslafayette.com/p/l+instant+de+guerlain+pour+homme+-+eau+de+toilette-guerlain/40000686/378" class="js-pdt-quick-shop-link" target="_self">
                              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/beaute-homme/prod/16.jpg" alt="L’Instant de Guerlain pour Homme - Eau de Toilette">
                             <span class="carousel-infos">
                                  <span class="title">GUERLAIN</span>
                                  <span class="texte">L’Instant de Guerlain pour <br class="show-for-xmedium-up">Homme - Eau de Toilette</span>
                                  <span class="link">Voir le produit</span>
                            </span>
                           </a>
                     </div>
                   </li>
                </ul>
             </div>
           </div>
      </div> 
      
      <div class="all-coats">
          <h3>Toute la beauté homme</h3>
          <a href="/h/beaute" class="link">Découvrir</a>
      </div>    
  </section>
  <script type="text/javascript" src="https://static.galerieslafayette.com/media/LP/src/js/2019/reveal.js"></script>
  <!--=========================== FIN LANDING PAGE ========================-->
  
<!-- build:js /media/LP/src/js/2021/prada-collection-ss21.min.v01.js 
  <script src="../../assets/js/blazy.min.js"></script>
  <script src="../src/js/2021/prada-collection-ss21.js"></script>
<!-- endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
