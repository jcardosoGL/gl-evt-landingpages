<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "3-labels-beaute-responsable";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../media/LP/src/css/2021/la-beaute-est-un-geste-rose-hermes.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->
  <link href="https://static.galerieslafayette.com/media/LP/src/css/2020/edito_guide_mode02.css?02" rel="stylesheet" type="text/css" />
  <style type="text/css">@media screen and (max-width: 1023px) { 
      .manteaux-hiver .block03 .with-carousel-variable-width li div {
          min-height: 517px;
      }
  }
  .header__product-list {
      display: none;
  }
  .manteaux-hiver .intro span {
      background: #A2228F;
  }
  .manteaux-hiver .text h2, 
  .manteaux-hiver .text:before,
  .manteaux-hiver .all-coats h3 {
      color: #A2228F;
  }
  .manteaux-hiver .all-coats h3 {
      font-size: 57px;
      line-height: 1.18;
      width: auto;
      margin: 0 auto;
      border: none;
  }
  .manteaux-hiver .content-prod a {
      position: relative;
  }
  .manteaux-hiver .content-prod .go-for-good a:after {
      position: absolute;
      content: '';
      width: 19px;
      height: 75px;
      top: 0;
      right: 0;
      background: url(https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/123-soleil/go-for-good.png) no-repeat;
  }
  @media screen and (min-width: 1024px) { 
      .manteaux-hiver .texte.one-line {
          height: 41px;
      }
      .manteaux-hiver .carousel-variable-width .slick-prev, .manteaux-hiver .carousel-variable-width .slick-next {
          display: none !important;
      }
      .header__product-list {
          max-width: 1250px
      }
      .manteaux-hiver .all-coats h3 {
          font-size: 104px;
          line-height: 1.18;
      }
  }
  @media screen and (min-width: 1250px) { 
      .manteaux-hiver .intro h1 {
          font-size: 123px;
          max-width: 1006px;
          margin: 0 auto;
      }
      .manteaux-hiver .text h2 {
          font-size: 51px; 
          letter-spacing: -2px;
      }
      .manteaux-hiver .with-carousel-variable-width {
          max-width: 846px;
      }
      .manteaux-hiver .with-carousel-variable-width li {
          background: #fff;
      }
      .manteaux-hiver .all-coats h3 {
         font-size: 84px;
      }  
      .manteaux-hiver .block02 .with-carousel-variable-width li div {
          min-height: auto;
      }
  }
  </style>
  <section class="manteaux-hiver">
  <div class="intro"><span>GO FOR GOOD</span>
  <h1>3 labels beaut&eacute; responsables</h1>
  
  <p>L&rsquo;&eacute;t&eacute;, face &agrave; la chaleur et &agrave; l&rsquo;humidit&eacute;, notre peau est mise &agrave; rude &eacute;preuve. Pour anticiper ces effets n&eacute;fastes, on twiste notre routine beaut&eacute; avec des soins labellis&eacute;s Go For Good, meilleurs pour nous et pour la plan&egrave;te. R&eacute;sultat ? La peau est pr&ecirc;te &agrave; accueillir les premiers rayons du soleil et &agrave; conserver son h&acirc;le bien au-del&agrave; des vacances. Voici 3 marques &agrave; d&eacute;couvrir d&rsquo;urgence.</p>
  </div>
  
  <div class="block01 blk-left">
  <div class="situation"><img alt="Les fragrances clean de 100bon" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/labels-beaute-responsables/01.jpg" /></div>
  
  <div class="text reveal">
  <h2 class="reveal-2">Les fragrances clean de 100bon</h2>
  
  <p class="reveal-3">100BON est la premi&egrave;re Maison de parfums aux formules naturelles et 100% clean. Au programme ? Des fragrances pour sentir bon et se sentir bien. Formul&eacute;es &agrave; partir d&rsquo;huiles essentielles et d&rsquo;ingr&eacute;dients naturels, les cr&eacute;ations 100BON sont &eacute;labor&eacute;es dans l&rsquo;atelier de la maison install&eacute; &agrave; Grasse. Naturelle, accessible et green, sont les trois promesses de la marque novatrice qui imagine des sillages &eacute;co-con&ccedil;us et rechargeables. Parmi les best-sellers de la marque, le spray Doux R&ecirc;ves qui favorise l&rsquo;endormissement ou encore la fragrance gourmande Amaretto &amp; Framboise poudr&eacute;e.</p>
  <!--        <p class="reveal-4"></p>--></div>
  <!-- CAROUSEL MOBILE  -->
  
  <div class="content-prod">
  <div class="with-carousel-variable-width">
  <ul class="carousel-variable-width js-carousel-variable-width">
    <li>
    <div class="go-for-good"><a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/76180203/378" target="_self"><img alt="Doux-Rêves, Spray" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/labels-beaute-responsables/prod/01.jpg" /> <span class="carousel-infos"> <span class="title">100BON</span> <span class="texte one-line">Doux-R&ecirc;ves, Spray</span> <span class="link">Voir le produit</span> </span> </a></div>
    </li>
    <li>
    <div class="go-for-good"><a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/76180214/378" target="_self"><img alt="Soleil d’Ambre" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/labels-beaute-responsables/prod/02.jpg" /> <span class="carousel-infos"> <span class="title">100BON</span> <span class="texte one-line">Soleil d&rsquo;Ambre</span> <span class="link">Voir le produit</span> </span> </a></div>
    </li>
    <li>
    <div class="go-for-good"><a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/76180275/378" target="_self"><img alt="Mirage du Désert" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/labels-beaute-responsables/prod/03.jpg" /> <span class="carousel-infos"> <span class="title">100BON</span> <span class="texte one-line">Mirage du D&eacute;sert</span> <span class="link">Voir le produit</span> </span> </a></div>
    </li>
    <li>
    <div class="go-for-good"><a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/76180328/378" target="_self"><img alt="Amaretto &amp; Framboise Poudre, eau fraîche" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/labels-beaute-responsables/prod/04.jpg" /> <span class="carousel-infos"> <span class="title">100BON</span> <span class="texte">Amaretto &amp; Framboise<br class="show-for-xmedium-up" />
    Poudre, eau fra&icirc;che</span> <span class="link">Voir le produit</span> </span> </a></div>
    </li>
  </ul>
  </div>
  </div>
  </div>
  
  <div class="block02 blk-right">
  <div class="situation"><img alt="Les soins solaires des Laboratoires de Biarritz" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/labels-beaute-responsables/02.jpg" /></div>
  
  <div class="text reveal">
  <h2 class="reveal-2">Les soins solaires des Laboratoires de Biarritz</h2>
  
  <p class="reveal-3">Inspir&eacute;s par les ressources de l&rsquo;oc&eacute;an, les Laboratoires de Biarritz imaginent des soins bio d&rsquo;origine naturelle compos&eacute;s d&rsquo;actifs brevet&eacute;s. Sensibles aux probl&eacute;matiques environnementales, les fondateurs des Laboratoires de Biarritz &eacute;laborent chaque saison des produits respectueux de l&rsquo;&eacute;cosyst&egrave;me marin qui assurent une protection optimale pour la peau. Au c&oelig;ur de chaque produit, un principe actif &agrave; base d&rsquo;algues de la c&ocirc;te basque. Les cr&egrave;mes solaires min&eacute;rales, le stick solaire SPF50 ou encore le baume &agrave; l&egrave;vres font partie des essentiels de la marque.</p>
  <!--        <p class="reveal-4"></p>--></div>
  <!-- CAROUSEL MOBILE  -->
  
  <div class="content-prod">
  <div class="with-carousel-variable-width">
  <ul class="carousel-variable-width js-carousel-variable-width">
    <li>
    <div class="go-for-good"><a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/70745460/378" target="_self"><img alt="Lait Solaire SPF 30 certifié Bio" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/labels-beaute-responsables/prod/05.jpg" /> <span class="carousel-infos"> <span class="title">LABO DE BIARRITZ</span> <span class="texte one-line">Lait Solaire SPF 30 certifi&eacute; Bio</span> <span class="link">Voir le produit</span> </span> </a></div>
    </li>
    <li>
    <div class="go-for-good"><a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/70745474/378" target="_self"><img alt="Baume à Lèvres SPF30 Mer et Montagne certifié Bio" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/labels-beaute-responsables/prod/06.jpg" /> <span class="carousel-infos"> <span class="title">LABO DE BIARRITZ</span> <span class="texte">Baume &agrave; L&egrave;vres SPF30 Mer et<br class="show-for-xmedium-up" />
    Montagne certifi&eacute; Bio</span> <span class="link">Voir le produit</span> </span> </a></div>
    </li>
    <li>
    <div class="go-for-good"><a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/70745477/378" target="_self"><img alt="Fluide après Soleil certifié Bio" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/labels-beaute-responsables/prod/07.jpg" /> <span class="carousel-infos"> <span class="title">LABO DE BIARRITZ</span> <span class="texte one-line">Fluide apr&egrave;s Soleil certifi&eacute; Bio</span> <span class="link">Voir le produit</span> </span> </a></div>
    </li>
    <li>
    <div class="go-for-good"><a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/70745456/378" target="_self"><img alt="" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/labels-beaute-responsables/prod/08.jpg" /> <span class="carousel-infos"> <span class="title">LABO DE BIARRITZ</span> <span class="texte">Cr&egrave;me Solaire Visage SPF30<br class="show-for-xmedium-up" />
    certifi&eacute; Bio</span> <span class="link">Voir le produit</span> </span> </a></div>
    </li>
  </ul>
  </div>
  </div>
  </div>
  
  <div class="block03 blk-left">
  <div class="situation"><img alt="PAI skincare, pour les peaux sensibles" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/labels-beaute-responsables/03.jpg" /></div>
  
  <div class="text reveal">
  <h2 class="reveal-2">PAI skincare, pour les peaux sensibles</h2>
  
  <p class="reveal-3">La mission de Pai Skincare ? D&eacute;velopper des produits et des soins destin&eacute;s aux peaux les plus sensibles. Saison apr&egrave;s saison, la griffe londonienne fait partie des marques les plus responsables avec ses produits haute-tol&eacute;rance et certifi&eacute;s. Avec leurs compositions irr&eacute;prochables et leurs textures toutes douces, les gammes Pai Skincare labellis&eacute;es par Cosmos, Cruelty-Free International et la Vegan Society s&rsquo;attaquent aux peaux sujettes aux picotements, aux rougeurs et aux d&eacute;mangeaisons. Parmi les incontournables, l&rsquo;huile de Rosier Sauvage et la cr&egrave;me pour le corps au Calendula.</p>
  <!--        <p class="reveal-4"></p>--></div>
  <!-- CAROUSEL MOBILE  -->
  
  <div class="content-prod">
  <div class="with-carousel-variable-width">
  <ul class="carousel-variable-width js-carousel-variable-width">
    <li>
    <div class="go-for-good"><a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/71932335/378" target="_self"><img alt="Huile BioRegenerate Rosier Sauvage" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/labels-beaute-responsables/prod/09.jpg" /> <span class="carousel-infos"> <span class="title">PAI</span> <span class="texte">Huile BioRegenerate Rosier<br class="show-for-xmedium-up" />
    Sauvage</span> <span class="link">Voir le produit</span> </span> </a></div>
    </li>
    <li>
    <div class="go-for-good"><a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/71932333/378" target="_self"><img alt="Crème Délicate Pour Les Yeux Vipérine &amp; Argan" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/labels-beaute-responsables/prod/10.jpg" /> <span class="carousel-infos"> <span class="title">PAI</span> <span class="texte">Cr&egrave;me D&eacute;licate Pour Les Yeux<br class="show-for-xmedium-up" />
    Vip&eacute;rine &amp; Argan</span> <span class="link">Voir le produit</span> </span> </a></div>
    </li>
    <li>
    <div class="go-for-good"><a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/71932367/378" target="_self"><img alt="Exfoliant Illuminateur de Teint Noix de Bancoule &amp; Perles de Jojoba" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/labels-beaute-responsables/prod/11.jpg" /> <span class="carousel-infos"> <span class="title">PAI</span> <span class="texte one-line">Exfoliant Illuminateur de Teint<br class="show-for-xmedium-up" />
    Noix de Bancoule &amp; Perles de<br class="show-for-xmedium-up" />
    Jojoba</span> <span class="link">Voir le produit</span> </span> </a></div>
    </li>
    <li>
    <div class="go-for-good"><a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/71932374/378" target="_self"><img alt="Comfrey &amp; Calendula Calming Body Cream Tube 2017*" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/edito/labels-beaute-responsables/prod/12.jpg" /> <span class="carousel-infos"> <span class="title">PAI</span> <span class="texte">Comfrey &amp; Calendula Calming<br class="show-for-xmedium-up" />
    Body Cream Tube 2017*</span> <span class="link">Voir le produit</span> </span> </a></div>
    </li>
  </ul>
  </div>
  </div>
  </div>
  
  <div class="all-coats">
  <h3>Toute la s&eacute;lection beaut&eacute;<br />
  Go For Good</h3>
  <a class="link" href="https://www.galerieslafayette.com/t/go-for-good/c/beaute?l=n">D&eacute;couvrir</a></div>
  </section>
  <script type="text/javascript" src="https://static.galerieslafayette.com/media/LP/src/js/2019/reveal.js"></script><!--=========================== FIN LANDING PAGE ========================-->
  
<!-- build:js /media/LP/src/js/2021/prada-collection-ss21.min.v01.js 
  <script src="../../assets/js/blazy.min.js"></script>
  <script src="../src/js/2021/prada-collection-ss21.js"></script>
<!-- endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
