<?php include ('pages-defaults/header.php'); ?>
<script>
  document.title = "Journée de la femme";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../media/LP/src/css/2021/journeedelafemme.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/journeedelafemme.min.v03.css" rel="stylesheet" type="text/css" />
<style>
  html {
    scroll-behavior: smooth;
  }
</style>
<div class="journeedelafemme">
  <section class="journeedelafemme-hero">
    <div class="container">
      <div class="luxe-row no-gutter">
        <div class="luxe-col-mobile-12">
          <h1>
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/journeedelafemme/happy_women-b.png"
              srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/journeedelafemme/happy_women-b@2x.png 2x" 
              alt="Happy Women - Galeries Lafayette">
            </figure>
          </h1>
        </div>
        <div class="luxe-col-mobile-11 luxe-col-mobile-offset-1 luxe-col-tablet-8 luxe-col-tablet-offset-2 luxe-col-widescreen-6 luxe-col-widescreen-offset-3">
          <p>A l’occasion de la journée internationale des droits des Femmes (lundi 8 mars 2021), les Galeries Lafayette réaffirment leur engagement auprès de la Fondation des Femmes et présentent <span class="is-bold"
          >HAPPY WOMEN</span>.</p>
          <p>HAPPY WOMEN, c’est l’occasion de vous engager pour la cause des femmes avec les Galeries Lafayette en réalisant un don à la Fondation des Femmes via l’arrondi en caisse ou via l’achat d’un produit solidaire Atelier Amelot aux Galeries Lafayette Champs Elysées, mais aussi en participant à la campagne #RegardeMoiBien.</p>
        </div>
        <div class="luxe-col-mobile-11 luxe-col-mobile-offset-1 luxe-col-tablet-8 luxe-col-tablet-offset-2 luxe-col-widescreen-6 luxe-col-widescreen-offset-3">
          <iframe id="regiondo-booking-widget" data-width="100%" data-url="https://galerieslafayette.regiondo.fr/bookingwidget/vendor/12477/id/180479" data-title="Cours de Macarons aux Galeries Lafayette" style="border:0;background:transparent;"></iframe>
          <script id="regiondo-booking-js" src="https://cdn.regiondo.net/js/integration/regiondo-booking.js" async defer></script>
        </div>
      </div>
    </div>
  </section>
  
  <!-- centimes -->
  <div class="hash" id="arrondi"></div>
  <section class="journeedelafemme-body arrondi">
    <div class="container">
      <div class="luxe-row no-gutter">
        <div class="luxe-col-mobile-8 luxe-col-tablet-5 luxe-col-widescreen-6">
          <h2>Vos centimes ont du pouvoir !</h2>
        </div>
        <div class="luxe-col-mobile-10 luxe-col-mobile-offset-1  luxe-col-tablet-8 luxe-col-tablet-offset-2 luxe-col-widescreen-6 luxe-col-widescreen-offset-3">
          <p>Du 1er février au 28 mars, l’ensemble des <a href="https://www.galerieslafayette.com/m/nos-magasins">magasins Galeries Lafayette en France</a> vous proposent de faire un don à la Fondation des Femmes lors du paiement de vos achats en caisse, en arrondissant à l’euro supérieur. </p>
          <p><span class="is-bold">A quoi servent les dons ?</span> La somme collectée permettra de financer des actions pour faire reculer les violences faites aux femmes et faire avancer l’égalité entre les femmes et les hommes.</p>
          <p><span class="is-bold">Comment s’engagent les Galeries Lafayette ?</span> Nous avons déjà pu reverser <span class="is-bold">354 000 euros</span> à la Fondation en 2020 grâce au développement de produits solidaires avec notre marque propre Galeries Lafayette (dont les bénéfices étaient reversés à la Fondation). Nous avons ainsi contribué à financer l’accueil d’une cinquantaine d’associations au sein de la Cité Audacieuse, d’un studio de podcast féministe et surtout la mise en place d’une plateforme logistique d’urgence pendant les deux confinements pour répartir des biens de première nécessité à destination des associations luttant contre les violences conjugales. Nos collaborateurs ont aussi pu s’engager pour la Fondation en donnant de leur temps sur le chantier de la Cité Audacieuse, ou en apprenant à réagir dans les situations de harcèlement de rue grâce à la formation Stand Up.</p>
            
          <p><a href="https://www.groupegalerieslafayette.fr/engagements/#responsabilite-solidarite" class="has-text-white">Découvrir l’ensemble des engagements des Galeries Lafayette</a></p>
        </div>
      </div>
    </div>
  </section>
  
  <!-- solidaire -->
  <div class="hash" id="solidaire"></div>
  <section class="journeedelafemme-body solidaire">
    <div class="container">
      <div class="luxe-row no-gutter">
        <div class="luxe-col-mobile-10 luxe-col-tablet-5 luxe-col-widescreen-6">
          <h2>Achetez engagé</h2>
        </div>
        <div class="luxe-col-mobile-10 luxe-col-mobile-offset-1 luxe-col-tablet-8 luxe-col-tablet-offset-2 luxe-col-widescreen-6 luxe-col-widescreen-offset-3">
          <figure class="image">
            <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/journeedelafemme/pdt-solidaire-b.png"
            srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/journeedelafemme/pdt-solidaire-b@2x.png 2x" 
            alt="Le produit solidaire - Galeries Lafayette">
          </figure>
          <p>Du 4 au 14 mars, les Galeries Lafayette Champs Elysées et Atelier Amelot vous proposent d’acheter des articles solidaires. Rendez-vous en magasin pour personnaliser vos t-shirts, sweats, hoodies, avec la Fondation des Femmes, les bénéfices des ventes permettront d’aider à lutter contre les violences conjugales.</p>
        </div>
      </div>
    </div>
  </section>
  
</div>
<!--=========================== FIN LANDING PAGE ========================-->

<!-- <script src="https://static.galerieslafayette.com/media/LP/src/js/2021/prada-collection-ss21.min.v01.js"></script> --> 
  
<!-- build:js /media/LP/src/js/2021/journeedelafemme.min.v01.js
  <script src="src/js/2021/journeedelafemme.js"></script>
<!-- endbuild -->
  
<?php include ('pages-defaults/footer.php'); ?>
