<?php include ('../pages-defaults/header.php'); ?>
<script>
	document.title = "Richs Luxe SAD";
	document.querySelector(".section-main-container").classList.add("brands-page");
</script>
<div class="row header__product-list">
	<div class="header__product-list--first-line">
		<div class="columns large-12 medium-24">
			 <ul id="breadcrumb" class="breadcrumbs">
					<li>
						<a href="/" data-ga="gaEvent|gaEvent_action:Chemin de fer|gaEvent_category:Page marque|gaEvent_label:Accueil">
							<span>Accueil</span>
						</a>
					</li>
					<li>
						<a href="/marques" data-ga="gaEvent|gaEvent_action:Chemin de fer|gaEvent_category:Page marque|gaEvent_label:Retour Univers">Marques et créateurs</a>
					</li>
					<li>
						<a href="#">Byredo</a>
					</li>
			 </ul>
		</div>
		<div class="columns large-12 show-for-large-up">
			<p id="title-article-counter" class="quantity-title">
				<span class="js-total-records-text nowrap">
					91 articles
				</span>
			</p>
		</div>
		<div id="title" class="results-title">
			<h1 class="main-title">
				Patou			
			</h1>
		</div>
	</div>         
</div>

<!-- alt template -->
<?php // include ('alt-template/the-kooples-tomorrow.html'); ?>
<?php // include ('alt-template/polo-ralph-lauren-generique.html'); ?>
<?php // include ('alt-template/polo-ralph-lauren-femme.html'); ?>
<?php // include ('alt-template/polo-ralph-lauren-homme.html'); ?>
<?php // include ('alt-template/polo-ralph-lauren-enfant.html'); ?>
<?php // include ('alt-template/kenzo-coming-soon.html'); ?>

<!-- old template -->
<?php // include ('old-template/sad-la-perla-no-cta.html'); ?>
<?php // include ('old-template/sad-roger-vivier-no-cta.html'); ?>
<?php // include ('old-template/eshop-de-beers.html'); ?>
<?php // include ('old-template/sad-de-beers.html'); ?>
<?php // include ('old-template/sad-prada-homme.html'); ?>
<?php // include ('old-template/sad-prada-femme.html'); ?>
<?php // include ('old-template/sad-prada-racine.html'); ?>
<?php // include ('old-template/sad-givenchy.html'); ?>
<?php // include ('old-template/sad-tods-homme.html'); ?>
<?php // include ('old-template/sad-tods-femme.html'); ?>
<?php // include ('old-template/sad-alexander-mcqueen.html'); ?>
<?php // include ('old-template/sad-kenzo.html'); ?>
<?php // include ('old-template/sad-kenzo-femme.html'); ?>
<?php // include ('old-template/sad-miu-miu-no-cta.html'); ?>
<?php // include ('old-template/sad-pomellato.html'); ?>
<?php // include ('old-template/sad-jacquemus-pe22.html'); ?>
<?php // include ('old-template/sad-chaumet-noel21.html'); ?>
<?php // include ('old-template/sad-chloe-fw21.html'); ?>
<?php // include ('old-template/sad-isabel-marant.html'); ?>
<?php // include ('old-template/sad-icicle-no-cta.html'); ?>
<?php // include ('old-template/sad-prada-no-cta.html'); ?>
<?php // include ('old-template/sad-bottega-veneta.html'); ?>
<?php // include ('old-template/sad-valentino-garavani-ah21.html'); ?>
<?php // include ('old-template/sad-valentino-ah21.html'); ?>
<?php // include ('old-template/sad-jw-anderson-no-cta.html'); ?>
<?php // include ('old-template/sad-off-white.html'); ?>
<?php // include ('old-template/sad-gauchere-no-cta.html'); ?>
<?php // include ('old-template/sad-alessandra-rich-no-cta.html'); ?>
<?php // include ('old-template/sad-chaumet-no-cta.html'); ?>
<?php // include ('old-template/sad-saint-laurent-no-cta.html'); ?>
<?php // include ('old-template/sad-fred.html'); ?>

<?php // include ('old-template/sad-cartier.html'); ?>
<?php // include ('old-template/sad-loewe.html'); ?>

<!-- e-shop -->
<?php // include ('eshop-old/eshop-paladini.html'); ?>
<?php //include ('eshop-old/eshop-gianvito-rossi-no-cta.html'); ?>
<?php // include ('eshop-old/eshop-dodo-no-cta.html'); ?>
<?php // include ('eshop-old/eshop-tudor-no-cta.html'); ?>
<?php // include ('eshop-old/eshop-versace.html'); ?>
<?php // include ('eshop-old/eshop-versace-femme.html'); ?>
<?php // include ('eshop-old/eshop-versace-homme.html'); ?>
<?php // include ('eshop-old/eshop-weston-no-cta.html'); ?>
<?php // include ('eshop-old/eshop-tartine-et-chocolat.html'); ?>
<?php // include ('eshop-old/eshop-dsquared2-no-cta.html'); ?>
<?php // include ('eshop-old/eshop-pomellato-no-cta.html'); ?>
<?php // include ('eshop-old/eshop-hogan-fw21-no-cta.html'); ?>
<?php // include ('eshop-old/eshop-stone-island-fw21-no-cta.html'); ?>
<?php // include ('eshop-old/eshop-stone-island-no-cta.html'); ?>
<?php // include ('eshop-old/eshop-letrange-no-cta.html'); ?>
<?php // include ('eshop-old/eshop-golden-goose-no-cta.html'); ?>
<?php // include ('eshop-old/eshop-tela-no-cta.html'); ?>
<?php // include ('eshop-old/eshop-hogan.html'); ?>
<?php // include ('eshop-old/eshop-luniform.html'); ?>
<?php // include ('eshop-old/eshop-valextra.html'); ?>
<?php // include ('eshop-old/eshop-qeelin-no-cta.html'); ?>

<!-- new template e-shop -->
<?php // include ('eshop/eshop-montblanc.html'); ?>
<?php include ('eshop/eshop-hogan.html'); ?>
<?php // include ('eshop/eshop-ami.html'); ?>
<?php // include ('eshop/eshop-versace-generique.html'); ?>
<?php // include ('eshop/eshop-versace-homme.html'); ?>
<?php // include ('eshop/eshop-versace-femme.html'); ?>
<?php // include ('eshop/eshop-luniform-no-cta.html'); ?>
<?php // include ('eshop/eshop-gianvito-rossi-no-cta.html'); ?>

<!-- new template e-shop video -->
<?php // include ('eshop-actu-video/eshop-video-ami.html'); ?>
<?php // include ('eshop-actu-video/eshop-video-patou.html'); ?>
<?php // include ('eshop-actu-video/eshop-video-ines-de-la-fressange.html'); ?>
<?php // include ('eshop-actu-video/eshop-video-stone-island.html'); ?>
	
<!-- new template sad actu -->
<?php // include ('sad-actu/sad-burberry.html'); ?>	
		
<!-- new template -->
<?php // include ('sad-bottega-veneta.html'); ?>
<?php // include ('sad-bottega-veneta-femme.html'); ?>
<?php // include ('sad-bottega-veneta-homme.html'); ?>
<?php // include ('sad-chaumet.html'); ?>
<?php // include ('sad-balenciaga.html'); ?>
<?php // include ('sad-balenciaga-femme.html'); ?>
<?php // include ('sad-balenciaga-homme.html'); ?>
<?php // include ('sad-balenciaga-hourglass.html'); ?>
<?php // include ('sad-alessandra-rich-no-cta.html'); ?>
<?php // include ('sad-alexander-mcqueen.html'); ?>
<?php // include ('sad-alexander-mcqueen-femme.html'); ?>
<?php // include ('sad-alexander-mcqueen-homme.html'); ?>
<?php // include ('sad-burberry-no-actu.html'); ?>	
<?php // include ('sad-chloe.html'); ?>
<?php // include ('sad-fred.html'); ?>
<?php // include ('sad-gauchere-no-cta.html'); ?>
<?php // include ('sad-givenchy.html'); ?>
<?php // include ('sad-icicle-no-cta.html'); ?>
<?php // include ('sad-isabel-marant.html'); ?>
<?php // include ('sad-jacquemus.html'); ?>
<?php // include ('sad-jw-anderson-no-cta.html'); ?>
<?php // include ('sad-kenzo-generique.html'); ?>
<?php // include ('sad-kenzo-femme.html'); ?>
<?php // include ('sad-kenzo-homme.html'); ?>
<?php // include ('sad-la-perla-no-cta.html'); ?>
<?php // include ('sad-miu-miu.html'); ?>
<?php // include ('sad-off-white.html'); ?>
<?php // include ('sad-prada-racine.html'); ?>
<?php // include ('sad-prada-femme.html'); ?>
<?php // include ('sad-prada-homme.html'); ?>
<?php // include ('sad-roger-vivier-no-cta.html'); ?>
<?php // include ('sad-tods-homme.html'); ?>
<?php // include ('sad-tods.html'); ?>
<?php // include ('sad-valentino.html'); ?>
<?php // include ('sad-valentino-femme.html'); ?>
<?php // include ('sad-valentino-homme.html'); ?>
<?php // include ('sad-valentino-garavani.html'); ?>
<?php // include ('sad-valentino-garavani-femme.html'); ?>
<?php // include ('sad-valentino-garavani-homme.html'); ?>
<?php // include ('sad-eres.html'); ?>
<?php // include ('sad-jimmy-choo.html'); ?>


 <!-- templates -->
<?php // include ('templates/template-sad.html'); ?>
<?php // include ('templates/template-eshop.html'); ?>
<?php // include ('templates/template-sad-actu.html'); ?>
<?php // include ('templates/template-eshop-actu.html'); ?>
<?php // include ('templates/template-eshop-actu-video.html'); ?>
<?php // include ('templates/template-eshop-v1.html'); ?>
<?php // include ('templates/template-new-stack.html'); ?>
	
<!-- build:js /media/LP/src/js/2021/shoppingadistance-goinstore-v2.min.v02.js 
	<script src="src/js/2021/shoppingadistance-goinstore-v2.js"></script>
<!-- endbuild -->



<div class="row header__product-list__filter">
	<div id="new-filters" class="columns large-7 medium-17 small-20">
		 <div class="head columns small-offset-3 small-21">
				<p class="head-title columns small-19 medium-21 large-21">Filtrer par</p>
				<p class="head-close columns small-5 medium-3 large-3"></p>
		 </div>
		 <div id="available-facets" class="js-sticky-nav push-top columns small-offset-3 small-21">
				<div id="filtrer-par" class="filter-group-wrapper">
					 <div id="drop-group-filter" class="filter-group-toogle filter-group js-fgrp" data-tnr-id="qa-catalog-dynamic-results-filters-actives-container">
							<dl class="filter-group-content js-fgrp-pnl accordion" data-accordion="filter-accordion">
								 <dd id="filter-wrapper-facet-custom" class="columns small-24 js-accordion-navigation accordion-navigation filter-wrapper active" data-tnr-id="qa-catalog-dynamic-results-filters-ligne-produit-container">
										<a href="#drop-ligne-produit" class="filter-toogle" aria-expanded="true">Ligne Produit</a>
										<div id="drop-ligne-produit" class="content filter-panel active content-facet">
											 <div class="filter-content columns small-21">
													<ul>
														 <li>
																<span class="js-scramble-link  js-ajax-load" data-href="||b||byredo||flp||collection+maison" data-target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Ligne Produit|gaEvent_category:Page marque|gaEvent_label:Collection maison">
																Collection maison
																</span>
														 </li>
														 <li>
																<span class="js-scramble-link  js-ajax-load" data-href="||b||byredo||flp||fragrance" data-target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Ligne Produit|gaEvent_category:Page marque|gaEvent_label:Fragrance">
																Fragrance
																</span>
														 </li>
														 <li>
																<span class="js-scramble-link  js-ajax-load" data-href="||b||byredo||flp||soin+pour+le+corps" data-target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Ligne Produit|gaEvent_category:Page marque|gaEvent_label:Soin pour le corps">
																Soin pour le corps
																</span>
														 </li>
														 <li>
																<span class="js-scramble-link  js-ajax-load" data-href="||b||byredo||flp||soin+pour+les+mains" data-target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Ligne Produit|gaEvent_category:Page marque|gaEvent_label:Soin pour les mains">
																Soin pour les mains
																</span>
														 </li>
													</ul>
											 </div>
										</div>
								 </dd>
									<dd id="filter-wrapper-facet-custom" class="columns small-24 js-accordion-navigation accordion-navigation filter-wrapper active" data-tnr-id="qa-catalog-dynamic-results-filters-categories-container">
											<a href="#drop-categories" class="filter-toogle" aria-expanded="true">Catégories</a>
											<div id="drop-categories" class="content filter-panel content-facet active">
											<div class="filter-search-wrapper columns small-21">
											<input type="text" name="filter-search" class="filter-search" placeholder="Rechercher">
											</div>
											<div class="filter-content columns small-21">
											<ul>
											<li>
											<a class="js-scramble-link active js-ajax-load" href="/b/the+kooples" target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Categories|gaEvent_category:Page marque|gaEvent_label:HommeHomme">
											Homme
											</a>
											</li>
											<li>
											<a class="js-scramble-link  js-ajax-load" href="/b/the+kooples/ct/homme-accessoires" target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Categories|gaEvent_category:Page marque|gaEvent_label:HommeAccessoires">
											Accessoires
											</a>
											</li>
											<li>
											<a class="js-scramble-link  js-ajax-load" href="/b/the+kooples/ct/homme-bijouterie+et+joaillerie" target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Categories|gaEvent_category:Page marque|gaEvent_label:HommeBijouterie et joaillerie">
											Bijouterie et joaillerie
											</a>
											</li>
											<li>
											<a class="js-scramble-link  js-ajax-load" href="/b/the+kooples/ct/homme-chaussures" target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Categories|gaEvent_category:Page marque|gaEvent_label:HommeChaussures">
											Chaussures
											</a>
											</li>
											<li>
											<a class="js-scramble-link  js-ajax-load" href="/b/the+kooples/ct/homme-sacs+et+bagages" target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Categories|gaEvent_category:Page marque|gaEvent_label:HommeSacs et bagages">
											Sacs et bagages
											</a>
											</li>
											<li>
											<a class="js-scramble-link  js-ajax-load" href="/b/the+kooples/ct/homme-sous-vetements+et+homewear" target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Categories|gaEvent_category:Page marque|gaEvent_label:HommeSous-vêtements et Homewear">
											Sous-vêtements et Homewear
											</a>
											</li>
											<li>
											<a class="js-scramble-link  js-ajax-load" href="/b/the+kooples/ct/homme-vetements" target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Categories|gaEvent_category:Page marque|gaEvent_label:HommeVêtements">
											Vêtements
											</a>
											</li>
											</ul>
											</div>
											</div>
											</dd>
									<dd id="filter-wrapper-facet-custom" class="columns small-24 js-accordion-navigation accordion-navigation filter-wrapper active" data-tnr-id="qa-catalog-dynamic-results-filters-couleurs-container">
<a href="#drop-couleurs" class="filter-toogle" aria-expanded="true">Couleurs</a>
<div id="drop-couleurs" class="content filter-panel content-facet active">
<div class="filter-search-wrapper columns small-21">
<input type="text" name="filter-search" class="filter-search" placeholder="Rechercher">
</div>
<div class="filter-content columns small-21">
<ul>
<li>
<a class="js-scramble-link  js-ajax-load" href="/b/the+kooples/ct/homme/fc/beige" target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Couleur|gaEvent_category:Page marque|gaEvent_label:Beige">
Beige
</a>
</li>
<li>
<a class="js-scramble-link  js-ajax-load" href="/b/the+kooples/ct/homme/fc/blanc" target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Couleur|gaEvent_category:Page marque|gaEvent_label:Blanc">
Blanc
</a>
</li>
<li>
<a class="js-scramble-link  js-ajax-load" href="/b/the+kooples/ct/homme/fc/bleu" target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Couleur|gaEvent_category:Page marque|gaEvent_label:Bleu">
Bleu
</a>
</li>
<li>
<a class="js-scramble-link  js-ajax-load" href="/b/the+kooples/ct/homme/fc/gris" target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Couleur|gaEvent_category:Page marque|gaEvent_label:Gris">
Gris
</a>
</li>
<li>
<a class="js-scramble-link  js-ajax-load" href="/b/the+kooples/ct/homme/fc/jaune" target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Couleur|gaEvent_category:Page marque|gaEvent_label:Jaune">
Jaune
</a>
</li>
<li>
<a class="js-scramble-link  js-ajax-load" href="/b/the+kooples/ct/homme/fc/marron" target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Couleur|gaEvent_category:Page marque|gaEvent_label:Marron">
Marron
</a>
</li>
<li>
<a class="js-scramble-link  js-ajax-load" href="/b/the+kooples/ct/homme/fc/noir" target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Couleur|gaEvent_category:Page marque|gaEvent_label:Noir">
Noir
</a>
</li>
<li>
<a class="js-scramble-link  js-ajax-load" href="/b/the+kooples/ct/homme/fc/rose" target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Couleur|gaEvent_category:Page marque|gaEvent_label:Rose">
Rose
</a>
</li>
<li>
<a class="js-scramble-link  js-ajax-load" href="/b/the+kooples/ct/homme/fc/rouge" target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Couleur|gaEvent_category:Page marque|gaEvent_label:Rouge">
Rouge
</a>
</li>
<li>
<a class="js-scramble-link  js-ajax-load" href="/b/the+kooples/ct/homme/fc/vert" target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Couleur|gaEvent_category:Page marque|gaEvent_label:Vert">
Vert
</a>
</li>
</ul>
</div>
</div>
</dd>
								 <dd id="filter-wrapper-facet-custom" class="columns small-24 js-accordion-navigation accordion-navigation filter-wrapper active" data-tnr-id="qa-catalog-dynamic-results-filters-prix-container">
										<a href="#drop-prix" class="filter-toogle" aria-expanded="true">Prix</a>
										<div id="drop-prix" class="content filter-panel active content-facet">
											 <div class="filter-content columns small-21">
													<div class="slider-box slider-price-box">
														 <input type="text" id="price-range-min" class="price-min js-ajax-load-on-submit" />
														 <input type="text" id="price-range-max" class="price-max js-ajax-load-on-submit" />
														 <div id="price-range" class="slider js-ajax-load-on-submit" data-ga-slider="" data-base-slider-ga="gaEvent|gaEvent_action:Filtre disponible - TranchePrix|gaEvent_category:Page marque|gaEvent_label:" data-template-url="/b/byredo/prix/${min}-${max}" data-price-min="30" data-price-max="245" data-price-start="30" data-price-end="245"></div>
													</div>
											 </div>
										</div>
								 </dd>
							</dl>
							<a href="#" class="filter-toogle filter-group-toogle" data-dropdown="drop-sort" aria-controls="drop-sort" aria-expanded="false">
							<span class="show-for-small-only">Trier par</span>
							<span class="show-for-medium-up">Trier par</span>
							</a>
					 </div>
				</div>
		 </div>
	</div>
	<div class="header__product-list__filter--button columns large-12 medium-12 small-8 text-left">
		 <button class="button button--filter large-6 medium-10 small-24">Filtrer par</button>
	</div>
	<div class="columns large-12 medium-12 small-16">
		 <div class="columns large-offset-12 large-12 medium-offset-10 medium-14 small-offset-1 small-23 sort-by">
				<ul>
					 <li class="init"><span class="sort-by__text">Trier par</span></li>
					 <div class="sort-by__options">
							<li>
								 <span class="js-scramble-link js-ajax-load" data-href="||b||byredo||tri||prix-croissant" data-target="_self" data-ga="gaEvent|gaEvent_action:Choix du tri|gaEvent_category:Page marque|gaEvent_label:Prix croissant">
								 <span class="filter-by-text">Trier par</span>
								 Prix croissant
								 </span>
							</li>
							<li>
								 <span class="js-scramble-link js-ajax-load" data-href="||b||byredo||tri||prix-decroissant" data-target="_self" data-ga="gaEvent|gaEvent_action:Choix du tri|gaEvent_category:Page marque|gaEvent_label:Prix decroissant">
								 <span class="filter-by-text">Trier par</span>
								 Prix decroissant
								 </span>
							</li>
							<li>
								 <span class="js-scramble-link js-ajax-load" data-href="||b||byredo||tri||meilleures-ventes" data-target="_self" data-ga="gaEvent|gaEvent_action:Choix du tri|gaEvent_category:Page marque|gaEvent_label:Meilleures ventes">
								 <span class="filter-by-text">Trier par</span>
								 Meilleures ventes
								 </span>
							</li>
							<li>
								 <span class="js-scramble-link js-ajax-load" data-href="||b||byredo||tri||nouveautes" data-target="_self" data-ga="gaEvent|gaEvent_action:Choix du tri|gaEvent_category:Page marque|gaEvent_label:Nouveautes">
								 <span class="filter-by-text">Trier par</span>
								 Nouveautes
								 </span>
							</li>
							<li>
								 <span class="js-scramble-link js-ajax-load" data-href="||b||byredo||tri||reduction" data-target="_self" data-ga="gaEvent|gaEvent_action:Choix du tri|gaEvent_category:Page marque|gaEvent_label:Reduction Decroissante">
								 <span class="filter-by-text">Trier par</span>
								 Reduction Decroissante
								 </span>
							</li>
					 </div>
				</ul>
		 </div>
	</div>
</div>
<div class="product-list">
	 <div id="catalog" class="row">
			<div id="filters" class="expends-top-bottom columns block-filters filters-overflow-y small-24 small-centered xmedium-5 xmedium-uncentered large-4 large-uncentered hide">
				 <div id="filterBy" class="columns small-10 medium-10 xmedium-4 xmedium-push-0 xmedium-hide results-sort">
						<a href="#drop-group-filter" class="js-filter-dropdown-group filter-group-toogle filter-toogle">Filtrer par</a>
				 </div>
				 <div id="sortBy" class="nowrap results-sort columns small-11 medium-10 medium-push-4 xmedium-hide small-push-3">
						<div class="filter">
							 <a href="#drop-sort-filter" class="js-filter-dropdown-group filter-group-toogle filter-toogle">Trier par</a>
						</div>
				 </div>
				 <div id="drop-sort" class="block-filter filter-panel filter-active filter-group-wrapper" data-dropdown-content="" aria-hidden="true" data-tnr-id="qa-catalog-dynamic-results-filters-sort-by-container">
						<dl class="filter-group-content accordion" data-accordion="filter-accordion">
							 <dd class="js-accordion-navigation accordion-navigation filter-wrapper">
									<a id="sort" href="#drop-sort-filter" class="filter-toogle"><span>Trier par</span></a>
									<div id="drop-sort-filter" class="content filter-panel filter-active" data-dropdown-content="" aria-hidden="true">
										 <div class="filter-content filter-medium-border">
												<ul>
													 <li>
															<span class="js-scramble-link js-ajax-load" data-href="||b||byredo||tri||prix-croissant" data-target="_self" data-ga="gaEvent|gaEvent_action:Choix du tri|gaEvent_category:Page marque|gaEvent_label:Prix croissant">
															Prix croissant
															</span>
													 </li>
													 <li>
															<span class="js-scramble-link js-ajax-load" data-href="||b||byredo||tri||prix-decroissant" data-target="_self" data-ga="gaEvent|gaEvent_action:Choix du tri|gaEvent_category:Page marque|gaEvent_label:Prix decroissant">
															Prix decroissant
															</span>
													 </li>
													 <li>
															<span class="js-scramble-link js-ajax-load" data-href="||b||byredo||tri||meilleures-ventes" data-target="_self" data-ga="gaEvent|gaEvent_action:Choix du tri|gaEvent_category:Page marque|gaEvent_label:Meilleures ventes">
															Meilleures ventes
															</span>
													 </li>
													 <li>
															<span class="js-scramble-link js-ajax-load" data-href="||b||byredo||tri||nouveautes" data-target="_self" data-ga="gaEvent|gaEvent_action:Choix du tri|gaEvent_category:Page marque|gaEvent_label:Nouveautes">
															Nouveautes
															</span>
													 </li>
													 <li>
															<span class="js-scramble-link js-ajax-load" data-href="||b||byredo||tri||reduction" data-target="_self" data-ga="gaEvent|gaEvent_action:Choix du tri|gaEvent_category:Page marque|gaEvent_label:Reduction Decroissante">
															Reduction Decroissante
															</span>
													 </li>
												</ul>
										 </div>
									</div>
							 </dd>
						</dl>
				 </div>
				 <div id="active-facets" class="js-active-facets block-filter filter-group-wrapper columns small-offset-3 small-21" data-tnr-id="qa-catalog-dynamic-results-filters-actives-container"></div>
				 <div id="available-facets" class="js-sticky-nav push-top columns small-offset-3 small-21">
						<div id="filtrer-par" class="filter-group-wrapper">
							 <div id="drop-group-filter" class="filter-group-toogle filter-group js-fgrp" data-tnr-id="qa-catalog-dynamic-results-filters-actives-container">
									<dl class="filter-group-content js-fgrp-pnl accordion" data-accordion="filter-accordion">
										 <dd id="filter-wrapper-facet-custom" class="columns small-24 js-accordion-navigation accordion-navigation filter-wrapper active" data-tnr-id="qa-catalog-dynamic-results-filters-ligne-produit-container">
												<a href="#drop-ligne-produit" class="filter-toogle" aria-expanded="true">Ligne Produit</a>
												<div id="drop-ligne-produit" class="content filter-panel active content-facet">
													 <div class="filter-content columns small-21">
															<ul>
																 <li>
																		<span class="js-scramble-link  js-ajax-load" data-href="||b||byredo||flp||collection+maison" data-target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Ligne Produit|gaEvent_category:Page marque|gaEvent_label:Collection maison">
																		Collection maison
																		</span>
																 </li>
																 <li>
																		<span class="js-scramble-link  js-ajax-load" data-href="||b||byredo||flp||fragrance" data-target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Ligne Produit|gaEvent_category:Page marque|gaEvent_label:Fragrance">
																		Fragrance
																		</span>
																 </li>
																 <li>
																		<span class="js-scramble-link  js-ajax-load" data-href="||b||byredo||flp||soin+pour+le+corps" data-target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Ligne Produit|gaEvent_category:Page marque|gaEvent_label:Soin pour le corps">
																		Soin pour le corps
																		</span>
																 </li>
																 <li>
																		<span class="js-scramble-link  js-ajax-load" data-href="||b||byredo||flp||soin+pour+les+mains" data-target="_self" data-ga="gaEvent|gaEvent_action:Filtre disponible - Ligne Produit|gaEvent_category:Page marque|gaEvent_label:Soin pour les mains">
																		Soin pour les mains
																		</span>
																 </li>
															</ul>
													 </div>
												</div>
										 </dd>
										 <dd id="filter-wrapper-facet-custom" class="columns small-24 js-accordion-navigation accordion-navigation filter-wrapper active" data-tnr-id="qa-catalog-dynamic-results-filters-prix-container">
												<a href="#drop-prix" class="filter-toogle" aria-expanded="true">Prix</a>
												<div id="drop-prix" class="content filter-panel active content-facet">
													 <div class="filter-content columns small-21">
															<div class="slider-box slider-price-box">
																 <input type="text" id="price-range-min" class="price-min js-ajax-load-on-submit" />
																 <input type="text" id="price-range-max" class="price-max js-ajax-load-on-submit" />
																 <div id="price-range" class="slider js-ajax-load-on-submit" data-ga-slider="" data-base-slider-ga="gaEvent|gaEvent_action:Filtre disponible - TranchePrix|gaEvent_category:Page marque|gaEvent_label:" data-template-url="/b/byredo/prix/${min}-${max}" data-price-min="30" data-price-max="245" data-price-start="30" data-price-end="245"></div>
															</div>
													 </div>
												</div>
										 </dd>
									</dl>
									<a href="#" class="filter-toogle filter-group-toogle" data-dropdown="drop-sort" aria-controls="drop-sort" aria-expanded="false">
									<span class="show-for-small-only">Trier par</span>
									<span class="show-for-medium-up">Trier par</span>
									</a>
							 </div>
						</div>
				 </div>
			</div>
			<div class="products-list columns xmedium-24 large-24">
				 <ul class="product-grid js-product-grid small-block-grid-1 medium-block-grid-2 xmedium-block-grid-3 large-block-grid-4   full-grid " data-equalizer="" data-js="product-grid" data-total-records="91">
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-32224978">
							 <a class="anchor" id="32224978-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32224978-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="0">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Blanche Eau de Parfum" data-src="https://static.galerieslafayette.com/media/322/32224978/G_32224978_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/322/32224978/G_32224978_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/blanche+eau+de+parfum-byredo/32224978/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/blanche+eau+de+parfum-byredo/32224978/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32224978-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="0">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Blanche Eau de Parfum</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||blanche+eau+de+parfum-byredo||32224978||378" data-target="_self">
													 <div class="price">
															187,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/blanche+eau+de+parfum-byredo/32224978/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32224978-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="0">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Blanche Eau de Parfum</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||blanche+eau+de+parfum-byredo||32224978||378" data-target="_self">
															<div class="price">
																 187,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/322/32224978/G_32224978_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/32224978/378" data-pdt-url="/p/blanche+eau+de+parfum-byredo/32224978/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Blanche Eau de Parfum" data-lazy="https://static.galerieslafayette.com/media/322/32224978/G_32224978_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-32225167">
							 <a class="anchor" id="32225167-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225167-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="1">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="1996 Eau de Parfum" data-src="https://static.galerieslafayette.com/media/322/32225167/G_32225167_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/322/32225167/G_32225167_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/1996+eau+de+parfum-byredo/32225167/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/1996+eau+de+parfum-byredo/32225167/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225167-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="1">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">1996 Eau de Parfum</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||1996+eau+de+parfum-byredo||32225167||378" data-target="_self">
													 <div class="starting-from">à partir de</div>
													 <div class="price">
															137,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/1996+eau+de+parfum-byredo/32225167/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225167-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="1">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">1996 Eau de Parfum</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||1996+eau+de+parfum-byredo||32225167||378" data-target="_self">
															<div class="starting-from">à partir de</div>
															<div class="price">
																 137,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/322/32225167/G_32225167_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/32225167/378" data-pdt-url="/p/1996+eau+de+parfum-byredo/32225167/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="1996 Eau de Parfum" data-lazy="https://static.galerieslafayette.com/media/322/32225167/G_32225167_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-32225058">
							 <a class="anchor" id="32225058-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225058-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="2">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Bal D&#39;afrique Eau de Parfum" data-src="https://static.galerieslafayette.com/media/322/32225058/G_32225058_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/322/32225058/G_32225058_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/bal+d+afrique+eau+de+parfum-byredo/32225058/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/bal+d+afrique+eau+de+parfum-byredo/32225058/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225058-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="2">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Bal D'afrique Eau de Parfum</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||bal+d+afrique+eau+de+parfum-byredo||32225058||378" data-target="_self">
													 <div class="starting-from">à partir de</div>
													 <div class="price">
															127,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/bal+d+afrique+eau+de+parfum-byredo/32225058/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225058-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="2">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Bal D'afrique Eau de Parfum</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||bal+d+afrique+eau+de+parfum-byredo||32225058||378" data-target="_self">
															<div class="starting-from">à partir de</div>
															<div class="price">
																 127,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/322/32225058/G_32225058_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/32225058/378" data-pdt-url="/p/bal+d+afrique+eau+de+parfum-byredo/32225058/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Bal D&#39;afrique Eau de Parfum" data-lazy="https://static.galerieslafayette.com/media/322/32225058/G_32225058_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>            
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-59160066">
							 <a class="anchor" id="59160066-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;59160066-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="4">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Rose of No Man&#39;s Land Crème mains" data-src="https://static.galerieslafayette.com/media/591/59160066/G_59160066_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/591/59160066/G_59160066_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/rose+of+no+man+s+land+creme+mains-byredo/59160066/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/rose+of+no+man+s+land+creme+mains-byredo/59160066/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;59160066-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="4">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Rose of No Man's Land Crème mains</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Soin pour les mains</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||rose+of+no+man+s+land+creme+mains-byredo||59160066||378" data-target="_self">
													 <div class="price">
															33,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/rose+of+no+man+s+land+creme+mains-byredo/59160066/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;59160066-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="4">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Rose of No Man's Land Crème mains</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Soin pour les mains</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||rose+of+no+man+s+land+creme+mains-byredo||59160066||378" data-target="_self">
															<div class="price">
																 33,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/591/59160066/G_59160066_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/59160066/378" data-pdt-url="/p/rose+of+no+man+s+land+creme+mains-byredo/59160066/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Rose of No Man&#39;s Land Crème mains" data-lazy="https://static.galerieslafayette.com/media/591/59160066/G_59160066_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-32224957">
							 <a class="anchor" id="32224957-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32224957-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="5">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Gypsy Water Eau de Parfum" data-src="https://static.galerieslafayette.com/media/322/32224957/G_32224957_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/322/32224957/G_32224957_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/gypsy+water+eau+de+parfum-byredo/32224957/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/gypsy+water+eau+de+parfum-byredo/32224957/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32224957-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="5">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Gypsy Water Eau de Parfum</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||gypsy+water+eau+de+parfum-byredo||32224957||378" data-target="_self">
													 <div class="starting-from">à partir de</div>
													 <div class="price">
															127,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/gypsy+water+eau+de+parfum-byredo/32224957/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32224957-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="5">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Gypsy Water Eau de Parfum</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||gypsy+water+eau+de+parfum-byredo||32224957||378" data-target="_self">
															<div class="starting-from">à partir de</div>
															<div class="price">
																 127,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/322/32224957/G_32224957_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/32224957/378" data-pdt-url="/p/gypsy+water+eau+de+parfum-byredo/32224957/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Gypsy Water Eau de Parfum" data-lazy="https://static.galerieslafayette.com/media/322/32224957/G_32224957_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>            
						
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-32225051">
							 <a class="anchor" id="32225051-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225051-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="27">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Blanche Parfum pour cheveux" data-src="https://static.galerieslafayette.com/media/322/32225051/G_32225051_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/322/32225051/G_32225051_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/blanche+parfum+pour+cheveux-byredo/32225051/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/blanche+parfum+pour+cheveux-byredo/32225051/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225051-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="27">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Blanche Parfum pour cheveux</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||blanche+parfum+pour+cheveux-byredo||32225051||378" data-target="_self">
													 <div class="price">
															54,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/blanche+parfum+pour+cheveux-byredo/32225051/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225051-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="27">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Blanche Parfum pour cheveux</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||blanche+parfum+pour+cheveux-byredo||32225051||378" data-target="_self">
															<div class="price">
																 54,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/322/32225051/G_32225051_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/32225051/378" data-pdt-url="/p/blanche+parfum+pour+cheveux-byredo/32225051/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Blanche Parfum pour cheveux" data-lazy="https://static.galerieslafayette.com/media/322/32225051/G_32225051_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-59160068">
							 <a class="anchor" id="59160068-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;59160068-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="28">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Rose of No Man&#39;s Land Parfum pour cheveux" data-src="https://static.galerieslafayette.com/media/591/59160068/G_59160068_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/591/59160068/G_59160068_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/rose+of+no+man+s+land+parfum+pour+cheveux-byredo/59160068/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/rose+of+no+man+s+land+parfum+pour+cheveux-byredo/59160068/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;59160068-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="28">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Rose of No Man's Land Parfum pour cheveux</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||rose+of+no+man+s+land+parfum+pour+cheveux-byredo||59160068||378" data-target="_self">
													 <div class="price">
															54,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/rose+of+no+man+s+land+parfum+pour+cheveux-byredo/59160068/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;59160068-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="28">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Rose of No Man's Land Parfum pour cheveux</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||rose+of+no+man+s+land+parfum+pour+cheveux-byredo||59160068||378" data-target="_self">
															<div class="price">
																 54,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/591/59160068/G_59160068_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/59160068/378" data-pdt-url="/p/rose+of+no+man+s+land+parfum+pour+cheveux-byredo/59160068/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Rose of No Man&#39;s Land Parfum pour cheveux" data-lazy="https://static.galerieslafayette.com/media/591/59160068/G_59160068_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-48550169">
							 <a class="anchor" id="48550169-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;48550169-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="29">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Rose Savon liquide pour les mains" data-src="https://static.galerieslafayette.com/media/485/48550169/G_48550169_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/485/48550169/G_48550169_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/rose+savon+liquide+pour+les+mains-byredo/48550169/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/rose+savon+liquide+pour+les+mains-byredo/48550169/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;48550169-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="29">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Rose Savon liquide pour les mains</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Soin pour les mains</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||rose+savon+liquide+pour+les+mains-byredo||48550169||378" data-target="_self">
													 <div class="price">
															38,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/rose+savon+liquide+pour+les+mains-byredo/48550169/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;48550169-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="29">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Rose Savon liquide pour les mains</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Soin pour les mains</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||rose+savon+liquide+pour+les+mains-byredo||48550169||378" data-target="_self">
															<div class="price">
																 38,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/485/48550169/G_48550169_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/48550169/378" data-pdt-url="/p/rose+savon+liquide+pour+les+mains-byredo/48550169/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Rose Savon liquide pour les mains" data-lazy="https://static.galerieslafayette.com/media/485/48550169/G_48550169_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-32224898">
							 <a class="anchor" id="32224898-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32224898-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="30">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Inflorescence Eau de Parfum" data-src="https://static.galerieslafayette.com/media/322/32224898/G_32224898_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/322/32224898/G_32224898_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/inflorescence+eau+de+parfum-byredo/32224898/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/inflorescence+eau+de+parfum-byredo/32224898/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32224898-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="30">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Inflorescence Eau de Parfum</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||inflorescence+eau+de+parfum-byredo||32224898||378" data-target="_self">
													 <div class="starting-from">à partir de</div>
													 <div class="price">
															127,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/inflorescence+eau+de+parfum-byredo/32224898/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32224898-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="30">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Inflorescence Eau de Parfum</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||inflorescence+eau+de+parfum-byredo||32224898||378" data-target="_self">
															<div class="starting-from">à partir de</div>
															<div class="price">
																 127,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/322/32224898/G_32224898_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/32224898/378" data-pdt-url="/p/inflorescence+eau+de+parfum-byredo/32224898/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Inflorescence Eau de Parfum" data-lazy="https://static.galerieslafayette.com/media/322/32224898/G_32224898_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-65359753">
							 <a class="anchor" id="65359753-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;65359753-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="31">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Sellier Extrait de Parfum" data-src="https://static.galerieslafayette.com/media/653/65359753/G_65359753_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/653/65359753/G_65359753_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/sellier+extrait+de+parfum-byredo/65359753/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/sellier+extrait+de+parfum-byredo/65359753/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;65359753-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="31">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Sellier Extrait de Parfum</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||sellier+extrait+de+parfum-byredo||65359753||378" data-target="_self">
													 <div class="price">
															245,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/sellier+extrait+de+parfum-byredo/65359753/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;65359753-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="31">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Sellier Extrait de Parfum</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||sellier+extrait+de+parfum-byredo||65359753||378" data-target="_self">
															<div class="price">
																 245,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/653/65359753/G_65359753_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/65359753/378" data-pdt-url="/p/sellier+extrait+de+parfum-byredo/65359753/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Sellier Extrait de Parfum" data-lazy="https://static.galerieslafayette.com/media/653/65359753/G_65359753_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-32224866">
							 <a class="anchor" id="32224866-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32224866-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="32">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Oud Immortel Eau de Parfum" data-src="https://static.galerieslafayette.com/media/322/32224866/G_32224866_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/322/32224866/G_32224866_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/oud+immortel+eau+de+parfum-byredo/32224866/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/oud+immortel+eau+de+parfum-byredo/32224866/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32224866-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="32">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Oud Immortel Eau de Parfum</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||oud+immortel+eau+de+parfum-byredo||32224866||378" data-target="_self">
													 <div class="starting-from">à partir de</div>
													 <div class="price">
															127,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/oud+immortel+eau+de+parfum-byredo/32224866/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32224866-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="32">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Oud Immortel Eau de Parfum</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||oud+immortel+eau+de+parfum-byredo||32224866||378" data-target="_self">
															<div class="starting-from">à partir de</div>
															<div class="price">
																 127,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/322/32224866/G_32224866_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/32224866/378" data-pdt-url="/p/oud+immortel+eau+de+parfum-byredo/32224866/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Oud Immortel Eau de Parfum" data-lazy="https://static.galerieslafayette.com/media/322/32224866/G_32224866_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-50842870">
							 <a class="anchor" id="50842870-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;50842870-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="33">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Woods Bougie Parfumée" data-src="https://static.galerieslafayette.com/media/508/50842870/G_50842870_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/508/50842870/G_50842870_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/woods+bougie+parfumee-byredo/50842870/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/woods+bougie+parfumee-byredo/50842870/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;50842870-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="33">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Woods Bougie Parfumée</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Collection maison</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||woods+bougie+parfumee-byredo||50842870||378" data-target="_self">
													 <div class="price">
															62,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/woods+bougie+parfumee-byredo/50842870/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;50842870-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="33">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Woods Bougie Parfumée</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Collection maison</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||woods+bougie+parfumee-byredo||50842870||378" data-target="_self">
															<div class="price">
																 62,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/508/50842870/G_50842870_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/50842870/378" data-pdt-url="/p/woods+bougie+parfumee-byredo/50842870/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Woods Bougie Parfumée" data-lazy="https://static.galerieslafayette.com/media/508/50842870/G_50842870_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-47169221">
							 <a class="anchor" id="47169221-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;47169221-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="34">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Velvet Haze Eau de Parfum" data-src="https://static.galerieslafayette.com/media/471/47169221/G_47169221_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/471/47169221/G_47169221_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/velvet+haze+eau+de+parfum-byredo/47169221/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/velvet+haze+eau+de+parfum-byredo/47169221/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;47169221-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="34">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Velvet Haze Eau de Parfum</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||velvet+haze+eau+de+parfum-byredo||47169221||378" data-target="_self">
													 <div class="starting-from">à partir de</div>
													 <div class="price">
															127,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/velvet+haze+eau+de+parfum-byredo/47169221/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;47169221-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="34">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Velvet Haze Eau de Parfum</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||velvet+haze+eau+de+parfum-byredo||47169221||378" data-target="_self">
															<div class="starting-from">à partir de</div>
															<div class="price">
																 127,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/471/47169221/G_47169221_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/47169221/378" data-pdt-url="/p/velvet+haze+eau+de+parfum-byredo/47169221/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Velvet Haze Eau de Parfum" data-lazy="https://static.galerieslafayette.com/media/471/47169221/G_47169221_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-35616106">
							 <a class="anchor" id="35616106-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;35616106-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="35">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Gypsy Water Crème mains" data-src="https://static.galerieslafayette.com/media/356/35616106/G_35616106_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/356/35616106/G_35616106_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/gypsy+water+creme+mains-byredo/35616106/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/gypsy+water+creme+mains-byredo/35616106/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;35616106-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="35">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Gypsy Water Crème mains</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Soin pour les mains</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||gypsy+water+creme+mains-byredo||35616106||378" data-target="_self">
													 <div class="price">
															33,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/gypsy+water+creme+mains-byredo/35616106/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;35616106-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="35">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Gypsy Water Crème mains</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Soin pour les mains</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||gypsy+water+creme+mains-byredo||35616106||378" data-target="_self">
															<div class="price">
																 33,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/356/35616106/G_35616106_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/35616106/378" data-pdt-url="/p/gypsy+water+creme+mains-byredo/35616106/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Gypsy Water Crème mains" data-lazy="https://static.galerieslafayette.com/media/356/35616106/G_35616106_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-32225082">
							 <a class="anchor" id="32225082-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225082-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="36">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Blanche Roll-on huile parfumée" data-src="https://static.galerieslafayette.com/media/322/32225082/G_32225082_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/322/32225082/G_32225082_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/blanche+roll-on+huile+parfumee-byredo/32225082/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/blanche+roll-on+huile+parfumee-byredo/32225082/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225082-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="36">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Blanche Roll-on huile parfumée</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||blanche+roll-on+huile+parfumee-byredo||32225082||378" data-target="_self">
													 <div class="price">
															50,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/blanche+roll-on+huile+parfumee-byredo/32225082/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225082-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="36">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Blanche Roll-on huile parfumée</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||blanche+roll-on+huile+parfumee-byredo||32225082||378" data-target="_self">
															<div class="price">
																 50,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/322/32225082/G_32225082_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/32225082/378" data-pdt-url="/p/blanche+roll-on+huile+parfumee-byredo/32225082/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Blanche Roll-on huile parfumée" data-lazy="https://static.galerieslafayette.com/media/322/32225082/G_32225082_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-32225107">
							 <a class="anchor" id="32225107-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225107-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="37">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Blanche Crème mains" data-src="https://static.galerieslafayette.com/media/322/32225107/G_32225107_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/322/32225107/G_32225107_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/blanche+creme+mains-byredo/32225107/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/blanche+creme+mains-byredo/32225107/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225107-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="37">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Blanche Crème mains</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Soin pour les mains</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||blanche+creme+mains-byredo||32225107||378" data-target="_self">
													 <div class="price">
															33,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/blanche+creme+mains-byredo/32225107/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225107-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="37">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Blanche Crème mains</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Soin pour les mains</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||blanche+creme+mains-byredo||32225107||378" data-target="_self">
															<div class="price">
																 33,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/322/32225107/G_32225107_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/32225107/378" data-pdt-url="/p/blanche+creme+mains-byredo/32225107/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Blanche Crème mains" data-lazy="https://static.galerieslafayette.com/media/322/32225107/G_32225107_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>   
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-32224911">
							 <a class="anchor" id="32224911-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32224911-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="56">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Bal d&#39;Afrique Parfum pour cheveux" data-src="https://static.galerieslafayette.com/media/322/32224911/G_32224911_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/322/32224911/G_32224911_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/bal+d+afrique+parfum+pour+cheveux-byredo/32224911/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/bal+d+afrique+parfum+pour+cheveux-byredo/32224911/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32224911-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="56">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Bal d'Afrique Parfum pour cheveux</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||bal+d+afrique+parfum+pour+cheveux-byredo||32224911||378" data-target="_self">
													 <div class="price">
															54,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/bal+d+afrique+parfum+pour+cheveux-byredo/32224911/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32224911-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="56">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Bal d'Afrique Parfum pour cheveux</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||bal+d+afrique+parfum+pour+cheveux-byredo||32224911||378" data-target="_self">
															<div class="price">
																 54,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/322/32224911/G_32224911_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/32224911/378" data-pdt-url="/p/bal+d+afrique+parfum+pour+cheveux-byredo/32224911/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Bal d&#39;Afrique Parfum pour cheveux" data-lazy="https://static.galerieslafayette.com/media/322/32224911/G_32224911_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-32225063">
							 <a class="anchor" id="32225063-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225063-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="57">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Bal d&#39;Afrique Crème Corps" data-src="https://static.galerieslafayette.com/media/322/32225063/G_32225063_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/322/32225063/G_32225063_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/bal+d+afrique+creme+corps-byredo/32225063/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/bal+d+afrique+creme+corps-byredo/32225063/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225063-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="57">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Bal d'Afrique Crème Corps</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Soin pour le corps</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||bal+d+afrique+creme+corps-byredo||32225063||378" data-target="_self">
													 <div class="price">
															62,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/bal+d+afrique+creme+corps-byredo/32225063/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;32225063-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="57">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Bal d'Afrique Crème Corps</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Soin pour le corps</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||bal+d+afrique+creme+corps-byredo||32225063||378" data-target="_self">
															<div class="price">
																 62,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/322/32225063/G_32225063_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/32225063/378" data-pdt-url="/p/bal+d+afrique+creme+corps-byredo/32225063/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Bal d&#39;Afrique Crème Corps" data-lazy="https://static.galerieslafayette.com/media/322/32225063/G_32225063_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-35616055">
							 <a class="anchor" id="35616055-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;35616055-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="58">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="La Tulipe Parfum pour cheveux" data-src="https://static.galerieslafayette.com/media/356/35616055/G_35616055_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/356/35616055/G_35616055_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/la+tulipe+parfum+pour+cheveux-byredo/35616055/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/la+tulipe+parfum+pour+cheveux-byredo/35616055/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;35616055-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="58">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">La Tulipe Parfum pour cheveux</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||la+tulipe+parfum+pour+cheveux-byredo||35616055||378" data-target="_self">
													 <div class="price">
															54,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/la+tulipe+parfum+pour+cheveux-byredo/35616055/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;35616055-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="58">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">La Tulipe Parfum pour cheveux</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Fragrance</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||la+tulipe+parfum+pour+cheveux-byredo||35616055||378" data-target="_self">
															<div class="price">
																 54,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/356/35616055/G_35616055_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/35616055/378" data-pdt-url="/p/la+tulipe+parfum+pour+cheveux-byredo/35616055/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="La Tulipe Parfum pour cheveux" data-lazy="https://static.galerieslafayette.com/media/356/35616055/G_35616055_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>
						<li class="pdt-cell pdt-cell-with-hover js-pdt-cell" id="pdt-cell-57776063">
							 <a class="anchor" id="57776063-378"></a>
							 <div class="visual opaque-visual js-recommendation-event" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;57776063-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="59">
									<div class="slideshow-container">
										 <div class="mySlides fade">
												<img class="" style="width:100%" src="/img/category/loader_img_frise.gif" alt="Rose Savon" data-src="https://static.galerieslafayette.com/media/577/57776063/G_57776063_378_VPP_1.jpg" data-lazy="https://static.galerieslafayette.com/media/577/57776063/G_57776063_378_VPP_1.jpg" />
										 </div>
									</div>
									<a href="/p/rose+savon-byredo/57776063/378" class="js-pdt-link" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-internet-flag="false">
										 <div class="opaque"></div>
										 <span class="back-flag grey picto-new picto-hide">New</span>
									</a>
							 </div>
							 <div class="content pdt-hover-hide">
									<div class="pdt-details columns large-18 small-15">
										 <a href="/p/rose+savon-byredo/57776063/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;57776063-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="59">
												<div class="pdt-brand-name reverseOrder">
													 <span class="pdt-name three pdt-hover-hide">Rose Savon</span>
													 <strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Soin pour les mains</strong>
													 <strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
												</div>
										 </a>
									</div>
									<div class="columns large-6 small-9">
										 <div class="pdt-price">
												<span class="js-scramble-link" data-href="||p||rose+savon-byredo||57776063||378" data-target="_self">
													 <div class="price">
															30,00&#160;&#8364;
													 </div>
												</span>
										 </div>
									</div>
							 </div>
							 <div class="pdt-color pdt-hover-content columns large-24">
									<div class="content">
										 <div class="pdt-details columns large-18 small-15">
												<a href="/p/rose+savon-byredo/57776063/378" class="js-pdt-link js-recommendation-event" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Accéder à la fiche produit" data-product="{&quot;ru&quot;:null,&quot;iID&quot;:&quot;57776063-378&quot;,&quot;sp&quot;:&quot;null-null-null&quot;}" data-index="59">
													 <div class="pdt-brand-name reverseOrder">
															<span class="pdt-name three pdt-hover-hide">Rose Savon</span>
															<strong class="bold-large-title-marque ligne-produit two pdt-hover-hide ligne-produit-byredo">Soin pour les mains</strong>
															<strong class="pdt-brand bold-large-title-marque one">Byredo</strong>
													 </div>
												</a>
										 </div>
										 <div class="columns large-6 small-9">
												<div class="pdt-price">
													 <span class="js-scramble-link" data-href="||p||rose+savon-byredo||57776063||378" data-target="_self">
															<div class="price">
																 30,00&#160;&#8364;
															</div>
													 </span>
												</div>
										 </div>
									</div>
									<div class="slideshow-container-color columns large-24">
										 <div class="mySlides fade" data-thumb="thumb-1" data-visual-main="https://static.galerieslafayette.com/media/577/57776063/G_57776063_378_VPP_1.jpg" data-quick-shop-url="/quickshop/load/57776063/378" data-pdt-url="/p/rose+savon-byredo/57776063/378" data-ga="gaEvent|gaEvent_action:Produit|gaEvent_category:Page marque|gaEvent_label:Visualisation de miniatures" data-novelty-flag="false">
												<img class="visual-thumb" src="/img/category/loader_img_frise.gif" alt="Rose Savon" data-lazy="https://static.galerieslafayette.com/media/577/57776063/G_57776063_378_VPMIN_1.jpg" />
										 </div>
									</div>
							 </div>
						</li>
				 </ul>
			</div>
	 </div>
	 <div class="row" data-js="see-pagination">
			<div class="columns small-22 small-centered pagination-centered">
				 <ul class="pagination" data-tnr-id="qa-catalog-dynamic-pagination-container">
						<li class="current" data-tnr-id="qa-catalog-dynamic-pagination-current">
							 1
						</li>
						<li data-tnr-id="qa-catalog-dynamic-pagination-no-current">
							 <a href="/b/byredo/p:2" target="_self" class="js-ajax-load">
							 2
							 </a>
						</li>
						<li class="arrow" data-tnr-id="qa-catalog-dynamic-pagination-next">
							 <a href="/b/byredo/p:2" target="_self" class="js-ajax-load">
							 <span class="show-for-small-only">&raquo;</span><span class="show-for-medium-up">Suivant</span>
							 </a>
						</li>
				 </ul>
			</div>
	 </div>
</div>
</th:block>

<?php include ('../pages-defaults/footer.php'); ?>