<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Crinière de rêve";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

    
<!-- =========================== LANDING PAGE ========================== -->
 <link href="https://static.galerieslafayette.com/media/LP/src/css/2022/multimarque-capillaire.css" rel="stylesheet" type="text/css">
 <style type="text/css">
   .header__product-list,
   .footer-description,
   .ab684263 {
     display: none;
   }
   .reveal-modal {
     z-index: 7005;
     border: 8px solid #000;
   }
   .reveal-modal-bg {
     z-index: 7000;
   }
   @media screen and (min-width: 768px) {
     .medium-offset-2 {
       margin-left: 0 !important;
     }
   }
   @media screen and (min-width: 1250px) {
     .landing-levis {
       border-bottom: 1px solid #000;
       margin: 0 auto 50px;
     }
   }
 </style>
 <div class="multimarque-capillaire">
   <div class="hero-content">
     <div class="show-for-small"><img alt="Le guide beauté" class="reveal-4" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/hero-m.jpg" /></div>
     <div class="hide-for-small"><img alt="Le guide beauté" class="reveal-4" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/hero-d.jpg" /></div>
     <span class="hero-title">
       <img alt="Mission: crinière rêve" class="show-for-small reveal-5" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/titre-m.png" />
       <img alt="Mission: crinière rêve" class="hide-for-small reveal-5" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/titre-d.png" />
     </span>
     <h1>Mission: crinière rêve</h1>
     <p class="extremity">L’été, entre le sel de la mer et le soleil, nos cheveux sont mis à rude épreuve. Pour afficher une chevelure brillante et renforcée, voici les meilleurs soins sur lesquels miser, bien au-delà des vacances.</p>
   </div>
   <div class="bloc-trend">
     <div class="bloc-product">
       <div>
         <div class="description">
           <h3><img alt="Pour protéger" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/cartouches/01.png" /></h3>
           <p>Un flacon à secouer pour libérer les actifs qui vont protéger, faire briller et réparer.</p>
         </div>
         <a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/77461480/378" target="_self">
           <span><img alt="VIRTUE" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/prod/01.jpg" /> </span> <span> <strong>VIRTUE</strong> Huile Réparatrice </span>
         </a>
       </div>
       <div>
         <div class="description">
           <h3><img alt="Pour la douche" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/cartouches/02.png" /></h3>
           <p>Un shampoing idéal pour <br class="hide-for-small" />un démêlage optimal</p>
         </div>
         <a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/83899489/378" target="_self">
           <span><img alt="ORIBE" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/prod/02b.jpg" /> </span>
           <span> <strong>ORIBE</strong>Run-Through&nbsp;Detangling Shampoo</span>
         </a>
       </div>
       <div>
         <div class="description">
           <h3><img alt="Pour la nuit" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/cartouches/03.png" /></h3>
           <p>Un sérum de nuit vegan à base de plantes pour des cheveux réparés en une seule nuit.</p>
         </div>
         <a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/84211444/378" target="_self">
           <span><img alt="AVEDA" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/prod/03.jpg" /> </span>
           <span> <strong>AVEDA</strong> Botanical Repair™ Strengthening Overnight Serum -Sérum réparateur de nuit </span>
         </a>
       </div>
       <div>
         <div class="description">
           <h3><img alt="Pour voyager" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/cartouches/04.png" /></h3>
           <p>Un pack de trois essentiels au parfum 100% biologique de Grasse à glisser dans sa valise.</p>
         </div>
         <a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/85525165/378" target="_self">
           <span><img alt="D LAB" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/prod/04.jpg" /> </span>
           <span> <strong>BEAUTY DISRUPTED</strong> L'Essentiel de douche Ocean Magic pour Cheveux Normaux </span>
         </a>
       </div>
       <div>
         <div class="description">
           <h3><img alt="Pour renforcer" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/cartouches/05.png" /></h3>
           <p>Un traitement hebdomadaire qui réduit la casse et renforce les cheveux.</p>
         </div>
         <a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/80335822/378" target="_self">
           <span><img alt="OLAPLEX" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/prod/05.jpg" /> </span> <span> <strong>OLAPLEX</strong> No. 3 Hair Perfector </span>
         </a>
       </div>
       <div>
         <div class="description">
           <h3><img alt="Pour discpliner" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/cartouches/06.png" /></h3>
           <p>Une huile réparatrice qui dompte les frisottis, disponible en exclusivité aux Galeries Lafayette.</p>
         </div>
         <a class="js-pdt-quick-shop-link" href="https://www.galerieslafayette.com/quickshop/load/83145983/378" target="_self">
           <span><img alt="ON THE WILD SIDE" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/prod/06.jpg" /> </span>
           <span> <strong>ON THE WILD SIDE</strong> Huile de soin cheveux </span>
         </a>
       </div>
     </div>
     <!--            <a href="/t/plus-size" class="button white">VOIR toute la sélection</a>        -->
   </div>
   <div class="all-univers reveal">
     <a class="reveal-5" href="/c/beaute/soin/soins-cheveux">
       <img alt="Tous les soins cheveux" class="show-for-small" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/cta-bas-m.png" />
       <img alt="Tous les compléments alimentaires" class="hide-for-small" src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/edito/multimarque-capillaire/cta-bas-d.png" />
     </a>
   </div>
 </div>

  <!--=========================== FIN LANDING PAGE ========================-->
  
<!-- build:js /media/LP/src/js/2021/prada-collection-ss21.min.v01.js 
  <script src="../../assets/js/blazy.min.js"></script>
  <script src="../src/js/2021/prada-collection-ss21.js"></script>
<!-- endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
