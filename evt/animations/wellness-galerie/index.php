<?php include ('../../pages-defaults/header.php'); ?>
<script>
  document.title = "Wellness";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../../media/LP/src/css/2022/wellness-galerie.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2022/wellness-galerie.min.v10c.css" rel="stylesheet" type="text/css" />
<div class="lp-container">
  <section class="section lp-hero has-video">
    <div class="container is-fullheight">
      <div class="luxe-row no-gutter is-fullheight luxe-middle-mobile">
        <div class="hero-bg-image is-hidden-tablet" style="background-image:url('https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202210/hero_M@2x.jpg'); background-position:center center;">&nbsp;</div>
        <div class="hero-bg-image is-hidden-mobile" style="background-image:url('https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202210/hero_D@2x.jpg'); background-position:center center;">&nbsp;</div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body hero-txt">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-tablet">
        <div class="luxe-col-mobile-12 luxe-center-mobile">
          <div class="content">
            <h1 class="boxed-text">
              <span class="is-hidden-mobile">&nbsp;</span>
              <span>La wellness galerie <span class="is-hidden-mobile"> &nbsp;</span></span>
            </h1>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-9">
          <div class="content">
            <p class="is-bold">Le bien-être nouvelle génération</p>
            <p>Loin du tumulte de la ville, la Wellness Galerie est un écrin de 3000m<sup>2</sup> qui invite à&nbsp;la&nbsp;déconnexion au sein du magasin Galeries Lafayette Paris Haussmann.</p>
            <p>L'espace réunit sous le même toit une sélection des meilleurs experts du bien-être, de la beauté, du soin, de l’énergie et du sport. Il invite et accompagne chacun à prendre soin de soi grâce à des services personnalisés. Prenez rendez-vous dès maintenant pour une expérience résolument&nbsp;inédite.</p>
            <a href="https://wellness.galerieslafayette.com?utm_source=glcom&utm_medium=cta" class="button primary">Prendre rendez-vous<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body middle-section">
    <div class="container">
      <div class="luxe-row no-gutter luxe-middle-desktop">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="main-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202210/wellness-inspi-round_D.png"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202210/wellness-inspi-round_D@2x.png 2x"
                alt="Wellness Galerie - Galeries Lafayette">
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <ul>
              <li>
                <h2>Le Sport</h2>
                <p>avec cours collectifs ou individuels et une sélection de pièces sportswear.</p>
              </li>
              <li>
                <h2>Les Soins</h2>
                <p>proposés par des marques iconiques ou plus confidentielles, des experts triés sur le volet pour des parcours et routines à personnaliser.</p>
              </li>
              <li>
                <h2>Le Bien-être</h2>
                <p>alliant une cantine healthy et des rituels, entre tradition et innovation.</p>
              </li>
              <li class="luxe-center-mobile luxe-start-tablet">
                <a href=" https://wellness.galerieslafayette.com/?utm_source=glcom&utm_medium=cta_D%C3%89COUVRIR" class="button outlined">Découvrir</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body rituals">
    <div class="container">
      <div class="luxe-row luxe-center-mobile intro">
        <div class="luxe-col-mobile-12">
          <div class="content">
            <h2 class="boxed-text">
              <span>Les rituels de la</span>
              <span>Wellness Galerie</span>
            </h2>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-9 luxe-col-desktop-8 luxe-col-widescreen-7">
          <div class="content">
            <p>Des parcours proposés à des prix exclusifs adaptés aux besoins de chacun. À&nbsp;offrir aux autres ou à soi, pour une heure ou une après-midi, les Rituels de la Wellness Galerie sont le cadeau idéal pour un moment de bien-être inédit pour toutes et tous.</p>
            <a href="https://wellness.galerieslafayette.com/rituals?utm_source=glcom&utm_medium=cta_D%C3%89COUVRIR+LES+RITUELS" class="button primary">Découvrir les rituels<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
      <div class="slider-rituals slider-flickity ritual-items has-large-txt">
        
        <div class="ritual">
          <a href="https://wellness.galerieslafayette.com/rituals/le-rituel-express-beaute?utm_source=glcom&utm_medium=cta_reserver">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/rituals/express-beaute.png"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/rituals/express-beaute@2x.png 2x"
                alt="Wellness Galerie - Galeries Lafayette">
            </figure>
          </a>
          <div class="ritual-info">
            <p class="ritual-txt">Cette offre découverte vous fait vivre un moment de beauté avec Maison Chill et Boudoir du Regard. Pour être parfaite, du bout des mains au bout des cils !</p>
            <p class="ritual-price"><span class="is-striked">115€</span> <span class="is-bold">95,45€</span></p>
            <a href="https://wellness.galerieslafayette.com/rituals/le-rituel-express-beaute?utm_source=glcom&utm_medium=cta_reserver" class="button outlined">Réserver</a>
          </div>
        </div>
        
        <div class="ritual">
          <a href=" https://wellness.galerieslafayette.com/rituals/le-rituel-express-deconnexion?utm_source=glcom&utm_medium=cta_reserver">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/rituals/express-deconnexion.png"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/rituals/express-deconnexion@2x.png 2x"
                alt="Wellness Galerie - Galeries Lafayette">
            </figure>
          </a>
          <div class="ritual-info">
            <p class="ritual-txt">Pour une pause relaxante, profitez du sauna infrarouge de chez Belleyme et d’un massage signé Maison Chill pour repartir le corps et l’esprit légers.</p>
            <p class="ritual-price"><span class="is-striked">100€</span> <span class="is-bold">95,45€</span></p>
            <a href=" https://wellness.galerieslafayette.com/rituals/le-rituel-express-deconnexion?utm_source=glcom&utm_medium=cta_reserver" class="button outlined">Réserver</a>
          </div>
        </div>
        
        <div class="ritual">
          <a href="https://wellness.galerieslafayette.com/rituals/le-rituel-remise-en-forme?utm_source=glcom&utm_medium=cta_reserver">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/rituals/remise-en-forme.png"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/rituals/remise-en-forme@2x.png 2x"
                alt="Wellness Galerie - Galeries Lafayette">
            </figure>
          </a>
          <div class="ritual-info">
            <p class="ritual-txt">Besoin d’une remise en forme complète ? Mixez cryothérapie et massage sportif chez Anatomik avec une séance de sauna infrarouge chez Belleyme.</p>
            <p class="ritual-price"><span class="is-striked">200€</span> <span class="is-bold">176€</span></p>
            <a href="https://wellness.galerieslafayette.com/rituals/le-rituel-remise-en-forme?utm_source=glcom&utm_medium=cta_reserver" class="button outlined">Réserver</a>
          </div>
        </div>
        
        <div class="ritual">
          <a href="https://wellness.galerieslafayette.com/rituals/le-rituel-beaute-complete?utm_source=glcom&utm_medium=cta_reserver">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/rituals/beaute-complete.png"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/rituals/beaute-complete@2x.png 2x"
                alt="Wellness Galerie - Galeries Lafayette">
            </figure>
          </a>
          <div class="ritual-info">
            <p class="ritual-txt">Craquez pour une mise en beauté pour les fêtes ! Au programme, restructuration des sourcils chez Boudoir du Regard et manucure-pédicure chez Maison Chill.</p>
            <p class="ritual-price"><span class="is-striked">195€</span> <span class="is-bold">175,50€</span></p>
            <a href="https://wellness.galerieslafayette.com/rituals/le-rituel-beaute-complete?utm_source=glcom&utm_medium=cta_reserver" class="button outlined">Réserver</a>
          </div>
        </div>
        
        <div class="ritual">
          <a href="https://wellness.galerieslafayette.com/rituals/le-rituel-holistique?utm_source=glcom&utm_medium=cta_reserver">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/rituals/holistique.png"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/rituals/holistique@2x.png 2x"
                alt="Wellness Galerie - Galeries Lafayette">
            </figure>
          </a>
          <div class="ritual-info">
            <p class="ritual-txt">Allier détente et efficacité ? Misez sur un massage Maison Chill, un soin du visage Skinneo et une séance de drainage lymphatique signée Anatomik.</p>
            <p class="ritual-price"><span class="is-striked">320€</span> <span class="is-bold">275€</span></p>
            <a href="https://wellness.galerieslafayette.com/rituals/le-rituel-holistique?utm_source=glcom&utm_medium=cta_reserver" class="button outlined">Réserver</a>
          </div>
        </div>
        
      </div>
      
    </div>
  </section>
  
  <section class="section lp-body brands">
    <div class="container">
      <div class="luxe-row luxe-center-mobile">
        <div class="luxe-col-mobile-12">
          <div class="content">
            <h2 class="boxed-text">
              <span>Les marques du mois</span>
            </h2>
            <div class="luxe-row brand-items has-large-txt">
              <div class="luxe-col-mobile-6 luxe-col-tablet-3">
                <a href="https://www.galerieslafayette.com/b/dermalogica">
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202211/brand-01.png"
                      srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202211/brand-01@2x.png 2x"
                      alt="Wellness Galerie - Galeries Lafayette">
                  </figure>
                </a>
                <p class="brand-title is-bold">Dermalogica</p>
                <p class="brand-txt">Trois décennies de recherche pour le meilleur des soins visage.</p>
                <a href="https://www.galerieslafayette.com/b/dermalogica">Découvrir</a>
              </div>
              <div class="luxe-col-mobile-6 luxe-col-tablet-3">
                <a href="https://www.galerieslafayette.com/b/furtuna-skin">  
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202211/brand-02.png"
                      srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202211/brand-02@2X.png 2x"
                      alt="Wellness Galerie - Galeries Lafayette">
                  </figure>
                </a>
                <p class="brand-title is-bold">Furtuna Skin</p>
                <p class="brand-txt">La beauté sauvage née du soleil italien.</p>
                <a href="https://www.galerieslafayette.com/b/furtuna-skin">Découvrir</a>
              </div>
              <div class="luxe-col-mobile-6 luxe-col-tablet-3">
                <a href="https://www.galerieslafayette.com/b/susanne+kaufmann">  
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202211/brand-03.png"
                      srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202211/brand-03@2x.png 2x"
                      alt="Wellness Galerie - Galeries Lafayette">
                  </figure>
                </a>
                <p class="brand-title is-bold">Susanne<br /> Kaufmann</p>
                <p class="brand-txt">Le rituel des teints éclatants.</p>
                <a href="https://www.galerieslafayette.com/b/susanne+kaufmann">Découvrir</a>
              </div>
              <div class="luxe-col-mobile-6 luxe-col-tablet-3">
                <a href="https://www.galerieslafayette.com/b/cosmic+dealer">  
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202211/brand-04.png"
                      srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202211/brand-04@2x.png 2x"
                      alt="Wellness Galerie - Galeries Lafayette">
                  </figure>
                </a>
                <p class="brand-title is-bold">Cosmic Dealer</p>
                <p class="brand-txt">L’alchimie du changement pour des plaisirs plus sains.</p>
                <a href="https://www.galerieslafayette.com/b/cosmic+dealer">Découvrir</a>
              </div>
            </div>
            <a href="https://www.galerieslafayette.com/c/wellness+galerie" class="button outlined">Voir toute la sélection</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body magasin">
    <div class="container">
      <div class="luxe-row luxe-center-tablet luxe-middle-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content text-left">
            <h3>Préparez votre visite à<br class="is-hidden-desktop" /> la Wellness Galerie</h3>
            <hr />
            <p>40 Bd. Haussmann, 75009 Paris<br />
              <a class="underlined" href="https://www.google.fr/maps/place/Galeries+Lafayette+Haussmann/@48.8733284,2.3299672,17z/data=!3m1!4b1!4m5!3m4!1s0x47e66e3703a1108b:0xe6773845cdab1593!8m2!3d48.8733284!4d2.3321559" target="_blank">> Voir sur Google Maps</a></p>
            <p>Tel: +33969397575</p>
            <ul class="week">
              <li><span class="day">Lundi</span> 10:00-20:30</li>
              <li><span class="day">Mardi</span> 10:00-20:30</li>
              <li><span class="day">Mercredi</span> 10:00-20:30</li>
              <li><span class="day">Jeudi</span> 10:00-20:30</li>
              <li><span class="day">Vendredi</span> 10:00-20:30</li>
              <li><span class="day">Samedi</span> 10:00-20:30</li>
              <li><span class="day">Dimanche</span> 11:00-20:00</li>
            </ul>
            <hr />
            <a href=" https://wellness.galerieslafayette.com?utm_source=glcom&utm_medium=cta" class="button primary">Prendre rendez-vous<span class="icon has-arrow"></span></a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/coupole-hsm-round.png"
                alt="Galeries Lafayette Haussmann">
            </figure>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-hero has-video">
    <div class="container is-fullheight">
      <div class="luxe-row no-gutter is-fullheight luxe-middle-mobile">
        <iframe class="bg-video video is-hidden-tablet" src="https://player.vimeo.com/video/760231969?background=1" width="767" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>
        <iframe class="bg-video video is-hidden-mobile" src="https://player.vimeo.com/video/760231152?background=1" width="1440" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>
      </div>
    </div>
  </section>

</div>
<script src="https://static.galerieslafayette.com/media/LP/src/js/2022/wellness-galerie.min.v02.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->

  
<!-- build:js /media/LP/src/js/2022/wellness-galerie.min.v02.js
  <script src="../../../assets/js/flickity.pkgd.min.js"></script>
  <script src="../../src/js/2022/wellness-galerie.js"></script>
endbuild -->
  
<?php include ('../../pages-defaults/footer.php'); ?>
