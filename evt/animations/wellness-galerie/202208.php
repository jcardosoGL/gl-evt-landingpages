<?php include ('../../pages-defaults/header.php'); ?>
<script>
  document.title = "Wellness";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

  <!-- <link href="../../../media/LP/src/css/2022/wellness-galerie.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2022/wellness-galerie.min.v07.css" rel="stylesheet" type="text/css" />
<div class="lp-container">
  <section class="section lp-hero">
    <div class="container is-fullheight">
      <div class="luxe-row no-gutter is-fullheight luxe-middle-mobile">
        <div class="hero-bg-image is-hidden-tablet" style="background-image:url('https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/hero_M.jpg'); background-position:center center;">&nbsp;</div>
        <div class="hero-bg-image is-hidden-mobile" style="background-image:url('https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/hero_D.jpg'); background-position:center center;">&nbsp;</div>
        <div class="luxe-col-mobile-12">  
          <div class="title"><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/wellness-galerie-logo-d.svg" alt="La Wellness Galerie" /></div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body hero-txt">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-tablet">
        <div class="luxe-col-mobile-12">
          <div class="content">
            <h1 class="boxed-text">
              <span>Le bien-être </span><br class="is-hidden-tablet"/>
              <span>nouvelle génération</span>
            </h1>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-9">
          <div class="content">
            <p>Loin du tumulte de la ville, la Wellness Galerie est un écrin de 3000m<sup>2</sup> qui invite à&nbsp;la&nbsp;déconnexion.</p>
            <p>L'espace réunit sous le même toit une sélection des meilleurs experts du bien-être, de la beauté, du soin, de l’énergie et du sport. Il invite et accompagne chacun à prendre soin de soi grâce à des services personnalisés. Prenez rendez-vous dès maintenant pour une expérience résolument&nbsp;inédite.</p>
            <a href=" https://wellness.galerieslafayette.com?utm_source=glcom&utm_medium=cta" class="button primary">Prendre rendez-vous<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body middle-section">
    <div class="container">
      <div class="luxe-row no-gutter luxe-reverse luxe-middle-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <ul>
              <li>
                <h2>Le Sport</h2>
                <p>avec cours collectifs ou individuels et une sélection de pièces sportswear.</p>
              </li>
              <li>
                <h2>Les Soins</h2>
                <p>proposés par des marques iconiques ou plus confidentielles, des experts triés sur le volet pour des parcours et routines à personnaliser.</p>
              </li>
              <li>
                <h2>Le Bien-être</h2>
                <p class="is-marginless">alliant une cantine healthy et des rituels, entre tradition et innovation.</p>
              </li>
            </ul>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/wellness-inspi-round_D.png"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/wellness-inspi-round_D@2x.png 2x"
                alt="Wellness Galerie - Galeries Lafayette">
            </figure>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body brands">
    <div class="container">
      <div class="luxe-row luxe-center-mobile">
        <div class="luxe-col-mobile-12">
          <div class="content">
            <h2 class="boxed-text">
              <span>Les marques du mois</span>
            </h2>
            <div class="luxe-row brand-items has-large-txt">
              <div class="luxe-col-mobile-6 luxe-col-tablet-3">
                <a href="https://www.galerieslafayette.com/b/nideco">
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202208/brand-01.png"
                      srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202208/brand-01@2x.png 2x"
                      alt="Wellness Galerie - Galeries Lafayette">
                  </figure>
                </a>
                <p class="brand-title is-bold">Nideco</p>
                <p class="brand-txt">L'imagination et la co-création de toute une communauté de consommateurs pour des produits cosmétiques naturels.</p>
                <a href="https://www.galerieslafayette.com/b/nideco">Découvrir</a>
              </div>
              <div class="luxe-col-mobile-6 luxe-col-tablet-3">
                <a href="https://www.galerieslafayette.com/b/sarah+chapman">  
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202208/brand-02.png"
                      srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202208/brand-02@2x.png 2x"
                      alt="Wellness Galerie - Galeries Lafayette">
                  </figure>
                </a>
                <p class="brand-title is-bold">Sarah<br /> Chapman</p>
                <p class="brand-txt">Les actifs puissants et la technologie de pointe d'une facialiste de renommée mondiale dans des soins pour un éclat parfait.</p>
                <a href="https://www.galerieslafayette.com/b/sarah+chapman">Découvrir</a>
              </div>
              <div class="luxe-col-mobile-6 luxe-col-tablet-3">
                <a href="https://www.galerieslafayette.com/b/rodial">  
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202208/brand-03.png"
                      srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202208/brand-03@2x.png 2x"
                      alt="Wellness Galerie - Galeries Lafayette">
                  </figure>
                </a>
                <p class="brand-title is-bold">Rodial</p>
                <p class="brand-txt">L'expertise d'une rédactrice beauté dans des produits de soin hi-tech et innovants, pour des bienfaits instantanés et durables.</p>
                <a href="https://www.galerieslafayette.com/b/rodial">Découvrir</a>
              </div>
              <div class="luxe-col-mobile-6 luxe-col-tablet-3">
                <a href="https://www.galerieslafayette.com/b/roll+on+jade">  
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202208/brand-04.png"
                      srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/202208/brand-04@2x.png 2x"
                      alt="Wellness Galerie - Galeries Lafayette">
                  </figure>
                </a>
                <p class="brand-title is-bold">Roll on<br /> Jade</p>
                <p class="brand-txt">Le meilleur des techniques de beauté asiatiques et l'énergie des cristaux dans des outils de massages incontournables.</p>
                <a href="https://www.galerieslafayette.com/b/roll+on+jade">Découvrir</a>
              </div>
            </div>
            <a href="https://www.galerieslafayette.com/c/wellness+galerie" class="button outlined">Voir toute la sélection</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body magasin">
    <div class="container">
      <div class="luxe-row luxe-center-tablet luxe-middle-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content text-left">
            <h3>Préparez votre visite à<br class="is-hidden-desktop" /> la Wellness Galerie</h3>
            <hr />
            <p>40 Bd. Haussmann, 75009 Paris<br />
              <a class="underlined" href="https://www.google.fr/maps/place/Galeries+Lafayette+Haussmann/@48.8733284,2.3299672,17z/data=!3m1!4b1!4m5!3m4!1s0x47e66e3703a1108b:0xe6773845cdab1593!8m2!3d48.8733284!4d2.3321559" target="_blank">> Voir sur Google Maps</a></p>
            <p>Tel: +33969397575</p>
            <ul class="week">
              <li><span class="day">Lundi</span> 10:00-20:30</li>
              <li><span class="day">Mardi</span> 10:00-20:30</li>
              <li><span class="day">Mercredi</span> 10:00-20:30</li>
              <li><span class="day">Jeudi</span> 10:00-20:30</li>
              <li><span class="day">Vendredi</span> 10:00-20:30</li>
              <li><span class="day">Samedi</span> 10:00-20:30</li>
              <li><span class="day">Dimanche</span> 11:00-20:00</li>
            </ul>
            <hr />
            <a href=" https://wellness.galerieslafayette.com?utm_source=glcom&utm_medium=cta" class="button primary">Prendre rendez-vous<span class="icon has-arrow"></span></a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/wellness-galerie/coupole-hsm-round.png"
                alt="Galeries Lafayette Haussmann">
            </figure>
          </div>
        </div>
      </div>
    </div>
  </section>

</div>
<!--=========================== FIN LANDING PAGE ========================-->

<!-- <script src="https://static.galerieslafayette.com/media/LP/src/js/2021/pangaia.min.v01.js"></script> -->
  
    
  <!-- build:js /media/LP/src/js/2021/miu-miu-2021.min.v00.js
    <script src="../src/js/2021/miu-miu-2021.js"></script>
  <!-- endbuild -->
  
<?php include ('../../pages-defaults/footer.php'); ?>
