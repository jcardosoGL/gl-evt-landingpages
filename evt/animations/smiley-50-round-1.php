<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Smiley";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../media/LP/src/css/2022/smiley-50.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2022/smiley-50.min.v02.css" rel="stylesheet" type="text/css" />
<div class="lp-container">
  <section class="section lp-hero">
    <div class="container">
      <div class="luxe-row no-gutter">
        <div class="luxe-col-mobile-12 luxe-col-tablet-7">
          <div class="hero-image"></div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-5 luxe-col-fullhd-4 luxe-first-tablet">    
          <div class="boxed-text-container">      
            <h1 class="boxed-text">
              <span>Happy Birthday</span>
              <span>Smiley<sub class="r">&reg;</sub> !</span>
            </h1>
            <a href="#collection" class="has-smoothscroll selection-cta is-hidden-tablet">
              <p class="boxed-text selection">
                <span>Explorer la collection</span>
                <span>↓</span>
              </p>
            </a>
          </div>
          <div class="content main-text">
            <p>Du 21 février au 3 avril, SMILEY fête ses 50 ans aux Galeries Lafayette.</p>
            <p>En ligne et en magasin, des collaborations exclusives, des installations immersives et autres animations feel good vous invitent à prendre le temps de sourire&nbsp;!</p>
          </div>
          
          <a href="#collection" class="has-smoothscroll selection-cta is-hidden-mobile">
            <p class="boxed-text selection">
              <span>Découvrir</span>
              <span>↓</span>
            </p>
          </a>
        </div>
        <div class="luxe-col-mobile-12">
          <figure class="image smiley-logo">
            <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/logo-smiley.svg"
              alt="Smiley : Prenez le temps de sourire - Galeries Lafayette">
          </figure>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body collection" id="collection">
    <div class="container">
      <div class="luxe-row no-gutter">
        <div class="luxe-col-mobile-12 luxe-col-tablet-8 luxe-col-widescreen-10 luxe-col-fullhd-9">
          <div class="content">
            <h2 class="boxed-text">
              <span>Des produits</span><br>
              <span>100% bonne humeur</span>
            </h2>
            <p>50 marques se sont associées à Smiley pour imaginer des produits collector ou des collections capsules reprenant le fameux logo Smiley réinventé par l'artiste André Saraiva.
            </p>
          </div>
        </div>
      </div>
      
      <div class="products luxe-row text-center">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="luxe-row is-fullheight">
            <div class="product main luxe-col-mobile-12">
              <a href="https://www.galerieslafayette.com/p/sac+a+dos+padded+musco+eastpak+x+smiley-eastpak/81623911/256
              ">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_main_top.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_main_top@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="588" height="588">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_main_top.jpg" alt="Smiley - Galeries Lafayette" width="747" height="747" /></noscript>
                  </figure>
                </div>
                <div class="product-txt has-text-white">
                  <p class="is-uppercase">Eastpak</p>
                  <p> Sac à dos Padded Musco Eastpak x Smiley</p>
                </div>
              </a>
            </div>
          </div>
        </div>
        <!--   ROW 1   -->
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="luxe-row">
            <div class="product luxe-col-mobile-6">
              <a href="https://www.galerieslafayette.com/p/bottes+de+pluie+x+smiley+50-aigle/81443176/328" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_01.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_01@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_01.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">Aigle</p>
                  <p>Bottes de pluie x Smiley 50</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6">
              <a href="https://www.galerieslafayette.com/p/83472111" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_02.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_02@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_02.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">Maison Assouline</p>
                  <p>Livre Maison Assouline x Smiley</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6">
              <a href="https://www.galerieslafayette.com/p/83472198" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_03.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_03@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_03.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">YellowPop</p>
                  <p>Néon Yellowpop x Smiley</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6">
              <a href="https://www.galerieslafayette.com/p/pull+yellow+x+smiley+50-philosophy/83472162/256" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_04.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_04@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_04.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">Philosophy</p>
                  <p>Pull Yellow x Smiley 50</p>
                </div>
              </a>
            </div>
          </div>
        </div>
        <!--   END ROW 1   -->
          
        <!--   ROW 2   -->
        <div class="luxe-col-mobile-12">
          <div class="luxe-row">
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/sweat+beat+smiley+droit+coton+molleton+bio-galeries+lafayette/80358950/125" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_05.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_05@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_05.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">Galeries Lafayette</p>
                  <p>Sweat Beat Smiley droit coton molleton bio</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/82995031" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_06.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_06@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_06.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">Havaianas</p>
                  <p>Tongs Hava x Smiley</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/lunettes+de+soleil+x+smiley-thierry+lasry/83472060/256" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_07c.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_07c@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_07c.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">THIERRY LASRY</p>
                  <p>Lunettes de soleil x Smiley</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/palette+eyeshadow+x+smiley-ciate/83472064/306" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_08.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_08@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_08.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">Ciaté</p>
                  <p>Palette Eyeshadow x Smiley</p>
                </div>
              </a>
            </div>
          </div>
        </div>
        <!--  END ROW 2   -->
          
        <!--   ROW 3   -->
        <div class="luxe-col-mobile-12 luxe-col-tablet-3">
          <div class="luxe-row">
            <div class="product luxe-col-mobile-6 luxe-col-tablet-12">
              <a href="https://www.galerieslafayette.com/p/ballon+de+basketball+chinatown+market+x+smiley-chinatown+market/83472189/384" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_09.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_09@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_09.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">MARKET</p>
                  <p>Ballon de basketball Market x Smiley</p>
                </div>
              </a>
            </div>            
            <div class="product luxe-col-mobile-6 luxe-col-tablet-12">
              <a href="https://www.galerieslafayette.com/p/83420863" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_10.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_10@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_10.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">Vanessa Bruno</p>
                  <p>Sac cabas L Vanessa Bruno x Smiley</p>
                </div>
              </a>
            </div>            
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="luxe-row is-fullheight">
            <div class="product main luxe-col-mobile-12">
              <a href="https://www.galerieslafayette.com/p/hoodie+x+smiley+droit+molletonne-champion/83171056/235">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_main_bottom.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_main_bottom@2x.jpg"
                    alt="Smiley - Galeries Lafayette" width="588" height="588">
                    <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_main_bottom.jpg" alt="Smiley - Galeries Lafayette" width="747" height="747" /></noscript>
                  </figure>
                </div>
                <div class="product-txt has-text-white">
                  <p class="is-uppercase">Champion</p>
                  <p class="">Hoodie x Smiley droit molletonné</p>
                </div>
              </a>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-3">
          <div class="luxe-row">
            <div class="product luxe-col-mobile-6 luxe-col-tablet-12">
              <a href="https://www.galerieslafayette.com/p/jogging+x+smiley+50+droit+molletonne-champion/83171062/232" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_11.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_11@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_11.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">Champion</p>
                  <p>Jogging x Smiley 50 droit molletonné</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-12">
              <a href="https://www.galerieslafayette.com/p/mini+jupe+sequins+x+smiley-alice+olivia/83472036/259" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_12c.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_12c@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-1/product_12c.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">ALICE & OLIVIA</p>
                  <p>Mini jupe sequins x Smiley</p>
                </div>
              </a>
            </div>
          </div>
        </div>
        <!--   END ROW 3   -->
        
      </div>
      
      <div class="luxe-row no-gutter">
        <div class="luxe-col-mobile-12 text-center">
          <a href="https://www.galerieslafayette.com/t/offre-smiley" class="collection-cta">Voir toute la collection<span class="plus"></span></a>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body graff">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-10">
          <div class="content">
            <figure class="image default-graff">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/graff-prenezletemps.svg"
                alt="Prenez le temps de sourire - Galeries Lafayette">
            </figure>
            <p>Cliquez sur "Démarrer" et utilisez votre <span class="is-hidden-desktop">doigt</span> <span class="is-hidden-touch">souris</span> pour graffer le dessin ou <br class="is-hidden-touch">le message de votre choix. Place à l’imagination et à la créativité&nbsp;!</p>
            <button id="startgraff" class="button primary has-icon" type="button">Démarrer<span class="icon has-arrow"></span></button>
          </div>
        </div>
        <div class="luxe-col-mobile-12">
          <div class="graffcontainer is-hidden">
            <button id="endgraff" class="close-button" type="button"></button>
            <canvas id="graffcanvas" width="320" height="500"></canvas>
            <div class="buttons">
              <button id="clear-button" class="button outlined" type="button">Recommencer</button>
              <a id="save-button" class="button outlined" download="my-graffiti" target="_blank">Enregistrer</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body section-with-image andre">
    <div class="container">
      <div class="luxe-row text-left">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-last-tablet">
          <div class="visuel-container">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/mr-andre.gif" 
                alt="Prenez le temps de sourire - Galeries Lafayette">
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <h2 class="boxed-text">
              <span>MR. A</span>
              <span>André Saraiva</span>
            </h2>
            <p class="has-text-white">Pour son anniversaire, Smiley fait appel à l’artiste André Saraiva pour réinventer son logo. Figure emblématique du graffiti parisien, André est devenu mondialement connu grâce à son alter ego Mr. A : une signature et un personnage qui apparaît aux quatre coins du monde. Pour Smiley, André Saraiva a voulu capturer les valeurs au cœur de la marque que sont la joie et l’optimisme.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body section-with-image haussmann">
    <div class="container">
      <div class="luxe-row no-gutter visuel-container">
        <a class="luxe-col-mobile-12 visuel" href="https://haussmann.galerieslafayette.com/prada-le-pop-up" target="_blank"></a>
      </div>
      <div class="luxe-row no-gutter text-left">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <h2 class="boxed-text">
              <span>Smiley<sub class="r">&reg;</sub> fait <span class="is-hidden-mobile">sourire</span></span>
              <span><span class="is-hidden-tablet">sourire</span> votre <span class="is-hidden-mobile">magasin</span></span>
              <span class="is-hidden-tablet"> magasin</span>
            </h2>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <p class="has-text-white">L’expérience Smiley se vit jusqu’au bout du smile. Du 21 février au 2 avril, le skateboard s’invite aux Galeries Lafayette avec l’installation d’une rampe de skate aux couleurs de Smiley sur le boulevard Haussmann, en face du magasin de l’Homme. Rendez-vous également sur la Smiley Gaming Area, pour tester une sélection de jeux d’Arcade originaux et des consoles de jeux en réalité virtuelle. </p>
            <a href="https://haussmann.galerieslafayette.com/50-ans-de-smiley" class="is-bold button  has-icon primary outlined" target="_blank">En savoir plus<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body section-with-image fondation" style="background-color:#FFE816;">
    <div class="container">
      <div class="luxe-row text-left">
        <div class="luxe-col-mobile-12 luxe-col-tablet-7 luxe-col-desktop-6 luxe-last-tablet">
          <a class="visuel-container" href="https://www.galerieslafayette.com/t/smile-for-equality">
            <figure class="image is-hidden-tablet">
              <img class="b-lazy" 
              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/bandana-yellow_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/bandana-yellow_M@2x.jpg"
              alt="Smiley for equality - Galeries Lafayette" width="360" height="450">
              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/bandana-yellow_M.jpg" alt="Smiley for equality - Galeries Lafayette" width="360" height="450" /></noscript>
            </figure>
            <figure class="image is-hidden-mobile">
              <img class="b-lazy" 
              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/bandana-yellow_D.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/bandana-yellow_D@2x.jpg"
              alt="Smiley for equality - Galeries Lafayette" width="720" height="800">
              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/bandana-yellow_M.jpg" alt="Smiley for equality - Galeries Lafayette" width="720" height="800" /></noscript>
            </figure>
          </a>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-5 luxe-col-desktop-6">
          <div class="content">
            <h2 class="boxed-text">
              <span>Smile for equality</span>
              <span>Galeries&nbsp;Lafayette</span>
              <span style="border-top:none;">S'engage</span>
            </h2>
            <p>À l’occasion de la journée internationale du droit des Femmes, Galeries Lafayette vous invitent à faire un don à la Fondation des Femmes, à travers le dispositif d’arrondi en caisse jusqu'au 3 avril.</p>
            <p>Du 25 février au 9 mars, découvrez aussi une sélection de produits caritatifs, signalés par un Smiley, dont une partie du prix de vente sera reversée à des associations soutenant le droit des Femmes. Enfin, les bénéfices de la vente d’un bandana développé par la marque propre des Galeries Lafayette seront reversés à la Fondation des Femmes.</p>
            <a href="https://www.galerieslafayette.com/p/bandana+smiley+coton+organique-galeries+lafayette/80563776/356" class="is-bold button  has-icon primary outlined inverted" target="_blank">En savoir plus<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body univers">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-tablet">
        <div class="luxe-col-mobile-12">
          <a href="https://www.galerieslafayette.com/t/produits-smiley-collector" class="univers-cta">
            <h2 class="boxed-text">
              <span>tout l'univers <span class="is-hidden-mobile">Smiley<sub class="r">&reg;</sub></span></span>
              <span class="is-hidden-tablet">Smiley<sub class="r">&reg;</sub><br  /></span>
              <span><span class="is-hidden-tablet">découvrir →</span><span class="is-hidden-mobile">→︎</span></span>
            </h2>
          </a> 
        </div>
      </div>
    </div>
  </section>

</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2022/smiley-50.min.v02.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->

<!-- build:js /media/LP/src/js/2022/smiley-50.min.v02.js
  <script src="../../assets/js/blazy.min.js"></script>
  <script src="../../assets/js/flickity.pkgd.min.js"></script>
  <script src="../src/js/2022/smiley-50.js"></script>
endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
