<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "My Clarins";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../media/LP/src/css/2021/lp-beaute.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/lp-beaute.min.css" rel="stylesheet" type="text/css" />
<style>
  .lp-beaute .lp-beaute-hero .hero-image {
    background-position: center bottom;
    background-image: url('https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/hero_M.jpg');
  }
  @media
  only screen and (-webkit-min-device-pixel-ratio: 2),
  only screen and (   min--moz-device-pixel-ratio: 2),
  only screen and (   -moz-min-device-pixel-ratio: 2),
  only screen and (     -o-min-device-pixel-ratio: 2/1),
  only screen and (        min-device-pixel-ratio: 2),
  only screen and (                min-resolution: 192dpi),
  only screen and (                min-resolution: 2dppx) {
    .lp-beaute .lp-beaute-hero .hero-image {
      background-image: url('https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/hero_M.jpg');
    }
  }
  @media screen and (min-width: 1025px) {
    .lp-beaute .lp-beaute-hero .hero-image {
      background-position: center center;
      background-image: url('https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/hero_D.jpg');
    }
  }
  @media only screen and (min-width: 1025px) and
  (-webkit-min-device-pixel-ratio: 2),
  only screen and (   min--moz-device-pixel-ratio: 2),
  only screen and (   -moz-min-device-pixel-ratio: 2),
  only screen and (     -o-min-device-pixel-ratio: 2/1),
  only screen and (        min-device-pixel-ratio: 2),
  only screen and (                min-resolution: 192dpi),
  only screen and (                min-resolution: 2dppx) {
    .lp-beaute .lp-beaute-hero .hero-image {
      background-image: url('https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/hero_D@2x.jpg');
    }
  }
</style>
<div class="lp-beaute">
  <section class="lp-beaute-hero">
    <div class="container">
      <div class="luxe-row no-gutter luxe-reverse luxe-middle-mobile luxe-center-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="hero-image"></div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <div class="date">Beauté — 03.05.21</div>
            <h1>My Clarins n'a rien à cacher</h1>
            <p>My Clarins dévoile ses produits vegan friendly ciblés pour faire face au stress et à la pollution. Au programme ? Des soins éco-conscious à adopter sans plus tarder.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  
  <section class="lp-beaute-body edito">
    <div class="container">

      <div class="luxe-row no-gutter luxe-middle-mobile luxe-center-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-11 luxe-col-desktop-10 luxe-col-widescreen-9">
          <div class="article-image">
            <figure class="image is-hidden-desktop">
              <img class="b-lazy" 
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/edito_full_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/edito_full_M@2x.jpg"
                alt="Des formules plus naturelles My Clarins - Galeries Lafayette">
                <!-- Fallback for non JavaScript browsers -->
                <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/edito_full_M.jpg" alt="Des formules plus naturelles My Clarins - Galeries Lafayette" width="767" height="206" /></noscript>
            </figure>
            <figure class="image is-hidden-touch">
              <img class="b-lazy" 
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/edito_full_D.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/edito_full_D@2x.jpg"
                alt="Des formules plus naturelles My Clarins - Galeries Lafayette" width="960" height="538">
                <!-- Fallback for non JavaScript browsers -->
                <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/edito_full_D.jpg" alt="Des formules plus naturelles My Clarins - Galeries Lafayette" width="960" height="538" /></noscript>
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-10 luxe-col-desktop-9 luxe-col-widescreen-8">
          <div class="content">
            <h3>Des formules plus naturelles</h3>
            <p>Avis aux beautistas, les soins My Clarins débarquent dans toutes les salles de bain. Tournée vers une démarche responsable, My Clarins imagine des soins clean, bons pour la peau, bons pour la planète. De la crème fraîcheur hydratante au gel nettoyant purifiant en passant par le masque de nuit relaxant, les soins My Clarins sont composés à 88% d’ingrédients d’origine naturelle. Un véritable cocktail d’extraits de plantes et de fruits pour une peau en pleine forme. Sans phtalates, sans parabènes, sans sulfates, chaque formule est proposée dans un packaging en matières&nbsp;recyclées.</p>
          </div>
        </div>
      </div>
      
    </div>
  </section>
  
  
  <!-- PRODUCTS -->
  <section class="lp-beaute-body products">
    <div class="container">
      
      <div class="luxe-row no-gutter luxe-center-mobile">
        <div class="luxe-col-mobile-12">
          <div class="luxe-row no-gutter">
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/re-boost+creme+fraicheur+hydratante-my+clarins/67667916/378">
                <div class="product-image luxe-middle-mobile" style="background-color:#f1f1f1;">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/01-prod-01-reboost.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/01-prod-01-reboost@2x.jpg"
                      alt="My Clarins RE-BOOST Crème fraîcheur hydratante - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/01-prod-01-reboost.jpg" alt="My Clarins RE-BOOST Crème fraîcheur hydratante - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">My Clarins</p>
                  <p>RE-BOOST Crème fraîcheur hydratante<br />
                    My Clarins</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/re-fresh+brume+beaute+hydratante-my+clarins/67667925/378">
                <div class="product-image luxe-middle-mobile" style="background-color:#f1f1f1;">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/01-prod-02-refresh.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/01-prod-02-refresh@2x.jpg"
                      alt="My Clarins RE-FRESH Brume beauté hydratante - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/01-prod-02-refresh.jpg" alt="My Clarins RE-FRESH Brume beauté hydratante - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">My Clarins</p>
                  <p>RE-FRESH Brume beauté hydratante<br />
                    My Clarins</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/re-moveeau+lactee+micellaire+demaquillante-my+clarins/67667927/378">
                <div class="product-image luxe-middle-mobile" style="background-color:#f1f1f1;">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/01-prod-03-remove.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/01-prod-03-remove@2x.jpg"
                      alt="My Clarins RE-MOVE Eau lactée micellaire démaquillante - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/01-prod-03-remove.jpg" alt="My Clarins RE-MOVE Eau lactée micellaire démaquillante - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">My Clarins</p>
                  <p>RE-MOVE Eau lactée micellaire démaquillante<br />
                    My Clarins</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/re-move+gel+nettoyant+purifiant-my+clarins/67667929/378" class="">
                <div class="product-image luxe-middle-mobile" style="background-color:#f1f1f1;">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/01-prod-04-remove.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/01-prod-04-remove@2x.jpg"
                      alt="My Clarins RE-MOVE Gel nettoyant purifiant - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/01-prod-04-remove.jpg" alt="My Clarins RE-MOVE Gel nettoyant purifiant - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">My Clarins</p>
                  <p>RE-MOVE Gel nettoyant purifiant<br />
                    My Clarins</p>
                </div>
              </a>
            </div>
          </div>
        </div>
        
        <div class="luxe-col-mobile-12">
          <a href="https://www.galerieslafayette.com/b/my+clarins" class="button outlined">Voir toutes les produits<span class="icon has-arrow"></span></a>
        </div>
        
      </div>
    </div>
  </section>
  
  
  <section class="lp-beaute-body edito">
    <div class="container">

      <div class="luxe-row no-gutter luxe-middle-mobile luxe-center-tablet">
        <div class="luxe-col-mobile-12 luxe-col-widescreen-11 luxe-col-fullhd-10">
          <div class="luxe-row no-gutter">
            <div class="luxe-col-mobile-12 luxe-col-tablet-6">
            <div class="article-image article-left">
              <figure class="image is-hidden-tablet">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/edito_half_left_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/edito_half_left_M@2x.jpg"
                  alt="Une peau éclatante de santé My Clarins - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/edito_half_left_M.jpg" alt="Une peau éclatante de santé My Clarins - Galeries Lafayette" width="767" height="206" /></noscript>
              </figure>
              <figure class="image is-hidden-mobile">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/edito_half_left_D.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/edito_half_left_D@2x.jpg"
                  alt="Une peau éclatante de santé My Clarins - Galeries Lafayette" width="538" height="538">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/edito_half_left_D.jpg" alt="Une peau éclatante de santé My Clarins - Galeries Lafayette" width="538" height="538" /></noscript>
              </figure>
            </div>
          </div>
            <div class="luxe-col-mobile-12 luxe-col-tablet-6 is-hidden-mobile">
            <div class="article-image article-right">
              <figure class="image is-hidden-mobile">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/edito_half_right_D.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/edito_half_right_D@2x.jpg"
                  alt="Une peau éclatante de santé My Clarins - Galeries Lafayette" width="538" height="538">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/edito_half_right_D.jpg" alt="Une peau éclatante de santé My Clarins - Galeries Lafayette" width="538" height="538" /></noscript>
              </figure>
            </div>
          </div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-10 luxe-col-desktop-9 luxe-col-fullhd-8">
          <div class="content">
            <h3>Une peau éclatante de santé</h3>
            <p>Pour My Clarins, si une peau est belle à l’intérieur, cela se verra à l’extérieur. Les cosmétiques aux formulations simples offrent ce que la nature a de meilleur tout en la respectant pour une peau éclatante, jour après jour. Les soins My Clarins s’adaptent à tous les types de peau. Ils hydratent, rééquilibrent et embellissent sans jamais agresser. Gorgée d’hydratation, la peau est fraîche, douce et radieuse. Énergisée et dépolluée de l’intérieur, elle affiche un grain parfait.</p>
          </div>
        </div>
      </div>
      
    </div>
  </section>
  
  
  <!-- PRODUCTS -->
  <section class="lp-beaute-body products">
    <div class="container">
      
      <div class="luxe-row no-gutter luxe-center-mobile">
        <div class="luxe-col-mobile-12">
          <div class="luxe-row no-gutter">
            <div class="product luxe-col-mobile-6 luxe-col-tablet-4">
              <a href="https://www.galerieslafayette.com/p/re-boost+creme+confort+hydratante-my+clarins/67667918/378">
                <div class="product-image luxe-middle-mobile" style="background-color:#f1f1f1;">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/02-prod-01-reboost.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/02-prod-01-reboost@2x.jpg"
                      alt="My Clarins RE-BOOST Crème confort hydratante - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/02-prod-01-reboost.jpg" alt="My Clarins RE-BOOST Crème confort hydratante - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">My Clarins</p>
                  <p>RE-BOOST Crème confort hydratante<br />
                   My Clarins</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-4">
              <a href="https://www.galerieslafayette.com/p/re-charge+masque+nuit+relaxant-my+clarins/67667922/378">
                <div class="product-image luxe-middle-mobile" style="background-color:#f1f1f1;">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/02-prod-02-recharge.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/02-prod-02-recharge@2x.jpg"
                      alt="My Clarins RE-CHARGE Masque nuit relaxant - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/02-prod-02-recharge.jpg" alt="My Clarins RE-CHARGE Masque nuit relaxant - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">My Clarins</p>
                  <p>RE-CHARGE Masque nuit relaxant<br />
                   My Clarins</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-4">
              <a href="https://www.galerieslafayette.com/p/re-boost+creme+matite+hydratante-my+clarins/67667920/378">
                <div class="product-image luxe-middle-mobile" style="background-color:#f1f1f1;">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/02-prod-03-reboost.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/02-prod-03-reboost@2x.jpg"
                      alt="My Clarins RE-BOOST Crème matité hydratante - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/beaute-my-clarins/02-prod-03-reboost.jpg" alt="My Clarins RE-BOOST Crème matité hydratante - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">My Clarins</p>
                  <p>RE-BOOST Crème matité hydratante<br />
                   My Clarins</p>
                </div>
              </a>
            </div>
          </div>
        </div>
        
        <div class="luxe-col-mobile-12">
          <a href="https://www.galerieslafayette.com/b/my+clarins" class="button outlined">Voir tous les produits<span class="icon has-arrow"></span></a>
        </div>
        
      </div>
    </div>
  </section>
    
    
  <section class="lp-beaute-body last-section">
    <div class="container">

      <div class="luxe-row no-gutter luxe-middle-mobile luxe-center-mobile">
        <div class="luxe-col-mobile-12 luxe-col-tablet-10 luxe-col-desktop-9">
          <div class="content">
            <h3>Tout l’univers My&nbsp;Clarins</h3>
            <a href="https://www.galerieslafayette.com/b/my+clarins" class="button primary">Découvrir<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
      
    </div>
  </section>
  
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/blazy.min.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->
  
<!-- build:js /media/LP/src/js/2021/prada-collection-ss21.min.v01.js 
  <script src="../../assets/js/blazy.min.js"></script>
  <script src="../src/js/2021/prada-collection-ss21.js"></script>
<!-- endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
