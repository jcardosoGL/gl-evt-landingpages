<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Roland Garros";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../media/LP/src/css/2021/roland-garros-2021.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/roland-garros-2021.min.v02.css" rel="stylesheet" type="text/css" />
<div class="roland-garros">
  <section class="hero">
    <div class="container">
      <div class="luxe-row">
        <div class="luxe-col-mobile-12">
          <h1 class="is-uppercase"><span class="is-bigger">Le Roland-Garros style</span class="is-bigger"> <br> est aux Galeries Lafayette</h1>
          <a href="#collections" class="has-smoothscroll button borderless has-arrow-down">Découvrir les collections</a>
        </div>
      </div>
    </div>
  </section>
  
  <!-- Intro -->
  <section class="scrolling-text" style="background-color: #D35220;">
    <div class="container">
      <div class="scrolling-text-container text-left ltr-scroll">
        <p class="is-compress is-uppercase has-text-white text-size-1">Plus qu’un tournoi : une légende  •  Plus qu’un tournoi : une légende  •  Plus qu’un tournoi : une légende  •  Plus qu’un tournoi : une légende  •  Plus qu’un tournoi : une légende  •  Plus qu’un tournoi : une légende  •  Plus qu’un tournoi : une légende  •  Plus qu’un tournoi : une légende  •  Plus qu’un tournoi : une légende  •  Plus qu’un tournoi</p>
      </div>
    </div>
  </section>
  
  <section class="body intro" style="background-color: #D35220;">
    <div class="container">
      <div class="luxe-row">
        <div class="luxe-col-tablet-2 luxe-col-desktop-3 is-hidden-mobile"><hr /></div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-8  luxe-col-desktop-6 text-left">
          <p>Les joueurs sont prêts ! Roland-Garros prend ses quartiers aux Galeries Lafayette : collections mode Printemps-Été, diffusion des matchs sur la terrasse des Galeries Lafayette Paris Haussmann, boutiques officielles Roland-Garros au Magasin Homme et au Magasin Coupole…  tous les coups sont gagnants et célèbrent la créativité du tournoi iconique. Jeu, set et match !</p>
        </div>
        <div class="luxe-col-tablet-2 luxe-col-desktop-3 is-hidden-mobile"><hr /></div>
      </div>
    </div>
  </section>
  
  
  <section class="scrolling-text" style="background-color: #05482F;">
    <div class="container">
      <div class="scrolling-text-container has-text-2 text-left ltr-scroll">
        <p class="is-compress is-uppercase has-text-white text-size-2">La ligne Héritage  •  La ligne Héritage  •  La ligne Héritage  •  La ligne Héritage  •  La ligne Héritage  •  La ligne Héritage  •  La ligne Héritage  •  La ligne Héritage  •  La ligne Héritage  •  La ligne Héritage  •  </p>
      </div>
    </div>
  </section>
  <section class="scrolling-text" style="border-bottom:1px solid #05482F;" id="collections">
    <div class="container">
      <div class="scrolling-text-container text-left rtl-scroll">
        <p class="is-compress is-uppercase has-text-green text-size-1">l’élégance à la française  • l’élégance à la française  • l’élégance à la française  • l’élégance à la française  • l’élégance à la française  • l’élégance à la française  • l’élégance à la française  • l’élégance à la française  • l’élégance à la française  • l’élégance à la française  • l’élégance à la française  • l’élégance à la française  • </p>
      </div>
    </div>
  </section>
  
  
  <section class="body products">
    <div class="container">
      <div class="luxe-row no-gutter slider-heritage slider-flickity">
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/debardeur+raye+roland+garros+-+ecru-roland+garros/300409344893/60">
            <div class="product-image">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/debardeur_heritage.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/debardeur_heritage@2x.jpg"
                  alt="Débardeur rayé - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/debardeur_heritage.jpg" alt="Débardeur rayé - Galeries Lafayette" width="360" height="392" /></noscript>
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Roland-Garros</p>
              <p>Débardeur rayé</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/polo+homme+roland+garros+avec+fines+rayures+-+terre+battue-roland+garros/300409329527/356">
            <div class="product-image">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/polo_heritage.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/polo_heritage@2x.jpg"
                  alt="Polo homme avec fines rayures - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/polo_heritage.jpg" alt="Polo homme avec fines rayures - Galeries Lafayette" width="360" height="392" /></noscript>
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Roland-Garros</p>
              <p>Polo homme avec fines rayures</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/robe+aux+manches+courtes+roland+garros+-+bleu+marine-roland+garros/300409344531/101">
            <div class="product-image">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/robe_heritage@1x.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/robe_heritage@2x.jpg"
                  alt="Robe aux manches courtes - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/robe_heritage.jpg" alt="Robe aux manches courtes - Galeries Lafayette" width="360" height="392" /></noscript>
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Roland-Garros</p>
              <p>Robe aux manches courtes</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sac+de+voyage+village+croute+de+cuir+de+vachette+roland-garros+-+marine-roland+garros/300409356548/101">
            <div class="product-image">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sac_voyage_village.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sac_voyage_village@2x.jpg"
                  alt="Sac cabas Village - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sac_voyage_village.jpg" alt="Sac cabas Village - Galeries Lafayette" width="360" height="392" /></noscript>
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Roland-Garros</p>
              <p>Sac cabas Village</p>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>
  
  
  <section class="scrolling-text" style="background-color: #D35220;">
    <div class="container">
      <div class="scrolling-text-container  has-text-2 text-left ltr-scroll">
        <p class="is-compress is-uppercase has-text-white text-size-2">La capsule Beau Joueur / Belle Joueuse  •  La capsule Beau Joueur / Belle Joueuse  •  La capsule Beau Joueur / Belle Joueuse  •  La capsule Beau Joueur / Belle Joueuse  •  La capsule Beau Joueur / Belle Joueuse  • </p>
      </div>
    </div>
  </section>
  <section class="scrolling-text" style="border-bottom:1px solid #D35220;">
    <div class="container">
      <div class="scrolling-text-container text-left rtl-scroll">
        <p class="is-compress is-uppercase has-text-orange text-size-1">humour et style  • humour et style  •  humour et style  •  humour et style  •  humour et style  •  humour et style  •  humour et style  •  humour et style  •  humour et style  •  humour et style  •  humour et style  •  humour et style  •</p>
      </div>
    </div>
  </section>
  
  
  <section class="body products">
    <div class="container">
      <div class="luxe-row no-gutter slider-capsule slider-flickity">
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sweat+col+rond+beau+joueur+roland-garros+homme+-+blanc-roland+garros/300407929131/85">
            <div class="product-image">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sweat_beaujoueur_blanc.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sweat_beaujoueur_blanc@2x.jpg"
                  alt="Sweat col rond Beau Joueur - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sweat_beaujoueur_blanc.jpg" alt="Sweat col rond Beau Joueur - Galeries Lafayette" width="360" height="392" /></noscript>
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Roland-Garros</p>
              <p>Sweat col rond Beau Joueur</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sweat+col+rond+beau+joueur+roland-garros+homme+-+marine-roland+garros/300407929161/101">
            <div class="product-image">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sweat_beaujoueur_marine.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sweat_beaujoueur_marine@2x.jpg"
                  alt="Sweat col rond Beau Joueur - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sweat_beaujoueur_marine.jpg" alt="Sweat col rond Beau Joueur - Galeries Lafayette" width="360" height="392" /></noscript>
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Roland-Garros</p>
              <p>Sweat col rond Beau Joueur</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sweat+col+rond+belle+joueuse+roland-garros+femme+-+blanc-roland+garros/300407929047/85">
            <div class="product-image">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sweat_bellejoueuse.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sweat_bellejoueuse@2x.jpg"
                  alt="Sweat col rond Belle Joueuse - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sweat_bellejoueuse.jpg" alt="Sweat col rond Belle Joueuse - Galeries Lafayette" width="360" height="392" /></noscript>
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Roland-Garros</p>
              <p>Sweat col rond Belle Joueuse</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/t-shirt+col+rond+beau+joueur+roland-garros+homme+-+marine-roland+garros/300407929533/101">
            <div class="product-image">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/tshirt_beaujoueur_marine.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/tshirt_beaujoueur_marine@2x.jpg"
                  alt="T-shirt col rond Beau Joueur - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/tshirt_beaujoueur_marine.jpg" alt="T-shirt col rond Beau Joueur - Galeries Lafayette" width="360" height="392" /></noscript>
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Roland-Garros</p>
              <p>T-shirt col rond Beau Joueur</p>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>
  
  
  <section class="scrolling-text" style="background-color: #05482F;">
    <div class="container">
      <div class="scrolling-text-container  has-text-2 text-left ltr-scroll">
        <p class="is-compress is-uppercase has-text-white text-size-2">La ligne Color Block  •  La ligne Color block  •  La ligne Color Block  •  La ligne Color block  •  La ligne Color Block  •  La ligne Color block  •  La ligne Color Block  •  La ligne Color block  • </p>
      </div>
    </div>
  </section>
  <section class="scrolling-text" style="border-bottom:1px solid #05482F;">
    <div class="container">
      <div class="scrolling-text-container text-left rtl-scroll">
        <p class="is-compress is-uppercase has-text-green text-size-1">le mix and match vitaminé  • le mix and match vitaminé  •  le mix and match vitaminé  •  le mix and match vitaminé  •  le mix and match vitaminé  •  le mix and match vitaminé  •  le mix and match vitaminé  •  le mix and match vitaminé  •  </p>
      </div>
    </div>
  </section>
  
  
  <section class="body products">
    <div class="container">
      <div class="luxe-row no-gutter slider-colorblock slider-flickity">
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sac+a+dos+color+block+roland-garros+-+marine+et+terre+battue-roland+garros/300407930043/101">
            <div class="product-image">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sac_colorblock.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sac_colorblock@2x.jpg"
                  alt="Sac à dos Color Block - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sac_colorblock.jpg" alt="Sac à dos Color Block - Galeries Lafayette" width="360" height="392" /></noscript>
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Roland-Garros</p>
              <p>Sac à dos Color Block</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/t-shirt+col+rond+color+block+roland-garros+homme+-+terre+battue+et+marine-roland+garros/300408538965/356">
            <div class="product-image">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/tshirt_colorblock.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/tshirt_colorblock@2x.jpg"
                  alt="T-shirt col rond Color Block - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/tshirt_colorblock.jpg" alt="T-shirt col rond Color Block - Galeries Lafayette" width="360" height="392" /></noscript>
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Roland-Garros</p>
              <p>T-shirt col rond Color Block</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/debardeur+col+rond+color+block+roland-garros+femme+-+rose+et+marine-roland+garros/300409344758/384">
            <div class="product-image">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/debardeur_colorblock.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/debardeur_colorblock@2x.jpg"
                  alt="Débardeur col rond Color Block - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/debardeur_colorblock.jpg" alt="Débardeur col rond Color Block - Galeries Lafayette" width="360" height="392" /></noscript>
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Roland-Garros</p>
              <p>Débardeur col rond Color Block</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sweat+a+capuche+color+block+roland-garros+femme+-+marine+et+terre+battue-roland+garros/300407929101/101">
            <div class="product-image">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sweatcap_colorblock_femme.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sweatcap_colorblock_femme@2x.jpg"
                  alt="Hoodie Color Block - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/sweatcap_colorblock_femme.jpg" alt="Hoodie Color Block - Galeries Lafayette" width="360" height="392" /></noscript>
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Roland-Garros</p>
              <p>Hoodie Color Block</p>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>
  
  <section class="body">
    <div class="container">
      <div class="luxe-row no-gutter">
        <div class="luxe-col-mobile-12">
          <a href="https://www.galerieslafayette.com/b/roland+garros" class="collection-cta">Voir toutes les collections<span class="arrow"></span></a> 
        </div>
      </div>
    </div>
  </section>
  
  
  <section class="scrolling-text" style="background-color: #D35220;">
    <div class="container">
      <div class="scrolling-text-container  has-text-2 text-left ltr-scroll">
        <p class="is-compress is-uppercase has-text-white text-size-2">Coup droit vers des animations exceptionnelles  •  Les evenements en magasin  •  Coup droit vers des animations exceptionnelles  •  Les evenements en magasin  •  Coup droit vers des animations exceptionnelles •  </p>
      </div>
    </div>
  </section>
  
  
  <section class="body evenements">
    <div class="container">
      <div class="luxe-row no-gutter">
        <div class="luxe-col-mobile-12 luxe-col-tablet-5">
          <article>
            <div class="article-image terrasse text-left">
              <figure class="image is-hidden-desktop">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/tournoi-terrasse_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/tournoi-terrasse_M@2x.jpg"
                  alt="terrasse - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/tournoi-terrasse_M.jpg" alt="terrasse - Galeries Lafayette" width="316" height="316" /></noscript>
              </figure>
              <figure class="image is-hidden-touch">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/tournoi-terrasse_D.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/tournoi-terrasse_D@2x.jpg"
                  width="516" height="560"
                  alt="terrasse - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/tournoi-terrasse_D.jpg" alt="terrasse - Galeries Lafayette" width="516" height="560" /></noscript>
              </figure>
            </div>
            <div class="article-txt text-left">
              <h2>Vivez Roland-Garros 2021&nbsp;!</h2>
              <p>Rendez-vous sur la terrasse des Galeries Lafayette Paris Haussmann pour suivre les matchs du tournoi sur un écran géant. Installez-vous confortablement dans les transats mis à disposition et vibrez aux exploits des champion(ne)s, tout en profitant d’une vue imprenable sur Paris.</p>
            </div>
          </article>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-2">
          <hr  />
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-5">
          <article>
            <div class="article-image text-left">
              <figure class="image is-hidden-desktop">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/jeu-concours_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/jeu-concours_M@2x.jpg"
                  alt="jeu concours - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/jeu-concours_M.jpg" alt="jeu concours - Galeries Lafayette" width="316" height="316" /></noscript>
              </figure>
              <figure class="image is-hidden-touch">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/jeu-concours_D.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/jeu-concours_D@2x.jpg"
                  width="516" height="560"
                  alt="jeu concours - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/roland-garros/jeu-concours_D.jpg" alt="jeu concours - Galeries Lafayette" width="516" height="560" /></noscript>
              </figure>
            </div>
            <div class="article-txt text-left">
              <h2>Gagnez vos places pour la finale&nbsp;!</h2>
              <p>Dès le 29 mai, participez à notre grand jeu-concours et tentez de remporter des places pour la finale du 13 juin prochain. Tentez votre chance sur le compte Instagram des Galeries Lafayette. Résultat du concours le 10 juin. </p>
              <a href="https://www.instagram.com/p/CPda25jDaRm/" class="button primary outlined">Participer au concours<span class="icon has-arrow"></span></a>
            </div>
          </article>
        </div>
      </div>
    </div>
  </section>
  
  
  <section class="scrolling-text" style="background-color: #D35220;">
    <div class="container">
      <div class="scrolling-text-container  has-text-2 text-left ltr-scroll">
        <p class="is-compress is-uppercase has-text-white text-size-2">aux galeries lafayette  •  le style roland garros  •  aux galeries lafayette  •  le style roland garros  •  aux galeries lafayette  •  le style roland garros  •  aux galeries lafayette  •  le style roland garros</p>
      </div>
    </div>
  </section>
  
  
  <section class="body video">
    <div class="luxe-row no-gutter">
      <div class="luxe-col-mobile-12">
        <div class="video-container">
          <a href="https://www.galerieslafayette.com/b/roland+garros" class="video-cta"></a>
          <iframe class="bg-image" src="https://player.vimeo.com/video/555083635?background=1" width="1250" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
        </div>
      </div>
    </div>
  </section>
  
  
  <section class="scrolling-text" style="background-color: #D35220;">
    <div class="container">
      <div class="scrolling-text-container  has-text-2 text-left ltr-scroll">
        <p class="is-compress is-uppercase has-text-white text-size-2">aux galeries lafayette  •  le style roland garros  •  aux galeries lafayette  •  le style roland garros  •  aux galeries lafayette  •  le style roland garros  •  aux galeries lafayette  •  le style roland garros</p>
      </div>
    </div>
  </section>

</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2021/roland-garros-2021.min.v01.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->
  
  
<!-- build:js /media/LP/src/js/2021/roland-garros-2021.min.v01.js
  <script src="../../assets/js/blazy.min.js"></script>
  <script src="../../assets/js/flickity.pkgd.min.js"></script>
  <script src="../../assets/js/ScrollMagic.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
  <script src="../src/js/2021/roland-garros-2021.js"></script>
<!-- endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
