<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Prada Tropico";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../media/LP/src/css/2022/prada-tropico.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2022/prada-tropico.min.v02.css" rel="stylesheet" type="text/css" />
<div class="lp-container">
  <section class="section lp-hero">
    <div class="container is-fullheight">
      <div class="luxe-row no-gutter luxe-center-tablet">
        <div class="luxe-col-mobile-12">    
          <div class="boxed-text-container">      
            <h1 class="boxed-text">
              <span>Prada présente</span>
              <span>Prada Tropico</span>
            </h1>
            
            <a href="#collection" class="has-smoothscroll selection-cta">
              <p class="boxed-text selection">
                <span>Explorer</span>
                <span>↓</span>
              </p>
            </a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-11 luxe-col-desktop-8 luxe-col-widescreen-11 luxe-col-fullhd-9">
          <div class="content main-text">
            <p>Du 24 janvier au 20 février 2022, Prada présente en avant-première « Prada Tropico » aux Galeries Lafayette Haussmann et sur GaleriesLafayette.com. Une collection pop-up qui a inspiré une collection aux multiples combinaisons de rayures colorées qui prend vie dans une installation aux effets lumineux hypnotiques. </p>
          </div>
        </div>
        
        <div class="luxe-col-mobile-12" id="collection">
          <div class="content text-center">
            <div class="slider-ancres slider-flickity luxe-row luxe-center-desktop">
              <div class="product luxe-col-desktop">
                <a href="#chapeaux" class="has-smoothscroll">
                  <div class="product-image">
                    <figure class="image">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/ancres/ancre-chapeaux.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/ancres/ancre-chapeaux@2x.jpg 2x"
                        width="174" height="174"
                        alt="Prada : Les chapeaux - Galeries Lafayette">
                    </figure>
                  </div>
                  <div class="product-txt">
                    <p class="product-title is-uppercase is-bold">Les Chapeaux</p>
                  </div>
                </a>
              </div>
              <div class="product luxe-col-desktop">
                <a href="#sac-triangle"  class="has-smoothscroll">
                  <div class="product-image">
                    <figure class="image">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/ancres/ancre-sacs-vert.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/ancres/ancre-sacs-vert@2x.jpg 2x"
                        width="174" height="174"
                        alt="Prada : Le sac triangle - Galeries Lafayette">
                    </figure>
                  </div>
                  <div class="product-txt">
                    <p class="product-title is-uppercase is-bold">Le sac triangle</p>
                  </div>
                </a>
              </div>
              <div class="product luxe-col-desktop is-hidden">
                <a href="#souliers"  class="has-smoothscroll">
                  <div class="product-image">
                    <figure class="image">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/ancres/ancre-chapeaux.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/ancres/ancre-chapeaux@2x.jpg 2x"
                        width="174" height="174"
                        alt="Prada : Les chapeaux - Galeries Lafayette">
                    </figure>
                  </div>
                  <div class="product-txt">
                    <p class="product-title is-uppercase is-bold">Les souliers</p>
                  </div>
                </a>
              </div>
              <div class="product luxe-col-desktop">
                <a href="#raphia"  class="has-smoothscroll">
                  <div class="product-image">
                    <figure class="image">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/ancres/ancres-raphia-bob.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/ancres/ancres-raphia-bob@2x.jpg 2x"
                        width="174" height="174"
                        alt="Prada : Le raphia - Galeries Lafayette">
                    </figure>
                  </div>
                  <div class="product-txt">
                    <p class="product-title is-uppercase is-bold">Le Raphia</p>
                  </div>
                </a>
              </div>
              <div class="product luxe-col-desktop is-hidden">
                <a href="#lunettes-soleil"  class="has-smoothscroll">
                  <div class="product-image">
                    <figure class="image">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/ancres/ancre-chapeaux.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/ancres/ancre-chapeaux@2x.jpg 2x"
                        width="174" height="174"
                        alt="Prada : Les chapeaux - Galeries Lafayette">
                    </figure>
                  </div>
                  <div class="product-txt">
                    <p class="product-title is-uppercase is-bold">Les lunettes de soleil</p>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body section-header chapeaux" id="chapeaux">
    <div class="container is-fullheight">
      <div class="luxe-row no-gutter luxe-center-mobile is-fullheight">
        <a class="luxe-col-mobile-12" href="https://www.galerieslafayette.com/t/prada-tropico-chapeaux">
          <div class="content">
            <h2 class="boxed-text">
              <span>Les Chapeaux</span>
            </h2>
          </div>
        </a>
      </div>
    </div>
  </section>
  
  <section class="section lp-body products chapeaux">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-mobile text-center">
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/bob+en+tissu+eponge-prada/83482589/259">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/chapeaux/bob_1.webp"
                  alt="Prada : Bob en tissu éponge - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Bob en tissu éponge</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/bob+en+re-nylon-prada/83092756/401">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/chapeaux/bob_rouge.webp"
                alt="Prada : Bob en re-nylon - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Bob en Re-Nylon</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/bob+en+re-nylon-prada/83092756/320">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/chapeaux/bob_3.webp"
                alt="Prada : Bob en Re-Nylon - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Bob en Re-Nylon</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/bob+en+re-nylon-prada/83482964/85">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/chapeaux/bob_4.webp"
                alt="Prada : Bob en Re-Nylon - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Bob en Re-Nylon</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-12">
          <div class="content">
            <a href="https://www.galerieslafayette.com/t/prada-tropico-chapeaux" class="button outlined">Voir toute la sélection<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body section-header sac-triangle" id="sac-triangle">
    <div class="container is-fullheight">
      <div class="luxe-row no-gutter luxe-center-mobile is-fullheight">
        <a class="luxe-col-mobile-12" href="https://www.galerieslafayette.com/t/prada-tropico-sacs-triangle">
          <div class="content">
            <h2 class="boxed-text">
              <span>Le Sac Triangle</span>
            </h2>
          </div>
        </a>
      </div>
    </div>
  </section>
  
  <section class="section lp-body products sac-triangle">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-mobile text-center">
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sac+porte+epaule+triangulaire+en+cuir-prada/83482509/85">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/sacs/sac_blanc.webp"
                  alt="Prada : Sac à bandoulière en cuir Saffiano - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Sac à bandoulière en cuir Saffiano</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sac+a+bandouliere+en+cuir+saffiano-prada/83483018/101">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/sacs/sac_bleu.webp"
                alt="Prada : Sac à bandoulière en cuir Saffiano - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Sac à bandoulière en cuir Saffiano</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sac+porte+epaule+triangulaire+en+cuir-prada/83482509/453">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/sacs/sac_vert.webp"
                alt="Prada : Sac porté épaule triangulaire en cuir - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Sac porté épaule triangulaire en cuir</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sac+porte+epaule+triangulaire+en+cuir-prada/83482509/256">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/sacs/sac_jaune.webp"
                alt="Prada : Sac porté épaule triangulaire en cuir - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Sac porté épaule triangulaire en cuir</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-12">
          <div class="content">
            <a href="https://www.galerieslafayette.com/t/prada-tropico-sacs-triangle" class="button outlined">Voir toute la sélection<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body section-header souliers is-hidden" id="souliers">
    <div class="container is-fullheight">
      <div class="luxe-row no-gutter luxe-center-mobile is-fullheight">
        <div class="luxe-col-mobile-12">
          <div class="content">
            <h2 class="boxed-text">
              <span>Les Souliers</span>
            </h2>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body products souliers is-hidden">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-mobile text-center">
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/bob+en+raphia-prada/83482582/60">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/chapeaux/bob_1.webp"
                  alt="Prada : Bob en tissu éponge - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Bob en tissu éponge</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sac+cabas+en+raphia-prada/83482502/85">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/chapeaux/bob_2.webp"
                alt="Prada : Bob en tissu éponge - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Bob en tissu éponge</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sac+cabas+en+raphia-prada/83482502/320">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/chapeaux/bob_3.webp"
                alt="Prada : Bob en tissu éponge - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Bob en Re-Nylon</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sandales+a+talons+dot+en+cuir+et+plumes-bottega+veneta/80893550/384">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/chapeaux/bob_4.webp"
                alt="Prada : Bob en tissu éponge - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Bob en Re-Nylon</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-12">
          <div class="content">
            <a href="https://www.galerieslafayette.com/b/bottega+veneta" class="button outlined">Voir toute la sélection<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body section-header raphia" id="raphia">
    <div class="container is-fullheight">
      <div class="luxe-row no-gutter luxe-center-mobile is-fullheight">
        <a class="luxe-col-mobile-12" href="https://www.galerieslafayette.com/t/prada-tropico-raphia">
          <div class="content">
            <h2 class="boxed-text">
              <span>Le Raphia</span>
            </h2>
          </div>
        </a>
      </div>
    </div>
  </section>
  
  <section class="section lp-body products raphia">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-mobile text-center">
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sac+besace+prada+re-editiona+2005+en+raphia-prada/83482514/256">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/raphias/raphia_4.webp"
                alt="Prada : Sac besace Prada Re-Edition® 2005 en raphia - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Sac besace Prada Re-Edition® 2005 en raphia</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sac+cabas+en+raphia-prada/83659035/320">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/raphias/raphia_cabas_noir.webp"
                alt="Prada : Sac cabas en raphia - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Sac cabas en raphia</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sac+cabas+en+raphia-prada/83482502/356">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/raphias/raphia_cabas_rouge.jpg"
                alt="Prada : Sac cabas en raphia - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Sac cabas en raphia</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/bob+en+raphia-prada/83482582/60">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/raphias/raphia_1.webp"
                  alt="Prada : Bob en raphia - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Bob en raphia</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-12">
          <div class="content">
            <a href="https://www.galerieslafayette.com/t/prada-tropico-raphia" class="button outlined">Voir toute la sélection<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body section-header lunettes-soleil is-hidden" id="lunettes-soleil">
    <div class="container is-fullheight">
      <div class="luxe-row no-gutter luxe-center-mobile is-fullheight">
        <div class="luxe-col-mobile-12">
          <div class="content">
            <h2 class="boxed-text">
              <span>Les lunettes de soleil</span>
            </h2>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body products lunettes-soleil is-hidden">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-mobile text-center">
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sac+pochette+chain+pouch+en+peau+lainee-bottega+veneta/80893801/204">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/chapeaux/bob_1.webp"
                  alt="Prada : Bob en tissu éponge - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Bob en tissu éponge</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/bottines+lug+en+cuir-bottega+veneta/80893794/320">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/chapeaux/bob_2.webp"
                alt="Prada : Bob en tissu éponge - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Bob en tissu éponge</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sac+besace+padded+cassette+en+cuir+tresse-bottega+veneta/80893628/464?version=v2">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/chapeaux/bob_3.webp"
                alt="Prada : Bob en tissu éponge - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Bob en Re-Nylon</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sandales+a+talons+dot+en+cuir+et+plumes-bottega+veneta/80893550/384">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/prada-tropico/produits/chapeaux/bob_4.webp"
                alt="Prada : Bob en tissu éponge - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Prada</p>
              <p>Bob en Re-Nylon</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-12">
          <div class="content">
            <a href="https://www.galerieslafayette.com/b/bottega+veneta" class="button outlined">Voir toute la sélection<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body section-header pop-up">
    <div class="container is-fullheight">
      <div class="luxe-row no-gutter luxe-center-mobile is-fullheight">
        <a class="luxe-col-mobile-12" href="https://haussmann.galerieslafayette.com/prada-le-pop-up" target="_blank">
          
        </a>
      </div>
    </div>
  </section>
  <section class="section lp-body haussmann">
    <div class="container">
      <div class="luxe-row no-gutter text-left">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <h2 class="boxed-text">
              <span>une expérience</span>
              <span>unique</span>
              <span>en magasin</span>
            </h2>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <p>Un pop-up exclusif et l’intégralité des vitrines des Galeries Lafayette Paris Haussmann se parent des couleurs de “Prada Tropico”. Effets graphiques, jeux de rayures et de lumières, néon hypnotiques et forêt tropicale stylisée habillent les espaces du magasin d’une énergie particulière. Pour sublimer une collection (prêt-à-porter et accessoires) déjà culte.</p>
            <a href="https://haussmann.galerieslafayette.com/prada-le-pop-up" class="is-bold alt-button" target="_blank">En savoir plus<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body univers">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-tablet">
        <div class="luxe-col-mobile-12">
          <a href="https://www.galerieslafayette.com/b/prada" class="univers-cta">
            <h2 class="boxed-text">
              <span>tout l'univers <span class="is-hidden-mobile">Prada</span></span>
              <span class="is-hidden-tablet">Prada<br  /></span>
              <span><span class="is-hidden-tablet">découvrir →</span><span class="is-hidden-mobile">→︎</span></span>
            </h2>
          </a> 
        </div>
      </div>
    </div>
  </section>

</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2022/prada-tropico.min.v01.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->


<!-- build:js /media/LP/src/js/2022/prada-tropico.min.v01.js
  <script src="../../assets/js/flickity.pkgd.min.js"></script>
  <script src="../src/js/2022/prada-tropico.js"></script>
endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
