<?php include ('../../../pages-defaults/header.php'); ?>
<script>
  document.title = "Noël";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
  
<!-- <link href="../../../../media/LP/src/css/2020/calendrier-de-l-avent.css" rel="stylesheet" type="text/css"> -->  
  
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/calendrier-de-l-avent.min.css" rel="stylesheet" type="text/css">
<style>
  .main-nav {
    padding-top: 0 !important;
  }
  .ab_widget_container_promotional-banner {
    display: none; 
  }
</style>
<div class="tourbillon-jouets">
  <figure class="image jouets">
    <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/jouets.png"
      srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/jouets@2x.png 2x" 
      alt="Jouets de Noël  - Galeries Lafayette">
  </figure>
  <figure class="image valise-open">
    <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/valise-open.png" 
      alt="Valise de Jouets  - Galeries Lafayette">
  </figure>
</div>
<div class="grand-jeu-noel">
  <section class="calendrier-avent">
    <div class="welcome container">
      <div class="luxe-row no-gutter luxe-middle">
        <figure class="image guirlande">
          <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/welcome-header.png"
            srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/welcome-header@2x.png 2x" 
            alt="Guirlandes de Noël  - Galeries Lafayette" width="360" height="84">
        </figure>
        <figure class="image soleil">
          <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/soleil.png"
            srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/soleil@2x.png 2x" 
            alt="Soleil  - Galeries Lafayette" width="31" height="31">
        </figure>
        <figure class="image discoball">
          <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/discoball.png"
            srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/discoball@2x.png 2x" 
            alt="Discoball  - Galeries Lafayette" width="182" height="182">
        </figure>
        <div class="luxe-col-mobile-12">
          <div class="article-body">
            <figure class="image title">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/title-valise-de-l-avent.png"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/title-valise-de-l-avent@2x.png 2x" 
                alt="Valise de l'avent - Galeries Lafayette" width="245" height="104">
            </figure>
            <p>Du 1er au 24 décembre, tentez de gagner chaque jour, un des merveilleux cadeaux de&nbsp;notre calendrier de l'Avent.<sup>*</sup></p>
            <button class="button is-gold has-arrow trigger_anim">Découvrir le cadeau du jour</button>
          </div>
        </div>
        <figure class="image cadeaux cadeau-bleu">
          <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/cadeau-bleu.png"
            srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/cadeau-bleu@2x.png 2x" 
            alt="cadeau bleu - Galeries Lafayette" width="40" height="38">
        </figure>
        <figure class="image cadeaux cadeau-rouge">
          <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/cadeau-rouge.png"
            srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/cadeau-rouge@2x.png 2x" 
            alt="cadeau rouge - Galeries Lafayette" width="17" height="18">
        </figure>
        <figure class="image cadeaux cadeau-vert">
          <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/cadeau-vert.png"
            srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/cadeau-vert@2x.png 2x" 
            alt="cadeau vert - Galeries Lafayette" width="27" height="21">
        </figure>
        <figure class="image cadeaux cadeau-rose">
          <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/cadeau-rose.png"
            srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/cadeau-rose@2x.png 2x" 
            alt="cadeau rose - Galeries Lafayette" width="41" height="36">
        </figure>
        <figure class="image valise">
          <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/valise-de-l-avent.png"
            srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/valise-de-l-avent@2x.png 2x" 
            alt="Valise de l'avent - Galeries Lafayette" width="300" height="298">
        </figure>
      </div>
    </div>
    <div class="gifts container is-hidden">
      <div class="gifts luxe-row no-gutter luxe-middle text-left">        
        <div class="luxe-col-mobile-12">
          <figure class="image day-title">
            <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/day-title-valise-de-lavent.png"
              srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/day-title-valise-de-lavent@2x.png 2x" 
              alt="Valise de l'avent - Galeries Lafayette" width="307" height="75">
            <figcaption class="day"></figcaption>
          </figure>
        </div>
        
        <div class="luxe-col-mobile-12">
          <div class="article-body">
            <div class="gift-display">
              
              <figure class="image text-center">
                <img class="product" src="" srcset="" alt="">
              </figure>
              <h2 class="title"><span class="brand is-uppercase"></span><span class="product"></span></h2>
              <p class="description"></p>
              <button class="button is-blue trigger_form">Tenter sa chance</button>
            </div>
          </div>
        </div>
        
        <div class="luxe-col-mobile-12"></div>
      </div>
      
      <div class="form" style="display: none;">
        <div class="calendar-form-container">
          <div class="form-main">
            <div class="form-header text-right">
              <button class="close_form" aria-label="Fermer">Fermer</button>
            </div>
            <div class="text-center">
              <h2 class="is-uppercase">Inscrivez-vous à <br />notre tirage au sort</h2>
              <p>Remplissez les informations ci-dessous et tentez votre chance pour remporter le cadeau du jour.</p>
            </div>
            <form class="text-left form-body" id="calendar-form" method="POST">
              <div class="form-group">
                <select class="formSubmit" name="genre" required="" id="civilite">
                    <option disabled="disabled" selected="selected" value="">Civilit&eacute;*</option>
                    <option value="Monsieur">Monsieur</option>
                    <option value="Madame">Madame</option>
                </select>
                <label for="civilite">Civilité<sup>*</sup></label>
              </div>
              
              <div class="form-group">
                <input class="formSubmit" name="lastname" placeholder="Nom*" required="" type="text" id="nom"/>
                <label for="nom">Nom<sup>*</sup></label>
              </div>
              <div class="form-group">
                <input class="formSubmit" name="name" placeholder="Prénom*" required="" type="text" id="prenom"/>
                <label for="prenom">Prénom<sup>*</sup></label>
              </div>
              <div class="form-group">
                <input class="formSubmit" name="email" placeholder="Email*" required="" type="email" id="email"/>
                <label for="email">Email<sup>*</sup></label>
              </div>
              <div class="form-group">
                <input class="formSubmit" name="adress" placeholder="Adresse*" required="" type="text" id="adresse"/>
                <label for="adresse">Adresse<sup>*</sup></label>
              </div>
              <div class="form-group">
                <input class="formSubmit" name="postalcode" placeholder="Code postal*" required="" type="text" id="code-postal"/>
                <label for="code-postal">Code postal<sup>*</sup></label>
              </div>
              <div class="form-group">
                <input class="formSubmit" name="city" placeholder="Ville*" required="" type="text" id="ville"/>
                <label for="ville">Ville<sup>*</sup></label>
              </div>
              <div class="form-group">
                <input class="formSubmit" name="phone" placeholder="Téléphone" type="tel" id="telephone"/>
                <label for="telephone">Téléphone mobile (facultatif)</label>
              </div>
              
              <input class="formSubmit optin" data-type="email" id="offreLafayetteMail" name="optin" type="checkbox" value="false">
              <label for="offreLafayetteMail">Je souhaite recevoir par email les offres des Galeries Lafayette*</label>
              
              <input class="formSubmit partneroptin" data-type="email" id="offrePartnerMail" name="partneroptin" type="checkbox" value="false">
              <label for="offrePartnerMail">Je souhaite recevoir par email les offres des partenaires des Galeries Lafayette*</label>
              
              <input class="button is-primary" id="register-button" type="submit" value="Participer">
              <div class="errormessage"></div>
              <p class="is-lighter">*champs obligatoires</p>
              <p class="is-lighter">Jeu gratuit sans obligation d'achat valable du mardi 1er au jeudi 24 décembre sur le site des <a href="https://www.galerieslafayette.com" class="is-text" target="_blank">galerieslafayette.com</a> et depuis le compte Instagram des Galeries Lafayette (@galerieslafayette) . Tirage au sort chaque jour du 2 au 25 décembre. Différents lots sont à gagner chaque jour, à retrouver sur <a href="https://www.galerieslafayette.com" class="is-text" target="_blank">galerieslafayette.com</a> et sur la page Instagram des Galeries Lafayette. Lot non cessible et non remboursable. La participation à ce jeu entraîne l'acceptation pure et simple du règlement du jeu disponible sur <a href="https://www.galerieslafayette.com" class="is-text" target="_blank">galerieslafayette.com</a>. Vous bénéficiez d’un droit d’accès, de rectification et d’effacement de vos données que vous pouvez exercer à tout moment soit en adressant un email à <a href="mailto:relaiscil@galerieslafayette.com" class="is-text">relaiscil@galerieslafayette.com</a> <a class="is-text" href="http://static.galerieslafayette.com/media/LP/src/pdf/2020/reglement-de-jeu-calendrier-de-l-avent-20201203.pdf" target="_blank">Règlement du jeu</a>.
              </p>
            </form>
          </div>
          <div class="form-success is-hidden">
            <div class="form-header"></div>
            <div class="text-center">
              <span class="h2 is-uppercase">Merci de votre <br />participation</span>
              <p>En attendant les résultats de notre tirage au sort, laissez-vous envahir par la magie du Voyage de Noël.</p>
            </div>
            <div class="form-body">
              <a class="button is-primary" href="https://www.galerieslafayette.com/evt/animations/noel/boutique-de-noel">Toutes nos sélections cadeaux</a>
              <figure class="image lutins">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/lutins-thankyou.png"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/lutins-thankyou@2x.png 2x" 
                  alt="Merci de votre participation - Galeries Lafayette" width="183" height="189">
              </figure>
            </div>
          </div>
          <div class="form-already-registered is-hidden">
            <div class="form-header"></div>
            <div class="text-center">
              <span class="h2 is-uppercase">Une fois mais pas deux !</span>
              <p>Vous avez déjà participé. Retentez votre chance demain, un nouveau cadeau est à la clé !</p>
            </div>
            <div class="form-body">
              <a class="button is-primary" href="https://www.galerieslafayette.com/evt/animations/noel/boutique-de-noel">Toutes nos sélections cadeaux</a>
              <figure class="image lutins">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/lutins-thankyou.png"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/calendrier-de-l-avent/lutins-thankyou@2x.png 2x" 
                  alt="Merci de votre participation - Galeries Lafayette" width="183" height="189">
              </figure>
            </div>
          </div>
          
        </div>
        <div class="form-footer"></div>
      </div> 
    </div>
  </section>
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/calendrier-de-l-avent.min.v02.js"></script>  
  
  
<script>  
  var date = new Date();
  
  if (date.getDate() > 24) {
    var randomDay = Math.floor(Math.random() * 24) + 1;
    date.setDate(randomDay);
    var currentDay = date.getDate();
  } else {
    var currentDay = date.getDate();
  }

  getGiftOfTheDay(currentDay);
</script>
<!--=========================== FIN LANDING PAGE ========================-->
  

  
<!-- build:js /media/LP/src/js/2020/calendrier-de-l-avent.min.v02.js
  <script src="../../../src/js/2020/calendrier-de-l-avent.js"></script>
<!-- endbuild -->
  
<?php include ('../../../pages-defaults/footer.php'); ?>
