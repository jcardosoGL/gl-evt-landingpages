<?php include ('../../../../pages-defaults/header.php'); ?>
<script>
  document.title = "Noël";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
  
<!-- <link href="../../../../../media/LP/src/css/2022/calendrier-de-l-avent.css" rel="stylesheet" type="text/css">   -->
  
<!-- =========================== LANDING PAGE ========================== -->  
 <link href="https://static.galerieslafayette.com/media/LP/src/css/2022/calendrier-de-l-avent.min.v01.css" rel="stylesheet" type="text/css">
<style>
  .main-nav {
    padding-top: 0 !important;
  }
  #toky_container,
  .header-info-banner-display {
    display: none !important; 
  }
</style>

<div class="grand-jeu-noel">
  <section class="calendrier-avent">
    <div class="welcome container">
      <div class="luxe-row no-gutter luxe-middle">
        
        <div class="luxe-col-mobile-12">
          <div class="article-body">
            <figure class="image title">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/calendrier-de-l-avent/logo-calendrier-de-l-avent.png"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/calendrier-de-l-avent/logo-calendrier-de-l-avent@2x.png 2x" 
                alt="Calendrier de l'avent - Galeries Lafayette" width="200" height="200">
            </figure>
            <p>Du 1er au 24 décembre, tentez de gagner chaque jour un des merveilleux cadeaux de&nbsp;notre calendrier de l'Avent.<sup>*</sup></p>
            <button class="button is-pink has-arrow trigger_anim">Découvrir le cadeau du jour</button>
          </div>
        </div>
        
        <figure class="image nordman">
          <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/calendrier-de-l-avent/nordman-guitar.png"
            srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/calendrier-de-l-avent/nordman-guitar@2x.png 2x" 
            alt="Planète Sapin Nordman - Galeries Lafayette" width="340" height="213">
        </figure>
      </div>
    </div>
    
    <div class="video-transition container is-hidden">
      <div class="bg-video-container">
        <video id="transition-vid" class="bg-video" poster="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/calendrier-de-l-avent/cover-video@2x.jpg" preload="auto"  muted playsinline>
          <source src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/calendrier-de-l-avent/video-intro-comp.mp4" type="video/mp4">
        </video>
      </div>
    </div>
    
    <div class="gifts container bg-gifts is-hidden">
      <div class="gifts luxe-row no-gutter luxe-middle text-left">        
        <div class="luxe-col-mobile-12">
          <figure class="image day-title">
            <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/calendrier-de-l-avent/days-title.png"
              srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/calendrier-de-l-avent/days-title@2x.png 2x" 
              alt="Calendrier de l'avent - Galeries Lafayette">
          </figure>
        </div>
        
        <div class="luxe-col-mobile-12">
          <div class="article-body">
            <div class="gift-display">
              
              <figure class="image text-center">
                <img class="product-img" src="" srcset="" alt="">
              </figure>
              <h2 class="title"><span class="brand is-uppercase"></span><span class="product"></span></h2>
              <p class="description"></p>
              <p class="value"></p>
              <button class="button is-pink trigger_form">Tenter sa chance</button>
            </div>
          </div>
        </div>
        
        <div class="luxe-col-mobile-12"></div>
      </div>
      
      <div class="form" style="display: none;">
        <div class="calendar-form-container">
          <div class="form-main">
            <div class="form-header text-right">
              <button class="close_form" aria-label="Fermer">Fermer</button>
            </div>
            <div class="text-center">
              <h2 class="is-uppercase">Inscrivez-vous à <br />notre tirage au sort</h2>
              <p>Remplissez les informations ci-dessous et tentez votre chance pour remporter le cadeau du jour.</p>
            </div>
            <form  class="text-left form-body" id="calendar-form" name="calendar-form">
              <div class="form-group">
                <select class="formSubmit" name="civilite" required="" id="civilite">
                    <option disabled="disabled" selected="selected" value="">Civilit&eacute;</option>
                    <option value="Monsieur">Monsieur</option>
                    <option value="Madame">Madame</option>
                </select>
                <label for="civilite">Civilité<sup>*</sup></label>
              </div>
              
              <div class="form-group">
                <input class="formSubmit" name="lastName" placeholder="Nom" required="" type="text" id="lastName"/>
                <label for="lastName">Nom<sup>*</sup></label>
              </div>
              <div class="form-group">
                <input class="formSubmit" name="firstName" placeholder="Prénom" required="" type="text" id="firstName"/>
                <label for="firstName">Prénom<sup>*</sup></label>
              </div>
              <div class="form-group">
                <input class="formSubmit" name="email" placeholder="Email" required="" type="email" id="email"/>
                <label for="email">Email<sup>*</sup></label>
              </div>
              <div class="form-group">
                <input class="formSubmit" name="country" placeholder="Pays" required="" type="text" id="country"/>
                <label for="country">Pays<sup>*</sup></label>
              </div>
              
              <input class="formSubmit optin" data-type="email" id="emailOptin" name="emailOptin" type="checkbox" value="ok">
              <label for="emailOptin">Je souhaite recevoir par email les offres des Galeries Lafayette*</label>
              
              <input class="formSubmit partneroptin" data-type="email" id="partnerOptin" name="partnerOptin" type="checkbox" value="ok">
              <label for="partnerOptin">Je souhaite recevoir par email les offres des partenaires des Galeries Lafayette*</label>
              
              <input class="button is-pink" id="register-button" name="registerButton" type="submit" value="Participer">
              <div class="errormessage"></div>
              <p class="is-lighter">*champs obligatoires</p>
              <p class="is-lighter">Jeu gratuit sans obligation d’achat du 1er au 24 décembre 2022 inclus sur <a href="https://www.galerieslafayette.com" class="is-text" target="_blank">galerieslafayette.com</a>. Réservé aux personnes majeures. Conditions et règlement du jeu <a href="https://static.galerieslafayette.com/media/LP/src/pdf/2022/reglement-jeu-calendrier-de-l-avent-2022.pdf" class="is-text" target="_blank">disponibles ici</a>. La participation à ce jeu entraîne l’acceptation pure et simple du règlement du jeu. A gagner : 24 lots d’une valeur minimum de 100€. Les gains sont non cessibles et non remboursables.</p>
              <p class="is-lighter">Vos données sont collectées par GALERIES LAFAYETTE MANAGEMENT, responsable du traitement, afin de permettre votre participation au jeu-concours et au tirage au sort et, si vous y avez consenti, vous adresser des offres commerciales pouvant prendre en compte vos centres d’intérêts.<br />
                Vos données sont destinées à la société GALERIES LAFAYETTE, aux Sociétés du Groupe Galeries Lafayette, ainsi qu’à leurs sous-traitants habilités dont certains peuvent être situés en dehors de l’Union Européenne. Ce partage de données entre les enseignes du groupe Galeries Lafayette est opéré afin notamment d’améliorer la connaissance client de nos enseignes, améliorer nos offres et votre satisfaction. Vous bénéficiez d’un droit d’accès de rectification et d’effacement de vos données que vous pouvez exercer à tout moment en adressant un email à l’adresse : <a href="mailto:relaiscil@galerieslafayette.com" class="is-text">relaiscil@galerieslafayette.com</a>. Pour parcourir dans le détail notre Politique de vie privée, <a href="https://www.galerieslafayette.com/service/service-confidence" class="is-text" target="_blank">cliquez ici</a>.
</p>
            </form>
          </div>
          <div class="form-success is-hidden">
            <div class="form-header"></div>
            <div class="text-center">
              <span class="h2 is-uppercase">Merci de votre <br />participation</span>
              <p>En attendant les résultats de notre tirage au sort, laissez-vous&nbsp;envahir par la magie du Voyage de Noël.</p>
            </div>
            <div class="form-body">
              <a class="button is-pink" href="https://www.galerieslafayette.com/h/noel">Toutes nos sélections cadeaux</a>
              <figure class="image lutins">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/calendrier-de-l-avent/nordman-et-annie.png"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/calendrier-de-l-avent/nordman-et-annie@2x.png 2x" 
                  alt="Merci de votre participation - Galeries Lafayette" width="239" height="360">
              </figure>
            </div>
          </div>
          <div class="form-already-registered is-hidden">
            <div class="form-header"></div>
            <div class="text-center">
              <span class="h2 is-uppercase">&nbsp;<br />Une fois mais pas deux !</span>
              <p>Vous avez déjà participé. Retentez votre chance demain, un nouveau cadeau est à la clé !</p>
            </div>
            <div class="form-body">
              <a class="button is-pink" href="https://www.galerieslafayette.com/h/noel">Toutes nos sélections cadeaux</a>
              <figure class="image lutins">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/calendrier-de-l-avent/nordman-et-annie.png"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/calendrier-de-l-avent/nordman-et-annie@2x.png 2x" 
                alt="Merci de votre participation - Galeries Lafayette" width="239" height="360">
              </figure>
            </div>
          </div>
          
        </div>
        <div class="form-footer"></div>
      </div> 
    </div>
  </section>
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2022/calendrier-de-l-avent.min.v01d.js"></script> 
   
<script>  
  var date = new Date();
  
  if (date.getDate() > 24) {
    var randomDay = Math.floor(Math.random() * 24) + 1;
    date.setDate(randomDay);
    var currentDay = date.getDate();
  } else {
    var currentDay = date.getDate();
  }
console.log(currentDay);
  getGiftOfTheDay(currentDay);
</script>
<!--=========================== FIN LANDING PAGE ========================-->
  

  
<!-- build:js /media/LP/src/js/2022/calendrier-de-l-avent.min.v01.js
  <script src="../../../../src/js/2022/calendrier-de-l-avent.js"></script>
 endbuild -->
  
<?php include ('../../../../pages-defaults/footer.php'); ?>
