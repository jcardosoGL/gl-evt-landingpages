<?php include ('../../../pages-defaults/header.php'); ?>
<script>
  document.title = "Noël";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
  
<!-- <link href="../../../../media/LP/src/css/2020/calendrier-de-l-avent.css" rel="stylesheet" type="text/css"> -->  
  
<!-- =========================== LANDING PAGE ========================== -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet">  
<div class="container py-5">
    <div class="row">
        <div class="col-lg-5 col-md-8 mx-auto shadow border bg-white p-4 rounded">
            <h2 class="text-center fw-bold mb-3">Contact Us</h2>
            <form name="google-sheet">
                <div id="form_alerts"></div>
                <div class="form-group mb-3">
                    <label for="firstName" class="form-label">Prénom</label>
                    <input type="text" id="firstName" name="firstName" class="form-control" placeholder="Enter your name" required>
                </div>
                <div class="form-group mb-3">
                    <label for="lastName" class="form-label">Nom</label>
                    <input type="text" id="lastName" name="lastName" class="form-control" placeholder="Enter your last name" required>
                </div>
                <div class="form-group mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" id="email" name="email" class="form-control" placeholder="Enter your email address" required>
                </div>                
                <div class="form-group mb-3">
                    <label for="adress" class="form-label">Adresse</label>
                    <input type="text" id="adress" name="adress" class="form-control" placeholder="Enter your address" required>
                </div>
                <div class="form-group mb-3">
                    <label for="zipCode" class="form-label">Code postal</label>
                    <input type="text" id="zipCode" name="zipCode" class="form-control" placeholder="Enter your zip code" required>
                </div>
                <div class="form-group mb-3">
                    <label for="city" class="form-label">Ville</label>
                    <input type="text" id="city" name="city" class="form-control" placeholder="Enter your city" required>
                </div>
                <div class="form-group mb-3">
                    <label for="telephone" class="form-label">Téléphone</label>
                    <input type="number" id="telephone" name="telephone" class="form-control" placeholder="Enter your phone number" required>
                </div>
                
                <div>
                    <button class="btn btn-primary me-2" type="submit">Send message!</button>
                    <button class="btn btn-danger" type="reset">Reset the form!</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
<script>
    const scriptURL = 'https://script.google.com/macros/s/AKfycbw-K68mVexsvsX1QoMMVfxwaogofiAV6Oyh_4R4Zz69wj1eWkS5qzpT1J2_0awK2Khu5Q/exec';
    const form = document.forms['google-sheet'];

    form.addEventListener('submit', e => {
        e.preventDefault();
        fetch(scriptURL, { method: 'POST', body: new FormData(form)})
        .then(response => $("#form_alerts").html("<div class='alert alert-success'>Contact message sent successfully.</div>"))
        .catch(error => $("#form_alerts").html("<div class='alert alert-danger'>Contact message not sent.</div>"))
    })
</script>
<!--=========================== FIN LANDING PAGE ========================-->
  

  
<!-- build:js /media/LP/src/js/2020/calendrier-de-l-avent.min.v02.js
  <script src="../../../src/js/2020/calendrier-de-l-avent.js"></script>
<!-- endbuild -->
  
<?php include ('../../../pages-defaults/footer.php'); ?>
