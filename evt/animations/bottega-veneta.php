<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Bottega Veneta";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../media/LP/src/css/2021/bottega-veneta.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/bottega-veneta.min.v04b.css" rel="stylesheet" type="text/css" />
<style>
  .back-top {
    display: none !important;
  }
  .gis-cta-logo {
    height: 65px !important;
    width: 65px !important; 
  }
  .gis-cta-indicator {
    height: 15px !important;
    width: 15px !important;
  }
  @media screen and (max-width: 1024px) {
    .gis-cta-logo {
      height: 60px !important;
      width: 60px !important; 
    }
  }
</style>
<div class="lp-container">
  
  <section class="section lp-hero">
    <div class="container">
      <div class="luxe-row no-gutter luxe-reverse luxe-middle-mobile luxe-center-desktop">
        <div class="luxe-col-mobile-12">
          <div class="content">
            <div class="bottega-logo"></div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-desktop-10">
          <div class="content main">
            <p><strong>Salon 02</strong></p>
            <p>La collection Salon 02 de Bottega Veneta, imaginée par son directeur artistique Daniel Lee, est une invitation à l'opulence et au mystère. C'est la subtile rencontre entre confort, exagération et excès. Avec l'idée que chaque pièce se complète et que la garde-robe prend son sens. Les accessoires sont les justes associés du prêt à porter pour une allure toujours plus glamour, toujours plus audacieuse.</p>
            <a href="#collection" class="has-smoothscroll button borderless inverted has-arrow-down">Découvrir la collection</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12">
          <div class="content">
            <div class="bottega-logo aligned-right"></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  
  <section class="section lp-body animated-article">
    <div class="container">
      <div class="luxe-row no-gutter luxe-middle-mobile text-center line-1">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-image left-1">
            <a href="https://www.galerieslafayette.com/b/bottega+veneta">
              <figure class="image">
                <img style="object-position: top;" 
                  src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/bottega-veneta-salon-02-detail-01.jpg"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/bottega-veneta-salon-02-detail-01@2x.jpg 2x"
                  width="720" height="1080"
                  alt="Bottega Veneta - Galeries Lafayette">
              </figure>
            </a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-image right-1">
            <a href="https://www.galerieslafayette.com/b/bottega+veneta">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/bottega-veneta-salon-02-detail-02.jpg"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/bottega-veneta-salon-02-detail-02@2x.jpg 2x"
                  width="720" height="1080"
                  alt="Bottega Veneta - Galeries Lafayette">
              </figure>
            </a>
          </div>
        </div>
      </div>
      <div class="luxe-row no-gutter luxe-middle-mobile text-center line-2">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-image left-2">
            <a href="https://www.galerieslafayette.com/b/bottega+veneta">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/bottega-veneta-salon-02-detail-03.jpg"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/bottega-veneta-salon-02-detail-03@2x.jpg 2x"
                  width="720" height="1080"
                  alt="Bottega Veneta - Galeries Lafayette">
              </figure>
            </a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-image right-2">
            <a href="https://www.galerieslafayette.com/b/bottega+veneta">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/bottega-veneta-salon-02-detail-04.jpg"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/bottega-veneta-salon-02-detail-04@2x.jpg 2x"
                  width="720" height="1080"
                  alt="Bottega Veneta - Galeries Lafayette">
              </figure>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  
  <section class="section lp-body mount-bag" id="collection">
    <div class="container">

      <div class="luxe-row no-gutter luxe-middle-mobile luxe-center-tablet text-center">
        <div class="luxe-col-mobile-12 luxe-col-tablet-10 luxe-col-widescreen-8 has-text-left-mobile">
          <div class="content mount-bag-title">
            <h2>Le Mount bag</h2>
            <p>Fonctionnel et unique avec sa chaîne et sa fermeture en V, le Mount Bag se décline en plusieurs coloris et plusieurs matériaux comme le cuir grainé ou le suede cachemire. Élégant, graphique, technique, le Mount Bag est la parfaite ponctuation des looks de la saison.</p>
            <a href="https://www.galerieslafayette.com/b/bottega+veneta/ct/femme-sacs+et+bagages" class="button outlined">Voir le Mount Bag<span class="icon has-arrow"></span></a>
          </div>
        </div>
        <div class="luxe-col-mobile-12">
          <div class="bags">
            <div class="article-image">
              <a href="https://www.galerieslafayette.com/b/bottega+veneta/ct/femme-sacs+et+bagages">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-1.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-1@2x.jpg 2x"
                    width="740" height="573"
                    alt="Le Mount bag - Galeries Lafayette">
                    <!-- Fallback for non JavaScript browsers -->
                    <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-1.jpg" alt="Le Mount bag - Galeries Lafayette" width="740" height="573" /></noscript>
                </figure>
              </a>
            </div>
            <div class="article-image">
              <a href="https://www.galerieslafayette.com/b/bottega+veneta/ct/femme-sacs+et+bagages">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-2.jpg"
                   srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-2@2x.jpg 2x"
                   width="740" height="573"
                    alt="Le Mount bag - Galeries Lafayette">
                    <!-- Fallback for non JavaScript browsers -->
                    <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-2.jpg" alt="Le Mount bag - Galeries Lafayette" width="740" height="573" /></noscript>
                </figure>
              </a>
            </div>
            <div class="article-image">
              <a href="https://www.galerieslafayette.com/b/bottega+veneta/ct/femme-sacs+et+bagages">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-3.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-3@2x.jpg 2x"
                    width="740" height="573"
                    alt="Le Mount bag - Galeries Lafayette">
                    <!-- Fallback for non JavaScript browsers -->
                    <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-3.jpg" alt="Le Mount bag - Galeries Lafayette" width="740" height="573" /></noscript>
                </figure>
              </a>
            </div>
            <div class="article-image">
              <a href="https://www.galerieslafayette.com/b/bottega+veneta/ct/femme-sacs+et+bagages">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-4.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-4@2x.jpg 2x"
                    width="740" height="573"
                    alt="Le Mount bag - Galeries Lafayette">
                    <!-- Fallback for non JavaScript browsers -->
                    <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-4.jpg" alt="Le Mount bag - Galeries Lafayette" width="740" height="573" /></noscript>
                </figure>
              </a>
            </div>
            <div class="article-image">
              <a href="https://www.galerieslafayette.com/b/bottega+veneta/ct/femme-sacs+et+bagages">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-1.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-1@2x.jpg 2x"
                    width="740" height="573"
                    alt="Le Mount bag - Galeries Lafayette">
                    <!-- Fallback for non JavaScript browsers -->
                    <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-1.jpg" alt="Le Mount bag - Galeries Lafayette" width="740" height="573" /></noscript>
                </figure>
              </a>
            </div>
            <div class="article-image">
              <a href="https://www.galerieslafayette.com/b/bottega+veneta/ct/femme-sacs+et+bagages">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-2.jpg"
                   srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-2@2x.jpg 2x"
                   width="740" height="573"
                    alt="Le Mount bag - Galeries Lafayette">
                    <!-- Fallback for non JavaScript browsers -->
                    <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/mount-bag-v2-2.jpg" alt="Le Mount bag - Galeries Lafayette" width="740" height="573" /></noscript>
                </figure>
              </a>
            </div>
          </div>
        </div>       
      </div>
      
    </div>
  </section>
  
  
  <section class="section lp-body products">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-mobile text-center">
        <div class="luxe-col-mobile-12">
          <div class="content">
            <h2>Les pièces phares</h2>
          </div>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sac+pochette+chain+pouch+en+peau+lainee-bottega+veneta/80893801/204">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/product_1.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/product_1@2x.jpg"
                  alt="Sac à dos Color Block - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/product_1.jpg" alt="Sac à dos Color Block - Galeries Lafayette" width="360" height="392" /></noscript>
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">BOTTEGA VENETA</p>
              <p>Sac pochette Chain Pouch en peau lainée</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/bottines+lug+en+cuir-bottega+veneta/80893794/320">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/product_2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/product_2@2x.jpg"
                  alt="T-shirt col rond Color Block - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/product_2.jpg" alt="T-shirt col rond Color Block - Galeries Lafayette" width="360" height="392" /></noscript>
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">BOTTEGA VENETA</p>
              <p>Bottines Lug en cuir</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sac+besace+padded+cassette+en+cuir+tresse-bottega+veneta/80893628/464?version=v2">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/product_3.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/product_3@2x.jpg"
                  alt="Débardeur col rond Color Block - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/product_3.jpg" alt="Débardeur col rond Color Block - Galeries Lafayette" width="360" height="392" /></noscript>
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">BOTTEGA VENETA</p>
              <p>Sac besace Padded Cassette en cuir tressé</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/sandales+a+talons+dot+en+cuir+et+plumes-bottega+veneta/80893550/384">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/product_4.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/product_4@2x.jpg"
                  alt="Hoodie Color Block - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/product_4.jpg" alt="Hoodie Color Block - Galeries Lafayette" width="360" height="392" /></noscript>
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">BOTTEGA VENETA</p>
              <p>Sandales à talons Dot en cuir et plumes</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-12">
          <div class="content">
            <a href="https://www.galerieslafayette.com/b/bottega+veneta" class="button outlined">Voir toute la sélection<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  
  <section class="section lp-body pop-up">
    <div class="container">

      <div class="luxe-row no-gutter luxe-reverse luxe-middle-mobile">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-image">
            <figure class="image">
              <img class="b-lazy" 
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/coupole-hsm.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/coupole-hsm@2x.jpg"
                alt="Coupole - Galeries Lafayette">
                <!-- Fallback for non JavaScript browsers -->
                <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/coupole-hsm.jpg" alt="Coupole - Galeries Lafayette" width="648" height="800" /></noscript>
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <div class="tag is-hidden">À Paris Haussmann</div>
            <h2>Un pop up à découvrir</h2>
            <p>Du 28 septembre au 26 octobre, le style avant-gardiste made in Italie s'invite aux Galeries Paris Lafayette Haussmann. Sur un pop-up dédié, les collections automne-hiver de la maison Bottega Veneta se déploient tout en radicalité.  L'ocassion de découvrir les nouveautés sacs et chaussures de la marque ainsi que des modèles en exclusivité.</p>
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/bottega-veneta" class="button outlined is-hidden">En savoir plus<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
      
    </div>
  </section>
  
  
  <section class="section lp-body">
    <div class="container">

      <div class="luxe-row no-gutter luxe-middle-mobile">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-image">
            <div class="read-more is-hidden">
              <button class="read-more-toggle"><span class="plus"></span></button>
              <div class="read-more-content hidden">
                <button class="read-less-toggle"><span class="cross"></span></button>
                
              </div>
            </div>
            <figure class="image">
              <img class="b-lazy" 
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/pret-a-porter.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/pret-a-porter@2x.jpg"
                alt="Le Shopping À Distance Bottega Veneta x Galeries Lafayette - Galeries Lafayette">
                <!-- Fallback for non JavaScript browsers -->
                <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/pret-a-porter.jpg" alt="Le Shopping À Distance Bottega Veneta x Galeries Lafayette - Galeries Lafayette" width="720" height="800" /></noscript>
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <h2>Le prêt-à-porter en Shopping à Distance</h2>
            <p>À l’occasion du pop-up Bottega Veneta, les Galeries Lafayette ont le plaisir de vous proposer un service de Shopping à Distance. Vous pouvez ainsi réaliser votre shopping où que vous soyez et bénéficier de conseils et d’un accompagnement personnalisé.</p>
            <p class="is-smaller">Service disponible par rendez-vous, du lundi au samedi, 11h-17h</p>
            <a href="" class="button outlined" onclick="initSAD(); return false;">Acheter en Shopping à Distance<span class="icon has-arrow"></span></a>
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance" class="is-text is-tiny is-block">En savoir plus sur le service de Shopping À Distance</a>
          </div>
        </div>
      </div>
      
    </div>
  </section>
  
  
  <section class="section lp-body">
    <div class="container">
      <div class="luxe-row no-gutter text-center">
        <div class="luxe-col-mobile-12">
          <a href="https://www.galerieslafayette.com/b/bottega+veneta" class="collection-cta">Tout l'univers Bottega&nbsp;Veneta<span class="arrow"></span></a> 
        </div>
      </div>
    </div>
  </section>
  
  
  <section class="section section-video bottom">
    <div class="video-container">
      <div class="desktop-vid is-hidden-mobile">
        <video class="bg-video" preload="auto" autoplay loop playsinline muted>
          <source src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/bottega-veneta_D.mp4" type="video/mp4">
        </video>
      </div>
      <div class="mobile-vid is-hidden-tablet">
        <video class="bg-video" preload="auto" autoplay loop playsinline muted>
          <source src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/bottega-veneta_M.mp4" type="video/mp4">
        </video>
      </div>
    </div>
  </section>
  
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2021/bottega-veneta.min.v04.js"></script>

<div id="goinstore_modals" style="height:0;"></div>
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/shoppingadistance-goinstore-v2.min.css" rel="stylesheet" type="text/css">
<script src="https://static.galerieslafayette.com/media/LP/src/js/2021/shoppingadistance-goinstore-v2.min.v03.js"></script>

<script>
  var routingKey = "brand5";
  var clickedBrand='Bottega Veneta'; 
  var cleanBrandName='bottega-veneta-v2'; 
  function initSAD() {
    clickedBrand; 
    cleanBrandName; 
    appointedWidgetId='5f8878506a0d547bea7db8e8'; 
    GL_GIS.LiveShopping.displaySadBrand();  
    GL_GIS.LiveShopping.checkPopinClerkAvailability(routingKey);
  }

  window.onload = function() {
    GL_GIS.LiveShopping.init(routingKey);    
    
    var gisCTA = document.getElementById('gis-cta');
    if (gisCTA) {
      gisCTA.addEventListener('click', function(e) {
        e.stopPropagation();
        initSAD();
        GL_GIS.LiveShopping.displaySadBrand();
        return false;  
      }, true);
    }
    
    var gisMsgContainer = document.getElementById('gis-awareness-msg-container');
    if (gisMsgContainer) {
      gisMsgContainer.lastChild.addEventListener('click', function(e) { 
        e.stopPropagation();
        initSAD();
        GL_GIS.LiveShopping.displaySadBrand();
        return false;
      }, true);
    }
  }; 
</script>

<!--=========================== FIN LANDING PAGE ========================-->
    
<!-- build:js /media/LP/src/js/2021/bottega-veneta.min.v03.js
  <script src="../../assets/js/blazy.min.js"></script>
  <script src="../../assets/js/ScrollMagic.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
  <script src="../src/js/2021/bottega-veneta.js"></script>
<!-- endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
