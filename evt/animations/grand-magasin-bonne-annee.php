<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Grand Magasin Bonne Année";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
<!-- <link href="https://static.galerieslafayette.com/media/LP/src/html/test/grand-magasin-de-tous/grand-magasin-de-tous.css" rel="stylesheet" type="text/css" /> -->
<!-- <link href="../../media/LP/src/css/2022/grand-magasin-bonne-annee-anims.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->
<link href="https://static.galerieslafayette.com/media/LP/src/html/test/grand-magasin-de-tous/grand-magasin-de-tous.css" rel="stylesheet" type="text/css" />

 <link href="https://static.galerieslafayette.com/media/LP/src/css/2022/grand-magasin-bonne-annee-anims.min.v03.css" rel="stylesheet" type="text/css" />

  
<style type="text/css">
  footer.footer,
  #toky_container,
  .header-info-banner-display,
  .ab684263 {
    display: none !important;
  }
  .ab682943--first-banner,
  .ab684263--first-banner {
    margin-top: 0 !important;
  }
</style>

<section class="great-store-of-all-container">

  <div class="great-store-of-all">
    <a href="https://www.galerieslafayette.com/" class="button back-button is-fixed">accéder à galerieslafayette.com</a>
    
    <div class="div-fixed arrow">
       <a class="button-anchor_b has-smoothscroll" href="#a-quoi-je-reve">
         <svg fill="none" height="24" viewBox="0 0 20 24" width="20" xmlns="http://www.w3.org/2000/svg">                        <path d="M 10 23 C 5.02944 23 1 18.9706 1 14 C 1 9.02944 5.02944 5 10 5 C 14.9706 5 19 9.02944 19 14 C 19 18.9706 14.9706 23 10 23 Z" stroke-width="2">                        </path> <path d="M 15 12.75 L 10 16.5 L 5 12.75" stroke-linejoin="round" stroke-width="2"></path>                   </svg>
       </a>
     </div>
    
     <div class="intro reveal" id="intro">      
        
        <div class="hero_logo">
          <a href="https://www.galerieslafayette.com/">
            <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/animations/grand-magasin-de-tous/logo-gl-yellow.png"></a>
        </div>
         
        <div class="hero-image-container">
           <div class="hero_M hero-image">&nbsp;</div>
           <div class="hero_D hero-image">&nbsp;</div>
        </div>

         

          <h1 class="content-cartouches">
             <span class="cartouche-01 reveal-1">LE</span>
             <span class="cartouche-02 reveal-2">GRAND</span>
             <span class="cartouche-03 reveal-3">MAGASIN</span>
             <span class="cartouche-04 reveal-4">VOUS</span>
             <span class="cartouche-05 reveal-5">SOUHAITE</span>
             <span class="cartouche-06 reveal-6">UNE</span>
             <span class="cartouche-07 reveal-7">GRANDE</span>
             <span class="cartouche-08 reveal-8">ANNÉE</span>
         </h1>
         
         
         
      </div>
 
       <div class="part02">
          <div class="videoContainer small-video01 show-for-large-up">            
             <div class="video_wrapper"> 
                 <div style="padding:56.25% 0 0 0;position:relative;">                 
                     <iframe class="" src="https://player.vimeo.com/video/739907631?h=4b2580a638?autoplay=1&muted=1&background=1" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                 </div>
             </div>
          </div>
           
          <div class="videoContainer small-video02" id="a-quoi-je-reve">            
              <div class="video_wrapper"> 
                  <div style="padding:56.25% 0 0 0;position:relative;">                 
                      <iframe class="" src="https://player.vimeo.com/video/739906479?h=010891a196?autoplay=1&muted=1&background=1" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                  </div>
              </div>
          </div>
          
          <div class="txt-container txt02">
            <p>Pour 2023, les Galeries Lafayette vous souhaitent de réaliser tout ce à quoi vous rêvez</p>
          </div>
          
           <div class="videoContainer small-video03">            
              <div class="video_wrapper"> 
                  <div style="padding:56.25% 0 0 0;position:relative;">                 
                      <iframe class="" src="https://player.vimeo.com/video/739907073?h=a4271f3073?autoplay=1&muted=1&background=1" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                  </div>
              </div>
           </div>
          
          <div class="videoContainer small-video04 show-for-large-up">            
              <div class="video_wrapper"> 
                  <div style="padding:56.25% 0 0 0;position:relative;">                 
                      <iframe class="" src="https://player.vimeo.com/video/739908198?h=0784aa4568?autoplay=1&muted=1&background=1" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                  </div>
              </div>
          </div>
          
          <a href="#le-film" class="button is-yellow has-smoothscroll">Voir le film</a>
          
       </div>
  
      
       <div class="part03">
        <div class="le-film" id="le-film"></div>
         <h2>le film</h2>
         
         <div class="videoContainer">            
              <div class="video_wrapper"> 
                  <div class="video_M" style="padding:112.25% 0 0 0;position:relative;">                 
                      <iframe class="" src="https://player.vimeo.com/video/741067556?h=c7dc1a8889" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                  </div>
                  <div class="video_T" style="padding:56.25% 0 0 0;position:relative;">
                    <iframe class="" src="https://player.vimeo.com/video/741062356?h=2aafe4d8df" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                  </div>
                  <div class="video_D">
                    <iframe class="" src="https://player.vimeo.com/video/741062356?h=2aafe4d8df" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                  </div>
             </div>
         </div>
                 
         <span class="mention">Xavier Veilhan - Light Machine, 2022 © ADAGP Paris (Courtesy Galerie Perrotin)</span>
      </div>  

 
       <div class="part04 is-hidden">
          <h2>L'hiver au beau fixe</h2>
                      <ul class="wrapper reveal">
                          <li class="reveal-3 visuel-title">
                           <a href="https://www.galerieslafayette.com/h/femme">
                             <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/grand-magasin-bonne-annee/look01.jpg" alt="Cadeaux pour elle">
                              <span>mode femme</span>
                           </a>
                         </li>
                          <li class="reveal-4 visuel-title">
                           <a href="https://www.galerieslafayette.com/h/createurs">
                             <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/grand-magasin-bonne-annee/look02.jpg" alt="Cadeaux luxe et créateurs">
                              <span>luxe et créateurs</span>
                           </a>
                         </li>
                          <li class="reveal-5 visuel-title">
                           <a href="https://www.galerieslafayette.com/h/homme">
                             <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/grand-magasin-bonne-annee/look03.jpg" alt="Cadeaux pour lui">
                              <span>mode homme</span>
                           </a>
                         </li>
                     </ul>     
                </div> 
     </div> 
    
</section>

<script src="https://static.galerieslafayette.com/media/LP/src/js/assets/scrollmagic/scrollmagic-complete.min.js"></script>
<script src="https://static.galerieslafayette.com/media/LP/src/js/2022/grand-magasin-bonne-annee.min.v02.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->

  
<!-- build:js /media/LP/src/js/2022/grand-magasin-bonne-annee.min.v02.js
  <script src="../../assets/js/ScrollMagic.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
  <script src="../src/js/2022/grand-magasin-bonne-annee.js"></script>
endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
