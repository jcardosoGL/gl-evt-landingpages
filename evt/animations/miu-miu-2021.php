<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Miu-Miu";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../media/LP/src/css/2021/miu-miu-2021.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/miu-miu-2021.min.v03b.css" rel="stylesheet" type="text/css" />
<div class="lp-container">
  <section class="section lp-hero">
    <div class="container is-fullheight">
      <div class="luxe-row no-gutter is-fullheight text-left luxe-bottom-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-5 luxe-col-desktop-7">          
          <h1 class="">
            <span>Miu</span>
            <span>Miu</span>
            <span>Nuit</span>
          </h1>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-7 luxe-col-desktop-5">
          <p class="is-uppercase">Depuis 1993, la maison Miu Miu célèbre les femmes à travers des collections originales, fantaisistes et colorées. Cet automne, la maison italienne arrive sur GALERIESLAFAYETTE.com et installe sa collection Miu Miu Nuit sur un pop exclusif aux Galeries Lafayette Paris Haussmann. Et Noël n’a jamais été aussi magique&nbsp;!
          </p>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body marque">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-tablet">
        <div class="luxe-col-mobile-12">
          <div class="content text-left">
            <h2 class="boxed-text">
              <span>Miu Miu,</span>
              <span>une maison</span> <br />
              <span>emblématique</span>
            </h2>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-10 luxe-col-desktop-8">
          <div class="content text-left">
            <p>Depuis 1993 Miu Miu est la représentation la plus débridée de la créativité de Miuccia Prada. Loin des images esthétiques traditionnelles, la marque exprime l’essence d’une femme émancipée et consciente. Miu Miu est un atelier de nouvelles expressions, de design qui joue avec la nature kaléidoscopique de la mode. Charmante et indisciplinée, la Maison allie une élégance naïve et sophistiquée avec une âme jeune, non-conformiste et insouciante.</p>
            <a href="https://www.galerieslafayette.com/b/miu+miu" class="is-bold alt-button">Découvrir la marque<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body visus">
    <div class="container">
      <div class="luxe-row no-gutter text-center">
        <div class="luxe-col-mobile-6 luxe-col-tablet-3">
          <div class="article-image">
            <figure class="image">
              <img class=""
                src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/emblematique_01.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/emblematique_01@2x.jpg 2x"
                width="723" height="471"
                alt="Miu Miu - Galeries Lafayette">
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3">
          <a href="https://www.galerieslafayette.com/p/sac+miu+confidential+en+cuir+matelasse-miu+miu/81664236/320">
            <span class="plus"></span>
            <div class="article-image">
              <figure class="image">
                <img class=""
                  src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/emblematique_02.jpg"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/emblematique_02@2x.jpg 2x"
                  width="723" height="471"
                  alt="Miu Miu - Galeries Lafayette">
              </figure>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3">
          <a href="https://www.galerieslafayette.com/p/barrette+avec+cristaux-miu+miu/81664639/306">
            <span class="plus"></span>
            <div class="article-image">
              <figure class="image">
                <img class=""
                  src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/emblematique_03.jpg"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/emblematique_03@2x.jpg 2x"
                  width="723" height="471"
                  alt="Miu Miu - Galeries Lafayette">
              </figure>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3">
          <a href="https://www.galerieslafayette.com/p/sac+miu+crystal+en+cuir-miu+miu/81663944/355">
            <span class="plus"></span>
            <div class="article-image">
              <figure class="image">
                <img class=""
                  src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/emblematique_04.jpg"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/emblematique_04@2x.jpg 2x"
                  width="723" height="471"
                  alt="Miu Miu - Galeries Lafayette">
              </figure>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body collection-nuit">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-tablet">
        <div class="luxe-col-mobile-12">
          <div class="content text-left">
            <h2 class="boxed-text">
              <span>La collection du soir</span>
              <span>Miu Miu Nuit</span>
            </h2>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-10 luxe-col-desktop-8">
          <div class="content text-left">
            <p>Cette saison, Miu Miu fait rimer mode et esprit de fête. A travers sa nouvelle collection Miu Miu Nuit, la maison italienne dévoile une série de pièces féminines et ultra désirables qui se parent de détails luxueux et joyeux tels que des cristaux brillants. Tissus élégants, couleurs délicates et volants romantiques composent cette collection sensuelle qui puise également ses inspirations dans le vestiaire de fête masculin. Pour une allure rock et androgyne d’oiseau de nuit.</p>
            <a href="https://www.galerieslafayette.com/b/miu+miu" class="is-bold alt-button">Découvrir la collection<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
    
  <section class="section lp-body products">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-tablet">
        <div class="luxe-col-mobile-12">
          <div class="content">
            <h2 class="boxed-text">
              <span>Les pièces phares</span>
            </h2>
          </div>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/mini-robe+en+maille+punto+stoffa-miu+miu/81663891/320">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img 
                  src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/robe-cady.jpg"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/robe-cady@2x.jpg 2x"
                  alt="Miu Miu : Mini-robe en maille - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Miu Miu</p>
              <p>Mini-robe en maille</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/portefeuille+en+cuir+matelasse+a+bandouliere-miu+miu/81663758/2893">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img 
                  src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/portefeuille-cuir-matelasse.jpg"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/portefeuille-cuir-matelasse@2x.jpg 2x"
                  alt="Miu Miu : Portefeuille en cuir matelassé à bandoulière - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Miu Miu</p>
              <p>Portefeuille en cuir matelassé à bandoulière</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/escarpins+a+bride+arriere+en+cuir+verni-miu+miu/81664319/320">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img 
                  src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/escarpins-cuir-verni.jpg"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/escarpins-cuir-verni@2x.jpg 2x"
                  alt="Miu Miu : Escarpins à bride arrière en cuir verni - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Miu Miu</p>
              <p>Escarpins à bride arrière en cuir verni</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3 product">
          <a href="https://www.galerieslafayette.com/p/boucles+d+oreilles+longues+ornees+de+cristaux-miu+miu/81664075/38">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img 
                  src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/boucles-doreilles.jpg"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/boucles-doreilles@2x.jpg 2x"
                  alt="Miu Miu : Boucles d’oreilles ornées de cristaux - Galeries Lafayette">
              </figure>
            </div>
            <div class="product-txt">
              <p class="product-title is-uppercase is-bold">Miu Miu</p>
              <p>Boucles d’oreilles ornées de cristaux</p>
            </div>
          </a>
        </div>
        <div class="luxe-col-mobile-12">
          <div class="content">
            <a href="https://www.galerieslafayette.com/b/miu+miu" class="button primary outlined">Voir toute la sélection<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
    
  <section class="section lp-body pop-up">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-tablet">
        <div class="luxe-col-mobile-12">
          <div class="content text-left">
            <h2 class="boxed-text main">
              <span>Un Pop Up</span>
              <span>Exclusif</span>
            </h2>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-10 luxe-col-desktop-8">
          <div class="content text-left">
            <div class="article-image">
              <figure class="image">
                <img class="is-hidden-tablet is-fullwidth"
                  src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/coupole_M.jpg"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/coupole_M@2x.jpg 2x"
                  width="330" height="476"
                  alt="Coupole Haussmann - Galeries Lafayette">
                <img class="is-hidden-mobile is-fullwidth"
                  src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/coupole_D.jpg"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/miu-miu-2021/coupole_D@2x.jpg 2x"
                  width="724" height="476"
                  alt="Coupole Haussmann - Galeries Lafayette">
              </figure>
            </div>
            <p>Du 17 novembre au 14 décembre Miu Miu a le plaisir de présenter «&nbsp;Miu Miu Nuit&nbsp;» une installation exclusive en collaboration avec les Galeries Lafayette Paris Haussmann. Le grand magasin parisien ouvre un pop-up dédié aux accessoires de la collection Miu Miu Nuit. Retrouvez en exclusivité et en avant-première une sélection de sacs de la maison italienne mais aussi toute une panoplie d’accessoires et de bijoux.</p>
          </div>
        </div>
        <div class="luxe-col-mobile-12 univers">
          <a href="https://www.galerieslafayette.com/b/miu+miu" class="univers-cta">
            <h2 class="boxed-text">
              <span>tout l'univers <span class="is-hidden-mobile">Miu Miu</span></span>
              <span class="is-hidden-tablet">Miu Miu<br  /></span>
              <span>↗︎</span>
            </h2>
          </a> 
        </div>
      </div>
    </div>
  </section>

</div>
<!--=========================== FIN LANDING PAGE ========================-->

<!-- <script src="https://static.galerieslafayette.com/media/LP/src/js/2021/pangaia.min.v01.js"></script> -->
  
    
  <!-- build:js /media/LP/src/js/2021/miu-miu-2021.min.v00.js
    <script src="../src/js/2021/miu-miu-2021.js"></script>
  <!-- endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
