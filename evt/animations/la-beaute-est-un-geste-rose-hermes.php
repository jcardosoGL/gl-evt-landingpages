<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Hermès rose";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../media/LP/src/css/2021/la-beaute-est-un-geste-rose-hermes.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/la-beaute-est-un-geste-rose-hermes.min.v01.css" rel="stylesheet" type="text/css" />
<div class="hermes">
  <section class="hermes-hero">
    <div class="container">
      <div class="luxe-row no-gutter luxe-reverse luxe-middle-mobile luxe-center-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="hero-image"></div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <div class="date">Beauté — 26.04.21</div>
            <h1>Hermès, la beauté est un&nbsp;geste</h1>
            <p>Rose Hermès est le second chapitre de la beauté Hermès. La collection se compose de huit nuances de fards à joues Rose Hermès Silky Blush, de deux pinceaux, et de trois Roses à lèvres embellisseurs naturels. Une nouvelle collection d'objets de beauté rechargeables et durables. </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  
  <section class="hermes-body edito-main">
    <div class="container">
      
      <div class="luxe-row no-gutter luxe-middle-mobile luxe-center-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-8 luxe-col-desktop-7 luxe-col-widescreen-9 luxe-col-fullhd-8">
          <div class="content">
            <h2>Hermès, une histoire de&nbsp;roses</h2>
            <p>Après Rouge Hermès, geste fondateur du métier de la Beauté Hermès, voici Rose Hermès, un second chapitre tout en douceur. Quand Rouge Hermès est une couleur contenue en un trait, Rose Hermès est la couleur d’une présence au monde sensible et radieuse.</p>
          </div>
        </div>
      </div>

    </div>
  </section>
  
  
  <section class="hermes-body edito">
    <div class="container">

      <div class="luxe-row no-gutter luxe-middle-mobile luxe-center-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-11 luxe-col-desktop-10 luxe-col-widescreen-9">
          <div class="article-image">
            <figure class="image is-hidden-desktop">
              <img class="b-lazy" 
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/fard-a-joues-edito_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/fard-a-joues-edito_M@2x.jpg"
                alt="Fard à joues Silky Blush Hermès - Galeries Lafayette" width="728" height="458">
                <!-- Fallback for non JavaScript browsers -->
                <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/fard-a-joues-edito_M.jpg" alt="Fard à joues Silky Blush Hermès - Galeries Lafayette" width="728" height="458" /></noscript>
            </figure>
            <figure class="image is-hidden-touch">
              <img class="b-lazy" 
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/fard-a-joues-edito_D.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/fard-a-joues-edito_D@2x.jpg"
                alt="Fard à joues Silky Blush Hermès - Galeries Lafayette" width="960" height="538">
                <!-- Fallback for non JavaScript browsers -->
                <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/fard-a-joues-edito_D.jpg" alt="Fard à joues Silky Blush Hermès - Galeries Lafayette" width="960" height="538" /></noscript>
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-10 luxe-col-desktop-9">
          <div class="content">
            <h3>Un objet galet, geste de lumière</h3>
            <p>Avec son boîtier circulaire, le fard à joues Rose Hermès Silky Blush est un galet de lumière blanc et or imaginé par le créateur Pierre Hardy. En rehaussant les traits du visage, les huit teintes Rose Hermès Silky Blush agissent instantanément comme un révélateur de personnalité, une touche soyeuse qui fait rayonner la lumière intérieure.</p>
          </div>
        </div>
      </div>
      
    </div>
  </section>
  
  
  <!-- PRODUCTS -->
  <section class="hermes-body products fard-a-joues">
    <div class="container">
      
      <div class="luxe-row no-gutter luxe-center-mobile">
        <div class="luxe-col-mobile-12">
          <div class="luxe-row no-gutter">
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/rose+hermes+fard+a+joues+silky+blush-hermes/76601499/212011" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-abricot-19.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-abricot-19@2x.jpg"
                      alt="Hermès Fard à joues Silky Blush, 19 Rose Abricot - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-abricot-19.jpg" alt="Hermès Fard à joues Silky Blush, 19 Rose Abricot - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">Hermès</p>
                  <p>Fard à joues Silky Blush, 19 Rose <br class="is-hidden-tablet" />Abricot<br />
                    Rose Hermès</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/rose+hermes+fard+a+joues+silky+blush-hermes/76601499/212015" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-blush-23.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-blush-23@2x.jpg"
                      alt="Hermès Fard à joues Silky Blush, 23 Rose Blush - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-blush-23.jpg" alt="Hermès Fard à joues Silky Blush, 23 Rose Blush - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">Hermès</p>
                  <p>Fard à joues Silky Blush, 23 Rose <br class="is-hidden-tablet" />Blush<br />
                    Rose Hermès</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/rose+hermes+fard+a+joues+silky+blush-hermes/76601499/212014" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-plume-28.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-plume-28@2x.jpg"
                      alt="Hermès Fard à joues Silky Blush, 28 Rose Plume - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-plume-28.jpg" alt="Hermès Fard à joues Silky Blush, 28 Rose Plume - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">Hermès</p>
                  <p>Fard à joues Silky Blush, 28 Rose <br class="is-hidden-tablet" />Plume<br />
                    Rose Hermès</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/rose+hermes+fard+a+joues+silky+blush-hermes/76601499/212013" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-pommette-32.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-pommette-32@2x.jpg"
                      alt="Hermès Fard à joues Silky Blush, 32 Rose Pommette - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-pommette-32.jpg" alt="Hermès Fard à joues Silky Blush, 32 Rose Pommette - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">Hermès</p>
                  <p>Fard à joues Silky Blush, 32 Rose <br class="is-hidden-tablet" />Pommette<br />
                    Rose Hermès</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/rose+hermes+fard+a+joues+silky+blush-hermes/76601499/212012" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-ombre-45.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-ombre-45@2x.jpg"
                      alt="Hermès Fard à joues Silky Blush, 45 Rose Ombré - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-ombre-45.jpg" alt="Hermès Fard à joues Silky Blush, 45 Rose Ombré - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">Hermès</p>
                  <p>Fard à joues Silky Blush, 45 Rose <br class="is-hidden-tablet" />Ombré<br />
                    Rose Hermès</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/rose+hermes+fard+a+joues+silky+blush-hermes/76601499/212016" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-tan-49.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-tan-49@2x.jpg"
                      alt="Hermès Fard à joues Silky Blush, 49 Rose Tan - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-tan-49.jpg" alt="Hermès Fard à joues Silky Blush, 49 Rose Tan - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">Hermès</p>
                  <p>Fard à joues Silky Blush, 49 Rose <br class="is-hidden-tablet" />Tan (épuisé)<br />
                    Rose Hermès</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/rose+hermes+fard+a+joues+silky+blush-hermes/76601499/212010" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-nuit-54.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-nuit-54@2x.jpg"
                      alt="Hermès Fard à joues Silky Blush, 54 Rose Nuit - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-nuit-54.jpg" alt="Hermès Fard à joues Silky Blush, 54 Rose Nuit - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">Hermès</p>
                  <p>Fard à joues Silky Blush, 54 Rose <br class="is-hidden-tablet" />Nuit<br />
                    Rose Hermès</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/rose+hermes+fard+a+joues+silky+blush-hermes/76601499/212009" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-feu-61.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-feu-61@2x.jpg"
                      alt="Hermès Fard à joues Silky Blush, 61 Rose Feu - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-fard-a-joues-silky-blush-rose-feu-61.jpg" alt="Hermès Fard à joues Silky Blush, 61 Rose Feu - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">Hermès</p>
                  <p>Fard à joues Silky Blush, 61 Rose <br class="is-hidden-tablet" />Feu<br />
                    Rose Hermès</p>
                </div>
              </a>
            </div>
            
          </div>
        </div>
        
        <div class="luxe-col-mobile-12">
          <a href="https://www.galerieslafayette.com/p/rose+hermes+fard+a+joues+silky+blush-hermes/76601499/212013" class="button outlined">Voir toutes les teintes<span class="icon has-arrow"></span></a>
        </div>
        
      </div>
    </div>
  </section>
  
  
  <section class="hermes-body edito">
    <div class="container">

      <div class="luxe-row no-gutter luxe-middle-mobile luxe-center-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-image article-left">
            <figure class="image is-hidden-tablet">
              <img class="b-lazy" 
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-roses-a-levres-1_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-roses-a-levres-1_M@2x.jpg"
                alt="Le rose à lèvres Hermès - Galeries Lafayette" width="728" height="458">
                <!-- Fallback for non JavaScript browsers -->
                <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-roses-a-levres-1_M.jpg" alt="Le rose à lèvres Hermès - Galeries Lafayette" width="728" height="970" /></noscript>
            </figure>
            <figure class="image is-hidden-mobile">
              <img class="b-lazy" 
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-roses-a-levres-1_D.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-roses-a-levres-1_D@2x.jpg"
                alt="Le rose à lèvres Hermès - Galeries Lafayette" width="640" height="852">
                <!-- Fallback for non JavaScript browsers -->
                <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-roses-a-levres-1_D.jpg" alt="Le rose à lèvres Hermès - Galeries Lafayette" width="640" height="852" /></noscript>
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 is-hidden-mobile">
          <div class="article-image article-right">
            <figure class="image is-hidden-mobile">
              <img class="b-lazy" 
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-roses-a-levres-2_D.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-roses-a-levres-2_D@2x.jpg"
                alt="Le rose à lèvres Hermès - Galeries Lafayette" width="640" height="852">
                <!-- Fallback for non JavaScript browsers -->
                <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-roses-a-levres-2_D.jpg" alt="Le rose à lèvres Hermès - Galeries Lafayette" width="640" height="852" /></noscript>
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-10 luxe-col-desktop-9">
          <div class="content">
            <h3>Le rose à lèvres, objet de beauté</h3>
            <p>Rose Abricoté, Rose d’Été et Rose Tan sont les couleurs des trois roses à lèvres embellisseurs naturels de la maison Hermès. Chaque rose est un halo léger et translucide qui révèle la beauté des lèvres au naturel. Ce rose à lèvres durable, poli, brossé et protégé par un pochon en toile est un objet Hermès par excellence. </p>
          </div>
        </div>
      </div>
      
    </div>
  </section>
  
  
  <!-- PRODUCTS -->
  <section class="hermes-body products fard-a-joues">
    <div class="container">
      
      <div class="luxe-row no-gutter luxe-center-mobile">
        <div class="luxe-col-mobile-12">
          <div class="luxe-row no-gutter luxe-around-tablet">
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/rose+hermes+rose+a+levres+embellisseur+rose+abricote-hermes/76601527/210001" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-rose-a-levres-rose-d-ete-30.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-rose-a-levres-rose-d-ete-30@2x.jpg"
                      alt="Hermès Rose à lèvres embellisseur, 30 Rose d'Été - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-rose-a-levres-rose-d-ete-30.jpg" alt="Hermès Rose à lèvres embellisseur, 30 Rose d'Été - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">Hermès</p>
                  <p>Rose à lèvres embellisseur, 30 Rose <br class="is-hidden-tablet" />d’Été<br />
                    Rose Hermès</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/rose+hermes+rose+a+levres+embellisseur+rose+abricote-hermes/76601527/210002" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-rose-a-levres-rose-tan-49.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-rose-a-levres-rose-tan-49@2x.jpg"
                      alt="Hermès Rose à lèvres embellisseur, 49 Rose Tan - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-rose-a-levres-rose-tan-49.jpg" alt="Hermès Rose à lèvres embellisseur, 49 Rose Tan - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">Hermès</p>
                  <p>Rose à lèvres embellisseur, 49 Rose <br class="is-hidden-tablet" />Tan<br />
                    Rose Hermès</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/rose+hermes+rose+a+levres+embellisseur+rose+abricote-hermes/76601527/210003" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-rose-a-levres-rose-abricote-14.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-rose-a-levres-rose-abricote-14@2x.jpg"
                      alt="Hermès Rose à lèvres embellisseur, 14 Rose Abricoté - Galeries Lafayette" width="360" height="392">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/hermes-rose/rose-hermes-rose-a-levres-rose-abricote-14.jpg" alt="Hermès Rose à lèvres embellisseur, 14 Rose Abricoté - Galeries Lafayette" width="360" height="392" /></noscript>
                  </figure>
                </div>
                <div class="luxe-middle-mobile product-txt">
                  <p class="is-bold is-uppercase">Hermès</p>
                  <p>Rose à lèvres embellisseur, 14 Rose <br class="is-hidden-tablet" />Abricoté<br />
                    Rose Hermès</p>
                </div>
              </a>
            </div>
          </div>
        </div>
        
        <div class="luxe-col-mobile-12">
          <a href="https://www.galerieslafayette.com/p/rose+hermes+rose+a+levres+embellisseur+rose+abricote-hermes/76601527/210002" class="button outlined">Voir tous les rouges à lèvres<span class="icon has-arrow"></span></a>
        </div>
        
      </div>
    </div>
  </section>
    
    
  <section class="hermes-body last-section">
    <div class="container">

      <div class="luxe-row no-gutter luxe-middle-mobile luxe-center-mobile">
        <div class="luxe-col-mobile-12 luxe-col-tablet-10 luxe-col-desktop-9">
          <div class="content">
            <h3>Tout l’univers Hermès&nbsp;Beauté</h3>
            <a href="https://www.galerieslafayette.com/b/hermes" class="button primary">Découvrir<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
      
    </div>
  </section>
  
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/blazy.min.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->
  
<!-- build:js /media/LP/src/js/2021/prada-collection-ss21.min.v01.js 
  <script src="../../assets/js/blazy.min.js"></script>
  <script src="../src/js/2021/prada-collection-ss21.js"></script>
<!-- endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
