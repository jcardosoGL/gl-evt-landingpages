<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Digital Fashion";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div> 
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

  <!-- <link href="../../media/LP/src/css/2022/digital-fashion-microverse.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->
<style type="text/css">
  #app, #header,
  footer.footer {
    display: none !important;
  }
</style>
<link href="https://static.galerieslafayette.com/media/LP/src/css/2022/digital-fashion-microverse.min.v01.css" rel="stylesheet" type="text/css" /> 
<iframe id="microverse-app"
    class="microverse-app is-fixed-top"
    loading="eager"
    src="https://capable-eclair-982b7d.netlify.app/">
</iframe>

<!--=========================== FIN LANDING PAGE ========================-->
    
<?php include ('../pages-defaults/footer.php'); ?>
