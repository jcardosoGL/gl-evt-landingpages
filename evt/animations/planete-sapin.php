<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Planète Sapin";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
<!-- <link href="../../media/LP/src/css/2022/planete-sapin.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->
<link href="https://static.galerieslafayette.com/media/LP/src/css/2022/planete-sapin.min.v04.css" rel="stylesheet" type="text/css" />

<style type="text/css">
  #toky_container,
  .ab682943,
  .ab684263 {
   display: none !important;
   }
   .ab682943--first-banner,
   .ab684263--first-banner {
   margin-top: 0 !important;
   }
</style>
<div class="lp-container">
  <a href="https://www.galerieslafayette.com/" class="button back-button is-fixed">accéder à galerieslafayette.com</a>
  
  <section class="section lp-logo">
    <div class="container is-fullheight">
      <div class="luxe-row luxe-center-mobile no-gutter is-fullheight">
        <div class="luxe-col-mobile-12">
          <a href="https://www.galerieslafayette.com/" class="hero_logo">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/planete-sapin/logo-gl-blanc.svg" alt="Galeries Lafayette">
            </figure>
          </a>
        </div>
      </div>
    </div>
  </section>
  
  
  
  <section class="section lp-hero">
    <div class="lp-hero-anim is-fullheight">
      <div class="lp-hero-anim-container-planete">
        <div class="lp-hero-anim-logo">&nbsp;</div>
        <div class="lp-hero-anim-planete">&nbsp;</div>
      </div>
      <div class="lp-hero-anim-container-nordman">
        <div class="lp-hero-anim-nordman">&nbsp;</div>
      </div>
      <div class="lp-hero-anim-container-bg">
        <div class="lp-hero-anim-bg">&nbsp;</div>
      </div>
    </div>
    
    <div class="div-fixed arrow">
      <a class="button-anchor_b has-smoothscroll" href="#planete-sapin">
        <svg fill="none" height="24" viewBox="0 0 20 24" width="20" xmlns="http://www.w3.org/2000/svg"><path d="M 10 23 C 5.02944 23 1 18.9706 1 14 C 1 9.02944 5.02944 5 10 5 C 14.9706 5 19 9.02944 19 14 C 19 18.9706 14.9706 23 10 23 Z" stroke-width="2"></path> <path d="M 15 12.75 L 10 16.5 L 5 12.75" stroke-linejoin="round" stroke-width="2"></path></svg>
      </a>
    </div>
  </section>
  
  
  <section class="section lp-main-content">
    <div class="container is-fullheight">
      <div class="luxe-row luxe-center-mobile no-gutter is-fullheight">
        <div class="luxe-col-mobile-12">
          
          <div class="videoContainer small-video01 is-hidden-touch">            
             <div class="video_wrapper"> 
                 <div style="padding:56.25% 0 0 0;position:relative;">                 
                     <iframe class="" src="https://player.vimeo.com/video/768157058?autoplay=1&muted=1&background=1" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                 </div>
             </div>
          </div>
          
          <div class="txt-container txt01">
            <div id="planete-sapin"></div>
            <h1>Planète sapin</h1>  
            <p>Cette année, les Galeries Lafayette vous offrent un conte de Noël pour faire des fêtes un enchantement.</p>
          </div>
           
          <div class="videoContainer small-video02">            
              <div class="video_wrapper"> 
                  <div style="padding:56.25% 0 0 0;position:relative;">                 
                      <iframe class="" src="https://player.vimeo.com/video/768157090?autoplay=1&muted=1&background=1" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                  </div>
              </div>
          </div>
          
          <div class="txt-container txt02">
            <p>Nordmann, un sapin originaire d’Elato, la planète  des sapins, est envoyé sur terre au secours d’Annie, dont le papa a décidé de ne pas fêter&nbsp;Noël.</p>
          </div>
          
           <div class="videoContainer small-video03">            
              <div class="video_wrapper"> 
                  <div style="padding:56.25% 0 0 0;position:relative;">                 
                      <iframe class="" src="https://player.vimeo.com/video/768157161?autoplay=1&muted=1&background=1" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                  </div>
              </div>
           </div>
          
          <div class="txt-container txt03">
            <p>Il retrouve la petite fille sous la coupole des Galeries Lafayette, à Paris.  Ensemble, ils s’amusent dans le grand magasin lorsque le père d’Annie surgit.</p>  
          </div>
          
          <div class="videoContainer small-video04 is-hidden-touch">            
              <div class="video_wrapper"> 
                  <div style="padding:56.25% 0 0 0;position:relative;">                 
                      <iframe class="" src="https://player.vimeo.com/video/768157202?autoplay=1&muted=1&background=1" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                  </div>
              </div>
          </div>
          
          <a href="#le-film" class="has-smoothscroll is-hidden-touch button is-gl-pink">Voir le film<span class="icon has-arrow"></span></a>
        </div>
      </div>
    </div>
  </section>
  
  
  <section class="section lp-main-video">
    <div class="container is-fullheight">
      <div class="luxe-row luxe-center-mobile no-gutter is-fullheight">
        <div class="luxe-col-mobile-12">
          <div class="le-film" id="le-film"></div>
          <h2>Le film</h2>
          <div class="videoContainer">            
            <div class="video_wrapper"> 
              <div class="video_M" style="padding:112.25% 0 0 0;position:relative;">                 
                <iframe class="" src="https://player.vimeo.com/video/768136840" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
              </div>
              <div class="video_T" style="padding:56.25% 0 0 0;position:relative;">
                <iframe class="" src="https://player.vimeo.com/video/768053663" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
              </div>
              <div class="video_D">
                <iframe class="" src="https://player.vimeo.com/video/768053663" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
              </div>
           </div>
           </div>
        </div>
      </div>
    </div>
  </section>
  
  
  <section class="section lp-boutique-links">
    <div class="container">
      <div class="luxe-row luxe-center-mobile">
        <div class="luxe-col-mobile-12">
          <h2>Pour un noël merveilleux</h2>
          
          <div class="slider-links slider-flickity luxe-row luxe-center-desktop">
            
            <div class="link luxe-col-desktop">
              <a href="https://www.galerieslafayette.com/h/noel" class="cadeaux" target="_blank">
                <div class="product-image">
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/planete-sapin/link-cadeaux.jpg"
                      srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/planete-sapin/link-cadeaux@2x.jpg 2x"
                      alt="La boutique cadeaux - Galeries Lafayette">
                  </figure>
                </div>
                <div class="product-txt">
                  <button class="button is-gl-pink">La boutique cadeaux<span class="icon has-arrow"></span></button>
                </div>
              </a>
            </div>
            <div class="link luxe-col-desktop">
              <a href="https://www.galerieslafayette.com/m/nos-magasins" class="livre">
                <div class="product-image">
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/planete-sapin/link-livre.png"
                      srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/planete-sapin/link-livre@2x.png 2x"
                      alt="Acheter le livre - Galeries Lafayette">
                  </figure>
                </div>
                <div class="product-txt">
                  <button class="button is-gl-pink">Disponible en magasin<span class="icon has-arrow"></span></button>
                </div>
              </a>
            </div>
            <div class="link luxe-col-desktop">
              <a href="https://haussmann.galerieslafayette.com/events/expedition-planete-sapin/?utm_source=GLCOM&utm_medium=LPNOEL" class="sapin" target="_blank">
                <div class="product-image">
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/planete-sapin/link-planete-sapin.png"
                      srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/planete-sapin/link-planete-sapin@2x.png 2x"
                      alt="Réserver une place - Galeries Lafayette">
                  </figure>
                  <p>Les Galeries Lafayette Paris Haussmann proposent une expérience immersive inédite a&nbsp;vivre en magasin</p>
                </div>
                <div class="product-txt">
                  <button class="button is-gl-pink">Réserver une place<span class="icon has-arrow"></span></button>
                </div>
              </a>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2022/planete-sapin.min.v05.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->

  
<!-- build:js /media/LP/src/js/2022/planete-sapin.min.v05.js
  <script src="../../assets/js/flickity.pkgd.min.js"></script>
  <script src="../../assets/js/ScrollMagic.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
  <script src="../src/js/2022/planete-sapin.js"></script>
endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>