<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Pangaia";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../media/LP/src/css/2021/pangaia.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/pangaia.min.v01.css" rel="stylesheet" type="text/css" />
<div class="lp-container">
  <section class="section lp-hero">
    <div class="container is-fullheight">
      <div class="luxe-row no-gutter is-fullheight">
        <div class="luxe-col-mobile-12">          
          <h1 class="boxed-text">
            <span>Pangaia</span>
          </h1>
          
          <div class="boxed-text-container">
            <p class="boxed-text exclu">
              <span>en exclusivité aux galeries lafayette</span>
            </p>
            <p class="boxed-text exclu">
              <span>en exclusivité aux galeries lafayette</span>
            </p>
            <p class="boxed-text exclu is-hidden-widescreen">
              <span>en exclusivité aux galeries lafayette</span>
            </p>
          </div>
          
          <a href="#marque" class="has-smoothscroll selection-cta">
            <p class="boxed-text selection">
              <span>Découvrez la marque</span>
              <span>↓</span>
            </p>
          </a>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body marque" style="background-color:#000000;" id="marque">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-10 luxe-col-desktop-8 luxe-col-widescreen-7 luxe-col-fullhd-8">
          <div class="content text-left">
            <div class="video-container">
              <video class="" preload="auto" autoplay loop playsinline muted>
                <source src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/planet-earth-revolving.mp4" type="video/mp4">
              </video>
            </div>
            <p>Fondée sur l’innovation et le design, PANGAIA s’est donné pour mission de confectionner un vestiaire durable et responsable. Pour la première fois en France, retrouvez en exclusivité PANGAIA, du 27 octobre au 31 décembre, aux Galeries Lafayette Paris Haussmann et sur GALERIESLAFAYETTE.COM.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body produits incontournables" style="background-color:#CDE1FF;">
    <div class="container">
      <div class="luxe-row no-gutter">
        <div class="luxe-col-mobile-12">
          <div class="content">
            <h2 class="boxed-text">
              <span>Les incontournables</span>
              <span>Pangaia</span>
              <span>↓</span>
            </h2>
          </div>
        </div>
        <div class="luxe-col-mobile-12">
          <div class="content text-center">
            <div class="slider-incontournables slider-flickity">
              <div class="product">
                <a href="https://www.galerieslafayette.com/p/hoodie+droit+365+signature-pangaia/81851387/101">
                  <div class="product-image">
                    <figure class="image">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_hoodie.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_hoodie@2x.jpg 2x"
                        width="360" height="360"
                        alt="Pangaia : - Galeries Lafayette">
                    </figure>
                  </div>
                  <div class="product-txt">
                    <p class="product-title is-uppercase is-bold">Pangaia</p>
                    <p>Hoodie droit 365 Signature</p>
                  </div>
                </a>
              </div>
              <div class="product">
                <a href="https://www.galerieslafayette.com/p/pantalon+jogging+365+signature-pangaia/81851655/230">
                  <div class="product-image">
                    <figure class="image">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_jogging.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_jogging@2x.jpg 2x"
                        alt="Pangaia : - Galeries Lafayette">
                    </figure>
                  </div>
                  <div class="product-txt">
                    <p class="product-title is-uppercase is-bold">Pangaia</p>
                    <p>Pantalon Jogging</p>
                  </div>
                </a>
              </div>
              <div class="product">
                <a href="https://www.galerieslafayette.com/p/t-shirt+droit+en+coton+biologique-pangaia/81851841/464?version=v2">
                  <div class="product-image">
                    <figure class="image">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_tshirt.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_tshirt@2x.jpg 2x"
                        alt="Pangaia : - Galeries Lafayette">
                    </figure>
                  </div>
                  <div class="product-txt">
                    <p class="product-title is-uppercase is-bold">Pangaia</p>
                    <p>T-shirt droit en coton biologique</p>
                  </div>
                </a>
              </div>
              <div class="product">
                <a href="https://www.galerieslafayette.com/p/sweat+droit+365+signature-pangaia/81851521/125?version=v2">
                  <div class="product-image">
                    <figure class="image">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_sweat.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_sweat@2x.jpg 2x"
                        alt="Pangaia : - Galeries Lafayette">
                    </figure>
                  </div>
                  <div class="product-txt">
                    <p class="product-title is-uppercase is-bold">Pangaia</p>
                    <p>Sweat droit 365 Signature</p>
                  </div>
                </a>
              </div>
              <div class="product">
                <a href="https://www.galerieslafayette.com/p/bob+en+coton+biologique-pangaia/81852278/104">
                  <div class="product-image">
                    <figure class="image">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_bob.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_bob@2x.jpg 2x"
                        alt="Pangaia : - Galeries Lafayette">
                    </figure>
                  </div>
                  <div class="product-txt">
                    <p class="product-title is-uppercase is-bold">Pangaia</p>
                    <p>Bob en coton biologique</p>
                  </div>
                </a>
              </div>
              <div class="product">
                <a href="https://www.galerieslafayette.com/p/doudoune+ajustee+a+capuche+mi-longue-pangaia/81852161/320">
                  <div class="product-image">
                    <figure class="image">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_doudoune.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_doudoune@2x.jpg 2x"
                        alt="Pangaia : - Galeries Lafayette">
                    </figure>
                  </div>
                  <div class="product-txt">
                    <p class="product-title is-uppercase is-bold">Pangaia</p>
                    <p>Doudoune ajustée à capuche mi-longue</p>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 is-hidden-tablet">
          <div class="content">
            <a href="https://www.galerieslafayette.com/b/pangaia" class="selection-cta">
              <p class="boxed-text selection">
                <span>Voir la séléction</span>
                <span>→</span>
              </p>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body innovation">
    <div class="container">
      <div class="luxe-row no-gutter luxe-middle-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-last-tablet" style="background-color:#FF4D82;">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/innovation.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/innovation@2x.jpg 2x"
                width="720" height="918"
                alt="Pangaia : Innovation entre confort et responsabilité - Galeries Lafayette">
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12">
          <div class="content" style="background-color:#FF4D82;">
            <h2 class="boxed-text">
              <span>Innovation</span>
              <span>entre confort</span> <br />
              <span> et responsabilité</span>
            </h2>
          </div>
        </div>
        <div class="luxe-col-mobile-11 luxe-col-tablet-6">
          <div class="alt-content">
            <p>PANGAIA est un collectif international de designers, d’ingénieurs et de scientifiques fondé sur l’innovation et le design. Tourné vers une mode toujours plus responsable, PANGAIA imagine des collections placées sous le signe du confort et créées en collaboration avec les meilleurs instituts de recherche et laboratoires scientifiques. L’idée ? Réduire au maximum les impacts environnementaux liés à la conception des vêtements et proposer la mode la plus durable possible.</p>
            <a href="https://www.galerieslafayette.com/b/pangaia" class="is-bold alt-button">Voir la sélection<span class="icon has-arrow"></span></a>
          </div>
        </div>
        <div class="luxe-col-mobile-1 is-hidden-tablet" style="background-color:#FF4D82;"></div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body exclusivite vert">
    <div class="container">
      <div class="luxe-row no-gutter luxe-middle-mobile">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-last-tablet products">
          <div class="content text-center">
            <div class="couleur vert">
              <div class="article-image">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/exclu-vert.png"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/exclu-vert@2x.png 2x"
                    width="520" height="520"
                    alt="Pangaia : Hoodie coton en Vert Luxembourg - Galeries Lafayette">
                </figure>
              </div>
              <div class="product-txt">
                <p class="product-title is-uppercase is-bold">Pangaia</p>
                <p>Hoodie Signature en Vert Luxembourg</p>
              </div>
            </div>
            <div class="couleur brun">
              <div class="article-image">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/exclu-brun.png"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/exclu-brun@2x.png 2x"
                    width="520" height="520"
                    alt="Pangaia : Hoodie Signature 365 en Brun Marais - Galeries Lafayette">
                </figure>
              </div>
              <div class="product-txt">
                <p class="product-title is-uppercase is-bold">Pangaia</p>
                <p>Hoodie Signature 365 en Brun Marais</p>
              </div>
            </div>
            <div class="couleur rouge">
              <div class="article-image">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/exclu-rouge.png"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/exclu-rouge@2x.png 2x"
                    width="520" height="520"
                    alt="Pangaia : Hoodie Signature 365 en Rouge Luxembourg - Galeries Lafayette">
                </figure>
              </div>
              <div class="product-txt">
                <p class="product-title is-uppercase is-bold">Pangaia</p>
                <p>Hoodie Signature 365 en Rouge Luxembourg</p>
              </div>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="luxe-row no-gutter">
            <div class="luxe-col-mobile-12">
              <div class="content">
                <h2 class="boxed-text">
                  <span>En exclusivité</span>
                  <span>Des coloris iconiques</span>
                </h2>
              </div>
            </div>
            <div class="luxe-col-mobile-12 luxe-col-tablet-10 luxe-col-fullhd-11">
              <div class="content">
                <p>PANGAIA propose une collection d'essentiels ultra désirables. Parmi ces nouveaux indispensables, des sweats à capuche, des t-shirts, des pantalons, des accessoires et des pièces activewear déclinées dans un joli colorama, à porter au quotidien. 
                </p>
                <p>A l’occasion de son arrivée aux Galeries Lafayette Paris Haussmann et sur GALERIESLAFAYETTE.COM, trois coloris exclusifs sont à découvrir sans plus tarder : le Rouge Opéra, le Vert Luxembourg et le Brun Marais.
                  </p>
                <a href="https://www.galerieslafayette.com/b/pangaia" class="is-bold alt-button">Voir la sélection<span class="icon has-arrow"></span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body go-for-good" style="background-color:#9762F3;">
    <div class="container">
      <div class="luxe-row no-gutter">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="luxe-row no-gutter">
            <div class="luxe-col-mobile-12">
              <div class="content">
                <h2 class="boxed-text main">
                  <span>100% good</span>
                  <span>un collectif <span class="is-hidden-widescreen">toujours</span></span>
                  <span class="is-hidden-touch is-hidden-desktop-only">toujours plus</span>
                  <span><span class="is-hidden-widescreen">plus</span> engagé</span>
                </h2>
              </div>
            </div>
            <div class="luxe-col-mobile-12 luxe-col-tablet-10 luxe-col-fullhd-11">
              <div class="content">
                <p>Le collectif PANGAIA a passé de nombreuses années à imaginer des pièces à l’impact moindre sur l’environnement. L’arrivée du label boulevard Haussmann s’inscrit donc parfaitement dans la démarche GO FOR GOOD initiée par les Galeries Lafayette en faveur d’une mode responsable.                   
                </p>
                <p>La collection, présentée dans un pop-up immersif, dévoile les différentes innovations et matériaux utilisés par la marque. Découvrez des innovations exclusives.
                  </p>
              </div>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 textiles">
          <div class="content">
            <h2 class="boxed-text">
              <span>↓</span><br class="is-hidden-tablet">
              <span>des textiles <span class="is-hidden-mobile">innovants</span></span>
              <span class="is-hidden-tablet">innovants</span>
            </h2>
          </div>
          <div class="luxe-row no-gutter luxe-middle-mobile text-center">
            <div class="luxe-col-mobile-6" style="background-color:#73C3F8;">
              <div class="textile">
                <div class="textile-logo">
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/flwrdwn-icon.png"
                      srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/flwrdwn-icon@2x.png 2x"
                      width="88" height="88"
                      alt="Pangaia : FLWRDWN - Galeries Lafayette">
                  </figure>
                </div>
                <div class="textile-txt">
                  <p class="textile-title is-uppercase is-bold">FLWRDWN™</p>
                  <p>Revêtements à partir de fleurs sauvages séchées naturelles</p>
                </div>
              </div>
            </div>
            <div class="luxe-col-mobile-6" style="background-color:#009D1F;">
              <div class="textile">
                <div class="textile-logo">
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/pprmnt-icon.png"
                      srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/pprmnt-icon@2x.png 2x"
                      width="88" height="88"
                      alt="Pangaia : FLWRDWN - Galeries Lafayette">
                  </figure>
                </div>
                <div class="textile-txt">
                  <p class="textile-title is-uppercase is-bold">PPRMNT™</p>
                  <p>Traitement à base de menthe poivrée qui permet de garder les vêtements frais plus longtemps</p>
                </div>
              </div>
            </div>
            <div class="luxe-col-mobile-6" style="background-color:#F8C800;">
              <div class="textile">
                <div class="textile-logo">
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/air-ink-icon.png"
                      srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/air-ink-icon@2x.png 2x"
                      width="88" height="88"
                      alt="Pangaia : FLWRDWN - Galeries Lafayette">
                  </figure>
                </div>
                <div class="textile-txt">
                  <p class="textile-title is-uppercase is-bold">AIR INK®</p>
                  <p>Technologie qui recycle les particules fines de pollution en pigments</p>
                </div>
              </div>
            </div>
            <div class="luxe-col-mobile-6" style="background-color:#F9400F;">
              <div class="textile">
                <div class="textile-logo">
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/c-fiber-icon.png"
                      srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/c-fiber-icon@2x.png 2x"
                      width="88" height="88"
                      alt="Pangaia : FLWRDWN - Galeries Lafayette">
                  </figure>
                </div>
                <div class="textile-txt">
                  <p class="textile-title is-uppercase is-bold">C-FIBER™</p>
                  <p>à partir de pulpe d'eucalyptus et de poudre d’algues</p>
                </div>
              </div>
            </div>
          </div> 
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body produits innovations is-hidden" style="background-color:#AA7AFD;">
    <div class="container">
      <div class="luxe-row no-gutter">
        <div class="luxe-col-mobile-12">
          <div class="content">
            <h2 class="boxed-text">
              <span>Les innovations</span>
              <span>Pangaia</span>
              <span>↓</span>
            </h2>
          </div>
        </div>
        <div class="luxe-col-mobile-12">
          <div class="content text-center">
            <div class="slider-incontournables slider-flickity">
              <div class="product">
                <a href="https://www.galerieslafayette.com/b/pangaia">
                  <div class="product-image">
                    <figure class="image">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_prod_test.png"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_prod_test@2x.png 2x"
                        width="360" height="360"
                        alt="Pangaia : - Galeries Lafayette">
                    </figure>
                  </div>
                  <div class="product-txt">
                    <p class="product-title is-uppercase is-bold">Pangaia</p>
                    <p>Hoodie</p>
                  </div>
                </a>
              </div>
              <div class="product">
                <a href="https://www.galerieslafayette.com/b/pangaia">
                  <div class="product-image">
                    <figure class="image">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_prod_test.png"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_prod_test@2x.png 2x"
                        alt="Pangaia : - Galeries Lafayette">
                    </figure>
                  </div>
                  <div class="product-txt">
                    <p class="product-title is-uppercase is-bold">Pangaia</p>
                    <p>Hoodie</p>
                  </div>
                </a>
              </div>
              <div class="product">
                <a href="https://www.galerieslafayette.com/b/pangaia">
                  <div class="product-image">
                    <figure class="image">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_prod_test.png"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_prod_test@2x.png 2x"
                        alt="Pangaia : - Galeries Lafayette">
                    </figure>
                  </div>
                  <div class="product-txt">
                    <p class="product-title is-uppercase is-bold">Pangaia</p>
                    <p>Hoodie</p>
                  </div>
                </a>
              </div>
              <div class="product">
                <a href="https://www.galerieslafayette.com/b/pangaia">
                  <div class="product-image">
                    <figure class="image">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_prod_test.png"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_prod_test@2x.png 2x"
                        alt="Pangaia : - Galeries Lafayette">
                    </figure>
                  </div>
                  <div class="product-txt">
                    <p class="product-title is-uppercase is-bold">Pangaia</p>
                    <p>Hoodie</p>
                  </div>
                </a>
              </div>
              <div class="product">
                <a href="https://www.galerieslafayette.com/b/pangaia">
                  <div class="product-image">
                    <figure class="image">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_prod_test.png"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_prod_test@2x.png 2x"
                        alt="Pangaia : - Galeries Lafayette">
                    </figure>
                  </div>
                  <div class="product-txt">
                    <p class="product-title is-uppercase is-bold">Pangaia</p>
                    <p>Hoodie</p>
                  </div>
                </a>
              </div>
              <div class="product">
                <a href="https://www.galerieslafayette.com/b/pangaia">
                  <div class="product-image">
                    <figure class="image">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_prod_test.png"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/incontournables_prod_test@2x.png 2x"
                        alt="Pangaia : - Galeries Lafayette">
                    </figure>
                  </div>
                  <div class="product-txt">
                    <p class="product-title is-uppercase is-bold">Pangaia</p>
                    <p>Hoodie</p>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 is-hidden-tablet">
          <div class="content">
            <a href="https://www.galerieslafayette.com/b/pangaia" class="selection-cta">
              <p class="boxed-text selection">
                <span>Voir la séléction</span>
                <span>→</span>
              </p>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body haussmann visu-1">
    <div class="container">
      <div class="luxe-row no-gutter">
        <div class="luxe-col-mobile-12 luxe-last-tablet">
          
          <div class="article-image visus">
            <figure class="image visu visu-1">
              <img class="is-hidden-tablet"
                src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/pop-up_M.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/pop-up_M@2x.jpg 2x"
                width="360" height="480"
                alt="Pangaia : a paris haussmann une expérience unique - Galeries Lafayette">
              <img class="is-hidden-mobile"
                src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/pop-up_D.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/pop-up_D@2x.jpg 2x"
                width="1440" height="876"
                alt="Pangaia : a paris haussmann une expérience unique - Galeries Lafayette">
            </figure>
            <figure class="image visu visu-2">
              <img class="is-hidden-tablet"
                src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/pop-up-v2_M.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/pop-up-v2_M@2x.jpg 2x"
                width="360" height="480"
                alt="Pangaia : a paris haussmann une expérience unique - Galeries Lafayette">
              <img class="is-hidden-mobile"
                src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/pop-up-v2_D.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/pop-up-v2_D@2x.jpg 2x"
                width="1440" height="876"
                alt="Pangaia : a paris haussmann une expérience unique - Galeries Lafayette">
            </figure>
            <figure class="image visu visu-3">
              <img class="is-hidden-tablet"
                src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/pop-up-v3_M.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/pop-up-v3_M@2x.jpg 2x"
                width="360" height="480"
                alt="Pangaia : a paris haussmann une expérience unique - Galeries Lafayette">
              <img class="is-hidden-mobile"
                src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/pop-up-v3_D.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/pangaia/pop-up-v3_D@2x.jpg 2x"
                width="1440" height="876"
                alt="Pangaia : a paris haussmann une expérience unique - Galeries Lafayette">
            </figure>
          </div>
          
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <h2 class="boxed-text">
              <span>A Paris Haussmann</span>
              <span>une expérience <span class="is-hidden-touch">unique</span></span> <br />
              <span class="is-hidden-desktop">unique</span>
            </h2>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <p>Du 27 octobre au 31 décembre, retrouvez en exclusivité PANGAIA aux Galeries Lafayette Paris Haussmann. Sur un pop-up dédié, les collections automne-hiver du label se déploient dans un colorama éblouissant. L'ocassion de découvrir les nouveautés de la marque ainsi que des modèles en exclusivité.</p>
            <a href="https://haussmann.galerieslafayette.com/pangaia-marque/" class="is-bold alt-button" target="_blank">En savoir plus<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body univers">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-tablet">
        <div class="luxe-col-mobile-12">
          <a href="https://www.galerieslafayette.com/b/pangaia" class="univers-cta">
            <h2 class="boxed-text">
              <span>tout l'univers <span class="is-hidden-mobile">Pangaia</span></span>
              <span class="is-hidden-tablet">Pangaia<br  /></span>
              <span><span class="is-hidden-tablet">découvrir →</span><span class="is-hidden-mobile">↗︎</span></span>
            </h2>
          </a> 
        </div>
      </div>
    </div>
  </section>

</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2021/pangaia.min.v01.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->


<!-- build:js /media/LP/src/js/2021/pangaia.min.v00.js 
  <script src="../../assets/js/flickity.pkgd.min.js"></script>
  <script src="../src/js/2021/pangaia.js"></script>
<!-- endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
