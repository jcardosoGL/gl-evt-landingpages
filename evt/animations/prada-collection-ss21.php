<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Prada SS21";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../media/LP/src/css/2021/prada-collection-ss21.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/prada-collection-ss21.min.v04.css" rel="stylesheet" type="text/css" />
<div class="luxe-augmente">
  <section class="luxe-augmente-hero">
    <iframe class="bg-image hero-video b-lazy" data-src="https://player.vimeo.com/video/503395062?background=1" width="1250" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
    <div class="container">
      <div class="scrolling-text-container text-left rtl-scroll">
        <p class="is-compress is-uppercase has-text-white text-size-1">« L’avenir a-t-il un attrait romantique pour vous ? »  •  « L’avenir a-t-il un attrait romantique pour vous ? »  •  « L’avenir a-t-il un attrait romantique pour vous ? »  •  «  La créativité est-elle un don ou une compétence ? »  •  «  La créativité est-elle un don ou une compétence ? »  •  « L’avenir a-t-il un attrait romantique pour vous ? »  •  «  La créativité est-elle un don ou une compétence ? »</p>
      </div>
      <div class="luxe-row">
        <div class="luxe-col-mobile-12 text-left">
          <h1 class="is-uppercase">Prada<span class="is-smaller is-block">Collection SS21 — symboles</span></h1>
          <a href="#sac-cleo" class="has-smoothscroll button borderless has-arrow-down">Découvrir</a>
        </div>
      </div>
      <div class="scrolling-text-container text-left ltr-scroll is-hidden-mobile">
        <p class="is-compress is-uppercase has-text-white text-size-1">« L’avenir a-t-il un attrait romantique pour vous ? »  •  « L’avenir a-t-il un attrait romantique pour vous ? »  •  « L’avenir a-t-il un attrait romantique pour vous ? »  •  «  La créativité est-elle un don ou une compétence ? »  •  «  La créativité est-elle un don ou une compétence ? »  •  « L’avenir a-t-il un attrait romantique pour vous ? »  •  «  La créativité est-elle un don ou une compétence ? »</p>
      </div>
    </div>
  </section>
  
  <!-- Bio -->
  <section class="luxe-augmente-body bio">
    <div class="container">
      <div class="luxe-row no-gutter luxe-reverse luxe-middle-mobile">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
          <p class="is-uppercase is-compress">Prada Femme Printemps/Été 2021 explore la notion d’uniforme avec des créations ciblées et raffinées, qui associent des matières innovantes et des techniques inspirées de la haute couture.</p>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/bio-img.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/bio-img@2x.jpg"
                  alt="Prada femme printemps/éré - Galeries Lafayette" width="526" height="560">
                <!-- Fallback for non JavaScript browsers -->
                <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/bio-img.jpg" alt="Prada femme printemps/éré - Galeries Lafayette" width="526" height="560" /></noscript>
              </figure>
            </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="luxe-augmente-body scrolling-text">
    <div class="container">
      <div class="scrolling-text-container text-left rtl-scroll">
        <p class="is-bold text-size-1">« L’avenir a-t-il un attrait romantique pour vous ? »  •  « L’avenir a-t-il un attrait romantique pour vous ? »  •  « Devrions-nous ralentir ou accélérer ? »  •  « Devrions-nous ralentir ou accélérer ? »  •  «  Devrions-nous ralentir ou accélérer ? »  •  « Devrions-nous ralentir ou accélérer ? »  • </p>
      </div>
    </div>
  </section>
  
  <section class="luxe-augmente-body" id="sac-cleo">
    <div class="container">
     
      <div class="products main-products luxe-row no-gutter luxe-middle-mobile">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 col-video">
          <iframe class="bg-image product-video b-lazy" data-src="https://player.vimeo.com/video/504291698?background=1" width="1250" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 col-product">
          <a href="https://www.galerieslafayette.com/p/sac+besace+cleo+en+cuir+brosse-prada/76069487/320?version=v2" class="">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/sac-cleo-push.png|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/sac-cleo-push@2x.png"
                  alt="Prada Le sac Cleo - Galeries Lafayette" width="520" height="480">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/sac-cleo-push.png" alt="Prada Le sac Cleo - Galeries Lafayette" width="520" height="480" /></noscript>
              </figure>
            </div>
            <br />
            <div class="luxe-middle-mobile product-txt"><p class="is-bold has-arrow">Sac besace Cleo</p></div>
          </a>
        </div>
      </div>

    </div>
  </section>
  
  <section class="luxe-augmente-body scrolling-text icones">
    <div class="container">
      <div class="scrolling-text-container text-left ltr-scroll">
        <p class="is-compress is-uppercase text-size-3">Les icones •  Les icones  •  Les icones •  Les icones  •  Les icones • Les icones •  Les icones  •  Les icones •  Les icones  • </p>
      </div>
    </div>
  </section>
  
  <!-- PRODUCTS -->
  <section class="luxe-augmente-body collection" id="collection">
    <div class="container">
      
      <div class="products luxe-row luxe-middle-mobile">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="luxe-row">
            <div class="product luxe-col-mobile-6">
              <a href="https://www.galerieslafayette.com/p/mini+sac+besace+re-edition+2000+satin+cristaux+fantaisie-prada/76205896/179" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-1.png"
                      alt="Prada Mini sac Re-Edition 2000 - Galeries Lafayette" width="240" height="232">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-1.png" alt="Prada Mini sac Re-Edition 2000 - Galeries Lafayette" width="240" height="232" /></noscript>
                  </figure>
                </div>
                <br />
                <div class="luxe-middle-mobile product-txt"><p class="is-bold">Mini sac Re-Edition 2000</p></div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6">
              <a href="https://www.galerieslafayette.com/p/t-shirt+logo+en+jersey+brode-prada/76065173/85" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/prada-t-shirt-jersey-brode.png"
                      alt="Prada T-shirt en jersey brodé - Galeries Lafayette" width="240" height="232">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/prada-t-shirt-jersey-brode.png" alt="Prada T-shirt en jersey brodé - Galeries Lafayette" width="240" height="232" /></noscript>
                  </figure>
                </div>
                <br />
                <div class="luxe-middle-mobile product-txt"><p class="is-bold">T-shirt en jersey brodé</p></div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6">
              <a href="https://www.galerieslafayette.com/p/t-shirt+oversize+signaux+jersey+imprime-prada/76065186/85" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-3.png"
                      alt="Prada T-shirt oversize Signaux - Galeries Lafayette" width="240" height="232">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-3.png" alt="Prada T-shirt oversize Signaux - Galeries Lafayette" width="240" height="232" /></noscript>
                  </figure>
                </div>
                <br />
                <div class="luxe-middle-mobile product-txt"><p class="is-bold">T-shirt oversize Signaux</p></div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6">
              <a href="https://www.galerieslafayette.com/p/sac+a+dos+signaux+en+nylon+imprime-prada/76069544/320" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-4.png"
                      alt="Prada Sac à dos Signaux - Galeries Lafayette" width="240" height="232">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-4.png" alt="Prada Sac à dos Signaux - Galeries Lafayette" width="240" height="232" /></noscript>
                  </figure>
                </div>
                <br />
                <div class="luxe-middle-mobile product-txt"><p class="is-bold">Sac à dos Signaux</p></div>
              </a>
            </div>
          </div>
        </div>
        <div class="is-hidden-mobile luxe-col-tablet-6">
          <div class="luxe-row">
            <div class="product main luxe-col-tablet-12">
              <a href="https://www.galerieslafayette.com/p/bob+en+re-nylon-prada/76065243/320" class="has-text-white">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/prada-bob-nylon-femme.png|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/prada-bob-nylon-femme@2x.png"
                      alt="Prada Bob en Re-Nylon - Galeries Lafayette" width="482" height="586">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/prada-bob-nylon-femme.png" alt="Prada Bob en Re-Nylon - Galeries Lafayette" width="482" height="586" /></noscript>
                  </figure>
                </div>
                <br />
                <div class="luxe-middle-mobile product-txt"><p class="is-bold">Bob en Re-Nylon</p></div>
              </a>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-12">
          <div class="luxe-row">
            <div class="product main luxe-col-mobile-6 is-hidden-tablet">
              <a href="https://www.galerieslafayette.com/p/bob+en+re-nylon-prada/76065243/320" class="has-text-white">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/prada-bob-nylon-femme.png"
                      alt="Prada Bob en Re-Nylon - Galeries Lafayette" width="240" height="232">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/prada-bob-nylon-femme.png" alt="Prada Bob en Re-Nylon - Galeries Lafayette" width="240" height="232" /></noscript>
                  </figure>
                </div>
                <br />
                <div class="luxe-middle-mobile product-txt"><p class="is-bold">Bob en Re-Nylon</p></div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/sac+besace+en+cuir+brosse-prada/76069497/453" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/prada-sac-besace-cuir-brosse.png"
                      alt="Prada Sac besace en cuir brossé - Galeries Lafayette" width="240" height="232">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/prada-sac-besace-cuir-brosse.png" alt="Prada Sac besace en cuir brossé - Galeries Lafayette" width="240" height="232" /></noscript>
                  </figure>
                </div>
                <br />
                <div class="luxe-middle-mobile product-txt"><p class="is-bold">Sac besace en cuir brossé</p></div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/foulard+soie+en+serge+imprime-prada/76065254/101" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/prada-foulard-soie-en-serge.png"
                      alt="Prada Foulard soie en sergé  - Galeries Lafayette" width="240" height="232">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/prada-foulard-soie-en-serge.png" alt="Prada Foulard soie en sergé - Galeries Lafayette" width="240" height="232" /></noscript>
                  </figure>
                </div>
                <br />
                <div class="luxe-middle-mobile product-txt"><p class="is-bold">Foulard soie en sergé</p></div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/sac+besace+re-edition+2005+en+re-nylon-prada/76069528/104" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/prada-sac-besace-re-edition-2005-gris.png"
                      alt="Prada Sac besace en cuir - Galeries Lafayette" width="240" height="232">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/prada-sac-besace-re-edition-2005-gris.png" alt="Prada Sac besace en cuir - Galeries Lafayette" width="240" height="232" /></noscript>
                  </figure>
                </div>
                <br />
                <div class="luxe-middle-mobile product-txt"><p class="is-bold">Sac besace Re-Edition 2005</p></div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/boucle+d+oreilles+pendante+logo+smalto+jewels-prada/76065273/320" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-8.png"
                      alt="Prada Boucle d’oreille - Galeries Lafayette" width="240" height="232">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-8.png" alt="Prada Boucle d’oreille - Galeries Lafayette" width="240" height="232" /></noscript>
                  </figure>
                </div>
                <br />
                <div class="luxe-middle-mobile product-txt"><p class="is-bold">Boucle d’oreille</p></div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 is-hidden-tablet">
              <a href="https://www.galerieslafayette.com/p/slippers+logo+en+peau+de+mouton-prada/76065190/320" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-9.png"
                      alt="Prada Slippers en mouton - Galeries Lafayette" width="240" height="232">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-9.png" alt="Prada Slippers en mouton - Galeries Lafayette" width="240" height="232" /></noscript>
                  </figure>
                </div>
                <br />
                <div class="luxe-middle-mobile product-txt"><p class="is-bold">Slippers en mouton</p></div>
              </a>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3">
          <div class="luxe-row">
            <div class="product luxe-col-tablet-12 is-hidden-mobile">
              <a href="https://www.galerieslafayette.com/p/slippers+logo+en+peau+de+mouton-prada/76065190/320" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/prada-slippers-en-mouton-noir.png"
                      alt="Prada Slippers en mouton - Galeries Lafayette" width="240" height="232">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/prada-slippers-en-mouton-noir.png" alt="Prada Slippers en mouton - Galeries Lafayette" width="240" height="232" /></noscript>
                  </figure>
                </div>
                <br />
                <div class="luxe-middle-mobile product-txt"><p class="is-bold">Slippers en mouton</p></div>
              </a>
            </div>            
            <div class="product luxe-col-mobile-12">
              <a href="https://www.galerieslafayette.com/p/mini+sac+besace+re-edition+2000+peau+de+mouton-prada/76205901/101" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-11.png"
                      alt="Prada Sac Re-Edition en mouton - Galeries Lafayette" width="240" height="232">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-11.png" alt="Prada Sac Re-Edition en mouton - Galeries Lafayette" width="240" height="232" /></noscript>
                  </figure>
                </div>
                <br />
                <div class="luxe-middle-mobile product-txt"><p class="is-bold">Sac Re-Edition en mouton</p></div>
              </a>
            </div>
            <div class="product main luxe-col-mobile-12 is-hidden-tablet">
              <a href="https://www.galerieslafayette.com/p/sac+besace+cleo+a+rabat+en+cuir+brosse-prada/76069470/453" class="has-text-white">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-p2.png"
                      alt="Prada Mini Sac Cleo en cuir - Galeries Lafayette" width="240" height="232">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-p2.png" alt="Prada Mini Sac Cleo en cuir - Galeries Lafayette" width="240" height="232" /></noscript>
                  </figure>
                </div>
                <br />
                <div class="luxe-middle-mobile product-txt"><p class="is-bold">Mini Sac Cleo en cuir</p></div>
              </a>
            </div>
            
          </div>
        </div>
        <div class="is-hidden-mobile luxe-col-tablet-6">
          <div class="luxe-row">
            <div class="product main luxe-col-tablet-12">
              <a href="https://www.galerieslafayette.com/p/sac+besace+cleo+a+rabat+en+cuir+brosse-prada/76069470/453" class="has-text-white">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-p2.png|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-p2@2x.png"
                      alt="Prada Mini Sac Cleo en cuir - Galeries Lafayette" width="482" height="586">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-p2.png" alt="Prada Mini Sac Cleo en cuir - Galeries Lafayette" width="482" height="586" /></noscript>
                  </figure>
                </div>
                <br />
                <div class="luxe-middle-mobile product-txt"><p class="is-bold">Mini Sac Cleo en cuir</p></div>
              </a>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3">
          <div class="luxe-row">
            <div class="product luxe-col-mobile-12">
              <a href="https://www.galerieslafayette.com/p/bandeau+en+satin+cristaux-prada/76205892/179" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-10.png"
                      alt="Prada Bandeau en satin - Galeries Lafayette" width="240" height="232">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/img-prod-10.png" alt="Prada Bandeau en satin - Galeries Lafayette" width="240" height="232" /></noscript>
                  </figure>
                </div>
                <br />
                <div class="luxe-middle-mobile product-txt"><p class="is-bold">Bandeau en satin</p></div>
              </a>
            </div>
            <div class="product luxe-col-mobile-12">
              <a href="https://www.galerieslafayette.com/p/lunettes+de+soleil-prada/76069453/230" class="">
                <div class="product-image luxe-middle-mobile">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/prada-lunettes-de-soleil.png"
                      alt="Prada Lunettes de soleil - Galeries Lafayette" width="240" height="232">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/prada-lunettes-de-soleil.png" alt="Prada Lunettes de soleil - Galeries Lafayette" width="240" height="232" /></noscript>
                  </figure>
                </div>
                <br />
                <div class="luxe-middle-mobile product-txt"><p class="is-bold">Lunettes de soleil</p></div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="luxe-row luxe-middle-mobile text-center">
        <div class="luxe-col-mobile-12">
          <a href="https://www.galerieslafayette.com/b/prada/ct/femme" class="collection-cta">Voir toute la collection<span class="plus"></span></a>
        </div>
      </div>
    </div>
  </section>
  
  <section class="luxe-augmente-body scrolling-text">
    <div class="container">
      <div class="scrolling-text-container text-left rtl-scroll">
        <p class="is-bold text-size-1">« L’avenir a-t-il un attrait romantique pour vous ? »  •  « L’avenir a-t-il un attrait romantique pour vous ? »  •  « Devrions-nous ralentir ou accélérer ? »  •  « Devrions-nous ralentir ou accélérer ? »  •  «  Devrions-nous ralentir ou accélérer ? »  •  « Devrions-nous ralentir ou accélérer ? »  • </p>
      </div>
    </div>
  </section>
  
  <section class="luxe-augmente-body middle-page">
    <div class="container">
      <div class="luxe-row no-gutter luxe-middle-mobile luxe-reverse">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 col-video">
          <iframe class="bg-image product-video b-lazy" data-src="https://player.vimeo.com/video/504315685?background=1" width="1250" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
        </div>
        <div class="luxe-col-tablet-6 is-hidden-mobile col-video">
          <iframe class="bg-image product-video b-lazy" data-src="https://player.vimeo.com/video/504315685?background=1" width="1250" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
        </div>
      </div>
    </div>
  </section>
  
  <section class="luxe-augmente-body scrolling-text">
    <div class="container">
      <div class="scrolling-text-container text-left ltr-scroll">
        <p class="is-bold text-size-1">« L’avenir a-t-il un attrait romantique pour vous ? »  •  « L’avenir a-t-il un attrait romantique pour vous ? »  •  « Devrions-nous ralentir ou accélérer ? »  •  « Devrions-nous ralentir ou accélérer ? »  •  «  Devrions-nous ralentir ou accélérer ? »  •  « Devrions-nous ralentir ou accélérer ? »  • </p>
      </div>
    </div>
  </section>
  
  <section class="luxe-augmente-body">
    <div class="container">
     
      <div class="products main-products luxe-row no-gutter luxe-middle-mobile luxe-reverse">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 col-video">
          <iframe class="bg-image product-video b-lazy" data-src="https://player.vimeo.com/video/504299561?background=1" width="1250" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 col-product">
          <a href="https://www.galerieslafayette.com/p/boucle+d+oreille+unique+logo+smalto+jewels-prada/76065285/58" class="">
            <div class="product-image luxe-middle-mobile">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/earrings-push.png|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/earrings-push@2x.png" 
                  alt="Prada Boucles d'oreilles - Galeries Lafayette" width="520" height="480">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/earrings-push.png" alt="Prada Boucles d'oreilles - Galeries Lafayette" width="520" height="480" /></noscript>
              </figure>
            </div>
            <br />
            <div class="luxe-middle-mobile product-txt"><p class="is-bold has-arrow">Boucles d'oreilles</p></div>
          </a>
        </div>
      </div>

    </div>
  </section>
  
  <!-- SHOPPING À DISTANCE -->
    <section class="luxe-augmente-body shoppingadistance">
      <div class="container">

        <div class="luxe-row no-gutter luxe-middle-mobile">
          <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-right">
            <div class="article-image">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/live-shopping.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/live-shopping@2x.jpg"
                  alt="Shopping à distance - Galeries Lafayette" width="560" height="560">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/prada-collection-ss21/live-shopping.jpg" alt="Shopping à distance - Galeries Lafayette" width="560" height="560" /></noscript>
              </figure>
            </div>
          </div>
          <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
            <div class="article-body has-text-white">
              <div class="tag is-uppercase">Shopping à distance</div>
              <h2 class="has-text-white">Laissez-vous inspirer par nos Personal Shoppers</h2>
              <p>Toute l’offre Prada du magasin Galeries Lafayette Paris Haussmann depuis chez vous grâce au Shopping à distance. Échangez en live vidéo avec les conseillers de vos marques préférées ou les personnel Shoppers des Galeries Lafayette Paris Haussmann, et aussi sur wishlist.</p>
              <a href="" class="button primary outlined trigger_liveshopping" onclick="clickedBrand='Prada'; appointedWidgetId='5f8878d26a0d547bea7db8f8'; GL_GIS.LiveShopping.checkClerkAvailability('brand15'); return false;">Acheter en live vidéo<span class="icon"><i class="microphone"></i></span></a>
            </div>
          </div>
        </div>
        
      </div>
    </section>
    
    <section class="luxe-augmente-body scrolling-text link">
      <div class="container">
        <div class="scrolling-text-container text-left ltr-scroll">
          <a href="https://www.galerieslafayette.com/b/prada/ct/femme">
            <p class="is-compress is-uppercase text-size-2 has-arrow">découvrir la collection</p>
            <p class="is-compress is-uppercase text-size-2 has-arrow">découvrir la collection</p>
            <p class="is-compress is-uppercase text-size-2 has-arrow">découvrir la collection</p>
            <p class="is-compress is-uppercase text-size-2 has-arrow">découvrir la collection</p>
            <p class="is-compress is-uppercase text-size-2 has-arrow">découvrir la collection</p>
          </a>
        </div>
      </div>
    </section>
    
    <section class="luxe-augmente-body page-bottom">
      <div class="container">
        <div class="luxe-row no-gutter luxe-middle-mobile">
          <div class="luxe-col-mobile-12 col-video">
            <iframe class="bg-image product-video b-lazy" data-src="https://player.vimeo.com/video/504307913?background=1" width="1250" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
          </div>
        </div>
      </div>
    </section>

</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2021/prada-collection-ss21.min.v01.js"></script>

<div id="goinstore_modals"></div>
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance-goinstore.min.v02.css" rel="stylesheet" type="text/css">
<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-goinstore.min.v07.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->
  
<!-- build:js /media/LP/src/js/2021/prada-collection-ss21.min.v01.js
  <script src="../../assets/js/blazy.min.js"></script>
  <script src="../../assets/js/ScrollMagic.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
  <script src="../src/js/2021/prada-collection-ss21.js"></script>
<!-- endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
