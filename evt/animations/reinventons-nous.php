<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Reinventons-nous";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../media/LP/src/css/2021/reinventons-nous.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/reinventons-nous.min.v07j.css" rel="stylesheet" type="text/css" />
<style>
  .ab682943,
  .ab684263 {
    display: none !important;
  }
  .ab682943--first-banner,
  .ab684263--first-banner {
    margin-top: 0 !important;
  }
</style>
<div class="reinventons-nous">
  <div class="reinventons-nous-app">
    
    <div class="go-down-container is-hidden-touch">
      <ul id="go-down-nav" role="tablist" data-scroll-header>
        <li class="next-link-is-active">
          <a href="#intro" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#manifesto" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#manifesto-vid" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#mode-main" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#mode-proenza-schouler" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#mode-proenza-schouler-produits" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#mode-ganni" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#mode-ganni-produits" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#mode-designers-remix" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#mode-parisienne-et-alors" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#mode-parisienne-et-alors-produits" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#mode-musier" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#mode-musier-produits" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#shoes-main" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#shoes-golden-goose" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#shoes-roger-vivier" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#shoes-roger-vivier-produits" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#shoes-chloe" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#shoes-vagabond" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#shoes-by-far" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#responsable-main" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#responsable-go-for-good" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#responsable-recc-paris" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#responsable-justine-clenquet" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#responsable-aveda" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#actumag-main" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#actumag-espace-chaussures" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#actumag-designer-galerie" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#actumag-creative-galerie" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#actumag-sneaker-galerie" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#actumag-re-store" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#actumag-re-store-b" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#actumag-renouveau-femme" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#actumag-creative-galerie-mode" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#actumag-marques-digitales" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
        <li>
          <a href="#restart" class="go-down has-smoothscroll"><span class="nav-arrow arrow-down"></span></a>
        </li>
      </ul>
    </div>
    
    <section class="reinventons-top-menu is-fixed-top is-hidden-desktop">
      <div class="top-container">
        <button class="close"></button>
      </div>
      <div class="tabs-container">
        <div class="tabs is-centered" role="tablist">
          <ul>
            <li role="presentation">
              <a href="#pane-mode" aria-controls="pane-mode" role="tab" class="button outlined is-width-auto mode is-active" aria-selected="true">Mode</a>
            </li>
            <li role="presentation">
              <a href="#pane-shoes" aria-controls="pane-shoes" role="tab" class="button outlined is-width-auto shoes" aria-selected="false" tabindex="-1">Chaussures</a>
            </li>
            <li role="presentation">
              <a href="#pane-responsable" aria-controls="pane-responsable" class="button outlined is-width-auto responsable" role="tab" aria-selected="false" tabindex="-1">Mode responsable</a>
            </li>
            <li role="presentation">
              <a href="#pane-actumag" aria-controls="pane-actumag"  class="button outlined is-width-auto actumag" role="tab" aria-selected="false" tabindex="-1">Paris Haussmann</a>
            </li>
          </ul>
        </div>
        
        <div class="panes">
          <div class="tab-panes">
            <div class="tab-pane is-active" id="pane-mode" role="tabpanel" tabindex="0" aria-labelledby="mode-tab">
              <div class="luxe-row luxe-middle-mobile">
                <div class="luxe-col-mobile-4">
                  <a href="#mode-main">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-main.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-main@2x.jpg"
                      alt="Les nouveautés femme - Galeries Lafayette">
                      <!-- Fallback for non JavaScript browsers -->
                      <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-main.jpg" alt="Les nouveautés femme - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#mode-proenza-schouler">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-proenza.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-proenza@2x.jpg"
                        alt="Proenza Schouler - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-proenza.jpg" alt="Proenza Schouler - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#mode-proenza-schouler-produits">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-proenza-produits-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-proenza-produits-v2@2x.jpg"
                        alt="Proenza Schouler - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-proenza-produits-v2.jpg" alt="Proenza Schouler - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#mode-ganni">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-ganni-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-ganni-v2@2x.jpg"
                        alt="Ganni - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-ganni-v2.jpg" alt="Ganni - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#mode-ganni-produits">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-ganni-produits-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-ganni-produits-v2@2x.jpg"
                        alt="Ganni - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-ganni-produits-v2.jpg" alt="Ganni - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#mode-designers-remix">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-designers-remix-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-designers-remix-v2@2x.jpg"
                        alt="Designers Remix - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-designers-remix-v2.jpg" alt="Designers Remix - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#mode-parisienne-et-alors">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-parisienne-et-alors-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-parisienne-et-alors-v2@2x.jpg"
                        alt="Parisienne et alors - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-parisienne-et-alors-v2.jpg" alt="Parisienne et alors - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#mode-parisienne-et-alors-produits">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-parisienne-et-alors-produits-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-parisienne-et-alors-produits-v2@2x.jpg"
                        alt="Parisienne et alors - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-parisienne-et-alors-produits-v2.jpg" alt="Parisienne et alors - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#mode-musier">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-musier-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-musier-v2@2x.jpg"
                        alt="Musier - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-musier-v2.jpg" alt="Musier - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#mode-musier-produits">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-musier-produits-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-musier-produits-v2@2x.jpg"
                        alt="Musier - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/cover_mode-musier-produits-v2.jpg" alt="Musier - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
              </div>
            </div>
            
            
            <div class="tab-pane" id="pane-shoes" role="tabpanel" tabindex="0" aria-labelledby="shoes-tab" hidden>
              <div class="luxe-row luxe-middle-mobile">
                <div class="luxe-col-mobile-4">
                  <a href="#shoes-main">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-main.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-main@2x.jpg"
                        alt="Les nouveautés chaussures - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-main.jpg" alt="Les nouveautés chaussures - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#shoes-golden-goose">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-golden-goose.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-golden-goose@2x.jpg"
                      alt="Golden Goose - Galeries Lafayette">
                      <!-- Fallback for non JavaScript browsers -->
                      <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-golden-goose.jpg" alt="Golden Goose - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#shoes-roger-vivier">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-roger-vivier.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-roger-vivier@2x.jpg"
                      alt="Roger Vivier - Galeries Lafayette">
                      <!-- Fallback for non JavaScript browsers -->
                      <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-roger-vivier.jpg" alt="Roger Vivier - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#shoes-roger-vivier-produits">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-roger-vivier-produits-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-roger-vivier-produits-v2@2x.jpg"
                      alt="Roger Vivier - Galeries Lafayette">
                      <!-- Fallback for non JavaScript browsers -->
                      <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-roger-vivier-produits-v2.jpg" alt="Roger Vivier - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#shoes-chloe" class="card-has-video">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-chloe.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-chloe@2x.jpg"
                      alt="Chloe - Galeries Lafayette">
                      <!-- Fallback for non JavaScript browsers -->
                      <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-chloe.jpg" alt="Chloe - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#shoes-vagabond">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-vagabond.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-vagabond@2x.jpg"
                      alt="Alohas - Galeries Lafayette">
                      <!-- Fallback for non JavaScript browsers -->
                      <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-vagabond.jpg" alt="Vagabond - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#shoes-by-far">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-by-far-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-by-far-v2@2x.jpg"
                      alt="By Far - Galeries Lafayette">
                      <!-- Fallback for non JavaScript browsers -->
                      <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/cover_shoes-by-far-v2.jpg" alt="By Far - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
              </div>              
            </div>
            
            
            <div class="tab-pane" id="pane-responsable" role="tabpanel" tabindex="0" aria-labelledby="responsable-tab" hidden>
              <div class="luxe-row luxe-middle-mobile">
                <div class="luxe-col-mobile-4">
                  <a href="#responsable-main">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/responsable/cover_responsable-main.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/responsable/cover_responsable-main@2x.jpg"
                        alt=" - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/responsable/cover_responsable-main.jpg" alt=" - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#responsable-go-for-good">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/responsable/cover_responsable-go-for-good.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/responsable/cover_responsable-go-for-good@2x.jpg"
                        alt=" - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/responsable/cover_responsable-go-for-good.jpg" alt=" - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#responsable-recc-paris">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/responsable/cover_responsable-recc-paris.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/responsable/cover_responsable-recc-paris@2x.jpg"
                        alt=" - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/responsable/cover_responsable-recc-paris.jpg" alt=" - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#responsable-justine-clenquet">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/responsable/cover_responsable-justine-clenquet.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/responsable/cover_responsable-justine-clenquet@2x.jpg"
                        alt=" - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/responsable/cover_responsable-justine-clenquet.jpg" alt=" - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#responsable-aveda">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/responsable/cover_responsable-aveda.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/responsable/cover_responsable-aveda@2x.jpg"
                        alt=" - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/responsable/cover_responsable-aveda.jpg" alt=" - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
              </div>              
            </div>
            
            <div class="tab-pane" id="pane-actumag" role="tabpanel" tabindex="0" aria-labelledby="actumag-tab" hidden>
              <div class="luxe-row luxe-middle-mobile">
                <div class="luxe-col-mobile-4">
                  <a href="#actumag-main">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-main-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-main-v2@2x.jpg"
                        alt="Votre magasin se réinvente - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-main-v2.jpg" alt="Votre magasin se réinvente - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#actumag-espace-chaussures">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-espace-chaussures.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-espace-chaussures@2x.jpg"
                      alt="Nouvel espace chaussures - Galeries Lafayette">
                      <!-- Fallback for non JavaScript browsers -->
                      <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-espace-chaussures.jpg" alt="Nouvel espace chaussures - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#actumag-designer-galerie">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-designer-galerie-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-designer-galerie-v2@2x.jpg"
                        alt="La designer galerie - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-designer-galerie-v2.jpg" alt="La designer galerie - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#actumag-creative-galerie">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-creative-galerie-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-creative-galerie-v2@2x.jpg"
                        alt="La créative galerie - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-creative-galerie-v2.jpg" alt="La créative galerie - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#actumag-sneaker-galerie">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-sneaker-galerie-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-sneaker-galerie-v2@2x.jpg"
                        alt="La sneakers galerie - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-sneaker-galerie-v2.jpg" alt="La sneakers galerie - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#actumag-re-store">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-re-store-v3.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-re-store-v3@2x.jpg"
                        alt="Nouvel espace seconde main - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-re-store-v3.jpg" alt="Nouvel espace seconde main - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#actumag-re-store-b">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-re-store-b-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-re-store-b-v2@2x.jpg"
                        alt="Le (Re)store - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-re-store-b-v2.jpg" alt="Le (Re)store - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#actumag-renouveau-femme">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-renouveau-femme-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-renouveau-femme-v2@2x.jpg"
                        alt="Le renouveau de l'offre femme - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-renouveau-femme-v2.jpg" alt="Le renouveau de l'offre femme - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#actumag-creative-galerie-mode">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-creative-galerie-mode-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-creative-galerie-mode-v2@2x.jpg"
                        alt="Nouvel espace mode femme - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-creative-galerie-mode-v2.jpg" alt="Nouvel espace mode femme - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
                <div class="luxe-col-mobile-4">
                  <a href="#actumag-marques-digitales">
                    <figure class="image">
                      <img class="b-lazy"
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-marques-digitales-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-marques-digitales-v2@2x.jpg"
                        alt="L'arrivée des marques digitales - Galeries Lafayette">
                        <!-- Fallback for non JavaScript browsers -->
                        <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/actumag/cover_actumag-marques-digitales-v2.jpg" alt="L'arrivée des marques digitales - Galeries Lafayette" width="120" height="214" /></noscript>
                    </figure>
                  </a>
                </div>
              </div>              
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="logo-anim is-fixed-top">
      <div class="tuto is-hidden-desktop">
        <div class="luxe-row no-gutter luxe-middle-mobile is-fullheight text-center tuto-container">
          <div class="luxe-col-mobile-12">
            <h3>Comment ça fonctionne ?</h3>
          </div>
          <div class="luxe-col-mobile-12">
            <div class="luxe-row no-gutter luxe-middle-mobile is-fullheight tuto-nav">
              <div class="luxe-col-mobile-6 is-fullheight">
                <button class="back is-fullheight">
                  <span class="hand-tap"></span>
                  <span>Tapez à gauche <br />pour revenir</span>
                </button>
              </div>
              <div class="luxe-col-mobile-6 is-fullheight">
                <button class="start is-fullheight">
                  <span class="hand-tap hand-right"></span>
                  <span>Tapez à droite <br />pour avancer</span>
                </button>
              </div>
            </div>
          </div>
          <div class="luxe-col-mobile-12">
            <button class="button primary is-width-auto start">J'ai compris</button>  
          </div>
        </div>
      </div>
      <figure class="image logo">
        <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/logo-gl.svg"
          alt="Galeries Lafayette">
      </figure>
    </section>
    
    <section class="reinventons-splash" id="intro">
      <div class="container">
        <div class="luxe-row no-gutter luxe-middle-mobile">
          <div class="luxe-col-mobile-12 col-visu">
            <button class="trigger-tuto-hover-video has-bg-image is-hidden-desktop"></button>
            <div class="bg-video-container">
              <iframe class="bg-video" id="splash-vid" src="https://player.vimeo.com/video/593196896?background=1&autopause=0" width="425" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
            </div>
            <div class="content">
              <h1 class="boxed-text" style="top: 13px;">
                <span style="padding-bottom: 6px;">Réinventons</span> <br />
                <span style="border-top: none;padding-bottom: 12px;top: -13px;line-height: 39px;">nous</span>
              </h1>
              <p class="subtitle boxed-text">
                <span style="padding-bottom: 2px;">Un magasin grand par ceux qui sont dedans</span>
              </p>
              <div class="trigger-tuto is-hidden-desktop">
                <button class="button primary is-width-auto">D&eacute;marrer L&apos;exp&eacute;rience<span class="icon has-arrow"></span></button>
              </div>
            </div>
          </div>          
        </div>
      </div>
    </section>
    
    <section class="reinventons-manifesto manifesto section hidden-section has-background-generique" id="manifesto">
      <div class="reinventons-nav">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev close"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-middle-desktop luxe-reverse is-fullheight">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu">
            <div class="has-bg-image">&nbsp;</div>
            <div class="content">
              <h2 class="boxed-text" style="top: 18px;">
                <span style="padding-top: 4px; padding-bottom: 4px; line-height: 36px;">Le grand</span> <br />
                <span style="border-top: none; padding-bottom: 4px; top: -6px; line-height: 36px;">magasin</span> <br />
                <span style="border-top: none; padding-bottom: 6px; top: -12px; line-height: 36px;">de tous</span>
              </h2>
            </div>
          </div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-content">
            <div class="content luxe-row no-gutter is-fullheight">
              <div class="luxe-col-mobile-12">
                <div class="txt-container">
                  <p>En septembre, les Galeries Lafayette se vivent en GRAND. Révélateurs des modes, les grands magasins et galerieslafayette.com mettent en scène tous les styles avec une offre encore plus riche, plus créative et plus responsable, de nouvelles marques et de nouveaux services pour se réinventer. En ligne comme en magasin, la mode bat plus fort que jamais.
                    
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-manifesto manifesto-vid section hidden-section has-background-generique" id="manifesto-vid">
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-middle-mobile is-fullheight">
          <div class="luxe-col-mobile-12 is-fullheight">
            <div class="video-container">                
              <div class="mobile-vid is-hidden-desktop" id="manifesto-mobile-vid-container">
                <button class="manage-sound is-muted"></button>
                <video id="manifesto-mobile-vid" class="bg-video" poster="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/manifesto/manifesto-vid-cover_M@2x.jpg" preload="auto" loop playsinline muted>
                  <source src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/manifesto/manifesto_M.mp4" type="video/mp4">
                </video>
              </div> 
                
              <div class="desktop-vid video-with-controls is-hidden-touch">
                <iframe class="bg-video b-lazy" data-src="https://player.vimeo.com/video/596004879?controls=1&autoplay=1&loop=1&muted=0&speed=0&autopause=0" width="1400" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-manifesto section hidden-section programme has-background-generique is-hidden-desktop" id="programme">
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-middle-desktop luxe-reverse is-fullheight">
          <div class="luxe-col-mobile-12 text-center">
            <h2 class="boxed-text is-fullwidth">
              <span style="padding-bottom: 3px;">Au programme</span>
            </h2>
          </div>
          <div class="luxe-col-mobile-12">
            <ul class="main-links">
              <li><a href="#mode-main">Continuer<span class="icon has-arrow"></span></a></li>
              <li><a href="#mode-main">les nouveautés mode<span class="icon has-arrow"></span></a></li>
              <li><a href="#shoes-main">les nouveautés chaussures<span class="icon has-arrow"></span></a></li>
              <li><a href="#responsable-main">la mode responsable<span class="icon has-arrow"></span></a></li>
              <li><a href="#actumag-main">les actualités en magasin<span class="icon has-arrow"></span></a></li>
            </ul>
          </div>
        </div>
      </div>
    </section> 
      
    <section class="reinventons-mode section hidden-section mode-main has-background-affranchies" id="mode-main">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter is-fullheight main-card">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu">
            <div class="has-bg-image">&nbsp;</div>
            <div class="content">
              <h2 class="boxed-text">
                <span style="padding-bottom: 3px;">Les nouveautés</span> <br />
                <span style="border-top: none;padding-bottom: 9px;top: -9px;line-height: 30px;">femme <span class="is-hidden-desktop">→</span></span>
              </h2>
              <p class="subtitle boxed-text">
                <span style="padding-bottom: 2px;">Le grand magasin des affranchies</span></p>
            </div>
          </div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-logo">
            <figure class="image logo-gl-white">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/logo-gl-blanc.svg"
                alt="Galeries Lafayette">
            </figure>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-mode section hidden-section" id="mode-proenza-schouler">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-middle-mobile luxe-reverse full-image-card is-fullheight">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu has-text-light">
            <div class="has-bg-image">&nbsp</div>
            <div class="content text-center">
              <div class="tag title-has-two-lines is-hidden">Nouveauté</div>
              <a href="https://www.galerieslafayette.com/b/proenza+schouler" target="_blank">
                <h2 class="boxed-text no-float" style="top: 8px;">
                  <span style="padding-bottom: 3px;">Proenza Schouler</span>
                  <span style="padding-bottom: 7px;border-top: none;top: -8px;line-height: 32px;">White label</span>
                </h2>
              </a>
              <p class="has-text-white">L’allure Proenza Schouler : sportswear, <span class="nowrap">avant-gardiste</span> et&nbsp;sophistiquée.</p>
              <a href="https://www.galerieslafayette.com/b/proenza+schouler" class="button primary outlined is-width-auto" target="_blank">Découvrir la marque</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-mode section hidden-section" id="mode-proenza-schouler-produits">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter is-fullheight">
          <div class="is-hidden-touch luxe-col-desktop-6"></div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 has-background-grey">
            <div class="luxe-row no-gutter luxe-middle-mobile product-card">
              <div class="luxe-col-mobile-12 col-title">
                <div class="content">
                  <h3>Les produits phare</h3>
                  <p class="has-text-grey is-smaller">Proenza Schouler White Label</p>
                </div>
              </div>
              <div class="luxe-col-mobile-12 col-content">
                <a href="https://www.galerieslafayette.com/b/proenza+schouler" class="content luxe-row no-gutter text-center" target="_blank">
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/mode-proenza-produits.gif"
                    alt="Proenza Schouler White Label - Galeries Lafayette">
                  </figure>
                </a>
              </div>
              <div class="luxe-col-mobile-12">
                <div class="cta-container">
                  <a href="https://www.galerieslafayette.com/b/proenza+schouler" class="button outlined is-width-auto" target="_blank">voir toute la Sélection</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-mode section hidden-section" id="mode-ganni">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-middle-mobile luxe-reverse full-image-card is-fullheight">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu has-text-light">
            <div class="has-bg-image">&nbsp</div>
            <div class="content text-center">
              <a href="https://www.galerieslafayette.com/b/ganni" target="_blank">
                <h2 class="boxed-text no-float">
                  <span style="padding-bottom: 3px;">Ganni</span>
                </h2>
              </a>
              <p class="has-text-white">Robes fleuries, pièces en maille arc-en-ciel ou maillots de bain flashy, impossible de ne pas craquer pour ce vestiaire 100% fun et accessible.</p>
              <a href="https://www.galerieslafayette.com/b/ganni" class="button primary outlined is-width-auto" target="_blank">Découvrir la marque</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-mode section hidden-section" id="mode-ganni-produits">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter is-fullheight">
          <div class="is-hidden-touch luxe-col-desktop-6"></div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 has-background-grey">
            <div class="luxe-row no-gutter luxe-middle-mobile product-card">
              <div class="luxe-col-mobile-12 col-title">
                <div class="content">
                  <h3>Les produits phare</h3>
                  <p class="has-text-grey is-smaller">Ganni</p>
                </div>
              </div>
              <div class="luxe-col-mobile-12 col-content">
                <a href="https://www.galerieslafayette.com/b/ganni" class="content luxe-row no-gutter text-center" target="_blank">
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/mode-ganni-produits.gif"
                    alt="Ganni - Galeries Lafayette">
                  </figure>
                </a>
              </div>
              <div class="luxe-col-mobile-12">
                <div class="cta-container">
                  <a href="https://www.galerieslafayette.com/b/ganni" class="button outlined is-width-auto" target="_blank">voir toute la Sélection</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-mode section hidden-section" id="mode-designers-remix">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-middle-mobile luxe-reverse full-image-card is-fullheight">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu has-text-light">
            <div class="has-bg-image">&nbsp</div>
            <div class="content text-center">
              <a href="https://www.galerieslafayette.com/b/designers+remix" target="_blank">  
                <h2 class="boxed-text no-float">
                  <span style="padding-bottom: 3px;">DESIGNERS, REMIX</span>
                </h2>
              </a>
              <p class="has-text-white">Qualité des matières, finitions soignées, collections originales et coupes branchées, la griffe empreinte le meilleur des tendances pour créer des vêtements au style affirmé.</p>
              <a href="https://www.galerieslafayette.com/b/designers+remix" class="button primary outlined is-width-auto" target="_blank">Découvrir la marque</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-mode section hidden-section" id="mode-parisienne-et-alors">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-middle-mobile luxe-reverse full-image-card is-fullheight">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu has-text-light">
            <div class="has-bg-image">&nbsp</div>
            <div class="content text-center">
              <a href="https://www.galerieslafayette.com/b/parisienne+alors" target="_blank">
                <h2 class="boxed-text no-float">
                  <span style="padding-bottom: 3px;">Parisienne & Alors</span>
                </h2>
              </a>
              <p class="has-text-white">La marque engagée imaginée par Laury Thilleman dévoile un vestiaire 100% Made in France tourné vers l’avenir.</p>
              <a href="https://www.galerieslafayette.com/b/parisienne+alors" class="button primary outlined is-width-auto" target="_blank">Découvrir la marque</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-mode section hidden-section" id="mode-parisienne-et-alors-produits">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter is-fullheight">
          <div class="is-hidden-touch luxe-col-desktop-6"></div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 has-background-grey">
            <div class="luxe-row no-gutter luxe-middle-mobile product-card">
              <div class="luxe-col-mobile-12 col-title">
                <div class="content">
                  <h3>Les produits phare</h3>
                  <p class="has-text-grey is-smaller">Parisienne & Alors</p>
                </div>
              </div>
              <div class="luxe-col-mobile-12 col-content">
                <a class="content luxe-row no-gutter text-center" href="https://www.galerieslafayette.com/b/parisienne+alors" target="_blank">
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/mode-parisienne-produits.gif"
                    alt="Parisienne & Alors- Galeries Lafayette">
                  </figure>
                </a>
              </div>
              <div class="luxe-col-mobile-12">
                <div class="cta-container">
                  <a href="https://www.galerieslafayette.com/b/parisienne+alors" class="button outlined is-width-auto" target="_blank">voir toute la Sélection</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-mode section hidden-section" id="mode-musier">
      <div class="stepper inverted">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav inverted active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-middle-mobile luxe-reverse full-image-card is-fullheight">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu has-text-light">
            <div class="has-bg-image">&nbsp</div>
            <div class="content text-center">
              <a href="https://www.galerieslafayette.com/b/musier" target="_blank">
                <h2 class="boxed-text no-float">
                  <span style="padding-bottom: 3px;">Musier</span>
                </h2>
              </a>
              <p class="has-text-white">Amoureuse inconditionnelle de Paris, Anne-Laure Mais fondatrice de Musier, puise son inspiration auprès des muses qui l’entourent au quotidien.</p>
              <a href="https://www.galerieslafayette.com/b/musier" class="button primary outlined is-width-auto" target="_blank">Découvrir la marque</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-mode section hidden-section" id="mode-musier-produits">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter is-fullheight">
          <div class="is-hidden-touch luxe-col-desktop-6"></div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 has-background-grey">
            <div class="luxe-row no-gutter luxe-middle-mobile product-card">
              <div class="luxe-col-mobile-12 col-title">
                <div class="content">
                  <h3>Les produits phare</h3>
                  <p class="has-text-grey is-smaller">Musier</p>
                </div>
              </div>
              <div class="luxe-col-mobile-12 col-content">
                <a class="content luxe-row no-gutter text-center" href="https://www.galerieslafayette.com/b/musier" target="_blank">
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/mode/mode-musier-produits.gif"
                    alt="Musier - Galeries Lafayette">
                  </figure>
                </a>
              </a>
              <div class="luxe-col-mobile-12">
                <div class="cta-container">
                  <a href="https://www.galerieslafayette.com/b/musier" class="button outlined is-width-auto" target="_blank">voir toute la Sélection</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-mode reinventons-replay section hidden-section is-hidden-desktop has-background-affranchies" id="mode-replay">
      <div class="reinventons-nav inverted active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container">
        <div class="luxe-row no-gutter luxe-middle-mobile text-center has-text-white">
          <div class="luxe-col-mobile-12 col-more-news">
            <a href="https://www.galerieslafayette.com/t/le-renouveau-de-l-offre-femme" class="cta-container" target="_blank">
              <p class="is-bold has-text-white">Découvrir toutes <br>les nouveautés mode femme</p>
              <button class="button primary inverted is-width-auto" target="_blank">Voir la sélection</button>
            </a>
          </div>
          <div class="luxe-col-mobile-12">
            <div class="luxe-row no-gutter col-restart">
              <div class="luxe-col-mobile-6">
                <button class="start back">
                  <span class="nav-arrow arrow-left inverted"></span>
                  <span>Revenir</span>
                </button>
              </div>
              <div class="luxe-col-mobile-6">
                <button class="start">
                  <span class="nav-arrow inverted"></span>
                  <span>Avancer</span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  
  
  
  
    <section class="reinventons-shoes section hidden-section has-background-collectionneuses" id="shoes-main">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-reverse is-fullheight main-card">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu">
            <div class="has-bg-image">&nbsp;</div>
            <div class="content has-text-right">
              <h2 class="boxed-text">
                <span style="padding-bottom: 3px;">Les nouveautés</span> <br />
                <span style="border-top: none;padding-bottom: 9px;top: -9px;line-height: 30px;">chaussures <span class="is-hidden-desktop">→</span></span>
              </h2>
              <p class="subtitle boxed-text">
                <span style="padding-bottom: 2px;">Le grand magasin des collectionneuses</span></p>
            </div>
          </div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-logo">
            <figure class="image logo-gl-white">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/logo-gl-blanc.svg"
                alt="Galeries Lafayette">
            </figure>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-shoes section hidden-section" id="shoes-golden-goose">
      <div class="stepper inverted">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav inverted active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-middle-mobile full-image-card is-fullheight">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu has-text-light">
            <div class="has-bg-image">&nbsp</div>
            <div class="content text-center">
              <a href="https://www.galerieslafayette.com/b/golden+goose" target="_blank">
                <h2 class="boxed-text no-float">
                  <span style="padding-bottom: 3px;">Golden Goose</span>
                </h2>
              </a>
              <p class="has-text-white">Déclinées dans une palette de couleurs flashy, ces paires made in Italie, au style inimitable et à la qualité irréprochable, s'adaptent à toutes les envies mode.</p>
              <a href="https://www.galerieslafayette.com/b/golden+goose" class="button primary outlined is-width-auto" target="_blank">Découvrir la marque</a>
            </div>
          </div>
        </div>
      </div>
    </section>
     
    <section class="reinventons-shoes section hidden-section" id="shoes-roger-vivier">
      <div class="stepper inverted">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav inverted active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-middle-mobile full-image-card is-fullheight">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu has-text-light">
            <div class="has-bg-image">&nbsp</div>
            <div class="content text-center">
              <a href="https://www.galerieslafayette.com/b/roger+vivier" target="_blank">
                <h2 class="boxed-text no-float">
                  <span style="padding-bottom: 3px;">Roger Vivier</span>
                </h2>
              </a>
              <p class="has-text-white"> Ornées de la boucle iconique en métal ou en cristal précieux, les chaussures Roger Vivier sont réalisées à la main par des maîtres-artisans.</p>
              <a href="https://www.galerieslafayette.com/b/roger+vivier" class="button primary outlined is-width-auto" target="_blank">Découvrir la marque</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-shoes section hidden-section" id="shoes-roger-vivier-produits">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-reverse is-fullheight">
          <div class="is-hidden-touch luxe-col-desktop-6"></div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 has-background-grey">
            <div class="luxe-row no-gutter luxe-middle-mobile product-card">
              <div class="luxe-col-mobile-12 col-title">
                <div class="content">
                  <h3>Les produits phare</h3>
                  <p class="has-text-grey is-smaller">Roger Vivier</p>
                </div>
              </div>
              <div class="luxe-col-mobile-12 col-content">
                <a class="content luxe-row no-gutter text-center" href="https://www.galerieslafayette.com/b/roger+vivier" target="_blank">
                  <figure class="image">
                    <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/shoes/shoes-roger-vivier-produits.gif"
                    alt="Roger Vivier - Galeries Lafayette">
                  </figure>
                </a>
              </div>
              <div class="luxe-col-mobile-12">
                <div class="cta-container">
                  <a href="https://www.galerieslafayette.com/b/roger+vivier" class="button outlined is-width-auto" target="_blank">voir toute la Sélection</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-shoes section video-section hidden-section" id="shoes-chloe">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-middle-mobile full-image-card is-fullheight">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu">
            <div class="video-container">
              <div class="mobile-vid is-hidden-desktop">
                <iframe class="bg-video" id="chloe-mobile-vid" width="425" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen="" data-vidsrc="https://player.vimeo.com/video/597122562?background=1"></iframe>
              </div>
              <div class="desktop-vid is-hidden-touch">
                <iframe class="bg-video b-lazy" data-src="https://player.vimeo.com/video/597122562?background=1" width="720" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
              </div>
            </div>
            <div class="content text-center">
              <a href="https://www.galerieslafayette.com/b/chloe" target="_blank">
                <h2 class="boxed-text no-float">
                  <span style="padding-bottom: 3px;">Chloé</span>
                </h2>
              </a>
              <p class="">Inspirations romantiques, élégance naturelle et détails raffinés.</p>
              <a href="https://www.galerieslafayette.com/b/chloe" class="button outlined is-width-auto" target="_blank">Découvrir la marque</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-shoes section hidden-section" id="shoes-vagabond">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-middle-mobile full-image-card is-fullheight">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu has-text-light">
            <div class="has-bg-image">&nbsp</div>
            <div class="content text-center">
              <a href="https://www.galerieslafayette.com/b/vagabond" target="_blank">
                <h2 class="boxed-text no-float">
                  <span style="padding-bottom: 3px;">Vagabond</span>
                </h2>
              </a>
              <p class="has-text-white">Des chaussures au design léché pensées en Suède qui intègrent parfaitement la notion de tendance et de responsabilité. </p>
              <a href="https://www.galerieslafayette.com/b/vagabond" class="button primary outlined is-width-auto" target="_blank">Découvrir la marque</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-shoes section hidden-section" id="shoes-by-far">
      <div class="stepper inverted">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav inverted active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-middle-mobile full-image-card is-fullheight">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu has-text-light">
            <div class="has-bg-image">&nbsp</div>
            <div class="content text-center">
              <a href="https://www.galerieslafayette.com/b/by+far" target="_blank">
                <h2 class="boxed-text no-float">
                  <span style="padding-bottom: 3px;">By Far</span>
                </h2>
              </a>
              <p class="has-text-white">Le label bulgare imagine des chaussures et des sacs à la féminité subtile et aux détails originaux.</p>
              <a href="https://www.galerieslafayette.com/b/by+far" class="button primary outlined is-width-auto" target="_blank">Découvrir la marque</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-shoes reinventons-replay section hidden-section is-hidden-desktop has-background-collectionneuses" id="shoes-replay">
      <div class="reinventons-nav inverted active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container">
        <div class="luxe-row no-gutter luxe-middle-mobile text-center has-text-white">
          <div class="luxe-col-mobile-12 col-more-news">
            <a href="https://www.galerieslafayette.com/t/le-renouveau-de-l-offre-chaussures" class="cta-container"  target="_blank">
              <p class="is-bold has-text-white">Découvrir toutes <br>les nouveautés chaussures</p>
              <button class="button primary inverted is-width-auto" target="_blank">Voir la sélection</button>
            </a>
          </div>
          <div class="luxe-col-mobile-12">
            <div class="luxe-row no-gutter col-restart">
              <div class="luxe-col-mobile-6">
                <button class="start back">
                  <span class="nav-arrow arrow-left inverted"></span>
                  <span>Revenir</span>
                </button>
              </div>
              <div class="luxe-col-mobile-6">
                <button class="start">
                  <span class="nav-arrow inverted"></span>
                  <span>Avancer</span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    
    
    
    <section class="reinventons-responsable section hidden-section has-background-visionnaires" id="responsable-main">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter is-fullheight main-card">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu">
            <div class="has-bg-image">&nbsp</div>            
            <div class="content">
              <h2 class="boxed-text">
                <span style="border-bottom: none; top: -5px; z-index: 1;">La mode</span> <br />
                <span style="padding-bottom: 4px; top: -9px;">responsable <span class="is-hidden-desktop">→</span></span>
              </h2>
              <p class="subtitle boxed-text">
                <span style="padding-bottom: 2px;">La sélection engagée</span></p>
            </div>
          </div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-logo">
            <figure class="image logo-gl-white">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/logo-gl-blanc.svg"
                alt="Galeries Lafayette">
            </figure>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-responsable section hidden-section" id="responsable-go-for-good">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-middle-mobile luxe-reverse full-content-card is-fullheight">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 has-background-visionnaires is-fullheight">
            <div class="content">
              <h2 class="boxed-text has-text-fat">
                <span style="border-bottom: none; bottom: -8px; z-index: 1; line-height: 48px; padding-top: 6px;">La mode</span> <br />
                <span style="padding-bottom: 4px; line-height: 62px;">responsable</span>
              </h2>
              <p>Les Galeries Lafayette renforcent leur engagement pour une mode plus responsable avec leur label Go for Good et une nouvelle offre inédite de seconde main.
                
                
              </p>
              <p><span class="is-stronger">GOOD FOR GOOD</span><br />
                Lancé par les Galeries Lafayette en 2018, le label Go for Good propose une sélection mode, beauté et lifestyle qui a un impact moindre sur l'environnement, soutient la production locale ou contribue au développement social. Une sélection qui fait rimer style et responsabilité.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-responsable section hidden-section" id="responsable-recc-paris">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container">
        <div class="luxe-row no-gutter">
          <div class="is-hidden-touch luxe-col-desktop-6"></div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 has-background-white">
            <div class="luxe-row no-gutter luxe-middle-mobile half-content-card">
              <div class="luxe-col-mobile-12 col-visu">
                <div class="has-bg-image">&nbsp</div>
                <div class="content">
                  <div class="go-for-good"></div>
                  <a href="https://www.galerieslafayette.com/b/recc" target="_blank">
                    <h2 class="boxed-text">
                      <span style="padding-bottom: 3px;">Recc Paris</span>
                    </h2>
                  </a>
                </div>
              </div>
              <div class="luxe-col-mobile-12 col-content">
                <div class="content luxe-row no-gutter">
                  <div class="luxe-col-mobile-12">
                    <div class="txt-container">
                      <p class="has-text-grey">Lancée par l’influenceuse Caroline Receveur, RECC PARIS revisite les essentiels de la citadine. Dans ce vestiaire désirable, des pièces féminines et décontractées et une confection made in Paris pour favoriser la production locale.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="luxe-col-mobile-12">
                <div class="cta-container">
                  <a href="https://www.galerieslafayette.com/b/recc" class="button outlined" target="_blank">Découvrir la marque</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-responsable section hidden-section" id="responsable-justine-clenquet">
      <div class="stepper inverted">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav inverted active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container">
        <div class="luxe-row no-gutter">
          <div class="is-hidden-touch luxe-col-desktop-6"></div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 has-background-white">
            <div class="luxe-row no-gutter luxe-middle-mobile half-content-card">
              <div class="luxe-col-mobile-12 col-visu">
                <div class="has-bg-image">&nbsp</div>
                <div class="content">
                  <div class="go-for-good"></div>
                  <a href="https://www.galerieslafayette.com/b/justine+clenquet" target="_blank">
                    <h2 class="boxed-text">
                      <span style="padding-bottom: 3px;">Justine Clenquet</span>
                    </h2>
                  </a>
                </div>
              </div>
              <div class="luxe-col-mobile-12 col-content">
                <div class="content luxe-row no-gutter">
                  <div class="luxe-col-mobile-12">
                    <div class="txt-container">
                      <p class="has-text-grey">La créatrice française Justine Clenquet imagine des bijoux élégants à porter pour toutes les occasions. Sa signature ? Des créations glamour et fantaisies confectionnées à la main en France en or 24 carats ou à partir de métaux trempés au palladium.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="luxe-col-mobile-12">
                <div class="cta-container">
                  <a href="https://www.galerieslafayette.com/b/justine+clenquet" class="button outlined" target="_blank">Découvrir la marque</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-responsable section hidden-section" id="responsable-aveda">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container">
        <div class="luxe-row no-gutter">
          <div class="is-hidden-touch luxe-col-desktop-6"></div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 has-background-white">
            <div class="luxe-row no-gutter luxe-middle-mobile half-content-card">
              <div class="luxe-col-mobile-12 col-visu">
                <div class="has-bg-image">&nbsp</div>
                <div class="content">
                  <div class="go-for-good"></div>
                  <a href="https://www.galerieslafayette.com/b/aveda" target="_blank">
                    <h2 class="boxed-text">
                      <span style="padding-bottom: 3px;">Aveda</span>
                    </h2>
                  </a>
                </div>
              </div>
              <div class="luxe-col-mobile-12 col-content">
                <div class="content luxe-row no-gutter">
                  <div class="luxe-col-mobile-12">
                    <div class="txt-container">
                      <p class="has-text-grey">Avec ses gammes 100% vegan, AVEDA propose des soins qui puisent leur efficacité dans la force des plantes. Spécialiste du cheveu, la marque propose aussi des produits pour le visage, le corps ainsi qu’une ligne de maquillage et de bougies. Des produits bons pour le corps et la planète.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="luxe-col-mobile-12">
                <div class="cta-container">
                  <a href="https://www.galerieslafayette.com/b/aveda" class="button outlined" target="_blank">Découvrir la marque</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-responsable reinventons-replay section hidden-section is-hidden-desktop has-background-visionnaires" id="responsable-replay">
      <div class="reinventons-nav inverted active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container">
        <div class="luxe-row no-gutter luxe-middle-mobile text-center has-text-white">
          <div class="luxe-col-mobile-12 col-more-news">
            <a href="https://www.galerieslafayette.com/t/le-renouveau-go-for-good" class="cta-container" target="_blank">
              <p class="is-bold has-text-white">Découvrir toutes <br>les nouveautés mode&nbsp;responsable</p>
              <button class="button primary inverted is-width-auto" target="_blank">Voir la sélection</button>
            </a>
          </div>
          <div class="luxe-col-mobile-12">
            <div class="luxe-row no-gutter col-restart">
              <div class="luxe-col-mobile-6">
                <button class="start back">
                  <span class="nav-arrow arrow-left inverted"></span>
                  <span>Revenir</span>
                </button>
              </div>
              <div class="luxe-col-mobile-6">
                <button class="start">
                  <span class="nav-arrow inverted"></span>
                  <span>Avancer</span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    
    
    
    <section class="reinventons-actumag section hidden-section has-background-actumag" id="actumag-main">
      <div class="stepper inverted">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav inverted active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-reverse is-fullheight main-card">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu">
            <div class="has-bg-image">&nbsp</div>
            <div class="content has-text-right">
              <h2 class="boxed-text">
                <span style="padding-bottom: 4px;">Votre magasin</span> <br />
                <span style="border-top: none;padding-bottom: 9px;top: -10px;line-height: 30px;">se réinvente <span class="is-hidden-desktop">→</span></span>
              </h2>
              <p class="subtitle boxed-text">
                <span style="padding-bottom: 2px;">galeries lafayette paris haussmann</span>
              </p>
            </div>
          </div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-logo">
            <figure class="image logo-gl-white">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reinventons-nous/logo-gl-blanc.svg"
                alt="Galeries Lafayette">
            </figure>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-actumag section hidden-section" id="actumag-espace-chaussures">
      <div class="stepper inverted">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav inverted active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-middle-mobile full-image-card is-fullheight">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu">
            <div class="has-bg-image">&nbsp</div>
            <div class="content" style="margin-bottom:40px;">
              <p class="subtitle boxed-text" style="top: -6px;">
                <span style="padding-bottom: 2px;">Paris Haussmann</span>
              </p>
              <br />
              <h2 class="boxed-text" style="top: 9px;">
                <span style="padding-bottom: 3px;">Nouvel espace</span> <br />
                <span style="border-top: none;padding-bottom: 9px;top: -9px;line-height: 30px;">chaussures <span class="is-hidden-desktop">→</span></span>
              </h2>
              <p class="subtitle boxed-text">
                <span style="padding-bottom: 2px;">Le plus grand espace chaussures</span> <br />
                <span style="border-top: none;padding-bottom: 6px;top: -9px;line-height: 11px;">en europe</span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-actumag section hidden-section" id="actumag-designer-galerie">
      <div class="stepper inverted">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav inverted active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container">
        <div class="luxe-row no-gutter luxe-reverse">
          <div class="is-hidden-touch luxe-col-desktop-6"></div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 has-background-white">
            <div class="luxe-row no-gutter luxe-middle-mobile half-content-card">
              <div class="luxe-col-mobile-12 col-visu">
                <div class="has-bg-image">&nbsp</div>
                <div class="content">
                  <p class="subtitle boxed-text">
                    <span style="padding-bottom: 2px;">Nouvel espace chaussures</span>
                  </p>
                  <h2 class="boxed-text">
                  <span style="padding-bottom: 3px;">La&nbsp;Designer&nbsp;Galerie</span>
                  </h2>
                </div>
              </div>
              <div class="luxe-col-mobile-12 col-content">
                <div class="content luxe-row no-gutter">
                  <div class="luxe-col-mobile-12">
                    <div class="txt-container">
                      <p class="has-text-grey">Évoquant l’esthétique emblématique des amphithéâtres, ce nouvel espace met en lumière les souliers iconiques sur des gradins en velours. Maison Margiela, The Row, Burberry ou Alaïa, les plus belles maisons sont à retrouver dans cet écrin luxueux, glamour et intimiste.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="luxe-col-mobile-12">
                <div class="cta-container">
                  <a href="https://haussmann.galerieslafayette.com/nouvel-espace-chaussures-femme" class="button outlined" target="_blank">En savoir plus</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-actumag section hidden-section" id="actumag-creative-galerie">
      <div class="stepper inverted">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav inverted active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container">
        <div class="luxe-row no-gutter luxe-reverse">
          <div class="is-hidden-touch luxe-col-desktop-6"></div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 has-background-white">
            <div class="luxe-row no-gutter luxe-middle-mobile half-content-card">
              <div class="luxe-col-mobile-12 col-visu">
                <div class="has-bg-image">&nbsp</div>
                <div class="content">
                  <p class="subtitle boxed-text">
                    <span style="padding-bottom: 2px;">Nouvel espace chaussures</span>
                  </p>
                  <h2 class="boxed-text">
                  <span style="padding-bottom: 3px;">La&nbsp;Creative&nbsp;Galerie</span>
                  </h2>
                </div>
              </div>
              <div class="luxe-col-mobile-12 col-content">
                <div class="content luxe-row no-gutter">
                  <div class="luxe-col-mobile-12">
                    <div class="txt-container">
                      <p class="has-text-grey">Dans ce salon boudoir rose poudré, cette sélection de souliers s’adresse aux clientes audacieuses. Des maisons incontournables comme MM6, By Far ou Ganni au chic parisien de Vanessa Bruno et Soeur sans oublier les nouveaux labels exclusifs tels que Alohas ou Youyou, impossible de ne pas craquer.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="luxe-col-mobile-12">
                <div class="cta-container">
                  <a href="https://haussmann.galerieslafayette.com/nouvel-espace-chaussures-femme" class="button outlined" target="_blank">En savoir plus</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-actumag section hidden-section" id="actumag-sneaker-galerie">
      <div class="stepper inverted">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav inverted active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container">
        <div class="luxe-row no-gutter luxe-reverse">
          <div class="is-hidden-touch luxe-col-desktop-6"></div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 has-background-white">
            <div class="luxe-row no-gutter luxe-middle-mobile half-content-card">
              <div class="luxe-col-mobile-12 col-visu">
                <div class="has-bg-image">&nbsp</div>
                <div class="content">
                  <p class="subtitle boxed-text">
                    <span style="padding-bottom: 2px;">Nouvel espace chaussures</span>
                  </p>
                  <h2 class="boxed-text">
                  <span style="padding-bottom: 3px;">La&nbsp;Sneakers&nbsp;Galerie</span>
                  </h2>
                </div>
              </div>
              <div class="luxe-col-mobile-12 col-content">
                <div class="content luxe-row no-gutter">
                  <div class="luxe-col-mobile-12">
                    <div class="txt-container">
                      <p class="has-text-grey">Avec son mur lumineux, l’espace dévoile une large sélection de baskets présentées comme des œuvres d'art. Des marques emblématiques aux nouveaux créateurs indépendants, les passionnés de sneakers pourront également découvrir les marques responsables labellisées Go For Good.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="luxe-col-mobile-12">
                <div class="cta-container">
                  <a href="https://haussmann.galerieslafayette.com/nouvel-espace-chaussures-femme" class="button outlined" target="_blank">En savoir plus</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-actumag section hidden-section" id="actumag-re-store">
      <div class="stepper inverted">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav inverted active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-middle-mobile full-image-card is-fullheight">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu">
            <div class="has-bg-image">&nbsp</div>
            <div class="content" style="margin-bottom:40px;">
              <p class="subtitle boxed-text" style="top: -12px;">
                <span style="padding-bottom: 2px;">Paris Haussmann</span>
              </p>
              <br />
              <h2 class="boxed-text" style="top: 4px;">
                <span style="padding-bottom: 4px; border-bottom: none; z-index: 1;">Le (Re)Store</span> <br />
                <span style="padding-bottom: 5px;top: -4px;line-height: 36px;">Galeries&nbsp;Lafayette&nbsp;<span class="is-hidden-desktop">→</span></span>
              </h2>
              <p class="subtitle boxed-text">
                <span style="padding-bottom: 2px;">Le nouvel espace seconde main</span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-actumag section hidden-section" id="actumag-re-store-b">
      <div class="stepper inverted">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav inverted active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container">
        <div class="luxe-row no-gutter luxe-reverse">
          <div class="is-hidden-touch luxe-col-desktop-6"></div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 has-background-white">
            <div class="luxe-row no-gutter luxe-middle-mobile half-content-card">
              <div class="luxe-col-mobile-12 col-visu">
                <div class="has-bg-image">&nbsp</div>
                <div class="content">
                  <h2 class="boxed-text">
                    <span style="padding-bottom: 4px; border-bottom: none; z-index: 1;">Le (Re)Store</span> <br />
                    <span style="padding-bottom: 5px;top: -4px;line-height: 36px;">Galeries&nbsp;Lafayette</span>
                  </h2>
                </div>
              </div>
              <div class="luxe-col-mobile-12 col-content">
                <div class="content luxe-row no-gutter">
                  <div class="luxe-col-mobile-12">
                    <div class="txt-container">
                      <p class="has-text-grey">Découvrez LE [RE]STORE GALERIES LAFAYETTE. Ce nouvel espace dédié à la seconde main et à la mode responsable est situé au 3ème étage du Magasin Coupole. Conçu en partenariat avec des acteurs incontournables de la seconde main et du vintage, il dévoile une extraordinaire sélection.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="luxe-col-mobile-12">
                <div class="cta-container">
                  <a href="https://haussmann.galerieslafayette.com/espace-re-store" class="button outlined" target="_blank">En savoir plus</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-actumag section hidden-section" id="actumag-renouveau-femme">
      <div class="stepper inverted">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav inverted active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container is-fullheight">
        <div class="luxe-row no-gutter luxe-middle-mobile full-image-card is-fullheight">
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 col-visu">
            <div class="has-bg-image">&nbsp</div>
            <div class="content" style="margin-bottom:40px;">
              <p class="subtitle boxed-text" style="top: -12px;">
                <span style="padding-bottom: 2px;">Paris Haussmann</span>
              </p>
              <br />
              <h2 class="boxed-text" style="top: 4px;">
                <span style="padding-bottom: 4px; border-bottom: none; z-index: 1;">Le Renouveau</span> <br />
                <span style="padding-bottom: 5px;top: -4px;line-height: 36px;">de&nbsp;l'offre&nbsp;femme&nbsp;<span class="is-hidden-desktop">→</span></span>
              </h2>
              <p class="subtitle boxed-text">
                <span style="padding-bottom: 2px;">Plus de 30 nouvelles marques à découvrir</span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-actumag section hidden-section" id="actumag-creative-galerie-mode">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container">
        <div class="luxe-row no-gutter luxe-reverse">
          <div class="is-hidden-touch luxe-col-desktop-6"></div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 has-background-white">
            <div class="luxe-row no-gutter luxe-middle-mobile half-content-card">
              <div class="luxe-col-mobile-12 col-visu">
                <div class="has-bg-image">&nbsp</div>
                <div class="content">
                  <p class="subtitle boxed-text">
                    <span style="padding-bottom: 2px;">Nouvel espace mode femme</span>
                  </p>
                  <h2 class="boxed-text">
                    <span style="padding-bottom: 3px;">La&nbsp;Creative&nbsp;Galerie</span>
                  </h2>
                </div>
              </div>
              <div class="luxe-col-mobile-12 col-content">
                <div class="content luxe-row no-gutter">
                  <div class="luxe-col-mobile-12">
                    <div class="txt-container">
                      <p class="has-text-grey">Installé au 2ème étage du Magasin Coupole, ce multimarque met à l’honneur une trentaine de créateurs dont 15 s’installent pour la première fois aux Galeries Lafayette. Ici, sont représentées les tendances du moment et celles à venir à travers une sélection pointue.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="luxe-col-mobile-12">
                <div class="cta-container">
                  <a href="https://haussmann.galerieslafayette.com/renouveau-espace-femme" class="button outlined" target="_blank">En savoir plus</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="reinventons-actumag section hidden-section" id="actumag-marques-digitales">
      <div class="stepper">
        <ul class="luxe-row no-gutter">
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step"></span></li>
          <li class="luxe-col-mobile"><span class="step is-active"></span></li>
        </ul>
      </div>
      <div class="reinventons-nav active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav">
        <button class="prev"></button>
        <button class="next"></button>
      </div>
      <div class="container">
        <div class="luxe-row no-gutter luxe-reverse">
          <div class="is-hidden-touch luxe-col-desktop-6"></div>
          <div class="luxe-col-mobile-12 luxe-col-desktop-6 has-background-white">
            <div class="luxe-row no-gutter luxe-middle-mobile half-content-card">
              <div class="luxe-col-mobile-12 col-visu">
                <div class="has-bg-image">&nbsp</div>
                <div class="content">
                  <p class="subtitle boxed-text">
                    <span style="padding-bottom: 2px;">Nouvel espace mode femme</span>
                  </p>
                  <h2 class="boxed-text">
                    <span style="padding-bottom: 4px; border-bottom: none; z-index: 1;">L'arrivée des</span> <br />
                    <span style="padding-bottom: 5px;top: -4px;line-height: 36px;">marques&nbsp;digitales</span>
                  </h2>
                </div>
              </div>
              <div class="luxe-col-mobile-12 col-content">
                <div class="content luxe-row no-gutter">
                  <div class="luxe-col-mobile-12">
                    <div class="txt-container">
                      <p class="has-text-grey">Les marques digitales qui font la mode d’aujourd’hui sont aux Galeries Lafayette. Découvrez de plus près ces collections imaginées sur les réseaux sociaux par une nouvelle génération de créatrices et de créateurs tels que Songe Lab, Jane Mill, Pretty Wire ou Seven August.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="luxe-col-mobile-12">
                <div class="cta-container">
                  <a href="https://haussmann.galerieslafayette.com/renouveau-espace-femme" class="button outlined" target="_blank">En savoir plus</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    
    
    
    <section class="reinventons-actumag reinventons-replay section hidden-section has-background-actumag" id="restart">
      <div class="reinventons-nav inverted active">
        <button class="trigger-nav-menu"></button>
        <button class="close"></button>
      </div>
      <div class="prev-next-nav is-hidden-desktop">
        <button class="prev"></button>
        <button class="next is-hidden"></button>
      </div>
      <div class="container">
        <div class="luxe-row no-gutter luxe-middle-mobile text-center">
          <div class="luxe-col-mobile-12 col-more-news">
            <a href="https://www.galerieslafayette.com/t/reinventons-nous" class="cta-container has-text-white">
              <p class="is-bold">Découvrir toutes <br>les nouveautés</p>
              <button class="button primary inverted is-width-auto" target="_blank">Voir la sélection</button>
            </a>
          </div>
          <div class="luxe-col-mobile-12 is-hidden-desktop">
            <div class="luxe-row no-gutter col-restart">
              <div class="luxe-col-mobile-6">
                <button class="start back">
                  <span class="nav-arrow arrow-left inverted"></span>
                  <span>Revenir</span>
                </button>
              </div>
              <div class="luxe-col-mobile-6">
                <button class="start restart-button">
                  <span class="nav-arrow inverted restart"></span>
                  <span>Redémarrer</span>
                </button>
              </div>
            </div>
          </div>
          <div class="luxe-col-mobile-12 col-restart is-hidden-touch">
            <a href="#header" class="go-to-top is-hidden-touch has-text-white has-smoothscroll">
              <span class="nav-arrow inverted restart"></span>
              <span>Redémarrer  <br />l'expérience</span>
            </a>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2021/reinventons-nous.min.v07.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->
  
  
<!-- build:js /media/LP/src/js/2021/reinventons-nous.min.v07.js 
  <script src="../../assets/js/blazy.min.js"></script>
  <script src="../../assets/js/tabs.js"></script>
  <script src="../../assets/js/gumshoe-scrollspy.polyfills.min.js"></script>
  <script src="../../assets/js/ScrollMagic.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
  <script src="../src/js/2021/reinventons-nous.js"></script>
<!-- endbuild --> 
  
  
<?php include ('../pages-defaults/footer.php'); ?>
