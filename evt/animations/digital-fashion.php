<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Digital Fashion";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div> 
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

  <!-- <link href="../../media/LP/src/css/2022/digital-fashion.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2022/digital-fashion.min.v07.css" rel="stylesheet" type="text/css" />
<div class="hero-bg-image" style="background-image:url('https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/bg-microverse-hero-vig.jpg');">&nbsp;</div>
<div class="lp-container">
  <section class="section lp-hero">
    <div class="container is-fullheight">
      <div class="luxe-row no-gutter is-fullheight luxe-bottom-mobile luxe-top-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-5 luxe-start-tablet">  
          <div class="title">
            <h1><span class="is-smaller is-block">Egonlab x Galeries Lafayette</span>
              Bienvenue dans<br /> la digital fashion </h1>
            <a href="https://wonderland-digitalfashion.com" class="button primary has-arrow is-purple" id="goto-egonlab">Découvrir le microverse</a>
            <br />
            <a href="#wearables" class="button primary is-purple has-arrow-down has-smoothscroll" id="goto-egonlab">Essayer la capsule</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="intro-btn-container is-hidden-mobile">
    <a href="#intro" class="go-down has-smoothscroll is-hidden"><span class="nav-arrow arrow-down"></span></a>
  </div>
  <section class="section lp-body hero-txt">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-tablet luxe-start-mobile" id="intro">
        <div class="luxe-col-mobile-12 luxe-col-tablet-9">
          <div class="content">
            <p>La Digital Fashion fait son entrée aux Galeries Lafayette. On dit oui à une mode augmentée, pour tous, à vivre dans tous les mondes virtuels et sur chaque écran. Une mode inclusive, plus fun et interactive. Matières, textures, formes, tout devient possible dans le metaverse.
              <span class="is-hidden-mobile"><br /><br /> Les Galeries Lafayette s’associent avec Egonlab, une marque française précurseur fondée en 2019, fidèle à son ADN expérimental et inclusif. Le label parisien a imaginé une capsule virtuelle, inspirée de sa dernière collection printemps-été 2023 baptisée 'Wonderland'. Découvrez ces trois pièces phares augmentées, des wearables iconiques à gagner sous forme de NFT au travers d'un jeu concours et à porter fièrement sur vous-même ou votre avatar.</span>
              <span class="read more is-hidden-tablet">&hellip; <span class="underline-text">Lire plus</span></span> 
              <span class="read-more-content is-hidden"><span class="is-hidden-tablet"><br /><br /> Les Galeries Lafayette s’associent avec Egonlab, une marque française précurseur fondée en 2019, fidèle à son ADN expérimental et inclusif. Le label parisien a imaginé une capsule virtuelle, inspirée de sa dernière collection printemps-été 2023 baptisée 'Wonderland'. Découvrez ces trois pièces phares augmentées, des wearables iconiques à gagner sous forme de NFT au travers d'un jeu concours et à porter fièrement sur vous-même ou votre avatar.&nbsp;</span>
              <span class="read less is-hidden-tablet"><span class="underline-text">Fermer</span></span></span>
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body wearables" id="wearables">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-tablet">
        <div class="luxe-col-mobile-12 luxe-col-desktop-4 wearable">
          <div class="content">
            <div class="luxe-row no-gutter">
              <div class="luxe-col-mobile-12">
                <iframe class="video" src="https://player.vimeo.com/video/722550431?background=1&autopause=0" width="640" height="640" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen  mozallowfullscreen="" webkitallowfullscreen=""></iframe>
              </div>
              <div class="luxe-col-mobile-12">
                <span class="tag is-block">Wearable 1</span>
                <h3 class="is-uppercase">Wonder Bow</h3>
              </div>
            </div>
          </div>
        </div>
        
        <div class="luxe-col-mobile-12 luxe-col-desktop-4 wearable">
          <div class="content">
            <div class="luxe-row no-gutter">
              <div class="luxe-col-mobile-12">
                <iframe class="video" src="https://player.vimeo.com/video/722554501?background=1&autopause=0" width="640" height="640" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen  mozallowfullscreen="" webkitallowfullscreen=""></iframe>
              </div>
              <div class="luxe-col-mobile-12">
                <span class="tag is-block">Wearable 2</span>
                <h3 class="is-uppercase">Bubble Shirt</h3>
              </div>
            </div>
          </div>
        </div>
        
        <div class="luxe-col-mobile-12 luxe-col-desktop-4 wearable">
          <div class="content">
            <div class="luxe-row no-gutter">
              <div class="luxe-col-mobile-12">
                <iframe class="video" src="https://player.vimeo.com/video/722555496?background=1&autopause=0" width="640" height="640" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen  mozallowfullscreen="" webkitallowfullscreen=""></iframe>
              </div>
              <div class="luxe-col-mobile-12">
                <span class="tag is-block">Wearable 3</span>
                <h3 class="is-uppercase">Mushroom Boots</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body steps-header">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-tablet">
        <div class="luxe-col-mobile-12">
          <div class="content">
            <p class="title is-uppercase">le jeu concours est maintenant terminé</p>
            <p class="is-uppercase is-marginless is-bigger is-bold">L'annonce des gagnants arrive très bientôt</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section lp-body scrolling-text">
    <div class="container">
      <div class="scrolling-text-container text-left rtl-scroll">
        <p class="is-uppercase text-size-1">QU’EST-CE QUE LA DIGITAL FASHION  //  QU’EST-CE QUE LA DIGITAL FASHION  //  QU’EST-CE QUE LA DIGITAL FASHION  //  QU’EST-CE QUE LA DIGITAL FASHION  // QU’EST-CE QUE LA DIGITAL FASHION  //  QU’EST-CE QUE LA DIGITAL FASHION  //  QU’EST-CE QUE LA DIGITAL FASHION  //  </p>
      </div>
    </div>
  </section>
  
  <section class="section lp-body digital-fashion">
    <div class="container">
      <div class="luxe-row no-gutter luxe-middle-tablet luxe-reverse">
        <div class="luxe-col-mobile-12 luxe-col-tablet-5 luxe-col-tablet-offset-2 luxe-center-tablet">
          <div class="content">
            <a href="https://www.snapchat.com/add/gl_france">
              <div class="video-container">                
                <video preload="auto" autoplay loop playsinline muted>
                  <source src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/video-bloc-digital-fashion.mp4" type="video/mp4">
                </video>
              </div>              
            </a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-5 luxe-start-mobile">
          <p class="subtitle is-bold is-hidden">Digital Fashion</p>
          <h2 class="is-uppercase is-bigger">La digital fashion vous permet de  vous habiller de façon extraordinaire.</h2>
        </div>
      </div>
    </div>
  </section>  
  
  <section class="section lp-body benefices">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-mobile">
        <div class="luxe-col-mobile-12">
          <h2 class="is-uppercase">Les Bénéfices</h2>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3">
          <div class="content">
            <figure class="image">
              <img class="b-lazy"
              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/benefice_1.png|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/benefice_1@2x.png"
              alt="Digital Fashion - Galeries Lafayette">
              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/benefice_1.png" alt="Digital Fashion - Galeries Lafayette" /></noscript>
            </figure>
            <p class="is-uppercase">Taille unique</p>
          </div>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3">
          <div class="content">
            <figure class="image">
              <img class="b-lazy"
              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/benefice_2.png|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/benefice_2@2x.png"
              alt="Digital Fashion - Galeries Lafayette">
              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/benefice_2.png" alt="Digital Fashion - Galeries Lafayette" /></noscript>
            </figure>
            <p class="is-uppercase">Mixte</p>
          </div>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3">
          <div class="content">
            <figure class="image">
              <img class="b-lazy"
              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/benefice_3.png|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/benefice_3@2x.png"
              alt="Digital Fashion - Galeries Lafayette">
              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/benefice_3.png" alt="Digital Fashion - Galeries Lafayette" /></noscript>
            </figure>
            <p class="is-uppercase">Encore plus stylé</p>
          </div>
        </div>
        <div class="luxe-col-mobile-6 luxe-col-tablet-3">
          <div class="content">
            <figure class="image">
              <img class="b-lazy"
              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/benefice_4.png|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/benefice_4@2x.png"
              alt="Digital Fashion - Galeries Lafayette">
              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/benefice_4.png" alt="Digital Fashion - Galeries Lafayette" /></noscript>
            </figure>
            <p class="is-uppercase">Encore plus fun</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body scrolling-text">
    <div class="container">
      <div class="scrolling-text-container text-left ltr-scroll">
        <p class="is-uppercase text-size-1">QU’EST-CE QUE LE METAVERSE // QU’EST-CE QUE LE METAVERSE //  QU’EST-CE QUE LE METAVERSE  //  QU’EST-CE QUE LE METAVERSE  //  QU’EST-CE QUE LE METAVERSE  //  QU’EST-CE QUE LE METAVERSE  //   QU’EST-CE QUE LE METAVERSE ? //</p>
      </div>
    </div>
  </section>
  
  <section class="section lp-body metaverse">
    <div class="container">
      <div class="luxe-row no-gutter luxe-middle-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-5 luxe-center-tablet">
          <div class="content">
            <figure class="image">
              <img class="b-lazy"
              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/img-metaverse.png|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/img-metaverse@2x.png"
              alt="Digital Fashion - Galeries Lafayette">
              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/img-metaverse.png" alt="Digital Fashion - Galeries Lafayette" /></noscript>
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-5 luxe-col-tablet-offset-2 luxe-start-mobile">
          <p class="subtitle is-bold is-hidden">Metavers</p>
          <h2 class="is-uppercase is-bigger">le metaverse c’est votre nouveau terrain de jeu virtuel avec des possibilités infinies</h2>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body scrolling-text">
    <div class="container">
      <div class="scrolling-text-container text-left rtl-scroll">
        <p class="is-uppercase text-size-1">QU’EST-CE QUE LE METAVERSE // QU’EST-CE QUE LE METAVERSE //  QU’EST-CE QUE LE METAVERSE  //  QU’EST-CE QUE LE METAVERSE  //  QU’EST-CE QUE LE METAVERSE  //  QU’EST-CE QUE LE METAVERSE  //   QU’EST-CE QUE LE METAVERSE ? //</p>
      </div>
    </div>
  </section>
  
  <section class="section lp-body">
    <div class="container">
      <div class="luxe-row no-gutter luxe-middle-tablet luxe-reverse">
        <div class="luxe-col-mobile-12 luxe-col-tablet-5 luxe-col-tablet-offset-2 luxe-center-tablet">
          <div class="content">
            <figure class="image">
              <img class="b-lazy"
              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/img-nfts.png|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/img-nfts@2x.png"
              alt="Digital Fashion - Galeries Lafayette">
              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/img-nfts.png" alt="Digital Fashion - Galeries Lafayette" /></noscript>
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-5 luxe-start-mobile">
          <p class="subtitle is-bold is-hidden">NFT</p>
          <h2 class="is-uppercase is-bigger">les nft sont des œuvres d’art digitales uniques et certifiées qui revolutionnent le monde de l’art </h2>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body is-hidden">
    <div class="container">
      <div class="luxe-row no-gutter luxe-middle-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-center-tablet">
          <div class="content">
            <figure class="image">
              <img class="b-lazy"
              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/digital-clothing-the-fabricant-original.png|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/digital-clothing-the-fabricant-original@2x.png"
              alt="Digital Fashion - Galeries Lafayette">
              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/digital-fashion/digital-clothing-the-fabricant-original-model.png" alt="Digital Fashion - Galeries Lafayette" /></noscript>
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-start-mobile">
          <p class="subtitle is-bold">Mushroom game</p>
          <h2 class="is-uppercase is-bigger">Encore plus du fun<br />
            Jouez le mushroom et partagez votre score pour augmenter vos chances de gagner</h2>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body section-faq" id="faq">
    <div class="container">
      <div class="luxe-row">
        <div class="luxe-col-mobile-12">
          <article class="faq">
            <h2>
              <span class="is-hidden-mobile">Foire aux questions</span>
              <span class="is-hidden-tablet">FAQ</span>
            </h2>
            <div class="accordions text-left">
              <div class="accordion">
                <div class="accordion-header">
                  <p>Qu’est-ce que c’est la blockchain&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>La Blockchain est une base de données partagée par des millions d'ordinateurs reliés. Elle permet aux utilisateurs de transférer différents actifs numériques en sécurité sans l'intervention d'une société tiers, les données restent la propriété de l’utilisateur.  Tout se passe de manière décentralisée.<br />
                      Le fonctionnement même  de la blockchain permet d'attester la validité de toutes les transactions en son sein et donc la validité d’un NFT par exemple.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Qu’est-ce que la cryptomonnaie&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Monnaie utilisée sur internet, indépendante des réseaux bancaires, elle sert de moyen de transaction pour l’achat et vente de NFTs et parfois même d’articles dans la vraie vie. Véritable économie parallèle, elle tire son indépendance de la blockchain.<br />
                      On en dénombre des dizaines, les plus connues sont Ethereum et Bitcoin. </p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Qu’est-ce que c’est un crypto wallet&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Le wallet, ou portefeuille, c’est votre allié pour tout ce qui concerne les collectibles. Un l'endroit protégé où vos crypto-monnaies et vos NFTs seront stockés et utilisables dans les metaverses (comme Decentraland ou The Sandbox.) Pour acheter de la crypto-monnaie et ensuite acquérir des NFTs, la création d’un wallet est la première étape fondamentale.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Qu’est-ce que c’est ‘minter’&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>C’est l’action requise pour vraiment posséder un NFT, quand on le mint, on l'inscrit dans la Blockchain afin de permettre son acquisition et revendiquer sa propriété.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Mode d’emploi d’un possesseur de NFT&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>On suit le guide :<br />
                      
                      Vous créez votre wallet - c’est la première étape.<br />
                      On achète la crypto-monnaie de son choix via Moonpay.<br />
                      On choisit son NFT parmis les collections disponibles.<br />
                      On mint son NFT sur la blockchain.<br />
                      Le NFT est à vous. Vous pouvez l’utiliser dans les différents metaverses.</p>
                  </div>
                </div>
              </div>            
            </div>
          </article>
          <div class="cgv">
            <p class="is-tiny is-marginless">Jeu concours valable du mercredi 22 juin au jeudi 18 juillet sur le site des galerieslafayette.com, depuis le compte Instagram des Galeries Lafayette (@galerieslafayette), le compte Snapchat des Galeries Lafayette (@gl_france) et le compte Tiktok des Galeries Lafayette (@galerieslafayette). Tirage au sort le 18 juillet et annonce des gagnants sur Instagram. Différents prix à gagner à retrouver sur galerieslafayette.com. La participation à ce jeu entraîne l'acceptation pure et simple du règlement du jeu concours disponible sur galerieslafayette.com. Vous bénéficiez d’un droit d’accès, de rectification et d’effacement de vos données que vous pouvez exercer à tout moment soit en adressant un email à relaiscil@galerieslafayette.com.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  

</div>

<div class="fadeout-splash"></div>
<script src="https://static.galerieslafayette.com/media/LP/src/js/2022/digital-fashion.min.v03.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->
  
   
  <!-- build:js /media/LP/src/js/2022/digital-fashion.min.v01.js
    <script src="../../assets/js/blazy.min.js"></script>
    <script src="../../assets/js/accordion.js"></script>
    <script src="../../assets/js/ScrollMagic.min.js"></script>
    <script src="../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
    <script src="../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
    <script src="../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
    <script src="../src/js/2022/digital-fashion.js"></script>
   endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
