<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "AMI";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../media/LP/src/css/2021/ami-de-coeur.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/ami-de-coeur.min.v05.css" rel="stylesheet" type="text/css" />
<style>
  .back-top {
    display: none !important;
  }
  .gis-cta-logo {
    height: 65px !important;
    width: 65px !important; 
  }
  .gis-cta-indicator {
    height: 15px !important;
    width: 15px !important;
  }
  @media screen and (max-width: 1024px) {
    .gis-cta-logo {
      height: 60px !important;
      width: 60px !important; 
    }
  }
</style>
<div class="ami-de-coeur">
  
  <section class="ami-de-coeur-hero">
    <div class="container">
      <div class="luxe-row no-gutter luxe-reverse luxe-middle-mobile luxe-center">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="video-container">
            <div class="mobile-vid">
              <iframe class="bg-video" src="https://player.vimeo.com/video/566979677?background=1" width="767" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <div class="ami-logo is-hidden-mobile"></div>
            <h1>AMI de c&oelig;ur</h1>
            <p>Découvrez la nouvelle collection capsule</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="ami-de-coeur-body edito">
    <div class="container">
      <div class="luxe-row no-gutter luxe-middle-mobile luxe-center-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-10">
          <div class="content">
            <div class="ami-logo is-hidden-tablet"></div>
            <p>Fondée à Paris et inspirée par la ville, AMI propose depuis 2011, un vestiaire élégant et complet pour l'Homme et la Femme. Disponible dès maintenant aux Galeries Lafayette, la ligne AMI de Cœur célèbre les valeurs de joie, d’amitié, d’inclusivité et d’authenticité en revisitant les basiques de la marque aux couleurs de la saison.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  
  <section class="ami-de-coeur-body animated-article">
    <div class="container">

      <div class="luxe-row no-gutter luxe-middle-mobile">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-image alt-image">
            <figure class="image">
              <img class="b-lazy" 
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/ami-de-coeur/ami-c-quoi.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/ami-de-coeur/ami-c-quoi@2x.jpg"
                alt="AMI, c'est quoi ? - Galeries Lafayette">
                <!-- Fallback for non JavaScript browsers -->
                <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/ami-de-coeur/ami-c-quoi.jpg" alt="AMI, c'est quoi ? - Galeries Lafayette" width="540" height="676" /></noscript>
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <h2>AMI, c’est quoi ?</h2>
            <p>La Maison AMI capture l’essence particulière d’une nonchalance parisienne qui brouille les frontières entre chic et décontraction. Inspirée par la ville depuis ses débuts, AMI se distingue par des collections conviviales et insouciantes. Comme Paris.</p>
          </div>
        </div>
      </div>
      
    </div>
  </section>
  
  
  <section class="section-video mid">
    <div class="video-container">
      <div class="read-more">
        <button class="read-more-toggle"><span class="plus"></span></button>
        <div class="read-more-content hidden">
          <button class="read-less-toggle"><span class="cross"></span></button>
          <em>Sweatshirt Broderie Ami De Coeur Rouge en rouge, Polo Broderie Ami De Coeur Rouge en vert, Short Broderie Ami De Coeur Ton Sur Ton en marine<br />
            <br />
            Pull Oversize Ami De Coeur en vert, Polo Broderie Ami De Coeur Rouge en rouge, Mariniere Broderie Ami De Coeur Rouge en marine<br />
            <br />
            Sweatshirt Broderie Ami De Coeur Ton Sur Ton en marine, Hoodie Broderie Ami De Coeur Ton Sur Ton en rouge, Short Broderie Ami De Coeur Ton Sur Ton en vert, Chaussettes Ami De Coeur en ecru<br />
            <br />
            Sweatshirt Broderie Ami De Coeur Rouge en marine, Polo Broderie Ami De Coeur Rouge en vert, Sweatshirt Broderie Ami De Coeur Rouge en vert, Maillot De Bain Broderie Ami De Coeur Rouge en rouge</em>
        </div>
      </div>
      <div class="desktop-vid is-hidden-mobile">
        <iframe class="bg-video" src="https://player.vimeo.com/video/567414459?background=1" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
      </div>
      <div class="mobile-vid is-hidden-tablet">
        <iframe class="bg-video" src="https://player.vimeo.com/video/567463110?background=1" width="767" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
      </div>
    </div>
    
  </section>

  
  <section class="ami-de-coeur-body">
    <div class="container">

      <div class="luxe-row no-gutter luxe-middle-mobile">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-image">
            <div class="read-more">
              <button class="read-more-toggle"><span class="plus"></span></button>
              <div class="read-more-content hidden">
                <button class="read-less-toggle"><span class="cross"></span></button>
                <em>Cardigan Oversize Ami De Coeur en blanc<br /> Pull Oversize Ami De Coeur en marine</em>
              </div>
            </div>
            <figure class="image">
              <img class="b-lazy" 
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/ami-de-coeur/lesprit-ami-de-coeur.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/ami-de-coeur/lesprit-ami-de-coeur@2x.jpg"
                alt="L'esprit AMI de Coeur - Galeries Lafayette">
                <!-- Fallback for non JavaScript browsers -->
                <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/ami-de-coeur/lesprit-ami-de-coeur.jpg" alt="L'esprit AMI de Coeur - Galeries Lafayette" width="672" height="840" /></noscript>
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <h2>L'esprit AMI<br class="is-hidden-mobile" /> de Coeur</h2>
            <p>AMI, c’est avant tout une communauté, basée sur des valeurs clés d’amitiés et d’inclusivité. Bienveillance et authenticité sont au cœur de la Maison qui d’abord pensée pour les Hommes, a glissé vers le féminin comme une évidence.</p>
          </div>
        </div>
      </div>
      
    </div>
  </section>
  
  
  <section class="ami-de-coeur-body">
    <div class="container">

      <div class="luxe-row no-gutter luxe-reverse luxe-middle-mobile">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-image">
            <figure class="image">
              <img class="b-lazy" 
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/ami-de-coeur/ami-coupole.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/ami-de-coeur/ami-coupole@2x.jpg"
                alt="AMI, le cœur battant de la Coupole - Galeries Lafayette">
                <!-- Fallback for non JavaScript browsers -->
                <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/ami-de-coeur/ami-coupole.jpg" alt="AMI, le cœur battant de la Coupole - Galeries Lafayette" width="648" height="800" /></noscript>
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <div class="tag">À Paris Haussmann</div>
            <h2>AMI, le cœur battant de la&nbsp;Coupole</h2>
            <p>Pour déclarer son amour à Paris, AMI imagine aux Galeries Lafayette Paris Haussmann une scénographie hors du commun. Un AMI de Cœur rouge, géant, flottera sous la légendaire Coupole du grand magasin. Un clin d’œil au concept créatif au cœur de la campagne publicitaire de Jean-Paul Goude.</p>
            <a href="https://haussmann.galerieslafayette.com/ami-paris-mon-amour" class="button outlined">En savoir plus<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
      
    </div>
  </section>
  
  
  <section class="section-video">
    <div class="video-container">
      <div class="desktop-vid">
        <iframe class="bg-video" src="https://player.vimeo.com/video/567472288?background=1" width="1440" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
      </div>
    </div>
  </section>
  
  
  <section class="ami-de-coeur-body">
    <div class="container">

      <div class="luxe-row no-gutter luxe-middle-mobile">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-image">
            <div class="read-more">
              <button class="read-more-toggle"><span class="plus"></span></button>
              <div class="read-more-content hidden">
                <button class="read-less-toggle"><span class="cross"></span></button>
                <em>Sweatshirt Broderie Ami De Coeur Ton Sur Ton en beige<br />
                  Hoodie Broderie Ami De Coeur Ton Sur Ton en beige<br />
                  Polo Broderie Ami De Coeur Ton Sur Ton en beige</em>
              </div>
            </div>
            <figure class="image">
              <img class="b-lazy" 
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/ami-de-coeur/sad-ami-gl.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/ami-de-coeur/sad-ami-gl@2x.jpg"
                alt="Le Shopping À Distance AMI x Galeries Lafayette - Galeries Lafayette">
                <!-- Fallback for non JavaScript browsers -->
                <noscript><img class="noscript-img" src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/ami-de-coeur/sad-ami-gl.jpg" alt="Le Shopping À Distance AMI x Galeries Lafayette - Galeries Lafayette" width="540" height="676" /></noscript>
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <h2>Le Shopping à Distance AMI&nbsp;x Galeries Lafayette</h2>
            <p>À l’occasion du pop-up AMI Paris, les Galeries Lafayette ont le plaisir de vous proposer le service de Shopping à Distance. Vous pouvez ainsi réaliser votre shopping où que vous soyez et bénéficier d’un accompagnement personnalisé en live shopping vidéo ou en renseignant votre wishlist avec les pièces dont vous avez envie. <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text" target="_blank">En savoir +</a></p>
            <p class="is-smaller">Service disponible du lundi au samedi de 11h à 19h et dimanche et jours fériés de&nbsp;12h à 19h.</p>
            <a href="" class="button outlined" onclick="initSAD(); return false;">Shopping à Distance<span class="icon has-arrow"></span></a>
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance" class="is-text is-tiny is-block is-hidden">En savoir plus sur le service de Shopping À Distance</a>
          </div>
        </div>
      </div>
      
    </div>
  </section>
  
  
  <section class="section-video bottom">
    <div class="video-container">
      <div class="read-more">
        <button class="read-more-toggle"><span class="plus"></span></button>
        <div class="read-more-content hidden">
          <button class="read-less-toggle"><span class="cross"></span></button>
          <em>Hoodie Broderie Ami De Coeur Ton Sur Ton en beige<br />
            Sweatshirt Broderie Ami De Coeur Ton Sur Ton en beige<br />
            Polo Broderie Ami De Coeur Ton Sur Ton en beige</em>
        </div>
      </div>
      <div class="desktop-vid is-hidden-mobile">
        <iframe class="bg-video" src="https://player.vimeo.com/video/567408190?background=1" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
      </div>
      <div class="mobile-vid is-hidden-tablet">
        <iframe class="bg-video" src="https://player.vimeo.com/video/567400173?background=1" width="767" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
      </div>
    </div>
  </section>
    
    
  <section class="ami-de-coeur-body last-section">
    <div class="container">

      <div class="luxe-row no-gutter luxe-middle-mobile luxe-center-mobile">
        <div class="luxe-col-mobile-12 luxe-col-tablet-10 luxe-col-desktop-9">
          <div class="ami-logo"></div>
        </div>
      </div>
      
    </div>
  </section>
  
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2021/ami-de-coeur.min.v02.js"></script>

<div id="goinstore_modals" style="height:0;"></div>
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/shoppingadistance-goinstore-v2.min.css" rel="stylesheet" type="text/css">
<script src="https://static.galerieslafayette.com/media/LP/src/js/2021/shoppingadistance-goinstore-v2.min.v03.js"></script>

<script>
  var routingKey = "brand40";
  var clickedBrand='AMI'; 
  var cleanBrandName='ami';
  function initSAD() {
    clickedBrand; 
    cleanBrandName; 
    appointedWidgetId='5fa3b8fd48dc683d829fac1e'; 
    GL_GIS.LiveShopping.displaySadBrand();  
    GL_GIS.LiveShopping.checkPopinClerkAvailability(routingKey);
  }

  window.onload = function() {
    GL_GIS.LiveShopping.init(routingKey);    
    
    var gisCTA = document.getElementById('gis-cta');
    if (gisCTA) {
      gisCTA.addEventListener('click', function(e) {
        e.stopPropagation();
        initSAD();
        GL_GIS.LiveShopping.displaySadBrand();
        return false;  
      }, true);
    }
    
    var gisMsgContainer = document.getElementById('gis-awareness-msg-container');
    if (gisMsgContainer) {
      gisMsgContainer.lastChild.addEventListener('click', function(e) { 
        e.stopPropagation();
        initSAD();
        GL_GIS.LiveShopping.displaySadBrand();
        return false;
      }, true);
    }
  }; 
</script>

<!--=========================== FIN LANDING PAGE ========================-->
   
<!-- build:js /media/LP/src/js/2021/ami-de-coeur.min.v02.js
  <script src="../../assets/js/blazy.min.js"></script>
  <script src="../../assets/js/ScrollMagic.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
  <script src="../src/js/2021/ami-de-coeur.js"></script>
<!-- endbuild -->
  
  
  
<!-- build:js /media/LP/src/js/2021/ami-de-coeur.min.v01.js
  <script src="../../assets/js/blazy.min.js"></script>
  <script src="../../assets/js/ScrollMagic.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
  <script src="../src/js/2021/ami-de-coeur-v1.js"></script>
<!-- endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
