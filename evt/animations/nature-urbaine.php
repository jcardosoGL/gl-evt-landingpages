<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Nature Urbaine";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

 <!-- <link href="../../media/LP/src/css/2022/nature-urbaine.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
 <link href="https://static.galerieslafayette.com/media/LP/src/css/2022/nature-urbaine.min.v04.css" rel="stylesheet" type="text/css" />
<div class="lp-container" style="background-color: #3F7A60;">
  <section class="section lp-hero-video">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-tablet">
        <div class="luxe-col-mobile-12 col-visu">
          <div class="title"><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/nature-urbaine.svg" alt="Nature urbaine" /></div>
          <div class="video-container">
            <iframe class="bg-video video" src="https://player.vimeo.com/video/693905370?background=1&autopause=0" width="1438" height="809" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen  mozallowfullscreen="" webkitallowfullscreen=""></iframe>           
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <div class="scroller">
    <section class="section lp-hero">
      <div class="container">
        <div class="luxe-row no-gutter luxe-center-tablet">
          <div class="luxe-col-mobile-12 luxe-col-tablet-7 luxe-col-desktop-5 luxe-col-widescreen-6">
            <div class="content">
              <p class="is-uppercase has-text-white">POUR LES BEAUX JOURS, LES GALERIES LAFAYETTE RÉINVENTENT LA VILLE. NATURE URBAINE&nbsp;EST L'OCCASION DE DÉCOUVRIR UNE MODE QUI FAIT VIVRE LA NATURE DANS UN ESPRIT CONTEMPORAIN FAISANT LA PART BELLE AUX PRODUITS PLUS RESPONSABLES LABELLISÉS GO&nbsp;FOR&nbsp;GOOD.
                </p>
            </div>
          </div>
          <div class="luxe-col-mobile-12">
            <div class="buttons">
              <a href="#les-selections" class="has-smoothscroll button borderless has-arrow-down">Les Sélections</a>
              <a href="#nos-engagements" class="has-smoothscroll button borderless has-arrow-down">Nos engagements</a>
              <a href="#la-seconde-main" class="has-smoothscroll button borderless has-arrow-down">La seconde main</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  
    <section class="section lp-body selections" id="les-selections">
      <div class="container">
        <div class="luxe-row">
          <div class="luxe-col-mobile-12">
            <div class="content">
              <h2 class="boxed-text">
                <span>Les sélections</span>
              </h2>
            </div>
          </div>
          
          <div class="luxe-col-mobile-12 selection-1">
            <div class="luxe-row">
              <div class="luxe-col-mobile-12 selection is-animated aigle">
                <div class="luxe-row no-gutter">
                  <div class="luxe-col-mobile-12 luxe-col-tablet-6">
                    <a href="https://www.galerieslafayette.com/b/aigle" class="image-container">
                      <figure class="image main text-center">
                        <img class="b-lazy"
                        src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                        data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/selection-main-aigle.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/selection-main-aigle@2x.jpg"
                        alt="Aigle - Galeries Lafayette">
                        <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/selection-main-aigle.jpg" alt="Aigle - Galeries Lafayette" /></noscript>
                      </figure>
                    </a>
                  </div>
                  <div class="luxe-col-mobile-12  luxe-col-tablet-6">
                    <div class="content">
                      <h3 class="is-uppercase">Aigle</h3>
                      <div class="luxe-row slider-pdt slider-aigle slider-flickity">
                        <div class="luxe-col-mobile-4 pdt">
                          <a class="image-container" href="https://www.galerieslafayette.com/p/gilet+multipoches-aigle/83366075/263">
                            <figure class="image text-center is-hidden-desktop">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt1_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt1_M@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt1_M.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                            <figure class="image text-center is-hidden-touch">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt1.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt1@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt1.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                          </a>
                        </div>
                        <div class="luxe-col-mobile-4 pdt">
                          <a class="image-container" href="https://www.galerieslafayette.com/p/parka+courte-aigle/83366093/60">
                            <figure class="image text-center is-hidden-desktop">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt2_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt2_M@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt2_M.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                            <figure class="image text-center is-hidden-touch">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt2@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt2.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                          </a>
                        </div>
                        <div class="luxe-col-mobile-4 pdt">
                          <a class="image-container" href="https://www.galerieslafayette.com/p/robe+chemise+longue+et+evasee+coton-aigle/84132814/2876">
                            <figure class="image text-center is-hidden-desktop">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt3_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt3_M@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt3_M.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                            <figure class="image text-center is-hidden-touch">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt3.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt3@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/aigle_pdt3.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                          </a>
                        </div>
                      </div>
                      <div class="content-bottom">
                        <p>Icône du Made in France, Aigle conçoit des pièces pour la ville et l’outdoor. Depuis cette saison, la marque confie sa direction artistique au trio créatif d’Etudes Studio.</p>
                        <a href="https://www.galerieslafayette.com/b/aigle" class="is-bold button outlined" target="_blank">Voir la sélection</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="luxe-col-mobile-12 selection is-animated courreges">
                <div class="luxe-row no-gutter luxe-reverse">
                  <div class="luxe-col-mobile-12 luxe-col-tablet-6">
                    <a href="https://www.galerieslafayette.com/b/courreges" class="image-container">
                      <figure class="image main text-center">
                        <img class="b-lazy"
                        src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                        data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/selection-main-courreges-v2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/selection-main-courreges-v2@2x.jpg"
                        alt="Courrèges - Galeries Lafayette">
                        <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/selection-main-courreges-v2.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                      </figure>
                    </a>
                  </div>
                  <div class="luxe-col-mobile-12 luxe-col-tablet-6">
                    <div class="content">
                      <h3 class="is-uppercase">Courrèges</h3>
                      <div class="luxe-row slider-pdt slider-courreges slider-flickity">
                        <div class="luxe-col-mobile-4 pdt">
                          <a class="image-container" href="https://www.galerieslafayette.com/p/blouson+vinyle+iconique-courreges/82809607/256">
                            <figure class="image text-center is-hidden-desktop">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courrege_pdt1_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courrege_pdt1_M@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courrege_pdt1_M.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                            <figure class="image text-center is-hidden-touch">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courreges_pdt1.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courreges_pdt1@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courreges_pdt1.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                          </a>
                        </div>
                        <div class="luxe-col-mobile-4 pdt">
                          <a class="image-container" href="https://www.galerieslafayette.com/p/sac+camera+jersey-courreges/82345334/320">
                            <figure class="image text-center is-hidden-desktop">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courrege_pdt2_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courrege_pdt2_M@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courrege_pdt2_M.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                            <figure class="image text-center is-hidden-touch">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courreges_pdt2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courreges_pdt2@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courreges_pdt2.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                          </a>
                        </div>
                        <div class="luxe-col-mobile-4 pdt">
                          <a class="image-container" href="https://www.galerieslafayette.com/p/pull+en+maille+iconique+manches+courtes-courreges/82810025/1890">
                            <figure class="image text-center is-hidden-desktop">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courrege_pdt3_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courrege_pdt3_M@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courrege_pdt3_M.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                            <figure class="image text-center is-hidden-touch">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courreges_pdt3.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courreges_pdt3@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/courreges_pdt3.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                          </a>
                        </div>
                      </div>
                      <div class="content-bottom">
                        <p>Cette saison, Courrèges sublime une liberté enfin retrouvée à travers des pièces où les jeux de matières et de coupes se réinventent.</p>
                        <a href="https://www.galerieslafayette.com/b/courreges" class="is-bold button outlined" target="_blank">Voir la sélection</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="luxe-col-mobile-12 selection is-animated marc-o-polo">
                <div class="luxe-row no-gutter">
                  <div class="luxe-col-mobile-12 luxe-col-tablet-6">
                    <a href="https://www.galerieslafayette.com/b/marc-o-polo" class="image-container">
                      <figure class="image main text-center">
                        <img class="b-lazy"
                        src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                        data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/selection-main-marc-o-polo.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/selection-main-marc-o-polo@2x.jpg"
                        alt="Marc O’Polo - Galeries Lafayette">
                        <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/selection-main-marc-o-polo.jpg" alt="Marc O’Polo - Galeries Lafayette" /></noscript>
                      </figure>
                    </a>
                  </div>
                  <div class="luxe-col-mobile-12  luxe-col-tablet-6">
                    <div class="content">
                      <h3 class="is-uppercase">Marc <br class="is-hidden-touch" />o'polo</h3>
                      <div class="luxe-row slider-pdt slider-marc-o-polo slider-flickity">
                        <div class="luxe-col-mobile-4 pdt">
                          <a class="image-container" href="https://www.galerieslafayette.com/p/robe+en+lin+large+jupe+a+volants-marc+o+polo/300413479420/256">
                            <figure class="image text-center is-hidden-desktop">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt1_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt1_M@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt1_M.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                            <figure class="image text-center is-hidden-touch">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt1.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt1@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt1.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                          </a>
                        </div>
                        <div class="luxe-col-mobile-4 pdt">
                          <a class="image-container" href="https://www.galerieslafayette.com/p/veste+cargo+en+melange+robuste+de+lin+et+de+coton+biologique-marc+o+polo/300412622896/60">
                            <figure class="image text-center is-hidden-desktop">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt2_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt2_M@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt2_M.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                            <figure class="image text-center is-hidden-touch">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt2.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt2@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt2.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                          </a>
                        </div>
                        <div class="luxe-col-mobile-4 pdt">
                          <a class="image-container" href="https://www.galerieslafayette.com/p/robe-pull+a+manches+courtes+en+melange+de+coton+biologique+et+de+lin-marc+o+polo/300412860453/453?version=v2">
                            <figure class="image text-center is-hidden-desktop">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt3_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt3_M@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt3_M.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                            <figure class="image text-center is-hidden-touch">
                              <img class="b-lazy"
                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt3.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt3@2x.jpg"
                              alt="Courrèges - Galeries Lafayette">
                              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/produits/marcopolo_pdt3.jpg" alt="Courrèges - Galeries Lafayette" /></noscript>
                            </figure>
                          </a>
                        </div>
                      </div>
                      <div class="content-bottom">
                        <p>Marc O’Polo présente sa collection durable en lin, certifiée MASTERS OF LINEN® à l’occasion de Nature Urbaine et surprend par son design d’inspiration scandinave.</p>
                        <a href="https://www.galerieslafayette.com/b/marc-o-polo" class="is-bold button outlined" target="_blank">Voir la sélection</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="luxe-col-mobile-12 selection-2">
            <div class="luxe-row">
              <div class="luxe-col-mobile-12 luxe-col-tablet-4 selection">
                <div class="luxe-row luxe-middle-mobile">
                  <div class="luxe-col-mobile-6 luxe-col-tablet-12">
                    <a href="https://www.galerieslafayette.com/c/beauty+galerie" class="image-container">
                      <figure class="image">
                        <img class="b-lazy"
                        src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                        data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/img_beaute_b.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/img_beaute_b@2x.jpg"
                        alt="Beauté responsable - Galeries Lafayette">
                        <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/img_beaute_b.jpg" alt="Beauté responsable - Galeries Lafayette" /></noscript>
                      </figure>
                    </a>
                  </div>
                  <div class="luxe-col-mobile-6 luxe-col-tablet-12">
                    <div class="content">
                      <h3 class="is-uppercase has-text-white">Beauty <br />galerie</h3>
                      <p class="has-text-white">Une routine beauté engagée à l'épreuve de la ville qui fait rentrer la nature dans toutes les salles de bain</p>
                      <a href="https://www.galerieslafayette.com/c/beauty+galerie" class="is-bold button primary outlined" target="_blank">Voir la sélection</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="luxe-col-mobile-12 luxe-col-tablet-4 selection mode">
                <div class="luxe-row luxe-reverse luxe-middle-mobile">
                  <div class="luxe-col-mobile-6 luxe-col-tablet-12">
                    <span class="papillon papillon-1 is-hidden-mobile"></span>
                    <a href="https://www.galerieslafayette.com/t/go-for-good-mode" class="image-container">
                      <figure class="image is-go-for-good">
                        <span class="papillon papillon-2"></span>
                        <img class="b-lazy is-hidden-tablet" style="object-position: top;"
                        src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                        data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/img_mode_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/img_mode_M@2x.jpg"
                        alt="Mode durable - Galeries Lafayette">
                        <img class="b-lazy is-hidden-mobile"
                        src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                        data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/img_mode.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/img_mode@2x.jpg"
                        alt="Mode durable - Galeries Lafayette">
                        <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/img_mode.jpg" alt="Mode durable - Galeries Lafayette" /></noscript>
                      </figure>
                    </a>
                  </div>
                  <div class="luxe-col-mobile-6 luxe-col-tablet-12">
                    <div class="content">
                      <h3 class="is-uppercase has-text-white">Mode plus<br />durable</h3>
                      <p class="has-text-white">Un vestiaire contemporain et urbain toujours labellisé GO FOR GOOD pour un style toujours plus responsable</p>
                      <a href="https://www.galerieslafayette.com/t/go-for-good-mode" class="is-bold button primary outlined" target="_blank">Voir la sélection</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="luxe-col-mobile-12 luxe-col-tablet-4 selection">
                <div class="luxe-row luxe-middle-mobile">
                  <div class="luxe-col-mobile-6 luxe-col-tablet-12">
                    <a href="https://www.galerieslafayette.com/t/tendance-outdoor" class="image-container">
                      <figure class="image">
                        <img class="b-lazy"
                        src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                        data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/img_outdoor_b.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/img_outdoor_b@2x.jpg"
                        alt="Tendance outdoor - Galeries Lafayette">
                        <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/img_outdoor_b.jpg" alt="Tendance outdoor - Galeries Lafayette" /></noscript>
                      </figure>
                    </a>
                  </div>
                  <div class="luxe-col-mobile-6 luxe-col-tablet-12">
                    <div class="content">
                      <h3 class="is-uppercase has-text-white">Tendance <br />outdoor</h3>
                      <p class="has-text-white">Des pièces inspirées par le grand air qui mixent style urbain, matières techniques et couleurs naturelles</p>
                      <a href="https://www.galerieslafayette.com/t/tendance-outdoor" class="is-bold button primary outlined" target="_blank">Voir la sélection</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  
    <section class="section lp-body vacades" style="background-color: #ffffff;">
      <div class="halo-video"></div>
      <div class="container">
        <div class="luxe-row text-center luxe-middle-tablet luxe-center-tablet">
          <div class="luxe-col-mobile-12 luxe-col-tablet-11 luxe-col-fullhd-10">
              <div class="video-container">
                <iframe class="bg-video video" src="https://player.vimeo.com/video/694451903?loop=1" width="767" height="431" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen  mozallowfullscreen="" webkitallowfullscreen=""></iframe>           
              </div>
          </div>
          <div class="luxe-col-mobile-12 luxe-col-tablet-8 luxe-col-widescreen-7 luxe-col-fullhd-6">
            <div class="content">
              <p class="is-uppercase">L’heure est à l’exploration. Pour Nature Urbaine, les Galeries Lafayette invitent l’artiste allemand Timo Helgert et son agence de création @vacades à imaginer une expérience digitale et créative.
                </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  
    <section class="section lp-body engagements" id="nos-engagements" style="background-color:#e5f0f1;">
      <div class="container is-animated">
        <div class="luxe-row no-gutter text-left">
          <div class="luxe-col-mobile-12">
            <div class="content">
              <h2 class="boxed-text">
                <span>Nos engagements</span>
              </h2>
            </div>
          </div>
          <div class="luxe-col-mobile-12">
            <div class="luxe-row no-gutter slider-engagements slider-flickity">
              <div class="engagement luxe-col-desktop-3">
                <div class="engagement-container">
                  <p class="title is-uppercase is-bold">énergie</p>
                  <p class="number has-counter is-bold">90<sup>%</sup></p>
                  <p>De la consommation d’électricité couverte par des certificats d’origine renouvelable depuis 2017</p>
                </div>
              </div>
              <div class="engagement luxe-col-desktop-3">
                <div class="engagement-container">
                  <p class="title is-uppercase is-bold">transports</p>
                  <p class="number has-counter is-bold">100<sup>%</sup></p>
                  <p>Des livraisons magasin en Île-de-France effectuées en bioGNV, émettant 80 % de CO2 de moins que le diesel</p>
                </div>
              </div>
              <div class="engagement luxe-col-desktop-3">
                <div class="engagement-container">
                  <p class="title is-uppercase is-bold">éléments de décors</p>
                  <p class="number has-counter is-bold">48</p>
                  <p>Reventes solidaires de nos éléments de vitrines et décors pour + de 78000€ reversés depuis 2017</p>
                </div>
              </div>
              <div class="engagement luxe-col-desktop-3">
                <div class="engagement-container">
                  <p class="title is-uppercase is-bold">Go For Good</p>
                  <p class="number has-counter is-bold">+1000</p>
                  <p>Marques proposent une offre plus responsable dans le cadre du label GO&nbsp;FOR&nbsp;GOOD des Galeries&nbsp;Lafayette</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  
    <section class="section lp-body arrondi"  style="background-color:#ffffff;">
      <div class="container">
        <div class="luxe-row no-gutter text-left luxe-reverse">
          <div class="luxe-col-mobile-12 luxe-col-tablet-5">
            <figure class="image is-fullheight">
              <img class="b-lazy"
              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/magasin-fleuri.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/magasin-fleuri@2x.jpg"
              alt="magasin-fleuri - Galeries Lafayette">
              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/magasin-fleuri.jpg" alt="magasin-fleuri - Galeries Lafayette" /></noscript>
            </figure>
          </div>
          <div class="luxe-col-mobile-12 luxe-col-tablet-7">
            <div class="content">
              <h3 class="is-uppercase">à vos votes !<br /> l’Arrondi en caisse</h3>
              <p>Pendant Nature Urbaine, vous pouvez arrondir le montant de vos achats au bénéfice du fonds de dotation ONF-Agir pour la forêt, qui œuvre à préserver le patrimoine naturel forestier fragilisé par une exposition grandissante aux crises sanitaires.</p>
              <p>Vous pouvez également voter pour le projet qui sera financé par votre arrondi : plantation, adaptation au changement climatique, refuges de biodiversité&hellip;</p>
              <button class="is-bold button outlined" data-reveal-id="popin-arrondi" target="_self">Participer</button>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="section lp-body re-store" id="la-seconde-main"  style="background-color:#e5f0f1;">
      <div class="container">
        <div class="luxe-row text-left">
          <div class="luxe-col-mobile-12 luxe-col-tablet-7">
            <a class="image-container" href="https://www.galerieslafayette.com/t/smile-for-equality">
              <figure class="image is-fullheight">
                <img class="b-lazy" style="object-position:left;"
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/restore.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/restore@2x.jpg"
                alt="La seconde main - Galeries Lafayette">
                <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/nature-urbaine/restore.jpg" alt="La seconde main - Galeries Lafayette" /></noscript>
              </figure>
            </a>
          </div>
          <div class="luxe-col-mobile-12 luxe-col-tablet-5">
            <div class="content">
              <h2 class="boxed-text">
                <span>La Seconde main</span>
              </h2>
              <h3 class="is-uppercase">Pour une mode plus circulaire</h3>
              <p>Des pièces uniques de seconde main à découvrir dans l'espace (RE)STORE au 3ème étage du magasin Coupole et une sélection également accessible en ligne.</p>
              <a href="https://www.galerieslafayette.com/offer/crushon" class="is-bold button outlined" target="_blank">voir la sélection en ligne</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  
    <section class="section lp-body univers">
      <div class="container">
        <div class="luxe-row no-gutter luxe-center-tablet">
          <div class="luxe-col-mobile-12">
            <a href="https://www.galerieslafayette.com/t/tendance-outdoor" class="univers-cta">
              <h2 class="boxed-text">
                <span>tout l'univers nature urbaine</span>
                <span>→︎</span>
              </h2>
            </a> 
          </div>
        </div>
      </div>
    </section>
  
    <div class="halo-top-selections"></div>
    <div class="halo-mid-selections"></div>
    <div class="halo-bottom-selections"></div>
  </div>

</div>



<div class="reveal-modal opened video-modal" data-reveal id="popin-arrondi" role="dialog">
  <div>
      <a class="close-reveal-modal" data-close aria-label="Close">Fermer</a>
      <div class="modal-content">
        <h3>à vos votes pour décider&nbsp;quel&nbsp;projet sera&nbsp;financé</h3>
        <p>Trois projets vous sont proposés par le fonds de dotation ONF-Agir pour la forêt pour préserver le patrimoine naturel forestier :</p>
        <ol>
          <li><p>Reconstitution de mares forestières refuges de biodiversité</p></li>
          <li><p>Plantation de châtaigniers pour régénérer les forêts de l’Ouest parisien décimées par la maladie de l'encre</p></li>
          <li><p>Création de parcelles pilotes pour tester la résistance de différentes essences d'arbres au changement climatique</p></li>
        </ol>
        <div class="text-center">
          <a class="is-bold button outlined" href="https://goforgoodgl.typeform.com/to/E2j8w28d" target="_blank">voter pour votre projet favori</a>
        </div>
      </div>
  </div>
</div>




<script src="https://static.galerieslafayette.com/media/LP/src/js/2022/nature-urbaine.min.v02.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->


<!-- build:js /media/LP/src/js/2022/nature-urbaine.min.v02.js
  <script src="../../assets/js/blazy.min.js"></script>
  <script src="../../assets/js/flickity.pkgd.min.js"></script>
  <script src="../../assets/js/ScrollMagic.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
  <script src="../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
  <script src="../src/js/2022/nature-urbaine.js"></script>
endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
