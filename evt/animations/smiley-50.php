<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Smiley";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!--  <link href="../../media/LP/src/css/2022/smiley-50.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
 <link href="https://static.galerieslafayette.com/media/LP/src/css/2022/smiley-50.min.v04.css" rel="stylesheet" type="text/css" />
<div class="lp-container">
  <section class="section lp-hero">
    <div class="container">
      <div class="luxe-row no-gutter">
        <div class="luxe-col-mobile-12 luxe-col-tablet-7">
          <div class="hero-image"></div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-5 luxe-col-fullhd-4 luxe-first-tablet">    
          <div class="boxed-text-container">      
            <h1 class="boxed-text">
              <span>Happy Birthday</span>
              <span>Smiley<sub class="r">&reg;</sub> !</span>
            </h1>
            <a href="#collection" class="has-smoothscroll selection-cta is-hidden-tablet">
              <p class="boxed-text selection">
                <span>Explorer la collection</span>
                <span>↓</span>
              </p>
            </a>
          </div>
          <div class="content main-text">
            <p>Du 21 février au 3 avril, SMILEY fête ses 50 ans aux Galeries Lafayette.</p>
            <p>En ligne et en magasin, des collaborations exclusives, des installations immersives et autres animations feel good vous invitent à prendre le temps de sourire&nbsp;!</p>
          </div>
          
          <a href="#collection" class="has-smoothscroll selection-cta is-hidden-mobile">
            <p class="boxed-text selection">
              <span>Découvrir</span>
              <span>↓</span>
            </p>
          </a>
        </div>
        <div class="luxe-col-mobile-12">
          <figure class="image smiley-logo">
            <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/logo-smiley.svg"
              alt="Smiley : Prenez le temps de sourire - Galeries Lafayette">
          </figure>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body collection" id="collection">
    <div class="container">
      <div class="luxe-row no-gutter">
        <div class="luxe-col-mobile-12 luxe-col-tablet-8 luxe-col-widescreen-10 luxe-col-fullhd-9">
          <div class="content">
            <h2 class="boxed-text">
              <span>Des produits</span><br>
              <span>100% bonne humeur</span>
            </h2>
            <p>50 marques se sont associées à Smiley pour imaginer des produits collector ou des collections capsules reprenant le fameux logo Smiley réinventé par l'artiste André Saraiva.
            </p>
          </div>
        </div>
      </div>
      
      <div class="products luxe-row text-center">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="luxe-row is-fullheight">
            <div class="product main luxe-col-mobile-12">
              <a href="https://www.galerieslafayette.com/p/serigraphie+andre+x+smiley-andre/83472202/256">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_main_top.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_main_top@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="588" height="588">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_main_top.jpg" alt="Smiley - Galeries Lafayette" width="747" height="747" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">ANDRÉ</p>
                  <p style="color: #000000;">Sérigraphie André x Smiley</p>
                </div>
              </a>
            </div>
          </div>
        </div>
        <!--   ROW 1   -->
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="luxe-row">
            <div class="product luxe-col-mobile-6">
              <a href="https://www.galerieslafayette.com/p/mini+jupe+sequins+x+smiley-alice+olivia/83472036/259" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_01.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_01@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_01.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">ALICE & OLIVIA</p>
                  <p>Mini jupe sequins x Smiley</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6">
              <a href="https://www.galerieslafayette.com/p/beret+x+smiley-laulhere/83472089/256" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_02.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_02@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_02.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">LAULHERE</p>
                  <p>Béret x Smiley</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6">
              <a href="https://www.galerieslafayette.com/p/hoodie+molletonne+x+smiley-raf+simons/83472178/256" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_03.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_03@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_03.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">RAF SIMONS</p>
                  <p>Hoodie molletonné x Smiley</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6">
              <a href="https://www.galerieslafayette.com/p/bougie+parfumee-the+original+smiley/82871109/85" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_04.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_04@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_04.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">THE ORIGINAL SMILEY</p>
                  <p>Bougie parfumée</p>
                </div>
              </a>
            </div>
          </div>
        </div>
        <!--   END ROW 1   -->
          
        <!--   ROW 2   -->
        <div class="luxe-col-mobile-12">
          <div class="luxe-row">
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/protege+ordinateur+collector+loqi+x+smiley-loqi/83472094/256" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_05.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_05@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_05.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">LOQI</p>
                  <p>Protège ordinateur Collector Loqi x Smiley</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/coque+iphone+blanche+mini-the+original+smiley/82871148/498" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_06.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_06@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_06.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">THE ORIGINAL SMILEY</p>
                  <p>Coque Iphone blanche Mini</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/tongs+hava+x+smiley-havaianas/82995031/259" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_07.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_07@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_07.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">HAVAIANAS</p>
                  <p>Tongs Hava x Smiley</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-3">
              <a href="https://www.galerieslafayette.com/p/veste+courte+matelassee+x+smiley-joshua+sanders/83472082/263" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_08.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_08@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_08.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">JOSHUA SANDERS</p>
                  <p>Veste courte matelassée x Smiley</p>
                </div>
              </a>
            </div>
          </div>
        </div>
        <!--  END ROW 2   -->
          
        <!--   ROW 3   -->
        <div class="luxe-col-mobile-12 luxe-col-tablet-3">
          <div class="luxe-row">
            <div class="product luxe-col-mobile-6 luxe-col-tablet-12">
              <a href="https://www.galerieslafayette.com/p/collier+pendentif+charm+s+smiley-les+nereides/300412938395/306" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_09.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_09@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_09.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">LES NÉRÉIDES</p>
                  <p>Collier pendentif Charm's Smiley</p>
                </div>
              </a>
            </div>            
            <div class="product luxe-col-mobile-6 luxe-col-tablet-12">
              <a href="https://www.galerieslafayette.com/p/ecouteurs+air1+zen+ear+pods+happy+plugs+x+smiley-happy+plugs/83472200/256" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_10.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_10@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_10.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">HAPPY PLUGS</p>
                  <p>Ecouteurs Air1 Zen Ear Pods Happy Plugs x Smiley</p>
                </div>
              </a>
            </div>            
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="luxe-row is-fullheight">
            <div class="product main luxe-col-mobile-12">
              <a href="https://www.galerieslafayette.com/p/bob+smiley+coton+bio+galeries+lafayette+x+smiley-galeries+lafayette/80563783/125">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_main_bottom.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_main_bottom@2x.jpg"
                    alt="Smiley - Galeries Lafayette" width="588" height="588">
                    <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_main_bottom.jpg" alt="Smiley - Galeries Lafayette" width="747" height="747" /></noscript>
                  </figure>
                </div>
                <div class="product-txt has-text-white">
                  <p class="is-uppercase">GALERIES LAFAYETTE</p>
                  <p class="">Bob Smiley coton bio Galeries Lafayette x Smiley</p>
                </div>
              </a>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-3">
          <div class="luxe-row">
            <div class="product luxe-col-mobile-6 luxe-col-tablet-12">
              <a href="https://www.galerieslafayette.com/p/ballon+de+beachball+3d+sunnylife+x+smiley-sunnylife/83472194/256" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_11.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_11@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_11.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">SUNNYLIFE</p>
                  <p>Ballon de beachball 3D Sunnylife x Smiley</p>
                </div>
              </a>
            </div>
            <div class="product luxe-col-mobile-6 luxe-col-tablet-12">
              <a href="https://www.galerieslafayette.com/p/serviette+de+bain+slowtide+x+smiley-slowtide/83472192/256" class="">
                <div class="product-image">
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_12.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_12@2x.jpg"
                      alt="Smiley - Galeries Lafayette" width="294" height="294">
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/produits/round-2/product_12.jpg" alt="Smiley - Galeries Lafayette" width="294" height="294" /></noscript>
                  </figure>
                </div>
                <div class="product-txt">
                  <p class="is-uppercase">SLOWTIDE</p>
                  <p>Serviette de bain Slowtide x Smiley</p>
                </div>
              </a>
            </div>
          </div>
        </div>
        <!--   END ROW 3   -->
        
      </div>
      
      <div class="luxe-row no-gutter">
        <div class="luxe-col-mobile-12 text-center">
          <a href="https://www.galerieslafayette.com/t/offre-smiley" class="collection-cta">Voir toute la collection<span class="plus"></span></a>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body graff">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-10">
          <div class="content">
            <figure class="image default-graff">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/graff-prenezletemps.svg"
                alt="Prenez le temps de sourire - Galeries Lafayette">
            </figure>
            <p>Cliquez sur "Démarrer" et utilisez votre <span class="is-hidden-desktop">doigt</span> <span class="is-hidden-touch">souris</span> pour graffer le dessin ou <br class="is-hidden-touch">le message de votre choix. Place à l’imagination et à la créativité&nbsp;!</p>
            <button id="startgraff" class="button primary has-icon" type="button">Démarrer<span class="icon has-arrow"></span></button>
          </div>
        </div>
        <div class="luxe-col-mobile-12">
          <div class="graffcontainer is-hidden">
            <button id="endgraff" class="close-button" type="button"></button>
            <canvas id="graffcanvas" width="320" height="500"></canvas>
            <div class="buttons">
              <button id="clear-button" class="button outlined" type="button">Recommencer</button>
              <a id="save-button" class="button outlined" download="my-graffiti" target="_blank">Enregistrer</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body section-with-image andre">
    <div class="container">
      <div class="luxe-row text-left">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-last-tablet">
          <div class="visuel-container">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/mr-andre.gif" 
                alt="Prenez le temps de sourire - Galeries Lafayette">
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <h2 class="boxed-text">
              <span>MR. A</span>
              <span>André Saraiva</span>
            </h2>
            <p class="has-text-white">Pour son anniversaire, Smiley fait appel à l’artiste André Saraiva pour réinventer son logo. Figure emblématique du graffiti parisien, André est devenu mondialement connu grâce à son alter ego Mr. A : une signature et un personnage qui apparaît aux quatre coins du monde. Pour Smiley, André Saraiva a voulu capturer les valeurs au cœur de la marque que sont la joie et l’optimisme.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body section-with-image haussmann">
    <div class="container">
      <div class="luxe-row no-gutter visuel-container">
        <a class="luxe-col-mobile-12 visuel" href="https://haussmann.galerieslafayette.com/prada-le-pop-up" target="_blank"></a>
      </div>
      <div class="luxe-row no-gutter text-left">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <h2 class="boxed-text">
              <span>Smiley<sub class="r">&reg;</sub> fait <span class="is-hidden-mobile">sourire</span></span>
              <span><span class="is-hidden-tablet">sourire</span> votre <span class="is-hidden-mobile">magasin</span></span>
              <span class="is-hidden-tablet"> magasin</span>
            </h2>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <p class="has-text-white">L’expérience Smiley se vit jusqu’au bout du smile. Du 21 février au 2 avril, le skateboard s’invite aux Galeries Lafayette avec l’installation d’une rampe de skate aux couleurs de Smiley sur le boulevard Haussmann, en face du magasin de l’Homme. Rendez-vous également sur la Smiley Gaming Area, pour tester une sélection de jeux d’Arcade originaux et des consoles de jeux en réalité virtuelle. </p>
            <a href="https://haussmann.galerieslafayette.com/50-ans-de-smiley" class="is-bold button  has-icon primary outlined" target="_blank">En savoir plus<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body section-with-image fondation" style="background-color:#FFE816;">
    <div class="container">
      <div class="luxe-row text-left">
        <div class="luxe-col-mobile-12 luxe-col-tablet-7 luxe-col-desktop-6 luxe-last-tablet">
          <a class="visuel-container" href="https://www.galerieslafayette.com/t/smile-for-equality">
            <figure class="image is-hidden-tablet">
              <img class="b-lazy" 
              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/bandana-yellow_M.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/bandana-yellow_M@2x.jpg"
              alt="Smiley for equality - Galeries Lafayette" width="360" height="450">
              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/bandana-yellow_M.jpg" alt="Smiley for equality - Galeries Lafayette" width="360" height="450" /></noscript>
            </figure>
            <figure class="image is-hidden-mobile">
              <img class="b-lazy" 
              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
              data-src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/bandana-yellow_D.jpg|https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/bandana-yellow_D@2x.jpg"
              alt="Smiley for equality - Galeries Lafayette" width="720" height="800">
              <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/smiley-50/bandana-yellow_M.jpg" alt="Smiley for equality - Galeries Lafayette" width="720" height="800" /></noscript>
            </figure>
          </a>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-5 luxe-col-desktop-6">
          <div class="content">
            <h2 class="boxed-text">
              <span>Smile for equality</span>
              <span>Galeries&nbsp;Lafayette</span>
              <span style="border-top:none;">S'engage</span>
            </h2>
            <p>À l’occasion de la journée internationale du droit des Femmes, Galeries Lafayette vous invitent à faire un don à la Fondation des Femmes, à travers le dispositif d’arrondi en caisse jusqu'au 3 avril.</p>
            <p>Du 25 février au 9 mars, découvrez aussi une sélection de produits caritatifs, signalés par un Smiley, dont une partie du prix de vente sera reversée à des associations soutenant le droit des Femmes. Enfin, les bénéfices de la vente d’un bandana développé par la marque propre des Galeries Lafayette seront reversés à la Fondation des Femmes.</p>
            <a href="https://www.galerieslafayette.com/p/bandana+smiley+coton+organique-galeries+lafayette/80563776/356" class="is-bold button  has-icon primary outlined inverted" target="_blank">En savoir plus<span class="icon has-arrow"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body section-with-image fondation-vid" style="background-color:#FFE816;" id="smile-for-equality">
    <div class="container">
      <div class="luxe-row text-left luxe-middle-tablet luxe-center-tablet">
        <div class="luxe-col-mobile-12">
          <div class="visuel-container">
            <iframe class="bg-video video" src="https://player.vimeo.com/video/686196051?h=4e889f5a60&loop=1" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen  mozallowfullscreen="" webkitallowfullscreen=""></iframe>           
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-10 luxe-col-desktop-8">
          <div class="content">
            <h2 class="boxed-text is-hidden">
              <span>Smile for equality</span>
              <span>Ils témoignent</span>
            </h2>
            <p>À l'occasion de la journée internationale des droits des Femmes, les Galeries Lafayette s'engagent aux côtés de la Fondation des Femmes et de Smiley et présentent : SMILE FOR EQUALITY (*Souriez pour l'égalité). Découvrez les interviews de cinq personnalités qui partagent une vision positive de l’égalité hommes-femmes pour aujourd’hui et demain.
              </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section lp-body univers">
    <div class="container">
      <div class="luxe-row no-gutter luxe-center-tablet">
        <div class="luxe-col-mobile-12">
          <a href="https://www.galerieslafayette.com/t/produits-smiley-collector" class="univers-cta">
            <h2 class="boxed-text">
              <span>tout l'univers <span class="is-hidden-mobile">Smiley<sub class="r">&reg;</sub></span></span>
              <span class="is-hidden-tablet">Smiley<sub class="r">&reg;</sub><br  /></span>
              <span><span class="is-hidden-tablet">découvrir →</span><span class="is-hidden-mobile">→︎</span></span>
            </h2>
          </a> 
        </div>
      </div>
    </div>
  </section>

</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2022/smiley-50.min.v02.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->

<!-- build:js /media/LP/src/js/2022/smiley-50.min.v02.js
  <script src="../../assets/js/blazy.min.js"></script>
  <script src="../../assets/js/flickity.pkgd.min.js"></script>
  <script src="../src/js/2022/smiley-50.js"></script>
endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
