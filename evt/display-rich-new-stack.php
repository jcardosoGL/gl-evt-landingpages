<?php include ('pages-defaults/header-rich-new-stack.php'); ?>
<script>
	document.title = "Rich New Stack";
</script>

		
										<section name="ProductList" __typename="ProductList" seoPaginationText="page 1" class="product-list dynamic-components__component" data-v-b46d8838 data-v-995baa2e>
											
												<div data-test-id="listpageheader1" class="product-list__heading" data-v-b46d8838>
														<div data-test-id="listpagebreadcrumb" class="breadcrumb product-list__breadcrumb" data-v-07a2e998 data-v-b46d8838>
																<a href="/c/homme" class="breadcrumb__link router-link-active" data-v-07a2e998>
																		<span class="label" data-v-07a2e998>Mode homme</span>
																		/ 
																</a>
																<span class="breadcrumb__link breadcrumb__link--desactivated" data-v-07a2e998>
																		<span class="label" data-v-07a2e998>Vêtements homme</span>
																</span>
														</div>
														<h1 data-test-id="listpagetitle" class="product-list__title" data-v-b46d8838>Vêtements homme </h1>
												</div>
												
												<?php include ('rich-luxe/template.html'); ?>
												
												
												
												<header data-header-reactive="product-list__header" data-test-id="listpageheader2" class="product-list__header" data-v-b46d8838>
														<div class="product-list__actions" data-v-b46d8838>
																<div class="product-list__filters-switcher" data-v-b46d8838>
																		<button data-test-id="listpagefilterbutton" class="gl-button product-list__filters-switcher-button gl-button-primary" data-v-b46d8838>Filtrer par </button>
																</div>
																<div class="product-list__sticky-title" data-v-b46d8838>Vêtements homme </div>
																<div data-test-id="listpagevisualisationcomponent" class="product-list__grid-switcher" data-v-b46d8838>
																		<button data-test-id="listpage3productbutton" class="icon__wrapper product-list__grid-switcher-button icon__wrapper--clickable" style="--size:32px;" data-v-53fe7467 data-v-b46d8838>
																				
																		</button>
																		<button data-test-id="listpage4productbutton" class="icon__wrapper product-list__grid-switcher-button icon__wrapper--clickable product-list__grid-switcher-button--active" style="--size:32px;" data-v-53fe7467 data-v-b46d8838>
																				
																		</button>
																</div>
																<div class="product-list__sort-switcher" data-v-b46d8838>
																		<div id="_0ifrgdvph" data-test-id="selectmain" class="gl-select gl-select--size-s" data-v-33e152a6 data-v-b46d8838>
																				<div class="gl-select__dropdown" data-v-33e152a6>
																						<div class="gl-select__label gl-select__label--with-filter" data-v-33e152a6>
																								<span class="gl-select__filter" data-v-33e152a6>Trier par </span>
																								<svg xmlns="http://www.w3.org/2000/svg" aria-labelledby="triangle-down" role="presentation" width="1rem" height="1rem" viewport="0 0 1rem 1rem" fill="#000" class="gl-icon" data-v-33e152a6 data-v-64f47b68>
																										<title id="triangle-down" data-v-33e152a6 data-v-64f47b68>triangle-down</title>
																										<g data-v-33e152a6 data-v-64f47b68>
																												<path fill-rule="evenodd" d="M0 3l8 12 8-12z" data-v-33e152a6 data-v-33e152a6></path>
																										</g>
																								</svg>
																						</div>
																						<div class="gl-select__options" data-v-33e152a6>
																								<div data-test-id="option0" class="gl-option gl-option--as-filter" data-v-73f7526a>
																										<span class="gl-option__label" data-v-73f7526a>Prix croissant </span>
																								</div>
																								<div data-test-id="option1" class="gl-option gl-option--as-filter" data-v-73f7526a>
																										<span class="gl-option__label" data-v-73f7526a>Prix décroissant </span>
																								</div>
																						</div>
																				</div>
																		</div>
																</div>
														</div>
												</header>
												<main class="product-list__main" data-v-b46d8838>
														<div class="products-grid" data-v-1e20e04e data-v-b46d8838>
																<div class="products-grid__wrapper" data-v-1e20e04e>
																		<a href="/p/parka+mi-longue+urban-woolrich/72351716/320" title="Parka mi-longue Urban - Noir - Woolrich" class="product-card-wrapper product-card-wrapper--4-columns product-card-wrapper--no-border-top" data-v-1e20e04e>
																				<article class="product-card" data-v-5f1fab5e>
																						<section class="product-card__carousel" data-v-5f1fab5e>
																								<img alt="Parka mi-longue Urban" src="https://static.galerieslafayette.com/media/723/72351716/G_72351716_320_VPP_1.jpg" srcset="" class="product-card__carousel-wrapper" data-v-5f1fab5e data-v-5f1fab5e>
																						</section>
																						<section class="product-card__infos" data-v-5f1fab5e>
																								<section class="product-info" data-v-73342aec data-v-5f1fab5e>
																										<div class="product-info__idle" data-v-73342aec>
																												<div class="product-info__brand" data-v-73342aec>Woolrich </div>
																												<div class="product-info__description" data-v-73342aec>Parka mi-longue Urban </div>
																												<div class="product-info__price" data-v-73342aec>
																														<div class="product-info__price-final product-info__price-final--highlighted" data-v-73342aec>682,50 € </div>
																														<div class="product-info__price-original" data-v-73342aec>
																																<div class="product-info__price-striked" data-v-73342aec>975,00 € </div>
																																<div class="product-info__promotion" data-v-73342aec>-30% </div>
																														</div>
																												</div>
																										</div>
																								</section>
																						</section>
																						<div class="product-card__frame fade-on-desktop" data-v-5f1fab5e></div>
																				</article>
																		</a>
																		<a href="/p/doudoune+courte+droite+matelassee+capuche-polo+ralph+lauren/73070507/125" title="Doudoune courte droite matelassée capuche - Bleu - Polo Ralph Lauren" class="product-card-wrapper product-card-wrapper--4-columns product-card-wrapper--no-border-top" data-v-1e20e04e>
																				<article class="product-card" data-v-5f1fab5e>
																						<section class="product-card__carousel" data-v-5f1fab5e>
																								<img alt="Doudoune courte droite matelassée capuche" src="https://static.galerieslafayette.com/media/730/73070507/G_73070507_125_VPP_1.jpg" srcset="" class="product-card__carousel-wrapper" data-v-5f1fab5e data-v-5f1fab5e>
																						</section>
																						<section class="product-card__infos" data-v-5f1fab5e>
																								<section class="product-info" data-v-73342aec data-v-5f1fab5e>
																										<div class="product-info__idle" data-v-73342aec>
																												<div class="product-info__brand" data-v-73342aec>Polo Ralph Lauren </div>
																												<div class="product-info__description" data-v-73342aec>Doudoune courte droite matelassée capuche </div>
																												<div class="product-info__price" data-v-73342aec>
																														<div class="product-info__price-final product-info__price-final--highlighted" data-v-73342aec>199,50 € </div>
																														<div class="product-info__price-original" data-v-73342aec>
																																<div class="product-info__price-striked" data-v-73342aec>399,00 € </div>
																																<div class="product-info__promotion" data-v-73342aec>-50% </div>
																														</div>
																												</div>
																										</div>
																								</section>
																						</section>
																						<div class="product-card__frame fade-on-desktop" data-v-5f1fab5e></div>
																				</article>
																		</a>
																		<a href="/p/pull+droit+en+laine-polo+ralph+lauren/73070875/230" title="Pull droit en laine - Gris - Polo Ralph Lauren" class="product-card-wrapper product-card-wrapper--4-columns product-card-wrapper--no-border-top" data-v-1e20e04e>
																				<article class="product-card" data-v-5f1fab5e>
																						<section class="product-card__carousel" data-v-5f1fab5e>
																								<img alt="Pull droit en laine" src="https://static.galerieslafayette.com/media/463/46333097/G_46333097_230_VPP_1.jpg" srcset="" class="product-card__carousel-wrapper" data-v-5f1fab5e data-v-5f1fab5e>
																						</section>
																						<section class="product-card__infos" data-v-5f1fab5e>
																								<section class="product-info" data-v-73342aec data-v-5f1fab5e>
																										<div class="product-info__idle" data-v-73342aec>
																												<div class="product-info__brand" data-v-73342aec>Polo Ralph Lauren </div>
																												<div class="product-info__description" data-v-73342aec>Pull droit en laine </div>
																												<div class="product-info__price" data-v-73342aec>
																														<div class="product-info__price-final product-info__price-final--highlighted" data-v-73342aec>111,30 € </div>
																														<div class="product-info__price-original" data-v-73342aec>
																																<div class="product-info__price-striked" data-v-73342aec>159,00 € </div>
																																<div class="product-info__promotion" data-v-73342aec>-30% </div>
																														</div>
																												</div>
																										</div>
																										<div class="product-info__note" data-v-73342aec>Existe en 6 coloris </div>
																								</section>
																						</section>
																						<div class="product-card__frame fade-on-desktop" data-v-5f1fab5e></div>
																				</article>
																		</a>
																		<a href="/p/jean+slim+delave+coton-the+kooples/72443374/230" title="Jean slim délavé coton - Gris - The Kooples" class="product-card-wrapper product-card-wrapper--4-columns product-card-wrapper--no-border-top" data-v-1e20e04e>
																				<article class="product-card" data-v-5f1fab5e>
																						<section class="product-card__carousel" data-v-5f1fab5e>
																								<img alt="Jean slim délavé coton" src="https://static.galerieslafayette.com/media/724/72443374/G_72443374_230_VPP_1.jpg" srcset="" class="product-card__carousel-wrapper" data-v-5f1fab5e data-v-5f1fab5e>
																						</section>
																						<section class="product-card__infos" data-v-5f1fab5e>
																								<section class="product-info" data-v-73342aec data-v-5f1fab5e>
																										<div class="product-info__idle" data-v-73342aec>
																												<div class="product-info__brand" data-v-73342aec>The Kooples </div>
																												<div class="product-info__description" data-v-73342aec>Jean slim délavé coton </div>
																												<div class="product-info__price" data-v-73342aec>
																														<div class="product-info__price-final product-info__price-final--highlighted" data-v-73342aec>84,00 € </div>
																														<div class="product-info__price-original" data-v-73342aec>
																																<div class="product-info__price-striked" data-v-73342aec>168,00 € </div>
																																<div class="product-info__promotion" data-v-73342aec>-50% </div>
																														</div>
																												</div>
																										</div>
																								</section>
																						</section>
																						<div class="product-card__frame fade-on-desktop" data-v-5f1fab5e></div>
																				</article>
																		</a>
																		<a href="/p/chemise+droite+coton+a+carreaux-eden+park/72289363/117" title="Chemise droite coton à carreaux - Bleu - Eden Park" class="product-card-wrapper product-card-wrapper--4-columns" data-v-1e20e04e>
																				<article class="product-card" data-v-5f1fab5e>
																						<section class="product-card__carousel" data-v-5f1fab5e>
																								<img alt="Chemise droite coton à carreaux" src="https://static.galerieslafayette.com/media/722/72289363/G_72289363_117_VPP_1.jpg" srcset="" class="product-card__carousel-wrapper" data-v-5f1fab5e data-v-5f1fab5e>
																						</section>
																						<section class="product-card__infos" data-v-5f1fab5e>
																								<section class="product-info" data-v-73342aec data-v-5f1fab5e>
																										<div class="product-info__idle" data-v-73342aec>
																												<div class="product-info__brand" data-v-73342aec>Eden Park </div>
																												<div class="product-info__description" data-v-73342aec>Chemise droite coton à carreaux </div>
																												<div class="product-info__price" data-v-73342aec>
																														<div class="product-info__price-final product-info__price-final--highlighted" data-v-73342aec>60,00 € </div>
																														<div class="product-info__price-original" data-v-73342aec>
																																<div class="product-info__price-striked" data-v-73342aec>120,00 € </div>
																																<div class="product-info__promotion" data-v-73342aec>-50% </div>
																														</div>
																												</div>
																										</div>
																								</section>
																						</section>
																						<div class="product-card__frame fade-on-desktop" data-v-5f1fab5e></div>
																				</article>
																		</a>
																		<a href="/p/caban+peacoat+en+laine+melangee-lee/73071630/125" title="Caban Peacoat en laine mélangée - Bleu - Lee" class="product-card-wrapper product-card-wrapper--4-columns" data-v-1e20e04e>
																				<article class="product-card" data-v-5f1fab5e>
																						<section class="product-card__carousel" data-v-5f1fab5e>
																								<img alt="Caban Peacoat en laine mélangée" src="https://static.galerieslafayette.com/media/730/73071630/G_73071630_125_VPP_1.jpg" srcset="" class="product-card__carousel-wrapper" data-v-5f1fab5e data-v-5f1fab5e>
																						</section>
																						<section class="product-card__infos" data-v-5f1fab5e>
																								<section class="product-info" data-v-73342aec data-v-5f1fab5e>
																										<div class="product-info__idle" data-v-73342aec>
																												<div class="product-info__brand" data-v-73342aec>Lee </div>
																												<div class="product-info__description" data-v-73342aec>Caban Peacoat en laine mélangée </div>
																												<div class="product-info__price" data-v-73342aec>
																														<div class="product-info__price-final product-info__price-final--highlighted" data-v-73342aec>124,97 € </div>
																														<div class="product-info__price-original" data-v-73342aec>
																																<div class="product-info__price-striked" data-v-73342aec>249,95 € </div>
																																<div class="product-info__promotion" data-v-73342aec>-50% </div>
																														</div>
																												</div>
																										</div>
																								</section>
																						</section>
																						<div class="product-card__frame fade-on-desktop" data-v-5f1fab5e></div>
																				</article>
																		</a>
																		<a href="/p/chemise+slim+carreaux+fantaisie-tommy+hilfiger/72618949/88" title="Chemise slim carreaux fantaisie - Blanc - Tommy Hilfiger" class="product-card-wrapper product-card-wrapper--4-columns" data-v-1e20e04e>
																				<article class="product-card" data-v-5f1fab5e>
																						<section class="product-card__carousel" data-v-5f1fab5e>
																								<img alt="Chemise slim carreaux fantaisie" src="https://static.galerieslafayette.com/media/726/72618949/G_72618949_88_VPP_1.jpg" srcset="" class="product-card__carousel-wrapper" data-v-5f1fab5e data-v-5f1fab5e>
																						</section>
																						<section class="product-card__infos" data-v-5f1fab5e>
																								<section class="product-info" data-v-73342aec data-v-5f1fab5e>
																										<div class="product-info__idle" data-v-73342aec>
																												<div class="product-info__brand" data-v-73342aec>Tommy Hilfiger </div>
																												<div class="product-info__description" data-v-73342aec>Chemise slim carreaux fantaisie </div>
																												<div class="product-info__price" data-v-73342aec>
																														<div class="product-info__price-final product-info__price-final--highlighted" data-v-73342aec>54,50 € </div>
																														<div class="product-info__price-original" data-v-73342aec>
																																<div class="product-info__price-striked" data-v-73342aec>109,00 € </div>
																																<div class="product-info__promotion" data-v-73342aec>-50% </div>
																														</div>
																												</div>
																										</div>
																								</section>
																						</section>
																						<div class="product-card__frame fade-on-desktop" data-v-5f1fab5e></div>
																				</article>
																		</a>
																		<a href="/p/surchemise+droite+effet+bouilli-scotch+and+soda/71746262/154" title="Surchemise droite effet bouilli - Beige - Scotch And Soda" class="product-card-wrapper product-card-wrapper--4-columns" data-v-1e20e04e>
																				<article class="product-card" data-v-5f1fab5e>
																						<section class="product-card__carousel" data-v-5f1fab5e>
																								<img alt="Surchemise droite effet bouilli" src="https://static.galerieslafayette.com/media/717/71746262/G_71746262_154_VPP_1.jpg" srcset="" class="product-card__carousel-wrapper" data-v-5f1fab5e data-v-5f1fab5e>
																						</section>
																						<section class="product-card__infos" data-v-5f1fab5e>
																								<section class="product-info" data-v-73342aec data-v-5f1fab5e>
																										<div class="product-info__idle" data-v-73342aec>
																												<div class="product-info__brand" data-v-73342aec>Scotch And Soda </div>
																												<div class="product-info__description" data-v-73342aec>Surchemise droite effet bouilli </div>
																												<div class="product-info__price" data-v-73342aec>
																														<div class="product-info__price-final product-info__price-final--highlighted" data-v-73342aec>74,50 € </div>
																														<div class="product-info__price-original" data-v-73342aec>
																																<div class="product-info__price-striked" data-v-73342aec>149,00 € </div>
																																<div class="product-info__promotion" data-v-73342aec>-50% </div>
																														</div>
																												</div>
																										</div>
																								</section>
																						</section>
																						<div class="product-card__frame fade-on-desktop" data-v-5f1fab5e></div>
																				</article>
																		</a>																		
																</div>
														</div>
												</main>
												<footer class="product-list__footer" data-v-b46d8838>
														<div class="seo-links" data-v-366e744e>
																<a href="/c/homme/vetements" aria-current="page" class="router-link-exact-active router-link-active">1 </a>
																<a href="/c/homme/vetements/p:2">2 </a>
																<a href="/c/homme/vetements/p:3">3 </a>
																<a href="/c/homme/vetements/p:4">4 </a>
																<a href="/c/homme/vetements/p:5">5 </a>
																<a href="/c/homme/vetements/p:138">138 </a>
														</div>
														<div class="pagination product-list__pagination pagination--large" data-v-35cf1d02 data-v-b46d8838>
																<div role="button" data-test-id="pagination1button" class="pagination__element pagination__element--current pagination__element--first" data-v-35cf1d02>1 </div>
																<div role="button" data-test-id="pagination2button" class="pagination__element" data-v-35cf1d02>2 </div>
																<div role="button" data-test-id="pagination3button" class="pagination__element" data-v-35cf1d02>3 </div>
																<div role="button" data-test-id="pagination4button" class="pagination__element" data-v-35cf1d02>4 </div>
																<div role="button" data-test-id="pagination5button" class="pagination__element" data-v-35cf1d02>5 </div>
																<div role="button" data-test-id="pagination6button" class="pagination__element" data-v-35cf1d02>6 </div>
																<div role="button" data-test-id="pagination7button" class="pagination__element" data-v-35cf1d02>7 </div>
																<div role="button" data-test-id="pagination8button" class="pagination__element" data-v-35cf1d02>8 </div>
																<div class="pagination__action" data-v-35cf1d02>
																		<div class="pagination__outer-action" data-v-35cf1d02>
																				<div class="pagination__separator" data-v-35cf1d02></div>
																				<div role="button" data-test-id="pagination138button" class="pagination__outer-page" data-v-35cf1d02>138 </div>
																		</div>
																		<div role="button" data-test-id="paginationnextbutton" class="pagination__arrow pagination__arrow--right" data-v-35cf1d02>
																				<svg xmlns="http://www.w3.org/2000/svg" aria-labelledby="arrow-right" role="presentation" width="1rem" height="1rem" viewport="0 0 1rem 1rem" fill="#000" class="gl-icon" data-v-35cf1d02 data-v-64f47b68>
																						<title id="arrow-right" data-v-35cf1d02 data-v-64f47b68>arrow-right</title>
																						<g data-v-35cf1d02 data-v-64f47b68>
																								<path fill-rule="evenodd" d="M11.05 3.964L14.086 7H0v1h14.086l-3.036 3.035.707.708L16 7.5l-4.243-4.242z" data-v-35cf1d02 data-v-35cf1d02></path>
																						</g>
																				</svg>
																		</div>
																</div>
														</div>
														<div class="product-list__pagination-text" data-v-b46d8838>
																<p>
																		Selon l’âge et la personnalité, les <strong>vêtements pour homme </strong>
																		varient d’un dressing à un autre. Tantôt workwear, tantôt casual chic ou preppy, la mode se conjugue désormais au masculin. Tout au long de l’année, les tendances se succèdent au gré des envies et des saisons.
																</p>
																<br>
																<h2>
																		<strong>Comment choisir ses vêtements pour homme ?</strong>
																</h2>
																<br>
																<p>
																		Le choix des <strong>vêtements pour homme</strong>
																		dépend avant tout des saisons. En hiver, pulls, sweats et cardigans volent la vedette aux polos et t-shirts. Et durant les journées estivales, ce sont les 
																		<a href="https://www.galerieslafayette.com/c/homme-pret-a-porter-shorts+et+bermudas">
																				<strong>
																						<u>shorts et bermudas</u>
																				</strong>
																		</a>
																		qui opèrent un retour en force.
																</p>
																<p>
																		Au-delà des saisons, le contexte est un autre point essentiel à prendre en considération. En effet, le style vestimentaire pour une soirée entre amis diffère de celui pour se rendre au travail, où les <strong>costumes</strong>
																		sont souvent de mise. Style chic, casual ou plutôt streetwear, l’important est de savoir adapter sa tenue vestimentaire à l’occasion.
																</p>
																<p>
																		Qu’il s’agisse de tenues tendance ou de circonstance, le choix des <strong>vêtements pour homme</strong>
																		est avant tout individuel. L’objectif étant de mettre en avant ses goûts et sa personnalité. Ainsi, les hommes qui veulent se démarquer privilégieront des pièces vestimentaires originales.
																</p>
																<br>
																<h2>
																		<strong>Vêtements pour homme : les indispensables</strong>
																</h2>
																<br>
																<p>
																		En matière de <strong>vêtements pour homme</strong>
																		, certaines pièces sont incontournables. Pour une allure élégante, le costume trois-pièces de couleur sobre est un must. Il sera accompagné d’une 
																		<a href="https://www.galerieslafayette.com/c/homme-pret-a-porter-chemises">
																				<strong>
																						<u>chemise</u>
																				</strong>
																		</a>
																		et d’une ceinture, en cuir de préférence. Pour compléter la tenue, il est recommandé de porter une paire de mocassins en daim ou en cuir. Ici, l’accent est mis sur les couleurs sobres qui permettent d’obtenir une certaine cohérence sur l’ensemble.
																</p>
																<p>
																		Dans un registre plus confortable, le style casual chic plaît pour sa simplicité. Parmi les basiques figurent l’indémodable <strong>t-shirt</strong>
																		. Les vestes et les <strong>jeans </strong>
																		occupent également une place essentielle dans la garde-robe masculine. À cela s’ajoutent divers
																		<a href="https://www.galerieslafayette.com/c/accessoires-accessoires+homme">
																				<strong>
																						<u>accessoires</u>
																				</strong>
																		</a>
																		comme les chapeaux et les lunettes de soleil.
																</p>
																<p>
																		Dans la catégorie workwear, les vêtements se veulent pratiques et fonctionnels. Pour le haut, les cabans et les <strong>vestes</strong>
																		de couleur noire, beige ou encore bleu marine sont les éléments centraux. La chemise en coton épais, unie ou à carreaux, et le <strong>jean droit</strong>
																		à toile épaisse complètent l’ensemble.
																</p>
														</div>
												</footer>
										</section>

<?php include ('pages-defaults/footer-rich-new-stack.php'); ?>