document.addEventListener("DOMContentLoaded", function(event) {

	document.querySelector(".roland-garros").classList.add('has-js');
		
	//============================================ Smooth scroll ===========================================//
	var smoothAnchor = document.querySelectorAll('.has-smoothscroll');
	for (var i=0; i < smoothAnchor.length; i++) {
		smoothAnchor[i].addEventListener("click", function(e) {
			e.preventDefault();
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);
			if (target) {
				if (window.matchMedia("(min-width:1024px)").matches) {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 60;
				} else {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset;
				}
				scroll({
					top: offsetTop,
					behavior: "smooth"
				});
			}
		});
	}
	//======================================================================================================//
	
	//======================================= Scroll Magic ======================================//
	//https://github.com/janpaepke/ScrollMagic/wiki/Getting-Started-:-How-to-use-ScrollMagic
	// init controller
	var controller = new ScrollMagic.Controller();
	
	// create a scene - ajoute la class .visible sur les articles
	// toggle class
	var revealElements = document.querySelectorAll(".scrolling-text-container");
	//console.log(revealElements.length);
	for (var i=0; i<revealElements.length; i++) { // create a scene for each element
		var revealArticles = new ScrollMagic.Scene({
			triggerElement: revealElements[i],
			triggerHook: 1, // show, when scrolled 10% into view
			duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
			reverse: true, // only do once
			offset: 0 // move trigger to center of element
		})
		.setClassToggle(revealElements[i], "animate") // add class to reveal
		//.addIndicators({name: "article" + (i+1) }) // add indicators (requires plugin) for debbug
		.addTo(controller);
	}
	//======================================================================================================//
	
	
	//============================================ Slider ===========================================//
	var elemSliderHeritage = document.querySelector(".slider-heritage");
	var sliderHeritage = new Flickity(elemSliderHeritage, {
		// options
		//initialIndex: 1,
		cellAlign: "center",
		contain: true, // default :true place la carousel sur la gauche de la page
		percentPosition: false,
		prevNextButtons: false,
		autoPlay: false,
		pageDots: true,
		setGallerySize: false, //pour que la hauteur ne soit pas définie par flickity si on définis déjà la hauteur en css
		watchCSS: true,
		dragThreshold: 15,
		groupCells: 2
	});
	sliderHeritage.on("scroll", function(progress) {
		bLazy.revalidate();
	});
	//======================================================================================================//
	
	//============================================ Slider ===========================================//
	var elemSliderCapsule = document.querySelector(".slider-capsule");
	var sliderCapsule = new Flickity(elemSliderCapsule, {
		// options
		//initialIndex: 1,
		cellAlign: "center",
		contain: true, // default :true place la carousel sur la gauche de la page
		percentPosition: false,
		prevNextButtons: false,
		autoPlay: false,
		pageDots: true,
		setGallerySize: false, //pour que la hauteur ne soit pas définie par flickity si on définis déjà la hauteur en css
		watchCSS: true,
		dragThreshold: 15,
		groupCells: 2
	});
	sliderCapsule.on("scroll", function(progress) {
		bLazy.revalidate();
	});
	//======================================================================================================//
	
	//============================================ Slider ===========================================//
	var elemSliderColorblock = document.querySelector(".slider-colorblock");
	var sliderColorblock = new Flickity(elemSliderColorblock, {
		// options
		//initialIndex: 1,
		cellAlign: "center",
		contain: true, // default :true place la carousel sur la gauche de la page
		percentPosition: false,
		prevNextButtons: false,
		autoPlay: false,
		pageDots: true,
		setGallerySize: false, //pour que la hauteur ne soit pas définie par flickity si on définis déjà la hauteur en css
		watchCSS: true,
		dragThreshold: 15,
		groupCells: 2
	});
	sliderColorblock.on("scroll", function(progress) {
		bLazy.revalidate();
	});
	//======================================================================================================//
	
});
