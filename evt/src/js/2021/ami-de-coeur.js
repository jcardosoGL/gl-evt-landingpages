document.addEventListener("DOMContentLoaded", function(event) {
	
	document.querySelector(".ami-de-coeur").classList.add('has-js');
	
	var readMore = document.querySelectorAll('.read-more');
	for (var i=0; i < readMore.length; i++) {
		var readMoreTrigger = readMore[i].querySelector('.read-more-toggle');
		readMoreTrigger.addEventListener("click", function(e) {
			e.preventDefault();
			this.nextElementSibling.classList.toggle('hidden');
		});
		
		var readLessTrigger = readMore[i].querySelector('.read-less-toggle');
		readLessTrigger.addEventListener("click", function(e) {
			e.preventDefault();
			this.parentNode.classList.toggle('hidden');
		});
	}		
	
	//======================================= Scroll Magic ======================================//
	//https://github.com/janpaepke/ScrollMagic/wiki/Getting-Started-:-How-to-use-ScrollMagic
	// init controller
	var controller = new ScrollMagic.Controller();
	
	// create a scene - effet de parallax sur article 2
	// build tween
	var tweenArticle = TweenMax.to(".animated-article .article-image", 1, {bottom: 80});
	// build scene
	var tweenArticle2Scene = new ScrollMagic.Scene({
		triggerElement: '.animated-article',
		triggerHook: 0.85, // show, when scrolled 70% into view
		duration: "115%", // hide 10% before exiting view (80% + 10% from bottom)
		offset: 0 // move trigger to center of element
	})
	.setTween(tweenArticle) // add class to reveal
	//.addIndicators({name: "animated-article" }) // add indicators (requires plugin) for debbug
	.addTo(controller);
	
});
