//======== Création des modals ========//
const modalContainer = document.getElementById("goinstore_modals");

const modalRdv = `
<!--=========================== POPIN APPOINTED ========================-->
<div class="reveal-modal opened" data-reveal="" id="popin-live-shopping-rdv" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">Fermer</a>
	<div class="modal-header">
		<p class="brand"></p>
		<p class="sectors"></p>
	</div>
	<div class="shopping-modal">
		<div>
				<div id="ps-closed">
						<h3>Votre conseiller sur rendez-vous</h3>
						<p>Échangez en live vidéo sur le créneau de votre choix</p>
				</div>
		</div>
	</div>
	<div class="modal-content" id="apwidget-container"></div>
</div>`;

modalContainer.insertAdjacentHTML("afterbegin", modalRdv);
//==============================//


//======== GO IN STORE WIDGET ======//
var GL_GIS = GL_GIS || {};
GL_GIS.LiveShopping = (function () {
	'use strict';
	function createWidget() {
		var existingAPScript = document.getElementById('apwidget');
		if (!existingAPScript) {
			var apContainer = document.getElementById("apwidget-container");
			
			var appointedIframe = document.createElement('div');
			appointedIframe.id = "apwidget";
			apContainer.appendChild(appointedIframe);	
			
			var appointedScript = document.createElement('script'); 
			appointedScript.id = "apwidget-script";
			appointedScript.src = "https://retailwidgets.appointedd.com/widget/"+ appointedWidgetId +"/widget.js";
			appointedScript.async = true;
			apContainer.appendChild(appointedScript);
			appointedScript.onload = function() {
				console.log("appointeddWidget is loaded");
				if (typeof appointeddWidget === 'undefined') {
					console.log("appointeddWidget is not ready");
					setTimeout(function() {appointeddWidget.render('apwidget');console.log("appointeddWidget is late")}, 700);
				} else {
					appointeddWidget.render('apwidget');
				}
			}
		}		
	}
	
	function createAppointedWidget() {
		createWidget();
	}


	
	function displaySadBrand() {
		var displayBrand = document.querySelectorAll('.brand');
		var displaySectors = document.querySelectorAll('.sectors');
		
		if (typeof clickedBrand !== 'undefined' && clickedBrand != '') {			
			for (var e=0; e < displayBrand.length; e++) {
				displayBrand[e].innerHTML = clickedBrand;
			}
		}
		
		if (typeof cleanBrandName !== 'undefined' && cleanBrandName != '') {
			var bgImageUrl = "url('https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/popin/"+cleanBrandName+".jpg')";
			//console.log(bgImageUrl);
			document.querySelector('#popin-live-shopping-rdv .modal-header').style.backgroundImage = bgImageUrl;
		}
	}
	
	return {
		displaySadBrand: displaySadBrand,
		createWidget: createWidget,
		createAppointedWidget: createAppointedWidget
	};
}
());
	

document.addEventListener("DOMContentLoaded", function(event) {
	
	document.querySelector(".ami-de-coeur").classList.add('has-js');
	
	var readMore = document.querySelectorAll('.read-more');
	for (var i=0; i < readMore.length; i++) {
		var readMoreTrigger = readMore[i].querySelector('.read-more-toggle');
		readMoreTrigger.addEventListener("click", function(e) {
			e.preventDefault();
			this.nextElementSibling.classList.toggle('hidden');
		});
		
		var readLessTrigger = readMore[i].querySelector('.read-less-toggle');
		readLessTrigger.addEventListener("click", function(e) {
			e.preventDefault();
			this.parentNode.classList.toggle('hidden');
		});
	}	
	
	//======== Gestion du formulaire ========//
	$(document).on('close', '[data-reveal]', function () {
		var modal = $(this);
		var apContainer = document.getElementById("apwidget-container");
		apContainer.innerHTML = "";
	});
	//======================================//
	
	
	
	//======================================= Scroll Magic ======================================//
	//https://github.com/janpaepke/ScrollMagic/wiki/Getting-Started-:-How-to-use-ScrollMagic
	// init controller
	var controller = new ScrollMagic.Controller();
	
	// create a scene - effet de parallax sur article 2
	// build tween
	var tweenArticle = TweenMax.to(".animated-article .article-image", 1, {bottom: 80});
	// build scene
	var tweenArticle2Scene = new ScrollMagic.Scene({
		triggerElement: '.animated-article',
		triggerHook: 0.85, // show, when scrolled 70% into view
		duration: "115%", // hide 10% before exiting view (80% + 10% from bottom)
		offset: 0 // move trigger to center of element
	})
	.setTween(tweenArticle) // add class to reveal
	//.addIndicators({name: "animated-article" }) // add indicators (requires plugin) for debbug
	.addTo(controller);
	
});
