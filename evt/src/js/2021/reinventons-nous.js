document.addEventListener("DOMContentLoaded", function(event) {
	
	var url = location.href;
	var mainDiv = document.querySelector(".reinventons-nous");
	var manifesto = document.querySelector('.manifesto');
	var logoAnim = document.querySelector('.logo-anim');
	var pageFooter = document.querySelector('footer.footer');
	var splash = document.querySelector('.reinventons-splash');
	var programme = document.getElementById('programme');
	
	var splashVid = document.getElementById('splash-vid');
	var splashVidSrc = splashVid.getAttribute('src');
	
	var manifestoMobileVideo = document.getElementById('manifesto-mobile-vid');
	
	mainDiv.classList.add('has-js');
	
	//============================================ Gestion des ancres ===========================================//
	var anchorHash = location.hash.substr(1);	
	
	function cleanUrls() {
		var url = location.href;
		var cleanUrl = url.substring(0, url.indexOf("#"));
		window.history.pushState({}, document.title, cleanUrl);
	}
	

	//============================================ Menu Programme ===========================================//	
	
	var menuLinks = document.querySelectorAll('.main-links a');
	for (var i=0; i < menuLinks.length; i++) {
		menuLinks[i].addEventListener("click", function(e) {
			e.preventDefault();
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);
			if (target) {
				programme.classList.add('hidden-section');
				target.classList.remove('hidden-section');
				
				if (anchorHash) {
					var newUrl = url.substring(0, url.indexOf("#")) + targetId;
				} else {
					var newUrl = url + targetId;
				}
				window.history.pushState({}, document.title, newUrl);
			}
		});
	}
	//======================================================================================================/
	
	// bouton démarrer l'expérience du splash screen
	var videoTriggerTuto = document.querySelector('.trigger-tuto-hover-video');
	var tuto = document.querySelector('.tuto');
	var manifestoTopNav = manifesto.querySelector('.reinventons-nav');
	
	videoTriggerTuto.addEventListener("click", function(e) {
		tuto.classList.add('active');
		mainDiv.classList.add('is-fixed-top');
		pageFooter.classList.add('is-hidden');
		splash.classList.add('hidden-splash');
		logoAnim.classList.add('static');
	});
	
	
	// bouton retour gauche du tuto vers splash screen
	var backSplash = document.querySelector('.tuto-nav .back');
	backSplash.addEventListener("click", function(e) {
		tuto.classList.remove('active');
		mainDiv.classList.remove('is-fixed-top');
		pageFooter.classList.remove('is-hidden');
		setTimeout(function(){
			splash.classList.remove('hidden-splash');
			logoAnim.classList.remove('static');
		}, 500);
	});
	
	// bouton droit du tuto déclenche anim et manifesto, masque le footer de page pour éviter le scroll vertical et stop la vidéo du splash
	var startExpe = document.querySelectorAll('.tuto .start');
	for (var i=0; i < startExpe.length; i++) {
		startExpe[i].addEventListener("click", function(e) {
			logoAnim.classList.remove('static');
			logoAnim.classList.add('active', 'animate');
			splashVid.removeAttribute('src');
			tuto.classList.remove('active');
			
			setTimeout(function(){ 
				manifesto.classList.remove('hidden-section');
				manifestoTopNav.classList.add('active');
			}, 800);
				
			setTimeout(function(){
				logoAnim.classList.remove('animate');
			}, 1000);
			
			if (anchorHash) {
				var manifestoUrl = url.substring(0, url.indexOf("#")) + "#manifesto";
			} else {
				var manifestoUrl = url + "#manifesto";
			}
			window.history.pushState({}, document.title, manifestoUrl);
			
		});
	}
	
	// nav pages 
	var topMenu = document.querySelector('.reinventons-top-menu');
	var myTabsParent = topMenu.querySelector('.tabs-container');
	var tabUl = myTabsParent.querySelector('.tabs > ul');
	var tabMode = tabUl.querySelector('li > a.mode');
	var tabShoes = tabUl.querySelector('li > a.shoes');
	var tabResponsable = tabUl.querySelector('li > a.responsable');
	var tabActumag = tabUl.querySelector('li > a.actumag');
	var paneMode = myTabsParent.querySelector('.tab-pane#pane-mode');
	var paneShoes = myTabsParent.querySelector('.tab-pane#pane-shoes');
	var paneResponsable = myTabsParent.querySelector('.tab-pane#pane-responsable');
	var paneActumag = myTabsParent.querySelector('.tab-pane#pane-actumag');
	
	var topNav = document.querySelectorAll('.reinventons-nav');
	for (var i=0; i < topNav.length; i++) {
		// bouton menu sur chaque page 
		var topNavMenu = topNav[i].querySelector('.trigger-nav-menu');
		topNavMenu.addEventListener("click", function(e) {
			window.screenThatOpenedMenu = this.parentNode.parentNode;
			if (this.parentNode.parentNode.classList.contains('logo-anim')) {
				
			} else {
				screenThatOpenedMenu.classList.add('hidden-section');
			}
			
			// supprime le .is-active qui est appliqué par défaut  
			var menuTabs = myTabsParent.querySelectorAll(".tabs > ul > li > a");
			for (var i = 0; i < menuTabs.length; i++) {
				menuTabs[i].classList.remove("is-active");
				menuTabs[i].setAttribute("aria-selected", "false");
				menuTabs[i].setAttribute("tabindex", "-1");
			}
			// supprime .is-active sur le panneau par défaut à afficher
			var contentPanes = myTabsParent.querySelectorAll(".tab-pane");
			for (i = 0; i < contentPanes.length; i++) {
				contentPanes[i].classList.remove("is-active");
				contentPanes[i].setAttribute("hidden", "");
			}
			
			// ouvre le menu sur le bon tab et le bon pane en fonction de ou on se trouve au moment du clic
			if (this.parentNode.parentNode.classList.contains('reinventons-shoes')) {
				topMenu.classList.add('has-background-collectionneuses', 'active');
				tabShoes.classList.add('is-active');
				tabShoes.setAttribute("aria-selected", "true");
				tabShoes.setAttribute("tabindex", "0");
				tabUl.scroll({
					top: 0,
					left: 50,
					behavior: 'smooth'
				});
				paneShoes.classList.add("is-active");
				paneShoes.removeAttribute("hidden");
			} else if (this.parentNode.parentNode.classList.contains('reinventons-responsable')) {
				topMenu.classList.add('has-background-visionnaires', 'active');
				tabResponsable.classList.add('is-active');
				tabResponsable.setAttribute("aria-selected", "true");
				tabResponsable.setAttribute("tabindex", "0");
				tabUl.scroll({
					top: 0,
					left: 120,
					behavior: 'smooth'
				});
				paneResponsable.classList.add("is-active");
				paneResponsable.removeAttribute("hidden");
			} else if (this.parentNode.parentNode.classList.contains('reinventons-actumag')) {
				topMenu.classList.add('has-background-white', 'active');
				tabActumag.classList.add('is-active');
				tabActumag.setAttribute("aria-selected", "true");
				tabActumag.setAttribute("tabindex", "0");
				tabUl.scroll({
					top: 0,
					left: 600,
					behavior: 'smooth'
				});
				paneActumag.classList.add("is-active");
				paneActumag.removeAttribute("hidden");
			} else if (this.parentNode.parentNode.classList.contains('logo-anim')) {
				topMenu.classList.add('has-background-affranchies', 'active');
				tabMode.classList.add('is-active');
				tabMode.setAttribute("aria-selected", "true");
				tabMode.setAttribute("tabindex", "0");
				paneMode.classList.add("is-active");
				paneMode.removeAttribute("hidden");
				logoAnim.classList.remove('static');
			} else {
				topMenu.classList.add('has-background-affranchies', 'active');
				tabMode.classList.add('is-active');
				tabMode.setAttribute("aria-selected", "true");
				tabMode.setAttribute("tabindex", "0");
				paneMode.classList.add("is-active");
				paneMode.removeAttribute("hidden");
				logoAnim.classList.add('is-hidden-top');

				if (this.parentNode.parentNode.classList.contains('manifesto-vid')) {
					manifestoMobileVideo.pause();
				}
			}
			
			if (typeof bLazy != 'undefined') { 
				bLazy.revalidate();
			}
		});
		
		//bouton fermer retour au splash screen 
		var topNavClose = topNav[i].querySelector('.close');
		topNavClose.addEventListener("click", function(e) {
			logoAnim.classList.remove('static', 'active', 'is-hidden-top');
			pageFooter.classList.remove('is-hidden');
			
			splashVid.setAttribute('src', splashVidSrc);
			
			var sections = document.querySelectorAll('.section');
			for (var i=0; i < sections.length; i++) {
				sections[i].classList.add('hidden-section');
			}
			setTimeout(function(){ splash.classList.remove('hidden-splash'); }, 100);		
			setTimeout(function(){ mainDiv.classList.remove('is-fixed-top'); }, 300);
			
			if (this.parentNode.parentNode.classList.contains('logo-anim')) {
				this.parentNode.classList.remove('active');
			}
			
			if (this.parentNode.parentNode.classList.contains('manifesto-vid')) {
				manifestoMobileVideo.pause();
				manifestoMobileVideo.currentTime = 0;
			}
			
			if (this.parentNode.parentNode.classList.contains('video-section')) {
				this.parentNode.parentNode.querySelector('.video-container iframe').removeAttribute('src');
			}
			
			cleanUrls();
		});
	}
	
	
	// bouton précédent/suivant pour avancer ou reculer dans les pages
	var prevNextButtons = document.querySelectorAll('.prev-next-nav');
	for (var i=0; i < prevNextButtons.length; i++) {
		var prevButton = prevNextButtons[i].querySelector('.prev');		
		prevButton.addEventListener("click", function(e) {
			e.preventDefault();
			var previousElement = this.parentNode.parentNode.previousElementSibling;
			if (typeof bLazy != 'undefined') { 
				bLazy.revalidate();
			}
			if (this.classList.contains('close')) {
				splashVid.setAttribute('src', splashVidSrc);
				manifestoTopNav.classList.remove('active');
				logoAnim.classList.remove('active');
				pageFooter.classList.remove('is-hidden');
				this.parentNode.parentNode.classList.add('hidden-section');
				setTimeout(function(){ splash.classList.remove('hidden-splash'); }, 100);
				setTimeout(function(){ mainDiv.classList.remove('is-fixed-top'); }, 300);
				cleanUrls();
			} else {
				this.parentNode.parentNode.classList.add('hidden-section');
				previousElement.classList.remove('hidden-section');
				
				// modifie l'url en ajoutant les id's
				var previousElementId = previousElement.getAttribute("id");
				if (anchorHash) {
					var prevUrl = url.substring(0, url.indexOf("#")) + "#" + previousElementId;
				} else {
					var prevUrl = url + "#" + previousElementId;
				}
				window.history.pushState({}, document.title, prevUrl);
			}
			
			if (this.parentNode.parentNode.classList.contains('manifesto-vid')) {
				logoAnim.classList.remove('is-hidden-top');
				manifestoMobileVideo.pause();
			}
			
			if (this.parentNode.parentNode.classList.contains('programme')) {
				manifestoMobileVideo.play();
			}
			
			// ajoute/supprime le src des vidéos pour les arrêter quand elles ne sont pas visibles en mobile
			if (previousElement.classList.contains('video-section')) {
				var videoIframe = previousElement.querySelector('.video-container iframe');
				var videoIframeSrc = videoIframe.dataset.vidsrc;
				videoIframe.setAttribute('src', videoIframeSrc);
			}
			if (this.parentNode.parentNode.classList.contains('video-section')) {
				this.parentNode.parentNode.querySelector('.video-container iframe').removeAttribute('src');
			}			
		});
		
		var nextButton = prevNextButtons[i].querySelector('.next');
		nextButton.addEventListener("click", function(e) {
			e.preventDefault();
			var nextElement = this.parentNode.parentNode.nextElementSibling;
			if (typeof bLazy != 'undefined') { 
				bLazy.revalidate();
			}
			this.parentNode.parentNode.classList.add('hidden-section');
			nextElement.classList.remove('hidden-section');
			
			if (this.parentNode.parentNode.classList.contains('manifesto')) {
				logoAnim.classList.add('is-hidden-top');
				manifestoMobileVideo.play();
			}
			
			if (this.parentNode.parentNode.classList.contains('manifesto-vid')) {
				manifestoMobileVideo.pause();
			}
						
			// ajoute/supprime le src des vidéos pour les arrêter quand elles ne sont pas visibles en mobile
			if (nextElement.classList.contains('video-section')) {
				var videoIframe = nextElement.querySelector('.video-container iframe');
				var videoIframeSrc = videoIframe.dataset.vidsrc;				
				videoIframe.setAttribute('src', videoIframeSrc);
			}
			if (this.parentNode.parentNode.classList.contains('video-section')) {
				this.parentNode.parentNode.querySelector('.video-container iframe').removeAttribute('src');
			}
			
			// modifie l'url en ajoutant les id's
			var nextElementId = nextElement.getAttribute("id");
			if (anchorHash) {
				var nextUrl = url.substring(0, url.indexOf("#")) + "#" + nextElementId;
			} else {
				var nextUrl = url + "#" + nextElementId;
			}
			window.history.pushState({}, document.title, nextUrl);
		});
	}
	
	// bouton redémarrer à la fin du parcours
	var reStartExpe = document.querySelector('#restart .restart-button');
	reStartExpe.addEventListener("click", function(e) {
		pageFooter.classList.remove('is-hidden');
		logoAnim.classList.remove('static', 'active', 'is-hidden-top');
		
		splashVid.setAttribute('src', splashVidSrc);
		
		var sections = document.querySelectorAll('.section');
		for (var i=0; i < sections.length; i++) {
			sections[i].classList.add('hidden-section');
		}
		setTimeout(function(){ splash.classList.remove('hidden-splash'); }, 100);		
		setTimeout(function(){ mainDiv.classList.remove('is-fixed-top'); }, 300);
		
		cleanUrls();
	});
	
	//============================================ Top Menu ===========================================//	
	//bouton pour fermer le menu overlay
	var closeMenu = topMenu.querySelector('.close');
	closeMenu.addEventListener("click", function(e) {
		topMenu.classList.remove('active', 'has-background-affranchies', 'has-background-collectionneuses', 'has-background-visionnaires', 'has-background-white');
		screenThatOpenedMenu.classList.remove('hidden-section');
		
		if (screenThatOpenedMenu.classList.contains('logo-anim')) {
			logoAnim.classList.add('static');
		}
		if (screenThatOpenedMenu.classList.contains('manifesto')) {
			logoAnim.classList.remove('is-hidden-top');
		}
		if (screenThatOpenedMenu.classList.contains('manifesto-vid')) {
			manifestoMobileVideo.play();
		}
	});
	
	// boutons/onglets dans le menu
	var topMenuLinks = tabUl.querySelectorAll('a');
	for (var i=0; i < topMenuLinks.length; i++) {
		topMenuLinks[i].addEventListener("click", function(e) {
			e.preventDefault();
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);
			if (target) {
				// change le bg-color du menu
				if (targetId === "#pane-mode") {
					topMenu.classList.remove('has-background-collectionneuses', 'has-background-visionnaires', 'has-background-white');
					topMenu.classList.add('has-background-affranchies');
				} else if (targetId === "#pane-shoes") {
					topMenu.classList.remove('has-background-affranchies', 'has-background-visionnaires', 'has-background-white');
					topMenu.classList.add('has-background-collectionneuses');
				} else if (targetId === "#pane-responsable") {
					topMenu.classList.remove('has-background-collectionneuses', 'has-background-affranchies', 'has-background-white');
					topMenu.classList.add('has-background-visionnaires');
				} else if (targetId === "#pane-actumag") {
					topMenu.classList.remove('has-background-collectionneuses', 'has-background-visionnaires', 'has-background-affranchies');
					topMenu.classList.add('has-background-white');
				}
			}
		});
	}
	
	// grille de pages dans le menu
	var contentPaneLinks = myTabsParent.querySelectorAll(".tab-pane a");
	for (i = 0; i < contentPaneLinks.length; i++) {
		contentPaneLinks[i].addEventListener("click", function(e) {
			e.preventDefault();
			topMenu.classList.remove('active', 'has-background-affranchies', 'has-background-collectionneuses', 'has-background-visionnaires', 'has-background-white');
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);			
			
			if (target) {
				target.classList.remove('hidden-section');
				
				// ajoute le src des vidéos pour les lancer quand on clique sur les vignettes du menu
				if (this.classList.contains('card-has-video')) {
					var targetVideoIframe = target.querySelector('.video-container iframe');
					var targetVideoIframeSrc = targetVideoIframe.dataset.vidsrc;
					targetVideoIframe.setAttribute('src', targetVideoIframeSrc);
				}
				
				if (anchorHash) {
					var newUrl = url.substring(0, url.indexOf("#")) + targetId;
				} else {
					var newUrl = url + targetId;
				}
				window.history.pushState({}, document.title, newUrl);
			}
		});
	}
	//========================================================================================================//	
	
	
	
	
	
	//======================================= Gestion du responsive ======================================//
	
	//==== Desktop
	if("matchMedia" in window) {
		var mqDesktop = window.matchMedia("(min-width:1025px)");
		mqDesktop.addListener(WidthChange);
		WidthChange(mqDesktop);
	}
	
	function WidthChange(mqDesktop) {
		if (mqDesktop.matches) {
			tuto.classList.remove('active');
			mainDiv.classList.remove('is-fixed-top');
			splash.classList.remove('hidden-splash');
			logoAnim.classList.remove('active');
			
			var sections = document.querySelectorAll('.section');
			for (var i=0; i < sections.length; i++) {
				sections[i].classList.add('hidden-section');
			}
			
			//==== ScrollMagic
			//https://github.com/janpaepke/ScrollMagic/wiki/Getting-Started-:-How-to-use-ScrollMagic
			var mainMode = document.getElementById('mode-main');
			var mainShoes = document.getElementById('shoes-main');
			var mainResponsable = document.getElementById('responsable-main');
			var mainActu = document.getElementById('actumag-main');
			var theEnd = document.getElementById('restart');
			
			//gestion des cartes main en pin
			var mainCardHeight = window.innerHeight - 100;
			
			var mainModeSpaceBefore = mainMode.getBoundingClientRect().top;
			var mainShoesSpaceBefore = mainShoes.getBoundingClientRect().top;
			var mainResponsableSpaceBefore = mainResponsable.getBoundingClientRect().top;
			var mainActuSpaceBefore = mainActu.getBoundingClientRect().top;
			var theEndSpaceBefore = theEnd.getBoundingClientRect().top;
			
			window.addEventListener('resize', function(event) {
				mainCardHeight = window.innerHeight - 100;
				mainModeSpaceBefore = mainMode.getBoundingClientRect().top;
				mainShoesSpaceBefore = mainShoes.getBoundingClientRect().top;
				mainResponsableSpaceBefore = mainResponsable.getBoundingClientRect().top;
				mainActuSpaceBefore = mainActu.getBoundingClientRect().top;
				theEndSpaceBefore = theEnd.getBoundingClientRect().top;
			});
			
			function mainModeCallback() {
				var mainModeDuration = mainShoesSpaceBefore - mainModeSpaceBefore - mainCardHeight;
				return mainModeDuration;
			}
			function mainShoesCallback() {
				var mainShoesDuration = mainResponsableSpaceBefore - mainShoesSpaceBefore - mainCardHeight;
				return mainShoesDuration;
			}
			function mainResponsableCallback() {
				var mainResponsableDuration = mainActuSpaceBefore - mainResponsableSpaceBefore - mainCardHeight;
				return mainResponsableDuration;
			}
			function mainActuCallback() {
				var mainActuDuration = theEndSpaceBefore - mainActuSpaceBefore - mainCardHeight;
				return mainActuDuration;
			}
			
			
			controller = new ScrollMagic.Controller();
			
			var scenePinMode = new ScrollMagic.Scene({
				triggerElement: "#mode-main",
				triggerHook: 0,
				duration: mainModeCallback,
				offset: -100,
				pushFollowers: false
			})
			.setPin("#mode-main .container")
			//.addIndicators({name: "mode-main"}) // add indicators (requires plugin)
			.addTo(controller);
			
			var scenePinShoes = new ScrollMagic.Scene({
				triggerElement: "#shoes-main",
				triggerHook: 0,
				duration: mainShoesCallback,
				offset: -100,
				pushFollowers: false
			})
			.setPin("#shoes-main .container")
			//.addIndicators({name: "shoes-main"}) // add indicators (requires plugin)
			.addTo(controller);
			
			var scenePinResponsable = new ScrollMagic.Scene({
				triggerElement: "#responsable-main",
				triggerHook: 0,
				duration: mainResponsableCallback,
				offset: -100,
				pushFollowers: false
			})
			.setPin("#responsable-main .container")
			//.addIndicators({name: "responsable-main"}) // add indicators (requires plugin)
			.addTo(controller);
			
			var scenePinActu = new ScrollMagic.Scene({
				triggerElement: "#actumag-main",
				triggerHook: 0,
				duration: mainActuCallback,
				offset: -100,
				pushFollowers: false
			})
			.setPin("#actumag-main .container")
			//.addIndicators({name: "actumag-main"}) // add indicators (requires plugin)
			.addTo(controller);

			window.addEventListener('resize', function(event){
				scenePinMode.refresh();
				scenePinShoes.refresh();
				scenePinResponsable.refresh();
				scenePinActu.refresh();
			});
			
			//============================================= Gestion du scrollsy ====================================//
			var anchorHash = location.hash.substr(1);
			
			//https://github.com/cferdinandi/gumshoe
			var spy = new Gumshoe('#go-down-nav a', {
				navClass: 'next-link-is-active', // applied to the nav list item
				contentClass: 'is-active', // s'applique au contenu
				offset: 100, // pour prendre en compte la hauteur du header sticky
				reflow: true // if true, listen for reflows
			});
			
			// Listen for activate events
			document.addEventListener('gumshoeActivate', function (event) {
				// The link
				var link = event.detail.link;
				// The content
				var content = event.detail.content;
				
				var targetId = link.getAttribute('href');
				if (content) {
					if (anchorHash) {
						var newUrl = url.substring(0, url.indexOf("#")) + targetId;
					} else {
						var newUrl = url + targetId;
					}
					window.history.pushState({}, document.title, newUrl);
				}
			}, false);
			
			
			var desktopAnchorLinks = document.querySelectorAll("#go-down-nav a");
			for (i = 0; i < desktopAnchorLinks.length; i++) {
				desktopAnchorLinks[i].addEventListener("click", function(e) {
					e.preventDefault();
					var targetId = this.getAttribute('href');
					var target = document.querySelector(targetId);
					
					if (target) {
						if (anchorHash) {
							var newUrl = url.substring(0, url.indexOf("#")) + targetId;
						} else {
							var newUrl = url + targetId;
						}
						window.history.pushState({}, document.title, newUrl);
					}
				});
			}
			//======================================================================================================//
			
			
			
			
			//============================================ Smooth scroll ===========================================//
			var smoothAnchor = document.querySelectorAll('.has-smoothscroll');
			for (var i=0; i < smoothAnchor.length; i++) {
				smoothAnchor[i].addEventListener("click", function(e) {
					e.preventDefault();
					var targetId = this.getAttribute('href');
					var target = document.querySelector(targetId);
					if (target) {
						var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 100;
						scroll({
							top: offsetTop,
							behavior: "smooth"
						});
					}
				});
			}
			//======================================================================================================//
		}
		
		//==== Mobile
		else {
			if (typeof controller !== 'undefined') {
				controller.destroy(true);
				controller = null;
			}
			scroll({
				top: 0,
				behavior: "smooth"
			});
			
			var manageMobileVideoSound = document.querySelector('#manifesto-vid .manage-sound');
			manageMobileVideoSound.addEventListener('click', function(e) {
				this.classList.toggle('is-muted');
				manifestoMobileVideo.muted = !manifestoMobileVideo.muted;
				manifestoMobileVideo.setAttribute('data-state', manifestoMobileVideo.muted ? 'unmute' : 'mute');
			});
			
			// gestion des ancres au chargement sur mobile
			var anchorHash = location.hash.substr(1);
			if (anchorHash) {
				if (anchorHash === "intro") {
				
				} else {
					if (anchorHash === "manifesto") {
						splashVid.removeAttribute('src');
						logoAnim.classList.add('is-fixed-top', 'active');
					} else {
						logoAnim.classList.add('is-hidden-top', 'is-fixed-top', 'active');
					}
					if (anchorHash === "manifesto-vid") {
						manifestoMobileVideo.play();
					}
					mainDiv.classList.add('is-fixed-top');
					pageFooter.classList.add('is-hidden');
					splash.classList.add('hidden-splash');
					manifestoTopNav.classList.add('active');
					var anchor = document.getElementById(anchorHash);
					setTimeout(function(){
						anchor.classList.remove("hidden-section");
					}, 100);					
				}				
			}		
		}
	}
	//======================================================================================================//
	
	
	
	
	
});
