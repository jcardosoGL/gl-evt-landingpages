document.addEventListener("DOMContentLoaded", function(event) {

	document.querySelector(".lp-container").classList.add('has-js');
		
	//============================================ Smooth scroll ===========================================//
	var smoothAnchor = document.querySelectorAll('.has-smoothscroll');
	for (var i=0; i < smoothAnchor.length; i++) {
		smoothAnchor[i].addEventListener("click", function(e) {
			e.preventDefault();
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);
			if (target) {
				if (window.matchMedia("(min-width:1024px)").matches) {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 100;
				} else {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset;
				}
				scroll({
					top: offsetTop,
					behavior: "smooth"
				});
			}
		});
	}
	//======================================================================================================//
	
	
	//============================================ Slider ===========================================//
	var elemSliderIncontournables = document.querySelector(".incontournables .slider-incontournables");
	var sliderIncontournables = new Flickity(elemSliderIncontournables, {
		// options
		//initialIndex: 1,
		cellAlign: "left",
		contain: true, // default :true place la carousel sur la gauche de la page
		percentPosition: false,
		prevNextButtons: false,
		autoPlay: false,
		pageDots: false,
		setGallerySize: false, //pour que la hauteur ne soit pas définie par flickity si on définis déjà la hauteur en css
		watchCSS: true,
		dragThreshold: 15
	});
	// sliderIncontournables.on("scroll", function(progress) {
	// 	bLazy.revalidate();
	// });
	
	var elemSliderInnovations = document.querySelector(".innovations .slider-incontournables");
	var sliderInnovations = new Flickity(elemSliderInnovations, {
		// options
		//initialIndex: 1,
		cellAlign: "left",
		contain: true, // default :true place la carousel sur la gauche de la page
		percentPosition: false,
		prevNextButtons: false,
		autoPlay: false,
		pageDots: false,
		setGallerySize: false, //pour que la hauteur ne soit pas définie par flickity si on définis déjà la hauteur en css
		watchCSS: true,
		dragThreshold: 15
	});
	//======================================================================================================//
	
	
	var blocExclus = document.querySelector(".exclusivite");
	setInterval(function(){ 
		if (blocExclus.classList.contains("vert")) {
			blocExclus.classList.remove("vert");
			blocExclus.classList.add("brun");
		} else if (blocExclus.classList.contains("brun")) {
			blocExclus.classList.remove("brun");
			blocExclus.classList.add("rouge");
		} else if (blocExclus.classList.contains("rouge")) {
			blocExclus.classList.remove("rouge");
			blocExclus.classList.add("vert");
		}
	}, 5000);
	
	var blocPopup = document.querySelector(".haussmann");
	setInterval(function(){ 
		if (blocPopup.classList.contains("visu-1")) {
			blocPopup.classList.remove("visu-1");
			blocPopup.classList.add("visu-2");
		} else if (blocPopup.classList.contains("visu-2")) {
			blocPopup.classList.remove("visu-2");
			blocPopup.classList.add("visu-3");
		} else if (blocPopup.classList.contains("visu-3")) {
			blocPopup.classList.remove("visu-3");
			blocPopup.classList.add("visu-1");
		}
	}, 5000);
});
