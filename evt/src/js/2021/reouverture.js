document.addEventListener("DOMContentLoaded", function(event) {

	document.querySelector(".reouverture").classList.add('has-js');
		
	//============================================ Smooth scroll ===========================================//
	var smoothAnchor = document.querySelectorAll('.has-smoothscroll');
	for (var i=0; i < smoothAnchor.length; i++) {
		smoothAnchor[i].addEventListener("click", function(e) {
			e.preventDefault();
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);
			if (target) {
				if (window.matchMedia("(min-width:1024px)").matches) {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 60;
				} else {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset;
				}
				scroll({
					top: offsetTop,
					behavior: "smooth"
				});
			}
		});
	}
	//======================================================================================================//

	
	
	//============================================ Slider ===========================================//
	var elemSliderFondation = document.querySelector(".slider-services");
	var sliderFondation = new Flickity(elemSliderFondation, {
		// options
		//initialIndex: 1,
		cellAlign: "center",
		contain: true, // default :true place la carousel sur la gauche de la page
		percentPosition: false,
		prevNextButtons: false,
		autoPlay: false,
		pageDots: false,
		setGallerySize: false, //pour que la hauteur ne soit pas définie par flickity si on définis déjà la hauteur en css
		watchCSS: true,
		dragThreshold: 15
	});
	sliderFondation.on("scroll", function(progress) {
		bLazy.revalidate();
	});

	//======================================================================================================//


	//============== Stop la vidéo  ====================//
	var iframeD = document.getElementById("video-d");
	var iframeDSrc = iframeD.src;
	var iframeM = document.getElementById("video-m");
	var iframeMSrc = iframeM.src;
	
	$(document).on('open', '[data-reveal]', function () {
		//var modal = $(this);
		if (window.matchMedia("(min-width:768px)").matches) {
			console.log("tablet");
			if ( iframeD ) {
				iframeD.src = iframeDSrc + "?autoplay=1";
			}
		} else {
			console.log("mobile");
			if ( iframeM ) {
				iframeM.src = iframeMSrc + "?autoplay=1";
			}
		}
	});
	
	$(document).on('close', '[data-reveal]', function () {
		//var modal = $(this);
		if (window.matchMedia("(min-width:768px)").matches) {
			if ( iframeD ) {
				iframeD.src = iframeDSrc;
			}
		} else {
			if ( iframeM ) {
				iframeM.src = iframeMSrc;
			}
		}
		
	});
	
	//==================================================//

});
