document.addEventListener("DOMContentLoaded", function(event) {
	
	document.querySelector(".lp-container").classList.add('has-js');
	
	document.querySelector("#collection .bags").classList.add('animate');
	
	var bottegaLogo = document.querySelectorAll(".lp-hero .bottega-logo");
	for (var i=0; i < bottegaLogo.length; i++) {
		bottegaLogo[i].classList.add("visible");
	}
	document.querySelector(".lp-hero .content.main").classList.add("visible");
	
	var readMore = document.querySelectorAll('.read-more');
	for (var i=0; i < readMore.length; i++) {
		var readMoreTrigger = readMore[i].querySelector('.read-more-toggle');
		readMoreTrigger.addEventListener("click", function(e) {
			e.preventDefault();
			this.nextElementSibling.classList.toggle('hidden');
		});
		
		var readLessTrigger = readMore[i].querySelector('.read-less-toggle');
		readLessTrigger.addEventListener("click", function(e) {
			e.preventDefault();
			this.parentNode.classList.toggle('hidden');
		});
	}		
	
	//============================================ Smooth scroll ===========================================//
	var smoothAnchor = document.querySelectorAll('.has-smoothscroll');
	for (var i=0; i < smoothAnchor.length; i++) {
		smoothAnchor[i].addEventListener("click", function(e) {
			e.preventDefault();
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);
			if (target) {
				if (window.matchMedia("(min-width:1024px)").matches) {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 60;
				} else {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset;
				}
				scroll({
					top: offsetTop,
					behavior: "smooth"
				});
			}
		});
	}
	//======================================================================================================//
	
	//======================================= Scroll Magic ======================================//
	
	
	//==== Tablet
	if("matchMedia" in window) {
		var mqTablet = window.matchMedia("(min-width:768px)");
		mqTablet.addListener(WidthChange);
		WidthChange(mqTablet);
	}
	
	function WidthChange(mqTablet) {
		if (mqTablet.matches) {
			
			controllerTablet = new ScrollMagic.Controller();
			
			var imageLeft1 = document.querySelector('.animated-article .article-image.left-1');
			var imageLeft2 = document.querySelector('.animated-article .article-image.left-2');
			var imageRight1 = document.querySelector('.animated-article .article-image.right-1');
			var imageRight2 = document.querySelector('.animated-article .article-image.right-2');
			
			var imageHeight = imageLeft1.offsetHeight;
			
			window.addEventListener('resize', function(event) {
				imageHeight = imageLeft1.offsetHeight;
			});
			function imageLeftCallback() {
				var imageLeftDuration = imageHeight * 2 + 250;
				return imageLeftDuration;
			}
			function imageRightCallback() {
				var imageRightDuration = imageHeight * 2 + 550;
				return imageRightDuration;
			}
			
			// build tween
			var tweenImageLeft1 = TweenMax.fromTo(imageLeft1, 1, {y: 250}, {y: -250});
			// build scene
			var sceneImageLeft1 = new ScrollMagic.Scene({
				triggerElement: '.animated-article',
				triggerHook: 1, // show, when scrolled 70% into view
				duration: imageLeftCallback, // hide 10% before exiting view (80% + 10% from bottom)
				offset: 0 // move trigger to center of element
			})
			.setTween(tweenImageLeft1) // add class to reveal
			//.addIndicators({name: "image left 1" }) // add indicators (requires plugin) for debbug
			.addTo(controllerTablet);
			
			// build tween
			var tweenImageLeft2 = TweenMax.fromTo(imageLeft2, 1, {y: 250}, {y: -250});
			// build scene
			var sceneImageLeft2 = new ScrollMagic.Scene({
				triggerElement: '.animated-article',
				triggerHook: 1, // show, when scrolled 70% into view
				duration: imageLeftCallback, // hide 10% before exiting view (80% + 10% from bottom)
				offset: imageHeight // move trigger to center of element
			})
			.setTween(tweenImageLeft2) // add class to reveal
			//.addIndicators({name: "image left 2" }) // add indicators (requires plugin) for debbug
			.addTo(controllerTablet);
			
			
			// build tween
			var tweenImageRight1 = TweenMax.fromTo(imageRight1, 1, {y: -150}, {y: 150});
			// build scene
			var sceneImageRight1 = new ScrollMagic.Scene({
				triggerElement: '.animated-article',
				triggerHook: 1, // show, when scrolled 70% into view
				duration: imageRightCallback, // hide 10% before exiting view (80% + 10% from bottom)
				offset: 0 // move trigger to center of element
			})
			.setTween(tweenImageRight1) // add class to reveal
			//.addIndicators({name: "image right 1" }) // add indicators (requires plugin) for debbug
			.addTo(controllerTablet);
			
			// build tween
			var tweenImageRight2 = TweenMax.fromTo(imageRight2, 1, {y: -150}, {y: 200});
			// build scene
			var sceneImageRight2 = new ScrollMagic.Scene({
				triggerElement: '.animated-article',
				triggerHook: 1, // show, when scrolled 70% into view
				duration: imageRightCallback, // hide 10% before exiting view (80% + 10% from bottom)
				offset: imageHeight // move trigger to center of element
			})
			.setTween(tweenImageRight2) // add class to reveal
			//.addIndicators({name: "image right 2" }) // add indicators (requires plugin) for debbug
			.addTo(controllerTablet);
		}
		else {
			
			if (typeof controllerTablet !== 'undefined') {
				controllerTablet.destroy(true);
				controllerTablet = null;
			}
		}
	}
});