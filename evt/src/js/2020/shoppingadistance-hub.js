document.addEventListener("DOMContentLoaded", function(event) {

	document.querySelector(".luxe-augmente").classList.add('has-js');
		
	//============================================ Smooth scroll ===========================================//
	var smoothAnchor = document.querySelectorAll('.has-smoothscroll');
	for (var i=0; i < smoothAnchor.length; i++) {
		smoothAnchor[i].addEventListener("click", function(e) {
			e.preventDefault();
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);
			if (target) {
				if (window.matchMedia("(min-width:1024px)").matches) {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 60;
				} else {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset;
				}
				scroll({
					top: offsetTop,
					behavior: "smooth"
				});
			}
		});
	}
	//======================================================================================================//
	
	
	//======================================= Hauteur Video Mobile ======================================//
	if("matchMedia" in window) {
		var mq = window.matchMedia("(max-width:767px)");
		mq.addListener(WidthChange);
		WidthChange(mq);
	}
	
	function WidthChange(mq) {		
		if (mq.matches) {			
			window.onresize = function() {
				var largeur = window.innerWidth;
				var mobileVideo = document.querySelector(".luxe-augmente-hero .hero-video.is-hidden-tablet");
				mobileVideo.style.height = largeur + "px";
			};
		} else {
			
		}
	} 
	//======================================================================================================//
	
});
