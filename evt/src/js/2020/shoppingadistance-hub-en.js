document.addEventListener("DOMContentLoaded", function(event) {

	document.querySelector(".luxe-augmente").classList.add('has-js');
		
	//============================================ Smooth scroll ===========================================//
	var smoothAnchor = document.querySelectorAll('.has-smoothscroll');
	for (var i=0; i < smoothAnchor.length; i++) {
		smoothAnchor[i].addEventListener("click", function(e) {
			e.preventDefault();
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);
			if (target) {
				if (window.matchMedia("(min-width:1024px)").matches) {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 60;
				} else {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset;
				}
				scroll({
					top: offsetTop,
					behavior: "smooth"
				});
			}
		});
	}
	//======================================================================================================//
	
	
	//======================================= Hauteur Video Mobile ======================================//
	if("matchMedia" in window) {
		var mq = window.matchMedia("(max-width:767px)");
		mq.addListener(WidthChange);
		WidthChange(mq);
	}
	
	function WidthChange(mq) {		
		if (mq.matches) {			
			window.onresize = function() {
				var largeur = window.innerWidth;
				var mobileVideo = document.querySelector(".luxe-augmente-hero .hero-video.is-hidden-tablet");
				mobileVideo.style.height = largeur + "px";
			};
		} else {
			
		}
	} 
	//======================================================================================================//
	
	//===================================== ouverture/fermeture marques ====================================//
	var hiddenBrands = document.querySelector('.all-hide');
	var showMoreTrigger = document.querySelectorAll('.show-more');
	for ( var i=0; i < showMoreTrigger.length; i++) {
		showMoreTrigger[i].addEventListener("click", function(e) {
			e.preventDefault();			
			hiddenBrands.classList.toggle('is-active');
			
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);
			for (var j=0; j < showMoreTrigger.length; j++) {
				
				if (target) {
					if (showMoreTrigger[j].classList.contains('is-active')) {
						showMoreTrigger[j].classList.remove('is-active');
						if (window.matchMedia("(min-width:768px) and (max-width:1023px)").matches) {
							//console.log("tablet");
							var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 96;
						}
						if (window.matchMedia("(min-width:1024px)").matches) { 
							//console.log("desk");
							var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 100;
						}
						if (window.matchMedia("(max-width:767px)").matches) {
							//console.log("mobile");
							var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 96;
						}
					} else {
						showMoreTrigger[j].classList.add('is-active');
						if (window.matchMedia("(min-width:768px) and (max-width:1023px)").matches) {
							var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 10;
							//console.log("tablet");
						}
						if (window.matchMedia("(min-width:1024px)").matches) {
							var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 120;
							//console.log("desk");
						} 
						if (window.matchMedia("(max-width:767px)").matches) {
							var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 96;
							//console.log("mobile");
						}
					}					
					
					scroll({
						top: offsetTop,
						behavior: "smooth"
					});
				}
				
				
			}
						
		});
	}
	
	//======================================================================================================//

});
