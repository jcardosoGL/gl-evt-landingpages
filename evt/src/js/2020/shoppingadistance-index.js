document.addEventListener("DOMContentLoaded", function(event) {

	document.querySelector(".luxe-augmente").classList.add('has-js');
	
	//============================================= Gestion des Sticky  ====================================//
	
	// Duplique la nav par lettre sous le header
	// var headerContainer = document.querySelector("body .header-unified");
	// var stickyNavHtml = document.querySelector(".luxe-augmente-hero .index-nav").outerHTML;
	// headerContainer.insertAdjacentHTML("beforeEnd", stickyNavHtml);
	// var stickyNavHeader = document.querySelector("body .header-unified .index-nav");
	// stickyNavHeader.style.display = "none";
	
	var windowHeight = window.innerHeight;
	var scrollFromTop = window.scrollY + windowHeight;
	window.onscroll = function() {
		var scrolling = window.scrollY + windowHeight;
		var stickyNavOffset = document.querySelector(".sticky-spacer").getBoundingClientRect().top;
						
		var stickyNav = document.querySelector(".luxe-augmente-hero .index-nav");
		var stickyNavScrollDownOffset = stickyNav.getBoundingClientRect().top;
		
		var footerOffset = document.querySelector("footer.footer").getBoundingClientRect().top;
		
		var stickyNavHeader = document.querySelector("body .header-unified .index-nav");

		if (scrolling > scrollFromTop) {
			// scroll vers le bas ==================================
			if (window.matchMedia("(min-width:1024px)").matches) {
				// déclenche la sticky sur l'index
				if (stickyNavScrollDownOffset <= 100) {
					stickyNav.classList.add("is-fixed-top");
				}
				if (footerOffset <= 155) {
					stickyNav.style.top = "45px";
				}		
			} else {
				if (stickyNavScrollDownOffset <= 0) {
					stickyNavHeader.style.display = "flex";
					stickyNavHeader.style.top = "0";
				}
				if (footerOffset <= 50) {
					stickyNavHeader.style.position = "fixed";
					stickyNavHeader.style.top = "-50px";
				}
			}		
			
		} else {	
			// scroll vers le haut ==================================	
			if (window.matchMedia("(min-width:1024px)").matches) {		
				// retire la sticky sur l'index															
				if (stickyNavOffset >= 100) {
					stickyNav.classList.remove("is-fixed-top");
				}
				if (footerOffset >= 100) {
					stickyNav.style.top = "";
				}
			} else {
				stickyNav.classList.remove("is-fixed-top");
				
				stickyNavHeader.style.display = "flex";
				stickyNavHeader.style.top = "0";
						
				if (stickyNavScrollDownOffset >= 98) {
					stickyNavHeader.style.display = "none";	
				}
				if (footerOffset <= 148) {
					stickyNavHeader.style.position = "fixed";
					stickyNavHeader.style.top = "-50px";
				} else {
					stickyNavHeader.style.position = "";
					stickyNavHeader.style.top = "";
				}
			}
		}
		
		scrollFromTop = scrolling;
	};
	
	
	var controller;
	function topBrandSticky() {
		//======================================= Scroll Magic ======================================//
		//https://github.com/janpaepke/ScrollMagic/wiki/Getting-Started-:-How-to-use-ScrollMagic
		// init controller
		controller = new ScrollMagic.Controller();
		var pageHeight = document.querySelector(".brands-container").offsetHeight - 600 + "px";
		var revealArticles = new ScrollMagic.Scene({
			triggerElement: ".brands-container",
			triggerHook: 0, // show, when scrolled 70% into view
			duration: pageHeight, // hide 10% before exiting view (80% + 10% from bottom)
			//reverse: true, // only do once
			offset: -155 // move trigger to center of element
		})
		.setPin(".top-brand")
		//.addIndicators({name: "top-brand" }) // add indicators (requires plugin) for debbug
		.addTo(controller);
		//======================================================================================================//
	}
	
	if("matchMedia" in window) {
		var mq = window.matchMedia("(min-width:768px)");
		mq.addListener(WidthChange);
		WidthChange(mq);
	}
	
	function WidthChange(mq) {
		if (mq.matches) {
			topBrandSticky();
		} else {
			if (controller) {
				controller.destroy(true);
			}
		}		
	}
	//======================================================================================================//
	
	
	
	//============================================ Smooth scroll ===========================================//
	var smoothAnchor = document.querySelectorAll('.has-smoothscroll');
	for (var i=0; i < smoothAnchor.length; i++) {
		smoothAnchor[i].addEventListener("click", function(e) {
			e.preventDefault();
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);
			if (target) {
				if (window.matchMedia("(min-width:1024px)").matches) {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 160;
				} else {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 148;
				}
				scroll({
					top: offsetTop,
					behavior: "smooth"
				});
			}
		});
	}
	
	//======================================================================================================//
		
});

window.onload = function() {
	// Duplique la nav par lettre sous le header
	var headerContainer = document.querySelector("body .header-unified");
	var stickyNavHtml = document.querySelector(".luxe-augmente-hero .index-nav").outerHTML;
	headerContainer.insertAdjacentHTML("beforeEnd", stickyNavHtml);
	var stickyNavHeader = document.querySelector("body .header-unified .index-nav");
	stickyNavHeader.style.display = "none";
	
	//============================================= Gestion du scrollsy ====================================//
	//https://github.com/cferdinandi/gumshoe
	//console.log(headerContainer.getBoundingClientRect().height + 20);
	var spy = new Gumshoe('.luxe-augmente-hero .index-nav li a', {
		navClass: 'is-active',
		contentClass: 'is-active', // s'applique au contenu
		offset: function () {
			// pour prendre en compte la hauteur du header sticky
			return headerContainer.getBoundingClientRect().height + 60;
		}, 
		reflow: true
	});
	
	var spy = new Gumshoe('.header-unified .index-nav li a', {
		navClass: 'is-active',
		contentClass: 'is-active', // s'applique au contenu
		offset: function () {
			// pour prendre en compte la hauteur du header sticky
			return headerContainer.getBoundingClientRect().height + 60;
		}, 
		reflow: true
	});
	//======================================================================================================//
};