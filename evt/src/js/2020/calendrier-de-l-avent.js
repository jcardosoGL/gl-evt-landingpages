document.addEventListener("DOMContentLoaded", function(event) {

	var pageContainer = document.querySelector(".grand-jeu-noel");
	var welcomeScreen = document.querySelector(".grand-jeu-noel .welcome");
	var giftScreen = document.querySelector(".grand-jeu-noel .gifts");
	var formScreen = document.querySelector(".grand-jeu-noel .form");
	
	pageContainer.classList.add('has-js');
	welcomeScreen.classList.add('animate');
	
	
	//============================== Hauteur écran ================================//
	// Permet d'avoir toujours au moins 100% de la hauteur de la fenêtre disponible (moins le header)
	var windowHeight = function() {
		var hauteur;
		if (window.matchMedia("(max-width:767px)").matches) {
			if (window.matchMedia("(max-height:639px)").matches) {
				var hauteur = 646;
			} else {
				var hauteur = window.innerHeight - 49;
			}
		}
		if (window.matchMedia("(min-width:768px) and (max-width:1023px)").matches) {
			if (window.matchMedia("(min-width:768px) and (max-height:760px)").matches) {
				var hauteur = 646;
			} else {
				var hauteur = window.innerHeight - 114;
			}
		}
		if (window.matchMedia("(min-width:1024px)").matches) {
			if (window.matchMedia("(max-height:768px)").matches) {
				var hauteur = 646;
			} else {
				var hauteur = window.innerHeight - 162;
			}
		}

		pageContainer.style.height = hauteur + "px";
	}
	windowHeight();
	//===============================================================================//
	
	
	
	//============================= Gestion anim de transition =====================//
	// welcome -> gifts
	var animContainer = document.querySelector('.tourbillon-jouets');
	var triggerAnim = document.querySelector('.trigger_anim');

	var changeScreen = function() {
		welcomeScreen.classList.remove('animate', 'is-transitioning');
		welcomeScreen.classList.add('is-hidden');
		giftScreen.classList.remove('is-hidden');
	}
	// au clic on ajoute une class pour lancer l'anim puis on attends 5sec avant de switcher les écrans en background
	triggerAnim.addEventListener("click", function(e) {
		welcomeScreen.classList.add('is-transitioning');
		animContainer.classList.add('animate');
		
		setTimeout(changeScreen, 4500);
		setTimeout(function(){ animContainer.classList.remove('animate'); }, 6000);
	});
	//================================================================================//
	
	
	//============================= Gestion ouverture formulaire =====================//
	// gifts -> form
	var triggerForm = document.querySelector('.trigger_form');
	var closeForm = document.querySelector('.close_form');

	// au clic on ajoute une class pour activer le formulaire
	triggerForm.addEventListener("click", function(e) {
		formScreen.style.display = "block";
		setTimeout(function() {formScreen.classList.add('is-active');}, 50);
	});
	
	// au clic 
	var hideForm = function() {
		formScreen.classList.remove('is-active', 'is-hidding');
		formScreen.style.display = "none";
	}
	closeForm.addEventListener("click", function(e) {
		formScreen.classList.add('is-hidding');
		
		setTimeout(hideForm, 300);
	});
	//================================================================================//
});



//========================= Gestion des cadeaux selon les jours ==================//	
var getGiftOfTheDay = function(currentDay) {
	var request;
	// on récupère les infos dans le json
	var requestURL = 'https://static.galerieslafayette.com/media/LP/src/js/2020/calendrier-de-l-avant-data.json?4';
	// var requestURL = '../../../src/js/2020/calendrier-de-l-avant-data.json';
	
	//XMLHttpRequest ne marche pas sur ie11 donc dans ce cas on utilise ActiveXObject
	if (window.XMLHttpRequest) {
		request = new XMLHttpRequest();
	} else {
		request = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	function isEmptyObj(object) { 
		//console.log(object);
		for (var key in object) { 
			if (object.hasOwnProperty(key)) { 
				return false; 
			} 
		} 
		return true; 
	} 
	

	request.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var responseData = request.response[currentDay];
			if ( responseData === undefined) {console.log("Joyeux Noël!")}
			else {
				//on récupère les infos dans le json
				var brand 				= responseData[0].brand;
				var product 			= responseData[0].product;
				var description 	= responseData[0].description;
				var srcImage 			= responseData[0].src;
				var srcsetImage 	= responseData[0].srcset;
				var background  	= responseData[0].background;
				
				if (brand == "") {}
				else {
					 brand = brand + " — ";
				}
				
				//on injecte les infos dans le html
				document.querySelector(".calendrier-avent .gifts.container").classList.add('bg-' + background);
				document.querySelector(".gifts .day").classList.add('day-' + currentDay);
				document.querySelector(".gifts .day").innerHTML = currentDay;
				document.querySelector(".gifts .title .brand").innerHTML = brand;
				document.querySelector(".gifts .title .product").innerHTML = product;
				document.querySelector(".gifts .description").innerHTML = description;
				document.querySelector(".gifts .product").setAttribute("src", srcImage);
				document.querySelector(".gifts .product").setAttribute("srcset", srcsetImage + " 2x");
				document.querySelector(".gifts .product").setAttribute("alt", brand + product + " - Galeries Lafayette");
			}        
		}
	};
	request.open('GET', requestURL, true);
	request.responseType = 'json'; //ne marche pas sur ie11
	request.send(null);
	
	return false;
};

// var date = new Date();
// // pour tester A SUPPRIMER ===========
// date.setDate(4);
// // ===================================
// var currentDay = date.getDate();
// //console.log(currentDay);
// 
// getGiftOfTheDay(currentDay);
//========================================================================================//



document.addEventListener('DOMContentLoaded', calendarForm, false);
function calendarForm() {
	$("#calendar-form").on("submit", function(e) {
		e.preventDefault();
		var data={}
		var optin=[];
		var partneroptin=[];
		$(".formSubmit").each(function(e) {
			if ($(this).attr('name')==="optin") {
				if($(this).prop('checked')===true) {
					if(optin.indexOf($(this).attr('data-type'))===-1)
						optin.push($(this).attr('data-type'));
				}
				data["optin"]=optin.join(",");
			} else if ($(this).attr('name')==="partneroptin") {    
				if($(this).prop('checked')===true) {
					if(partneroptin.indexOf($(this).attr('data-type'))===-1)
						partneroptin.push($(this).attr('data-type'));
				}
				data["partneroptin"]=partneroptin.join(",");
			} else {
				data[$(this).attr('name')]=$(this).val();
			}
			//console.log($(this).attr('name'))
			//console.log($(this).val());
		});                                 
		
		var date = new Date();      
		if (date.getDate() > 24) {
			var currentDay = "";
		} else {
			var currentDay = "-12-" + date.getDate();
		}
		
		data.contest="calendrier-de-l-avent-2020" + currentDay;
		//console.log(data);
		
		$.ajax({
			url:"https://www.galerieslafayette.com/op/jeux/",
			data:data,
			method:"POST",
			dataType:"html",
			success:function(result, status) {
				if(result==="user registered") {
					document.querySelector(".form .form-main").classList.add('is-hidden');
					document.querySelector(".form .form-success").classList.remove('is-hidden');
					console.log(result);
					$("#calendar-form")
				} else if(result==="user already registered") {
					document.querySelector(".form .form-main").classList.add('is-hidden');
					document.querySelector(".form .form-already-registered").classList.remove('is-hidden');
					console.log(result);
				} else {
					$(".errormessage").html("Une erreur est survenue !");
					console.log("Une erreur est survenue !");
				}
			}
		});
	});
}      
