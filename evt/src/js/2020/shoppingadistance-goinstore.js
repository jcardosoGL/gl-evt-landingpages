//============ load goinstore script ===============//
var goinstoreScript = document.createElement('script'); 
goinstoreScript.src = "//gis.goinstore.com/gis/script/d320e21d-3fbc-468b-9dae-86409198d897"; 
goinstoreScript.async = true;
goinstoreScript.defer = true;
document.getElementById("goinstore_modals").appendChild(goinstoreScript); 
//===============================//



//====== Google ReCaptcha ========//
var recaptchaWidget;
var onloadCallback = function () {
	recaptchaWidget = grecaptcha.render('recaptcha_element', {
		sitekey: '6LeDz_cUAAAAAAv0cVuWW6F-xBAV2cD2BJmprF_F',
		callback: verifyCallback
	});
};

function verifyCallback() {
	if (grecaptcha.getResponse(recaptchaWidget)) {
		document.getElementById('liveshopping-form-submit').removeAttribute('disabled');
	}
}

// load google script
var recaptchaScript = document.createElement('script'); 
recaptchaScript.src = "https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"; 
recaptchaScript.async = true;
recaptchaScript.defer = true;
document.getElementById("goinstore_modals").appendChild(recaptchaScript); 
//===============================//





//======== Création des modals ========//
const modalContainer = document.getElementById("goinstore_modals");
const modalOpen = `
<!--=========================== POPIN BRAND AVAILABLE ========================-->
<div class="reveal-modal opened" data-reveal="" id="popin-live-shopping-open" role="dialog">
		<div>
				<a class="close-reveal-modal" aria-label="Close">Fermer</a>
				<div class="shopping-modal">
						<div>
								<div id="ps-closed">
										<h3>Votre conseiller est disponible</h3>
										<p>Pour être mis en relation, il vous suffit de démarrer le live vidéo ou de prendre un rendez-vous sur le créneau de votre choix.</p>
										<button id="live-shopping-goinstore-start-chat" class="live-shopping-cta video" onclick="GL_GIS.LiveShopping.onConnectClick(); return false;">Demarrer le live vid&eacute;o</button>
										<button class="live-shopping-cta appointed-start-rdv" onclick="appointeddWidget.render('apwidget'); return false;" data-reveal-id="popin-live-shopping-rdv" target="_self">Prendre un rendez-vous vid&eacute;o</button>

										<hr/>
										<div class="modal-footer">
											<h3>Vous savez ce que vous d&eacute;sirez ? </h3>
											<a href="" class="is-text" data-reveal-id="popin-live-shopping-form">Renseignez votre wishlist</a>
										</div>
								</div>
						</div>
				</div>
		</div>
</div>`;

const modalClosed = `
<!--=========================== POPIN BRAND UNAVAILABLE ========================-->
<div class="reveal-modal opened" data-reveal="" id="popin-live-shopping-closed" role="dialog">
		<div>
				<a class="close-reveal-modal" aria-label="Close">Fermer</a>
				<div class="shopping-modal">
						<div>
								<div id="ps-closed">
										<h3>Conseiller indisponible</h3>
										<p>Nous sommes d&eacute;sol&eacute;s, votre conseiller n&rsquo;est pas disponible. Nous vous invitons à prendre rendez-vous pour votre live shopping vidéo.</p>
										<button class="live-shopping-cta appointed-start-rdv" onclick="appointeddWidget.render('apwidget'); return false;" data-reveal-id="popin-live-shopping-rdv" target="_self">Prendre un rendez-vous vid&eacute;o</button>
										<p class="small">Le service est ouvert du lundi au vendredi de 10h à 18h <br />
										(hors jours fériés)</p>
										<hr/>
										<div class="modal-footer">
											<h3>Vous savez ce que vous d&eacute;sirez ? </h3>
											<a href="" class="is-text" data-reveal-id="popin-live-shopping-form">Renseignez votre wishlist</a>
										</div>
								</div>
						</div>
				</div>
		</div>
</div>`;

const modalGisUnavailable = `
<!--=========================== POPIN GIS UNAVAILABLE ========================-->
<div class="reveal-modal opened" data-reveal="" id="popin-live-shopping-unavailable" role="dialog">
		<div>
				<a class="close-reveal-modal" aria-label="Close">Fermer</a>
				<div class="shopping-modal">
						<div>
								<div id="ps-closed">
										<h3>Service indisponible</h3>
										<p>Nous sommes d&eacute;sol&eacute;s, le service est momentanément indisponible. Nous vous invitons à réessayer plus tard ou à remplir votre wishlist.</p>
										<hr/>
										<div class="modal-footer">
											<h3>Vous savez ce que vous d&eacute;sirez ? </h3>
											<a href="" class="is-text" data-reveal-id="popin-live-shopping-form">Renseignez votre wishlist</a>
										</div>
								</div>
						</div>
				</div>
		</div>
</div>`;

const modalBrowserUnsupported = `
<!--=========================== POPIN BROWSER UNSUPPORTED ========================-->
<div class="reveal-modal opened" data-reveal="" id="popin-live-shopping-browser-unsupported" role="dialog">
		<div>
				<a class="close-reveal-modal" aria-label="Close">Fermer</a>
				<div class="shopping-modal">
						<div>
								<div id="ps-closed">
										<h3>Navigateur non supporté</h3>
										<p>Nous sommes d&eacute;sol&eacute;s, votre navigateur ne supporte pas les fonctionnalités de live vidéo.</p>
										<hr/>
										<div class="modal-footer">
											<h3>Vous savez ce que vous d&eacute;sirez ? </h3>
											<a href="" class="is-text" data-reveal-id="popin-live-shopping-form">Renseignez votre wishlist</a>
										</div>
								</div>
						</div>
				</div>
		</div>
</div>`;

const modalForm = `<!--=========================== POPIN FORM ========================-->
<div class="reveal-modal opened" data-reveal="" id="popin-live-shopping-form" role="dialog">
		<div>
				<a class="close-reveal-modal" aria-label="Close">Fermer</a>	
				<div class="shopping-modal">
						<div class="live-shopping-form" id="liveshopping-id">
								<h3>Votre Wishlist</h3>
								<div class="categories">
									<p>Mode / Accessoires / Horlogerie / Joaillerie / Jouet / Autres secteurs sur demande</p>
								</div>
								<p>Remplissez le formulaire suivant pour nous partager votre wishlist avec les pièces Luxe & Créateurs dont vous avez envie et ainsi être livré sereinement chez vous. Nous nous occupons de tout.</p>

								<form id="liveshopping-form">
									<select name="entry.304588818" required="">
											<option disabled="disabled" selected="selected" value="">Civilit&eacute;*</option>
											<option value="Monsieur">Monsieur</option>
											<option value="Madame">Madame</option>
									</select>
									<input name="entry.509454348" placeholder="Nom*" required="" type="text" pattern="[A-Za-zÀ-ÖØ-öø-ÿ- ']+" maxlength="20" />
									<input name="entry.1522782477" placeholder="Prénom*" required="" type="text" pattern="[A-Za-zÀ-ÖØ-öø-ÿ- ']+" maxlength="20" />
									<input name="entry.468148627" placeholder="Numéro carte de fidélité" type="text" pattern="[0-9]{17}" maxlength="17" />
									<input name="entry.217612602" placeholder="Email*" required="" type="email"/>
									<input name="entry.517758040" placeholder="Téléphone*" required="" type="tel" pattern="[0-9-() +]+" maxlength="20" />
									<input name="entry.974087837" placeholder="Adresse*" required="" type="text" pattern="[0-9A-Za-zÀ-ÖØ-öø-ÿ- '/]+" maxlength="100" />
									<input name="entry.189941857" placeholder="Code postal*" required="" type="text" pattern="[a-zA-Z0-9 -/]+" maxlength="10" />
									<input name="entry.893139328" placeholder="Ville*" required="" type="text" pattern="[A-Za-zÀ-ÖØ-öø-ÿ- ']+" maxlength="40" />
									<input name="entry.733239574" placeholder="Pays*" required="" type="text" pattern="[A-Za-zÀ-ÖØ-öø-ÿ- ']+" maxlength="40" />
									<textarea id="liveshopping-form-marque" name="entry.1094282192" rows="3" placeholder="Marque(s)*" required="" pattern="[\p{L}-]+" maxlength="200"></textarea>
									<textarea name="entry.1705887713" rows="3" placeholder="Produits souhaités*" required="" pattern="[\p{L}-]+" maxlength="400"></textarea>
									<select name="entry.1908800211" required="">
											<option disabled="disabled" selected="selected" value="">Mode de r&eacute;cup&eacute;ration souhait&eacute;*</option>
											<option value="Click &amp; Collect Galeries Lafayette Haussmann">Click &amp; Collect Galeries Lafayette Haussmann</option>
											<option value="Click &amp; Collect dans un autre magasin Galeries Lafayette">Click &amp; Collect dans un autre magasin Galeries Lafayette</option>
											<option value="Livraison à domicile">Livraison &agrave; domicile</option>
									</select>
									<textarea name="entry.1884808902" rows="3" placeholder="Précisions complémentaires" pattern="[\p{L}-]+" maxlength="400"></textarea>
									<input name="entry.1901623741" placeholder="Matricule vendeur" type="hidden"/>
									<input name="entry.745477689" placeholder="Magasin" type="hidden"/>
									<input name="entry.1413210578" placeholder="ID Socle" type="hidden"/>
									<span>*champs obligatoires</span>
	
									<div id="recaptcha_element">&nbsp;</div>
									<button class="live-shopping-cta" disabled="true" id="liveshopping-form-submit" type="submit">Envoyer</button>
							</form>
						</div>
				</div>
		</div>
</div>`;

const modalFormSuccess = `
<!--=========================== POPIN FORM SUCCESS ========================-->
<div class="reveal-modal opened" data-reveal="" id="popin-live-shopping-form-success" role="dialog">
		<div>
				<a class="close-reveal-modal" aria-label="Close">Fermer</a>
				<img alt="" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/live-shopping/m-formulaire02.jpg"/>
				<div class="shopping-modal">
						<div>
								<div id="ps-closed">
										<h2 id="modalTitle">Merci !</h2>
										<p>
												Nous avons bien reçu votre message ! Un conseiller vous recontactera rapidement pour finaliser la vente !
										</p>
								</div>
						</div>
				</div>
		</div>
</div>`;

const modalRdv = `
<!--=========================== POPIN APPOINTED ========================-->
<div class="reveal-modal opened" data-reveal="" id="popin-live-shopping-rdv" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">Fermer</a>
	<div class="modal-content" id="apwidget-container"></div>
</div>`;

modalContainer.insertAdjacentHTML("afterbegin", modalOpen + modalClosed + modalGisUnavailable + modalBrowserUnsupported + modalForm + modalFormSuccess + modalRdv);
//==============================//






//======== GO IN STORE WIDGET ======//
var GL_GIS = GL_GIS || {};
GL_GIS.LiveShopping = (function () {
	'use strict';
	var PLUGIN_ERROR = "PluginError";
	function show(id) {
			document.getElementById(id).style.display = 'block';
	}
	function hide(id) {
			document.getElementById(id).style.display = 'none';
	}
	function showModal() {
			show('msc-confirm');
			document.getElementById('install-btn').onclick = installPlugin;
	}
	function hideModal() {
			hide('msc-confirm');
	}

	function callEndedHandler(data) {
			console.log('GIS: call ended.');
	}
	function connectWithAutoInstallPlugin() {
			console.log('GIS: connecting via auto install plugin');
			gisApi.checkMicrophone(checkMicrophoneHandler);
	}
	function connectWithOutAutoInstallPlugin() {
			console.log('GIS: connecting via without auto install plugin');
			gisApi.checkMicrophone(checkMicrophoneHandler, false);
	}
	function checkMicrophoneHandler(err, hasMicrophone) {
			if (err instanceof Error && err.name === PLUGIN_ERROR) {
					showModal();
			} else if(err) {
					console.log("Error while checking Microphone: ", err);
			} else {
					console.log('GIS: checkMicrophoneHandler ok');
					gisApi.startCall(callEndedHandler);
			}
	}
	function installPlugin() {
			hideModal();
			gisApi.installPlugin();
	}
	function onSubmitContact(contactForm) {
			gisApi.submitContactDetails(contactForm);
	}

	function checkApi() {
		return typeof gisApi != 'undefined';
	}
	function checkEnvironment() {
			return typeof gisApi != 'undefined'
					&& typeof gisApi.getVersionInfo() != 'undefined'
					&& gisApi.getVersionInfo().version.charAt(0) === '1'
					&& gisApi.checkSystemSupported();
	}
	function checkPopinClerkAvailability(routingKeys) {
		console.log('GIS: Checking clerk availability.');
		
		var existingAPScript = document.getElementById('apwidget');
		if (!existingAPScript) {
			var appointedIframe = document.createElement('div');
			appointedIframe.id = "apwidget";
			
			var appointedScript = document.createElement('script'); 
			appointedScript.src = "https://retailwidgets.appointedd.com/widget/"+ appointedWidgetId +"/widget.js";
			
			var apContainer = document.getElementById("apwidget-container");
			apContainer.appendChild(appointedIframe);
			apContainer.appendChild(appointedScript);
		}
		
		if (checkApi()) {
			console.log('GIS is enabled');
			if (checkEnvironment()) {
				console.log('GIS: Current system supported.');
				var routingkeyAsJSON = { required:[routingKeys.split(",")], optional: [] };
				gisApi.checkClerkAvailability(clerkPopinAvailableHandler, clerkPopinUnavailableHandler, JSON.stringify(routingkeyAsJSON));
				document.getElementById('live-shopping-goinstore-start-chat').dataset.brand = routingKeys;				
			} else {
				console.log('GIS: Current system not supported.');
				$('#popin-live-shopping-browser-unsupported').foundation('reveal', 'open');
			}
		} else {
			console.log('GIS is not available');
			$('#popin-live-shopping-unavailable').foundation('reveal', 'open');
		}
	}
	function clerkPopinAvailableHandler() {
			console.log('GIS: clerk available.');
			$('#popin-live-shopping-open').foundation('reveal', 'open');
			gisApi.logEvent(gisApi.Events.CALL_CTA_SHOWN);
	}
	function clerkPopinUnavailableHandler() {
			console.log('GIS: clerk unavailable.');
			$('#popin-live-shopping-closed').foundation('reveal', 'open');
	}
	function onConnectClick() {
		var routingKeys = document.getElementById('live-shopping-goinstore-start-chat').dataset.brand;
		$('#popin-live-shopping-open').foundation('reveal', 'close');

		var routingkeyAsJSON = { required:[routingKeys.split(",")], optional: [] };

		gisApi.logEvent(gisApi.Events.CALL_CTA_CLICK);
		gisApi.startCall(callEndedHandler, JSON.stringify(routingkeyAsJSON));
	}
	
	
	function clerkAvailableHandler() {
		console.log('GIS: clerk is available.');
		gisApi.logEvent(gisApi.Events.CALL_CTA_SHOWN);
	}
	function clerkUnavailableHandler() {
		console.log('GIS: clerk is unavailable.');
	}
	function init(routingKeyOnInit) {
		if (checkApi()) {
			console.log('GIS is enabled');
			if (checkEnvironment()) {
				console.log('GIS: Current system supported.');
				var routingKeyObject = { required:[], optional: [] };
				routingKeyObject.required.push([routingKeyOnInit]);
				gisApi.checkClerkAvailability(clerkAvailableHandler, clerkUnavailableHandler, JSON.stringify(routingKeyObject));
			} else {
				console.error("Incompatible version of GIS API detected!");
			}
		} else {
			console.log('gisApi not enabled.');
		}
	}
	function startCall(routingKeyOnInit) {
		var routingKeyObject = { required:[], optional: [] };
		routingKeyObject.required.push([routingKeyOnInit]);
		gisApi.startCall(callEndedHandler, JSON.stringify(routingKeyObject));
		gisApi.logEvent(gisApi.Events.CALL_CTA_CLICK);
	}
	
	return {
			init: init,
			startCall: startCall,
			onConnectClick: onConnectClick,
			onSubmitContact: onSubmitContact,
			connectWithAutoInstallPlugin: connectWithAutoInstallPlugin,
			connectWithOutAutoInstallPlugin: connectWithOutAutoInstallPlugin,
			checkPopinClerkAvailability: checkPopinClerkAvailability
	};
}
());
window.onGisApiReady = function () {
	gisApi.enableStandardJourney();	
};

	

document.addEventListener("DOMContentLoaded", function(event) {
	//======== Gestion du formulaire ========//
	$(document).on('close', '[data-reveal]', function () {
		var modal = $(this);
		var apContainer = document.getElementById("apwidget-container");
		apContainer.innerHTML = "";
	});
	
	$(document).on('closed', '[data-reveal]', function () {
			var modal = $(this);
			if (modal.attr('id') === 'popin-live-shopping-form') {
					document.getElementById('liveshopping-form-submit').setAttribute('disabled', 'true');
			 }
	});
	
	var triggerLiveshopping = document.querySelectorAll('.trigger_liveshopping');
	var displayBrand = document.querySelectorAll('.brand');
	for (var i=0; i < triggerLiveshopping.length; i++) {
		triggerLiveshopping[i].addEventListener("click", function(e) {
			//console.log(displayBrand);
			document.getElementById('liveshopping-form-marque').value = clickedBrand || '';
			for (var e=0; e < displayBrand.length; e++) {
				displayBrand[e].innerHTML = clickedBrand;
			}
		});
	}	
	
	function sendData() {
		var XHR = new XMLHttpRequest();
		var FD = new FormData(form);
		
		XHR.addEventListener("load", function (event) {
			form.reset();
			grecaptcha.reset(recaptchaWidget);
			$('#popin-live-shopping-form').foundation('reveal', 'close');
			$('#popin-live-shopping-form-success').foundation('reveal', 'open');
		});
		XHR.addEventListener("error", function (event) {
			form.reset();
			grecaptcha.reset(recaptchaWidget);
			$('#popin-live-shopping-form').foundation('reveal', 'close');
			$('#popin-live-shopping-form-success').foundation('reveal', 'open');
		});
		XHR.open("POST", "https://docs.google.com/forms/u/0/d/e/1FAIpQLScRe9z6i6MgsY0xvkyVvh1fDV7BMXRjRyKQbcG61hLoc9-RJw/formResponse");
		XHR.send(FD);
	}
	
	var form = document.getElementById("liveshopping-form");
	form.addEventListener("submit", function (event) {
		event.preventDefault();
		sendData();
	});
	//==============================//
		
	//========= close and reload page on liveshopping popin ========//
	document.getElementById('live-shopping-goinstore-start-chat').addEventListener("click", function() {
		var liveShoppingCloseButton = document.querySelector('.gis-v3-close-btn');
		if (liveShoppingCloseButton != undefined) {
			liveShoppingCloseButton.addEventListener("click", function(e) {
				e.preventDefault();
				document.location.reload();
			});
		}
	});
	//===============================================================//	
});