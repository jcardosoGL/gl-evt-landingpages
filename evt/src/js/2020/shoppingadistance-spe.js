document.addEventListener("DOMContentLoaded", function(event) {

	document.querySelector(".luxe-augmente").classList.add('has-js');
		
	var readMore = document.querySelector('.read-more');
	var readMoreTrigger = document.querySelector('.read-more .read-more-toggle');
	readMoreTrigger.onclick = function(e) {
		readMore.classList.toggle('is-active');
	}
	
	
	//======================================= Scroll Magic ======================================//
	//https://github.com/janpaepke/ScrollMagic/wiki/Getting-Started-:-How-to-use-ScrollMagic
	// init controller
	var controller = new ScrollMagic.Controller();
	
	// create a scene
	// récupère la couleur du bg depuis la page
	var pageBgColor = document.querySelector('.luxe-augmente').dataset.bgColor;
	if (pageBgColor.length < 1) {
		//console.log('pas de bg color');	
	} else {
		// animation de la couleur de fond de la page
		// build tween
		var tweenPageBg = TweenMax.to(".luxe-augmente", 1, {backgroundColor: pageBgColor});
		// build scene
		var heroFade = new ScrollMagic.Scene({
			triggerElement: ".luxe-augmente",
			triggerHook: 0.18, 
			duration: "70%", 
			offset: 0
			})
		.setTween(tweenPageBg)
		//.addIndicators({name: "tween bg"}) // add indicators (requires plugin)
		.addTo(controller);
	}
	
	// create a scene - fait un fade sur le hero
	// build tween
	var tweenHeroFade = TweenMax.to(".hero-bg-image", 1, {opacity: 0.1});
	// build scene
	var heroFade = new ScrollMagic.Scene({
		triggerElement: ".luxe-augmente-hero",
		triggerHook: 0, 
		duration: "40%", 
		offset: 0
		})
	.setTween(tweenHeroFade)
	//.addIndicators({name: "tween mask"}) // add indicators (requires plugin)
	.addTo(controller);
	
	
	var tweenHeroTxt = TweenMax.to(".luxe-augmente-hero .luxe-row", 2, {opacity: 0, bottom: -80});
	// build scene
	var heroTxt = new ScrollMagic.Scene({
		triggerElement: ".luxe-augmente-hero",
		triggerHook: 0, 
		duration: "27%", 
		offset: 210
		})
	.setTween(tweenHeroTxt)
	//.addIndicators({name: "tween mask"}) // add indicators (requires plugin)
	.addTo(controller);
	
	
	// create a scene - ajoute la class .visible sur les articles
	// toggle class
	var revealElements = document.getElementsByClassName("animated-article");
	for (var i=0; i<revealElements.length; i++) { // create a scene for each element
		var revealArticles = new ScrollMagic.Scene({
			triggerElement: revealElements[i],
			triggerHook: 0.85, // show, when scrolled 70% into view
			//duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
			reverse: false, // only do once
			offset: 0 // move trigger to center of element
		})
		.setClassToggle(revealElements[i], "visible") // add class to reveal
		//.addIndicators({name: "article" + (i+1) }) // add indicators (requires plugin) for debbug
		.addTo(controller);
	}
		
});
