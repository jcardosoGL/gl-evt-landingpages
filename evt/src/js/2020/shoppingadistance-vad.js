document.addEventListener("DOMContentLoaded", function(event) {

	document.querySelector(".luxe-augmente").classList.add('has-js');
		
	//============================================ Smooth scroll ===========================================//
	var smoothAnchor = document.querySelectorAll('.has-smoothscroll');
	for (var i=0; i < smoothAnchor.length; i++) {
		smoothAnchor[i].addEventListener("click", function(e) {
			e.preventDefault();
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);
			var abTasty = document.querySelector('.ab_widget_container_promotional-banner');
			if (target) {				
				if (window.matchMedia("(min-width:1024px)").matches) {
					if (abTasty) {
						var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 170;
					} else {
						var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 98;
					}
				} else {
					if (abTasty) {
						var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 70;
					} else {
						var offsetTop = target.getBoundingClientRect().top + window.pageYOffset;
					}
				}
				scroll({
					top: offsetTop,
					behavior: "smooth"
				});
			}
		});
	}
	//======================================================================================================//
	
	var readMore = document.querySelector('.read-more-content');
	var readMoreTrigger = document.querySelector('.read-more-toggle');
	var readLessTrigger = document.querySelector('.read-less-toggle');
	readMoreTrigger.onclick = function() {
		this.classList.toggle('is-hidden');
		readMore.classList.toggle('is-hidden');
	};
	readLessTrigger.onclick = function() {
		readMoreTrigger.classList.toggle('is-hidden');
		readMore.classList.toggle('is-hidden');
	};
	
	
	//======================================= Scroll Magic ======================================//
	//https://github.com/janpaepke/ScrollMagic/wiki/Getting-Started-:-How-to-use-ScrollMagic
	// init controller
	var controller = new ScrollMagic.Controller();
	
	// create a scene
	// récupère la couleur du bg depuis la page
	var pageBgColor = document.querySelector('.luxe-augmente').dataset.bgColor;
	if (pageBgColor.length < 1) {
		//console.log('pas de bg color');	
	} else {
		// animation de la couleur de fond de la page
		// build tween
		var tweenPageBg = TweenMax.to(".luxe-augmente", 1, {backgroundColor: pageBgColor});
		// build scene
		var heroFade = new ScrollMagic.Scene({
			triggerElement: ".luxe-augmente",
			triggerHook: 0.18, 
			duration: "70%", 
			offset: 0
			})
		.setTween(tweenPageBg)
		//.addIndicators({name: "tween bg"}) // add indicators (requires plugin)
		.addTo(controller);
	}
	
	// create a scene - fait un fade sur le hero
	// build tween
	var tweenHeroFade = TweenMax.to(".hero-bg-image", 1, {opacity: 0.1});
	// build scene
	var heroFade = new ScrollMagic.Scene({
		triggerElement: ".luxe-augmente-hero",
		triggerHook: 0, 
		duration: "40%", 
		offset: 48
		})
	.setTween(tweenHeroFade)
	//.addIndicators({name: "tween mask"}) // add indicators (requires plugin)
	.addTo(controller);
	
	var tweenHeroTxt = TweenMax.to(".luxe-augmente-hero .luxe-row", 2, {opacity: 0, bottom: "-=80"});
	// build scene
	var heroTxt = new ScrollMagic.Scene({
		triggerElement: ".luxe-augmente-hero",
		triggerHook: 0, 
		duration: "27%", 
		offset: 110
		})
	.setTween(tweenHeroTxt)
	//.addIndicators({name: "tween mask"}) // add indicators (requires plugin)
	.addTo(controller);
	
	
	// create a scene - ajoute la class .visible sur les articles
	// toggle class
	var revealElements = document.querySelectorAll(".animated-article");
	//console.log(revealElements.length);
	for (var i=0; i<revealElements.length; i++) { // create a scene for each element
		var revealArticles = new ScrollMagic.Scene({
			triggerElement: revealElements[i],
			triggerHook: 0.90, // show, when scrolled 10% into view
			//duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
			reverse: false, // only do once
			offset: 0 // move trigger to center of element
		})
		.setClassToggle(revealElements[i], "visible") // add class to reveal
		//.addIndicators({name: "article" + (i+1) }) // add indicators (requires plugin) for debbug
		.addTo(controller);
	}
	
	
	
	// create a scene - effet de parallax sur article 2
	// build tween
	var tweenArticle2 = TweenMax.to(".tabs-container .tab-pane:nth-child(2) .animated-article.article2 .article-image", 1, {bottom: 80});
	// build scene
	var tweenArticle2Scene = new ScrollMagic.Scene({
		triggerElement: '.tabs-container .tab-pane:nth-child(2) .animated-article.article2',
		triggerHook: 0.90, // show, when scrolled 70% into view
		duration: "135%", // hide 10% before exiting view (80% + 10% from bottom)
		offset: 0 // move trigger to center of element
	})
	.setTween(tweenArticle2) // add class to reveal
	.addTo(controller);

	// create a scene - effet de parallax sur article 2 du 2ème onglet
	// build tween
	var tweenArticle2Pane2 = TweenMax.to(".tabs-container .tab-pane:nth-child(3) .animated-article.article2 .article-image", 1, {bottom: 80});
	// build scene
	var tweenArticle2Pane2Scene = new ScrollMagic.Scene({
		triggerElement: '.tabs-container .tab-pane:nth-child(3) .animated-article.article2',
		triggerHook: 0.90, // show, when scrolled 70% into view
		duration: "135%", // hide 10% before exiting view (80% + 10% from bottom)
		offset: 0 // move trigger to center of element
	})
	.setTween(tweenArticle2Pane2) // add class to reveal
	//.addIndicators({name: "article tween 1" }) // add indicators (requires plugin) for debbug
	.addTo(controller);
	
	
	
	// create a scene - effet de parallax sur article 3
	// build tween
	var tweenArticle3 = TweenMax.to(".tabs-container .tab-pane:nth-child(2) .animated-article.article3 .article-image", 1, {bottom: 30});
	// build scene
	var tweenArticle3Scene = new ScrollMagic.Scene({
		triggerElement: '.tabs-container .tab-pane:nth-child(2) .animated-article.article3',
		triggerHook: 0.90, // show, when scrolled 70% into view
		duration: "135%", // hide 10% before exiting view (80% + 10% from bottom)
		offset: 0 // move trigger to center of element
	})
	.setTween(tweenArticle3) // add class to reveal
	//.addIndicators({name: "article tween 1" }) // add indicators (requires plugin) for debbug
	.addTo(controller);
	
	// create a scene - effet de parallax sur article 3
	// build tween
	var tweenArticle3Pane2 = TweenMax.to(".tabs-container .tab-pane:nth-child(3) .animated-article.article3 .article-image", 1, {bottom: 30});
	// build scene
	var tweenArticle3Pane2Scene = new ScrollMagic.Scene({
		triggerElement: '.tabs-container .tab-pane:nth-child(3) .animated-article.article3',
		triggerHook: 0.90, // show, when scrolled 70% into view
		duration: "135%", // hide 10% before exiting view (80% + 10% from bottom)
		offset: 0 // move trigger to center of element
	})
	.setTween(tweenArticle3Pane2) // add class to reveal
	//.addIndicators({name: "article tween 1" }) // add indicators (requires plugin) for debbug
	.addTo(controller);
	
	
	
	// create a scene - effet de parallax sur le texte de .distance-shopping
	// build tween
	var tweenDistanceShoppingTxt = TweenMax.to(".distance-shopping .animated-article .article-txt", 1, {bottom: 60});
	// build scene
	var tweenDistanceShoppingTxtScene = new ScrollMagic.Scene({
		triggerElement: '.distance-shopping .article-txt',
		triggerHook: 0.90, // show, when scrolled 70% into view
		duration: "135%", // hide 10% before exiting view (80% + 10% from bottom)
		offset: 0 // move trigger to center of element
	})
	.setTween(tweenDistanceShoppingTxt) // add class to reveal
	//.addIndicators({name: "article txt" }) // add indicators (requires plugin) for debbug
	.addTo(controller);

});
