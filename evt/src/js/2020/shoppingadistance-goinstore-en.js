//====== Google ReCaptcha ========//
var recaptchaWidget;
var onloadCallback = function () {
	recaptchaWidget = grecaptcha.render('recaptcha_element', {
		sitekey: '6LeDz_cUAAAAAAv0cVuWW6F-xBAV2cD2BJmprF_F',
		callback: verifyCallback
	});
};

function verifyCallback() {
	if (grecaptcha.getResponse(recaptchaWidget)) {
		document.getElementById('liveshopping-form-submit').removeAttribute('disabled');
	}
}

// load google script
var recaptchaScript = document.createElement('script'); 
recaptchaScript.src = "https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"; 
recaptchaScript.async = true;
recaptchaScript.defer = true;
document.getElementById("goinstore_modals").appendChild(recaptchaScript); 
//===============================//





//======== Création des modals ========//
const modalContainer = document.getElementById("goinstore_modals");

const modalForm = `<!--=========================== POPIN FORM ========================-->
<div class="reveal-modal opened" data-reveal="" id="popin-live-shopping-form" role="dialog">
		<div>
				<a class="close-reveal-modal" aria-label="Close">Close</a>	
				<div class="shopping-modal">
						<div class="live-shopping-form" id="liveshopping-id">
								<h3>The Wishlist</h3>
								<div class="categories">
									<p>Fashion / Accessories / Watches / Jewelry / Toys / Other on-demand categories </p>
								</div>
								<p>Fill in the form below to send us your Luxury & and Designer wishlist for a carefree delivery, we will take care of the rest.</p>

								<form id="liveshopping-form">
									<select name="entry.304588818" required="">
											<option disabled="disabled" selected="selected" value="">Civility*</option>
											<option value="Monsieur">Sir</option>
											<option value="Madame">Mrs</option>
									</select>
									<input name="entry.509454348" placeholder="Last name*" required="" type="text" pattern="[A-Za-zÀ-ÖØ-öø-ÿ- ']+" maxlength="20" />
									<input name="entry.1522782477" placeholder="First name*" required="" type="text" pattern="[A-Za-zÀ-ÖØ-öø-ÿ- ']+" maxlength="20" />
									<input name="entry.468148627" placeholder="Loyalty card number" type="text" pattern="[0-9]{17}" maxlength="17" />
									<input name="entry.217612602" placeholder="Email*" required="" type="email"/>
									<input name="entry.517758040" placeholder="Phone*" required="" type="tel" pattern="[0-9-() +]+" maxlength="20" />
									<input name="entry.974087837" placeholder="Address*" required="" type="text" pattern="[0-9A-Za-zÀ-ÖØ-öø-ÿ- '/]+" maxlength="100" />
									<input name="entry.189941857" placeholder="Postal code*" required="" type="text" pattern="[a-zA-Z0-9 -/]+" maxlength="10" />
									<input name="entry.893139328" placeholder="City*" required="" type="text" pattern="[A-Za-zÀ-ÖØ-öø-ÿ- ']+" maxlength="40" />
									<input name="entry.733239574" placeholder="Country*" required="" type="text" pattern="[A-Za-zÀ-ÖØ-öø-ÿ- ']+" maxlength="40" />
									<textarea id="liveshopping-form-marque" name="entry.1094282192" rows="3" placeholder="Brand(s)*" required="" pattern="[\p{L}-]+" maxlength="200"></textarea>
									<textarea name="entry.1705887713" rows="3" placeholder="Desired products*" required="" pattern="[\p{L}-]+" maxlength="400"></textarea>
									<select name="entry.1908800211" required="">
											<option disabled="disabled" selected="selected" value="">Desired recovery mode*</option>
											<option value="Click &amp; Collect Galeries Lafayette Haussmann">Click &amp; Collect Galeries Lafayette Haussmann</option>
											<option value="Livraison à domicile">Home delivery</option>
									</select>
									<textarea name="entry.1884808902" rows="3" placeholder="Additional details" pattern="[\p{L}-]+" maxlength="400"></textarea>
									<input name="entry.1901623741" placeholder="Matricule vendeur" type="hidden"/>
									<input name="entry.745477689" placeholder="Magasin" type="hidden"/>
									<input name="entry.1413210578" placeholder="ID Socle" type="hidden"/>
									<span>*Required fields</span>
	
									<div id="recaptcha_element">&nbsp;</div>
									<button class="live-shopping-cta" disabled="true" id="liveshopping-form-submit" type="submit">Send</button>
							</form>
						</div>
				</div>
		</div>
</div>`;

const modalFormSuccess = `
<!--=========================== POPIN FORM SUCCESS ========================-->
<div class="reveal-modal opened" data-reveal="" id="popin-live-shopping-form-success" role="dialog">
		<div>
				<a class="close-reveal-modal" aria-label="Close">Fermer</a>
				<img alt="" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/live-shopping/m-formulaire02.jpg"/>
				<div class="shopping-modal">
						<div>
								<div id="ps-closed">
										<h2 id="modalTitle">Thank you !</h2>
										<p>
												We have received your message ! An advisor will get back to you quickly to finalize the sale !
										</p>
								</div>
						</div>
				</div>
		</div>
</div>`;

modalContainer.insertAdjacentHTML("afterbegin", modalForm + modalFormSuccess);
//==============================//


document.addEventListener("DOMContentLoaded", function(event) {
	//======== Gestion du formulaire ========//
	$(document).on('closed', '[data-reveal]', function () {
			var modal = $(this);
			if (modal.attr('id') === 'popin-live-shopping-form') {
					document.getElementById('liveshopping-form-submit').setAttribute('disabled', 'true');
			 }
	});
	
	var triggerLiveshopping = document.querySelectorAll('.trigger_liveshopping');
	var displayBrand = document.querySelectorAll('.brand');
	for (var i=0; i < triggerLiveshopping.length; i++) {
		triggerLiveshopping[i].addEventListener("click", function(e) {
			//console.log(displayBrand);
			document.getElementById('liveshopping-form-marque').value = clickedBrand || '';
			for (var e=0; e < displayBrand.length; e++) {
				displayBrand[e].innerHTML = clickedBrand;
			}
		});
	}	
	
	function sendData() {
		var XHR = new XMLHttpRequest();
		var FD = new FormData(form);
		XHR.addEventListener("load", function (event) {
			form.reset();
			grecaptcha.reset(recaptchaWidget);
			$('#popin-live-shopping-form').foundation('reveal', 'close');
			$('#popin-live-shopping-form-success').foundation('reveal', 'open');
		});
		XHR.addEventListener("error", function (event) {
			form.reset();
			grecaptcha.reset(recaptchaWidget);
			$('#popin-live-shopping-form').foundation('reveal', 'close');
			$('#popin-live-shopping-form-success').foundation('reveal', 'open');
		});
		XHR.open("POST", "https://docs.google.com/forms/u/0/d/e/1FAIpQLScRe9z6i6MgsY0xvkyVvh1fDV7BMXRjRyKQbcG61hLoc9-RJw/formResponse");
		XHR.send(FD);
	}
	
	var form = document.getElementById("liveshopping-form");
	form.addEventListener("submit", function (event) {
		event.preventDefault();
		sendData();
	});
	//==============================//
});
