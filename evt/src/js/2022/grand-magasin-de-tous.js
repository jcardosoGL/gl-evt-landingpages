window.onbeforeunload = () => {
	window.scrollTo(0, 0);
}

// GESTION DES REVEAL
const threshold = .3
const optionsReveal = {
	root: null,
	rootMargin: '0px',
	threshold
}
const handleIntersect = function (entries, observer) {
	entries.forEach(function (entry) {
		if (entry.intersectionRatio > threshold) {
			entry.target.classList.remove('reveal')
			observer.unobserve(entry.target)
		}
	})
}
//document.documentElement.classList.add('reveal-loaded')
document.querySelector('body').classList.add('reveal-loaded')


window.addEventListener("DOMContentLoaded", function(event) {
	
	const observer = new IntersectionObserver(handleIntersect, optionsReveal)
	const targets = document.querySelectorAll('.reveal')
	targets.forEach(function (target) {
		observer.observe(target)
	})	
	
	//============================================ Smooth scroll ===========================================//
	var smoothAnchor = document.querySelectorAll('.has-smoothscroll');
	for (var i=0; i < smoothAnchor.length; i++) {
		smoothAnchor[i].addEventListener("click", function(e) {
			e.preventDefault();
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);
			if (target) {
				var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 20;
				scroll({
					top: offsetTop,
					behavior: "smooth"
				});
			}
		});
	}
	//======================================================================================================//
	
	//==== ScrollMagic
	//https://github.com/janpaepke/ScrollMagic/wiki/Getting-Started-:-How-to-use-ScrollMagic
	
	controller = new ScrollMagic.Controller();
	
	var revealHeroImage = new ScrollMagic.Scene({
		triggerElement: "body",
		triggerHook: 0, // show, when scrolled 10% into view
		offset: 0 // move trigger to center of element
	})
	.setClassToggle(".hero-image-container .hero_M", "show-image") // add class to reveal
	.addTo(controller);
	
	var revealHeroLogo = new ScrollMagic.Scene({
		triggerElement: "body",
		triggerHook: 0, // show, when scrolled 10% into view
		offset: 0 // move trigger to center of element
	})
	.setClassToggle(".hero_logo a", "show-logo") // add class to reveal
	.addTo(controller);
	
	var revealHeroArrow = new ScrollMagic.Scene({
		triggerElement: "body",
		triggerHook: 0, // show, when scrolled 10% into view
		offset: 0 // move trigger to center of element
	})
	.setClassToggle(".arrow", "show-arrow") // add class to reveal
	.addTo(controller);
	
	var scenePinIntro = new ScrollMagic.Scene({
		triggerElement: "body",
		triggerHook: 0,
		duration: "160px",
		offset: 0,
		pushFollowers: false
	})
	.setPin("#intro")
	//.addIndicators({name: "intro"}) // add indicators (requires plugin)
	.addTo(controller);
	
	var tweenHeroTxt = TweenMax.to(".content-cartouches", 2, {opacity: 0, marginTop: "+=30"});
	var heroTxt = new ScrollMagic.Scene({
		triggerElement: "body",
		triggerHook: 0, 
		duration: "30px", 
		offset: 0
		})
	.setTween(tweenHeroTxt)
	//.addIndicators({name: "heroTxt"})
	.addTo(controller);
	
	var tweenHeroArrow = TweenMax.to(".arrow", 2, {opacity: 0});
	var sceneHeroArrow = new ScrollMagic.Scene({
		triggerElement: "body",
		triggerHook: 0, 
		duration: "30px", 
		offset: 0
		})
	.setTween(tweenHeroArrow)
	//.addIndicators({name: "arrow"})
	.addTo(controller);
		
	var animateHeroImage = document.querySelector(".hero-image-container");
	var tweenHeroImage = TweenMax.to(animateHeroImage, 2, {scale: .38, opacity: 0.3});
	var heroImage = new ScrollMagic.Scene({
		triggerElement: ".great-store-of-all",
		triggerHook: 0, 
		duration: "90px", 
		offset: 70
		})
	.setTween(tweenHeroImage)
	heroImage.on("end", function (event) {
			animateHeroImage.style.opacity = 0;
	})
	//.addIndicators({name: "TweenheroImage"})
	.addTo(controller);
	
	window.addEventListener('resize', function(event){
		revealHeroImage.refresh();
		revealHeroLogo.refresh();
		revealHeroArrow.refresh();
		scenePinIntro.refresh();
		heroTxt.refresh();
		sceneHeroArrow.refresh();
		heroImage.refresh();
	});
	
	//==== Mobile
	var mobileController;
	
	function scrollMagicMobile() {
		mobileController = new ScrollMagic.Controller();
		
		var tweenHeroLogoM = TweenMax.to(".hero_logo", 2, {scale: .45, opacity: 0});
		var heroLogoM = new ScrollMagic.Scene({
			triggerElement: "body",
			triggerHook: 0, 
			duration: "90px", 
			offset: 0
			})
		.setTween(tweenHeroLogoM)
		//.addIndicators({name: "heroLogo"})
		.addTo(mobileController);
		
		window.addEventListener('resize', function(event){
			heroLogoM.refresh();
		});
	}
		
	//==== Desktop
	var desktopController;
	
	function scrollMagicDesktop() {
		desktopController = new ScrollMagic.Controller();
		
		var revealHeroImage = new ScrollMagic.Scene({
			triggerElement: "body",
			triggerHook: 0, // show, when scrolled 10% into view
			offset: 0 // move trigger to center of element
		})
		.setClassToggle(".hero-image-container .hero_D", "show-image") // add class to reveal
		.addTo(desktopController);
		
		var tweenHeroLogo = TweenMax.to(".hero_logo", 2, {scale: .40, top: "-210px"});
		var heroLogo = new ScrollMagic.Scene({
			triggerElement: "body",
			triggerHook: 0, 
			duration: "130px", 
			offset: 0
			})
		.setTween(tweenHeroLogo)
		//.addIndicators({name: "heroLogo"})
		.addTo(desktopController);
		
		var revealButtonPart02 = new ScrollMagic.Scene({
			triggerElement: "body",
			triggerHook: 0, // show, when scrolled 10% into view
			offset: 160 // move trigger to center of element
		})
		.setClassToggle(".button.is-yellow", "show-button") // add class to reveal
		.addTo(desktopController);
		
		var tweenSmallVideo01 = TweenMax.to(".part02 .small-video01", 2, {scale: 1, y: "-=60", opacity: 0.9});
		var smallVideo01 = new ScrollMagic.Scene({
			triggerElement: ".great-store-of-all",
			triggerHook: 0, 
			duration: "70px", 
			offset: 160
			})
		.setTween(tweenSmallVideo01)
		//.addIndicators({name: "smallvid01"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		var animateTxt01 = document.querySelector(".part02 .txt01");
		var tweenTxt01 = TweenMax.to(animateTxt01, 2, {scale: 1, opacity: 1});
		var sceneTxt01 = new ScrollMagic.Scene({
			triggerElement: ".great-store-of-all",
			triggerHook: 0, 
			duration: "160px", 
			offset: 160
			})
		.setTween(tweenTxt01)
		//.addIndicators({name: "txt01"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		var tweenTxt01Out = TweenMax.to(animateTxt01, 2, {scale: .5, opacity: 0});
		var sceneTxt01Out = new ScrollMagic.Scene({
			triggerElement: ".great-store-of-all",
			triggerHook: 0, 
			duration: "100px", 
			offset: 310
			})
		.setTween(tweenTxt01Out)
		//.addIndicators({name: "txt01Out"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		
		
		
		var tweenSmallVideo02 = TweenMax.to(".part02 .small-video02", 2, {scale: .9, y: "-=40", opacity: 0.9});
		var smallVideo02 = new ScrollMagic.Scene({
			triggerElement: ".great-store-of-all",
			triggerHook: 0, 
			duration: "60px", 
			offset: 300
			})
		.setTween(tweenSmallVideo02)
		//.addIndicators({name: "smallvid02"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		var animateTxt02 = document.querySelector(".part02 .txt02");
		var tweenTxt02 = TweenMax.to(animateTxt02, 2, {scale: 1, opacity: 1});
		var sceneTxt02 = new ScrollMagic.Scene({
			triggerElement: ".great-store-of-all",
			triggerHook: 0,
			duration: "160px",
			offset: 400
			})
		.setTween(tweenTxt02)
		//.addIndicators({name: "txt02"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		var tweenTxt02Out = TweenMax.to(animateTxt02, 2, {scale: .5, opacity: 0});
		var sceneTxt02Out = new ScrollMagic.Scene({
			triggerElement: ".great-store-of-all",
			triggerHook: 0, 
			duration: "100px", 
			offset: 550
			})
		.setTween(tweenTxt02Out)
		//.addIndicators({name: "txt02Out"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		
		
		
		var tweenSmallVideo03 = TweenMax.to(".part02 .small-video03", 2, {scale: 1, y: "-=40", opacity: 0.9});
		var smallVideo03 = new ScrollMagic.Scene({
			triggerElement: ".great-store-of-all",
			triggerHook: 0, 
			duration: "80px", 
			offset: 600
			})
		.setTween(tweenSmallVideo03)
		//.addIndicators({name: "smallvid03"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		var animateTxt03 = document.querySelector(".part02 .txt03");
		var tweenTxt03 = TweenMax.to(animateTxt03, 2, {scale: 1, opacity: 1});
		var sceneTxt03 = new ScrollMagic.Scene({
			triggerElement: ".great-store-of-all",
			triggerHook: 0, 
			duration: "160px", 
			offset: 640
			})
		.setTween(tweenTxt03)
		//.addIndicators({name: "txt03"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		
		
		var tweenSmallVideo04 = TweenMax.to(".part02 .small-video04", 2, {scale: 1, y: "-=40", opacity: 0.9});
		var smallVideo04 = new ScrollMagic.Scene({
			triggerElement: ".great-store-of-all",
			triggerHook: 0, 
			duration: "100px", 
			offset: 700
			})
		.setTween(tweenSmallVideo04)
		//.addIndicators({name: "smallvid04"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		
		
		var scenePinPart03 = new ScrollMagic.Scene({
			triggerElement: ".part03",
			triggerHook: 0,
			duration: "200%",
			offset: 0,
			pushFollowers: false
		})
		.setPin(".part03")
		//.addIndicators({name: "pinPart03"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		var tweenBigVideo = TweenMax.to(".part03 .video_wrapper", 2, {scale: 1.35});
		var sceneBigVid = new ScrollMagic.Scene({
			triggerElement: ".part03",
			triggerHook: 0, 
			duration: "80%", 
			offset: 0
			})
		.setTween(tweenBigVideo)
		//.addIndicators({name: "bigVid"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		
		window.addEventListener('resize', function(event){
			revealHeroImage.refresh();
			revealHeroLogo.refresh();
			revealHeroArrow.refresh();
			scenePinIntro.refresh();
			heroTxt.refresh();
			sceneHeroArrow.refresh();
			heroLogo.refresh();
			heroImage.refresh();
			smallVideo01.refresh();
			sceneTxt01.refresh();
			sceneTxt01Out.refresh();
			smallVideo02.refresh();
			sceneTxt02.refresh();
			sceneTxt02Out.refresh();
			smallVideo03.refresh();
			sceneTxt03.refresh();
			smallVideo04.refresh();
			scenePinPart03.refresh();
			sceneBigVid.refresh();
		});
	}
	
	
	
	if("matchMedia" in window) {
		var mqDesktop = window.matchMedia("(min-width:1250px)");
		mqDesktop.addListener(WidthChange);
		WidthChange(mqDesktop);
	}
	
	function WidthChange(mqDesktop) {
		if (mqDesktop.matches) {
			console.log("desktop");
			window.scrollTo(0, 0);
			scrollMagicDesktop();
			
			if (typeof scrollMagicMobile !== 'undefined') {
				scrollMagicMobile.destroy(true);
				scrollMagicMobile = null;
			}
		}
		//==== Mobile
		else {
			console.log("mobile");
			window.scrollTo(0, 0);
			scrollMagicMobile();
			
			if (typeof desktopController !== 'undefined') {
				desktopController.destroy(true);
				desktopController = null;
			}
		}
	}
	
	
	
	
	//======================================================================================================//
	
	
	
	
	// GESTION DES ANCRES	
	// $('.button-anchor_b').on('click', function(evt){
	// 	evt.preventDefault();
	// 	var target = $(this).attr('href');
	// 	$('html, body')
	// 		.stop()
	// 		.animate({scrollTop: $(target).offset().top}, 600 );     
	// });
	
	
});
