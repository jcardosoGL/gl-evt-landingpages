document.addEventListener("DOMContentLoaded", function(event) {
	
	const pageContainer = document.querySelector(".lp-container");
	pageContainer.classList.add('has-js');
		
	//============================================ Smooth scroll ===========================================//
	var smoothAnchor = document.querySelectorAll('.has-smoothscroll');
	for (var i=0; i < smoothAnchor.length; i++) {
		smoothAnchor[i].addEventListener("click", function(e) {
			e.preventDefault();
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);
			if (target) {
				if (window.matchMedia("(min-width:1024px)").matches) {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 100;
				} else {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset;
				}
				scroll({
					top: offsetTop,
					behavior: "smooth"
				});
			}
		});
	}
	//======================================================================================================//

	
	
	
	// =============
	// == Globals ==
	// =============
	const canvas = document.getElementById('graffcanvas');
	const canvasContext = canvas.getContext('2d');
	const clearButton = document.getElementById('clear-button');
	const saveButton = document.getElementById('save-button');
	const state = {
		mousedown: false
	};	
	
	// =================
	// == Show / Hide ==
	// =================
	const startButton = document.getElementById('startgraff');
	const closeButton = document.getElementById('endgraff');
	const defaultContent = document.querySelector(".graff .content");
	const canvasContainer = document.querySelector(".graff .graffcontainer");
	
	startButton.addEventListener("click", function(e) {
		defaultContent.classList.add('is-hidden');
		canvasContainer.classList.remove('is-hidden');
		drawCanvasBackground();
	});
	
	closeButton.addEventListener("click", function(e) {
		defaultContent.classList.remove('is-hidden');
		canvasContainer.classList.add('is-hidden');
		clearCanvas();
	});
		
	// ===================
	// == Configuration ==
	// ===================
	const lineWidth = 15;
	const lineJoin = canvasContext.lineCap = 'round';
	const halfLineWidth = lineWidth / 3;
	const fillStyle = '#FFE816';
	const strokeStyle = '#FFE816';
	const shadowColor = '#FFE816';
	const shadowBlur = 10;
	const density = 50;
	const radius = 22;
	const halfRadius = radius / 2;
	
	// =====================
	// == Event Listeners ==
	// =====================
	canvas.addEventListener('mousedown', handleWritingStart);
	canvas.addEventListener('mousemove', handleWritingInProgress);
	canvas.addEventListener('mouseup', handleDrawingEnd);
	canvas.addEventListener('mouseout', handleDrawingEnd);
	
	canvas.addEventListener('touchstart', handleWritingStart);
	canvas.addEventListener('touchmove', handleWritingInProgress);
	canvas.addEventListener('touchend', handleDrawingEnd);
	
	window.addEventListener('resize', resize);
	
	clearButton.addEventListener('click', handleClearButtonClick);
	saveButton.addEventListener('click', handleSaveButtonClick);	
	
	// ====================
	// == Event Handlers ==
	// ====================	
	
	
	function handleWritingStart(event) {
		event.preventDefault();
	
		const mousePos = getMousePositionOnCanvas(event);
		
		canvasContext.beginPath();
	
		canvasContext.moveTo(mousePos.x, mousePos.y);
		canvasContext.lineWidth = halfLineWidth;
		if (window.matchMedia("(min-width:1025px)").matches) {
			canvasContext.lineWidth = lineWidth;
		}
		canvasContext.lineJoin = lineJoin;
		canvasContext.strokeStyle = strokeStyle;
		canvasContext.shadowColor = shadowColor;
		canvasContext.shadowBlur = shadowBlur;
	
		canvasContext.fill();
		
		state.mousedown = true;
	}
	
	function handleWritingInProgress(event) {
		event.preventDefault();
		
		if (state.mousedown) {
			const mousePos = getMousePositionOnCanvas(event);
			
			for (var i = density; i--; ) {
				var offsetX = getRandomInt(-halfRadius, halfRadius);
				var offsetY = getRandomInt(-halfRadius, halfRadius);
				if (window.matchMedia("(min-width:1025px)").matches) {
					var offsetX = getRandomInt(-radius, radius);
					var offsetY = getRandomInt(-radius, radius);
				}
				canvasContext.fillStyle = fillStyle;
				canvasContext.fillRect(mousePos.x + offsetX, mousePos.y + offsetY, 1, 1);
			}
			
			canvasContext.lineTo(mousePos.x, mousePos.y);
			canvasContext.stroke();
		}
	}
	
	function handleDrawingEnd(event) {
		event.preventDefault();
		
		if (state.mousedown) {
			canvasContext.shadowColor = shadowColor;
			canvasContext.shadowBlur = shadowBlur;
	
			canvasContext.stroke();
		}
		
		state.mousedown = false;
	}
	
	function resize() {
		if (window.matchMedia("(min-width:768px)").matches) {
			canvasContext.canvas.width = 688;
			canvasContext.canvas.height = 600;
		} 
		if (window.matchMedia("(min-width:1025px)").matches) {
			canvasContext.canvas.width = 961;
			canvasContext.canvas.height = 600;
		} 
		if (window.matchMedia("(min-width:1250px)").matches) {
			canvasContext.canvas.width = 1048;
			canvasContext.canvas.height = 600;
		}
		if (window.matchMedia("(min-width:1440px)").matches) {
			canvasContext.canvas.width = 1238;
			canvasContext.canvas.height = 700;
		}
	}
	
	resize();
	
	function handleClearButtonClick(event) {
		event.preventDefault();
		
		clearCanvas();
		drawCanvasBackground();
	}
	
	function handleSaveButtonClick(event) {		
		saveCanvas();
	}
	
	// ======================
	// == Helper Functions ==
	// ======================
	function getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
	
	function getMousePositionOnCanvas(event) {
		const canvasOffsetLeft = canvas.getBoundingClientRect();
		const canvasOffsetTop = canvas.getBoundingClientRect();
		
		const clientX = event.clientX || event.touches[0].clientX;
		const clientY = event.clientY || event.touches[0].clientY;
		const { offsetLeft, offsetTop } = event.target;
		
		const canvasX = clientX - canvasOffsetLeft.left;
		const canvasY = clientY - canvasOffsetTop.top;
		
		return { x: canvasX, y: canvasY };
	}
	
	function clearCanvas() {
		canvasContext.clearRect(0, 0, canvas.width, canvas.height);
	}
	
	function saveCanvas () {
		const dataUrl = canvas.toDataURL();
		saveButton.href = dataUrl;
	}
	
	function drawCanvasBackground() {
		canvasContext.fillStyle = '#f5009b';
		canvasContext.fillRect(0, 0, canvas.width, canvas.height);
		canvasContext.globalCompositeOperation = 'source-over';
	}
	
});
