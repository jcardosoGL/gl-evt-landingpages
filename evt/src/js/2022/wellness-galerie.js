window.addEventListener("DOMContentLoaded", function(event) {
	
	const pageContainer = document.querySelector(".lp-container");
	pageContainer.classList.add('has-js');
	
	//============================================ Slider ===========================================//
	var elemSliderRituals = document.querySelector(".slider-rituals");
	var sliderRituals = new Flickity(elemSliderRituals, {
		// options
		initialIndex: 1,
		cellAlign: "center",
		contain: false, // default :true place la carousel sur la gauche de la page
		percentPosition: false,
		prevNextButtons: true,
		autoPlay: false,
		pageDots: true,
		wrapAround: true, // scroll en boucle
		setGallerySize: false, //pour que la hauteur ne soit pas définie par flickity si on définis déjà la hauteur en css
		watchCSS: true,
		dragThreshold: 15
	});
	// sliderIncontournables.on("scroll", function(progress) {
	// 	bLazy.revalidate();
	// });
	
	//======================================================================================================//
	
});
