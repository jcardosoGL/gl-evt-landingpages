document.addEventListener("DOMContentLoaded", function(event) {
	
	const pageContainer = document.querySelector(".lp-container");
	pageContainer.classList.add('has-js');
	
		
	//============================================ Smooth scroll ===========================================//
	var smoothAnchor = document.querySelectorAll('.has-smoothscroll');
	for (var i=0; i < smoothAnchor.length; i++) {
		smoothAnchor[i].addEventListener("click", function(e) {
			e.preventDefault();
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);
			if (target) {
				if (window.matchMedia("(min-width:1024px)").matches) {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 100;
				} else {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset;
				}
				scroll({
					top: offsetTop,
					behavior: "smooth"
				});
			}
		});
	}
	//======================================================================================================//

	
	//============================================ Sliders ===========================================//
	var elemSliderAigle = document.querySelector(".slider-aigle");
	var sliderAigle = new Flickity(elemSliderAigle, {
		// options
		//initialIndex: 1,
		cellAlign: "left",
		contain: true, // default :true place la carousel sur la gauche de la page
		percentPosition: false,
		prevNextButtons: true,
		autoPlay: false,
		pageDots: true,
		setGallerySize: false, //pour que la hauteur ne soit pas définie par flickity si on définis déjà la hauteur en css
		watchCSS: true,
		dragThreshold: 15
	});
	sliderAigle.on("scroll", function(progress) {
		bLazy.revalidate();
	});
	
	var elemSliderCourreges = document.querySelector(".slider-courreges");
	var sliderCourreges = new Flickity(elemSliderCourreges, {
		// options
		//initialIndex: 1,
		cellAlign: "left",
		contain: true, // default :true place la carousel sur la gauche de la page
		percentPosition: false,
		prevNextButtons: true,
		autoPlay: false,
		pageDots: true,
		setGallerySize: false, //pour que la hauteur ne soit pas définie par flickity si on définis déjà la hauteur en css
		watchCSS: true,
		dragThreshold: 15
	});
	sliderCourreges.on("scroll", function(progress) {
		bLazy.revalidate();
	});
	
	var elemSliderMarcoPolo = document.querySelector(".slider-marc-o-polo");
	var sliderMarcoPolo = new Flickity(elemSliderMarcoPolo, {
		// options
		//initialIndex: 1,
		cellAlign: "left",
		contain: true, // default :true place la carousel sur la gauche de la page
		percentPosition: false,
		prevNextButtons: true,
		autoPlay: false,
		pageDots: true,
		setGallerySize: false, //pour que la hauteur ne soit pas définie par flickity si on définis déjà la hauteur en css
		watchCSS: true,
		dragThreshold: 15
	});
	sliderMarcoPolo.on("scroll", function(progress) {
		bLazy.revalidate();
	});

	
	var elemSliderEngagements = document.querySelector(".slider-engagements");
	var sliderEngagements = new Flickity(elemSliderEngagements, {
		// options
		//initialIndex: 1,
		cellAlign: "left",
		contain: true, // default :true place la carousel sur la gauche de la page
		percentPosition: false,
		prevNextButtons: false,
		autoPlay: false,
		pageDots: false,
		setGallerySize: false, //pour que la hauteur ne soit pas définie par flickity si on définis déjà la hauteur en css
		watchCSS: true,
		dragThreshold: 15
	});
	// sliderIncontournables.on("scroll", function(progress) {
	// 	bLazy.revalidate();
	// });
	
	//======================================================================================================//
	
	
	//======================================= Scroll Magic ======================================//
	//https://github.com/janpaepke/ScrollMagic/wiki/Getting-Started-:-How-to-use-ScrollMagic
	// init controller
	var controller = new ScrollMagic.Controller();
	
	// create a scene - ajoute la class .visible sur les articles
	// toggle class
	var revealElements = document.getElementsByClassName("is-animated");
	for (var i=0; i<revealElements.length; i++) { // create a scene for each element
		var revealArticles = new ScrollMagic.Scene({
			triggerElement: revealElements[i],
			triggerHook: 0.95, // show, when scrolled 70% into view
			//duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
			reverse: true, // only do once
			offset: 0 // move trigger to center of element
		})
		.setClassToggle(revealElements[i], "visible") // add class to reveal
		//.addIndicators({name: "article" + (i+1) }) // add indicators (requires plugin) for debbug
		.addTo(controller);
	}
		
	var animatedArticles = document.querySelectorAll(".selection-2 .selection .image-container");
	for (var i=0; i<animatedArticles.length; i++) { // create a scene for each element
		//var articleImage = ".selection-2 .selection .image-container";
		//console.log(articleImage);
		
		var tweenArticle = TweenMax.to(animatedArticles[i], 1, {y: -60, ease:Power0.easeNone});
		// build scene
		var tweenArticle1Scene = new ScrollMagic.Scene({
			triggerElement: animatedArticles[i],
			triggerHook: 1, // show, when scrolled 70% into view
			duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
			offset: 0 // move trigger to center of element
		})
		.setTween(tweenArticle) // add class to reveal
		//.addIndicators({name: "article" + (i +1) }) // add indicators (requires plugin) for debbug
		.addTo(controller);
	}
	
	//======================================================================================================//
	
	
	
	var desktopController;
	
	function scrollMagicDesktop() {
		desktopController = new ScrollMagic.Controller();
		
		var tweenHeroTitle = TweenMax.to(".lp-hero-video .title", 1, {opacity: 0, y: "80", scale: 1.5});
		// build scene
		var heroTitle = new ScrollMagic.Scene({
			triggerElement: "#header",
			triggerHook: 0, 
			duration: "70%", 
			offset: 0
			})
		.setTween(tweenHeroTitle)
		//.addIndicators({name: "tween title"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		var heroScroller = new TimelineMax();
		heroScroller
			.from(".scroller", 1, {y: '-2.5%', ease:Power0.easeNone})
		;
		var heroScrollerScene = new ScrollMagic.Scene({
			triggerElement: "#header",
			triggerHook: 0, 
			duration: "70%", 
			offset: 0
			})
		.setTween(heroScroller)
		//.addIndicators({name: "scroller"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		var tweenArrondi = TweenMax.to(".arrondi .image", 1, {y: 100, ease:Power0.easeNone});
		// build scene
		var tweenArrondiScene = new ScrollMagic.Scene({
			triggerElement: ".arrondi",
			triggerHook: 1, // show, when scrolled 70% into view
			duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
			offset: 0 // move trigger to center of element
		})
		.setTween(tweenArrondi) // add class to reveal
		//.addIndicators({name: "arrondi" }) // add indicators (requires plugin) for debbug
		.addTo(desktopController);
	}
	
	
	
	
	if("matchMedia" in window) {
		var mq = window.matchMedia("(min-width:1025px)");
		mq.addListener(WidthChange);
		WidthChange(mq);
	}
	
	function WidthChange(mq) {		
		if (mq.matches) {			
			scrollMagicDesktop();
		} else {
			if (typeof desktopController !== 'undefined') {
				desktopController.destroy(true);
				desktopController = null;
			}
			
			document.querySelector(".scroller").style.transform = "";
			
			window.onresize = function() {
				bLazy.revalidate();
			};
		}
	} 
});
