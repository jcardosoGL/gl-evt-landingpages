document.addEventListener("DOMContentLoaded", function(event) {

	var pageContainer = document.querySelector(".grand-jeu-noel");
	var welcomeScreen = document.querySelector(".grand-jeu-noel .welcome");
	var giftScreen = document.querySelector(".grand-jeu-noel .gifts");
	var formScreen = document.querySelector(".grand-jeu-noel .form");
	
	pageContainer.classList.add('has-js');
	welcomeScreen.classList.add('animate');
	
	
	//============================== Hauteur écran ================================//
	// Permet d'avoir toujours au moins 100% de la hauteur de la fenêtre disponible (moins le header)
	var windowHeight = function() {
		var hauteur;
		if (window.matchMedia("(max-width:767px)").matches) {
			if (window.matchMedia("(max-height:639px)").matches) {
				var hauteur = 646;
			} else {
				var hauteur = window.innerHeight - 49;
			}
		}
		if (window.matchMedia("(min-width:768px) and (max-width:1023px)").matches) {
			if (window.matchMedia("(min-width:768px) and (max-height:760px)").matches) {
				var hauteur = 646;
			} else {
				var hauteur = window.innerHeight - 114;
			}
		}
		if (window.matchMedia("(min-width:1024px)").matches) {
			if (window.matchMedia("(max-height:768px)").matches) {
				var hauteur = 646;
			} else {
				var hauteur = window.innerHeight - 162;
			}
		}

		pageContainer.style.height = hauteur + "px";
	}
	windowHeight();
	//===============================================================================//
	
	
	
	//============================= Gestion anim de transition =====================//
	// welcome -> gifts
	var animVidContainer = document.querySelector('.grand-jeu-noel .video-transition');
	var transitionVideo = document.getElementById('transition-vid');
	var triggerAnim = document.querySelector('.trigger_anim');

	var changeScreen = function() {
		welcomeScreen.classList.remove('animate', 'is-transitioning');
		welcomeScreen.classList.add('is-hidden');
		giftScreen.classList.remove('is-hidden');
	}
	// au clic on ajoute une class pour lancer l'anim puis on attends 5sec avant de switcher les écrans en background
	triggerAnim.addEventListener("click", function(e) {
		welcomeScreen.classList.add('is-transitioning');
		animVidContainer.classList.add('animate');
		animVidContainer.classList.remove('is-hidden');
		transitionVideo.play();
		
		setTimeout(changeScreen, 4500);
		setTimeout(function(){ animVidContainer.classList.remove('animate'); transitionVideo.pause(); animVidContainer.classList.add('is-hidden'); }, 6100);
	});
	//================================================================================//
	
	
	//============================= Gestion ouverture formulaire =====================//
	// gifts -> form
	var triggerForm = document.querySelector('.trigger_form');
	var closeForm = document.querySelector('.close_form');

	// au clic on ajoute une class pour activer le formulaire
	triggerForm.addEventListener("click", function(e) {
		formScreen.style.display = "block";
		setTimeout(function() {formScreen.classList.add('is-active');}, 50);
	});
	
	// au clic on ferme le formulaire
	var hideForm = function() {
		formScreen.classList.remove('is-active', 'is-hidding');
		formScreen.style.display = "none";
	}
	closeForm.addEventListener("click", function(e) {
		formScreen.classList.add('is-hidding');
		
		setTimeout(hideForm, 300);
	});
	//================================================================================//
});



//========================= Gestion des cadeaux selon les jours ==================//	
var getGiftOfTheDay = function(currentDay) {
	var request;
	// on récupère les infos dans le json
	var requestURL = 'https://static.galerieslafayette.com/media/LP/src/js/2022/calendrier-de-l-avant-data.json?4';
	// var requestURL = '../../../../src/js/2022/calendrier-de-l-avant-data.json';
	
	//XMLHttpRequest ne marche pas sur ie11 donc dans ce cas on utilise ActiveXObject
	if (window.XMLHttpRequest) {
		request = new XMLHttpRequest();
	} else {
		request = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	function isEmptyObj(object) { 
		//console.log(object);
		for (var key in object) { 
			if (object.hasOwnProperty(key)) { 
				return false; 
			} 
		} 
		return true; 
	} 
	

	request.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var responseData = request.response[currentDay];
			if ( responseData === undefined) {console.log("Joyeux Noël!")}
			else {
				//on récupère les infos dans le json
				var brand 				= responseData[0].brand;
				var product 			= responseData[0].product;
				var goforgood 		= responseData[0].goforgood;
				var description 	= responseData[0].description;
				var value 				= responseData[0].value;
				var srcImage 			= responseData[0].src;
				var srcsetImage 	= responseData[0].srcset;
				
				if (brand == "") {}
				else {
					 brand = brand + " — ";
				}
				
				if (goforgood == true) {
					document.querySelector(".gifts .gift-display").classList.add("is-goforgood");
				}
				else {
					document.querySelector(".gifts .gift-display").classList.remove("is-goforgood");
				}
				
				//on injecte les infos dans le html
				document.querySelector(".gifts .title .brand").innerHTML = brand;
				document.querySelector(".gifts .title .product").innerHTML = product;
				document.querySelector(".gifts .description").innerHTML = description;
				document.querySelector(".gifts .value").innerHTML = value;
				document.querySelector(".gifts .product-img").setAttribute("src", srcImage);
				document.querySelector(".gifts .product-img").setAttribute("srcset", srcsetImage + " 2x");
				document.querySelector(".gifts .product-img").setAttribute("alt", brand + product + " - Galeries Lafayette");
			}        
		}
	};
	request.open('GET', requestURL, true);
	request.responseType = 'json'; //ne marche pas sur ie11
	request.send(null);
	
	return false;
};


// A SUPPRIMER ==========================
// var date = new Date();
// // pour tester un jour précis ===========
// date.setDate(3);
// 
// // pour tester en random ================
// // var randomDay = Math.floor(Math.random() * 24) + 1;
// // date.setDate(randomDay);
// 
// // =============================
// var currentDay = date.getDate();
// console.log(currentDay);
// 
// getGiftOfTheDay(currentDay);
//========================================================================================//

// GET https://sheets.googleapis.com/v4/spreadsheets/1M8S6bTWL7Wkx-KUVU2W2QXLAuJLgPOKnCE7fuaOTxmY/values/A2%3AE?dateTimeRenderOption=FORMATTED_STRING&majorDimension=ROWS&valueRenderOption=FORMATTED_VALUE&key=[YOUR_API_KEY] HTTP/1.1



document.addEventListener('DOMContentLoaded', calendarForm, false);

function calendarForm() {
	const scriptURL = 'https://script.google.com/macros/s/AKfycbw-K68mVexsvsX1QoMMVfxwaogofiAV6Oyh_4R4Zz69wj1eWkS5qzpT1J2_0awK2Khu5Q/exec';
	const form = document.forms['calendar-form'];
	const submitButton = document.getElementById("register-button");
	const giftScreen = document.querySelector(".grand-jeu-noel .gifts.container .gifts");
		
	form.addEventListener('submit', e => {
			e.preventDefault();
			
			giftScreen.classList.add('is-hidden');
			
			submitButton.disabled = true;
			submitButton.value = 'Participation enregistrée...';
			form.classList.add('is-submitting');
			
			
			fetch(scriptURL, { method: 'POST', body: new FormData(form)})
			.then((response) => response.json())
			.then((result) => {
				document.querySelector(".form .form-main").classList.add('is-hidden');
				document.querySelector(".form .form-success").classList.remove('is-hidden');
				//console.log(result);
			})
			.catch((error) => {
				document.querySelector(".errormessage").innerHTML = "Une erreur est survenue !";
				console.log('Une erreur est survenue :', error);
			});
	})
}      
