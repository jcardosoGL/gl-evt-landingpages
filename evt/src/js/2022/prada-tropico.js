document.addEventListener("DOMContentLoaded", function(event) {

	document.querySelector(".lp-container").classList.add('has-js');
		
	//============================================ Smooth scroll ===========================================//
	var smoothAnchor = document.querySelectorAll('.has-smoothscroll');
	for (var i=0; i < smoothAnchor.length; i++) {
		smoothAnchor[i].addEventListener("click", function(e) {
			e.preventDefault();
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);
			if (target) {
				if (window.matchMedia("(min-width:1024px)").matches) {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 100;
				} else {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset;
				}
				scroll({
					top: offsetTop,
					behavior: "smooth"
				});
			}
		});
	}
	//======================================================================================================//
	
	
	//============================================ Slider ===========================================//
	var elemSliderAncres = document.querySelector(".slider-ancres");
	var sliderAncres = new Flickity(elemSliderAncres, {
		// options
		//initialIndex: 1,
		cellAlign: "left",
		contain: true, // default :true place la carousel sur la gauche de la page
		percentPosition: false,
		prevNextButtons: false,
		autoPlay: false,
		pageDots: false,
		setGallerySize: false, //pour que la hauteur ne soit pas définie par flickity si on définis déjà la hauteur en css
		watchCSS: true,
		dragThreshold: 15
	});
	// sliderIncontournables.on("scroll", function(progress) {
	// 	bLazy.revalidate();
	// });
	
	//======================================================================================================//
	
});
