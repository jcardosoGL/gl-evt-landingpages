document.addEventListener("DOMContentLoaded", function(event) {

	document.querySelector(".lp-container").classList.add('has-js');
		
	//============================================ Smooth scroll ===========================================//
	var smoothAnchor = document.querySelectorAll('.has-smoothscroll');
	for (var i=0; i < smoothAnchor.length; i++) {
		smoothAnchor[i].addEventListener("click", function(e) {
			e.preventDefault();
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);
			if (target) {
				if (window.matchMedia("(min-width:1024px)").matches) {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 100;
				} else {
					var offsetTop = target.getBoundingClientRect().top + window.pageYOffset;
				}
				scroll({
					top: offsetTop,
					behavior: "smooth"
				});
			}
		});
	}
	//======================================================================================================//
	
	//======================================= Scroll Magic ======================================//
	//https://github.com/janpaepke/ScrollMagic/wiki/Getting-Started-:-How-to-use-ScrollMagic
	// init controller
	var controller = new ScrollMagic.Controller();
	
	// create a scene - ajoute la class .visible sur les articles
	// toggle class
	var revealElements = document.querySelectorAll(".scrolling-text-container");
	//console.log(revealElements.length);
	for (var i=0; i<revealElements.length; i++) { // create a scene for each element
		var revealArticles = new ScrollMagic.Scene({
			triggerElement: revealElements[i],
			triggerHook: 1, // show, when scrolled 10% into view
			duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
			reverse: true, // only do once
			offset: 0 // move trigger to center of element
		})
		.setClassToggle(revealElements[i], "animate") // add class to reveal
		//.addIndicators({name: "article" + (i+1) }) // add indicators (requires plugin) for debbug
		.addTo(controller);
	}
	//======================================================================================================//
	
	
	var startXperience = document.getElementById('goto-egonlab');
	var fadeoutScreen = document.querySelector('.fadeout-splash');
	startXperience.addEventListener("click", function(e) {
		e.preventDefault();
		var targetId = this.getAttribute('href');
		fadeoutScreen.classList.add('animate');
		var changeScreen = function() {
			window.location = targetId
		}
		setTimeout(changeScreen, 1000);
	});
	
	
	var readMore = document.querySelector('.read-more-content');
	var readMoreTrigger = document.querySelector('.read.more');
	var readLessTrigger = document.querySelector('.read.less');
	readMoreTrigger.onclick = function() {
		this.classList.toggle('is-hidden');
		readMore.classList.toggle('is-hidden');
	};
	readLessTrigger.onclick = function() {
		readMoreTrigger.classList.toggle('is-hidden');
		readMore.classList.toggle('is-hidden');
	};
	
	
	var controller = new ScrollMagic.Controller();
	var heroHeight = document.querySelector(".lp-hero").offsetHeight - 200 + "px";
	var tweenHeroFade = TweenMax.to(".hero-bg-image", 1, {opacity: 0.1, top: 120});
	var heroFade = new ScrollMagic.Scene({
		triggerElement: "#header",
		triggerHook: 0, 
		duration: heroHeight, 
		offset: 0
		})
	.setTween(tweenHeroFade)
	//.addIndicators({name: "yo" })
	.addTo(controller);
	
});
