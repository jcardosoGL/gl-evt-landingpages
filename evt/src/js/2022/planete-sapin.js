window.onbeforeunload = () => {
	window.scrollTo(0, 0);
}

window.addEventListener("DOMContentLoaded", function(event) {
	
	//============================================ Smooth scroll ===========================================//
	var smoothAnchor = document.querySelectorAll('.has-smoothscroll');
	for (var i=0; i < smoothAnchor.length; i++) {
		smoothAnchor[i].addEventListener("click", function(e) {
			e.preventDefault();
			var targetId = this.getAttribute('href');
			var target = document.querySelector(targetId);
			if (target) {
				var offsetTop = target.getBoundingClientRect().top + window.pageYOffset - 20;
				scroll({
					top: offsetTop,
					behavior: "smooth"
				});
			}
		});
	}
	//======================================================================================================//
	
	//============================================ Slider ===========================================//
	var elemSliderAncres = document.querySelector(".slider-links");
	var sliderAncres = new Flickity(elemSliderAncres, {
		// options
		//initialIndex: 1,
		cellAlign: "left",
		contain: true, // default :true place la carousel sur la gauche de la page
		percentPosition: false,
		prevNextButtons: false,
		autoPlay: false,
		pageDots: false,
		setGallerySize: false, //pour que la hauteur ne soit pas définie par flickity si on définis déjà la hauteur en css
		watchCSS: true,
		dragThreshold: 15
	});
	// sliderIncontournables.on("scroll", function(progress) {
	// 	bLazy.revalidate();
	// });
	
	//======================================================================================================//
	
	
	//======================================================================================================//
	
	//==== ScrollMagic
	//https://github.com/janpaepke/ScrollMagic/wiki/Getting-Started-:-How-to-use-ScrollMagic
	
	controller = new ScrollMagic.Controller();
	
	var scenePinHero = new ScrollMagic.Scene({
		triggerElement: ".lp-hero",
		triggerHook: 0,
		duration: "100%",
		offset: 0,
		pushFollowers: false
	})
	.setPin(".lp-hero")
	//.addIndicators({name: "pinHero"}) // add indicators (requires plugin)
	.addTo(controller);
		
	var revealHeroBG = new ScrollMagic.Scene({
		triggerElement: ".lp-hero",
		triggerHook: 0, // show, when scrolled 10% into view
		offset: 0 // move trigger to center of element
	})
	.setClassToggle(".lp-hero-anim", "reveal-anim") // add class to reveal
	.addTo(controller);
	
	var revealHeroLogo = new ScrollMagic.Scene({
		triggerElement: ".lp-hero",
		triggerHook: 0, // show, when scrolled 10% into view
		offset: 0 // move trigger to center of element
	})
	.setClassToggle(".hero_logo", "reveal-anim") // add class to reveal
	.addTo(controller);
	
	var revealHeroArrow = new ScrollMagic.Scene({
		triggerElement: ".lp-logo",
		triggerHook: 0, // show, when scrolled 10% into view
		offset: 0 // move trigger to center of element
	})
	.setClassToggle(".arrow", "show-arrow") // add class to reveal
	.addTo(controller);
		
	var tweenHeroOutNordman = TweenMax.to(".lp-hero-anim-container-nordman", 2, {opacity: 0});
	var heroOutNordman = new ScrollMagic.Scene({
		triggerElement: ".lp-main-content",
		triggerHook: 0.75, 
		duration: "20%", 
		offset: 0
		})
	.setTween(tweenHeroOutNordman)
	//.addIndicators({name: "tweenHeroOutNordman"}) // add indicators (requires plugin)
	.addTo(controller);
	
	var tweenHeroOutLogoPlanete = TweenMax.to(".lp-hero-anim-container-planete", 2, {scale: 0, opacity: 0});
	var heroOutLogoPlanete = new ScrollMagic.Scene({
		triggerElement: ".lp-main-content",
		triggerHook: 0.5, 
		duration: "50%", 
		offset: 0
		})
	.setTween(tweenHeroOutLogoPlanete)
	//.addIndicators({name: "heroOutLogoPlanete"}) // add indicators (requires plugin)
	.addTo(controller);
	
	var tweenHeroOutBg = TweenMax.to(".lp-hero-anim-container-bg", 2, {opacity: 0});
	var heroOutBg = new ScrollMagic.Scene({
		triggerElement: ".lp-main-content",
		triggerHook: 0.25, 
		duration: "40%", 
		offset: 0
		})
	.setTween(tweenHeroOutBg)
	//.addIndicators({name: "tweenHeroBg"}) // add indicators (requires plugin)
	.addTo(controller);
	
	window.addEventListener('resize', function(event){
		scenePinHero.refresh();
		revealHeroBG.refresh();
		revealHeroLogo.refresh();
		revealHeroArrow.refresh();
		heroOutNordman.refresh();
		heroOutLogoPlanete.refresh();
		heroOutBg.refresh();		
	});
	
	//==== Mobile
	var mobileController;
	
	function scrollMagicMobile() {
		mobileController = new ScrollMagic.Controller();
		
		var scenePinHeroLogoMob = new ScrollMagic.Scene({
			triggerElement: ".lp-logo",
			triggerHook: 0,
			duration: "50%",
			offset: 0,
			pushFollowers: false
		})
		.setPin(".lp-logo")
		//.addIndicators({name: "pinLogo"}) // add indicators (requires plugin)
		.addTo(mobileController);
		
		var tweenHeroOutArrowMob = TweenMax.to(".arrow", 2, {opacity: 0});
		var sceneHeroOutArrowMob = new ScrollMagic.Scene({
			triggerElement: ".lp-hero",
			triggerHook: 0, 
			duration: "50%", 
			offset: 0
		})
		.setTween(tweenHeroOutArrowMob)
		//.addIndicators({name: "arrow"})
		.addTo(mobileController);
		
		window.addEventListener('resize', function(event){
			scenePinHeroLogoMob.refresh();
			sceneHeroOutArrowMob.refresh();
		});
	}
		
	//==== Desktop
	var desktopController;
	
	function scrollMagicDesktop() {
		desktopController = new ScrollMagic.Controller();
		
		var scenePinHeroLogo = new ScrollMagic.Scene({
			triggerElement: ".lp-logo",
			triggerHook: 0,
			duration: "500%",
			offset: 0,
			pushFollowers: false
		})
		.setPin(".lp-logo")
		//.addIndicators({name: "pinLogo"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		var tweenHeroOutArrow = TweenMax.to(".arrow", 2, {y: "+=180"});
		var sceneHeroOutArrow = new ScrollMagic.Scene({
			triggerElement: ".lp-main-content",
			triggerHook: 0.5, 
			duration: "50%", 
			offset: 0
			})
		.setTween(tweenHeroOutArrow)
		//.addIndicators({name: "arrow"})
		.addTo(desktopController);
			
		var revealButtonPart02 = new ScrollMagic.Scene({
			triggerElement: ".lp-main-content",
			triggerHook: 0, // show, when scrolled 10% into view
			duration: "240%",
			offset: 320 // move trigger to center of element
		})
		.setClassToggle(".lp-main-content .button.is-gl-pink ", "show-button") // add class to reveal
		.addTo(desktopController);
		
		var scenePinMainContent = new ScrollMagic.Scene({
			triggerElement: ".lp-main-content",
			triggerHook: 0,
			duration: "420%",
			offset: 0,
			pushFollowers: false
		})
		.setPin(".lp-main-content")
		//.addIndicators({name: "pinMainContent"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		var tweenSmallVideo01 = TweenMax.to(".lp-main-content .small-video01", 2, {scale: 1, y: "+=60", opacity: 0.9});
		var smallVideo01 = new ScrollMagic.Scene({
			triggerElement: ".lp-main-content",
			triggerHook: 0, 
			duration: "70px", 
			offset: 10
			})
		.setTween(tweenSmallVideo01)
		//.addIndicators({name: "smallvid01"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		var animateTxt01 = document.querySelector(".lp-main-content .txt01");
		var tweenTxt01 = TweenMax.to(animateTxt01, 2, {scale: 1, opacity: 1});
		var sceneTxt01 = new ScrollMagic.Scene({
			triggerElement: ".lp-main-content",
			triggerHook: 0, 
			duration: "300px", 
			offset: 20
			})
		.setTween(tweenTxt01)
		//.addIndicators({name: "txt01"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		var tweenTxt01Out = TweenMax.to(animateTxt01, 2, {scale: .5, opacity: 0});
		var sceneTxt01Out = new ScrollMagic.Scene({
			triggerElement: ".lp-main-content",
			triggerHook: 0, 
			duration: "100px", 
			offset: 600
			})
		.setTween(tweenTxt01Out)
		//.addIndicators({name: "txt01Out"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		
		
		
		var tweenSmallVideo02 = TweenMax.to(".lp-main-content .small-video02", 2, {scale: .9, y: "+=40", opacity: 0.9});
		var smallVideo02 = new ScrollMagic.Scene({
			triggerElement: ".lp-main-content",
			triggerHook: 0, 
			duration: "60px", 
			offset: 850
			})
		.setTween(tweenSmallVideo02)
		//.addIndicators({name: "smallvid02"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		var animateTxt02 = document.querySelector(".lp-main-content .txt02");
		var tweenTxt02 = TweenMax.to(animateTxt02, 2, {scale: 1, opacity: 1});
		var sceneTxt02 = new ScrollMagic.Scene({
			triggerElement: ".lp-main-content",
			triggerHook: 0,
			duration: "300px",
			offset: 800
			})
		.setTween(tweenTxt02)
		//.addIndicators({name: "txt02"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		var tweenTxt02Out = TweenMax.to(animateTxt02, 2, {scale: .5, opacity: 0});
		var sceneTxt02Out = new ScrollMagic.Scene({
			triggerElement: ".lp-main-content",
			triggerHook: 0, 
			duration: "100px", 
			offset: 1400
			})
		.setTween(tweenTxt02Out)
		//.addIndicators({name: "txt02Out"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		
		
		
		var tweenSmallVideo03 = TweenMax.to(".lp-main-content .small-video03", 2, {scale: 1, y: "-=40", opacity: 0.9});
		var smallVideo03 = new ScrollMagic.Scene({
			triggerElement: ".lp-main-content",
			triggerHook: 0, 
			duration: "80px", 
			offset: 1200
			})
		.setTween(tweenSmallVideo03)
		//.addIndicators({name: "smallvid03"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		var animateTxt03 = document.querySelector(".lp-main-content .txt03");
		var tweenTxt03 = TweenMax.to(animateTxt03, 2, {scale: 1, opacity: 1});
		var sceneTxt03 = new ScrollMagic.Scene({
			triggerElement: ".lp-main-content",
			triggerHook: 0, 
			duration: "160px", 
			offset: 1700
			})
		.setTween(tweenTxt03)
		//.addIndicators({name: "txt03"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		
		
		var tweenSmallVideo04 = TweenMax.to(".lp-main-content .small-video04", 2, {scale: 1, y: "-=40", opacity: 0.9});
		var smallVideo04 = new ScrollMagic.Scene({
			triggerElement: ".lp-main-content",
			triggerHook: 0, 
			duration: "100px", 
			offset: 1850
			})
		.setTween(tweenSmallVideo04)
		//.addIndicators({name: "smallvid04"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		
		
		var scenePinVideoContent = new ScrollMagic.Scene({
			triggerElement: ".lp-main-video",
			triggerHook: 0,
			duration: "200%",
			offset: 0,
			pushFollowers: false
		})
		.setPin(".lp-main-video")
		//.addIndicators({name: "pinMainVideo"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		var tweenBigVideo = TweenMax.to(".lp-main-video .video_wrapper", 2, {scale: 1.35,y: "-=65"});
		var sceneBigVid = new ScrollMagic.Scene({
			triggerElement: ".lp-main-video",
			triggerHook: 0, 
			duration: "80%", 
			offset: 0
			})
		.setTween(tweenBigVideo)
		//.addIndicators({name: "bigVid"}) // add indicators (requires plugin)
		.addTo(desktopController);
		
		
		window.addEventListener('resize', function(event){
			scenePinHeroLogo.refresh();
			sceneHeroOutArrow.refresh();
			revealButtonPart02.refresh();
			scenePinMainContent.refresh();
			smallVideo01.refresh();
			sceneTxt01.refresh();
			sceneTxt01Out.refresh();
			smallVideo02.refresh();
			sceneTxt02.refresh();
			sceneTxt02Out.refresh();
			smallVideo03.refresh();
			sceneTxt03.refresh();
			smallVideo04.refresh();
			scenePinVideoContent.refresh();
			sceneBigVid.refresh();
			
			location.reload(false); // reload la page au resize
		});
	}
	
	
	if("matchMedia" in window) {
		var mqDesktop = window.matchMedia("(min-width:1025px)");
		mqDesktop.addListener(WidthChange);
		WidthChange(mqDesktop);
	}
	
	function WidthChange(mqDesktop) {
		if (mqDesktop.matches) {
			//console.log("desktop");
			window.scrollTo(0, 0);
			scrollMagicDesktop();
			
			if (typeof scrollMagicMobile !== 'undefined') {
				scrollMagicMobile.destroy(true);
				scrollMagicMobile = null;
			}
		}
		//==== Mobile
		else {
			//console.log("mobile");
			window.scrollTo(0, 0);
			scrollMagicMobile();
			
			if (typeof desktopController !== 'undefined') {
				desktopController.destroy(true);
				desktopController = null;
			}
		}
	}
	
	
	
	
	//======================================================================================================//
	
});
