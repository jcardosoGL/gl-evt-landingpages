<?php include ('pages-defaults/header.php'); ?>
<script>
  document.title = "Kenzo coming soon";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../media/LP/src/css/2022/rich-luxe-createurs-old-stack-coming-soon.css" rel="stylesheet" type="text/css"> -->
<!-- ============ HERO IMAGE ============  --> 
<link href="https://static.galerieslafayette.com/media/LP/src/css/2022/rich-luxe-createurs-old-stack-coming-soon.min.v02.css" rel="stylesheet" type="text/css">
<div class="lp-container">
  <section class="richcontent-hero" style="background-color: #e52728;">
    
    <div class="richcontent-hero-edito has-txt-white">
      <div class="luxe-row luxe-center luxe-middle-tablet">
        
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <figure class="inspi-image text-center">
            <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/hero-image/shoppingadistance/kenzo/img-coming-soon.jpg"
              srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/hero-image/shoppingadistance/kenzo/img-coming-soon@2x.jpg 2x" 
              alt="Kenzo Coming Soon - Galeries Lafayette">
          </figure>
        </div>
        <div class="luxe-col-tablet-6">
          <div class="inspi-txt">
            <p>Kenzo <br />coming soon<br />1.06.2022</p>
          </div>
      </div>
    </div>
    
  </section>
</div>

<!-- ============  FIN HERO IMAGE ============ -->

<!-- <script src="https://static.galerieslafayette.com/media/LP/src/js/2021/prada-collection-ss21.min.v01.js"></script> --> 
  
<!-- build:js /media/LP/src/js/2021/journeedelafemme.min.v01.js
  <script src="src/js/2021/journeedelafemme.js"></script>
<!-- endbuild -->
  
<?php include ('pages-defaults/footer.php'); ?>
