
<footer class="footer" data-v-ede4650c data-v-995baa2e>
	<div class="footer-social" data-v-089d7dca data-v-ede4650c>
			<div class="texts" data-v-089d7dca>
					<div class="texts__title" data-v-089d7dca>Toujours au contact de la mode </div>
					<div class="texts__subtitle" data-v-089d7dca>En nous suivant sur Instagram, Facebook ou Pinterest </div>
			</div>

	</div>
	<div class="footer-links" data-v-7f510af2 data-v-ede4650c>
			<div class="links-container" data-v-7f510af2>
					<div class="links-container__title" data-v-7f510af2>Nos Sélections </div>
					<div class="links-container__link-list links-container__link-list--first" data-v-7f510af2>
							<a href="https://www.galerieslafayette.com/c/nouvelles+collections" class="link" data-v-7f510af2>Nouvelle Collection </a>
							<a href="https://www.galerieslafayette.com/c/accessoires" class="link" data-v-7f510af2>Accessoires de mode </a>
							<a href="https://www.galerieslafayette.com/c/accessoires-bijoux" class="link" data-v-7f510af2>Bijoux </a>
							<a href="https://www.galerieslafayette.com/c/accessoires-montres" class="link" data-v-7f510af2>Montres </a>
							<a href="https://www.galerieslafayette.com/c/sacs+et+accessoires" class="link" data-v-7f510af2>Sacs </a>
							<a href="https://www.galerieslafayette.com/c/sacs+et+accessoires-sacs+femme-sacs+a+main" class="link" data-v-7f510af2>Sacs à main </a>
							<a href="https://www.galerieslafayette.com/c/chaussures" class="link" data-v-7f510af2>Chaussures </a>
							<a href="https://www.galerieslafayette.com/magazine/home" class="link" data-v-7f510af2>Actu mode </a>
							<a href="https://www.galerieslafayette.com/c/soldes" class="link" data-v-7f510af2>Soldes </a>
							<a href="https://www.galerieslafayette.com/c/soldes/ct/femme" class="link" data-v-7f510af2>Soldes femme </a>
							<a href="https://www.galerieslafayette.com/c/soldes/ct/homme" class="link" data-v-7f510af2>Soldes homme </a>
							<a href="https://www.galerieslafayette.com/c/noel" class="link" data-v-7f510af2>Idée cadeau Noël </a>
							<a href="https://www.galerieslafayette.com/c/black+friday" class="link" data-v-7f510af2>Black Friday </a>
							<a href="https://www.galerieslafayette.com/offer/boulanger" class="link" data-v-7f510af2>Offre Boulanger </a>
					</div>
			</div>
			<div class="links-container" data-v-7f510af2>
					<div class="links-container__title" data-v-7f510af2>A Propos </div>
					<div class="links-container__link-list links-container__link-list--second" data-v-7f510af2>
							<a href="https://www.galerieslafayette.com/evt/faq/" class="link" data-v-7f510af2>Besoin d'aide ? </a>
							<a href="https://www.galerieslafayette.com/i/nos-magasins/" class="link" data-v-7f510af2>Nos magasins </a>
							<a href="https://www.groupegalerieslafayette.fr/" class="link" data-v-7f510af2>
									Groupe Galeries <br data-v-7f510af2>Lafayette 
							</a>
							<a href="https://www.galerieslafayettechampselysees.com/" class="link link--break" data-v-7f510af2>
									Galeries Lafayette <br data-v-7f510af2>Champs Elysées 
							</a>
							<a href="https://www.laredoute.fr/" class="link" data-v-7f510af2>La Redoute </a>
							<a href="https://www.louispion.fr/" class="link" data-v-7f510af2>Louis Pion </a>
							<a href="https://fr.bazarchic.com/" class="link" data-v-7f510af2>Bazarchic </a>
							<a href="https://www.bhv.fr/" class="link" data-v-7f510af2>BHV </a>
							<a href="https://www.1001listes.fr/?gclid=COPc96O1ktICFYYcGwodhk8Ifw" class="link" data-v-7f510af2>Mille et une listes </a>
							<a href="https://voyages.galerieslafayette.com/#ebp=SmMvxkqDL6S1a502afB.ottgWYrTmtIqqHAPhljxe13a" class="link" data-v-7f510af2>Voyage </a>
							<a href="https://galeries-lafayette-fr.connect.studentbeans.com/fr" class="link" data-v-7f510af2>Réduction étudiante </a>
							<a href="https://www.galerieslafayette.com/cdn-cgi/l/email-protection#40232f2e342123346e2d2132313525330027212c25322925332c21262139253434256e232f2d" class="link" data-v-7f510af2>Devenez vendeur sur galerieslafayette.com </a>
							<a href="https://web.babbler.fr/newsroom/galeries-lafayette" class="link" data-v-7f510af2>Espace presse </a>
							<a href="https://carrieres.groupegalerieslafayette.com/" class="link" data-v-7f510af2>Recrutement </a>
					</div>
			</div>
	</div>
	<div class="footer-general-conditions" data-v-7c945cea data-v-ede4650c>
			<a href="https://www.galerieslafayette.com/service/conditions-generals" class="footer-general-conditions__condition" data-v-7c945cea>Conditions générales de vente </a>
			<a href="https://static.galerieslafayette.com/media/CGU/cgu.pdf" class="footer-general-conditions__condition" data-v-7c945cea>Conditions Générales d'Utilisation du programme de fidélité </a>
			<a class="footer-general-conditions__condition" data-v-7c945cea>Gestion des cookies </a>
			<a href="https://www.galerieslafayette.com/service/service-confidence" class="footer-general-conditions__condition" data-v-7c945cea>Données personnelles / Vie privée </a>
			<a href="https://www.galerieslafayette.com/service/conditions-legal-notice" class="footer-general-conditions__condition" data-v-7c945cea>Mentions légales </a>
	</div>
</footer>
</section>
</section>
<div class="page-loader" style="display:none;" data-v-8c62ac4e data-v-8c62ac4e>
<div class="logo" data-v-8c62ac4e data-v-8c62ac4e>
<div class="logo__overlay logo__overlay--server" data-v-8c62ac4e></div>
<img src="https://www.galerieslafayette.com/ggl-front-assets.logo.094d6478.svg" alt="Galeries Lafayette" data-v-8c62ac4e>
</div>
</div>
</div>
<script data-cfasync="false" src="https://www.galerieslafayette.com/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>

<script src="https://www.galerieslafayette.com/ggl-front-assets.chunk-vendors.c655017b.js" defer></script>
<script src="https://www.galerieslafayette.com/ggl-front-assets.accountOrderDetails~accountOrderReturn~accountOrders~headerAppPage~homePage~landingStoresPage~notFou~73bdb2b0.c655017b.js" defer></script>
<script src="https://www.galerieslafayette.com/ggl-front-assets.productListPage.c655017b.js" defer></script>
<script src="https://www.galerieslafayette.com/ggl-front-assets.chunk-6508a946.c655017b.js" defer></script>
<script src="https://www.galerieslafayette.com/ggl-front-assets.chunk-363bfbf4.c655017b.js" defer></script>
<script src="https://www.galerieslafayette.com/ggl-front-assets.chunk-2d665f02.c655017b.js" defer></script>
<script src="https://www.galerieslafayette.com/ggl-front-assets.chunk-c711f114.c655017b.js" defer></script>
<script src="https://www.galerieslafayette.com/ggl-front-assets.chunk-584e8b65.c655017b.js" defer></script>
<script src="https://www.galerieslafayette.com/ggl-front-assets.chunk-30f626c0.c655017b.js" defer></script>
</body>
</html>