


									
									<section class="columns x-medium-offset-5 xmedium-18 large-offset-4 large-19 footer-description">
									</section>
							 </section>
							 <footer class="footer">
									<hr class=" separator"/>
									<section class="section-newsletter">
										 <form class="newsletter-subscription js-action-newsletter-subscription" data-abide novalidate>
												<fieldset class="newsletter-subscription__fieldset row">
													 <div class="newsletter-subscription__legend small-24 xmedium-12 large-12 columns">
															<legend class=" newsletter-subscription__legend--big">REJOIGNEZ-NOUS !</legend>
															<legend class="newsletter-subscription__legend newsletter-subscription__legend--small">Inscrivez-vous à notre newsletter pour ne rien rater de nos actualités.</legend>
													 </div>
													 <div class="crtls small-24 xmedium-12 large-12 columns end">
															<div class="ctrl-text ctrl-email">
																 <input type="email" class="ctrl-text" placeholder="Entrez votre email" name="email" value="" required="true"/>
																 <small class="error">Email invalide (prenom.nom@example.fr)</small>
															</div>
															<div class="ctrl-submit">
																 <input type="submit" value="OK" aria-invalid="false"/>
															</div>
													 </div>
												</fieldset>
										 </form>
									</section>
									<hr class="separator show-for-xmedium-up"/>
									<section class="section-warranty">
										 <nav class="items-list clearfix js-footer-warranty-equalize-xmedium">
												<div class="line-container-medium clearfix" >
													 <div class="line-container-small clearfix xmedium-up-left large-up-left">
															<div class="item left" data-equalizer-watch="footer-warranty">
																 <picture>
																		<span class="js-scramble-link"
																			 data-target="_self">
																		<img src="//static.galerieslafayette.com/media/endeca2/footer/newpictofev18/livraison.png"/>
																		</span>
																 </picture>
																 <span class="js-scramble-link title-warranty main-title light"
																		data-href="||evt||footer||livraison-et-retour" data-target="_self">
																 Livraison offerte*
																 </span>
																 <p class="description">
																		<span class="js-scramble-link"
																			 data-href="||evt||footer||livraison-et-retour" data-target="_self">
																		- En magasin
																		</span>
																		<br/>
																		<span class="js-scramble-link"
																			 data-href="||evt||footer||livraison-et-retour" data-target="_self">
																		- A domicile ou en point relais dès 75€ d&#39;achat
																		</span>
																		<br/>
																		<span class="js-scramble-link"
																			 data-href="||evt||footer||livraison-et-retour" data-target="_self">
																		*Hors offre partenaire
																		</span>
																		<br/>
																 </p>
															</div>
															<div class="item left" data-equalizer-watch="footer-warranty">
																 <picture>
																		<span class="js-scramble-link"
																			 data-target="_self">
																		<img src="//static.galerieslafayette.com/media/endeca2/footer/newpictofev18/retours.png"/>
																		</span>
																 </picture>
																 <span class="js-scramble-link title-warranty main-title light"
																		data-href="||evt||footer||retour-et-livraison" data-target="_self">
																 Retours offerts*
																 </span>
																 <p class="description">
																		<span class="js-scramble-link"
																			 data-href="||evt||footer||retour-et-livraison" data-target="_self">
																		Satisfait ou remboursé
																		</span>
																		<br/>
																		<span class="js-scramble-link"
																			 data-href="||evt||footer||retour-et-livraison" data-target="_self">
																		pendant 30 jours
																		</span>
																		<br/>
																		<span class="js-scramble-link"
																			 data-target="_self">
																		</span>
																		<br/>
																		<span class="js-scramble-link"
																			 data-href="||evt||footer||retour-et-livraison" data-target="_self">
																		*Hors Offre Partenaire
																		</span>
																		<br/>
																 </p>
															</div>
													 </div>
													 <div class="line-container-small clearfix xmedium-up-left large-up-left">
															<div class="item left" data-equalizer-watch="footer-warranty">
																 <picture>
																		<span class="js-scramble-link"
																			 data-target="_self">
																		<img src="//static.galerieslafayette.com/media/endeca2/footer/newpictofev18/e-resa.png"/>
																		</span>
																 </picture>
																 <span class="js-scramble-link title-warranty main-title light"
																		data-href="||evt||footer||e-reservation" data-target="_self">
																 E-réservation
																 </span>
																 <p class="description">
																		<span class="js-scramble-link"
																			 data-href="||evt||footer||e-reservation" data-target="_self">
																		Réservez gratuitement
																		</span>
																		<br/>
																		<span class="js-scramble-link"
																			 data-href="||evt||footer||e-reservation" data-target="_self">
																		votre article en ligne,
																		</span>
																		<br/>
																		<span class="js-scramble-link"
																			 data-href="||evt||footer||e-reservation" data-target="_self">
																		2h après essayez-le
																		</span>
																		<br/>
																		<span class="js-scramble-link"
																			 data-href="||evt||footer||e-reservation" data-target="_self">
																		en magasin
																		</span>
																		<br/>
																 </p>
															</div>
															<div class="item left" data-equalizer-watch="footer-warranty">
																 <picture>
																		<span class="js-scramble-link"
																			 data-target="_self">
																		<img src="//static.galerieslafayette.com/media/endeca2/footer/newpictofev18/fid.png"/>
																		</span>
																 </picture>
																 <span class="js-scramble-link title-warranty main-title light"
																		data-href="https:||||www.galerieslafayette.com||evt||fid||programme-fidelite" data-target="_self">
																 Mes Galeries programme de fidélité
																 </span>
																 <p class="description">
																		<span class="js-scramble-link"
																			 data-href="https:||||galerieslafayette.com||evt||fid||programme-fidelite" data-target="_self">
																		- Découvrir les avantages
																		</span>
																		<br/>
																		<span class="js-scramble-link"
																			 data-href="https:||||www.galerieslafayette.com||my-account||app||loyalty" data-target="_self">
																		- Adhérer gratuitement
																		</span>
																		<br/>
																		<span class="js-scramble-link"
																			 data-href="https:||||www.galerieslafayette.com||my-account||app||loyalty" data-target="_self">
																		- Accéder à mon compte
																		</span>
																		<br/>
																 </p>
															</div>
													 </div>
												</div>
												<div class="line-container-medium large-up-left">
													 <div class="line-container-small">
															<div class="item left" data-equalizer-watch="footer-warranty">
																 <picture>
																		<span class="js-scramble-link"
																			 data-target="_self">
																		<img src="//static.galerieslafayette.com/media/endeca2/footer/newpictofev18/aide.png"/>
																		</span>
																 </picture>
																 <span class="js-scramble-link title-warranty main-title light"
																		data-href="||evt||faq" data-target="_self">
																 Besoin d&#39;Aide?
																 </span>
																 <p class="description">
																		<span class="js-scramble-link"
																			 data-href="||evt||faq" data-target="_self">
																		Aide et Contact
																		</span>
																		<br/>
																 </p>
															</div>
													 </div>
												</div>
										 </nav>
									</section>
									<hr class="separator"/>
									<section class="section-links">
										 <nav>
												<div class="footer-links-mobile hide-for-xmedium-up">
													 <dl class="accordion" data-accordion data-options="multi_expand:false;">
															<dd class="accordion-navigation links-list">
																 <a href="#footer_links_categories" class="link-item">
																		<span class="text bold-medium-title link-title">LEGAL</span>
																		<div class="sprite">
																			 <div class="icon-accordion"></div>
																		</div>
																 </a>
																 <div id="footer_links_categories" class="content">
																		<ul>
																			 <li class="link-item">
																					<a href="/service/conditions-generals">Conditions générales de vente</a>
																			 </li>
																			 <li class="link-item">
																					<a href="https://static.galerieslafayette.com/media/CGU/cgu.pdf">Conditions Générales d’Utilisation du programme de fidélité</a>
																			 </li>
																			 <li class="link-item">
																					<a href="/service/service-confidence">Gestion des cookies</a>
																			 </li>
																			 <li class="link-item">
																					<a href="/service/service-confidence">Données personnelles / Vie privée</a>
																			 </li>
																			 <li class="link-item">
																					<a href="/service/conditions-legal-notice">Mentions légales</a>
																			 </li>
																		</ul>
																 </div>
															</dd>
															<dd class="accordion-navigation links-list">
																 <a href="#footer_links_services" class="link-item">
																		<span class="text bold-medium-title link-title">A PROPOS</span>
																		<div class="sprite">
																			 <div class="icon-accordion"></div>
																		</div>
																 </a>
																 <div id="footer_links_services" class="content">
																		<ul>
																			 <li class="link-item">
																					<a href="http://www.galerieslafayette.com/i/nos-magasins/" target="_self">
																					Nos magasins
																					</a>
																			 </li>
																			 <li class="link-item">
																					<span class="js-scramble-link"
																						 data-href="https:||||carrieres.groupegalerieslafayette.com||" data-target="_self">
																					Recrutement
																					</span>
																			 </li>
																			 <li class="link-item">
																					<span class="js-scramble-link"
																						 data-href="https:||||fr.bazarchic.com" data-target="_self">
																					Bazarchic
																					</span>
																			 </li>
																			 <li class="link-item">
																					<span class="js-scramble-link"
																						 data-href="https:||||www.1001listes.fr||?gclid=COPc96O1ktICFYYcGwodhk8Ifw" data-target="_self">
																					Mille et une listes
																					</span>
																			 </li>
																			 <li class="link-item">
																					<span class="js-scramble-link"
																						 data-href="http:||||voyages.galerieslafayette.com||#ebp=SmMvxkqDL6S1a502afB.ottgWYrTmtIqqHAPhljxe13a" data-target="_self">
																					Voyage
																					</span>
																			 </li>
																		</ul>
																 </div>
															</dd>
													 </dl>
													 <div class="text-center">
															<ul class="social-list inline-list">
																 <li class="social-item">
																		<span class="js-scramble-link social-link social-fb"
																			 data-href="https:||||www.facebook.com||galerieslafayette" data-target="_blank">
																		</span>
																 </li>
																 <li class="social-item">
																		<span class="js-scramble-link social-link social-insta"
																			 data-href="http:||||instagram.com||galerieslafayette" data-target="_blank">
																		</span>
																 </li>
																 <li class="social-item">
																		<span class="js-scramble-link social-link social-tw"
																			 data-href="https:||||twitter.com||Galeries_Laf" data-target="_blank">
																		</span>
																 </li>
															</ul>
													 </div>
												</div>
												<div class="footer-links-desktop row show-for-xmedium-up">
													 <div class="medium-6 columns">
															<ul class="links-list">
																 <li class="link-item">
																		<span class="link-title">SELECTIONS</span>
																 </li>
																 <li class="link-item">
																		<a href="https://www.galerieslafayette.com/c/nouvelles+collections">Nouvelle Collection</a>
																 </li>
																 <li class="link-item">
																		<a href="/c/accessoires">Accessoires de mode</a>
																 </li>
																 <li class="link-item">
																		<a href="/c/accessoires-bijoux">Bijoux</a>
																 </li>
																 <li class="link-item">
																		<a href="/c/accessoires-montres">Montres</a>
																 </li>
																 <li class="link-item">
																		<a href="/c/sacs+et+accessoires">Sacs</a>
																 </li>
																 <li class="link-item">
																		<a href="/c/sacs+et+accessoires-sacs+femme-sacs+a+main">Sacs à main</a>
																 </li>
																 <li class="link-item">
																		<a href="/c/chaussures">Chaussures</a>
																 </li>
																 <li class="link-item">
																		<a href="/magazine/home">Actu mode</a>
																 </li>
																 <li class="link-item">
																		<a href="/c/soldes">Soldes</a>
																 </li>
																 <li class="link-item">
																		<a href="/c/3j">3J</a>
																 </li>
																 <li class="link-item">
																		<a href="/c/noel">Idée cadeau Noël</a>
																 </li>
																 <li class="link-item">
																		<a href="/c/black+friday">Black Friday</a>
																 </li>
																 <li class="link-item">
																		<a href="/offer/boulanger">Offre Boulanger</a>
																 </li>
																 <li class="link-item">
																		<a href="/c/soldes/ct/homme">Soldes homme</a>
																 </li>
															</ul>
													 </div>
													 <div class="medium-6 columns">
															<ul class="links-list">
																 <li class="link-item">
																		<span class="link-title">LEGAL</span>
																 </li>
																 <li class="link-item">
																		<a href="/service/conditions-generals">Conditions générales de vente</a>
																 </li>
																 <li class="link-item">
																		<a href="https://static.galerieslafayette.com/media/CGU/cgu.pdf">Conditions Générales d’Utilisation du programme de fidélité</a>
																 </li>
																 <li class="link-item">
																		<a href="/service/service-confidence">Gestion des cookies</a>
																 </li>
																 <li class="link-item">
																		<a href="/service/service-confidence">Données personnelles / Vie privée</a>
																 </li>
																 <li class="link-item">
																		<a href="/service/conditions-legal-notice">Mentions légales</a>
																 </li>
															</ul>
													 </div>
													 <div class="medium-6 columns">
															<ul class="links-list">
																 <li class="link-item">
																		<span class="link-title">A PROPOS</span>
																 </li>
																 <li class="link-item">
																		<a href="/c/go+for+good" target="_self">
																		Mode responsable
																		</a>
																 </li>
																 <li class="link-item">
																		<a href="http://www.galerieslafayette.com/i/nos-magasins/" target="_self">
																		Nos magasins
																		</a>
																 </li>
																 <li class="link-item">
																		<a href="http://www.groupegalerieslafayette.fr/" target="_self">
																		Groupe Galeries Lafayette
																		</a>
																 </li>
																 <li class="link-item">
																		<a href="https://www.galerieslafayettechampselysees.com/" target="_self">
																		Galeries Lafayette Champs Elysées
																		</a>
																 </li>
																 <li class="link-item">
																		<a href="https://galeries-lafayette-fr.connect.studentbeans.com/fr" target="_self">
																		Réduction étudiante
																		</a>
																 </li>
																 <li class="link-item">
																		<span class="js-scramble-link"
																			 data-href="https:||||carrieres.groupegalerieslafayette.com||" data-target="_self">
																		Recrutement
																		</span>
																 </li>
																 <li class="link-item">
																		<a href="https://www.laredoute.fr" target="_self">
																		La Redoute
																		</a>
																 </li>
																 <li class="link-item">
																		<span class="js-scramble-link"
																			 data-href="https:||||www.louispion.fr||" data-target="_self">
																		Louis Pion
																		</span>
																 </li>
																 <li class="link-item">
																		<span class="js-scramble-link"
																			 data-href="https:||||fr.bazarchic.com" data-target="_self">
																		Bazarchic
																		</span>
																 </li>
																 <li class="link-item">
																		<span class="js-scramble-link"
																			 data-href="https:||||www.1001listes.fr||?gclid=COPc96O1ktICFYYcGwodhk8Ifw" data-target="_self">
																		Mille et une listes
																		</span>
																 </li>
																 <li class="link-item">
																		<span class="js-scramble-link"
																			 data-href="http:||||voyages.galerieslafayette.com||#ebp=SmMvxkqDL6S1a502afB.ottgWYrTmtIqqHAPhljxe13a" data-target="_self">
																		Voyage
																		</span>
																 </li>
																 <li class="link-item">
																		<span class="js-scramble-link"
																			 data-href="mailto:contact.marques@galerieslafayette.com" data-target="_self">
																		Devenez vendeur sur galerieslafayette.com
																		</span>
																 </li>
																 <li class="link-item">
																		<span class="js-scramble-link"
																			 data-href="http:||||web.babbler.fr||brand||show||galeries-lafayette" data-target="_self">
																		Espace presse
																		</span>
																 </li>
																 <li class="link-item">
																		<a href="https://www.bhv.fr/" target="_self">
																		BHV
																		</a>
																 </li>
															</ul>
													 </div>
													 <div class="medium-6 columns">
															<ul class="links-list">
																 <li class="link-item">
																		<span class="link-title">APPLICATION MOBILE</span>
																 </li>
																 <li class="link-item">
																		<a href="https://itunes.apple.com/fr/app/galeries-lafayette/id427785176?mt=8" target="_self">
																		Télécharger dans l&#39;App Store
																		</a>
																 </li>
																 <li class="link-item">
																		<a href="https://play.google.com/store/apps/details?id=com.galerieslafayette.app" target="_self">
																		Télécharger sur Google Play
																		</a>
																 </li>
																 <li>
																		<ul class="social-list inline-list">
																			 <li class="social-item">
																					<span class="js-scramble-link social-link social-fb"
																						 data-href="https:||||www.facebook.com||galerieslafayette" data-target="_blank">
																					</span>
																			 </li>
																			 <li class="social-item">
																					<span class="js-scramble-link social-link social-insta"
																						 data-href="http:||||instagram.com||galerieslafayette" data-target="_blank">
																					</span>
																			 </li>
																			 <li class="social-item">
																					<span class="js-scramble-link social-link social-tw"
																						 data-href="https:||||twitter.com||Galeries_Laf" data-target="_blank">
																					</span>
																			 </li>
																		</ul>
																 </li>
															</ul>
													 </div>
												</div>
										 </nav>
									</section>
							 </footer>
							 <div class="back-top js-back-top right" style="display: none">
									<a href="#">
										 <div class="icon-top"></div>
									</a>
							 </div>
							 <div id="popinConfirmNewsletter" class="reveal-modal" data-reveal="" role="dialog"></div>
						</div>
				 </div>
			</div>
			<div id="popinQuickShop" class="reveal-modal wrapper-null" data-reveal="" role="dialog"></div>
			<div id="popinAddToCart" class="reveal-modal" data-reveal="" role="dialog"></div>
			<script type="text/javascript" src="//cdn.tagcommander.com/251/tc_GaleriesLafayette_11.js"></script>
			<script type="text/javascript">
				 var GL = GL || {};
				 GL.EVENTS = GL.EVENTS || {};
				 GL.EVENTS.GA = GL.EVENTS.GA || {};
				 
				 GL.EVENTS.GA.getScParam = function() { return '?sc=' + GL.EVENTS.GA.sc; };
				 GL.EVENTS.GA.sc = "lp";
			</script>
			<script type="text/javascript" src="https://www.galerieslafayette.com/js/24.4.0/dynamic.bundle.js"></script>
			<script type="text/javascript" src="https://www.galerieslafayette.com/js/24.4.0/scramble.js"></script>
			<script type="text/javascript">
				 var tc_vars = tc_vars || {};
				 tc_vars = $.extend(true, tc_vars, {"customer_id_ref":"","is_guest":false,"env_language":"fr","page_error":"","optin":false,"last_order_date":null,"Gender":null,"cardholder":false,"env_work":"prod_gl","customer_loggued":"N","user_id":"","page_name":"Programme de fid\u00E9lit\u00E9","hash_email":""});
			</script>
			<!-- <script type="text/javascript" async="async" src="//cdn.tagcommander.com/251/tc_GaleriesLafayette_1.js"></script> -->
			<noscript>
				 <iframe src="//redirect251.tagcommander.com/utils/noscript.php?id=1&amp;mode=iframe" width="1" height="1"></iframe>
			</noscript>
			<div style="display:none;" data-tnr-id="qa-catalog-dynamic-end-of-page"></div>
	 </body>
</html>