<!DOCTYPE html>
<html lang="fr" prefix="og: http://ogp.me/ns#" xmlns="http://www.w3.org/1999/xhtml">
	 <head>
			<meta charset="utf-8" />
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
			<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0" />
			<title>LP</title>
			<meta name="description" content="Programme de fidélité" />
			<meta name="robots" content="index, follow"/>
			<meta name="keywords" content="Programme de fidélité"/>      
			<link rel="shortcut icon" href="https://static.galerieslafayette.com/media/LP/src/img/favicon-test/favicon.ico" type="image/ico" />
			<link rel="stylesheet" type="text/css" href="https://www.galerieslafayette.com/css/24.4.0/dynamic.css" />
			<link rel="stylesheet" type="text/css" href="https://www.galerieslafayette.com/css/24.4.0/font.css" />
			<script type="text/javascript" src="https://www.galerieslafayette.com/js/24.4.0/modernizr.js"></script>
			<script type="text/javascript" src="//cdn.tagcommander.com/251/tc_GaleriesLafayette_10.js"></script>
			<style id="ab684263__css">
				.ab684263 { /* Le bandeau */
						display: flex;
						justify-content: center;
						align-items: center;
						position: fixed;
						top: 0;
						left: 0;
						width: 100vw;
						z-index: 6001;
						font-family: GL_Bader_Narrow-Regular, GLBaderNarrow-Regular, Arial, sans-serif; /* Nom de la typographie */
						font-size: 16px; /* Taille du texte */
						color: #fff; /* Couleur du texte */
						background-color: rgba(163, 136, 92, 1); /* Couleur de fond du bandeau */
						padding: 8px; /* Espacement intérieur du bandeau (entre le contenu et les bords) */
				}
				.ab684263 > a { /* Les liens dans le bandeau */
						font-family: GL_Bader_Narrow-Regular, GLBaderNarrow-Regular, Arial, sans-serif; /* Nom de la typographie */
						font-size: 16px; /* Taille du texte */
						color: #fff; /* Couleur du texte */
						text-decoration: underline; /* sousligné le texte */
						margin: 0 5px; /* Espacement extérieur des liens */
				}
				.ab684263__close { /* Le button de fermeture du bandeau */
						position: fixed;
						right: 10px; /* Position par rapport à la droite */
						top: 5px; /* Position par rapport au haut */
						font-size: 14px; /* Taille de la police */
						padding: 5px; /* Espacement */
						cursor: pointer;
						display: none;
				}
				.ab684263--first-banner {
						margin-top: var(--header-height) !important;
				}
				.ab684263--second-container {
						position: fixed !important;
						top: 0px;
						left: 0px;
						margin-top: var(--banner-height);
				}
				.ab684263--second-header {
						margin-top: var(--banner-height);
						position: fixed;
				}
				.ab684263--second-subheader {
						margin-top: var(--header-height) !important;
				}
				.header__borders {
						display: none !important;
				}
				.ab684263--third-banner {
						margin-top: var(--banner-height) !important;
				}
				.ab684263--middle {
						top: calc(6.0625rem + var(--banner-height)) !important;
				}
				.ab684263--hidden {
						top: var(--banner-height) !important;
				}
				#new-filters.show-filters {
						z-index: 6002;
				}
				#dynamic #overlay {
						z-index: 6002;
				}
				.pswp {
						z-index: 6002;
				}
				html body .ab684263--inline--css {
						margin-top: var(--banner-height) !important;
				}
				html body .ab684263--inline--css--header {
						margin-top: var(--header-height) !important;
				}
		</style>
		
		<style type="text/css">/*!
			 * Copyright (c) 2017 TokBox, Inc.
			 * Released under the MIT license
			 * http://opensource.org/licenses/MIT
			 */
			
			/**
			 * OT Base styles
			 */
			
			/* Root OT object, this is where our CSS reset happens */
			.OT_root,
			.OT_root * {
				color: #ffffff;
				margin: 0;
				padding: 0;
				border: 0;
				font-size: 100%;
				font-family: Arial, Helvetica, sans-serif;
				vertical-align: baseline;
			}
			
			/**
			 * Specific Element Reset
			 */
			
			.OT_root h1,
			.OT_root h2,
			.OT_root h3,
			.OT_root h4,
			.OT_root h5,
			.OT_root h6 {
				color: #ffffff;
				font-family: Arial, Helvetica, sans-serif;
				font-size: 100%;
				font-weight: bold;
			}
			
			.OT_root header {
			
			}
			
			.OT_root footer {
			
			}
			
			.OT_root div {
			
			}
			
			.OT_root section {
			
			}
			
			.OT_root video {
			
			}
			
			.OT_root button {
			
			}
			
			.OT_root strong {
				font-weight: bold;
			}
			
			.OT_root em {
				font-style: italic;
			}
			
			.OT_root a,
			.OT_root a:link,
			.OT_root a:visited,
			.OT_root a:hover,
			.OT_root a:active {
				font-family: Arial, Helvetica, sans-serif;
			}
			
			.OT_root ul, .OT_root ol {
				margin: 1em 1em 1em 2em;
			}
			
			.OT_root ol {
				list-style: decimal outside;
			}
			
			.OT_root ul {
				list-style: disc outside;
			}
			
			.OT_root dl {
				margin: 4px;
			}
			
			.OT_root dl dt,
			.OT_root dl dd {
				float: left;
				margin: 0;
				padding: 0;
			}
			
			.OT_root dl dt {
				clear: left;
				text-align: right;
				width: 50px;
			}
			
			.OT_root dl dd {
				margin-left: 10px;
			}
			
			.OT_root img {
				border: 0 none;
			}
			
			/* Modal dialog styles */
			
			/* Modal dialog styles */
			
			.OT_dialog-centering {
				display: table;
				width: 100%;
				height: 100%;
			}
			
			.OT_dialog-centering-child {
				display: table-cell;
				vertical-align: middle;
			}
			
			.OT_dialog {
				position: relative;
			
				box-sizing: border-box;
				max-width: 576px;
				margin-right: auto;
				margin-left: auto;
				padding: 36px;
				text-align: center; /* centers all the inline content */
			
				background-color: #363636;
				color: #fff;
				box-shadow: 2px 4px 6px #999;
				font-family: 'Didact Gothic', sans-serif;
				font-size: 13px;
				line-height: 1.4;
			}
			
			.OT_dialog * {
				font-family: inherit;
				box-sizing: inherit;
			}
			
			.OT_closeButton {
				color: #999999;
				cursor: pointer;
				font-size: 32px;
				line-height: 36px;
				position: absolute;
				right: 18px;
				top: 0;
			}
			
			.OT_dialog-messages {
				text-align: center;
			}
			
			.OT_dialog-messages-main {
				margin-bottom: 36px;
				line-height: 36px;
			
				font-weight: 300;
				font-size: 24px;
			}
			
			.OT_dialog-messages-minor {
				margin-bottom: 18px;
			
				font-size: 13px;
				line-height: 18px;
				color: #A4A4A4;
			}
			
			.OT_dialog-messages-minor strong {
				color: #ffffff;
			}
			
			.OT_dialog-actions-card {
				display: inline-block;
			}
			
			.OT_dialog-button-title {
				margin-bottom: 18px;
				line-height: 18px;
			
				font-weight: 300;
				text-align: center;
				font-size: 14px;
				color: #999999;
			}
			.OT_dialog-button-title label {
				color: #999999;
			}
			
			.OT_dialog-button-title a,
			.OT_dialog-button-title a:link,
			.OT_dialog-button-title a:active {
				color: #02A1DE;
			}
			
			.OT_dialog-button-title strong {
				color: #ffffff;
				font-weight: 100;
				display: block;
			}
			
			.OT_dialog-button {
				display: inline-block;
			
				margin-bottom: 18px;
				padding: 0 1em;
			
				background-color: #1CA3DC;
				text-align: center;
				cursor: pointer;
			}
			
			.OT_dialog-button:disabled {
				cursor: not-allowed;
				opacity: 0.5;
			}
			
			.OT_dialog-button-large {
				line-height: 36px;
				padding-top: 9px;
				padding-bottom: 9px;
			
				font-weight: 100;
				font-size: 24px;
			}
			
			.OT_dialog-button-small {
				line-height: 18px;
				padding-top: 9px;
				padding-bottom: 9px;
			
				background-color: #444444;
				color: #999999;
				font-size: 16px;
			}
			
			.OT_dialog-progress-bar {
				display: inline-block; /* prevents margin collapse */
				width: 100%;
				margin-top: 5px;
				margin-bottom: 41px;
			
				border: 1px solid #4E4E4E;
				height: 8px;
			}
			
			.OT_dialog-progress-bar-fill {
				height: 100%;
			
				background-color: #29A4DA;
			}
			
			/* Helpers */
			
			.OT_centered {
				position: fixed;
				left: 50%;
				top: 50%;
				margin: 0;
			}
			
			.OT_dialog-hidden {
				display: none;
			}
			
			.OT_dialog-button-block {
				display: block;
			}
			
			.OT_dialog-no-natural-margin {
				margin-bottom: 0;
			}
			
			/* Publisher and Subscriber styles */
			
			.OT_publisher, .OT_subscriber {
				position: relative;
				min-width: 48px;
				min-height: 48px;
			}
			
			.OT_publisher .OT_video-element,
			.OT_subscriber .OT_video-element {
				display: block;
				position: absolute;
				width: 100%;
				height: 100%;
			
				transform-origin: 0 0;
			}
			
			/* Styles that are applied when the video element should be mirrored */
			.OT_publisher.OT_mirrored .OT_video-element {
				transform: scale(-1, 1);
				transform-origin: 50% 50%;
			}
			
			.OT_subscriber_error {
				background-color: #000;
				color: #fff;
				text-align: center;
			}
			
			.OT_subscriber_error > p {
				padding: 20px;
			}
			
			/* The publisher/subscriber name/mute background */
			.OT_publisher .OT_bar,
			.OT_subscriber .OT_bar,
			.OT_publisher .OT_name,
			.OT_subscriber .OT_name,
			.OT_publisher .OT_archiving,
			.OT_subscriber .OT_archiving,
			.OT_publisher .OT_archiving-status,
			.OT_subscriber .OT_archiving-status,
			.OT_publisher .OT_archiving-light-box,
			.OT_subscriber .OT_archiving-light-box {
				-ms-box-sizing: border-box;
				box-sizing: border-box;
				top: 0;
				left: 0;
				right: 0;
				display: block;
				height: 34px;
				position: absolute;
			}
			
			.OT_publisher .OT_bar,
			.OT_subscriber .OT_bar {
				background: rgba(0, 0, 0, 0.4);
			}
			
			.OT_publisher .OT_edge-bar-item,
			.OT_subscriber .OT_edge-bar-item {
				z-index: 1; /* required to get audio level meter underneath */
			}
			
			/* The publisher/subscriber name panel/archiving status bar */
			.OT_publisher .OT_name,
			.OT_subscriber .OT_name {
				background-color: transparent;
				color: #ffffff;
				font-size: 15px;
				line-height: 34px;
				font-weight: normal;
				padding: 0 4px 0 36px;
				letter-spacing: normal;
			}
			
			.OT_publisher .OT_archiving-status,
			.OT_subscriber .OT_archiving-status {
				background: rgba(0, 0, 0, 0.4);
				top: auto;
				bottom: 0;
				left: 34px;
				padding: 0 4px;
				color: rgba(255, 255, 255, 0.8);
				font-size: 15px;
				line-height: 34px;
				font-weight: normal;
				letter-spacing: normal;
			}
			
			.OT_micro .OT_archiving-status,
			.OT_micro:hover .OT_archiving-status,
			.OT_mini .OT_archiving-status,
			.OT_mini:hover .OT_archiving-status {
				display: none;
			}
			
			.OT_publisher .OT_archiving-light-box,
			.OT_subscriber .OT_archiving-light-box {
				background: rgba(0, 0, 0, 0.4);
				top: auto;
				bottom: 0;
				right: auto;
				width: 34px;
				height: 34px;
			}
			
			.OT_archiving-light {
				width: 7px;
				height: 7px;
				border-radius: 30px;
				position: absolute;
				top: 14px;
				left: 14px;
				background-color: #575757;
				box-shadow: 0 0 5px 1px #575757;
			}
			
			.OT_archiving-light.OT_active {
				background-color: #970d13;
				-webkit-animation: OT_pulse 1.3s ease-in;
				-moz-animation: OT_pulse 1.3s ease-in;
				-webkit-animation: OT_pulse 1.3s ease-in;
				-webkit-animation-iteration-count: infinite;
				-moz-animation-iteration-count: infinite;
				-webkit-animation-iteration-count: infinite;
			}
			
			.OT_mini .OT_bar,
			.OT_bar.OT_mode-mini,
			.OT_bar.OT_mode-mini-auto {
				bottom: 0;
				height: auto;
			}
			
			.OT_mini .OT_name.OT_mode-off,
			.OT_mini .OT_name.OT_mode-on,
			.OT_mini .OT_name.OT_mode-auto,
			.OT_mini:hover .OT_name.OT_mode-auto {
				display: none;
			}
			
			.OT_publisher .OT_name,
			.OT_subscriber .OT_name {
					left: 10px;
					right: 37px;
					height: 34px;
					padding-left: 0;
			}
			
			.OT_publisher .OT_mute,
			.OT_subscriber .OT_mute {
					border: none;
					cursor: pointer;
					display: block;
					position: absolute;
					text-align: center;
					text-indent: -9999em;
					background-color: transparent;
					background-repeat: no-repeat;
			}
			
			.OT_publisher .OT_mute,
			.OT_subscriber .OT_mute {
				right: 0;
				top: 0;
				border-left: 1px solid rgba(255, 255, 255, 0.2);
				height: 36px;
				width: 37px;
			}
			
			.OT_mini .OT_mute,
			.OT_publisher.OT_mini .OT_mute.OT_mode-auto.OT_mode-on-hold,
			.OT_subscriber.OT_mini .OT_mute.OT_mode-auto.OT_mode-on-hold {
				top: 50%;
				left: 50%;
				right: auto;
				margin-top: -18px;
				margin-left: -18.5px;
				border-left: none;
			}
			
			.OT_publisher .OT_mute {
				background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAcCAMAAAC02HQrAAAA1VBMVEUAAAD3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pn3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pn3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj3+Pj39/j3+Pj3+Pn4+Pk/JRMlAAAAQ3RSTlMABAUHCQoLDhAQERwdHiAjLjAxOD9ASFBRVl1mbnZ6fH2LjI+QkaWqrrC1uLzAwcXJycrL1NXj5Ofo6u3w9fr7/P3+d4M3+QAAAQFJREFUGBlVwYdCglAABdCLlr5Unijm3hMUtBzlBLSr//9JgUToOcBdYXLgYVLAg4bHgNfAv9KZoXMJkTkDP1dyjj/JMyOXJEIqYypCkndX+iRCkjEJn9LSJGNSaykwaWVE6E0IkbFowqEtGZM2HRi0JWPSpgFlb2cZy9p7BdgsEi7JGwNuYrEBMPvGlJEpvmYAutSKDkNOUWMXQN4zUD0xcKrC8PLwjdiEOljv1wMVTY4QePlkP4VAqs+PV9ylTe6GtffacHc104iULZeka5XxKEcyh2eCpMAzQVIgJvR6u0feeu26LhCoLC+MXZYVAAafGfDpnfFqeyR53K7GHR34BfzvSd9/laOKAAAAAElFTkSuQmCC);
				background-position: 9px 5px;
			}
			
			.OT_publisher .OT_mute.OT_active {
				background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAdCAYAAABFRCf7AAADcElEQVRIiaWVXWhcRRTHf7NNd2aDtUKMIjTpg4ufFIuiUOmDEWm0Vi3VYhXRqIggQh4sWJFSig9+oOhTKSpIRUWMIBIr2kptoTbgU6ooxCiIjR+14kcJmf9sNceHnd3ebnc3Uv9wuXfOzPzmnDMz5zozGwdWAbc65w5RUJQ8cC2wDJgFJioh/MJCMrNxq2vOzK4HmIvRRemxKP0RJWt53o7S+d2Yzsx6gQ+AIUDAnUqpBLzXZd4RYFUlhB/bdZacc3PAOmAcCMC7wfvFwLNdoAPAyx09bXyYWRl4E7gDmAdGlNKFwLYu8GolhO9O87RJd64GbMrgEvB68P4osMWdXLtVV7czlooNpVRWSs8DO7NpR/B+3rBHsvetCgtCMTxwQCm9BbyQrc8F7/uBex3uRCeXO0PrUZ4NfKyUPgWeyj3bg/crDNsIRGwBaJQGorQ3Svdn2wHgc2BUKb0DPJHtjwfvbwRucc7tz+N+i9LFUdoXpfVN36I0CVwBTFI/q9e1LPxT8P4qYEdu70q12mYzWw1MYQzjeJF6zq+shHC4B7jklOBPP/TzSunh4P0DwKvAfb5c9krpe+CcwsEoZdbhEvBM9wxRAl5RShcA9wAngE3B+8tLpdLuwrhp4MNmK0pfRWkySr7NXS8+L5nZbWZWy/Vin1IaitJnUTqvwevJ71lgSSWEFKUfHG7Q2m/xqFJaGry/GXgfGPLl8mJgrXPur2JoUC8Qy3OpG+sAbGhEKT0ErAWOA6uBPWbW1wr9BOgFbgKezot0kAPYqJQA1gC/A9cA+82svzksSn1R+jNKX0SpnM/e1x3yqig92JhrZivM7FjO8bSZLSuCR/Ok16K0KMNHojQWpYko7Y7S1igN5PE3ROl4lNaZ2UVmNpPBU01orvZvZPCeKFXbBR+lEKVtUapFaSZKg9njqpl9aWYTrmXCImA7sCWb9lK/jj9TrwkrgA1AH3AQuKsSwkzbrLfxpgpsBtYDxf/R3xm2ExirhNCuHHZXTsmRwiat+S/zSt06eysVA/4pmGr/G3qm6ik28v29FKgCg8BS6pvS0KNRGgZ+Bb4FpsxsOkfUlMuwDcBWYOUZOHYM2AU8WQmhBifDv70O7PjX7KZ+4G7g3FM8zd6uBIaBy4AqxnIcZwFLCovPAhE4Sj38b4BDwEeVEFKD9S94Khjn486v3QAAAABJRU5ErkJggg==);
				background-position: 9px 4px;
			}
			
			.OT_subscriber .OT_mute {
				background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAATCAYAAAB7u5a2AAABx0lEQVQ4jaWUv48NURiGn3ONmCs32ZBd28ht1gqyZAkF21ylQkEiSp2ehpDlD1BoFGqqVdJohYKI7MaPxMoVNghCWMF+7ybLUewnOXfcMWO9yeQ857zne8+XmZOBGjJpr0kvTIomvTZpS526UCO4DUwD64FjwCFgqZnnR+oc8LfgzKQ73vGsr42ZtGjSQFV9o8KfBCacZwCaef4YmAf2rzjcpN3A2WSpm/AssKcqPDNpDBjs410CViXzTwk/A7b1C4wxDgOngAsZcAXY2buDfp/6S4F3lDS8DjgBzDWAjX/Y/e/QgYS/AhsKHa+OMQ6GEJ4Cj4BOAxgq6aCowyZtdf4OtAr+FHDO+R4wWnVbihr3cQnICt4boO38GWj9a/icjwOACt4m4K3zEPA+AxaAtTWCnwN3lzHkEL8V/OPAGud9wK2GF9XR1Wae/1zG2AI+pGYI4VUIoRtjHAc2A9cz4LRPevYCZ+i9/4sJt4GXJU10gaPAzdI2TTro/5Tfz8XEe2LSZGmxq/SDNvP8BnA5WRrx4BwYBe6vONx1EnjovGvBLAAd4Adwuyq8UiaNmDTvr+a8SQ9MuvbfwckBHZPe+QEfTdpep+4XZmPBHiHgz74AAAAASUVORK5CYII=);
				background-position: 8px 7px;
			}
			
			.OT_subscriber .OT_mute.OT_active {
				background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAUCAYAAACXtf2DAAACtklEQVQ4jZ2VSYiURxTHf+/T9Nc9iRrBuYySmIsXUU9iFMEFERRBvAjJLUQi5ioiHvSScfTmgqC4XAT1ZIgLuJHkICaaQAgKI2hAUBT30bjUq7bbv4eukXK029F3+eqtv/fqK6qQdEnSNUmT6CDB/bvgfjO4N9zj2RD8007xg1IABkwEzkma0qb4PGAPMBZYLtSD8eNwAEjqTlNI0gNJM4YU7w7ut4O7gvuhZFsR3C8NC5BBLiTIY0mzM8AvqbiC++pk+zLpE95XuwAws3vAQuBPYDRwWtL84P4tsDSLv5oaug4EYOawAMF9jMdoLxqNZcDvQA04UVYqL4G/svj7AF21mhJscrvCksYBFO7xc2AAGGg2mrdjvf4rcAyomNn+slLZmUEGBgsYdh945xZJmgvckDSrEJpK6ySBgV6q12O8ABwGPjGzfWWlsjdN9rpjoSfA+DYDXARGAksK4Is3XC1Ub4z1f4CDQGFmu6tleQSYk0U+p7WVeefLJc00s4fAeWB6Qeunvj0m2ugx9gO7kmlrtSxvBfcy6fXUZS6rgG/S+jLQUwCVNmMC9HqM14EtSe+rluWazN8YEv8IqKZ1E1qnaIDO0ucx3gX6kv6TpM3AM+D/IbGjgP60/gq4WQA33gMA2OQxPgHWJX1ttSwL4FAeZGYLgB2SasBs4A8L7qOBf9M0uXQB3a+TMYSmVctyDrA9mfcBK82smSdKWgCcAaa1bTm4fxbc/8uuCQX3RanAD5Ka6Wo5IGnE0HxJPZ03pQX5Org3MsD3AO5xXLPZXJ9BjkrqdFg6QjZkgG3Jtsw93pG0VFI9QU5K6voYQBHcTydAfwheBI9HgvvPAJIWS3qeIL9JGvUxkO7gfi1BrqTvwkG/pPmSnibIqTzXPgAyEVgBjAEu1qrVPbk/PVTHgb/NbPGg/RVIzOQqzSTBaQAAAABJRU5ErkJggg==);
				background-position: 7px 7px;
			}
			
			/**
			 * Styles for display modes
			 *
			 * Note: It's important that these completely control the display and opacity
			 * attributes, no other selectors should atempt to change them.
			 */
			
			/* Default display mode transitions for various chrome elements */
			.OT_publisher .OT_edge-bar-item,
			.OT_subscriber .OT_edge-bar-item {
				transition-property: top, bottom, opacity;
				transition-duration: 0.5s;
				transition-timing-function: ease-in;
			}
			
			.OT_publisher .OT_edge-bar-item.OT_mode-off,
			.OT_subscriber .OT_edge-bar-item.OT_mode-off,
			.OT_publisher .OT_edge-bar-item.OT_mode-auto,
			.OT_subscriber .OT_edge-bar-item.OT_mode-auto,
			.OT_publisher .OT_edge-bar-item.OT_mode-mini-auto,
			.OT_subscriber .OT_edge-bar-item.OT_mode-mini-auto {
				top: -25px;
				opacity: 0;
			}
			
			.OT_publisher .OT_edge-bar-item.OT_mode-off,
			.OT_subscriber .OT_edge-bar-item.OT_mode-off {
				display: none;
			}
			
			.OT_mini .OT_mute.OT_mode-auto,
			.OT_publisher .OT_mute.OT_mode-mini-auto,
			.OT_subscriber .OT_mute.OT_mode-mini-auto {
				top: 50%;
			}
			
			.OT_publisher .OT_edge-bar-item.OT_edge-bottom.OT_mode-off,
			.OT_subscriber .OT_edge-bar-item.OT_edge-bottom.OT_mode-off,
			.OT_publisher .OT_edge-bar-item.OT_edge-bottom.OT_mode-auto,
			.OT_subscriber .OT_edge-bar-item.OT_edge-bottom.OT_mode-auto,
			.OT_publisher .OT_edge-bar-item.OT_edge-bottom.OT_mode-mini-auto,
			.OT_subscriber .OT_edge-bar-item.OT_edge-bottom.OT_mode-mini-auto {
				top: auto;
				bottom: -25px;
			}
			
			.OT_publisher .OT_edge-bar-item.OT_mode-on,
			.OT_subscriber .OT_edge-bar-item.OT_mode-on,
			.OT_publisher .OT_edge-bar-item.OT_mode-auto.OT_mode-on-hold,
			.OT_subscriber .OT_edge-bar-item.OT_mode-auto.OT_mode-on-hold,
			.OT_publisher:hover .OT_edge-bar-item.OT_mode-auto,
			.OT_subscriber:hover .OT_edge-bar-item.OT_mode-auto,
			.OT_publisher:hover .OT_edge-bar-item.OT_mode-mini-auto,
			.OT_subscriber:hover .OT_edge-bar-item.OT_mode-mini-auto {
				top: 0;
				opacity: 1;
			}
			
			.OT_mini .OT_mute.OT_mode-on,
			.OT_mini:hover .OT_mute.OT_mode-auto,
			.OT_mute.OT_mode-mini,
			.OT_root:hover .OT_mute.OT_mode-mini-auto {
				top: 50%;
			}
			
			.OT_publisher .OT_edge-bar-item.OT_edge-bottom.OT_mode-on,
			.OT_subscriber .OT_edge-bar-item.OT_edge-bottom.OT_mode-on,
			.OT_publisher:hover .OT_edge-bar-item.OT_edge-bottom.OT_mode-auto,
			.OT_subscriber:hover .OT_edge-bar-item.OT_edge-bottom.OT_mode-auto {
				top: auto;
				bottom: 0;
				opacity: 1;
			}
			
			
			/* Contains the video element, used to fix video letter-boxing */
			.OT_widget-container {
				position: absolute;
				background-color: #000000;
				overflow: hidden;
			}
			
			.OT_hidden-audio {
				position: absolute !important;
				height: 1px !important;
				width: 1px !important;
			}
			
			/* Load animation */
			.OT_root .OT_video-loading {
				position: absolute;
				z-index: 1;
				width: 100%;
				height: 100%;
				display: none;
			
				background-color: rgba(0, 0, 0, .75);
			}
			
			.OT_root .OT_video-loading .OT_video-loading-spinner {
				background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9Ii0yMCAtMjAgMjQwIDI0MCI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJhIiB4Mj0iMCIgeTI9IjEiPjxzdG9wIG9mZnNldD0iMCIgc3RvcC1jb2xvcj0iI2ZmZiIgc3RvcC1vcGFjaXR5PSIwIi8+PHN0b3Agb2Zmc2V0PSIxIiBzdG9wLWNvbG9yPSIjZmZmIiBzdG9wLW9wYWNpdHk9IjAiLz48L2xpbmVhckdyYWRpZW50PjxsaW5lYXJHcmFkaWVudCBpZD0iYiIgeDE9IjEiIHgyPSIwIiB5Mj0iMSI+PHN0b3Agb2Zmc2V0PSIwIiBzdG9wLWNvbG9yPSIjZmZmIiBzdG9wLW9wYWNpdHk9IjAiLz48c3RvcCBvZmZzZXQ9IjEiIHN0b3AtY29sb3I9IiNmZmYiIHN0b3Atb3BhY2l0eT0iLjA4Ii8+PC9saW5lYXJHcmFkaWVudD48bGluZWFyR3JhZGllbnQgaWQ9ImMiIHgxPSIxIiB4Mj0iMCIgeTE9IjEiPjxzdG9wIG9mZnNldD0iMCIgc3RvcC1jb2xvcj0iI2ZmZiIgc3RvcC1vcGFjaXR5PSIuMDgiLz48c3RvcCBvZmZzZXQ9IjEiIHN0b3AtY29sb3I9IiNmZmYiIHN0b3Atb3BhY2l0eT0iLjE2Ii8+PC9saW5lYXJHcmFkaWVudD48bGluZWFyR3JhZGllbnQgaWQ9ImQiIHgyPSIwIiB5MT0iMSI+PHN0b3Agb2Zmc2V0PSIwIiBzdG9wLWNvbG9yPSIjZmZmIiBzdG9wLW9wYWNpdHk9Ii4xNiIvPjxzdG9wIG9mZnNldD0iMSIgc3RvcC1jb2xvcj0iI2ZmZiIgc3RvcC1vcGFjaXR5PSIuMzMiLz48L2xpbmVhckdyYWRpZW50PjxsaW5lYXJHcmFkaWVudCBpZD0iZSIgeDI9IjEiIHkxPSIxIj48c3RvcCBvZmZzZXQ9IjAiIHN0b3AtY29sb3I9IiNmZmYiIHN0b3Atb3BhY2l0eT0iLjMzIi8+PHN0b3Agb2Zmc2V0PSIxIiBzdG9wLWNvbG9yPSIjZmZmIiBzdG9wLW9wYWNpdHk9Ii42NiIvPjwvbGluZWFyR3JhZGllbnQ+PGxpbmVhckdyYWRpZW50IGlkPSJmIiB4Mj0iMSIgeTI9IjEiPjxzdG9wIG9mZnNldD0iMCIgc3RvcC1jb2xvcj0iI2ZmZiIgc3RvcC1vcGFjaXR5PSIuNjYiLz48c3RvcCBvZmZzZXQ9IjEiIHN0b3AtY29sb3I9IiNmZmYiLz48L2xpbmVhckdyYWRpZW50PjxtYXNrIGlkPSJnIj48ZyBmaWxsPSJub25lIiBzdHJva2Utd2lkdGg9IjQwIj48cGF0aCBzdHJva2U9InVybCgjYSkiIGQ9Ik04Ni42LTUwYTEwMCAxMDAgMCAwMTAgMTAwIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxMDAgMTAwKSIvPjxwYXRoIHN0cm9rZT0idXJsKCNiKSIgZD0iTTg2LjYgNTBBMTAwIDEwMCAwIDAxMCAxMDAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDEwMCAxMDApIi8+PHBhdGggc3Ryb2tlPSJ1cmwoI2MpIiBkPSJNMCAxMDBhMTAwIDEwMCAwIDAxLTg2LjYtNTAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDEwMCAxMDApIi8+PHBhdGggc3Ryb2tlPSJ1cmwoI2QpIiBkPSJNLTg2LjYgNTBhMTAwIDEwMCAwIDAxMC0xMDAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDEwMCAxMDApIi8+PHBhdGggc3Ryb2tlPSJ1cmwoI2UpIiBkPSJNLTg2LjYtNTBBMTAwIDEwMCAwIDAxMC0xMDAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDEwMCAxMDApIi8+PHBhdGggc3Ryb2tlPSJ1cmwoI2YpIiBkPSJNMC0xMDBhMTAwIDEwMCAwIDAxODYuNiA1MCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAwIDEwMCkiLz48L2c+PC9tYXNrPjwvZGVmcz48cmVjdCB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDAlIiB4PSItMjAiIHk9Ii0yMCIgbWFzaz0idXJsKCNnKSIgZmlsbD0iI2ZmZiIvPjwvc3ZnPg==) no-repeat;
				position: absolute;
				width: 32px;
				height: 32px;
				left: 50%;
				top: 50%;
				margin-left: -16px;
				margin-top: -16px;
				animation: OT_spin 2s linear infinite;
			}
			@keyframes OT_spin {
				100% {
					transform: rotate(360deg);
				}
			}
			
			.OT_publisher.OT_loading .OT_video-loading,
			.OT_subscriber.OT_loading .OT_video-loading {
				display: block;
			}
			
			.OT_publisher.OT_loading .OT_video-element,
			.OT_subscriber.OT_loading .OT_video-element {
				/*display: none;*/
			}
			
			.OT_video-centering {
				display: table;
				width: 100%;
				height: 100%;
			}
			
			.OT_video-container {
				display: table-cell;
				vertical-align: middle;
			}
			
			.OT_video-poster {
				position: absolute;
				z-index: 1;
				width: 100%;
				height: 100%;
				display: none;
			
				opacity: .25;
			
				background-repeat: no-repeat;
				background-image: url(data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgNDcxIDQ2NCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48bGluZWFyR3JhZGllbnQgaWQ9ImEiIHgxPSIwIiB4Mj0iMCIgeTE9IjAiIHkyPSIxIj48c3RvcCBvZmZzZXQ9IjY2LjY2JSIgc3RvcC1jb2xvcj0iI2ZmZiIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iI2ZmZiIgc3RvcC1vcGFjaXR5PSIwIi8+PC9saW5lYXJHcmFkaWVudD48cGF0aCBmaWxsPSJ1cmwoI2EpIiBkPSJNNzkgMzA4YzE0LjI1LTYuNSA1NC4yNS0xOS43NSA3MS0yOSA5LTMuMjUgMjUtMjEgMjUtMjFzMy43NS0xMyAzLTIyYy0xLjc1LTYuNzUtMTUtNDMtMTUtNDMtMi41IDMtNC43NDEgMy4yNTktNyAxLTMuMjUtNy41LTIwLjUtNDQuNS0xNi01NyAxLjI1LTcuNSAxMC02IDEwLTYtMTEuMjUtMzMuNzUtOC02Ny04LTY3cy4wNzMtNy4zNDYgNi0xNWMtMy40OC42MzctOSA0LTkgNCAyLjU2My0xMS43MjcgMTUtMjEgMTUtMjEgLjE0OC0uMzEyLTEuMzIxLTEuNDU0LTEwIDEgMS41LTIuNzggMTYuNjc1LTguNjU0IDMwLTExIDMuNzg3LTkuMzYxIDEyLjc4Mi0xNy4zOTggMjItMjItMi4zNjUgMy4xMzMtMyA2LTMgNnMxNS42NDctOC4wODggNDEtNmMtMTkuNzUgMi0yNCA2LTI0IDZzNzQuNS0xMC43NSAxMDQgMzdjNy41IDkuNSAyNC43NSA1NS43NSAxMCA4OSAzLjc1LTEuNSA0LjUtNC41IDkgMSAuMjUgMTQuNzUtMTEuNSA2My0xOSA2Mi0yLjc1IDEtNC0zLTQtMy0xMC43NSAyOS41LTE0IDM4LTE0IDM4LTIgNC4yNS0zLjc1IDE4LjUtMSAyMiAxLjI1IDQuNSAyMyAyMyAyMyAyM2wxMjcgNTNjMzcgMzUgMjMgMTM1IDIzIDEzNUwwIDQ2NHMtMy05Ni43NSAxNC0xMjBjNS4yNS02LjI1IDIxLjc1LTE5Ljc1IDY1LTM2eiIvPjwvc3ZnPg==);
				background-size: auto 76%;
			}
			
			.OT_fit-mode-cover .OT_video-element {
				object-fit: cover;
			}
			
			/* Workaround for iOS freezing issue when cropping videos */
			/* https://bugs.webkit.org/show_bug.cgi?id=176439 */
			@media only screen
				and (orientation: portrait) {
				.OT_subscriber.OT_ForceContain.OT_fit-mode-cover .OT_video-element {
					object-fit: contain !important;
				}
			}
			
			.OT_fit-mode-contain .OT_video-element {
				object-fit: contain;
			}
			
			.OT_fit-mode-cover .OT_video-poster {
				background-position: center bottom;
			}
			
			.OT_fit-mode-contain .OT_video-poster {
				background-position: center;
			}
			
			.OT_audio-level-meter {
				position: absolute;
				width: 25%;
				max-width: 224px;
				min-width: 21px;
				top: 0;
				right: 0;
				overflow: hidden;
			}
			
			.OT_audio-level-meter:before {
				/* makes the height of the container equals its width */
				content: '';
				display: block;
				padding-top: 100%;
			}
			
			.OT_audio-level-meter__audio-only-img {
				position: absolute;
				top: 22%;
				right: 15%;
				width: 40%;
			
				opacity: .7;
			
				background: url(data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgNzkgODYiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0iI2ZmZiI+PHBhdGggZD0iTTkuNzU3IDQwLjkyNGMzLjczOC01LjE5MSAxMi43MTEtNC4zMDggMTIuNzExLTQuMzA4IDIuMjIzIDMuMDE0IDUuMTI2IDI0LjU4NiAzLjYyNCAyOC43MTgtMS40MDEgMS4zMDEtMTEuNjExIDEuNjI5LTEzLjM4LTEuNDM2LTEuMjI2LTguODA0LTIuOTU1LTIyLjk3NS0yLjk1NS0yMi45NzV6bTU4Ljc4NSAwYy0zLjczNy01LjE5MS0xMi43MTEtNC4zMDgtMTIuNzExLTQuMzA4LTIuMjIzIDMuMDE0LTUuMTI2IDI0LjU4Ni0zLjYyNCAyOC43MTggMS40MDEgMS4zMDEgMTEuNjExIDEuNjI5IDEzLjM4LTEuNDM2IDEuMjI2LTguODA0IDIuOTU1LTIyLjk3NSAyLjk1NS0yMi45NzV6Ii8+PHBhdGggZD0iTTY4LjY0NyA1OC42Yy43MjktNC43NTMgMi4zOC05LjU2MSAyLjM4LTE0LjgwNCAwLTIxLjQxMi0xNC4xMTUtMzguNzctMzEuNTI4LTM4Ljc3LTE3LjQxMiAwLTMxLjUyNyAxNy4zNTgtMzEuNTI3IDM4Ljc3IDAgNC41NDEuNTE1IDguOTM2IDEuODAyIDEyLjk1IDEuNjk4IDUuMjk1LTUuNTQyIDYuOTkxLTYuNjE2IDIuMDczQzIuNDEgNTUuMzk0IDAgNTEuNzg3IDAgNDguMTAzIDAgMjEuNTM2IDE3LjY4NSAwIDM5LjUgMCA2MS4zMTYgMCA3OSAyMS41MzYgNzkgNDguMTAzYzAgLjcxOC0yLjg5OSA5LjY5My0zLjI5MiAxMS40MDgtLjc1NCAzLjI5My03Ljc1MSAzLjU4OS03LjA2MS0uOTEyeiIvPjxwYXRoIGQ9Ik01LjA4NCA1MS4zODVjLS44MDQtMy43ODIuNTY5LTcuMzM1IDMuMTM0LTcuOTIxIDIuNjM2LS42MDMgNS40ODUgMi4xNSA2LjI4OSA2LjEzMi43OTcgMy45NDgtLjc1MiA3LjQ1Ny0zLjM4OCA3Ljg1OS0yLjU2Ni4zOTEtNS4yMzctMi4zMTgtNi4wMzQtNi4wN3ptNjguODM0IDBjLjgwNC0zLjc4Mi0uNTY4LTcuMzM1LTMuMTMzLTcuOTIxLTIuNjM2LS42MDMtNS40ODUgMi4xNS02LjI4OSA2LjEzMi0uNzk3IDMuOTQ4Ljc1MiA3LjQ1NyAzLjM4OSA3Ljg1OSAyLjU2NS4zOTEgNS4yMzctMi4zMTggNi4wMzQtNi4wN3ptLTIuMDM4IDguMjg4Yy0uOTI2IDE5LjY1OS0xNS4xMTIgMjQuNzU5LTI1Ljg1OSAyMC40NzUtNS40MDUtLjYwNi0zLjAzNCAxLjI2Mi0zLjAzNCAxLjI2MiAxMy42NjEgMy41NjIgMjYuMTY4IDMuNDk3IDMxLjI3My0yMC41NDktLjU4NS00LjUxMS0yLjM3OS0xLjE4Ny0yLjM3OS0xLjE4N3oiLz48cGF0aCBkPSJNNDEuNjYyIDc4LjQyMmw3LjU1My41NWMxLjE5Mi4xMDcgMi4xMiAxLjE1MyAyLjA3MiAyLjMzNWwtLjEwOSAyLjczOGMtLjA0NyAxLjE4Mi0xLjA1MSAyLjA1NC0yLjI0MyAxLjk0NmwtNy41NTMtLjU1Yy0xLjE5MS0uMTA3LTIuMTE5LTEuMTUzLTIuMDcyLTIuMzM1bC4xMDktMi43MzdjLjA0Ny0xLjE4MiAxLjA1Mi0yLjA1NCAyLjI0My0xLjk0N3oiLz48L2c+PC9zdmc+) no-repeat center;
			}
			
			.OT_audio-level-meter__audio-only-img:before {
				/* makes the height of the container equals its width */
				content: '';
				display: block;
				padding-top: 100%;
			}
			
			.OT_audio-level-meter__value {
				will-change: transform;
				position: absolute;
				top: -100%;
				right: -100%;
				width: 200%;
				height: 200%;
				transform: scale(0);
				border-radius: 50%;
				background-image: radial-gradient(circle, rgba(151, 206, 0, 1) 0%, rgba(151, 206, 0, 0) 100%);
			}
			
			.OT_audio-level-meter.OT_mode-off {
					display: none;
			}
			
			.OT_audio-level-meter.OT_mode-on,
			.OT_audio-only .OT_audio-level-meter.OT_mode-auto {
				display: block;
			}
			
			.OT_audio-only.OT_publisher .OT_video-element,
			.OT_audio-only.OT_subscriber .OT_video-element {
				display: none;
			}
			
			
			.OT_video-disabled-indicator {
				opacity: 1;
				border: none;
				display: none;
				position: absolute;
				background-color: transparent;
				background-repeat: no-repeat;
				background-position: bottom right;
				pointer-events: none;
				top: 0;
				left: 0;
				bottom: 3px;
				right: 3px;
			}
			
			.OT_video-disabled {
				background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFIAAAAoCAYAAABtla08AAAINUlEQVR42u2aaUxUVxTHcRBmAAEBRVTK4sKwDIsg+wCK7CqIw1CN1YobbbS2qYlJ06Qx1UpdqMbYWq2pSzWmH6ytNbXWJY1Lq7VuqBERtW64V0XFLYae0/xvcp3MMAMzDz6IyT/ge2ce5/7ucpY3Ts3NzZ1ygF57AJ0gO0G2jyZPmdbFyclJSAV1EeoEaUUSLGdSV5KLLFxzFmA7QVqGqDqjixhWkxCVeyRVl38wM6bwj6yYItYK47BAuu9B0gCqs6Ng2r494KQtkj/Dz2jHraw6qw2fdSE4rNmcCPCvZONP8iF1I6kdBdMaQJWZLeJqRWa2kPJAxXY+GxE+zxLI03GRh8lGSwoi9WCY8FWlCEh+8JOnT7MfPGjMuXX7Tt61hoaCi/9cKmKdv3BxeEtim/UbNpnbQiqF4MmT7kqrbr4lkMcTo46TTSpJB5g+8NHuVWnWuaampvhmO/7duHmrGluoO4C6OsJZGRrkDIld43ZqUOTnlkDSmXmabAoBU0vqBf+6KgFSxQ9++uzZ8rZApM81TJ8xM5me0Z/UF7PuBmdVdkGEb5gYDeQmyZNW3SJLIP9Kj64lGyMpmxRN6sOfIbkoAhKOdnv2/PmB1kB88eLFo+olyyrps3rSINIAzLonnqlqK8R9w+L86vtrt5L2nhug3Vc3ULu/Liz8AOuXESlZZONH6kmr7gtLIA9lRNeRzVukAvj3BslLnJNKgfScO69K+/Lly0ZbQW7e8tNK+pwBjqaSIjDrXgJkW1ciAZvbQjQ+RDahpBBKd5ZZsqN758hmImk4KQHnpDd8UwSkCyJarx07d4+3BeKJmlMHyX4qaRxpBCmNFE4KENvHDpAutVERn1kCVBMfeRRgYvZnx62wZPdnZkw92VQA5GClQXYRBze2S+iJmpPVVoJLA9l9QKokjcWKTCT1R5rhLg70NuSsziT16diIKkuAjibrTpJNDkn/e17CahtAjlAWJAYkb29Sb1LE9Rs391kILk8mVkyuIpuZcLKUlEmKkra1WuSTNuesEPzwoEploSVAh9Oiz+BIyd9dOHhtx4OEpFpVg6gbNK3yXX1j48N6U5Dz5i/gc/FDrMY3sTLiSMEkXxGxzUEUAGnbxlPaksMlHUXWAlHS8URCPseSohZbCSLjSSU7ixLXdzhIWVKq4Y7t2a/2bN0qGeKly1fYsVmk6RgIDz4J0bonyUOcjeYqm/8hRoYbWkigV2NH9CHAS60EkUkkw47hSRs6FqT1LR5AVcsrueXlK1d5AO+RpmBrZZEiefByytPCanRGNLZY0uF52gNDYr9sCRB8MHY0SJu2OJWKS2WQV65e4y31DmkCImEi0hBfufRime0RIhpbKen0/Ny9OYNW2ghyYytABjNIaxNuKttAWk6HPLn0k0FevdZwFinPWFIuKZbUV16NVko6jbWSDoPO3pOf8K0jQWLSQ0S9bdpkYck+m7vfWpAiHfKgBsZiGSSt0FqcTeU8WETqAHE2CgcAVd3Gkm4MD3xXYeI6B4NMItvKbcUpQ9gP+KMWnSsW+TaYJtoo+avBWLoKoK0CCSDud+7eXWQGZAXqV3YoQjQCfixJ8+fzj9ta3JHhlUeJ8wJOY2ws6eRKpPS3oqTvHAESEz9ya0naXL5WH6pt3FqSOhTHkTcKEXc6k1POh4Q9YJu/03TT4a8PoGMFI4i2EqSbOZAYaBkpCyD92RkG6KCSbjI/H0HEISBnlOZPFdcEzI2GTO4KBZICGKyAKLTEmJOB2txf5MbgohBINCl4FTqmpJMB2W+HiRn1Q2l6lXyPmiEP6VVE2TfGoaMYrHyPdtAnyI0jEOn9RLWmNEhvBBE7SjpFQZaShtLK+1S+T12lRwxUvrZlVPp8jE1PikeO7C/nyEqBDCB1t7+kUx4kKUWclea0yZC5BIGpiJSNSD9QgFR0RQKkL6KxHSWdsiARHJNYewoGrzG1/bk4dTPSunL2EyDjcbb7MQ+lQfZmkKiN7SjpFAM5CWAyGcwyY84YsZ1lUcbRNNtQMAdtQWGvQ0DyVjzYAKQfQFodeAeC1C8vzymXIZqD+ZEh/2OyLSalS/3VbnJZ+VqDXGjMrTCFuK4s66vVZUNfqaDolcbjOcb899sLpEE+I20GifywXe2QR3KElu99PzqjGufhREqB1pjCnG3IL3fY1v733r2FMsiGhutn0LAoJWWIGbPxjKwgjUbF0m52mPhigrpdXOecEq9pR6MkHbu2LOtrcZ9y3d0ODTb15y9MePz48aF79+8fvXnr9sljx2u2I7KNxDuaMPGVECoRs7mC4eT7SIruFNfNHK15MKuM2evwNq+4qjxvGnd5CHwNNynawW4cOlUZdG8b55IIJHmkItwrZHH6QxB3OSL9kTtAGpIvZiQB3Z4SKBfXQtEE9sashWAW87Bt3sYZNR6zn4uzJwWDKUKXfaKCdqUoBpLxSjYe9nqGiwWRBGipuGZ3Qm76itYLbbJI/PEhUApfw73uOIy9xfse3M9F9BuFJHcYrseSouGkHtCVtkuGTTikI8XgZzhg9SeF4VqcvSWiaSvNHQ8JwkNjIfEHemCmNLD1RaEfLs18mlgNuN6PFALHo7CyU5W2g00gFAQF4ozvibH04muwDbWraSFAyt/AAMzewgGR8uCeWn77xzBxPxgzPRCDDMZ14bQ/3jqGKGoHf2Hjgx3kw5LbaJDYWb52t9FMgw4AuWNWukNeuOYqOsmQi2jgws4PA/DD/z0B2x0/veCs4naw0cgybezid7X9jV3rX2RSs0wfLkll4pBGcgifg+NYxe1kJ2ycTaRq66uG/wBOl0vjcw70xwAAAABJRU5ErkJggg==);
				background-size: 33px auto;
			}
			
			.OT_video-disabled-warning {
				background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFIAAAAoCAYAAABtla08AAAGMElEQVR4Ae2aA7D0yBaAc7oH12vbRmlLaxYWb23btm3btm2899a2bWuYtPZ01cmtU9lJrib315yqr9I3Oem/5/s7acwEnehEJzoxCcX2O+wEeIgRBDDaGjAZOgQ6ihRpLklHZDJIXK1WWymMIhGGkVBKCWMM+Iv/f/b5t7faYtM/sGgIS7j8RNLjceUVl41GvGN1BFiHy9sgtRWaYbhvuVQ6o1VOvV5/tLe3dyssKoZuh8xClkDEi2MMS6ZjR0cScxdK/+HgnJsmLccYOx0e/PUGUqfTJDEHkV5go9lcMQoj4R8RpSIRRUr4a9baTJFCCNfqESKJ7RYJibK0xoi05EhFRTxMi1Rit6xHAuLaKRLwEVi6q1x+EhlVpd3d3Wfh4VQkQhRhxthYLg7SRGqdLlIp7UVOHf+JhEhEMscUolVje3p63saeeOFoKsT7fjj++BNuw2I/0ouUENmGaQcQEilQvUU6xuWC0kqmVWCt8df6kG7WLoFA20VSCOyNh0RKPT+SyrTWtQsvuvTYCy84z3+oAdbgAiLGIvHjTz6bFuu/B3lKKfVkFKknwih6EnnipZdfXQZzepAupXSGSCfwUGZtkrx3t/0dSQGnnXbmdocdetArQoj+4VR23wMP3bj/vnv9Sv/rBmkish09ca655thHSrlWq4TFF1vkNDxsgjiUnPqZnHPABIq47jx7pPMcecShfz7x1DO7D6eit99576X1113nVd8rqLGAuDaNitJonTGIqHgQGQjDsJglMrUH5iDSEQbRa6y2yrNvv/PuWVmV/PTzLz8steTit1B9FtGJeZrJksmWdBzBMcami4xUkaY1A1Qe94WIaPGBApJhaERrLrXkElf8+NPPz6YMLs1DDjn0Wn9PnI/UiQadM4jNEkhzVsEGE8nIHESM1j5/KqRX+/IEiOQ/yifNBlEkpnb00cccesbpp13T3983H88/48xzrrvm6it/8U5JXgX5G6nSvSq1R5LATR7aYGkwMG1RSwkWABH+4jUb3vT/uJ1Z0xpjraTBRltrxUQhksIRmgTJyy69+Pv99tv3qYX6FxgU+fU33352xGEHf5wisU7nNWJpZRMkAjZ6aIN1mwV7h29Jo2wCHlveu/GV169z65E+T6koexCh6c+EEiky3lnxQKFjUeVyOeI5AOBzIiayRhJryd7YYnkIHgvB0qk9Tdql6N3XH4bRUIOIIIKJSiRb0hkSEpZKRd1CpEq8GxtIyCVmDSgFl94GacTgaJw1rUlYhYng0c4ewaUsmKRIJjpiqMSOCh9QeI+UYECmtQIsxEu6OorEcv6Rl0gu0woh8MhFkmSCTXVI4pC704WCFRJvSRNJSzrMMEZO2iKZTCHAZYnmvXCny7ed5vfZK3viHSBdIFCKEFj2+nt+73nw8m2uedcLJlktA++VNMEPaR45aYukcKnnCfY3/DFbZS8t7eHxNgsPM0N1hXhJJwwM1QbpoQFlog2R13a/zBxEYHAQEUYUM6qiVwEyBYoM6JFNF2kFLelI5KQf+fVI4dJFCguDS7oAyx2R6SFQJKRedSDj/cMg/RXQ6ZE05GSIDAaXdCi1I3L021SQWNJ1RLY5OiIdL4/yvuw8ADfWPFrSciaMyH8tEQPwf1uGG54g5+KlJGTmsrxsQdl5PKidnPFe2QS///7Hu+VS6WX/HYnf0sevGL7lXydwod2/9DykZq0s5yff0sgSWCigNOH7TPHL7ufj+/TH8P/+qYpL4HkBDiRYpEXeM8/89/9zzjn7EtY64dfd1nqccM7Bs8+9MKy8555/8TnKS+5MufH6EZVASkgPzf+mJXroet17JirU0ALST3nT0y5ONyLpeo1y64ih+vuQfsoTOeRFSJXa+SvyB90TUmdw49EjLaKpMQ0mzEeTzkWsd/oI6fzfiKM8gWg6X6OjpXstu5ZHnmIb0GFiu29MIUfUewkmVrEN3RqVQ/bY8FzNcquMBv/pCNUZ5pHHem01KdN/I/DG66/lLhKSvTO5M84kav5C5z2ZfyAivi9i9VGd45RH7UWJbjwGG/7NYsRECt7jiOToHedKAui8SW4CsxyRc54mKH/8f7ELhCCACyNcIl/wI+FaAJyc8yzRtinQPzWzuFZrFHq/AAAAAElFTkSuQmCC);
				background-size: 33px auto;
			}
			
			.OT_video-disabled-indicator.OT_active {
				display: block;
			}
			
			.OT_audio-blocked-indicator {
				opacity: 1;
				border: none;
				display: none;
				position: absolute;
				background-color: transparent;
				background-repeat: no-repeat;
				background-position: center;
				pointer-events: none;
				top: 0;
				left: 0;
				bottom: 0;
				right: 0;
			}
			
			.OT_audio-blocked {
				background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMTUwIiBoZWlnaHQ9IjkwIj48ZGVmcz48cGF0aCBkPSJNNjcgMTJMNi40NDggNzIuNTUyIDAgMzFWMThMMjYgMGw0MSAxMnptMyA3bDYgNDctMjkgMTgtMzUuNTAyLTYuNDk4TDcwIDE5eiIgaWQ9ImEiLz48L2RlZnM+PHJlY3Qgd2lkdGg9IjE1MCIgaGVpZ2h0PSI5MCIgcng9IjM1IiByeT0iNDUiIG9wYWNpdHk9Ii41Ii8+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgzNikiPjxtYXNrIGlkPSJiIiBmaWxsPSIjZmZmIj48dXNlIHhsaW5rOmhyZWY9IiNhIi8+PC9tYXNrPjxwYXRoIGQ9Ik0zOS4yNDkgNTEuMzEyYy42OTcgMTAuMzcgMi43ODUgMTcuODk3IDUuMjUxIDE3Ljg5NyAzLjAzOCAwIDUuNS0xMS40MTcgNS41LTI1LjVzLTIuNDYyLTI1LjUtNS41LTI1LjVjLTIuNTEgMC00LjYyOCA3Ljc5Ny01LjI4NyAxOC40NTNBOC45ODkgOC45ODkgMCAwMTQzIDQ0YTguOTg4IDguOTg4IDAgMDEtMy43NTEgNy4zMTJ6TTIwLjk4NSAzMi4yMjRsMTUuNzQ2LTE2Ljg3N2E3LjM4NSA3LjM4NSAwIDAxMTAuMzc0LS40MkM1MS43MDIgMTkuMTE0IDU0IDI5LjIwOCA1NCA0NS4yMDhjMCAxNC41MjctMi4zNDMgMjMuODgtNy4wMyAyOC4wNThhNy4yOCA3LjI4IDAgMDEtMTAuMTY4LS40NjhMMjAuNDA1IDU1LjIyNEgxMmE1IDUgMCAwMS01LTV2LTEzYTUgNSAwIDAxNS01aDguOTg1eiIgZmlsbD0iI0ZGRiIgbWFzaz0idXJsKCNiKSIvPjwvZz48cGF0aCBkPSJNMTA2LjUgMTMuNUw0NC45OTggNzUuMDAyIiBzdHJva2U9IiNGRkYiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIi8+PC9nPjwvc3ZnPg==);
				background-size: 90px auto;
			}
			
			.OT_container-audio-blocked {
				cursor: pointer;
			}
			
			.OT_container-audio-blocked.OT_mini .OT_edge-bar-item {
				display: none;
			}
			
			.OT_container-audio-blocked .OT_mute {
				display: none;
			}
			
			.OT_audio-blocked-indicator.OT_active {
				display: block;
			}
			
			.OT_video-unsupported {
				opacity: 1;
				border: none;
				display: none;
				position: absolute;
				background-color: transparent;
				background-repeat: no-repeat;
				background-position: center;
				background-image: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iOTciIGhlaWdodD0iOTAiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxkZWZzPjxwYXRoIGQ9Ik03MCAxMkw5LjQ0OCA3Mi41NTIgMCA2MmwzLTQ0TDI5IDBsNDEgMTJ6bTggMmwxIDUyLTI5IDE4LTM1LjUwMi02LjQ5OEw3OCAxNHoiIGlkPSJhIi8+PC9kZWZzPjxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoOCAzKSI+PG1hc2sgaWQ9ImIiIGZpbGw9IiNmZmYiPjx1c2UgeGxpbms6aHJlZj0iI2EiLz48L21hc2s+PHBhdGggZD0iTTkuMTEgMjAuOTY4SDQ4LjFhNSA1IDAgMDE1IDVWNTguMThhNSA1IDAgMDEtNSA1SDkuMTFhNSA1IDAgMDEtNS01VjI1Ljk3YTUgNSAwIDAxNS01em00Ny4wOCAxMy4zOTRjMC0uMzQ1IDUuNDcyLTMuMTU5IDE2LjQxNS04LjQ0M2EzIDMgMCAwMTQuMzA0IDIuNzAydjI2LjgzNWEzIDMgMCAwMS00LjMwNSAyLjcwMWMtMTAuOTQyLTUuMjg2LTE2LjQxMy04LjEtMTYuNDEzLTguNDQ2VjM0LjM2MnoiIGZpbGw9IiNGRkYiIG1hc2s9InVybCgjYikiLz48L2c+PHBhdGggZD0iTTgxLjUgMTYuNUwxOS45OTggNzguMDAyIiBzdHJva2U9IiNGRkYiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIi8+PC9nPjwvc3ZnPg==);
				background-size: 58px auto;
				pointer-events: none;
				top: 0;
				left: 0;
				bottom: 0;
				right: 0;
				margin-top: -30px;
			}
			
			.OT_video-unsupported-bar {
				display: none;
				position: absolute;
				width: 192%; /* copy the size of the audio meter bar for symmetry */
				height: 192%;
				top: -96% /* half of the size */;
				left: -96%;
				border-radius: 50%;
			
				background-color: rgba(0, 0, 0, .8);
			}
			
			.OT_video-unsupported-img {
				display: none;
				position: absolute;
				top: 11%;
				left: 15%;
				width: 70%;
				opacity: .7;
				background-image: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iOTciIGhlaWdodD0iOTAiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxkZWZzPjxwYXRoIGQ9Ik03MCAxMkw5LjQ0OCA3Mi41NTIgMCA2MmwzLTQ0TDI5IDBsNDEgMTJ6bTggMmwxIDUyLTI5IDE4LTM1LjUwMi02LjQ5OEw3OCAxNHoiIGlkPSJhIi8+PC9kZWZzPjxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoOCAzKSI+PG1hc2sgaWQ9ImIiIGZpbGw9IiNmZmYiPjx1c2UgeGxpbms6aHJlZj0iI2EiLz48L21hc2s+PHBhdGggZD0iTTkuMTEgMjAuOTY4SDQ4LjFhNSA1IDAgMDE1IDVWNTguMThhNSA1IDAgMDEtNSA1SDkuMTFhNSA1IDAgMDEtNS01VjI1Ljk3YTUgNSAwIDAxNS01em00Ny4wOCAxMy4zOTRjMC0uMzQ1IDUuNDcyLTMuMTU5IDE2LjQxNS04LjQ0M2EzIDMgMCAwMTQuMzA0IDIuNzAydjI2LjgzNWEzIDMgMCAwMS00LjMwNSAyLjcwMWMtMTAuOTQyLTUuMjg2LTE2LjQxMy04LjEtMTYuNDEzLTguNDQ2VjM0LjM2MnoiIGZpbGw9IiNGRkYiIG1hc2s9InVybCgjYikiLz48L2c+PHBhdGggZD0iTTgxLjUgMTYuNUwxOS45OTggNzguMDAyIiBzdHJva2U9IiNGRkYiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIi8+PC9nPjwvc3ZnPg==);
				background-repeat: no-repeat;
				background-position: center;
				background-size: 100% auto;
			}
			
			.OT_video-unsupported-img:before {
				/* makes the height of the container 93% of its width (90/97 px) */
				content: '';
				display: block;
				padding-top: 93%;
			}
			
			.OT_video-unsupported-text {
				display: flex;
				justify-content: center;
				align-items: center;
				text-align: center;
				height: 100%;
				margin-top: 40px;
			}
			
			.OT_video-unsupported.OT_active {
				display: block;
			}
			
			.OT_mini .OT_video-unsupported,
			.OT_micro .OT_video-unsupported {
				position: absolute;
				width: 25%;
				max-width: 224px;
				min-width: 21px;
				top: 0;
				left: 0;
				overflow: hidden;
				background: none;
				bottom: auto;
				right: auto;
				margin: auto;
			}
			
			.OT_mini .OT_video-unsupported:before,
			.OT_micro .OT_video-unsupported:before {
				/* makes the height of the container equals its width */
				content: '';
				display: block;
				padding-top: 100%;
			}
			
			.OT_mini .OT_video-unsupported-bar,
			.OT_micro .OT_video-unsupported-bar,
			.OT_mini .OT_video-unsupported-img,
			.OT_micro .OT_video-unsupported-img {
				display: block;
			}
			
			.OT_mini .OT_video-unsupported-text,
			.OT_micro .OT_video-unsupported-text {
				display: none;
			}
			
			.OT_hide-forced {
				display: none;
			}
			
			.OT_ModalDialog {
				position: fixed;
				top: 0px;
				left: 0px;
				right: 0px;
				bottom: 0px;
				z-index: 1000;
				background-color: rgba(0, 0, 0, 0.2);
			}
			
			#tb_alert {
				position: fixed;
				width: 570px;
				font-family: "Lucida Grande", Arial;
				top: 50%;
				left: 50%;
				margin-top: -75px;
				margin-left: -285px;
			}
			
			#tb_alert * {
				box-sizing: border-box;
			}
			
			#tb_alert .OT_alert {
				border-top-right-radius: 5px;
				border-bottom-right-radius: 5px;
				background-color: #333;
				position: relative;
				padding: 30px;
				overflow: visible;
				margin-left: 70px;
				border-bottom-color: rgba(255, 255, 255, 0.1);
				border-left-color: rgba(255, 255, 255, 0.1);
			
				background-image: linear-gradient(45deg, rgba(0, 0, 0, 0.2) 25%, transparent 25%, transparent 75%, rgba(0, 0, 0, 0.2) 75%, rgba(0, 0, 0, 0.2)), linear-gradient(45deg, rgba(0, 0, 0, 0.2) 25%, transparent 25%, transparent 75%, rgba(0, 0, 0, 0.2) 75%, rgba(0, 0, 0, 0.2));
			
				background-image: -ms-linear-gradient(45deg, rgba(0, 0, 0, 0.2) 25%, transparent 25%, transparent 75%, rgba(0, 0, 0, 0.2) 75%, rgba(0, 0, 0, 0.2)), -ms-linear-gradient(45deg, rgba(0, 0, 0, 0.2) 25%, transparent 25%, transparent 75%, rgba(0, 0, 0, 0.2) 75%, rgba(0, 0, 0, 0.2));
			
				background-size: 4px 4px;
				background-position: 0 0, 2px 2px;
				box-shadow: 0 2px 2px rgba(0, 0, 0, 0.15);
				border: 1px solid rgba(255, 255, 255, 0.3);
			}
			
			#tb_alert .OT_alert-exclamation {
				background-color: #9c1408;
				background-image: linear-gradient(0deg, #dd0c0a 0%, #9c1408 100%);
				box-shadow: 0 2px 2px rgba(0, 0, 0, 0.15);
				border: 1px solid rgba(255, 255, 255, 0.3);
			
				margin: -1px 0;
			
				font-family: MS Trebuchet;
				font-weight: bold;
				font-size: 60px;
				text-shadow: -1px -1px 2px rgba(0, 0, 0, 0.5);
			
				color: white;
				width: 65px;
				position: absolute;
				padding: 42px 0;
				text-align: center;
				left: -70px;
				top: 0;
				bottom: 0;
				border-top-left-radius: 5px;
				border-bottom-left-radius: 5px;
			
				border-bottom-color: transparent;
				border-left: none;
				border-right: none;
			}
			
			#tb_alert .OT_alert-exclamation:after,
			#tb_alert .OT_alert-exclamation:before {
				content: " ";
				position: absolute;
				right: -24px;
				top: 40%;
				border: 12px solid transparent;
				border-left-color: #9c1408;
			}
			
			#tb_alert .OT_alert-exclamation:before {
				border-left-color: #bc3428;
				top: 39%;
			}
			
			#tb_alert .OT_alert-body {
				color: #c8c5c5;
				margin: 0;
				text-shadow: 0 2px 0 rgba(0, 0, 0, 0.3);
				font-size: 14px;
				line-height: 1.3em;
			}
			
			#tb_alert .continue-text {
				margin-top: 12px
			}
			
			#tb_alert > * a {
				color: #a7d8df;
				text-decoration: none;
			}
			
			#tb_alert .OT_alert-header {
				font-size: 24px;
				padding-bottom: 5px;
				color: #aaa;
				font-weight: bold;
				position: relative;
				text-shadow: 0 2px 0 rgba(0, 0, 0, 0.3);
				margin: 0;
			}
			
			#tb_alert .OT_alert-header::before {
				content: attr(data-text);
				position: absolute;
				left: 0;
				color: white;
				-webkit-mask-image: -webkit-gradient(
					linear,
					left top, left bottom,
					from(rgba(0, 0, 0, 1)),
					color-stop(60%, rgba(0, 0, 0, 0)),
					to(rgba(0, 0, 0, 0))
				);
			}
			
			#tb_alert .OT_alert-close-button {
				position: absolute;
				right: 8px;
				top: 5px;
				background-color: #000;
				color: #666;
				border: none;
				font-size: 20px;
				/** Hack to bring it up to the proper line top */
				line-height: 14px;
				padding: 0;
				padding-bottom: 3px;
				cursor: pointer;
			}
			
			#tb_alert #section-mobile-browser,
			#tb_alert #section-supported-mobile-browser {
				width: 200px;
				top: 0px;
				left: 25%;
				margin-top: 0;
				margin-left: 0;
			}
			
			@media all and (max-height: 300px) {
				#tb_alert {
					width: 100%;
					height: 100%;
					left: 0;
					top: 0;
					margin-left: 0;
					margin-top: 0;
				}
				#tb_alert #section-mobile-browser,
				#tb_alert #section-supported-mobile-browser {
					margin-left: 0;
					margin-top: 10px;
					left: 0;
				}
			}
			
			#tb_alert #section-normal-install,
			#tb_alert #section-upgrade-install,
			#tb_alert #section-mobile-browser,
			#tb_alert #section-supported-mobile-browser {
				display: none;
			}
			</style>
			
		
	 </head>
	 <body>
			<div class="ab684263 is-hidden">Événement : Le Live shopping avec The Kooples, Iro, Ganni... <a href="https://www.galerieslafayette.com/leliveshopping" target="_blank">S'inscrire</a><div class="ab684263__close">x</div></div>
			<header data-js="header" class="ab684263--first-banner large">
				
				 <div class="row header header--1250">
						<div class="hide-for-xmedium-up header__burgerMenu">
							 <div class="header__burgerMenu__button" data-js="burger-menu-action">
									<div class="bar bar--1"></div>
									<div class="bar bar--3"></div>
									<div class="title">Menu</div>
							 </div>
						</div>
						<div class="header__mag-wrapper">
							 <div class="header__mag">
									<span class="js-scramble-link header-mag-link"
										 data-href="||i||nos-magasins||" data-target="_self" data-ga="gaEvent|gaEvent_action:Trouver mon magasin|gaEvent_category:Header|gaEvent_label:trouver mon magasin">
									<img class="header-picto" src="https://www.galerieslafayette.com/img/sprites/svg/Ac22_StoreIcon.svg" />
									<span class="picto__map__label">Trouver un magasin</span>
									</span>
							 </div>
						</div>
						<div class="header__logo">
							 <span class="js-scramble-link"
									data-href="||" data-target="_self">
							 <img src="https://www.galerieslafayette.com/img/common/logo-galeries-lafayette.svg" alt="Galeries Lafayette" />
							 </span>
						</div>
						<div class="header__searchAccountAndCart">
							 <div class="header__account" data-js="account">
									<span class="js-scramble-link header-picto-wrapper header-picto-link not-connected"
										 data-href="||my-account||app" data-target="_self" data-ga="gaEvent|gaEvent_action:Mon compte|gaEvent_category:Header|gaEvent_label:connexion">
									<span class="header-picto-label">Se connecter</span>
									<img class="header-picto" src="https://www.galerieslafayette.com/img/sprites/svg/Ac14_AccountIcon.svg"/>
									</span>
							 </div>
							 <div class="cart" data-js="refreshMinicart">
									<div class="header-picto-wrapper cart__visual">
										 <span class="js-scramble-link header-cart-link header-picto-link"
												data-href="||cart" data-target="_self">
										 <span class="header-picto-label label-full">Panier (0)</span>
										 <span class="header-picto-label label-partial">(0)</span>
										 <img class="header-picto" src="https://www.galerieslafayette.com/img/sprites/svg/Ac15_CartIcon.svg" />
										 </span>
									</div>
							 </div>
						</div>
				 </div>
				 <div class="menu-wrapper">
					 <div class="menu-wrapper__content row">
					 <div class="menu columns small-24 xmedium-18" data-js="menu">
					 <div class="menu-wrapper__nav ">
					 <ul class="menu__lvl lvl1">
					 <li class="lvl1__item" data-js="lvl1__item">
					 <a class="js-scramble-link lvl1--link" href="/c/nouveautes" data-js="enable-lvl2" target="_self" data-lvl1="nouveautés" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés">
					 <span class="lvl1__item__label lvl1__item__label--title picto__arrow--after ">Nouveautés</span>
					 </a>
					 <span class="lvl1__item--center" data-js="lvl1SubElement">
					 <span class="item--1250" style="width: 1363px;">
					 <ul class="menu__lvl lvl2 lvl2--withRightBorder" data-js="lvl2">
					 <li class="show-for-medium-only lvl2__item lvl2--category hide-for-xmedium-up ">
					 <a class="js-scramble-link" href="/c/nouveautes" target="_self">
					 <span class="medium show-for-medium-only">Tout l'univers Nouveautés</span>
					 </a>
					 </li>
					 <li class="lvl2__item show-for-small-only">
					 <a href="#" data-js="backToLvl1" class="lvl2--link">
					 <span class="lvl2__item__label picto__arrow--before picto__arrow--before--left">Retour</span>
					 </a>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-1">
					 <a class="js-scramble-link lvl2--link" href="/c/nouveautes/ct/femme" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Femme">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Femme</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/nouveautes/ct/femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Femme">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-1-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes/ct/femme-vetements" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Femme-Vêtements">
					 <span>Vêtements</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-1-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes/ct/femme-lingerie" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Femme-Lingerie">
					 <span>Lingerie</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-1-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes/ct/femme-chaussures" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Femme-Chaussures">
					 <span>Chaussures</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-1-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes/ct/femme-sacs+et+bagages" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Femme-Sacs et Bagages">
					 <span>Sacs et Bagages</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-1-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes/ct/femme-accessoires" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Femme-Accessoires">
					 <span>Accessoires</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-1-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes/ct/femme-bijouterie+et+joaillerie" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Femme-Bijouterie">
					 <span>Bijouterie</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-2">
					 <a class="js-scramble-link lvl2--link" href="/c/nouveautes/ct/homme" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Homme">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Homme</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/nouveautes/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Homme">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-2-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes/ct/homme-vetements" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Homme-Vêtements">
					 <span>Vêtements</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-2-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes/ct/homme-sous-vetements+et+homewear" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Homme-Sous-vêtements">
					 <span>Sous-vêtements</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-2-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes/ct/homme-chaussures" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Homme-Chaussures">
					 <span>Chaussures</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-2-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes/ct/homme-sacs+et+bagages" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Homme-Sacs et Bagages">
					 <span>Sacs et Bagages</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-2-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes/ct/homme-accessoires" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Homme-Accessoires">
					 <span>Accessoires</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-2-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes/ct/homme-montres" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Homme-Montres">
					 <span>Montres</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-3">
					 <a class="js-scramble-link lvl2--link" href="/c/nouveautes-nouveautes+enfant" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Enfant">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Enfant</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/nouveautes-nouveautes+enfant" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Enfant">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-3-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes-nouveautes+enfant/ct/fille" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Enfant-Fille">
					 <span>Fille</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-3-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes-nouveautes+enfant/ct/garcon" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Enfant-Garçon">
					 <span>Garçon</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-3-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes-nouveautes+enfant/ct/bebe" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Enfant-Bébé">
					 <span>Bébé</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-4">
					 <a class="js-scramble-link lvl2--link" href="/c/nouveautes/ct/maison/tri/nouveautes" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Maison">
						<span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Maison</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/nouveautes/ct/maison/tri/nouveautes" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Maison">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-4-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/offer/boulanger/c/nouveautes/ct/maison/tri/nouveautes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Maison-Electro &amp; Multimédia">
					 <span>Electro &amp; Multimédia</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-4-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes/ct/maison-meubles+et+canapes/tri/nouveautes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Maison-Meubles">
					 <span>Meubles</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-4-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes/ct/maison-arts+de+la+table/tri/nouveautes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Maison-Arts de la table">
					 <span>Arts de la table</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-5">
					 <a class="js-scramble-link lvl2--link" href="/c/nouveautes/ct/beaute" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Beauté">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Beauté</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/nouveautes/ct/beaute" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Beauté">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-5-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes/ct/beaute-parfum" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Beauté-Parfum">
					 <span>Parfum</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-5-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes/ct/beaute-soin" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Beauté-Soin">
					 <span>Soin</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-5-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/nouveautes/ct/beaute-maquillage" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Beauté-Maquillage">
					 <span>Maquillage</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-6">
					 <a class="js-scramble-link lvl2--link" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Nouvelles marques">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Nouvelles marques</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Nouvelles marques">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-6-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/b/alyx" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Nouvelles marques-Alyx">
					 <span>Alyx</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-6-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/b/bellerose" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Nouvelles marques-Bellerose">
					 <span>Bellerose</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-6-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/b/bonton" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Nouvelles marques-Bonton">
					 <span>Bonton</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-6-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/b/cire+trudon" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Nouvelles marques-Cire Trudon">
					 <span>Cire Trudon</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-6-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/b/comme+des+garcons+play" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Nouvelles marques-Comme des garçons PLAY">
					 <span>Comme des garçons PLAY</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-6-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/b/diptyque" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Nouvelles marques-Diptyque">
					 <span>Diptyque</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-6-sub-qa-catalog-dynamic-menu-level3-7">
					 <a class="js-scramble-link lvl3--link" href="/b/isabel+marant" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Nouvelles marques-Isabel Marant">
					 <span>Isabel Marant</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-6-sub-qa-catalog-dynamic-menu-level3-8">
					 <a class="js-scramble-link lvl3--link" href="/b/jo+malone+london" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Nouvelles marques-Jo Malone London">
					 <span>Jo Malone London</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-6-sub-qa-catalog-dynamic-menu-level3-9">
					 <a class="js-scramble-link lvl3--link" href="/b/palm+angels" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Nouvelles marques-Palm Angels">
					 <span>Palm Angels</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-6-sub-qa-catalog-dynamic-menu-level3-10">
					 <a class="js-scramble-link lvl3--link" href="/b/tartine+et+chocolat" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Nouvelles marques-Tartine &amp; Chocolat">
					 <span>Tartine &amp; Chocolat</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-6-sub-qa-catalog-dynamic-menu-level3-11">
					 <a class="js-scramble-link lvl3--link" href="/b/the+attico" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés-Nouvelles marques-The Attico">
					 <span>The Attico</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item lvl2__item--allUniverse" data-tnr-id="qa-catalog-dynamic-menu-level2-all-universe-xmedium-up-1">
					 <a class="js-scramble-link lvl2--link" href="/c/nouveautes" target="_self" data-lvl1="nouveautés" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Nouveautés">
					 <span class="lvl2__item__label lvl2__item__label--allUniverse">TOUT L'UNIVERS NOUVEAUTÉS</span>
					 </a>
					 </li>
					 </ul>
					 <ul class="menu__inspiration">
					 <li class="lvl2__item">
					 <span class="lvl2__item__label lvl2--link">Nos sélections</span>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/go+for+good" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Nos sélections-Mode responsable">
					 <span>Mode responsable</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a href="/c/nouveautes" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push image|gaEvent_category:Navigation|gaEvent_label:/c/nouveautes">
					 <img src="//static.galerieslafayette.com/media/endeca2/06 OP PAC generiques/3_OPES/NOUVEAUTES_PE21/NouvelleCoSS21_PushMenu_Generique.jpg" alt="">
					 </a>
					 </li>
					 </ul>
					 </span>
					 <span class="menu--overlay"></span>
					 </span>
					 </li>
					 <li class="lvl1__item" data-js="lvl1__item">
					 <a class="js-scramble-link lvl1--link" href="/h/femme" data-js="enable-lvl2" target="_self" data-lvl1="femme" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Femme">
					 <span class="lvl1__item__label lvl1__item__label--title picto__arrow--after ">Femme</span>
					 </a>
					 <span class="lvl1__item--center" data-js="lvl1SubElement">
					 <span class="item--1250" style="width: 1363px;">
					 <ul class="menu__lvl lvl2 lvl2--withRightBorder" data-js="lvl2">
					 <li class="show-for-medium-only lvl2__item lvl2--category hide-for-xmedium-up ">
					 <a class="js-scramble-link" href="/h/femme" target="_self">
					 <span class="medium show-for-medium-only">Tout l'univers Femme</span>
					 </a>
					 </li>
					 <li class="lvl2__item show-for-small-only">
					 <a href="#" data-js="backToLvl1" class="lvl2--link">
					 <span class="lvl2__item__label picto__arrow--before picto__arrow--before--left">Retour</span>
					 </a>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-7">
					 <a class="js-scramble-link lvl2--link" href="/c/marques+luxe+et+createurs/ct/femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Luxe &amp; Créateurs">
					 <span data-js="rotate-available" class="lvl2__item__label">Luxe &amp; Créateurs</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/marques+luxe+et+createurs/ct/femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Luxe &amp; Créateurs">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-8">
					 <a class="js-scramble-link lvl2--link" href="/c/go+for+good/ct/femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Mode Responsable">
					 <span data-js="rotate-available" class="lvl2__item__label">Mode Responsable</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/go+for+good/ct/femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Mode Responsable">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-9">
					 <a class="js-scramble-link lvl2--link" href="/c/femme-pret-a-porter" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Vêtements">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Vêtements</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/femme-pret-a-porter" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Vêtements">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-9-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/femme-pret-a-porter-robes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Vêtements-Robes">
					 <span>Robes</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-9-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/femme-pret-a-porter-tops+et+t-shirts" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Vêtements-Tee-Shirt">
					 <span>Tee-Shirt</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-9-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/femme-pret-a-porter-chemises+et+blouses/f/chemises" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Vêtements-Chemises">
					 <span>Chemises</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-9-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/femme-pret-a-porter-maille/f/pulls" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Vêtements-Pulls">
					 <span>Pulls</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-9-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/femme-pret-a-porter-maille/f/gilets" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Vêtements-Gilets">
					 <span>Gilets</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-9-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/c/femme-pret-a-porter-vestes+et+manteaux/f/vestes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Vêtements-Vestes">
					 <span>Vestes</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-9-sub-qa-catalog-dynamic-menu-level3-7">
					 <a class="js-scramble-link lvl3--link" href="/c/femme-pret-a-porter-vestes+et+manteaux" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Vêtements-Manteaux">
					 <span>Manteaux</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-9-sub-qa-catalog-dynamic-menu-level3-8">
					 <a class="js-scramble-link lvl3--link" href="/c/femme-pret-a-porter-pantalons+et+shorts/f/combinaisons" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Vêtements-Combinaisons">
					 <span>Combinaisons</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-9-sub-qa-catalog-dynamic-menu-level3-9">
					 <a class="js-scramble-link lvl3--link" href="/c/femme-pret-a-porter-pantalons+et+shorts" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Vêtements-Pantalons">
					 <span>Pantalons</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-9-sub-qa-catalog-dynamic-menu-level3-10">
					 <a class="js-scramble-link lvl3--link" href="/c/femme-pret-a-porter-jeans" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Vêtements-Jeans">
					 <span>Jeans</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-9-sub-qa-catalog-dynamic-menu-level3-11">
					 <a class="js-scramble-link lvl3--link" href="/c/femme-pret-a-porter-jupes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Vêtements-Jupes">
					 <span>Jupes</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-9-sub-qa-catalog-dynamic-menu-level3-12">
					 <a class="js-scramble-link lvl3--link" href="/c/femme-pret-a-porter-shorts+et+bermudas/f/shorts" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Vêtements-Shorts">
					 <span>Shorts</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-10">
					 <a class="js-scramble-link lvl2--link" href="/c/lingerie+et+bain-nuit+homewear+accessoires/f/pyjamas" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Homewear">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Homewear</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/lingerie+et+bain-nuit+homewear+accessoires/f/pyjamas" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Homewear">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-10-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/lingerie+et+bain-nuit+homewear+accessoires/f/pyjamas" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Homewear-Pyjamas">
					 <span>Pyjamas</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-11">
					 <a class="js-scramble-link lvl2--link" href="/c/sport/ct/femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Sport">
					 <span data-js="rotate-available" class="lvl2__item__label">Sport</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/sport/ct/femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Sport">
					 <span>Tous les produits</span>
					 
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-12">
					 <a class="js-scramble-link lvl2--link" href="/c/lingerie+et+bain-maillots+de+bain+plage" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Maillots de Bain">
					 <span data-js="rotate-available" class="lvl2__item__label">Maillots de Bain</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/lingerie+et+bain-maillots+de+bain+plage" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Maillots de Bain">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-13">
					 <a class="js-scramble-link lvl2--link" href="/c/lingerie+et+bain" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Lingerie">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Lingerie</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/lingerie+et+bain" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Lingerie">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-13-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/lingerie+et+bain/f/push-up/multipositions+bretelles+amovibles/guepieres/corbeilles+balconnets/bustiers/brassieres/bandeaux/armatures+classiques/ampliformes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Lingerie-Soutien-gorges">
					 <span>Soutien-gorges</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-13-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/lingerie+et+bain-culottes+et+bas/f/culottes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Lingerie-Culottes">
					 <span>Culottes</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-13-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/lingerie+et+bain-collants+et+chaussettes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Lingerie-Chaussettes &amp; Collants">
					 <span>Chaussettes &amp; Collants</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-14">
					 <a class="js-scramble-link lvl2--link" href="/c/sacs+et+accessoires" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Sacs &amp; Bagages">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Sacs &amp; Bagages</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/sacs+et+accessoires" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Sacs &amp; Bagages">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-14-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/sacs+et+accessoires-sacs+femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Sacs &amp; Bagages-Sacs">
					 <span>Sacs</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-14-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/accessoires-accessoires+femme-portefeuilles+et+portes-monnaie" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Sacs &amp; Bagages-Petite Maroquinerie">
					 <span>Petite Maroquinerie</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-14-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/sacs+et+accessoires-bagages" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Sacs &amp; Bagages-Bagages">
					 <span>Bagages</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-15">
					 <a class="js-scramble-link lvl2--link" href="/c/chaussures-femme" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Chaussures">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Chaussures</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/chaussures-femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Chaussures">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-15-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/chaussures-femme-chaussures+de+sport" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Chaussures-Baskets">
					 <span>Baskets</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-15-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/chaussures-femme-nu-pieds" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Chaussures-Sandales">
					 <span>Sandales</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-15-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/chaussures-femme-espadrilles" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Chaussures-Espadrilles">
					 <span>Espadrilles</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-15-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/chaussures-femme-escarpins+et+salomes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Chaussures-Escarpins">
					 <span>Escarpins</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-15-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/chaussures-femme-ballerines+et+slippers" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Chaussures-Ballerines">
					 <span>Ballerines</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-15-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/c/chaussures-femme/f/richelieus/mocassins" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Chaussures-Derbies &amp; Richelieus">
					 <span>Derbies &amp; Richelieus</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-15-sub-qa-catalog-dynamic-menu-level3-7">
					 <a class="js-scramble-link lvl3--link" href="/c/chaussures-femme-bottes+et+bottines" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Chaussures-Bottes &amp; Bottines">
					 <span>Bottes &amp; Bottines</span>
					 </a>
					 </li>
					 
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-16">
					 <a class="js-scramble-link lvl2--link" href="/c/accessoires-accessoires+femme-montres" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Montres">
					 <span data-js="rotate-available" class="lvl2__item__label">Montres</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/accessoires-accessoires+femme-montres" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Montres">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-17">
					 <a class="js-scramble-link lvl2--link" href="/c/accessoires-accessoires+femme-bijoux" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Bijouterie">
					 <span data-js="rotate-available" class="lvl2__item__label">Bijouterie</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/accessoires-accessoires+femme-bijoux" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Bijouterie">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-18">
					 <a class="js-scramble-link lvl2--link" href="/c/femme/accessoires/joaillerie" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Joaillerie">
					 <span data-js="rotate-available" class="lvl2__item__label">Joaillerie</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/femme/accessoires/joaillerie" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Joaillerie">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-19">
					 <a class="js-scramble-link lvl2--link" href="/c/accessoires-accessoires+femme/ct/femme-accessoires" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Accessoires">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Accessoires</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/accessoires-accessoires+femme/ct/femme-accessoires" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Accessoires">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-19-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/accessoires-accessoires+femme-lunettes+de+soleil" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Accessoires-Lunettes de soleil">
					 <span>Lunettes de soleil</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-19-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/accessoires-accessoires+femme-bonnets+et+chapeaux" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Accessoires-Chapeaux &amp; Casquettes">
					 <span>Chapeaux &amp; Casquettes</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-19-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/accessoires-accessoires+femme-echarpes+et+foulards" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Accessoires-Écharpes &amp; Foulards">
					 <span>Écharpes &amp; Foulards</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-19-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/accessoires-accessoires+femme-ceintures" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Accessoires-Ceintures">
					 <span>Ceintures</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-20">
					 <a class="js-scramble-link lvl2--link" href="/marques" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Top Marques">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Top Marques</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/marques" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Top Marques">
					 <span>Tous les produits</span>
					 
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-20-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/b/galeries+lafayette/ct/femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Top Marques-Galeries Lafayette">
					 <span>Galeries Lafayette</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-20-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/b/sandro/ct/femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Top Marques-Sandro">
					 <span>Sandro</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-20-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/b/maje" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Top Marques-Maje">
					 <span>Maje</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-20-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/b/jacquemus" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Top Marques-Jacquemus">
					 <span>Jacquemus</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-20-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/b/the+kooples" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Top Marques-The Kooples">
					 <span>The Kooples</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-20-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="https://www.galerieslafayette.com/b/zadig+voltaire/ct/femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Top Marques-Zadig&amp;Voltaire">
					 <span>Zadig&amp;Voltaire</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-20-sub-qa-catalog-dynamic-menu-level3-7">
					 <a class="js-scramble-link lvl3--link" href="/b/a+p+c+" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Top Marques-A.P.C.">
					 <span>A.P.C.</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-21">
					 <a class="js-scramble-link lvl2--link" href="/marques/d/femme-accessoires/femme-chaussures/femme-lingerie/femme-maillots-de-bain/femme-sacs-et-maroquinerie/femme-vetements/" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Toutes les marques">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Toutes les marques</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/marques/d/femme-accessoires/femme-chaussures/femme-lingerie/femme-maillots-de-bain/femme-sacs-et-maroquinerie/femme-vetements/" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Femme-Toutes les marques">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item lvl2__item--allUniverse" data-tnr-id="qa-catalog-dynamic-menu-level2-all-universe-xmedium-up-2">
					 <a class="js-scramble-link lvl2--link" href="/h/femme" target="_self" data-lvl1="femme" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Femme">
					 <span class="lvl2__item__label lvl2__item__label--allUniverse">TOUT L'UNIVERS FEMME</span>
					 </a>
					 </li>
					 </ul>
					 <ul class="menu__inspiration">
					 <li class="lvl2__item">
					 <span class="lvl2__item__label lvl2--link">Sélections</span>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/boutique+ceremonie/ct/femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Boutique Cérémonie">
					 <span>Boutique Cérémonie</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/les+pepites+go+for+good/ct/femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Pépites Go For Good">
					 <span>Pépites Go For Good</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/nouveautes/ct/femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Nouveautés">
					 <span>Nouveautés</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/denim+fun/ct/femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Denim Fun">
					 <span>Denim Fun</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/le+vert+espoir" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Le vert espoir">
					 <span>Le vert espoir</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/evt/fr/shoppingadistance" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Le nouveau service : Shopping à distance">
					 <span>Le nouveau service : Shopping à distance</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a href="/leliveshopping" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push image|gaEvent_category:Navigation|gaEvent_label:/leliveshopping">
					 <img src="//static.galerieslafayette.com/media/endeca2/01 FEMME/Home_Femme/LIVESHOPPING.jpg" alt="">
					 </a>
					 </li>
					 </ul>
					 </span>
					 <span class="menu--overlay"></span>
					 </span>
					 </li>
					 <li class="lvl1__item" data-js="lvl1__item">
					 <a class="js-scramble-link lvl1--link" href="/h/homme" data-js="enable-lvl2" target="_self" data-lvl1="homme" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Homme">
					 <span class="lvl1__item__label lvl1__item__label--title picto__arrow--after ">Homme</span>
					 </a>
					 <span class="lvl1__item--center" data-js="lvl1SubElement">
					 <span class="item--1250" style="width: 1363px;">
					 <ul class="menu__lvl lvl2 lvl2--withRightBorder" data-js="lvl2">
					 <li class="show-for-medium-only lvl2__item lvl2--category hide-for-xmedium-up ">
					 <a class="js-scramble-link" href="/h/homme" target="_self">
					 <span class="medium show-for-medium-only">Tout l'univers Homme</span>
					 </a>
					 </li>
					 <li class="lvl2__item show-for-small-only">
					 <a href="#" data-js="backToLvl1" class="lvl2--link">
					 <span class="lvl2__item__label picto__arrow--before picto__arrow--before--left">Retour</span>
					 </a>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-22">
					 <a class="js-scramble-link lvl2--link" href="/c/marques+luxe+et+createurs/ct/homme" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Luxe &amp; Créateurs">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Luxe &amp; Créateurs</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/marques+luxe+et+createurs/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Luxe &amp; Créateurs">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-23">
					 <a class="js-scramble-link lvl2--link" href="/c/go+for+good/ct/homme" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Mode Responsable">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Mode Responsable</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/go+for+good/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Mode Responsable">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-24">
					 <a class="js-scramble-link lvl2--link" href="/c/homme-pret-a-porter" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Vêtements">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Vêtements</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/homme-pret-a-porter" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Vêtements">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-24-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/homme-pret-a-porter-t-shirts+et+polos/f/t-shirts" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Vêtements-T-shirts">
					 <span>T-shirts</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-24-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/homme-pret-a-porter-t-shirts+et+polos/f/polos" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Vêtements-Polos">
					 <span>Polos</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-24-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/homme-pret-a-porter-chemises" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Vêtements-Chemises">
					 <span>Chemises</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-24-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/homme-pret-a-porter-maille/f/sweat-shirts" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Vêtements-Sweat-shirts">
					 <span>Sweat-shirts</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-24-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/homme-pret-a-porter-maille/f/pulls" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Vêtements-Pulls">
					 <span>Pulls</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-24-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/c/homme-pret-a-porter-vestes+et+manteaux/f/vestes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Vêtements-Vestes">
					 <span>Vestes</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-24-sub-qa-catalog-dynamic-menu-level3-7">
					 <a class="js-scramble-link lvl3--link" href="/c/homme-pret-a-porter-vestes+et+manteaux/f/manteaux" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Vêtements-Manteaux">
					 <span>Manteaux</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-24-sub-qa-catalog-dynamic-menu-level3-8">
					 <a class="js-scramble-link lvl3--link" href="/c/homme-pret-a-porter-shorts+et+bermudas/f/shorts" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Vêtements-Shorts">
					 <span>Shorts</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-24-sub-qa-catalog-dynamic-menu-level3-9">
					 <a class="js-scramble-link lvl3--link" href="/c/homme-pret-a-porter-shorts+et+bermudas/f/bermudas" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Vêtements-Bermudas">
					 <span>Bermudas</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-24-sub-qa-catalog-dynamic-menu-level3-10">
					 <a class="js-scramble-link lvl3--link" href="/c/homme-pret-a-porter-jeans" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Vêtements-Jeans">
					 <span>Jeans</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-24-sub-qa-catalog-dynamic-menu-level3-11">
					 <a class="js-scramble-link lvl3--link" href="/c/homme-pret-a-porter-pantalons+et+shorts/f/pantalons" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Vêtements-Pantalons">
					 <span>Pantalons</span>
						</a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-24-sub-qa-catalog-dynamic-menu-level3-12">
					 <a class="js-scramble-link lvl3--link" href="/c/homme-pret-a-porter-costumes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Vêtements-Costumes">
					 <span>Costumes</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-25">
					 <a class="js-scramble-link lvl2--link" href="/c/homme-homewear+et+sous-vetements" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Sous-vêtements">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Sous-vêtements</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/homme-homewear+et+sous-vetements" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Sous-vêtements">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-25-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/homme-homewear+et+sous-vetements-sous-vetements/f/calecons/boxers/slips" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Sous-vêtements-Boxers &amp; Caleçons">
					 <span>Boxers &amp; Caleçons</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-25-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/homme-homewear+et+sous-vetements-homewear/f/chaussettes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Sous-vêtements-Chaussettes">
					 <span>Chaussettes</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-26">
					 <a class="js-scramble-link lvl2--link" href="/c/homme/homewear" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Homewear">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Homewear</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/homme/homewear" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Homewear">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-27">
					 <a class="js-scramble-link lvl2--link" href="/c/homme/bain" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Bain">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Bain</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/homme/bain" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Bain">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-28">
					 <a class="js-scramble-link lvl2--link" href="/c/sacs+et+accessoires-sacs+homme" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Sacs &amp; Bagages">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Sacs &amp; Bagages</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/sacs+et+accessoires-sacs+homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Sacs &amp; Bagages">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-28-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/sacs+et+accessoires-sacs+homme/sacs" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Sacs &amp; Bagages-Sacs">
					 <span>Sacs</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-28-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/sacs+et+accessoires-bagages" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Sacs &amp; Bagages-Bagages">
					 <span>Bagages</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-28-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/accessoires-accessoires+homme-portefeuilles+et+portes-monnaie" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Sacs &amp; Bagages-Petite Maroquinerie">
					 <span>Petite Maroquinerie</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-29">
					 <a class="js-scramble-link lvl2--link" href="/c/chaussures-homme" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Chaussures">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Chaussures</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/chaussures-homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Chaussures">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-29-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/chaussures-homme-baskets" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Chaussures-Baskets">
					 <span>Baskets</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-29-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/chaussures-homme-chaussures+d+ete" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Chaussures-Mules &amp; Espadrilles">
					 <span>Mules &amp; Espadrilles</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-29-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/chaussures-homme-chaussures+de+ville/f/mocassins" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Chaussures-Mocassins">
					 <span>Mocassins</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-29-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/chaussures-homme-chaussures+de+ville/f/derbies" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Chaussures-Derbies &amp; Richelieus">
					 <span>Derbies &amp; Richelieus</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-29-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/chaussures-homme/bottes-bottines" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Chaussures-Bottes &amp; Bottines">
					 <span>Bottes &amp; Bottines</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-30">
					 <a class="js-scramble-link lvl2--link" href="/c/accessoires-accessoires+homme" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Accessoires">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Accessoires</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/accessoires-accessoires+homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Accessoires">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-30-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/accessoires-accessoires+homme-lunettes+de+soleil" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Accessoires-Lunettes de soleil">
					 <span>Lunettes de soleil</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-30-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/accessoires-accessoires+homme-bonnets+et+chapeaux" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Accessoires-Casquettes &amp; Chapeaux">
					 <span>Casquettes &amp; Chapeaux</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-30-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/accessoires-accessoires+homme-echarpes+et+foulards" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Accessoires-Echarpes &amp; Chèches">
					 <span>Echarpes &amp; Chèches</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-30-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/accessoires-accessoires+homme-ceintures" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Accessoires-Ceintures">
					 <span>Ceintures</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-30-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/accessoires-accessoires+homme-cravates" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Accessoires-Cravates &amp; Noeuds Papillon">
					 <span>Cravates &amp; Noeuds Papillon</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-30-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/c/accessoires-accessoires+homme-bijoux" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Accessoires-Bijoux">
					 <span>Bijoux</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-31">
					 <a class="js-scramble-link lvl2--link" href="/c/sport/ct/homme" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Sport">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Sport</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/sport/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Sport">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-32">
					 <a class="js-scramble-link lvl2--link" href="/c/homme/accessoires/montres" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Montres">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Montres</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/homme/accessoires/montres" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Montres">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-33">
					 <a class="js-scramble-link lvl2--link" href="/marques/d/homme-chaussures/homme-sacs-et-accessoires/homme-sous-vetements/homme-vetements/" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Top Marques</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/marques/d/homme-chaussures/homme-sacs-et-accessoires/homme-sous-vetements/homme-vetements/" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-33-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/b/a+p+c+/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques-A.P.C">
					 <span>A.P.C</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-33-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/b/boss/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques-BOSS">
					 <span>BOSS</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-33-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/b/calvin+klein/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques-Calvin Klein">
					 <span>Calvin Klein</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-33-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/b/comptoir+gl" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques-Comptoir GL">
					 <span>Comptoir GL</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-33-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/b/etudes/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques-Etudes">
					 <span>Etudes</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-33-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/b/galeries+lafayette/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques-Galeries Lafayette">
					 <span>Galeries Lafayette</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-33-sub-qa-catalog-dynamic-menu-level3-7">
					 <a class="js-scramble-link lvl3--link" href="/b/heron+preston/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques-Heron Preston">
					 
					 <span>Heron Preston</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-33-sub-qa-catalog-dynamic-menu-level3-8">
					 <a class="js-scramble-link lvl3--link" href="/b/jacquemus/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques-Jacquemus">
					 <span>Jacquemus</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-33-sub-qa-catalog-dynamic-menu-level3-9">
					 <a class="js-scramble-link lvl3--link" href="/b/lacoste/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques-Lacoste">
					 <span>Lacoste</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-33-sub-qa-catalog-dynamic-menu-level3-10">
					 <a class="js-scramble-link lvl3--link" href="/b/levi+s/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques-Levi's">
					 <span>Levi's</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-33-sub-qa-catalog-dynamic-menu-level3-11">
					 <a class="js-scramble-link lvl3--link" href="/b/off+white/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques-Off White">
					 <span>Off White</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-33-sub-qa-catalog-dynamic-menu-level3-12">
					 <a class="js-scramble-link lvl3--link" href="/b/polo+ralph+lauren/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques-Polo Ralph Lauren">
					 <span>Polo Ralph Lauren</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-33-sub-qa-catalog-dynamic-menu-level3-13">
					 <a class="js-scramble-link lvl3--link" href="/b/sandro/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques-Sandro">
					 <span>Sandro</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-33-sub-qa-catalog-dynamic-menu-level3-14">
					 <a class="js-scramble-link lvl3--link" href="/b/the+kooples/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques-The Kooples">
					 <span>The Kooples</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-33-sub-qa-catalog-dynamic-menu-level3-15">
					 <a class="js-scramble-link lvl3--link" href="/b/tommy+hilfiger/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques-Tommy Hilfiger">
					 <span>Tommy Hilfiger</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-33-sub-qa-catalog-dynamic-menu-level3-16">
					 <a class="js-scramble-link lvl3--link" href="/b/zadig+voltaire/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Top Marques-Zadig &amp; Voltaire">
					 <span>Zadig &amp; Voltaire</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-34">
					 <a class="js-scramble-link lvl2--link" href="/marques/d/homme-chaussures/homme-sacs-et-accessoires/homme-sous-vetements/homme-vetements/" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Toutes les marques">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Toutes les marques</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/marques/d/homme-chaussures/homme-sacs-et-accessoires/homme-sous-vetements/homme-vetements/" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Homme-Toutes les marques">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item lvl2__item--allUniverse" data-tnr-id="qa-catalog-dynamic-menu-level2-all-universe-xmedium-up-3">
					 <a class="js-scramble-link lvl2--link" href="/h/homme" target="_self" data-lvl1="homme" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Homme">
					 <span class="lvl2__item__label lvl2__item__label--allUniverse">TOUT L'UNIVERS HOMME</span>
					 </a>
					 </li>
					 </ul>
					 <ul class="menu__inspiration">
					 <li class="lvl2__item">
					 <span class="lvl2__item__label lvl2--link">Sélections</span>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/nouveautes/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Nouveautés">
					 <span>Nouveautés</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/jogging+chic/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Jogging Chic">
					 <span>Jogging Chic</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
						<a class="js-scramble-link" href="/c/vestiaire+urbain" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Vestiaire Urbain">
					 <span>Vestiaire Urbain</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/boutique+ceremonie/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Boutique Cérémonie">
					 <span>Boutique Cérémonie</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/happy+accessoires/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Accessoires de saison">
					 <span>Accessoires de saison</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/les+pepites+go+for+good/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Les Pépites Go for Good">
					 <span>Les Pépites Go for Good</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/magazine/homme/printemps-ete-2021/les-mocassins-sont-de-retour" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Le retour des mocassins">
					 <span>Le retour des mocassins</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/coups+de+coeur+beaux+jours/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Coups de coeur Beaux Jours, Bonjour">
					 <span>Coups de coeur Beaux Jours, Bonjour</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/evt/fr/shoppingadistance" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Le nouveau service : Shopping à distance">
					 <span>Le nouveau service : Shopping à distance</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a href="/evt/animations/the-kooples" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push image|gaEvent_category:Navigation|gaEvent_label:/evt/animations/the-kooples">
					 <img src="//static.galerieslafayette.com/media/endeca2/02 HOMME/PUSH_MENU/TheKooples_PushMenu_mixte.jpg" alt="">
					 </a>
					 </li>
					 </ul>
					 </span>
					 <span class="menu--overlay"></span>
					 </span>
					 </li>
					 <li class="lvl1__item" data-js="lvl1__item">
					 <a class="js-scramble-link lvl1--link" href="/h/beaute" data-js="enable-lvl2" target="_self" data-lvl1="beauté" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Beauté">
					 <span class="lvl1__item__label lvl1__item__label--title picto__arrow--after ">Beauté</span>
					 </a>
					 <span class="lvl1__item--center" data-js="lvl1SubElement">
					 <span class="item--1250" style="width: 1363px;">
					 <ul class="menu__lvl lvl2 lvl2--withRightBorder" data-js="lvl2">
					 <li class="show-for-medium-only lvl2__item lvl2--category hide-for-xmedium-up ">
					 <a class="js-scramble-link" href="/h/beaute" target="_self">
					 <span class="medium show-for-medium-only">Tout l'univers Beauté</span>
					 </a>
					 </li>
					 <li class="lvl2__item show-for-small-only">
					 <a href="#" data-js="backToLvl1" class="lvl2--link">
					 <span class="lvl2__item__label picto__arrow--before picto__arrow--before--left">Retour</span>
					 </a>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-35">
					 <a class="js-scramble-link lvl2--link" href="/c/beaute-parfum" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Parfums">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Parfums</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/beaute-parfum" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Parfums">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-35-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-parfum/ct/beaute-parfum-parfum+femme-coffrets+femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Parfums-Coffrets Femme">
					 <span>Coffrets Femme</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-35-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-parfum-parfum+femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Parfums-Parfums Femme">
					 <span>Parfums Femme</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-35-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-parfum/ct/beaute-parfum-parfum+homme-coffrets+homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Parfums-Coffrets Homme">
					 <span>Coffrets Homme</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-35-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-parfum-parfum+homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Parfums-Parfums Homme">
					 <span>Parfums Homme</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-35-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/boutique+parfums+d+exception" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Parfums-Parfums d'exception">
					 <span>Parfums d'exception</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-36">
					 <a class="js-scramble-link lvl2--link" href="/c/beaute-maquillage" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Maquillage">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Maquillage</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/beaute-maquillage" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Maquillage">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-36-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-maquillage-levres" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Maquillage-Lèvres">
					 <span>Lèvres</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-36-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-maquillage-teint" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Maquillage-Teint">
					 <span>Teint</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-36-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-maquillage-yeux" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Maquillage-Yeux">
					 <span>Yeux</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-36-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-maquillage-yeux-sourcils" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Maquillage-Sourcil">
					 <span>Sourcil</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-36-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-maquillage-ongles" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Maquillage-Ongles">
					 <span>Ongles</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-37">
					 <a class="js-scramble-link lvl2--link" href="/c/beaute+homme" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Homme">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Homme</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/beaute+homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Homme">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-37-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute+homme/ct/beaute-soin/f/rasage" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Homme-Rasage">
					 <span>Rasage</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-37-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-soin-soin+homme-hydratant+nourrissant" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Homme-Hydratant &amp; Nourissant">
					 <span>Hydratant &amp; Nourissant</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-37-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-soin-soin+homme-anti-age+anti-rides" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Homme-Anti-ride &amp; Anti-âge">
					 <span>Anti-ride &amp; Anti-âge</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-38">
					 <a class="js-scramble-link lvl2--link" href="/c/beaute-soin" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Soin">
						<span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Soin</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/beaute-soin" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Soin">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-38-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-soin-coffrets+soin" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Soin-Coffrets soin">
					 <span>Coffrets soin</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-38-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-soin-soin+visage-demaquillant+nettoyant" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Soin-Démaquillant &amp; Nettoyant">
					 <span>Démaquillant &amp; Nettoyant</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-38-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-soin-soin+visage-hydratant+nourrissant" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Soin-Hydratant visage">
					 <span>Hydratant visage</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-38-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-soin-soin+visage-anti-age+anti-rides" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Soin-Anti-âge &amp; Anti-rides">
					 <span>Anti-âge &amp; Anti-rides</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-38-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-soin/f/bb+cc+cremes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Soin-BB &amp; CC Crèmes">
					 <span>BB &amp; CC Crèmes</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-38-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-soin-soin+visage-exfoliant+masque" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Soin-Exfoliant &amp; Masque">
					 <span>Exfoliant &amp; Masque</span>
					 </a>
					 
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-38-sub-qa-catalog-dynamic-menu-level3-7">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-soin-soin+visage-serums" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Soin-Sérums">
					 <span>Sérums</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-38-sub-qa-catalog-dynamic-menu-level3-8">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute/ct/beaute-soin-soin+corps" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Soin-Corps &amp; Bain">
					 <span>Corps &amp; Bain</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-38-sub-qa-catalog-dynamic-menu-level3-9">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-soin-soin+solaire" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Soin-Solaires &amp; Autobronzants">
					 <span>Solaires &amp; Autobronzants</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-39">
					 <a class="js-scramble-link lvl2--link" href="/evt/animations/diagnostic-peau" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Routine sur mesure">
					 <span data-js="rotate-available" class="lvl2__item__label">Routine sur mesure</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/evt/animations/diagnostic-peau" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Routine sur mesure">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-40">
					 <a class="js-scramble-link lvl2--link" href="/c/beaute-soin-soin+cheveux" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Cheveux">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Cheveux</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/beaute-soin-soin+cheveux" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Cheveux">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-40-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-soin-soin+cheveux/f/shampoings" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Cheveux-Shampoings">
					 <span>Shampoings</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-40-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-soin-soin+cheveux/f/soin+cheveux" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Cheveux-Apres-shampoings et soins">
					 <span>Apres-shampoings et soins</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-40-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-soin-soin+cheveux/f/produits+coiffants" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Cheveux-Coiffants">
					 <span>Coiffants</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-40-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/beaute-soin/ct/beaute/f/coloration+eclaircissants" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Cheveux-Colorations">
					 <span>Colorations</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-41">
					 <a class="js-scramble-link lvl2--link" href="/c/beauty+galerie" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Beauté responsable">
					 <span data-js="rotate-available" class="lvl2__item__label">Beauté responsable</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/beauty+galerie" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Beauté responsable">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-42">
					 <a class="js-scramble-link lvl2--link" href="/marques/d/beaute/" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Top marques">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Top marques</span>
					 </a>
					 
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/marques/d/beaute/" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Top marques">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-42-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/b/chanel" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Top marques-CHANEL">
					 <span>CHANEL</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-42-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/b/hermes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Top marques-HERMÈS">
					 <span>HERMÈS</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-42-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/b/dior#ebp=SmMvxkqDL6S1a502afB.olPYuqlpCD7SZXLcvZz_2T6k" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Top marques-DIOR">
					 <span>DIOR</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-42-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/b/guerlain" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Top marques-GUERLAIN">
					 <span>GUERLAIN</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-42-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/b/the+ordinary#ebp=SmMvxkqDL6S1a502afB.osvSbDKw09RLn5HUWHbaUjn9" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Top marques-The Ordinary">
					 <span>The Ordinary</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-42-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/b/yves+saint+laurent/ct/beaute?s=yves%20saint%20laurent" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Top marques-Yves Saint Laurent">
					 <span>Yves Saint Laurent</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-42-sub-qa-catalog-dynamic-menu-level3-7">
					 <a class="js-scramble-link lvl3--link" href="/b/estee+lauder" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Top marques-Estée Lauder">
					 <span>Estée Lauder</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-42-sub-qa-catalog-dynamic-menu-level3-8">
					 <a class="js-scramble-link lvl3--link" href="/b/clinique" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Top marques-Clinique">
					 <span>Clinique</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-42-sub-qa-catalog-dynamic-menu-level3-9">
					 <a class="js-scramble-link lvl3--link" href="/b/clarins" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Top marques-Clarins">
					 <span>Clarins</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-42-sub-qa-catalog-dynamic-menu-level3-10">
					 <a class="js-scramble-link lvl3--link" href="/b/sisley+paris?s=sisley%20paris%20beaute" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Top marques-Sisley Paris (Beauté)">
					 <span>Sisley Paris (Beauté)</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-43">
					 <a class="js-scramble-link lvl2--link" href="/marques/d/beaute/" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Toutes les marques">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Toutes les marques</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/marques/d/beaute/" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Beauté-Toutes les marques">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item lvl2__item--allUniverse" data-tnr-id="qa-catalog-dynamic-menu-level2-all-universe-xmedium-up-4">
					 <a class="js-scramble-link lvl2--link" href="/h/beaute" target="_self" data-lvl1="beauté" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Beauté">
					 <span class="lvl2__item__label lvl2__item__label--allUniverse">TOUT L'UNIVERS BEAUTÉ</span>
					 </a>
					 </li>
					 </ul>
					 <ul class="menu__inspiration">
					 <li class="lvl2__item">
					 <span class="lvl2__item__label lvl2--link">Sélections</span>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/nouveautes/ct/beaute" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Nouveautés">
					 <span>Nouveautés</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/beauty+galerie" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Beauté responsable">
					 <span>Beauté responsable</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/boutique+parfums+d+exception" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Parfums d'exception">
					 <span>Parfums d'exception</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/beaute+detox" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Offres du moment">
					 <span>Offres du moment</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/teint+parfait" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Les coffrets beauté">
					 <span>Les coffrets beauté</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/beaute+pop" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Maquillage pop">
					 <span>Maquillage pop</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a href="/c/nouveautes/ct/beaute" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push image|gaEvent_category:Navigation|gaEvent_label:/c/nouveautes/ct/beaute">
					 <img src="//static.galerieslafayette.com/media/endeca2/06 OP PAC generiques/3_OPES/NOUVEAUTES_PE21/NouvelleCoSS21_PushMenu_Beaute.jpg" alt="">
					 </a>
					 </li>
					 </ul>
					 </span>
					 <span class="menu--overlay"></span>
					 </span>
					 </li>
					 <li class="lvl1__item" data-js="lvl1__item">
					 <a class="js-scramble-link lvl1--link" href="/h/enfant" data-js="enable-lvl2" target="_self" data-lvl1="enfant" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Enfant">
					 <span class="lvl1__item__label lvl1__item__label--title picto__arrow--after ">Enfant</span>
					 </a>
					 <span class="lvl1__item--center" data-js="lvl1SubElement">
					 <span class="item--1250" style="width: 1363px;">
					 <ul class="menu__lvl lvl2 lvl2--withRightBorder" data-js="lvl2">
					 <li class="show-for-medium-only lvl2__item lvl2--category hide-for-xmedium-up ">
					 <a class="js-scramble-link" href="/h/enfant" target="_self">
					 <span class="medium show-for-medium-only">Tout l'univers Enfant</span>
					 </a>
					 </li>
					 <li class="lvl2__item show-for-small-only">
					 <a href="#" data-js="backToLvl1" class="lvl2--link">
					 <span class="lvl2__item__label picto__arrow--before picto__arrow--before--left">Retour</span>
					 </a>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-44">
					 <a class="js-scramble-link lvl2--link" href="/c/go+for+good-enfant" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Sélection Responsable">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Sélection Responsable</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/go+for+good-enfant" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Sélection Responsable">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-45">
					 <a class="js-scramble-link lvl2--link" href="/c/nouveautes-nouveautes+enfant" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Nouveautés Enfant">
					 <span data-js="rotate-available" class="lvl2__item__label">Nouveautés Enfant</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/nouveautes-nouveautes+enfant" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Nouveautés Enfant">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-46">
					 <a class="js-scramble-link lvl2--link" href="/c/jeux+et+jouets" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Jeux &amp; Jouets">
					 <span data-js="rotate-available" class="lvl2__item__label">Jeux &amp; Jouets</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/jeux+et+jouets" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Jeux &amp; Jouets">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-47">
					 
					 <a class="js-scramble-link lvl2--link" href="/c/enfant-fille+3-16+ans+" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Fille">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Fille</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/enfant-fille+3-16+ans+" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Fille">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-47-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-fille+3-16+ans+-tops+t-shirts+chemises" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Fille-Hauts">
					 <span>Hauts</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-47-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-fille+3-16+ans+-manteaux+vestes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Fille-Vestes">
					 <span>Vestes</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-47-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-fille+3-16+ans+-manteaux+vestes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Fille-Manteaux">
					 <span>Manteaux</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-47-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-fille+3-16+ans+/f/salopettes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Fille-Combinaisons">
					 <span>Combinaisons</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-47-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-fille+3-16+ans+-robes+jupes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Fille-Robes">
					 <span>Robes</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-47-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-fille+3-16+ans+-jeans+pantalons" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Fille-Bas">
					 <span>Bas</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-47-sub-qa-catalog-dynamic-menu-level3-7">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-fille+3-16+ans+-pyjamas+sous-vetements" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Fille-Sous-Vêtements">
					 <span>Sous-Vêtements</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-47-sub-qa-catalog-dynamic-menu-level3-8">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-fille+3-16+ans+-pyjamas+sous-vetements" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Fille-Homewear">
					 <span>Homewear</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-47-sub-qa-catalog-dynamic-menu-level3-9">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-fille+3-16+ans+-maillots+de+bain" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Fille-Maillots de bain">
					 <span>Maillots de bain</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-47-sub-qa-catalog-dynamic-menu-level3-10">
					 <a class="js-scramble-link lvl3--link" href="/c/chaussures-enfant" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Fille-Chaussures">
					 <span>Chaussures</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-47-sub-qa-catalog-dynamic-menu-level3-11">
					 <a class="js-scramble-link lvl3--link" href="/c/accessoires-accessoires+fille" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Fille-Accessoires">
					 <span>Accessoires</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-48">
					 <a class="js-scramble-link lvl2--link" href="/c/enfant-garcon+3-16+ans+" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Garçon">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Garçon</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/enfant-garcon+3-16+ans+" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Garçon">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-48-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-garcon+3-16+ans+-polos+t-shirts+chemises" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Garçon-Hauts">
					 <span>Hauts</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-48-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-garcon+3-16+ans+-manteaux+vestes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Garçon-Vestes">
					 <span>Vestes</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-48-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-garcon+3-16+ans+-manteaux+vestes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Garçon-Manteaux">
					 <span>Manteaux</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-48-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-garcon+3-16+ans+-jeans+pantalons/f/salopettes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Garçon-Ensembles">
					 <span>Ensembles</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-48-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-garcon+3-16+ans+-jeans+pantalons" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Garçon-Bas">
					 <span>Bas</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-48-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-garcon+3-16+ans+-pyjamas+sous-vetements" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Garçon-Sous-Vêtements">
					 <span>Sous-Vêtements</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-48-sub-qa-catalog-dynamic-menu-level3-7">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-garcon+3-16+ans+-pyjamas+sous-vetements" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Garçon-Homewear">
					 <span>Homewear</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-48-sub-qa-catalog-dynamic-menu-level3-8">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-garcon+3-16+ans+-maillots+de+bain" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Garçon-Maillots de bain">
					 <span>Maillots de bain</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-48-sub-qa-catalog-dynamic-menu-level3-9">
					 <a class="js-scramble-link lvl3--link" href="/c/chaussures-enfant" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Garçon-Chaussures">
					 <span>Chaussures</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-48-sub-qa-catalog-dynamic-menu-level3-10">
					 <a class="js-scramble-link lvl3--link" href="/c/accessoires-accessoires+garcon" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Garçon-Accessoires">
					 <span>Accessoires</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-49">
					 <a class="js-scramble-link lvl2--link" href="/c/enfant-bebe+0-2+ans+" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Bébé">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Bébé</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/enfant-bebe+0-2+ans+" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Bébé">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-49-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-bebe+0-2+ans+" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Bébé-Vêtements">
					 <span>Vêtements</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-49-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-bebe+0-2+ans+-pyjamas+sous-vetements" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Bébé-Sous-vêtements">
					 <span>Sous-vêtements</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-49-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-bebe+0-2+ans+-pyjamas+sous-vetements" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Bébé-Pyjamas">
					 <span>Pyjamas</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-49-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-bebe+0-2+ans+-pyjamas+sous-vetements/f/gigoteuses" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Bébé-Gigoteuses">
					 <span>Gigoteuses</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-49-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/enfant-bebe+0-2+ans+-chaussures" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Bébé-Chaussures">
					 <span>Chaussures</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-49-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/c/accessoires-accessoires+bebe" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Bébé-Accessoires">
					 <span>Accessoires</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-50">
					 <a class="js-scramble-link lvl2--link" href="/marques/d/enfant/enfant-jeux-et-jouets/enfant-vetements/" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Top Marques">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Top Marques</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/marques/d/enfant/enfant-jeux-et-jouets/enfant-vetements/" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Top Marques">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-50-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/b/billieblush" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Top Marques-Billieblush">
					 <span>Billieblush</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-50-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/b/bonton" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Top Marques-Bonton">
						<span>Bonton</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-50-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/b/boss/c/enfant" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Top Marques-BOSS">
					 <span>BOSS</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-50-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/burberry" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Top Marques-Burberry">
					 <span>Burberry</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-50-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/b/cadet+rousselle" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Top Marques-Cadet Rousselle">
					 <span>Cadet Rousselle</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-50-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/b/cyrillus/c/enfant" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Top Marques-Cyrillus">
					 <span>Cyrillus</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-50-sub-qa-catalog-dynamic-menu-level3-7">
					 <a class="js-scramble-link lvl3--link" href="/b/galeries+lafayette/c/enfant" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Top Marques-Galeries Lafayette">
					 <span>Galeries Lafayette</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-50-sub-qa-catalog-dynamic-menu-level3-8">
					 <a class="js-scramble-link lvl3--link" href="/b/ikks/c/enfant" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Top Marques-IKKS">
					 <span>IKKS</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-50-sub-qa-catalog-dynamic-menu-level3-9">
					 <a class="js-scramble-link lvl3--link" href="/b/karl+lagerfeld+kids" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Top Marques-Karl Lagerfeld Kids">
					 <span>Karl Lagerfeld Kids</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-50-sub-qa-catalog-dynamic-menu-level3-10">
					 <a class="js-scramble-link lvl3--link" href="/b/polo+ralph+lauren/c/enfant" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Top Marques-Polo Ralph Lauren">
					 <span>Polo Ralph Lauren</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-50-sub-qa-catalog-dynamic-menu-level3-11">
					 <a class="js-scramble-link lvl3--link" href="/b/timberland/c/enfant#ebp=SmMvxkqDL6S1a502afB.okXrLZCAcVg0cpiUH.k0LRZr" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Top Marques-Timberland">
					 <span>Timberland</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-50-sub-qa-catalog-dynamic-menu-level3-12">
					 <a class="js-scramble-link lvl3--link" href="/b/zadig+voltaire+kids" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Top Marques-Zadig &amp; Voltaire Kids">
					 <span>Zadig &amp; Voltaire Kids</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-51">
					 <a class="js-scramble-link lvl2--link" href="/marques/d/enfant/enfant-jeux-et-jouets/enfant-vetements/" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Toutes les marques">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Toutes les marques</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/marques/d/enfant/enfant-jeux-et-jouets/enfant-vetements/" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Enfant-Toutes les marques">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item lvl2__item--allUniverse" data-tnr-id="qa-catalog-dynamic-menu-level2-all-universe-xmedium-up-5">
					 <a class="js-scramble-link lvl2--link" href="/h/enfant" target="_self" data-lvl1="enfant" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Enfant">
					 <span class="lvl2__item__label lvl2__item__label--allUniverse">TOUT L'UNIVERS ENFANT</span>
					 </a>
					 </li>
					 </ul>
					 <ul class="menu__inspiration">
					 <li class="lvl2__item">
					 <span class="lvl2__item__label lvl2--link">Sélections</span>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/mini+me" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Mini Me">
					 <span>Mini Me</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/nouveautes-nouveautes+enfant" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Nouveautés">
					 <span>Nouveautés</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/looks+imprimes+enfant" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Imprimé Coloré">
					 <span>Imprimé Coloré</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/chemises+en+jean" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Chemises en jean">
					 <span>Chemises en jean</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/les+robes+de+la+saison" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Robes qui tournent">
					 <span>Robes qui tournent</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/boutique+ceremonie-boutique+ceremonie+enfant" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Boutique Cérémonie">
					 <span>Boutique Cérémonie</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/kids+et+accessoires" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Accessoires de saison">
					 <span>Accessoires de saison</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/cadeaux+de+naissance" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Cadeaux de naissance">
					 <span>Cadeaux de naissance</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/coups+de+coeur+beaux+jours-coups+de+coeur+beaux+jours+enfant" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Coups de coeur Beaux Jours, Bonjour">
					 <span>Coups de coeur Beaux Jours, Bonjour</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a href="/c/les+pepites+go+for+good+enfant" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push image|gaEvent_category:Navigation|gaEvent_label:/c/les+pepites+go+for+good+enfant">
					 <img src="//static.galerieslafayette.com/media/endeca2/02 HOMME/PUSH_MENU/ENFANT/PUSH_MENU_GFG.png" alt="">
					 </a>
					 </li>
					 </ul>
					 </span>
					 <span class="menu--overlay"></span>
					 </span>
					 </li>
					 <li class="lvl1__item" data-js="lvl1__item">
					 <a class="js-scramble-link lvl1--link" href="/h/maison" data-js="enable-lvl2" target="_self" data-lvl1="maison" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Maison">
					 <span class="lvl1__item__label lvl1__item__label--title picto__arrow--after ">Maison</span>
					 </a>
					 <span class="lvl1__item--center" data-js="lvl1SubElement">
					 <span class="item--1250" style="width: 1363px;">
					 <ul class="menu__lvl lvl2 lvl2--withRightBorder" data-js="lvl2">
					 <li class="show-for-medium-only lvl2__item lvl2--category hide-for-xmedium-up ">
					 <a class="js-scramble-link" href="/h/maison" target="_self">
					 <span class="medium show-for-medium-only">Tout l'univers Maison</span>
					 </a>
					 </li>
					 <li class="lvl2__item show-for-small-only">
					 <a href="#" data-js="backToLvl1" class="lvl2--link">
					 <span class="lvl2__item__label picto__arrow--before picto__arrow--before--left">Retour</span>
					 </a>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-52">
					 <a class="js-scramble-link lvl2--link" href="/c/maison-linge+de+maison" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Linge de maison">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Linge de maison</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/maison-linge+de+maison" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Linge de maison">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-52-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-linge+de+maison-linge+de+lit" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Linge de maison-Linge de lit">
					 <span>Linge de lit</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-52-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-linge+de+maison-linge+de+toilette" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Linge de maison-Linge de toilette">
					 <span>Linge de toilette</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-52-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-linge+de+maison-couettes+et+oreillers" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Linge de maison-Couettes et oreillers">
					 <span>Couettes et oreillers</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-52-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-linge+de+maison-linge+de+table" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Linge de maison-Linge de table">
					 <span>Linge de table</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-52-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-linge+de+maison-linge+de+cuisine" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Linge de maison-Linge de cuisine">
					 <span>Linge de cuisine</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-53">
					 <a class="js-scramble-link lvl2--link" href="/c/maison-meubles+et+canapes" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Meubles &amp; canapés">
					 <span data-js="rotate-available" class="lvl2__item__label">Meubles &amp; canapés</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/maison-meubles+et+canapes" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Meubles &amp; canapés">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-54">
					 <a class="js-scramble-link lvl2--link" href="/c/maison/ct/maison-luminaires" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Luminaire">
					 <span data-js="rotate-available" class="lvl2__item__label">Luminaire</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/maison/ct/maison-luminaires" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Luminaire">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-55">
					 <a class="js-scramble-link lvl2--link" href="/c/maison-rangement" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Rangement">
					 <span data-js="rotate-available" class="lvl2__item__label">Rangement</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/maison-rangement" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Rangement">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-56">
					 <a class="js-scramble-link lvl2--link" href="/c/maison-decoration+de+la+maison" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Décorations &amp; senteurs">
					 <span data-js="rotate-available" class="lvl2__item__label">Décorations &amp; senteurs</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/maison-decoration+de+la+maison" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Décorations &amp; senteurs">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-57">
					 <a class="js-scramble-link lvl2--link" href="/c/maison-cuisine+et+arts+de+la+table-oenologie" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Oenologie">
					 <span data-js="rotate-available" class="lvl2__item__label">Oenologie</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/maison-cuisine+et+arts+de+la+table-oenologie" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Oenologie">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-58">
					 <a class="js-scramble-link lvl2--link" href="/c/maison-literie" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Literie">
					 <span data-js="rotate-available" class="lvl2__item__label">Literie</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/maison-literie" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Literie">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-59">
					 <a class="js-scramble-link lvl2--link" href="/c/maison-maison+idees+cadeaux" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Coffrets &amp; Idées cadeaux">
					 <span data-js="rotate-available" class="lvl2__item__label">Coffrets &amp; Idées cadeaux</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/maison-maison+idees+cadeaux" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Coffrets &amp; Idées cadeaux">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-60">
					 <a class="js-scramble-link lvl2--link" href="/c/maison-cuisine+et+arts+de+la+table" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Cuisine et arts de la table">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Cuisine et arts de la table</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/maison-cuisine+et+arts+de+la+table" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Cuisine et arts de la table">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-60-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-cuisine+et+arts+de+la+table-culinaire" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Cuisine et arts de la table-Culinaire">
					 <span>Culinaire</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-60-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-cuisine+et+arts+de+la+table-arts+de+la+table" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Cuisine et arts de la table-Arts de la table">
					 <span>Arts de la table</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-61">
					 
					 <a class="js-scramble-link lvl2--link" href="/c/maison-multimedia" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Multimédia">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Multimédia</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/maison-multimedia" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Multimédia">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-61-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-multimedia-informatique" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Multimédia-Informatique">
					 <span>Informatique</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-61-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-multimedia-smartphone+et+gps" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Multimédia-Smartphone et GPS">
					 <span>Smartphone et GPS</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-61-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-multimedia-tv+image+et+son" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Multimédia-TV Image et Son">
					 <span>TV Image et Son</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-61-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-multimedia" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Multimédia-> tous les produits">
					 <span>&gt; tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-62">
					 <a class="js-scramble-link lvl2--link" href="/c/maison-electromenager/ct/maison-gros+electromenager" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Gros électroménager">
					 <span data-js="rotate-available" class="lvl2__item__label">Gros électroménager</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 
					 <a class="js-scramble-link all-categories" href="/c/maison-electromenager/ct/maison-gros+electromenager" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Gros électroménager">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-63">
					 <a class="js-scramble-link lvl2--link" href="/c/maison-electromenager/ct/maison-petit+electromenager" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Petit électroménager">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Petit électroménager</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/maison-electromenager/ct/maison-petit+electromenager" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Petit électroménager">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-63-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-electromenager/ct/maison-petit+electromenager-petit+dejeuner" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Petit électroménager-Petit déjeuner">
					 <span>Petit déjeuner</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-63-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-electromenager/ct/maison-petit+electromenager-preparation+culinaire" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Petit électroménager-Préparation culinaire">
					 <span>Préparation culinaire</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-63-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-electromenager/ct/maison-petit+electromenager-aspirateurs+vapeur" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Petit électroménager-Aspirateur, vapeur">
					 <span>Aspirateur, vapeur</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-63-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-electromenager/ct/maison-petit+electromenager-soin+du+corps" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Petit électroménager-Soin du corps">
					 <span>Soin du corps</span>
					 
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-63-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-electromenager/ct/maison-petit+electromenager-soin+du+cheveu" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Petit électroménager-Soin du cheveu">
					 <span>Soin du cheveu</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-63-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-electromenager/ct/maison-petit+electromenager-rasoirs+tondeuses" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Petit électroménager-Rasoir &amp; tondeuse">
					 <span>Rasoir &amp; tondeuse</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-63-sub-qa-catalog-dynamic-menu-level3-7">
					 <a class="js-scramble-link lvl3--link" href="/c/maison-electromenager/ct/maison-petit+electromenager" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Petit électroménager-> tous les produits">
					 <span>&gt; tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-64">
					 <a class="js-scramble-link lvl2--link" href="/c/go+for+good/ct/maison" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Maison responsable">
					 <span data-js="rotate-available" class="lvl2__item__label">Maison responsable</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/go+for+good/ct/maison" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Maison responsable">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-65">
					 <a class="js-scramble-link lvl2--link" href="/marques/d/maison-bricolage/maison-cuisine-et-arts-de-la-table/maison-electromenager/maison-high-tech/maison-librairie-papeterie-et-loisirs-creatifs/maison-linge-de-maison/maison-mobilier-et-decoration/" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Top Marques">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Top Marques</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 
					 <a class="js-scramble-link all-categories" href="/marques/d/maison-bricolage/maison-cuisine-et-arts-de-la-table/maison-electromenager/maison-high-tech/maison-librairie-papeterie-et-loisirs-creatifs/maison-linge-de-maison/maison-mobilier-et-decoration/" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Top Marques">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-65-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/b/absolument+maison/ct/maison" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Top Marques-Absolument maison">
					 <span>Absolument maison</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-65-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/b/am+pm+/ct/maison" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Top Marques-AMPM">
					 <span>AMPM</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-65-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/b/apple" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Top Marques-Apple">
					 <span>Apple</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-65-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/b/cristel" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Top Marques-Cristel">
					 <span>Cristel</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-65-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/b/degrenne" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Top Marques-Degrenne">
					 <span>Degrenne</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-65-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/b/drouault" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Top Marques-Drouault">
					 <span>Drouault</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-65-sub-qa-catalog-dynamic-menu-level3-7">
					 <a class="js-scramble-link lvl3--link" href="/b/boss+home/ct/maison" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Top Marques-Hugo Boss Home">
					 <span>Hugo Boss Home</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-65-sub-qa-catalog-dynamic-menu-level3-8">
					 <a class="js-scramble-link lvl3--link" href="/b/lacoste/ct/maison" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Top Marques-Lacoste">
					 <span>Lacoste</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-65-sub-qa-catalog-dynamic-menu-level3-9">
					 <a class="js-scramble-link lvl3--link" href="/b/la+redoute+interieurs/ct/maison" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Top Marques-La Redoute Intérieurs">
					 <span>La Redoute Intérieurs</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-65-sub-qa-catalog-dynamic-menu-level3-10">
					 <a class="js-scramble-link lvl3--link" href="/b/le+creuset" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Top Marques-Le Creuset">
					 <span>Le Creuset</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-65-sub-qa-catalog-dynamic-menu-level3-11">
					 <a class="js-scramble-link lvl3--link" href="/b/mauviel" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Top Marques-Mauviel">
					 <span>Mauviel</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-65-sub-qa-catalog-dynamic-menu-level3-12">
					 <a class="js-scramble-link lvl3--link" href="/b/ralph+lauren/ct/maison" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Top Marques-Ralph Lauren">
					 <span>Ralph Lauren</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-65-sub-qa-catalog-dynamic-menu-level3-13">
					 <a class="js-scramble-link lvl3--link" href="/b/villeroy+boch" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Top Marques-Villeroy &amp; Boch">
					 <span>Villeroy &amp; Boch</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-66">
					 <a class="js-scramble-link lvl2--link" href="/marques/d/maison-bricolage/maison-cuisine-et-arts-de-la-table/maison-electromenager/maison-high-tech/maison-librairie-papeterie-et-loisirs-creatifs/maison-linge-de-maison/maison-mobilier-et-decoration/" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Toutes les marques">
						<span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Toutes les marques</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/marques/d/maison-bricolage/maison-cuisine-et-arts-de-la-table/maison-electromenager/maison-high-tech/maison-librairie-papeterie-et-loisirs-creatifs/maison-linge-de-maison/maison-mobilier-et-decoration/" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Maison-Toutes les marques">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item lvl2__item--allUniverse" data-tnr-id="qa-catalog-dynamic-menu-level2-all-universe-xmedium-up-6">
					 <a class="js-scramble-link lvl2--link" href="/h/maison" target="_self" data-lvl1="maison" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Maison">
					 <span class="lvl2__item__label lvl2__item__label--allUniverse">TOUT L'UNIVERS MAISON</span>
					 </a>
					 </li>
					 </ul>
					 <ul class="menu__inspiration">
					 <li class="lvl2__item">
					 <span class="lvl2__item__label lvl2--link">Sélections</span>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/offre+electromenager+multimedia/tri/meilleures-ventes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Bons plans électro &amp; Multimédia">
					 <span>Bons plans électro &amp; Multimédia</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/maison/fgfg/fabrique+en+france" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Made in France">
					 <span>Made in France</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/teletravail" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Sélection télétravail">
					 <span>Sélection télétravail</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/evt/fr/shoppingadistance" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Nouveau service : Shopping à distance">
					 <span>Nouveau service : Shopping à distance</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/c/go+for+good/ct/maison" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Sélections-Sélection responsable">
					 <span>Sélection responsable</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a href="/c/teletravail" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push image|gaEvent_category:Navigation|gaEvent_label:/c/teletravail">
					 <img src="//static.galerieslafayette.com/media/endeca2/03 MAISON_GOURMET/NOUVELLE_HOME/2021/TENDANCES/Teletravail/teletravail.jpg" alt="">
					 </a>
					 </li>
					 </ul>
					 </span>
					 <span class="menu--overlay"></span>
					 </span>
					 </li>
					 <li class="lvl1__item" data-js="lvl1__item">
					 <a class="js-scramble-link lvl1--link" href="/c/go+for+good" data-js="enable-lvl2" target="_self" data-lvl1="moderesponsable" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable">
					 <span class="lvl1__item__label lvl1__item__label--title picto__arrow--after ">Mode responsable</span>
					 </a>
					 <span class="lvl1__item--center" data-js="lvl1SubElement">
					 <span class="item--1250" style="width: 1363px;">
					 <ul class="menu__lvl lvl2 lvl2--withRightBorder" data-js="lvl2">
					 <li class="show-for-medium-only lvl2__item lvl2--category hide-for-xmedium-up ">
					 <a class="js-scramble-link" href="/c/go+for+good" target="_self">
					 <span class="medium show-for-medium-only">Tout l'univers Mode responsable</span>
					 </a>
					 </li>
					 <li class="lvl2__item show-for-small-only">
					 <a href="#" data-js="backToLvl1" class="lvl2--link">
					 <span class="lvl2__item__label picto__arrow--before picto__arrow--before--left">Retour</span>
					 </a>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-67">
					 <a class="js-scramble-link lvl2--link" href="/c/go+for+good/ct/femme" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Femme">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Femme</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/go+for+good/ct/femme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Femme">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-67-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/ct/femme-vetements" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Femme-Vêtements">
					 <span>Vêtements</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-67-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/ct/femme-sacs+et+bagages" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Femme-Maroquinerie">
					 <span>Maroquinerie</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-67-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/ct/femme-chaussures" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Femme-Chaussures">
					 <span>Chaussures</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-67-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/ct/femme-bijouterie+et+joaillerie" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Femme-Bijouterie et Joaillerie">
					 <span>Bijouterie et Joaillerie</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-67-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/accessoires-accessoires+femme-montres/fgfg/autres+criteres/cuir+tannage+vegetal+ou+sans+chrome/fabrique+en+france" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Femme-Montres">
					 <span>Montres</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-67-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/ct/femme-lingerie" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Femme-Lingerie">
					 <span>Lingerie</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-68">
					 <a class="js-scramble-link lvl2--link" href="/c/go+for+good/ct/maison" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Maison">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Maison</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/go+for+good/ct/maison" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Maison">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-68-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/ct/maison-linge+de+maison" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Maison-Linge de Maison">
					 <span>Linge de Maison</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-68-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/ct/maison-decoration+-+senteurs" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Maison-Décoration">
					 <span>Décoration</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-68-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/ct/maison-culinaire" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Maison-Culinaire">
					 <span>Culinaire</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-68-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/ct/maison-arts+de+la+table" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Maison-Arts de la table">
					 <span>Arts de la table</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-69">
					 <a class="js-scramble-link lvl2--link" href="/c/go+for+good/ct/beaute" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Beauté">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Beauté</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/go+for+good/ct/beaute" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Beauté">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-70">
					 <a class="js-scramble-link lvl2--link" href="/c/go+for+good/ct/homme" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Homme">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Homme</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/go+for+good/ct/homme" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Homme">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-70-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/ct/homme-vetements" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Homme-Vêtements">
					 <span>Vêtements</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-70-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/ct/homme-sacs+et+bagages" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Homme-Sacs et bagages">
					 <span>Sacs et bagages</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-70-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/ct/homme-chaussures" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Homme-Chaussures">
					 <span>Chaussures</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-70-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/ct/homme-sous-vetements+et+homewear" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Homme-Sous-vêtements">
					 <span>Sous-vêtements</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-70-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/accessoires-accessoires+homme-montres/fgfg/fabrique+en+france/autres+criteres" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Homme-Montres">
					 <span>Montres</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-71">
					 <a class="js-scramble-link lvl2--link" href="/c/go+for+good-enfant" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Enfant">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Enfant</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/go+for+good-enfant" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Enfant">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-72">
					 <a class="js-scramble-link lvl2--link" href="/c/go+for+good" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Critères responsables">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Top Critères responsables</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/go+for+good" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Critères responsables">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-72-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/fgfg/coton+bio" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Critères responsables-Coton Bio">
					 <span>Coton Bio</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-72-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/fgfg/cosmetique+naturelle" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Critères responsables-Cosmétique Naturelle">
					 <span>Cosmétique Naturelle</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-72-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/fgfg/fabrique+en+france" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Critères responsables-Made in France">
					 <span>Made in France</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-72-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/fgfg/matiere+recyclee" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Critères responsables-Matière Recyclée">
					 <span>Matière Recyclée</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-73">
					 <a class="js-scramble-link lvl2--link" href="/c/go+for+good" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Marques responsables">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Top Marques responsables</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/go+for+good" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Marques responsables">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-73-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/fm/bobo+choses" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Marques responsables-Bobo choses">
					 <span>Bobo choses</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-73-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/fm/cadet+rousselle" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Marques responsables-Cadet Rousselle">
					 <span>Cadet Rousselle</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-73-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/fm/casablanca" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Marques responsables-Casablanca">
					 <span>Casablanca</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-73-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/fm/galeries+lafayette" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Marques responsables-Galeries Lafayette">
					 <span>Galeries Lafayette</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-73-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/fm/ganni" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Marques responsables-Ganni">
					 <span>Ganni</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-73-sub-qa-catalog-dynamic-menu-level3-6">
					 
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/fm/lemaire" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Marques responsables-Lemaire">
					 <span>Lemaire</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-73-sub-qa-catalog-dynamic-menu-level3-7">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/fm/marine+serre" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Marques responsables-Marine Serre">
					 <span>Marine Serre</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-73-sub-qa-catalog-dynamic-menu-level3-8">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/fm/martinelli+luce" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Marques responsables-Martinelli Luce">
					 <span>Martinelli Luce</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-73-sub-qa-catalog-dynamic-menu-level3-9">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/fm/respire" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Marques responsables-Respire">
					 <span>Respire</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-73-sub-qa-catalog-dynamic-menu-level3-10">
					 <a class="js-scramble-link lvl3--link" href="/c/go+for+good/fm/studio+celeste" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable-Top Marques responsables-Studio Celeste">
					 <span>Studio Celeste</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item lvl2__item--allUniverse" data-tnr-id="qa-catalog-dynamic-menu-level2-all-universe-xmedium-up-7">
					 <a class="js-scramble-link lvl2--link" href="/c/go+for+good" target="_self" data-lvl1="moderesponsable" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Mode responsable">
					 <span class="lvl2__item__label lvl2__item__label--allUniverse">TOUT L'UNIVERS MODE RESPONSABLE</span>
					 </a>
					 </li>
					 </ul>
					 <ul class="menu__inspiration">
					 <li class="lvl2__item">
					 <span class="lvl2__item__label lvl2--link">Go for Good</span>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/evt/animations/go-for-good" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Go for Good-Découvrir nos engagements">
					 <span>Découvrir nos engagements</span>
					 </a>
					 </li>
					 
					 <li class="lvl3--item">
					 <a href="/c/go+for+good" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push image|gaEvent_category:Navigation|gaEvent_label:/c/go+for+good">
					 <img src="//static.galerieslafayette.com/media/endeca2/06 OP PAC generiques/3_OPES/CHANGEONS_DE_MODE/PushMenu_gfg.jpg" alt="">
					 </a>
					 </li>
					 </ul>
					 </span>
					 <span class="menu--overlay"></span>
					 </span>
					 </li>
					 <li class="lvl1__item" data-js="lvl1__item">
					 <a class="js-scramble-link lvl1--link" href="/h/createurs" data-js="enable-lvl2" target="_self" data-lvl1="luxecréateurs" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs">
					 <span class="lvl1__item__label lvl1__item__label--title picto__arrow--after ">Luxe &amp; Créateurs</span>
					 </a>
					 <span class="lvl1__item--center" data-js="lvl1SubElement">
					 <span class="item--1250" style="width: 1363px;">
					 <ul class="menu__lvl lvl2 lvl2--withRightBorder" data-js="lvl2">
					 <li class="show-for-medium-only lvl2__item lvl2--category hide-for-xmedium-up ">
					 <a class="js-scramble-link" href="/h/createurs" target="_self">
					 <span class="medium show-for-medium-only">Tout l'univers Luxe &amp; Créateurs</span>
					 </a>
					 </li>
					 <li class="lvl2__item show-for-small-only">
					 <a href="#" data-js="backToLvl1" class="lvl2--link">
					 <span class="lvl2__item__label picto__arrow--before picto__arrow--before--left">Retour</span>
					 </a>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74">
					 <a class="js-scramble-link lvl2--link" href="/c/marques+luxe+et+createurs" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Luxe</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/marques+luxe+et+createurs" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/alexander-mcqueen" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Alexander McQueen">
					 <span>Alexander McQueen</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/b/balmain" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Balmain">
						<span>Balmain</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/bottega-veneta" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Bottega Veneta">
					 <span>Bottega Veneta</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/burberry" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Burberry">
					 <span>Burberry</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/cartier" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Cartier">
					 <span>Cartier</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/celine" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Celine">
					 <span>Celine</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-7">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/dior" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Dior">
					 <span>Dior</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-8">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/dolce-gabbana" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Dolce &amp; Gabbana">
					 <span>Dolce &amp; Gabbana</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-9">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/eres" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Eres">
					 <span>Eres</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-10">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/fendi" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Fendi">
					 <span>Fendi</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-11">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/fred" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Fred">
					 <span>Fred</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-12">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/givenchy" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Givenchy">
					 <span>Givenchy</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-13">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/jimmy-choo" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Jimmy Choo">
					 <span>Jimmy Choo</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-14">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/weston" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-J.M. Weston">
					 <span>J.M. Weston</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-15">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/kenzo" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Kenzo">
					 <span>Kenzo</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-16">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/loewe" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Loewe">
					 <span>Loewe</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-17">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/messika" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Messika">
					 <span>Messika</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-18">
					 <a class="js-scramble-link lvl3--link" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/miu-miu" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Miu Miu">
					 <span>Miu Miu</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-19">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/prada" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Prada">
					 <span>Prada</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-20">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/roger-vivier" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Roger Vivier">
					 <span>Roger Vivier</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-21">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/rolex" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Rolex">
					 <span>Rolex</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-22">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/saint-laurent" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Saint Laurent">
					 <span>Saint Laurent</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-23">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/tods" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Tod's">
					 <span>Tod's</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-24">
					 <a class="js-scramble-link lvl3--link" href="/evt/fr/shoppingadistance/valentino" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Valentino">
					 <span>Valentino</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-25">
					 <a class="js-scramble-link lvl3--link" href="/b/valextra" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Valextra">
					 <span>Valextra</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-74-sub-qa-catalog-dynamic-menu-level3-26">
					 <a class="js-scramble-link lvl3--link" href="https://www.galerieslafayette.com/b/versace" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Luxe-Versace">
					 <span>Versace</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75">
					 <a class="js-scramble-link lvl2--link" href="/c/marques+luxe+et+createurs" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Créateurs</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/marques+luxe+et+createurs" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/b/a+p+c+" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-A.P.C.">
					 <span>A.P.C.</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/b/by+far" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-By Far">
					 <span>By Far</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/b/comme+des+garcons+play" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-Comme des Garçons PLAY">
					 <span>Comme des Garçons PLAY</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-4">
					 
					 <a class="js-scramble-link lvl3--link" href="/b/danse+lente" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-Danse Lente">
					 <span>Danse Lente</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-5">
					 <a class="js-scramble-link lvl3--link" href="/b/etudes" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-Etudes">
					 <span>Etudes</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/b/heron+preston" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-Heron Preston">
					 <span>Heron Preston</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-7">
					 <a class="js-scramble-link lvl3--link" href="/b/homme+plisse" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-Homme Plissé">
					 <span>Homme Plissé</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-8">
					 <a class="js-scramble-link lvl3--link" href="/b/isabel+marant" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-Isabel Marant">
					 <span>Isabel Marant</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-9">
					 <a class="js-scramble-link lvl3--link" href="/b/jacquemus" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-Jacquemus">
					 <span>Jacquemus</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-10">
					 <a class="js-scramble-link lvl3--link" href="/b/lemaire" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-Lemaire">
					 <span>Lemaire</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-11">
					 <a class="js-scramble-link lvl3--link" href="/b/maison+michel" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-Maison Michel">
					 <span>Maison Michel</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-12">
					 <a class="js-scramble-link lvl3--link" href="https://www.galerieslafayette.com/b/mira+mikati" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-Mira Mikati">
					 <span>Mira Mikati</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-13">
					 <a class="js-scramble-link lvl3--link" href="/b/off+white" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-Off White">
					 <span>Off White</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-14">
					 <a class="js-scramble-link lvl3--link" href="/b/simone+rocha" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-Simone Rocha">
					 <span>Simone Rocha</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-15">
					 <a class="js-scramble-link lvl3--link" href="/b/stalvey" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-Stalvey">
					 <span>Stalvey</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-16">
					 <a class="js-scramble-link lvl3--link" href="/b/staud" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-Staud">
					 <span>Staud</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-17">
					 <a class="js-scramble-link lvl3--link" href="/b/vince" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-Vince">
					 <span>Vince</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-18">
					 <a class="js-scramble-link lvl3--link" href="https://www.galerieslafayette.com/b/wandler" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-Wandler">
					 <span>Wandler</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-75-sub-qa-catalog-dynamic-menu-level3-19">
					 <a class="js-scramble-link lvl3--link" href="/b/y-3" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Créateurs-Y-3">
					 <span>Y-3</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-76">
					 <a class="js-scramble-link lvl2--link" href="/c/marques+luxe+et+createurs" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Top catégories">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Top catégories</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/c/marques+luxe+et+createurs" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Top catégories">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-76-sub-qa-catalog-dynamic-menu-level3-1">
					 <a class="js-scramble-link lvl3--link" href="/c/marques+luxe+et+createurs/ct/femme-vetements" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Top catégories-Vêtements femme">
					 <span>Vêtements femme</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-76-sub-qa-catalog-dynamic-menu-level3-2">
					 <a class="js-scramble-link lvl3--link" href="/c/marques+luxe+et+createurs/ct/homme-vetements" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Top catégories-Vêtements homme">
					 <span>Vêtements homme</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-76-sub-qa-catalog-dynamic-menu-level3-3">
					 <a class="js-scramble-link lvl3--link" href="/c/marques+luxe+et+createurs/ct/femme-sacs+et+bagages" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Top catégories-Maroquinerie">
					 <span>Maroquinerie</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-76-sub-qa-catalog-dynamic-menu-level3-4">
					 <a class="js-scramble-link lvl3--link" href="/c/chaussures+marques+luxe+et+createurs" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Top catégories-Chaussures">
					 <span>Chaussures</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-76-sub-qa-catalog-dynamic-menu-level3-5">
						<a class="js-scramble-link lvl3--link" href="/c/sneakers+marques+luxe+et+createurs" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Top catégories-Sneakers">
					 <span>Sneakers</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-76-sub-qa-catalog-dynamic-menu-level3-6">
					 <a class="js-scramble-link lvl3--link" href="/c/marques+luxe+et+createurs/ct/femme-bijouterie+et+joaillerie" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Top catégories-Bijoux">
					 <span>Bijoux</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-76-sub-qa-catalog-dynamic-menu-level3-7">
					 <a class="js-scramble-link lvl3--link" href="/c/marques+luxe+et+createurs/ct/beaute" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Top catégories-Beauté">
					 <span>Beauté</span>
					 </a>
					 </li>
					 <li class="lvl3--item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-76-sub-qa-catalog-dynamic-menu-level3-8">
					 <a class="js-scramble-link lvl3--link" href="/c/marques+luxe+et+createurs" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Top catégories-Tout voir">
					 <span>Tout voir</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item" data-tnr-id="qa-catalog-dynamic-menu-level2-category-77">
					 <a class="js-scramble-link lvl2--link" href="/evt/fr/shoppingadistance/index-luxe-createurs" data-js="enable-lvl3" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Toutes les marques">
					 <span data-js="rotate-available" class="lvl2__item__label picto__arrow--after ">Toutes les marques</span>
					 </a>
					 <ul class="menu__lvl lvl3" data-js="lvl3">
					 <li class="lvl3--item show-for-small-only">
					 <a class="js-scramble-link all-categories" href="/evt/fr/shoppingadistance/index-luxe-createurs" target="_self" data-ga="gaEvent|gaEvent_action:Sous menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs-Toutes les marques">
					 <span>Tous les produits</span>
					 </a>
					 </li>
					 </ul>
					 </li>
					 <li class="lvl2__item lvl2__item--allUniverse" data-tnr-id="qa-catalog-dynamic-menu-level2-all-universe-xmedium-up-8">
					 <a class="js-scramble-link lvl2--link" href="/h/createurs" target="_self" data-lvl1="luxecréateurs" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Luxe &amp; Créateurs">
					 <span class="lvl2__item__label lvl2__item__label--allUniverse">TOUT L'UNIVERS LUXE &amp; CRÉATEURS</span>
					 </a>
					 </li>
					 </ul>
					 <ul class="menu__inspiration">
					 <li class="lvl2__item">
					 <span class="lvl2__item__label lvl2--link">Nouveau service</span>
					 </li>
					 <li class="lvl3--item">
					 <a class="js-scramble-link" href="/evt/fr/shoppingadistance" target="_self" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push text|gaEvent_category:Navigation|gaEvent_label:Nouveau service-Découvrir le shopping à distance">
					 <span>Découvrir le shopping à distance</span>
					 </a>
					 </li>
					 <li class="lvl3--item">
					 <a href="/c/marques+luxe+et+createurs/ct/femme-sacs+et+bagages" data-ga="gaEvent|gaEvent_action:Sous sous menu - Push image|gaEvent_category:Navigation|gaEvent_label:/c/marques+luxe+et+createurs/ct/femme-sacs+et+bagages">
					 <img src="//static.galerieslafayette.com/media/endeca2/01 FEMME/Onglet_createur/Maroquinerie.jpg" alt="">
					 </a>
					 </li>
					 </ul>
					 </span>
					 <span class="menu--overlay"></span>
					 </span>
					 </li>
					 <li class="lvl1__item" data-js="lvl1__item">
					 <a class="js-scramble-link lvl1--link" href="/marques" target="_self" data-lvl1="marques" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Marques">
					 <span class="lvl1__item__label lvl1__item__label--title">Marques</span>
					 </a>
					 </li>
					 <li class="lvl1__item" data-js="lvl1__item">
					 <a class="js-scramble-link lvl1--link" href="https://www.galerieslafayette.com/services" target="_self" data-lvl1="nosservices" data-ga="gaEvent|gaEvent_action:Menu|gaEvent_category:Navigation|gaEvent_label:Nos Services">
					 <span class="lvl1__item__label lvl1__item__label--title">Nos Services</span>
					 </a>
					 </li>
					 </ul>
					 </div>
					 </div>
					 <div class="menu-wrapper__search columns small-24 xmedium-6">
					 <div class="header__search">
					 <span class="search">
					 <div class="search--bloc row" data-js="search--bloc">
						
					 <form data-js="search-form" class="search--forms small-24 columns">
					 <span class="algolia-autocomplete" style="position: relative; display: inline-block; direction: ltr;"><input data-js="search-input-text" autocomplete="off" class="search--input autocompletion-input-algolia algolia-desktop aa-input" maxlength="200" name="search" type="text" data-algolia="{&quot;app&quot;:&quot;CDZ6BKNQRW&quot;,&quot;brandsIndex&quot;:&quot;prod_gl_products_brands&quot;,&quot;productsIndex&quot;:&quot;prod_gl_products&quot;,&quot;topCatalogsIndex&quot;:&quot;prod_gl_products_top_catalogs&quot;,&quot;key&quot;:&quot;e4306e50d5efa978338e2b3b29bfaa9c&quot;}" data-loyalty="false" spellcheck="false" role="combobox" aria-autocomplete="both" aria-expanded="false" aria-owns="algolia-autocomplete-listbox-0" dir="auto" style="position: relative; vertical-align: top;" placeholder="Rechercher une marque, un article..."><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: gl_bader_narrow-regular; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: normal; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre></span>
					 <input class="picto__search show-for-xmedium-up" type="submit" value="OK" data-ga="gaEvent|gaEvent_action:Search|gaEvent_category:Header|gaEvent_label:Recherche">
					 <input class="picto__search mobile show-for-xmedium-down" type="submit" value="OK" data-ga="gaEvent|gaEvent_action:Search|gaEvent_category:Header|gaEvent_label:Recherche">
					 <input class="picto__search-wrapper hide show-for-xmedium-down picto__close" type="submit" value="" data-js="search--close">
					 <span class="hide" data-js="search-input-text-placeholder">Rechercher une marque, un article...</span>
					 <span class="hide" data-js="search-input-text-placeholder-short">Rechercher...</span>
					 </form>
					 <div class="search--autosuggestions column small-24 js-autocomplete-suggestions-algolia hide" data-tnr-id="qa-catalog-dynamic-search-autocompletion-feature" style="display: none; position: relative;">
					 <ul class="searchscreen-algolia">
					 <li class="aa-column">
					 <div class="aa-title">
					 <span>Top Recherche</span>
					 <ul class="aa-suggestion">
					 <li><a href="/b/longchamp">Longchamp</a></li>
					 <li><a href="/b/boss">Boss</a></li>
					 <li><a href="/b/chanel">CHANEL</a></li>
					 </ul>
					 </div>
					 </li>
					 <li class="aa-column">
					 <div class="aa-title">
					 <span>Raccourcis</span>
					 <ul class="aa-suggestion">
					 <li><a href="https://www.galerieslafayette.com/c/sport">Sélection sport</a></li>
					 <li><a href="https://www.galerieslafayette.com/c/nouveautes">Nouveautés</a></li>
					 <li><a href="https://www.galerieslafayette.com/c/go+for+good">Mode responsable</a></li>
					 </ul>
					 </div>
					 </li>
					 </ul>
					 <span class="aa-dropdown-menu hide" role="listbox" id="algolia-autocomplete-listbox-0" style="position: absolute; z-index: 100; display: none; top: 0px; left: 0px; right: auto;"><div class="algolia-dropdown">
									 <div class="aa-column">
											 <div>
													 <p class="aa-title-mobile aa-title ">
															 <span>Articles</span>
															 <span class="nbHitsAlgoliaHead"></span>
													 </p>
													 <ul class="aa-dataset-products">
													 </ul>
													 <div class="aa-btn-article_wrapper">
															 <a class="see_all_products aa-btn-article ripple-circle" href="">
																	 <span>Voir tous les articles</span>
																	 <span class="nbHitsAlgoliaFoot"></span>
															 </a>
													 </div>
					 
											 </div>
									 </div>
					 
									 <div class="aa-column">
											 <div>
													 <div>
															 <p class="aa-title-mobile aa-title">Marques</p>
															 <ul class="aa-dataset-brands">
															 </ul>
													 </div>
					 
													 <div class="aa-catalog clearboth">
															 <p class="aa-title-mobile aa-title">Catégories</p>
															 <ul class="aa-dataset-catalogs">
															 </ul>
													 </div>
											 </div>
									 </div>
					 </div>
					 <div class="aa-empty" style="display: none;"></div></span></div>
					 </div>
					 </span>
					 </div>
					 </div>
					 </div>
					 </div>
			</header>
			<div class="menu__overlay" data-js="menu__overlay"></div>
			<div class="header--shadow" data-js="header--shadow"></div>
			<!-- <div class="covidb">EXCLUSIVE LIVE SHOPPING : réalisez votre shopping luxe et créateurs en live vidéo. <u class="linkec"> <a href="https://www.galerieslafayette.com/evt/fr/live">Découvrir </a></u></div> -->
			<div id="dynamic" class="page">
				 <div class="main-nav off-canvas-wrap ab684263--inline--css--header" data-offcanvas="">
						<div id="wrapper-null" class="inner-wrap">
							 <section class="section-main-container">
