<!DOCTYPE html>
<html lang="fr">
		<head>
				<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">
				<link rel="icon" href="https://www.galerieslafayette.com/favicon.ico">
				<link rel="dns-prefetch" href="https://cdn.trustcommander.net">
				<link rel="dns-prefetch" href="https://cdn.tagcommander.com">
				<link rel="dns-prefetch" href="https://privacy.trustcommander.net">
				<link rel="dns-prefetch" href="https://ea.galerieslafayette.com">
				<link rel="preload" as="font" type="font/woff" href="https://www.galerieslafayette.com/ggl-front-assets.GLBaderCompress-Black.woff" crossorigin="anonymous">
				<link rel="preload" as="font" type="font/woff" href="https://www.galerieslafayette.com/ggl-front-assets.GLBaderCompress-Regular.woff" crossorigin="anonymous">
				<link rel="preload" as="font" type="font/woff" href="https://www.galerieslafayette.com/ggl-front-assets.GLBaderNarrow-Bold.woff" crossorigin="anonymous">
				<link rel="preload" as="font" type="font/woff" href="https://www.galerieslafayette.com/ggl-front-assets.GLBaderNarrow-Italic.woff" crossorigin="anonymous">
				<link rel="preload" as="font" type="font/woff" href="https://www.galerieslafayette.com/ggl-front-assets.GLBaderNarrow-Regular.woff" crossorigin="anonymous">
				<style type="text/css">
						@font-face {
								font-family: 'GLBaderCompress-Black';
								font-display: fallback;
								src: url('https://www.galerieslafayette.com/ggl-front-assets.GLBaderCompress-Black.woff') format('woff');
						}

						@font-face {
								font-family: 'GLBaderCompress-Regular';
								font-display: fallback;
								src: url('https://www.galerieslafayette.com/ggl-front-assets.GLBaderCompress-Regular.woff') format('woff');
						}

						@font-face {
								font-family: 'GLBaderNarrow-Bold';
								font-display: fallback;
								src: url('https://www.galerieslafayette.com/ggl-front-assets.GLBaderNarrow-Bold.woff') format('woff');
						}

						@font-face {
								font-family: 'GLBaderNarrow-Italic';
								font-display: fallback;
								src: url('https://www.galerieslafayette.com/ggl-front-assets.GLBaderNarrow-Italic.woff') format('woff');
						}

						@font-face {
								font-family: 'GLBaderNarrow-Regular';
								font-display: fallback;
								src: url('https://www.galerieslafayette.com/ggl-front-assets.GLBaderNarrow-Regular.woff') format('woff');
						}
				</style>
				<title>Vêtements homme : un large choix de vêtements | Galeries Lafayette</title>
				<meta data-vue-meta="ssr" data-vmid="title" name="title" content="Vêtements homme : un large choix de vêtements | Galeries Lafayette">
				<meta data-vue-meta="ssr" data-vmid="description" name="description" content="Retrouvez un large choix de vêtements homme homme sur la boutique en ligne GaleriesLafayette.com. Choisissez parmi une variété de genres et coloris adaptés à votre style. Livraison gratuite en magasin !">
				<script type="text/javascript" src="https://www.galerieslafayette.com/js/ruxitagentjs_ICA2SVfhqru_10207210127152629.js" data-dtconfig="rid=RID_286127576|rpid=874426528|domain=galerieslafayette.com|reportUrl=/js/rb_bf52497ugc|app=0be0a091e18da3a3|featureHash=ICA2SVfhqru|vcv=2|rdnt=1|uxrgce=1|bp=2|cuc=85ltyxmb|mel=100000|dpvc=1|md=mdcc1=cJSESSIONID,mdcc2=cJSESSIONID|lastModification=1611852450055|dtVersion=10207210127152629|tp=500,50,0,1|uxdcw=1500|vs=2|agentUri=/js/ruxitagentjs_ICA2SVfhqru_10207210127152629.js"></script>
				<link data-vue-meta="ssr" rel="canonical" href="https://www.galerieslafayette.com/c/homme/vetements">
				<link data-vue-meta="ssr" rel="next" href="https://www.galerieslafayette.com/c/homme/vetements/p:2">
				<link rel="preload" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-vendors.c655017b.js" as="script">
				<link rel="preload" href="https://www.galerieslafayette.com/ggl-front-assets.accountOrderDetails~accountOrderReturn~accountOrders~headerAppPage~homePage~landingStoresPage~notFou~73bdb2b0.c655017b.js" as="script">
				<link rel="preload" href="https://www.galerieslafayette.com/ggl-front-assets.productListPage.c655017b.js" as="script">
				<link rel="preload" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-6508a946.c655017b.js" as="script">
				<link rel="preload" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-363bfbf4.c655017b.js" as="script">
				<link rel="preload" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-2d665f02.c655017b.js" as="script">
				<link rel="preload" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-c711f114.c655017b.js" as="script">
				<link rel="preload" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-584e8b65.c655017b.js" as="script">
				<link rel="preload" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-30f626c0.c655017b.js" as="script">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.accountOrderDetails.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.accountOrderReturn.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.accountOrders.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.accountPage.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-04edce42.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-08a3eddb.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-143638d8.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-1b9a86d4.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-203e59bb.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-2365ecec.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-2d0ae5a4.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-3c0b64b9.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-3f116756.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-41761489.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-42bdc20c.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-4c961930.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-520bb367.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-529f0313.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-564bbdd8.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-5e5ecb83.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-5e96d30a.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-5f5b8cf7.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-62f8e110.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-636c5c53.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-715f00e4.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-7386308a.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-8daa1b36.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-9ee0e7b6.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-9ff793ce.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-bcf0c9e8.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.chunk-def9d33e.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.footerAppPage.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.headerAppPage.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.homePage.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.landingStoresPage.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.lang-en.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.notFoundPage.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.paymentPage.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.preHomePage.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.thanksPage.c655017b.js">
				<link rel="prefetch" href="https://www.galerieslafayette.com/ggl-front-assets.uriParserPage.c655017b.js">
				<style data-vue-ssr-id="b581268c:0 0def6319:0 6801b7c2:0 29a85a35:0 20acdb20:0 11e37ccc:0 302a9ffe:0 0de44942:0 64939b86:0 117fe1b5:0 ceab51ac:0 21ea9d99:0 143664cf:0 50895d32:0 77bf7d11:0 9ce4bb7a:0 ac8da7ba:0 bc028b88:0 6e061c71:0 22461892:0 610ec72e:0 0874240b:0 b0c9b356:0 0905f884:0 6957e3e6:0 46050a70:0 360d2d18:0 625b80ff:0 5991eddb:0 925bcc22:0 66bc98be:0 7f5d5fed:0">
						.title-l,.title-xl,.title-xxl {
								font-size: 4.5rem;
								line-height: 4.25rem;
								letter-spacing: -.15625rem
						}

						.title-l,.title-m,.title-xl,.title-xxl {
								font-family: GLBaderCompress-Black,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif
						}

						.title-m {
								font-size: 2.5rem;
								line-height: 2.5rem;
								letter-spacing: -.0625rem
						}

						.title-s,.title-xs {
								font-size: 1.75rem;
								line-height: 1.75rem
						}

						.title-s,.title-xs,.title-xxs {
								font-family: GLBaderCompress-Black,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								letter-spacing: -.03125rem
						}

						.title-xxs {
								font-size: 1.25rem;
								line-height: 1.25rem
						}

						.title-xxxs {
								font-family: GLBaderCompress-Black,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								font-size: 1rem;
								line-height: 1rem
						}

						.quote-l {
								font-size: 3.5rem;
								line-height: 3.5rem
						}

						.quote-l,.subtitle-l {
								font-family: GLBaderNarrow-Italic,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								letter-spacing: -.0625rem
						}

						.subtitle-l {
								font-size: 2.75rem;
								line-height: 2.75rem
						}

						.subtitle-m {
								font-size: 2rem;
								line-height: 2rem
						}

						.subtitle-m,.subtitle-s {
								font-family: GLBaderNarrow-Italic,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								letter-spacing: -.04375rem
						}

						.subtitle-s {
								font-size: 1.25rem;
								line-height: 1.25rem
						}

						.body-l {
								font-size: 1.25rem;
								line-height: 1.75rem
						}

						.body-l,.body-m {
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif
						}

						.body-m {
								font-size: 1rem;
								line-height: 1.25rem
						}

						.body-s {
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								font-size: .75rem;
								line-height: .9375rem
						}

						.action-m {
								font-family: GLBaderCompress-Black,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								font-size: .875rem;
								line-height: .875rem
						}

						.action-m,.action-s {
								letter-spacing: .0625rem
						}

						.action-s {
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								font-size: .625rem;
								line-height: .625rem
						}

						@media only screen and (min-width: 768px) {
								.title-m {
										font-size:3.5rem;
										line-height: 3.5rem;
										letter-spacing: -.125rem
								}

								.title-m,.title-xxxs {
										font-family: GLBaderCompress-Black,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif
								}

								.title-xxxs {
										font-size: 1.25rem;
										line-height: 1.25rem;
										letter-spacing: -.0125rem
								}
						}

						@media only screen and (min-width: 1025px) {
								.title-xxl {
										font-size:15rem;
										line-height: 13.75rem;
										letter-spacing: -.5625rem
								}

								.title-xl,.title-xxl {
										font-family: GLBaderCompress-Black,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif
								}

								.title-xl {
										font-size: 10rem;
										line-height: 9.5rem;
										letter-spacing: -.375rem
								}

								.title-l {
										font-size: 6rem;
										line-height: 5.75rem;
										letter-spacing: -.25rem
								}

								.title-l,.title-m {
										font-family: GLBaderCompress-Black,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif
								}

								.title-m {
										font-size: 4.5rem;
										line-height: 4.25rem;
										letter-spacing: -.15625rem
								}

								.title-s {
										font-size: 3.5rem;
										line-height: 3.5rem;
										letter-spacing: -.125rem
								}

								.title-s,.title-xs {
										font-family: GLBaderCompress-Black,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif
								}

								.title-xs {
										font-size: 2.5rem;
										line-height: 2.5rem;
										letter-spacing: -.0625rem
								}

								.title-xxs {
										font-size: 1.75rem;
										line-height: 1.75rem;
										letter-spacing: -.03125rem
								}

								.title-xxs,.title-xxxs {
										font-family: GLBaderCompress-Black,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif
								}

								.title-xxxs {
										font-size: 1.5rem;
										line-height: 1.5rem;
										letter-spacing: -.025rem
								}

								.quote-l {
										font-family: GLBaderNarrow-Italic,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
										font-size: 8rem;
										line-height: 8rem;
										letter-spacing: -.125rem
								}

								.subtitle-l {
										font-size: 4rem;
										line-height: 4rem
								}

								.subtitle-l,.subtitle-m {
										font-family: GLBaderNarrow-Italic,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
										letter-spacing: -.0625rem
								}

								.subtitle-m {
										font-size: 2.75rem;
										line-height: 2.75rem
								}

								.subtitle-s {
										font-family: GLBaderNarrow-Italic,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
										font-size: 2rem;
										line-height: 2rem;
										letter-spacing: -.0625rem
								}

								.body-l {
										font-size: 1.75rem;
										line-height: 2.5rem
								}

								.body-l,.body-m {
										font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif
								}

								.body-m {
										font-size: 1.25rem;
										line-height: 1.75rem
								}

								.body-s {
										font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
										font-size: 1rem;
										line-height: 1.25rem
								}

								.action-m {
										font-family: GLBaderCompress-Black,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
										font-size: 1rem;
										line-height: 1rem
								}

								.action-m,.action-s {
										letter-spacing: .05rem
								}

								.action-s {
										font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
										font-size: .75rem;
										line-height: .75rem
								}
						}

						html {
								-webkit-text-size-adjust: 100%;
								-moz-text-size-adjust: 100%;
								-ms-text-size-adjust: 100%;
								text-size-adjust: 100%
						}

						body,html {
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								color: #000;
								height: 100%
						}

						body {
								max-width: 100%;
								overflow-x: hidden
						}

						.no-scroll {
								overflow: hidden
						}

						a {
								color: #000;
								text-decoration: none
						}

						blockquote,body,dd,dl,dt,fieldset,figure,h1,h2,h3,h4,h5,h6,hr,html,iframe,legend,li,ol,p,pre,textarea,ul {
								margin: 0;
								padding: 0
						}

						h1,h2,h3,h4,h5,h6 {
								font-size: 100%;
								font-weight: 400
						}

						ul {
								list-style: none
						}

						button,input,select,textarea {
								margin: 0
						}

						html {
								-webkit-box-sizing: border-box;
								box-sizing: border-box
						}

						*,:after,:before {
								-webkit-box-sizing: inherit;
								box-sizing: inherit
						}

						img,video {
								height: auto;
								max-width: 100%
						}

						iframe {
								border: 0
						}

						table {
								border-collapse: collapse;
								border-spacing: 0
						}

						td,th {
								padding: 0
						}

						td:not([align]),th:not([align]) {
								text-align: left
						}

						.gl-icon[data-v-64f47b68] {
								display: inline-block;
								vertical-align: baseline
						}

						.gl-button {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;
								cursor: pointer;
								font-family: GLBaderCompress-Black,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								font-size: .75rem;
								height: 2.5rem;
								max-width: -webkit-max-content;
								max-width: -moz-max-content;
								max-width: max-content;
								min-width: -webkit-fit-content;
								min-width: -moz-fit-content;
								min-width: fit-content;
								padding: .5rem 1.5rem;
								text-decoration: none;
								text-transform: uppercase
						}

						.gl-button[disabled] {
								pointer-events: none;
								color: #aaa8a3
						}

						.gl-button:focus {
								outline: none
						}

						.gl-button--full {
								max-width: 100%;
								width: 100%
						}

						.gl-button--icon-left {
								margin-right: .5rem
						}

						.gl-button--icon-right {
								margin-left: .5rem
						}

						.gl-button-primary {
								background-color: #000;
								border-color: #000;
								color: #fff
						}

						.gl-button-primary,.gl-button-primary:hover {
								border-radius: 1.5rem;
								border-style: solid;
								border-width: .0625rem
						}

						.gl-button-primary:hover {
								background-color: #6fff00;
								border-color: #6fff00;
								color: #000
						}

						.gl-button-primary[disabled] {
								border-radius: 1.5rem;
								border-style: solid;
								background-color: #f3f2f0;
								border-color: #f3f2f0;
								border-width: .0625rem
						}

						.gl-button-primary--light {
								background-color: #fff;
								border-color: #fff
						}

						.gl-button-primary--light,.gl-button-secondary {
								border-radius: 1.5rem;
								border-style: solid;
								border-width: .0625rem;
								color: #000
						}

						.gl-button-secondary {
								background-color: transparent;
								border-color: #000
						}

						.gl-button-secondary:hover {
								border-color: #6fff00
						}

						.gl-button-secondary:hover,.gl-button-secondary[disabled] {
								border-radius: 1.5rem;
								border-style: solid;
								background-color: transparent;
								border-width: .0625rem
						}

						.gl-button-secondary[disabled] {
								border-color: #e1ded9
						}

						.gl-button-secondary--light {
								border-radius: 1.5rem;
								border-style: solid;
								background-color: transparent;
								border-color: #e1ded9;
								border-width: .0625rem;
								color: #fff
						}

						.gl-button-tertiary {
								background-color: transparent;
								border: none;
								border-bottom: .125rem solid transparent;
								color: #000;
								height: -webkit-fit-content;
								height: -moz-fit-content;
								height: fit-content;
								padding: 0
						}

						.gl-button-tertiary:hover {
								border-bottom: .125rem solid #6fff00
						}

						.gl-button-tertiary--light {
								color: #fff
						}

						@media only screen and (min-width: 768px) {
								.gl-button {
										font-size:1rem
								}
						}

						.gl-checkbox[data-v-80c7eb6c] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								outline: none
						}

						.gl-checkbox--color[data-v-80c7eb6c] {
								padding-left: .25rem;
								min-height: 2rem
						}

						.gl-checkbox__input[data-v-80c7eb6c] {
								width: 1.25rem;
								height: 1.25rem;
								-ms-flex-negative: 0;
								flex-shrink: 0;
								cursor: pointer;
								position: relative;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;
								border-radius: .125rem;
								border: .0625rem solid #aaa8a3
						}

						.gl-checkbox__input[data-v-80c7eb6c]:after {
								content: "";
								display: none;
								position: absolute;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;
								top: .125rem;
								left: .375rem;
								width: .4375rem;
								height: .75rem;
								-webkit-transform: rotate(45deg);
								transform: rotate(45deg);
								border: solid #000;
								border-width: 0 .1875rem .1875rem 0
						}

						.gl-checkbox__input--checked[data-v-80c7eb6c] {
								border-color: #000
						}

						.gl-checkbox__input--checked[data-v-80c7eb6c]:after {
								display: block
						}

						.gl-checkbox__input--checked.gl-checkbox__input--light[data-v-80c7eb6c] {
								border-color: #fff;
								background: #fff
						}

						.gl-checkbox__input--size-m[data-v-80c7eb6c] {
								margin-top: .0625rem
						}

						.gl-checkbox__color[data-v-80c7eb6c] {
								cursor: pointer;
								position: relative;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;
								border-radius: 50%;
								border: .0625rem solid #4f4e4c;
								-ms-flex-negative: 0;
								flex-shrink: 0
						}

						.gl-checkbox__color[data-v-80c7eb6c]:after {
								content: "";
								position: absolute;
								left: 50%;
								top: 50%;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;
								border-radius: 50%;
								-webkit-transition: all .3s ease-out;
								transition: all .3s ease-out;
								-webkit-transform: translate3d(-50%,-50%,0) scale(1);
								transform: translate3d(-50%,-50%,0) scale(1)
						}

						.gl-checkbox__color--checked[data-v-80c7eb6c]:after {
								display: block;
								border: .125rem solid #aaa8a3
						}

						.gl-checkbox__color--checked.gl-checkbox__color--light[data-v-80c7eb6c]:after {
								border-color: #e1ded9
						}

						.gl-checkbox__color--size-s[data-v-80c7eb6c],.gl-checkbox__color--size-s[data-v-80c7eb6c]:after {
								width: 1.25rem;
								height: 1.25rem
						}

						.gl-checkbox__color--size-s.gl-checkbox__color--checked[data-v-80c7eb6c]:after {
								width: 1.75rem;
								height: 1.75rem
						}

						.gl-checkbox__color--size-m[data-v-80c7eb6c] {
								margin-top: -1px;
								width: 1.5rem;
								height: 1.5rem
						}

						.gl-checkbox__color--size-m[data-v-80c7eb6c]:after {
								width: 1.5rem;
								height: 1.5rem
						}

						.gl-checkbox__color--size-m.gl-checkbox__color--checked[data-v-80c7eb6c]:after {
								width: 2rem;
								height: 2rem
						}

						.gl-checkbox__label[data-v-80c7eb6c] {
								cursor: pointer;
								-webkit-user-select: none;
								-moz-user-select: none;
								-ms-user-select: none;
								user-select: none;
								padding-left: 1rem;
								font-size: 1rem;
								letter-spacing: 0;
								line-height: 1.25;
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								color: #4f4e4c
						}

						@media only screen and (min-width: 1025px) {
								.gl-checkbox__label[data-v-80c7eb6c] {
										font-size:1.25rem;
										letter-spacing: 0;
										line-height: 1.4
								}
						}

						.gl-checkbox__label--light[data-v-80c7eb6c] {
								color: #e1ded9
						}

						.gl-checkbox__label--checked[data-v-80c7eb6c] {
								color: #000
						}

						.gl-checkbox__label--checked.gl-checkbox__label--light[data-v-80c7eb6c] {
								color: #fff
						}

						.gl-checkbox__label--size-s[data-v-80c7eb6c] {
								font-size: .75rem;
								padding-top: .125rem
						}

						.gl-checkbox__label--size-m[data-v-80c7eb6c] {
								font-size: 1rem
						}

						.gl-checkbox--disabled[data-v-80c7eb6c] {
								opacity: .5
						}

						.gl-checkbox--disabled .gl-checkbox__color[data-v-80c7eb6c],.gl-checkbox--disabled .gl-checkbox__input[data-v-80c7eb6c],.gl-checkbox--disabled .gl-checkbox__label[data-v-80c7eb6c] {
								cursor: default
						}

						.gl-checkbox:not(.gl-checkbox--disabled):hover .gl-checkbox__input[data-v-80c7eb6c] {
								border-color: #000
						}

						.gl-checkbox:not(.gl-checkbox--disabled):hover .gl-checkbox__input--light[data-v-80c7eb6c] {
								border-color: #fff
						}

						@media only screen and (min-width: 1025px) {
								.gl-checkbox__input--size-m[data-v-80c7eb6c] {
										margin-top:.25rem;
										width: 1.5rem;
										height: 1.5rem
								}

								.gl-checkbox__input--size-m[data-v-80c7eb6c]:after {
										left: .4375rem;
										top: .0625rem;
										width: .5625rem;
										height: 1rem;
										border-width: 0 .25rem .25rem 0
								}

								.gl-checkbox__input--size-s[data-v-80c7eb6c] {
										margin-top: .125rem;
										width: 1.25rem;
										height: 1.25rem
								}

								.gl-checkbox__label--size-s[data-v-80c7eb6c] {
										font-size: 1rem;
										padding-top: 0
								}

								.gl-checkbox__label--size-m[data-v-80c7eb6c] {
										font-size: 1.25rem
								}

								.gl-checkbox .gl-checkbox__color--size-s[data-v-80c7eb6c] {
										margin-top: .125rem
								}

								.gl-checkbox .gl-checkbox__color--size-m[data-v-80c7eb6c] {
										margin-top: .25rem
								}
						}

						.gl-option[data-v-73f7526a] {
								padding: 0 .75rem;
								color: #4f4e4c
						}

						.gl-option .gl-option__label[data-v-73f7526a] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								height: 100%;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								padding: .5rem 0 .75rem;
								border-top: .0625rem solid #e1ded9
						}

						.gl-option--active[data-v-73f7526a] {
								color: #fff;
								background-color: #000
						}

						.gl-option--active .gl-option__label[data-v-73f7526a] {
								border-top: none
						}

						.gl-option:hover .gl-option__label[data-v-73f7526a] {
								color: #000;
								font-family: GLBaderNarrow-Bold,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif
						}

						.gl-option--active:hover .gl-option__label[data-v-73f7526a] {
								color: #fff
						}

						@media only screen and (min-width: 1025px) {
								.gl-option--as-filter[data-v-73f7526a] {
										padding-left:1.5rem;
										padding-right: 1.5rem
								}
						}

						.gl-radio[data-v-a8631a44] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex
						}

						.gl-radio__container[data-v-a8631a44] {
								overflow: hidden;
								position: relative;
								width: 1.25rem;
								height: 1.25rem;
								min-width: 1.25rem
						}

						.gl-radio__shape[data-v-a8631a44] {
								top: 0;
								left: 0;
								z-index: 3;
								cursor: pointer;
								border-radius: 50%;
								border: .0625rem solid #aaa8a3
						}

						.gl-radio__shape[data-v-a8631a44],.gl-radio__shape[data-v-a8631a44]:after {
								width: 100%;
								height: 100%;
								display: block;
								position: absolute;
								-webkit-box-sizing: border-box;
								box-sizing: border-box
						}

						.gl-radio__shape[data-v-a8631a44]:after {
								content: "";
								opacity: 0;
								-webkit-transform: scale(.2);
								transform: scale(.2);
								-webkit-transition: .2s ease-in-out;
								transition: .2s ease-in-out;
								border-radius: 50%;
								background: #000
						}

						.gl-radio__shape--checked[data-v-a8631a44] {
								border-color: #000
						}

						.gl-radio__shape--checked[data-v-a8631a44]:after {
								opacity: 1;
								-webkit-transform: scale(.4);
								transform: scale(.4)
						}

						.gl-radio__input[data-v-a8631a44] {
								opacity: 0;
								z-index: 0;
								-webkit-transform: scale(.5);
								transform: scale(.5)
						}

						.gl-radio__label[data-v-a8631a44] {
								cursor: pointer;
								margin-left: .75rem;
								color: #4f4e4c;
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif
						}

						.gl-radio__label--checked[data-v-a8631a44] {
								color: #000
						}

						.gl-radio__label--size-s[data-v-a8631a44] {
								font-size: .75rem;
								letter-spacing: 0;
								line-height: 1.33333;
								margin-top: 2px
						}

						@media only screen and (min-width: 1025px) {
								.gl-radio__label--size-s[data-v-a8631a44] {
										font-size:1rem;
										letter-spacing: 0;
										line-height: 1.25
								}
						}

						.gl-radio__label--size-m[data-v-a8631a44] {
								font-size: 1rem;
								letter-spacing: 0;
								line-height: 1.25;
								margin-top: 1px
						}

						@media only screen and (min-width: 1025px) {
								.gl-radio__label--size-m[data-v-a8631a44] {
										font-size:1.25rem;
										letter-spacing: 0;
										line-height: 1.4
								}
						}

						.gl-radio--disabled[data-v-a8631a44] {
								opacity: .5
						}

						.gl-radio--disabled .gl-radio__label[data-v-a8631a44],.gl-radio--disabled .gl-radio__shape[data-v-a8631a44] {
								cursor: default
						}

						.gl-radio:not(.gl-radio--disabled):hover .gl-radio__shape[data-v-a8631a44] {
								border-color: #000
						}

						@media only screen and (min-width: 1025px) {
								.gl-radio__container--size-s[data-v-a8631a44] {
										width:1.25rem;
										height: 1.25rem;
										min-width: 1.25rem
								}

								.gl-radio__container--size-m[data-v-a8631a44] {
										width: 1.5rem;
										height: 1.5rem;
										min-width: 1.5rem
								}

								.gl-radio__shape--checked[data-v-a8631a44]:after {
										-webkit-transform: scale(.5);
										transform: scale(.5)
								}

								.gl-radio__label[data-v-a8631a44] {
										margin-left: 1rem
								}

								.gl-radio__label--size-s[data-v-a8631a44] {
										margin-top: 1px;
										font-size: 1rem;
										line-height: 1.25
								}

								.gl-radio__label--size-m[data-v-a8631a44] {
										margin-top: -2px;
										font-size: 1.25rem
								}
						}

						.gl-select[data-v-33e152a6] {
								position: relative;
								text-align: left;
								width: 100%
						}

						.gl-select__title[data-v-33e152a6] {
								font-size: .75rem;
								letter-spacing: 0;
								line-height: 1.33333;
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								margin-bottom: .25rem
						}

						@media only screen and (min-width: 1025px) {
								.gl-select__title[data-v-33e152a6] {
										font-size:1rem;
										letter-spacing: 0;
										line-height: 1.25
								}
						}

						.gl-select__options[data-v-33e152a6] {
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								height: auto;
								left: 0;
								opacity: 0;
								position: absolute;
								right: 0;
								-webkit-transition: max-height .3s ease-out;
								transition: max-height .3s ease-out;
								max-height: 0;
								margin-bottom: 1rem;
								overflow: auto;
								scrollbar-width: thin;
								scrollbar-color: #4f4e4c #000
						}

						.gl-select__options[data-v-33e152a6]::-webkit-scrollbar {
								width: .25rem
						}

						.gl-select__options[data-v-33e152a6]::-webkit-scrollbar-track {
								background: #000
						}

						.gl-select__options[data-v-33e152a6]::-webkit-scrollbar-thumb {
								background-color: #4f4e4c;
								border: .0625rem solid #4f4e4c
						}

						.gl-select__options--is-open[data-v-33e152a6] {
								max-height: 11.25rem;
								-webkit-transition: max-height .3s ease-out;
								transition: max-height .3s ease-out
						}

						.gl-select__label[data-v-33e152a6] {
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								padding: .5rem .75rem .75rem
						}

						.gl-select__filter[data-v-33e152a6],.gl-select__text[data-v-33e152a6] {
								-webkit-box-flex: 1;
								-ms-flex: 1;
								flex: 1;
								overflow: hidden;
								white-space: nowrap;
								text-overflow: ellipsis
						}

						.gl-select__text[data-v-33e152a6] {
								color: #aaa8a3;
								font-family: GLBaderNarrow-Italic,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif
						}

						.gl-select__text--with-value[data-v-33e152a6] {
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								color: #000
						}

						.gl-select__filter[data-v-33e152a6] {
								color: #000
						}

						.gl-select__filter .selected-option[data-v-33e152a6] {
								font-family: GLBaderNarrow-Bold,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif
						}

						.gl-select__dropdown[data-v-33e152a6] {
								border-style: solid;
								background-color: #fff;
								border-color: #4f4e4c;
								border-width: .0625rem;
								border-radius: .25rem;
								cursor: pointer
						}

						.gl-select__dropdown--dark[data-v-33e152a6] {
								background-color: #000;
								border-color: #aaa8a3
						}

						.gl-select__dropdown--dark .gl-select__filter[data-v-33e152a6],.gl-select__dropdown--dark .gl-select__text--with-value[data-v-33e152a6] {
								color: #fff
						}

						.gl-select__dropdown--open[data-v-33e152a6] {
								border-bottom-left-radius: 0;
								border-bottom-right-radius: 0;
								border-bottom-color: transparent
						}

						.gl-select__dropdown--open .gl-select__options[data-v-33e152a6] {
								border-style: solid;
								background-color: #fff;
								border-color: #4f4e4c;
								border-width: .0625rem;
								border-radius: .25rem;
								border-top-left-radius: 0;
								border-top-right-radius: 0;
								border-top: 0;
								opacity: 1;
								z-index: 1
						}

						.gl-select__dropdown--open .gl-select__filter[data-v-33e152a6] {
								font-style: italic;
								color: #4f4e4c
						}

						.gl-select__dropdown--open .gl-select__filter--dark[data-v-33e152a6] {
								color: #e1ded9
						}

						.gl-select__dropdown--up[data-v-33e152a6] {
								border-radius: .25rem;
								border-top-left-radius: 0;
								border-top-right-radius: 0;
								border-bottom-color: inherit;
								border-top-color: transparent;
								position: relative
						}

						.gl-select__dropdown--up .gl-select__options[data-v-33e152a6] {
								border-style: solid;
								background-color: #fff;
								border-color: #4f4e4c;
								border-width: .0625rem;
								border-radius: .25rem;
								border-bottom-left-radius: 0;
								border-bottom-right-radius: 0;
								border-bottom: 0;
								position: absolute;
								bottom: 1.5rem;
								-webkit-box-sizing: content-box;
								box-sizing: content-box;
								width: 100%;
								left: -.0625rem
						}

						.gl-select--size-s .gl-select__dropdown[data-v-33e152a6],.gl-select--size-s .gl-select__filter[data-v-33e152a6],.gl-select--size-s .gl-select__text[data-v-33e152a6] {
								font-size: 1rem
						}

						.gl-select--size-m .gl-select__dropdown[data-v-33e152a6],.gl-select--size-m .gl-select__filter[data-v-33e152a6],.gl-select--size-m .gl-select__text[data-v-33e152a6] {
								font-size: 1.25rem
						}

						@media only screen and (min-width: 1025px) {
								.gl-select__dropdown--up .gl-select__options[data-v-33e152a6] {
										bottom:2rem
								}
						}

						.gl-spinner[data-v-20de2faf] {
								-webkit-animation: rotate-data-v-20de2faf 1.5s ease-in-out infinite;
								animation: rotate-data-v-20de2faf 1.5s ease-in-out infinite;
								stroke-dasharray: 225%;
								-webkit-transform-origin: center;
								transform-origin: center
						}

						@-webkit-keyframes rotate-data-v-20de2faf {
								to {
										-webkit-transform: rotate(1turn);
										transform: rotate(1turn)
								}
						}

						@keyframes rotate-data-v-20de2faf {
								to {
										-webkit-transform: rotate(1turn);
										transform: rotate(1turn)
								}
						}

						.gl-input[data-v-c9a584c0] {
								display: block;
								position: relative;
								width: 100%
						}

						.gl-input__field[data-v-c9a584c0] {
								display: block;
								width: 100%;
								color: inherit;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;
								height: 3rem;
								border-radius: .125rem;
								background-color: transparent;
								padding: 0 1rem;
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								border: .0625rem solid #4f4e4c;
								-moz-appearance: textfield
						}

						.gl-input__field[data-v-c9a584c0]::-webkit-input-placeholder {
								font-style: italic;
								color: #aaa8a3
						}

						.gl-input__field[data-v-c9a584c0]::-moz-placeholder {
								font-style: italic;
								color: #aaa8a3
						}

						.gl-input__field[data-v-c9a584c0]:-ms-input-placeholder {
								font-style: italic;
								color: #aaa8a3
						}

						.gl-input__field[data-v-c9a584c0]::-ms-input-placeholder {
								font-style: italic;
								color: #aaa8a3
						}

						.gl-input__field[data-v-c9a584c0]::placeholder {
								font-style: italic;
								color: #aaa8a3
						}

						.gl-input__field[data-v-c9a584c0]::-webkit-inner-spin-button,.gl-input__field[data-v-c9a584c0]::-webkit-outer-spin-button {
								-webkit-appearance: none;
								margin: 0
						}

						.gl-input__field--textarea[data-v-c9a584c0] {
								resize: vertical;
								min-height: 9rem;
								padding-top: .75rem
						}

						.gl-input__field--input[data-v-c9a584c0] {
								line-height: 1rem
						}

						.gl-input__field--size-s[data-v-c9a584c0] {
								font-size: 1rem
						}

						.gl-input__field--size-s[data-v-c9a584c0]::-webkit-input-placeholder {
								font-size: 1rem
						}

						.gl-input__field--size-s[data-v-c9a584c0]::-moz-placeholder {
								font-size: 1rem
						}

						.gl-input__field--size-s[data-v-c9a584c0]:-ms-input-placeholder {
								font-size: 1rem
						}

						.gl-input__field--size-s[data-v-c9a584c0]::-ms-input-placeholder {
								font-size: 1rem
						}

						.gl-input__field--size-s[data-v-c9a584c0]::placeholder {
								font-size: 1rem
						}

						.gl-input__field--size-m[data-v-c9a584c0] {
								font-size: 1.25rem
						}

						.gl-input__field--size-m[data-v-c9a584c0]::-webkit-input-placeholder {
								font-size: 1rem
						}

						.gl-input__field--size-m[data-v-c9a584c0]::-moz-placeholder {
								font-size: 1rem
						}

						.gl-input__field--size-m[data-v-c9a584c0]:-ms-input-placeholder {
								font-size: 1rem
						}

						.gl-input__field--size-m[data-v-c9a584c0]::-ms-input-placeholder {
								font-size: 1rem
						}

						.gl-input__field--size-m[data-v-c9a584c0]::placeholder {
								font-size: 1rem
						}

						.gl-input__suffix[data-v-c9a584c0] {
								position: absolute;
								top: 50%;
								left: 0;
								-webkit-transform: translateY(-50%);
								transform: translateY(-50%)
						}

						.gl-input__suffix--size-s[data-v-c9a584c0] {
								font-size: 1rem
						}

						.gl-input__suffix--size-m[data-v-c9a584c0] {
								font-size: 1.25rem
						}

						.gl-slider-thumb[data-v-57e8a2c0] {
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								background-color: #000;
								border-radius: 50%;
								cursor: pointer;
								color: #000;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								font-size: .75rem;
								height: 1rem;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center;
								position: absolute;
								top: -.375rem;
								-webkit-transform: translateX(-.5rem);
								transform: translateX(-.5rem);
								width: 1rem;
								-webkit-transition: -webkit-transform .1s ease-in-out;
								transition: -webkit-transform .1s ease-in-out;
								transition: transform .1s ease-in-out;
								transition: transform .1s ease-in-out,-webkit-transform .1s ease-in-out
						}

						.gl-slider-thumb[data-v-57e8a2c0]:focus {
								outline: .0625rem solid #000;
								-webkit-transform: scale(1.3) translateX(-.5rem);
								transform: scale(1.3) translateX(-.5rem)
						}

						.gl-slider-thumb--is-dragging[data-v-57e8a2c0],.gl-slider-thumb[data-v-57e8a2c0]:active {
								cursor: -webkit-grabbing;
								cursor: grabbing;
								-webkit-transform: scale(1.3) translateX(-.5rem);
								transform: scale(1.3) translateX(-.5rem)
						}

						.gl-slider-thumb__indicator[data-v-57e8a2c0] {
								position: absolute;
								top: -1.125rem
						}

						.gl-slider-thumb--light[data-v-57e8a2c0] {
								background-color: #fff;
								color: #fff
						}

						.gl-slider-thumb--light[data-v-57e8a2c0]:focus {
								outline: .0625rem solid #fff
						}

						.gl-slider-tick[data-v-7ce9fb9a] {
								background-color: #fff;
								border-radius: 50%;
								height: .125rem;
								position: absolute;
								top: .0625rem;
								-webkit-transform: translateX(-.0625rem);
								transform: translateX(-.0625rem);
								width: .125rem
						}

						.gl-slider-tick--light[data-v-7ce9fb9a] {
								background-color: #000
						}

						.gl-slider-tick--hidden[data-v-7ce9fb9a] {
								display: none
						}

						.gl-slider[data-v-7317afda] {
								width: 100%;
								padding: .75rem;
								padding-top: 1rem
						}

						.gl-slider__fill[data-v-7317afda] {
								background-color: #000;
								cursor: pointer;
								height: .375rem;
								position: absolute;
								top: -.0625rem
						}

						.gl-slider__track[data-v-7317afda] {
								background-color: #aaa8a3;
								cursor: pointer;
								height: .25rem;
								position: relative;
								width: 100%
						}

						.gl-slider__track--light[data-v-7317afda] {
								background-color: #aaa8a3
						}

						.gl-slider__track--light .gl-slider__fill[data-v-7317afda] {
								background-color: #fff
						}

						.gl-mini-carousel[data-v-e2a879ea] {
								width: 100%;
								height: 100%;
								position: relative
						}

						.gl-mini-carousel__action[data-v-e2a879ea] {
								position: absolute;
								top: 50%;
								z-index: 1;
								display: none;
								padding: 1rem;
								border: none;
								background-color: transparent;
								cursor: pointer;
								opacity: 1;
								outline: none;
								-webkit-appearance: none;
								-moz-appearance: none;
								appearance: none;
								-webkit-transform: translate3d(0,-50%,0);
								transform: translate3d(0,-50%,0)
						}

						.gl-mini-carousel__action--previous[data-v-e2a879ea] {
								left: 0
						}

						.gl-mini-carousel__action--next[data-v-e2a879ea] {
								right: 0
						}

						.gl-mini-carousel__pagination[data-v-e2a879ea] {
								position: absolute;
								bottom: 1rem;
								left: 50%;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-transform: translate3d(-50%,0,0);
								transform: translate3d(-50%,0,0)
						}

						.gl-mini-carousel__wrapper[data-v-e2a879ea] {
								position: absolute;
								top: 0;
								left: 0;
								right: 0;
								bottom: 0;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex
						}

						.gl-mini-carousel__wrapper--no-movable .gl-mini-carousel__action[data-v-e2a879ea],.gl-mini-carousel__wrapper--no-movable .gl-mini-carousel__pagination[data-v-e2a879ea] {
								display: none
						}

						.gl-mini-carousel__slider[data-v-e2a879ea] {
								display: block;
								overflow: hidden;
								width: 100%;
								height: inherit
						}

						.gl-mini-carousel__track[data-v-e2a879ea] {
								position: relative;
								height: 100%;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-backface-visibility: hidden;
								backface-visibility: hidden;
								-webkit-transition-property: -webkit-transform;
								transition-property: -webkit-transform;
								transition-property: transform;
								transition-property: transform,-webkit-transform;
								-webkit-transition-timing-function: ease-out;
								transition-timing-function: ease-out;
								-webkit-transition-duration: .4s;
								transition-duration: .4s
						}

						.gl-mini-carousel__slide[data-v-e2a879ea] {
								position: relative;
								min-width: 100%;
								outline: none;
								-webkit-backface-visibility: hidden;
								backface-visibility: hidden;
								-webkit-user-select: none;
								-moz-user-select: none;
								-o-user-select: none;
								-ms-user-select: none;
								user-select: none;
								-webkit-tap-highlight-color: rgba(0,0,0,0)
						}

						.gl-mini-carousel__visual[data-v-e2a879ea] {
								display: block;
								position: absolute;
								top: 0;
								left: 0;
								width: 100%;
								height: 100%;
								-o-object-fit: cover;
								object-fit: cover
						}

						.gl-mini-carousel__spinner[data-v-e2a879ea] {
								position: absolute;
								top: 0;
								right: 0;
								bottom: 0;
								left: 0;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-orient: vertical;
								-webkit-box-direction: normal;
								-ms-flex-direction: column;
								flex-direction: column;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center
						}

						.gl-mini-carousel__dot[data-v-e2a879ea] {
								cursor: pointer;
								position: relative;
								display: block;
								margin: 0 .33rem;
								padding: .25rem;
								border: .1875rem solid #fff;
								border-radius: 50%;
								-webkit-box-shadow: 0 0 0 .0625rem #e1ded9;
								box-shadow: 0 0 0 .0625rem #e1ded9;
								width: .75rem;
								height: .75rem;
								background-color: #fff;
								outline: none
						}

						.gl-mini-carousel__dot-label[data-v-e2a879ea] {
								position: absolute;
								left: -1000rem
						}

						.gl-mini-carousel__dot--active[data-v-e2a879ea] {
								background-color: #000
						}

						@media only screen and (min-width: 768px) {
								.gl-mini-carousel__dot[data-v-e2a879ea] {
										padding:.125rem;
										border: .125rem solid #fff;
										width: .5rem;
										height: .5rem
								}
						}

						@media only screen and (min-width: 1025px) {
								.fade-on-desktop[data-v-e2a879ea] {
										opacity:0;
										-webkit-transition: opacity .3s ease-in-out;
										transition: opacity .3s ease-in-out
								}

								.gl-mini-carousel__action[data-v-e2a879ea] {
										display: block
								}

								.gl-mini-carousel__wrapper--controls .fade-on-desktop[data-v-e2a879ea] {
										opacity: 1
								}

								.gl-mini-carousel__wrapper--controls .gl-mini-carousel__action--hidden[data-v-e2a879ea] {
										opacity: 0;
										pointer-events: none
								}
						}

						.gl-group-picker[data-v-72392290] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								position: relative;
								width: 100%
						}

						.gl-group-picker__slider[data-v-72392290] {
								display: block;
								overflow: hidden;
								width: 100%
						}

						.gl-group-picker__track[data-v-72392290] {
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								display: -webkit-inline-box;
								display: -ms-inline-flexbox;
								display: inline-flex;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center;
								min-height: 3rem;
								position: relative;
								-webkit-transition: all .3s ease-out;
								transition: all .3s ease-out
						}

						.gl-group-picker__track--non-scrollable[data-v-72392290] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex
						}

						.gl-group-picker__action[data-v-72392290] {
								-webkit-appearance: none;
								-moz-appearance: none;
								appearance: none;
								background-color: transparent;
								border: none;
								cursor: pointer;
								display: block;
								margin: 0 auto;
								outline: none
						}

						.gl-group-picker__action--next[data-v-72392290] {
								-webkit-box-ordinal-group: 2;
								-ms-flex-order: 1;
								order: 1
						}

						.gl-group-picker__action--disabled[data-v-72392290] {
								visibility: hidden;
								pointer-events: none
						}

						.gl-group-picker__item[data-v-72392290] {
								border-style: solid;
								border-color: #e1ded9;
								border-width: .0625rem;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								-webkit-appearance: none;
								-moz-appearance: none;
								appearance: none;
								background-color: transparent;
								border-color: transparent;
								cursor: pointer;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center;
								margin: 0 auto;
								outline: none;
								padding: 0;
								position: relative
						}

						.gl-group-picker__item--bordered[data-v-72392290] {
								border: .0625rem solid #e1ded9
						}

						.gl-group-picker__item--active[data-v-72392290] {
								border-style: solid;
								background-color: transparent;
								border-color: #e1ded9;
								border-width: .0625rem
						}

						.gl-group-picker__item--disabled[data-v-72392290] {
								border: none;
								pointer-events: none;
								cursor: auto
						}

						.gl-group-picker__item--disabled[data-v-72392290] :hover {
								cursor: auto
						}

						.gl-group-picker--image .gl-group-picker__item[data-v-72392290] {
								width: 3rem;
								height: 3rem;
								border-radius: 50%;
								margin: 0 .75rem;
								padding: .25rem
						}

						.gl-group-picker--image .gl-group-picker__item--image[data-v-72392290] {
								width: 100%;
								height: 100%;
								border-radius: 50%
						}

						.gl-group-picker--text .gl-group-picker__item[data-v-72392290] {
								border-radius: 1rem;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center;
								min-height: 2rem;
								min-width: 2rem;
								padding: 0 .75rem;
								text-align: center;
								white-space: nowrap
						}

						.gl-group-picker--color .gl-group-picker__item[data-v-72392290] {
								width: 2rem;
								height: 2rem;
								border-radius: 50%;
								margin: 0 .75rem;
								padding: .25rem
						}

						.gl-group-picker--color .gl-group-picker__item--color[data-v-72392290] {
								width: 100%;
								height: 100%;
								border-radius: 50%
						}

						.gl-group-picker--hidden[data-v-72392290] {
								display: none
						}

						.gl-group-picker--disabled[data-v-72392290] {
								cursor: default
						}

						@media only screen and (min-width: 1025px) {
								.gl-group-picker__item[data-v-72392290]:hover {
										border-style:solid;
										background-color: transparent;
										border-color: #e1ded9;
										border-width: .0625rem
								}
						}

						.dynamic-components[data-v-995baa2e]>:last-child {
								border-bottom: none
						}

						.page-error__title[data-v-622d1992] {
								font-size: 1.5rem;
								margin-bottom: 1rem
						}

						.page-error__button[data-v-622d1992] {
								margin-top: 1rem;
								margin-right: 1rem
						}

						.page-error__business-error[data-v-622d1992] {
								padding: .75rem
						}

						.page-error__component-name[data-v-622d1992] {
								font-weight: 700;
								text-decoration: underline
						}

						.page-error__field-name[data-v-622d1992] {
								font-weight: 700
						}

						.page-error__cause-item[data-v-622d1992]:before {
								content: "-"
						}

						.header[data-v-d29e4508] {
								position: relative;
								z-index: 6000
						}

						.header__content[data-v-d29e4508] {
								width: 100%;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;
								z-index: 6000
						}

						.header__content--fixed[data-v-d29e4508] {
								width: 100%;
								position: fixed
						}

						.header__borders[data-v-d29e4508] {
								display: none;
								left: 0;
								bottom: 0;
								width: 100%;
								height: 100%;
								position: absolute
						}

						.header__borders-content[data-v-d29e4508] {
								height: 100%;
								margin: 0 auto;
								max-width: 1440px;
								border-left: .0625rem solid #000;
								border-right: .0625rem solid #000
						}

						.header[data-v-d29e4508]:not(.header--prehome) {
								height: 6.0625rem
						}

						.header:not(.header--prehome) .header__wrapper[data-v-d29e4508] {
								height: 100%;
								position: relative;
								background-color: #fff;
								padding-bottom: 3rem
						}

						.header:not(.header--prehome) .header__primary[data-v-d29e4508] {
								height: 100%;
								z-index: 100;
								max-height: 7rem;
								min-height: 3.0625rem
						}

						.header:not(.header--prehome) .header__secondary[data-v-d29e4508] {
								left: 0;
								bottom: 0;
								width: 100%;
								position: absolute;
								z-index: 1
						}

						.header:not(.header--prehome) .header__content[data-v-d29e4508] {
								top: 0;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;
								height: 6.0625rem
						}

						.header:not(.header--prehome) .header__content--css-transition[data-v-d29e4508] {
								-webkit-transition-property: height,top;
								transition-property: height,top;
								-webkit-transition-duration: .3s;
								transition-duration: .3s
						}

						.header:not(.header--prehome) .header__content--initial[data-v-d29e4508],.header:not(.header--prehome) .header__content--middle[data-v-d29e4508] {
								height: 6.0625rem
						}

						.header:not(.header--prehome) .header__content--hidden[data-v-d29e4508] {
								overflow: hidden;
								top: -6.0625rem;
								height: 6.0625rem
						}

						@media only screen and (min-width: 768px) {
								.header[data-v-d29e4508]:not(.header--prehome) {
										height:7.125rem
								}

								.header:not(.header--prehome) .header__primary[data-v-d29e4508] {
										min-height: 4.125rem
								}

								.header:not(.header--prehome) .header__content[data-v-d29e4508],.header:not(.header--prehome) .header__content--initial[data-v-d29e4508],.header:not(.header--prehome) .header__content--middle[data-v-d29e4508] {
										height: 7.125rem
								}

								.header:not(.header--prehome) .header__content--hidden[data-v-d29e4508] {
										top: -7.125rem;
										height: 7.125rem
								}
						}

						@media only screen and (min-width: 1025px) {
								.header[data-v-d29e4508]:not(.header--prehome),.header:not(.header--prehome) .header__content[data-v-d29e4508],.header:not(.header--prehome) .header__content--initial[data-v-d29e4508] {
										height:10.0625rem
								}

								.header:not(.header--prehome) .header__content--middle[data-v-d29e4508] {
										height: 6.0625rem
								}

								.header:not(.header--prehome) .header__content--hidden[data-v-d29e4508] {
										top: -6.0625rem;
										height: 6.0625rem
								}

								.header:not(.header--prehome) .header__primary[data-v-d29e4508] {
										min-height: 3rem
								}
						}

						@media only screen and (min-width: 1441px) {
								.header__borders[data-v-d29e4508] {
										display:block
								}
						}

						.primary-header[data-v-02b99818] {
								width: 100%;
								height: 100%;
								position: relative;
								z-index: 6000;
								background-color: #fff;
								border-bottom: .0625rem solid #4f4e4c
						}

						.primary-header__content[data-v-02b99818] {
								height: 100%;
								margin: 0 auto;
								position: relative;
								text-align: center;
								max-width: 1440px
						}

						.primary-header--prehome[data-v-02b99818] {
								height: 3rem
						}

						@media only screen and (min-width: 768px) {
								.primary-header--prehome[data-v-02b99818] {
										height:4.0625rem
								}
						}

						@media only screen and (min-width: 1025px) {
								.primary-header[data-v-02b99818] {
										z-index:6000
								}

								.primary-header--prehome[data-v-02b99818] {
										border-bottom: none;
										height: 7rem
								}
						}

						@media only screen and (min-width: 1441px) {
								.primary-header__content--prehome[data-v-02b99818] {
										height:7rem;
										border-right: .0625rem solid #4f4e4c;
										border-left: .0625rem solid #4f4e4c
								}
						}

						.primary-header-left[data-v-37a99f49] {
								position: absolute;
								left: 0;
								top: 50%;
								-webkit-transform: translateY(-50%);
								transform: translateY(-50%);
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center
						}

						.primary-header-left__find-store[data-v-37a99f49] {
								margin-left: 20px
						}

						.icon-link[data-v-0691da03] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								cursor: pointer
						}

						.icon-link__label[data-v-0691da03] {
								line-height: 1.25rem;
								font-size: 1rem;
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								color: var(--label-color)
						}

						.icon-link__label.left[data-v-0691da03] {
								margin-left: 8px
						}

						.icon-link__label.right[data-v-0691da03] {
								margin-right: 8px
						}

						.icon-link__icon[data-v-0691da03] {
								width: 24px;
								height: 24px
						}

						.icon-link:hover .icon-link__label[data-v-0691da03] {
								color: #000
						}

						@media only screen and (min-width: 768px) {
								.icon-link__icon[data-v-0691da03] {
										width:32px;
										height: 32px
								}
						}

						.icon[data-v-53fe7467] {
								display: inline-block;
								height: var(--size);
								width: var(--size);
								pointer-events: none
						}

						.icon__wrapper--clickable[data-v-53fe7467] {
								-webkit-appearance: none;
								-moz-appearance: none;
								appearance: none;
								background-color: transparent;
								border: none;
								cursor: pointer;
								display: block;
								padding: 0
						}

						.primary-header-logo[data-v-5ff8d89c] {
								height: 100%;
								overflow: hidden;
								margin-left: 48px;
								margin-right: 96px
						}

						.primary-header-logo__img[data-v-5ff8d89c] {
								height: 145%;
								width: auto;
								-webkit-transform: translateY(-15%);
								transform: translateY(-15%)
						}

						@media only screen and (min-width: 768px) {
								.primary-header-logo[data-v-5ff8d89c] {
										margin-left:0;
										margin-right: 0
								}
						}

						@media only screen and (min-width: 1025px) {
								.primary-header-logo--prehome[data-v-5ff8d89c] {
										overflow:visible
								}

								.primary-header-logo--prehome .primary-header-logo__img[data-v-5ff8d89c] {
										height: auto;
										width: 40%
								}
						}

						.primary-header-right[data-v-7445d06a] {
								position: absolute;
								right: 0;
								top: 0
						}

						.primary-header-right[data-v-7445d06a],.primary-header-right .header__user[data-v-7445d06a] {
								height: 100%;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center
						}

						.primary-header-right .header__user[data-v-7445d06a] {
								position: relative;
								background-color: transparent
						}

						@media (min-width: 1280px) {
								.primary-header-right .header__user.active:hover.header__user--scrolled[data-v-7445d06a] {
										height:80px
								}
						}

						@media only screen and (min-width: 768px) {
								.primary-header-right .minicart[data-v-7445d06a] {
										margin-right:20px
								}
						}

						.primary-header-right .minicart-wrapper[data-v-7445d06a] {
								width: 3rem;
								position: relative;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center;
								height: 100%;
								background-color: transparent;
								border-left: 1px solid #4f4e4c
						}

						@media only screen and (min-width: 768px) {
								.primary-header-right .minicart-wrapper[data-v-7445d06a] {
										width:auto;
										border-left: none;
										margin-left: 35px
								}
						}

						@media only screen and (min-width: 1281px) {
								.primary-header-right .minicart-wrapper.active:hover .minicart-overlay[data-v-7445d06a] {
										display:block
								}

								.primary-header-right .minicart-wrapper.active:hover.scrolled[data-v-7445d06a] {
										height: 80px
								}
						}

						.primary-header-right .minicart-overlay[data-v-7445d06a] {
								position: absolute;
								top: calc(50% + 36px);
								right: 0;
								display: none
						}

						.user-info[data-v-34d63d70] {
								border-left: 1px solid #4f4e4c;
								height: 3rem;
								width: 3rem;
								position: relative
						}

						.user-info-overlay[data-v-34d63d70] {
								display: none;
								height: auto;
								left: auto;
								line-height: normal;
								position: absolute;
								right: -48px;
								top: 48px;
								width: 100vw
						}

						.user-info-mobile-selected[data-v-34d63d70] {
								background: #f3f2f0
						}

						.user-info-overlay-mobile-open[data-v-34d63d70] {
								display: block
						}

						.user-info-scrolled .user-info-overlay[data-v-34d63d70] {
								top: calc(50% + 30px)
						}

						@media only screen and (min-width: 768px) {
								.user-info[data-v-34d63d70] {
										border:0;
										height: 80px;
										width: auto
								}

								.user-info-overlay[data-v-34d63d70] {
										left: auto;
										margin: 0;
										position: absolute;
										top: calc(50% + 36px);
										right: -15px;
										width: auto
								}

								.user-info-active:hover .user-info-overlay[data-v-34d63d70] {
										display: block
								}
						}

						.m-user-picto[data-v-2f51e7bd] {
								font-size: 1rem;
								display: -webkit-inline-box;
								display: -ms-inline-flexbox;
								display: inline-flex;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								height: 100%;
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif
						}

						.m-user-picto .picto-user-active[data-v-2f51e7bd] {
								position: relative
						}

						.m-user-picto .picto-user-active[data-v-2f51e7bd]:after {
								position: absolute;
								top: 0;
								right: 0;
								width: 12px;
								height: 12px;
								content: "";
								background-color: #6fff00;
								border-radius: 50%
						}

						@media only screen and (min-width: 768px) {
								.m-user-picto .picto-user-active[data-v-2f51e7bd]:after {
										width:16px;
										height: 16px
								}
						}

						.icon-link[data-v-c073c288] {
								position: relative
						}

						.active[data-v-c073c288]:after {
								position: absolute;
								content: "";
								top: 0;
								right: 0;
								width: 12px;
								height: 12px;
								background-color: #6fff00;
								border-radius: 50%
						}

						@media only screen and (min-width: 768px) {
								.active[data-v-c073c288]:after {
										width:16px;
										height: 16px
								}
						}

						.see-all-block[data-v-420b2b6e] {
								text-align: right;
								margin-top: 13px
						}

						.see-all-block .link[data-v-420b2b6e] {
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								font-size: 1rem;
								line-height: 1.25rem;
								font-weight: 700;
								color: #000
						}

						.see-all-block .link[data-v-420b2b6e]:hover {
								text-decoration: underline
						}

						.minicart-overlay[data-v-420b2b6e] {
								padding: 24px 0;
								width: 480px;
								border: 1px solid #4f4e4c;
								background-color: #fff;
								position: relative;
								text-align: left;
								-webkit-box-sizing: border-box;
								box-sizing: border-box
						}

						.minicart-overlay__block[data-v-420b2b6e] {
								padding: 0 24px
						}

						.minicart-overlay[data-v-420b2b6e]:before {
								border-color: transparent transparent #000;
								top: -12px
						}

						.minicart-overlay[data-v-420b2b6e]:after,.minicart-overlay[data-v-420b2b6e]:before {
								position: absolute;
								content: "";
								border-style: solid;
								border-width: 0 10px 12px;
								right: 20px
						}

						.minicart-overlay[data-v-420b2b6e]:after {
								border-color: transparent transparent #fff;
								top: -11px
						}

						.title-block[data-v-420b2b6e] {
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								font-size: 1.25rem;
								line-height: 1.75rem;
								color: #4f4e4c;
								font-weight: 700;
								margin-bottom: 20px
						}

						.product-list-block .minicart-entry[data-v-420b2b6e]:not(:last-child) {
								margin-bottom: 16px
						}

						.separator[data-v-420b2b6e] {
								height: 1px;
								background-color: #e1ded9;
								margin: 15px 24px;
								max-width: 100%
						}

						.total-block .total-row[data-v-420b2b6e] {
								position: relative;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-ms-flex-wrap: wrap;
								flex-wrap: wrap;
								width: 100%;
								max-width: 1440px;
								margin: 0 auto;
								padding: 0;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;
								margin-bottom: 0;
								margin-top: 0
						}

						.total-block .total-row .total-label[data-v-420b2b6e] {
								font-size: 1.25rem;
								line-height: 1.75rem;
								font-weight: 700
						}

						.total-block .total-row .total-entries-label[data-v-420b2b6e],.total-block .total-row .total-label[data-v-420b2b6e] {
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								color: #4f4e4c
						}

						.total-block .total-row .total-entries-label[data-v-420b2b6e] {
								font-size: .875rem;
								line-height: 1.25rem;
								padding-left: 5px
						}

						.total-block .total-row .total-value-column[data-v-420b2b6e] {
								text-align: right;
								font-size: 1.25rem;
								line-height: 1.75rem;
								color: #000;
								font-weight: 700;
								margin-left: .5rem
						}

						.total-block .legal-notice-row[data-v-420b2b6e],.total-block .total-row .total-value-column[data-v-420b2b6e] {
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif
						}

						.total-block .legal-notice-row[data-v-420b2b6e] {
								font-style: italic;
								font-size: .875rem;
								color: #aaa8a3;
								line-height: 1.25rem
						}

						.total-block .validate-button-row[data-v-420b2b6e] {
								margin-top: 20px
						}

						.total-block .validate-button-row .validate-button[data-v-420b2b6e] {
								width: 100%;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;
								display: inline-block
						}

						.secondary-header[data-v-b266d208] {
								position: relative;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center;
								background-color: #fff;
								border-bottom: .0625rem solid #000
						}

						.secondary-header__content[data-v-b266d208] {
								max-width: 1440px;
								width: 100%;
								border-left: 1px solid #4f4e4c;
								height: 3rem
						}

						.secondary-header__content[data-v-b266d208],.secondary-header__content .navigation-list[data-v-b266d208] {
								border-right: 1px solid #4f4e4c;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex
						}

						.secondary-header__content .navigation-list[data-v-b266d208] {
								width: 75%;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								-ms-flex-pack: distribute;
								justify-content: space-around
						}

						.secondary-header__content .navigation-list__link[data-v-b266d208] {
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								color: #000;
								cursor: pointer;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								font-size: 1rem;
								font-weight: 700;
								height: 100%;
								position: relative
						}

						.secondary-header__content .navigation-list__link[data-v-b266d208]:before {
								content: "";
								position: absolute;
								width: 100%;
								height: 5px;
								bottom: 0;
								left: 0;
								background-color: #4f4e4c;
								visibility: hidden;
								-webkit-transform: scaleX(0);
								transform: scaleX(0);
								-webkit-transition: all .3s ease-out;
								transition: all .3s ease-out
						}

						.secondary-header__content .navigation-list__link--active[data-v-b266d208]:before,.secondary-header__content .navigation-list__link[data-v-b266d208]:hover:before {
								visibility: visible;
								-webkit-transform: scaleX(1);
								transform: scaleX(1)
						}

						.secondary-header__content .search-bar[data-v-b266d208] {
								-webkit-box-flex: 1;
								-ms-flex: 1;
								flex: 1
						}

						.secondary-header__mega-menu-container[data-v-b266d208] {
								border-top: 1px solid #4f4e4c;
								top: 3rem;
								left: 0;
								right: 0;
								position: absolute
						}

						.mega-menu-animation-enter-active[data-v-b266d208],.mega-menu-animation-leave-active[data-v-b266d208] {
								-webkit-transition: -webkit-transform .3s ease-out;
								transition: -webkit-transform .3s ease-out;
								transition: transform .3s ease-out;
								transition: transform .3s ease-out,-webkit-transform .3s ease-out;
								height: auto;
								-webkit-transform: translateY(0);
								transform: translateY(0);
								-webkit-transform-origin: top;
								transform-origin: top;
								z-index: -1
						}

						.mega-menu-animation-enter[data-v-b266d208],.mega-menu-animation-leave-to[data-v-b266d208] {
								-webkit-transform: translateY(-100%);
								transform: translateY(-100%);
								z-index: -1
						}

						@media (max-width: 1439px) {
								.secondary-header__content[data-v-b266d208] {
										border-left:0;
										border-right: 0
								}
						}

						.search-bar[data-v-070b8510] {
								position: relative;
								width: 100%;
								z-index: 100
						}

						.search-bar__form[data-v-070b8510] {
								position: relative;
								z-index: 1;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								height: 100%
						}

						.search-bar__form form[data-v-070b8510] {
								height: 100%
						}

						.search-bar__field[data-v-070b8510] {
								width: 100%;
								height: 100%;
								border: none;
								display: block;
								border-radius: 0;
								-webkit-appearance: none;
								-moz-appearance: none;
								appearance: none;
								font-size: .95rem;
								padding: 0 1rem
						}

						.search-bar__field-box[data-v-070b8510] {
								position: relative;
								-webkit-box-flex: 1;
								-ms-flex: auto;
								flex: auto
						}

						.search-bar__field--default[data-v-070b8510] {
								color: #aaa8a3
						}

						.search-bar__field[placeholder][data-v-070b8510] {
								overflow: hidden;
								text-overflow: ellipsis;
								white-space: nowrap
						}

						.search-bar__icon[data-v-070b8510] {
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								height: 100%;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center;
								padding-right: 12px
						}

						.search-bar__icon--submit[data-v-070b8510] {
								top: 0;
								right: 0;
								position: absolute
						}

						.search-bar__icon--close[data-v-070b8510] {
								border-left: .0625rem solid #4f4e4c;
								padding: 0;
								position: relative;
								width: 3rem;
								z-index: 1
						}

						.search-bar__expand[data-v-070b8510] {
								background-color: #fff;
								border-bottom: .0625rem solid #4f4e4c;
								margin-top: 1px;
								overflow: auto;
								position: absolute;
								right: 0;
								top: 100%;
								width: 100vw;
								z-index: 1
						}

						.search-bar__no-results-head[data-v-070b8510] {
								border-bottom: 1px solid #4f4e4c;
								color: #000;
								padding: 1rem 20%;
								text-align: center
						}

						@media only screen and (min-width: 768px) {
								.search-bar__field[data-v-070b8510] {
										padding:0 64px 0 1rem
								}

								.search-bar__icon[data-v-070b8510] {
										padding-right: 1rem
								}

								.search-bar__icon--close[data-v-070b8510] {
										display: none
								}

								.search-bar__no-results-head[data-v-070b8510] {
										padding: 2rem 1rem
								}
						}

						@media only screen and (min-width: 1025px) {
								.search-bar__expand[data-v-070b8510] {
										border-left:.0625rem solid #4f4e4c;
										border-right: .0625rem solid #4f4e4c;
										right: -1px;
										width: 61.25rem
								}
						}

						.mega-menu .sub-universe__title-row[data-v-0745f5da] {
								margin: 0 8px;
								padding: 0 16px;
								height: 48px;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;
								border-bottom: 1px solid #e1ded9;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								font-weight: 700
						}

						.mega-menu .sub-universe__title-row .title-row-label[data-v-0745f5da] {
								color: #000;
								-webkit-box-flex: 1;
								-ms-flex: 1;
								flex: 1;
								pointer-events: none
						}

						.mega-menu .sub-universe__category[data-v-0745f5da] {
								margin: 0 8px;
								padding: 0 32px;
								height: 48px;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center
						}

						.mega-menu .sub-universe__category .category-label[data-v-0745f5da] {
								color: #fff;
								pointer-events: none
						}

						.mega-menu .title-row-icon[data-v-0745f5da],.mega-menu .title-row-icon[data-v-0745f5da] .icon {
								width: 24px;
								height: 24px
						}

						.mega-menu .sub-universe--active[data-v-0745f5da] {
								background-color: #000;
								padding-bottom: 8px
						}

						.mega-menu .sub-universe--active .sub-universe__title-row[data-v-0745f5da] {
								border: 0
						}

						.mega-menu .sub-universe--active .sub-universe__title-row .title-row-label[data-v-0745f5da] {
								color: #fff;
								pointer-events: none
						}

						.mega-menu .sub-universe--active .title-row-icon[data-v-0745f5da] .icon {
								fill: #fff
						}

						@media only screen and (min-width: 768px) {
								.mega-menu[data-v-0745f5da] {
										background-color:#fff;
										display: -webkit-box;
										display: -ms-flexbox;
										display: flex;
										font-size: medium;
										height: 100%;
										-webkit-box-pack: center;
										-ms-flex-pack: center;
										justify-content: center;
										line-height: normal
								}

								.mega-menu__title[data-v-0745f5da] {
										color: #000;
										text-transform: uppercase;
										font-weight: 700
								}

								.mega-menu__title[data-v-0745f5da]:hover {
										text-decoration: underline
								}

								.mega-menu__content[data-v-0745f5da] {
										display: -webkit-box;
										display: -ms-flexbox;
										display: flex;
										width: 100%;
										max-width: 1440px;
										-webkit-box-sizing: border-box;
										box-sizing: border-box;
										-webkit-box-orient: vertical;
										-webkit-box-direction: normal;
										-ms-flex-direction: column;
										flex-direction: column;
										padding: 24px 24px 24px 48px
								}

								.mega-menu__content .sub-universes[data-v-0745f5da] {
										padding: 0;
										width: 100%;
										-webkit-column-count: 2;
										-moz-column-count: 2;
										column-count: 2
								}

								.mega-menu__content .sub-universes .sub-universe[data-v-0745f5da] {
										-webkit-column-break-inside: avoid;
										-moz-column-break-inside: avoid;
										break-inside: avoid-column;
										padding-top: 14px
								}

								.mega-menu__content .sub-universes .sub-universe__title-row[data-v-0745f5da] {
										border: 0;
										padding: 0;
										margin: 0;
										height: auto
								}

								.mega-menu__content .sub-universes .sub-universe__category[data-v-0745f5da] {
										padding: 0;
										margin: 0;
										height: auto
								}

								.mega-menu__content .sub-universes .sub-universe__category .category-label[data-v-0745f5da] {
										color: #4f4e4c
								}

								.mega-menu__content .sub-universes .sub-universe__category[data-v-0745f5da],.mega-menu__content .sub-universes .sub-universe__title-row[data-v-0745f5da] {
										cursor: pointer
								}

								.mega-menu__content .sub-universes .sub-universe__category[data-v-0745f5da]:hover,.mega-menu__content .sub-universes .sub-universe__title-row[data-v-0745f5da]:hover {
										text-decoration: underline
								}
						}

						@media only screen and (min-width: 1025px) {
								.mega-menu[data-v-0745f5da] {
										border-bottom:1px solid #4f4e4c
								}

								.mega-menu__content[data-v-0745f5da] {
										padding: 0;
										-webkit-box-orient: horizontal;
										-webkit-box-direction: normal;
										-ms-flex-direction: row;
										flex-direction: row
								}

								.mega-menu__content .sub-universes[data-v-0745f5da] {
										padding: 0 24px 24px;
										width: 70%;
										-webkit-column-count: 3;
										-moz-column-count: 3;
										column-count: 3
								}

								.mega-menu__content .sidebar[data-v-0745f5da] {
										padding: 12px 24px 24px;
										border-left: 1px solid #4f4e4c;
										display: -webkit-box;
										display: -ms-flexbox;
										display: flex;
										-webkit-box-orient: vertical;
										-webkit-box-direction: normal;
										-ms-flex-direction: column;
										flex-direction: column
								}

								.mega-menu__content .sidebar__title[data-v-0745f5da] {
										color: #000;
										font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
										font-weight: 700
								}

								.mega-menu__content .sidebar__title[data-v-0745f5da]:hover {
										text-decoration: underline
								}

								.mega-menu__content .sidebar__link[data-v-0745f5da] {
										color: #4f4e4c
								}

								.mega-menu__content .sidebar__link .link-image[data-v-0745f5da] {
										margin-top: 32px
								}

								.mega-menu__content .sidebar__link[data-v-0745f5da]:hover {
										text-decoration: underline
								}
						}

						.product-list[data-v-b46d8838] {
								margin: 0 auto;
								max-width: 1440px
						}

						.product-list[data-v-b46d8838],.product-list__main[data-v-b46d8838] {
								position: relative
						}

						.product-list__heading[data-v-b46d8838] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								-webkit-box-orient: vertical;
								-webkit-box-direction: normal;
								-ms-flex-direction: column;
								flex-direction: column;
								position: relative;
								-webkit-transform: translateZ(0);
								transform: translateZ(0);
								background-color: #fff;
								z-index: 4001;
								padding-bottom: .75rem;
								padding-top: .75rem
						}

						.product-list__breadcrumb[data-v-b46d8838] {
								white-space: nowrap;
								margin-bottom: .5rem
						}

						.product-list__title[data-v-b46d8838] {
								opacity: 1;
								-webkit-transition: opacity .25s ease-in-out;
								transition: opacity .25s ease-in-out;
								font-family: GLBaderCompress-Black,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								line-height: 1;
								font-size: 1.75rem;
								letter-spacing: -.03125rem
						}

						@media only screen and (min-width: 768px) {
								.product-list__title[data-v-b46d8838] {
										font-size:2.5rem;
										letter-spacing: -.0625rem
								}
						}

						@media only screen and (min-width: 1025px) {
								.product-list__title[data-v-b46d8838] {
										font-size:3.5rem;
										letter-spacing: -.125rem
								}
						}

						.product-list__title--hidden[data-v-b46d8838] {
								opacity: 0
						}

						.product-list__title-link[data-v-b46d8838]:hover {
								text-decoration: underline
						}

						.product-list__header[data-v-b46d8838] {
								top: 0;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								position: -webkit-sticky;
								position: sticky;
								-webkit-box-orient: vertical;
								-webkit-box-direction: normal;
								-ms-flex-direction: column;
								flex-direction: column;
								-webkit-transform: translateZ(0);
								transform: translateZ(0);
								background-color: #fff;
								z-index: 4000;
								border-bottom: .0625rem solid #4f4e4c;
								-webkit-transition: top .3s;
								transition: top .3s
						}

						.product-list__header--initial[data-v-b46d8838],.product-list__header--middle[data-v-b46d8838] {
								top: 6.0625rem
						}

						.product-list__header--hidden[data-v-b46d8838] {
								top: 0
						}

						.product-list__no-product[data-v-b46d8838] {
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								border-bottom: .0625rem solid #4f4e4c;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center;
								text-align: center;
								padding-left: 1.5rem;
								padding-right: 1.5rem;
								padding-top: 2.5rem;
								padding-bottom: 7.5rem;
								font-size: 1.25rem;
								letter-spacing: 0;
								line-height: 1.4
						}

						@media only screen and (min-width: 1025px) {
								.product-list__no-product[data-v-b46d8838] {
										font-size:1.75rem;
										letter-spacing: 0;
										line-height: 1.42857
								}
						}

						.product-list__actions[data-v-b46d8838] {
								position: relative;
								z-index: 100;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								height: 72px;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								border: none
						}

						.product-list__filters-switcher[data-v-b46d8838] {
								-webkit-box-flex: 1;
								-ms-flex: auto;
								flex: auto;
								padding: 0 8px
						}

						.product-list__filters-switcher-button[data-v-b46d8838] {
								display: -webkit-inline-box;
								display: -ms-inline-flexbox;
								display: inline-flex;
								min-height: 40px;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								outline: none
						}

						.product-list__filters-switcher-button.primary[data-v-b46d8838] {
								padding: 6px 24px;
								font-size: 1rem
						}

						.product-list__grid-switcher[data-v-b46d8838] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex
						}

						.product-list__grid-switcher-button[data-v-b46d8838] {
								margin-left: 16px;
								cursor: pointer;
								outline: none
						}

						.product-list__grid-switcher-button--active[data-v-b46d8838] {
								cursor: default
						}

						.product-list__sort-switcher[data-v-b46d8838] {
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-flex: 1;
								-ms-flex: auto;
								flex: auto;
								height: inherit;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center;
								max-width: 17.5rem;
								position: relative;
								width: -webkit-min-content;
								width: -moz-min-content;
								width: min-content
						}

						.product-list__sort-switcher[data-v-b46d8838] .gl-select__dropdown {
								border-radius: 0
						}

						.product-list__sort-switcher[data-v-b46d8838] .gl-select__dropdown:not(.gl-select__dropdown--open) {
								border: none
						}

						.product-list__sort-switcher[data-v-b46d8838] .gl-select__dropdown .gl-select__label--with-filter {
								margin-top: .75rem;
								margin-bottom: 1rem
						}

						.product-list__footer[data-v-b46d8838] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								-ms-flex-line-pack: center;
								align-content: center;
								-webkit-box-orient: vertical;
								-webkit-box-direction: normal;
								-ms-flex-direction: column;
								flex-direction: column;
								padding: 1.5rem;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center;
								border-top: .0625rem solid #4f4e4c;
								border-bottom: .0625rem solid #4f4e4c;
								border-top: none
						}

						.product-list__pagination-text[data-v-b46d8838] {
								font-size: .625rem;
								letter-spacing: .01562rem;
								line-height: 1.2;
								text-align: center;
								margin: 1rem;
								color: #aaa8a3;
								max-width: 65rem
						}

						@media only screen and (min-width: 1025px) {
								.product-list__pagination-text[data-v-b46d8838] {
										font-size:.75rem;
										letter-spacing: 0;
										line-height: 1.33333
								}
						}

						.product-list__pagination-text[data-v-b46d8838] a {
								color: inherit
						}

						@media only screen and (min-width: 768px) {
								.product-list__title[data-v-b46d8838] {
										font-size:2.5rem
								}

								.product-list__header--initial[data-v-b46d8838],.product-list__header--middle[data-v-b46d8838] {
										top: 7.125rem
								}

								.product-list__header--hidden[data-v-b46d8838] {
										top: 0
								}

								.product-list__filters-switcher[data-v-b46d8838] {
										padding: 0 16px
								}

								.product-list__no-product[data-v-b46d8838] {
										padding-top: 6rem;
										padding-bottom: 18rem
								}

								.product-list__sort-switcher[data-v-b46d8838] {
										max-width: 280px;
										padding-left: 1.5rem;
										width: 100%
								}
						}

						@media only screen and (min-width: 1025px) {
								.product-list__heading[data-v-b46d8838] {
										-webkit-box-orient:horizontal;
										-webkit-box-direction: normal;
										-ms-flex-direction: row;
										flex-direction: row;
										position: relative;
										padding-bottom: .75rem;
										padding-top: 1rem
								}

								.product-list__header--middle[data-v-b46d8838] {
										top: 6.0625rem
								}

								.product-list__header--hidden[data-v-b46d8838],.product-list__header--initial[data-v-b46d8838] {
										top: 0
								}

								.product-list__breadcrumb[data-v-b46d8838] {
										position: absolute;
										left: 1.25rem;
										top: 1rem
								}

								.product-list__title[data-v-b46d8838] {
										margin-top: 1rem;
										font-size: 3.5rem
								}

								.product-list__sticky-title[data-v-b46d8838] {
										-webkit-transition: all .25s ease-in-out;
										transition: all .25s ease-in-out;
										-webkit-transform: translateY(-50px);
										transform: translateY(-50px);
										opacity: 0;
										width: 100%;
										text-align: center;
										position: absolute;
										padding-bottom: .5rem;
										font-family: GLBaderCompress-Black,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
										line-height: 1;
										font-size: 1.25rem;
										letter-spacing: -.0125rem
								}
						}

						@media only screen and (min-width: 1025px) and (min-width:768px) {
								.product-list__sticky-title[data-v-b46d8838] {
										font-size:1.5rem;
										letter-spacing: -.025rem
								}
						}

						@media only screen and (min-width: 1025px) and (min-width:1025px) {
								.product-list__sticky-title[data-v-b46d8838] {
										font-size:1.75rem;
										letter-spacing: -.03125rem
								}
						}

						@media only screen and (min-width: 1025px) {
								.product-list__sticky-title--show[data-v-b46d8838] {
										-webkit-transform:translateY(0);
										transform: translateY(0);
										opacity: 1;
										z-index: -1
								}

								.product-list__notes[data-v-b46d8838] {
										font-size: 1rem;
										position: absolute;
										right: 1rem;
										bottom: .75rem
								}

								.product-list__footer[data-v-b46d8838] {
										padding: 2.5rem
								}

								.product-list__pagination-text[data-v-b46d8838] {
										text-align: left
								}

								.product-list__sort-switcher[data-v-b46d8838] .gl-select__dropdown:not(.gl-select__dropdown--open) {
										border-left: .0625rem solid #e1ded9
								}

								.product-list__sort-switcher[data-v-b46d8838] .gl-select__label {
										padding-left: 1.25rem
								}
						}

						@media only screen and (min-width: 1441px) {
								.product-list__footer[data-v-b46d8838],.product-list__header[data-v-b46d8838],.product-list__heading[data-v-b46d8838],.product-list__no-product[data-v-b46d8838] {
										border-left:.0625rem solid #4f4e4c;
										border-right: .0625rem solid #4f4e4c
								}

								.product-list__filters-switcher[data-v-b46d8838] {
										padding: 0 1.5rem
								}
						}

						.seo-links[data-v-366e744e] {
								height: 0;
								width: .0625rem;
								overflow: hidden
						}

						.breadcrumb[data-v-07a2e998] {
								color: #4f4e4c;
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								font-size: .75rem
						}

						.breadcrumb__link:hover .label[data-v-07a2e998] {
								text-decoration: underline
						}

						.breadcrumb__link--desactivated[data-v-07a2e998] {
								color: #aaa8a3
						}

						.breadcrumb__link--desactivated:hover .label[data-v-07a2e998] {
								text-decoration: none
						}

						@media only screen and (min-width: 1025px) {
								.breadcrumb[data-v-07a2e998] {
										font-size:1rem
								}
						}

						.products-grid[data-v-1e20e04e] {
								overflow: hidden;
								width: 100%
						}

						.products-grid__wrapper[data-v-1e20e04e] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								border-right: .0625rem solid #4f4e4c;
								border-bottom: .0625rem solid #4f4e4c;
								-ms-flex-wrap: wrap;
								flex-wrap: wrap
						}

						.product-card-wrapper[data-v-1e20e04e] {
								-ms-flex-preferred-size: 50%;
								flex-basis: 50%;
								border-top: .0625rem solid #4f4e4c;
								border-left: .0625rem solid #4f4e4c
						}

						.product-card-wrapper--no-border-top[data-v-1e20e04e] {
								border-top: none
						}

						@media only screen and (min-width: 768px) {
								.product-card-wrapper[data-v-1e20e04e] {
										-ms-flex-preferred-size:33.33333%;
										flex-basis: 33.33333%
								}
						}

						@media only screen and (min-width: 1025px) {
								.product-card-wrapper--4-columns[data-v-1e20e04e] {
										-ms-flex-preferred-size:25%;
										flex-basis: 25%
								}
						}

						.product-card[data-v-5f1fab5e] {
								width: 100%;
								position: relative;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;
								overflow: hidden;
								display: grid;
								grid-template-columns: auto;
								grid-template-rows: -webkit-min-content 1fr;
								grid-template-rows: min-content 1fr;
								height: 100%
						}

						.product-card__brand[data-v-5f1fab5e] {
								padding: 1rem;
								font-weight: 700
						}

						.product-card__description[data-v-5f1fab5e] {
								padding: 0 1rem
						}

						.product-card__seo-link[data-v-5f1fab5e] {
								display: block;
								position: absolute;
								top: -6rem
						}

						.product-card__reduced-spinner[data-v-5f1fab5e] {
								position: absolute;
								top: 0;
								right: 0;
								bottom: 0;
								left: 0;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center
						}

						.product-card__carousel[data-v-5f1fab5e] {
								overflow: hidden;
								width: 100%;
								height: 0;
								padding-top: 109.09091%;
								position: relative;
								background: #f3f2f0;
								cursor: pointer
						}

						.product-card__carousel-wrapper[data-v-5f1fab5e] {
								position: absolute;
								top: 0;
								right: 0;
								bottom: 0;
								left: 0
						}

						.product-card__carousel-action-btn[data-v-5f1fab5e] {
								position: absolute;
								top: 1rem;
								right: 1rem
						}

						.product-card__infos[data-v-5f1fab5e] {
								position: relative;
								overflow: hidden;
								height: 100%
						}

						.product-card__infos--edito[data-v-5f1fab5e] {
								height: 2.75rem
						}

						.product-card__infos--loading[data-v-5f1fab5e] {
								opacity: .5
						}

						.product-card__edito-cta[data-v-5f1fab5e] {
								background-color: #fff;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								padding: 1rem 0;
								position: relative
						}

						.product-card__variants[data-v-5f1fab5e] {
								display: none
						}

						@media only screen and (min-width: 768px) {
								.product-card[data-v-5f1fab5e] {
										display:block;
										height: auto
								}

								.product-card__infos[data-v-5f1fab5e] {
										height: 13.3125rem
								}

								.product-card__infos--edito[data-v-5f1fab5e] {
										height: 8.8125rem
								}
						}

						@media only screen and (min-width: 1025px) {
								.product-card__edito-cta[data-v-5f1fab5e] {
										pointer-events:none
								}

								.product-card__variants[data-v-5f1fab5e] {
										position: absolute;
										right: 0;
										bottom: 0;
										left: 0;
										-webkit-transition: -webkit-transform .33s ease-out;
										transition: -webkit-transform .33s ease-out;
										transition: transform .33s ease-out;
										transition: transform .33s ease-out,-webkit-transform .33s ease-out;
										-webkit-transform: translateY(100%);
										transform: translateY(100%);
										padding-top: 1rem;
										display: grid;
										width: 100%
								}

								.product-card__frame[data-v-5f1fab5e] {
										position: absolute;
										top: 0;
										right: 0;
										bottom: 0;
										left: 0;
										pointer-events: none;
										-webkit-box-sizing: border-box;
										box-sizing: border-box;
										border-style: solid;
										background-color: transparent;
										border-color: #4f4e4c;
										border-width: .1875rem
								}

								.product-card .fade-on-desktop[data-v-5f1fab5e] {
										opacity: 0;
										-webkit-transition: opacity .3s ease-out;
										transition: opacity .3s ease-out
								}

								.product-card .fade-on-desktop--show[data-v-5f1fab5e] {
										opacity: 1
								}

								.product-card:hover .product-card__variants[data-v-5f1fab5e] {
										-webkit-transform: translateY(0);
										transform: translateY(0)
								}
						}

						.product-title[data-v-5f1fab5e] {
								font-weight: 700
						}

						.bottomslide-enter-active[data-v-5f1fab5e],.bottomslide-leave-active[data-v-5f1fab5e] {
								-webkit-transition: -webkit-transform .2s ease-out;
								transition: -webkit-transform .2s ease-out;
								transition: transform .2s ease-out;
								transition: transform .2s ease-out,-webkit-transform .2s ease-out
						}

						.bottomslide-enter[data-v-5f1fab5e],.bottomslide-leave-to[data-v-5f1fab5e] {
								-webkit-transform: translateY(100%);
								transform: translateY(100%)
						}

						.product-info[data-v-73342aec] {
								width: 100%;
								height: 100%;
								padding: 1rem;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;
								overflow: hidden;
								position: relative
						}

						.product-info__seo-link[data-v-73342aec] {
								position: absolute;
								top: -6rem;
								font-size: .625rem;
								letter-spacing: .01562rem;
								line-height: 1.2
						}

						@media only screen and (min-width: 1025px) {
								.product-info__seo-link[data-v-73342aec] {
										font-size:.75rem;
										letter-spacing: 0;
										line-height: 1.33333
								}
						}

						.product-info__idle[data-v-73342aec] {
								display: grid;
								grid-template-columns: 1fr auto;
								grid-template-rows: repeat(3,-webkit-min-content);
								grid-template-rows: repeat(3,min-content);
								grid-gap: 0 .5rem;
								height: 100%;
								padding-bottom: 2.5rem;
								font-size: .625rem;
								letter-spacing: .01562rem;
								line-height: 1.2
						}

						@media only screen and (min-width: 1025px) {
								.product-info__idle[data-v-73342aec] {
										font-size:.75rem;
										letter-spacing: 0;
										line-height: 1.33333
								}
						}

						.product-info__note[data-v-73342aec] {
								display: none;
								position: absolute;
								bottom: 1rem;
								color: #aaa8a3
						}

						.product-info__brand[data-v-73342aec] {
								grid-row: 1;
								font-weight: 700;
								text-transform: uppercase
						}

						.product-info__brand[data-v-73342aec],.product-info__description[data-v-73342aec] {
								grid-column: 1;
								overflow: hidden;
								display: -webkit-box;
								-webkit-line-clamp: 2;
								-webkit-box-orient: vertical
						}

						.product-info__description[data-v-73342aec] {
								grid-row: 2;
								color: #4f4e4c;
								text-overflow: ellipsis
						}

						@media (-ms-high-contrast:active),(-ms-high-contrast:none) {
								.product-info__description[data-v-73342aec] {
										line-height: 1.25rem;
										max-height: 2.5rem
								}
						}

						.product-info__promotion-label[data-v-73342aec] {
								grid-row: 3;
								grid-column: 1;
								overflow: hidden;
								-webkit-line-clamp: 2;
								color: #4f4e4c;
								display: -webkit-box;
								-webkit-box-orient: vertical;
								font-family: GLBaderNarrow-Italic,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								font-size: .625rem;
								letter-spacing: .01562rem;
								line-height: 1.2
						}

						@media only screen and (min-width: 1025px) {
								.product-info__promotion-label[data-v-73342aec] {
										font-size:.75rem;
										letter-spacing: 0;
										line-height: 1.33333
								}
						}

						.product-info__price[data-v-73342aec] {
								display: grid;
								grid-template-columns: 1fr 1fr;
								grid-template-rows: 1rem 2rem;
								grid-gap: 0 .5rem;
								position: absolute;
								bottom: 0
						}

						.product-info__price-from[data-v-73342aec] {
								grid-column: 1;
								grid-row: 1
						}

						.product-info__price-final[data-v-73342aec] {
								grid-column: 1;
								grid-row: 2;
								font-weight: 700
						}

						.product-info__price-final--highlighted[data-v-73342aec] {
								color: #df5b53
						}

						.product-info__price-original[data-v-73342aec] {
								grid-column: 1/2;
								grid-row: 1;
								display: grid;
								grid-template-columns: repeat(2,-webkit-min-content);
								grid-template-columns: repeat(2,min-content)
						}

						.product-info__price-striked[data-v-73342aec] {
								color: #000;
								text-decoration: line-through
						}

						.product-info__promotion[data-v-73342aec] {
								font-weight: 700;
								grid-row: 2;
								grid-column: 2;
								position: absolute
						}

						@media only screen and (min-width: 768px) {
								.product-info__idle[data-v-73342aec] {
										grid-template-rows:repeat(3,-webkit-min-content) 1fr;
										grid-template-rows: repeat(3,min-content) 1fr;
										font-size: .75rem;
										letter-spacing: 0;
										line-height: 1.33333
								}
						}

						@media only screen and (min-width: 768px) and (min-width:1025px) {
								.product-info__idle[data-v-73342aec] {
										font-size:1rem;
										letter-spacing: 0;
										line-height: 1.25
								}
						}

						@media only screen and (min-width: 768px) {
								.product-info__note[data-v-73342aec] {
										display:block;
										font-size: .75rem;
										letter-spacing: 0;
										line-height: 1.33333
								}
						}

						@media only screen and (min-width: 768px) and (min-width:1025px) {
								.product-info__note[data-v-73342aec] {
										font-size:1rem;
										letter-spacing: 0;
										line-height: 1.25
								}
						}

						@media only screen and (min-width: 768px) {
								.product-info__promotion-label[data-v-73342aec] {
										font-size:.75rem;
										letter-spacing: 0;
										line-height: 1.33333
								}
						}

						@media only screen and (min-width: 768px) and (min-width:1025px) {
								.product-info__promotion-label[data-v-73342aec] {
										font-size:1rem;
										letter-spacing: 0;
										line-height: 1.25
								}
						}

						@media only screen and (min-width: 768px) {
								.product-info__price[data-v-73342aec] {
										grid-column:2;
										grid-row: 1/5;
										grid-template-columns: auto;
										grid-template-rows: repeat(2,-webkit-min-content) 1fr;
										grid-template-rows: repeat(2,min-content) 1fr;
										text-align: right;
										position: static
								}

								.product-info__price-from[data-v-73342aec] {
										display: initial;
										grid-column: 1;
										grid-row: 1
								}

								.product-info__price-final[data-v-73342aec] {
										grid-column: 1;
										grid-row: 2
								}

								.product-info__price-original[data-v-73342aec] {
										grid-column: 1;
										grid-row: 3;
										display: block
								}

								.product-info__promotion[data-v-73342aec] {
										position: relative
								}
						}

						@media only screen and (min-width: 1025px) {
								.product-info__price[data-v-73342aec] {
										grid-template-columns:auto auto;
										grid-template-rows: -webkit-min-content -webkit-min-content;
										grid-template-rows: min-content min-content;
										-webkit-column-gap: .5rem;
										-moz-column-gap: .5rem;
										column-gap: .5rem;
										-webkit-box-align: end;
										-ms-flex-align: end;
										align-items: end
								}

								.product-info__price-from[data-v-73342aec] {
										grid-column: 1;
										grid-row: 1;
										font-size: .625rem;
										letter-spacing: .01562rem;
										line-height: 1.2
								}
						}

						@media only screen and (min-width: 1025px) and (min-width:1025px) {
								.product-info__price-from[data-v-73342aec] {
										font-size:.75rem;
										letter-spacing: 0;
										line-height: 1.33333
								}
						}

						@media only screen and (min-width: 1025px) {
								.product-info__price-final[data-v-73342aec] {
										grid-column:2;
										grid-row: 1
								}

								.product-info__price-original[data-v-73342aec] {
										grid-column: 1/3;
										grid-row: 2
								}
						}

						.pagination[data-v-35cf1d02] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-user-select: none;
								-moz-user-select: none;
								-ms-user-select: none;
								user-select: none;
								-webkit-box-orient: horizontal;
								-webkit-box-direction: normal;
								-ms-flex-direction: row;
								flex-direction: row;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center;
								color: #4f4e4c;
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif
						}

						.pagination__element[data-v-35cf1d02] {
								cursor: pointer;
								text-align: center;
								margin-left: 1rem;
								padding: .75rem 0
						}

						.pagination__element--first[data-v-35cf1d02] {
								margin-left: 0
						}

						.pagination__element--current[data-v-35cf1d02] {
								font-weight: 700;
								color: #000
						}

						.pagination__separator[data-v-35cf1d02] {
								width: 6px;
								height: 100%;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								position: relative;
								margin: 0 .75rem
						}

						.pagination__separator[data-v-35cf1d02]:after {
								content: "";
								width: 100%;
								bottom: 50%;
								position: absolute;
								-webkit-transform: translateY(50%);
								transform: translateY(50%);
								border-bottom: .0625rem solid #aaa8a3
						}

						.pagination__action[data-v-35cf1d02],.pagination__arrow[data-v-35cf1d02],.pagination__element[data-v-35cf1d02],.pagination__outer-action[data-v-35cf1d02] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								-ms-flex-line-pack: center;
								align-content: center;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center
						}

						.pagination__arrow[data-v-35cf1d02],.pagination__outer-page[data-v-35cf1d02] {
								cursor: pointer
						}

						.pagination__arrow[data-v-35cf1d02] {
								width: 2.5rem;
								height: 2.5rem;
								border-radius: 50%
						}

						.pagination__arrow[data-v-35cf1d02]:hover {
								background: #6fff00
						}

						.pagination__arrow--left[data-v-35cf1d02] {
								margin-right: 1rem;
								margin-left: -1rem
						}

						.pagination__arrow--right[data-v-35cf1d02] {
								margin-left: 1rem;
								margin-right: -1rem
						}

						.pagination--large .pagination__element[data-v-35cf1d02] {
								margin-left: 1.5rem
						}

						.pagination--large .pagination__element--first[data-v-35cf1d02] {
								margin-left: 0
						}

						.pagination--large .pagination__separator[data-v-35cf1d02] {
								width: .5rem
						}

						.pagination--large .pagination__separator[data-v-35cf1d02]:after {
								bottom: 50%;
								-webkit-transform: translateY(.25rem);
								transform: translateY(.25rem)
						}

						.pagination--large .pagination__arrow--left[data-v-35cf1d02] {
								margin-right: 2.5rem
						}

						.pagination--large .pagination__arrow--right[data-v-35cf1d02] {
								margin-left: 2.5rem
						}

						.footer-reinsure[data-v-4b6fd7ae] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-orient: vertical;
								-webkit-box-direction: normal;
								-ms-flex-direction: column;
								flex-direction: column;
								-webkit-box-pack: justify;
								-ms-flex-pack: justify;
								justify-content: space-between;
								border-bottom: 1px solid #4f4e4c;
								max-width: 1440px;
								margin: 0 auto;
								padding: 55px 26px 65px 27px
						}

						.footer-reinsure--prehome[data-v-4b6fd7ae] {
								position: relative
						}

						.footer-reinsure__item+.footer-reinsure__item[data-v-4b6fd7ae] {
								margin-top: 34px
						}

						.footer-reinsure--prehome[data-v-4b6fd7ae]:before {
								content: "";
								width: 100%;
								height: 1px;
								background-color: #4f4e4c;
								position: absolute;
								top: 0;
								left: 0;
								z-index: 100
						}

						@media only screen and (min-width: 768px) {
								.footer-reinsure[data-v-4b6fd7ae] {
										-webkit-box-orient:horizontal;
										-webkit-box-direction: normal;
										-ms-flex-direction: row;
										flex-direction: row;
										padding: 40px 10px 45px 16px
								}

								.footer-reinsure__item[data-v-4b6fd7ae] {
										-ms-flex-preferred-size: 100%;
										flex-basis: 100%
								}

								.footer-reinsure__item+.footer-reinsure__item[data-v-4b6fd7ae] {
										margin-top: 0;
										margin-left: 24px
								}
						}

						@media only screen and (min-width: 1025px) {
								.footer-reinsure[data-v-4b6fd7ae] {
										padding:80px 20px 80px 42px
								}

								.footer-reinsure__item+.footer-reinsure__item[data-v-4b6fd7ae] {
										margin-left: 62px
								}
						}

						@media only screen and (min-width: 1281px) {
								.footer-reinsure[data-v-4b6fd7ae] {
										padding:80px 30px 80px 50px
								}

								.footer-reinsure__item+.footer-reinsure__item[data-v-4b6fd7ae] {
										margin-left: 80px
								}
						}

						@media only screen and (min-width: 1441px) {
								.footer-reinsure[data-v-4b6fd7ae] {
										padding:80px 30px 80px 60px;
										border-left: 1px solid #4f4e4c;
										border-right: 1px solid #4f4e4c
								}

								.footer-reinsure__item+.footer-reinsure__item[data-v-4b6fd7ae] {
										margin-left: 90px
								}
						}

						.reinsure-item[data-v-32a9591e] {
								-webkit-box-orient: horizontal;
								-ms-flex-direction: row;
								flex-direction: row
						}

						.reinsure-item[data-v-32a9591e],.reinsure-item__wrapper[data-v-32a9591e] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-direction: normal
						}

						.reinsure-item__wrapper[data-v-32a9591e] {
								-webkit-box-orient: vertical;
								-ms-flex-direction: column;
								flex-direction: column
						}

						.reinsure-item__link[data-v-32a9591e] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex
						}

						.reinsure-item__icon[data-v-32a9591e] {
								margin-bottom: 0;
								height: 40px;
								margin-right: 13px;
								display: block
						}

						.reinsure-item__title[data-v-32a9591e] {
								margin-bottom: 6px;
								font-family: GLBaderCompress-Black,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								font-size: 1.25rem;
								line-height: 1;
								letter-spacing: -.025rem;
								text-transform: uppercase;
								color: #000
						}

						.reinsure-item__label[data-v-32a9591e] {
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								font-size: .75rem;
								line-height: 1.33;
								color: #4f4e4c;
								text-decoration: underline
						}

						.reinsure-item__label[data-v-32a9591e]:hover {
								color: #000
						}

						@media only screen and (min-width: 768px) {
								.reinsure-item[data-v-32a9591e] {
										-webkit-box-orient:vertical;
										-webkit-box-direction: normal;
										-ms-flex-direction: column;
										flex-direction: column
								}

								.reinsure-item__link[data-v-32a9591e] {
										display: block
								}

								.reinsure-item__icon[data-v-32a9591e] {
										margin-right: auto;
										margin-bottom: 10px
								}

								.reinsure-item__title[data-v-32a9591e] {
										font-size: 1.5rem;
										margin-bottom: 10px
								}
						}

						@media only screen and (min-width: 1281px) {
								.reinsure-item__label[data-v-32a9591e] {
										font-size:1rem;
										line-height: 1.25
								}

								.reinsure-item__title[data-v-32a9591e] {
										font-size: 1.75rem;
										letter-spacing: -.03125rem;
										margin-bottom: 13px
								}
						}

						.footer[data-v-ede4650c] {
								width: 100%;
								padding: 0
						}

						@media only screen and (min-width: 1441px) {
								.footer[data-v-ede4650c] {
										max-width:1440px;
										margin: 0 auto
								}
						}

						.footer-social[data-v-089d7dca] {
								background-color: #000;
								color: #fff;
								padding: 0 60.5px;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								min-height: 233px;
								border: 1px solid #4f4e4c
						}

						.footer-social .texts[data-v-089d7dca] {
								-webkit-box-flex: 1;
								-ms-flex: 1;
								flex: 1
						}

						.footer-social .texts__title[data-v-089d7dca] {
								text-transform: uppercase;
								font-family: GLBaderCompress-Black,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								font-size: 2.5rem
						}

						.footer-social .texts__subtitle[data-v-089d7dca] {
								font-size: 1rem;
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif
						}

						.footer-social .social-buttons[data-v-089d7dca] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center
						}

						.footer-social .social-buttons__button[data-v-089d7dca] {
								width: 128px
						}

						.footer-social .social-buttons__button[data-v-089d7dca],.footer-social .social-buttons__button .button-icon[data-v-089d7dca] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center
						}

						.footer-social .social-buttons__button .button-icon[data-v-089d7dca] {
								cursor: pointer;
								background-color: #fff;
								width: 64px;
								height: 64px;
								padding: 8px;
								border-radius: 100%;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								-webkit-transition: background-color .3s,width .3s,height .3s;
								transition: background-color .3s,width .3s,height .3s
						}

						.footer-social .social-buttons__button .button-icon[data-v-089d7dca] .icon {
								width: 100%;
								height: 100%
						}

						.footer-social .social-buttons__button .button-icon[data-v-089d7dca]:hover {
								width: 80px;
								height: 80px;
								background-color: #6fff00
						}

						@media (max-width: 1439px) {
								.footer-social[data-v-089d7dca] {
										border-right:0;
										border-left: 0
								}
						}

						@media (max-width: 1023px) {
								.footer-social[data-v-089d7dca] {
										-webkit-box-orient:vertical;
										-webkit-box-direction: normal;
										-ms-flex-direction: column;
										flex-direction: column;
										-webkit-box-align: start;
										-ms-flex-align: start;
										align-items: flex-start
								}

								.footer-social .social-buttons__button[data-v-089d7dca] {
										-webkit-box-pack: start;
										-ms-flex-pack: start;
										justify-content: start
								}

								.footer-social .social-buttons__button .button-icon[data-v-089d7dca] {
										pointer-events: none
								}
						}

						@media (min-width: 768px) and (max-width:1023px) {
								.footer-social[data-v-089d7dca] {
										-webkit-box-pack:center;
										-ms-flex-pack: center;
										justify-content: center;
										padding: 0 30.25px;
										min-height: 250px
								}

								.footer-social .texts[data-v-089d7dca] {
										margin-bottom: 32px
								}

								.footer-social .social-buttons[data-v-089d7dca],.footer-social .texts[data-v-089d7dca] {
										-webkit-box-flex: unset;
										-ms-flex: unset;
										flex: unset
								}
						}

						@media (min-width: 318px) and (max-width:767px) {
								.footer-social[data-v-089d7dca] {
										padding:60.5px 30.25px
								}

								.footer-social .texts__title[data-v-089d7dca] {
										line-height: 1;
										margin-bottom: 16px
								}

								.footer-social .texts__subtitle[data-v-089d7dca] {
										margin-bottom: -32px
								}

								.footer-social .social-buttons[data-v-089d7dca] {
										margin-top: 60.5px;
										width: 100%;
										-webkit-box-align: start;
										-ms-flex-align: start;
										align-items: flex-start
								}

								.footer-social .social-buttons__button[data-v-089d7dca] {
										width: 80px
								}

								.footer-social .social-buttons__button .button-icon[data-v-089d7dca] {
										width: 56px;
										height: 56px
								}
						}

						.footer-links[data-v-7f510af2] {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								padding: 40px 0;
								border: 1px solid #4f4e4c
						}

						.footer-links .links-container[data-v-7f510af2] {
								text-align: left;
								padding-left: 60.5px
						}

						.footer-links .links-container[data-v-7f510af2]:first-child {
								border-right: 1px solid #e1ded9
						}

						.footer-links .links-container__title[data-v-7f510af2] {
								color: #000;
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								font-weight: 700;
								font-size: 1.25rem;
								margin-bottom: 16px
						}

						.footer-links .links-container__link-list[data-v-7f510af2] {
								vertical-align: top;
								padding: 0
						}

						.footer-links .links-container__link-list--first[data-v-7f510af2] {
								-webkit-column-count: 4;
								-moz-column-count: 4;
								column-count: 4
						}

						.footer-links .links-container__link-list--second[data-v-7f510af2] {
								-webkit-column-count: 3;
								-moz-column-count: 3;
								column-count: 3
						}

						.footer-links .links-container__link-list .link[data-v-7f510af2] {
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								font-size: 1rem;
								display: inline-block;
								width: 100%;
								margin-bottom: 8px;
								color: #4f4e4c
						}

						.footer-links .links-container__link-list .link--break[data-v-7f510af2] {
								-webkit-column-break-after: column;
								-moz-column-break-after: column;
								break-after: column
						}

						.footer-links .links-container__link-list .link[data-v-7f510af2]:hover {
								text-decoration: underline
						}

						@media (max-width: 1439px) {
								.footer-links[data-v-7f510af2] {
										border-right:0;
										border-left: 0
								}
						}

						@media (min-width: 1024px) and (max-width:1279px) {
								.footer-links .links-container[data-v-7f510af2]:first-child {
										min-width:50%
								}

								.footer-links .links-container__link-list[data-v-7f510af2] {
										-webkit-column-count: 3;
										-moz-column-count: 3;
										column-count: 3
								}
						}

						@media (max-width: 1023px) {
								.footer-links[data-v-7f510af2] {
										padding:0 30.25px;
										-webkit-box-orient: vertical;
										-webkit-box-direction: normal;
										-ms-flex-direction: column;
										flex-direction: column
								}

								.footer-links .links-container[data-v-7f510af2] {
										padding: 50px 0
								}

								.footer-links .links-container[data-v-7f510af2]:first-child {
										width: 100%;
										border: 0;
										border-bottom: 1px solid #e1ded9
								}
						}

						@media (min-width: 768px) and (max-width:1023px) {
								.footer-links .links-container__link-list--second[data-v-7f510af2] {
										-webkit-column-count:4;
										-moz-column-count: 4;
										column-count: 4
								}
						}

						@media (min-width: 318px) and (max-width:767px) {
								.footer-links .links-container__link-list[data-v-7f510af2] {
										-webkit-column-count:2;
										-moz-column-count: 2;
										column-count: 2
								}
						}

						.footer-general-conditions[data-v-7c945cea] {
								width: 100%;
								border: 1px solid #4f4e4c;
								border-top: 0;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-ms-flex-pack: distribute;
								justify-content: space-around;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center;
								-ms-flex-wrap: wrap;
								flex-wrap: wrap;
								padding: 24px
						}

						.footer-general-conditions__condition[data-v-7c945cea] {
								font-family: GLBaderNarrow-Regular,Arial,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
								color: #aaa8a3;
								white-space: nowrap;
								padding: 8px
						}

						.footer-general-conditions__condition[data-v-7c945cea]:hover {
								text-decoration: underline
						}

						@media (max-width: 1439px) {
								.footer-general-conditions[data-v-7c945cea] {
										border-right:0;
										border-left: 0;
										border-bottom: 0
								}
						}

						@media (max-width: 1023px) {
								.footer-general-conditions[data-v-7c945cea] {
										border-right:0;
										border-left: 0;
										padding: 18px;
										-webkit-box-orient: vertical;
										-webkit-box-direction: normal;
										-ms-flex-direction: column;
										flex-direction: column;
										text-align: center
								}

								.footer-general-conditions__condition[data-v-7c945cea] {
										padding: 8px;
										white-space: normal
								}
						}

						@-webkit-keyframes serverAnmiation-data-v-8c62ac4e {
								0% {
										width: 100%
								}

								50% {
										width: 20%
								}

								60% {
										width: 18%
								}

								to {
										width: 0
								}
						}

						@keyframes serverAnmiation-data-v-8c62ac4e {
								0% {
										width: 100%
								}

								50% {
										width: 20%
								}

								60% {
										width: 18%
								}

								to {
										width: 0
								}
						}

						.page-loader[data-v-8c62ac4e] {
								position: fixed;
								-ms-touch-action: none;
								touch-action: none;
								width: 100vw;
								height: 100vh;
								margin: auto;
								top: 0;
								right: 0;
								left: 0;
								bottom: 0;
								z-index: 8000;
								background-color: #fff;
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-webkit-box-pack: center;
								-ms-flex-pack: center;
								justify-content: center;
								-webkit-box-align: center;
								-ms-flex-align: center;
								align-items: center
						}

						.page-loader .logo[data-v-8c62ac4e] {
								position: relative;
								width: 181px
						}

						.page-loader .logo__overlay[data-v-8c62ac4e] {
								background: hsla(0,0%,100%,.7);
								position: absolute;
								width: 100%;
								height: 100%;
								-webkit-transition: width 1s;
								transition: width 1s;
								right: 0
						}

						.page-loader .logo__overlay--start[data-v-8c62ac4e] {
								width: 20%
						}

						.page-loader .logo__overlay--end[data-v-8c62ac4e] {
								width: 0
						}

						.page-loader .logo__overlay--server[data-v-8c62ac4e] {
								-webkit-animation: serverAnmiation-data-v-8c62ac4e 5s;
								animation: serverAnmiation-data-v-8c62ac4e 5s;
								-webkit-animation-fill-mode: forwards;
								animation-fill-mode: forwards
						}

						.loader-background-fade-enter-active[data-v-8c62ac4e],.loader-background-fade-leave-active[data-v-8c62ac4e] {
								-webkit-transition: opacity .5s;
								transition: opacity .5s
						}

						.loader-logo-fade-enter-active[data-v-8c62ac4e],.loader-logo-fade-leave-active[data-v-8c62ac4e] {
								-webkit-transition: opacity .3s;
								transition: opacity .3s
						}

						.loader-background-fade-enter[data-v-8c62ac4e],.loader-background-fade-leave-to[data-v-8c62ac4e],.loader-logo-fade-enter[data-v-8c62ac4e],.loader-logo-fade-leave-to[data-v-8c62ac4e] {
								opacity: 0
						}

						@media only screen and (min-width: 768px) {
								.page-loader .logo[data-v-8c62ac4e] {
										width:192px
								}
						}

						@media only screen and (min-width: 1025px) {
								.page-loader .logo[data-v-8c62ac4e] {
										width:360px
								}
						}
				</style>
				<script type='text/javascript' src='https://cdn.tagcommander.com/251/tc_GaleriesLafayette_18.js' id='18'></script>
				<script type='text/javascript' id='abtasty'>
						(function(i, s, o, g, r, a, m) {
								i[r] = i[r] || [],
								i["abtiming"] = 1 * new Date();
								a = s.createElement(o),
								m = s.getElementsByTagName(o)[0];
								a.async = 1;
								a.src = g;
								m.parentNode.insertBefore(a, m)
						}
						)(window, document, "script", "//try.abtasty.com/e727942257aae7bc6c7162b3b993d2e2.js", "_abtasty");
				</script>
		</head>
		
		
	 <body>
		 <div id="app" data-server-rendered="true">
				 <section>
						 <section class="dynamic-components" data-v-995baa2e>
								 <div name="Header" __typename="Header" class="header dynamic-components__component" data-v-d29e4508 data-v-995baa2e>
										 <div class="header__borders" data-v-d29e4508>
												 <div class="header__borders-content" data-v-d29e4508></div>
										 </div>
										 <header class="header__content" data-v-d29e4508>
												 <div class="header__wrapper" data-v-d29e4508>
														 <div class="header__primary" data-v-d29e4508>
																 <div class="primary-header" data-v-02b99818 data-v-d29e4508>
																		 <div class="primary-header__content" data-v-02b99818>
																				 <div class="primary-header-left" data-v-37a99f49 data-v-02b99818>
																						 <span data-href="http:||||www.galerieslafayette.com||i||nos-magasins" data-test-id="store-locator" class="icon-link primary-header-left__find-store" style="--label-color:#aaa8a3;" data-v-0691da03 data-v-37a99f49>
																								 <div class="icon__wrapper icon-link__icon" style="--size:32px;" data-v-53fe7467 data-v-0691da03>
																										 
																								 </div>
																								 <div class="icon-link__label left" data-v-0691da03>Trouver un magasin </div>
																						 </span>
																				 </div>
																				 <div class="primary-header-logo" data-v-5ff8d89c data-v-02b99818>
																						 <a href="https://www.galerieslafayette.com/" class="router-link-active" data-v-5ff8d89c>
																								 <img data-test-id="logo" src="https://www.galerieslafayette.com/ggl-front-assets.logo.094d6478.svg" alt="Galeries Lafayette" class="primary-header-logo__img" data-v-5ff8d89c>
																						 </a>
																				 </div>
																				 <div class="primary-header-right" data-v-7445d06a data-v-02b99818>
																						 <div class="header__user" data-v-7445d06a>
																								 <div class="user-info" data-v-34d63d70 data-v-7445d06a>
																										 <div class="m-user-picto" data-v-2f51e7bd data-v-34d63d70>
																												 <span data-href="" data-test-id="account" class="icon-link" style="--label-color:#aaa8a3;" data-v-0691da03 data-v-2f51e7bd>
																														 <div class="icon-link__label right" data-v-0691da03>Se connecter </div>
																														 <div class="icon__wrapper icon-link__icon" style="--size:32px;" data-v-53fe7467 data-v-0691da03>
																																 
																														 </div>
																												 </span>
																										 </div>
																								 </div>
																						 </div>
																						 <div class="minicart-wrapper" data-v-7445d06a>
																								 <span data-href="" data-test-id="cart" class="icon-link minicart" style="--label-color:#aaa8a3;" data-v-0691da03 data-v-c073c288 data-v-7445d06a>
																										 <div class="icon-link__label right" data-v-0691da03>Panier (0) </div>
																										 <div class="icon__wrapper icon-link__icon" style="--size:32px;" data-v-53fe7467 data-v-0691da03>
																												 
																										 </div>
																								 </span>
																								 <div class="minicart-overlay minicart-overlay" data-v-420b2b6e data-v-7445d06a>
																										 <div class="minicart-overlay__block product-list-block" data-v-420b2b6e></div>
																										 <div class="separator" data-v-420b2b6e></div>
																										 <div class="minicart-overlay__block total-block" data-v-420b2b6e>
																												 <div class="total-row" data-v-420b2b6e>
																														 <div class="total-label-column" data-v-420b2b6e>
																																 <span class="total-label" data-v-420b2b6e>Total</span>
																																 <span class="total-entries-label" data-v-420b2b6e>(0 articles)</span>
																														 </div>
																														 <div class="total-value-column" data-v-420b2b6e>0,00 € </div>
																												 </div>
																												 <div class="legal-notice-row" data-v-420b2b6e>Hors avantages, frais de port et services / Livraison OFFERTE sous conditions </div>
																												 <div class="validate-button-row" data-v-420b2b6e>
																														 <button class="gl-button validate-button gl-button-primary" data-v-420b2b6e>Valider la commande </button>
																												 </div>
																										 </div>
																								 </div>
																						 </div>
																				 </div>
																		 </div>
																 </div>
														 </div>
														 <div class="header__secondary" data-v-d29e4508>
																 <div class="secondary-header" data-v-b266d208 data-v-d29e4508>
																		 <div class="secondary-header__content" data-v-b266d208>
																				 <div class="navigation-list" data-v-b266d208>
																						 <a href="https://www.galerieslafayette.com/c/soldes" class="navigation-list__link" data-v-b266d208>Soldes </a>
																						 <a href="https://www.galerieslafayette.com/h/femme" class="navigation-list__link" style="color:#000000;" data-v-b266d208>Femme </a>
																						 <a href="https://www.galerieslafayette.com/h/homme" class="navigation-list__link" style="color:#000000;" data-v-b266d208>Homme </a>
																						 <a href="https://www.galerieslafayette.com/h/beaute" class="navigation-list__link" style="color:#000000;" data-v-b266d208>Beauté </a>
																						 <a href="https://www.galerieslafayette.com/h/enfant" class="navigation-list__link" style="color:#000000;" data-v-b266d208>Enfant </a>
																						 <a href="https://www.galerieslafayette.com/h/maison" class="navigation-list__link" style="color:#000000;" data-v-b266d208>Maison </a>
																						 <a href="https://www.galerieslafayette.com/h/createurs" class="navigation-list__link" data-v-b266d208>Luxe et créateurs </a>
																						 <a href="https://www.galerieslafayette.com/c/go+for+good#ebp=SmMvxkqDL6S1a502afB.oke2N1OyXt1BelxkrS0XZqKu" class="navigation-list__link" style="color:#000000;" data-v-b266d208>Mode responsable </a>
																						 <a href="https://www.galerieslafayette.com/marques" class="navigation-list__link" style="color:#000000;" data-v-b266d208>Marques </a>
																						 <a href="https://www.galerieslafayette.com/evt/faq" class="navigation-list__link" data-v-b266d208>Nos services </a>
																				 </div>
																				 <div class="search-bar search-bar" data-v-070b8510 data-v-b266d208>
																						 <div class="search-bar__form" data-v-070b8510>
																								 <div class="search-bar__field-box" data-v-070b8510>
																										 <form method="get" data-v-070b8510>
																												 <input placeholder="Rechercher une marque, un article..." data-test-id="charSearch-searchBar" type="text" value="" class="body-m search-bar__field search-bar__field--default" data-v-070b8510>
																												 <button data-test-id="charSearch-searchButton" class="icon__wrapper search-bar__icon search-bar__icon--submit icon__wrapper--clickable" style="--size:32px;" data-v-53fe7467 data-v-070b8510>
																														 
																												 </button>
																										 </form>
																								 </div>
																						 </div>
																				 </div>
																		 </div>
																		 <div class="secondary-header__mega-menu-container" style="display:none;" data-v-b266d208 data-v-b266d208>
																				 <div data-test-id="layer-soldes" class="mega-menu" data-v-0745f5da>
																						 <div class="mega-menu__content" data-v-0745f5da>
																								 <div class="sub-universes" data-v-0745f5da>
																										 <ul id="_507nn9dtc-sub-universe--0" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||femme" class="title-row-label" data-v-0745f5da>Femme </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||femme-vetements" class="category-label" data-v-0745f5da>Vêtements </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||femme-sacs+et+bagages" class="category-label" data-v-0745f5da>Sacs &amp;Bagages </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||femme-chaussures" class="category-label" data-v-0745f5da>Chaussures </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||femme-lingerie" class="category-label" data-v-0745f5da>Lingerie </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||femme-accessoires" class="category-label" data-v-0745f5da>Accessoires </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||femme-montres" class="category-label" data-v-0745f5da>Montres </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||femme-bijouterie+et+joaillerie" class="category-label" data-v-0745f5da>Bijoux </span>
																												 </li>
																										 </ul>
																										 <ul id="_507nn9dtc-sub-universe--1" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||homme" class="title-row-label" data-v-0745f5da>Homme </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||homme-vetements" class="category-label" data-v-0745f5da>Vêtements </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||homme-chaussures" class="category-label" data-v-0745f5da>Chaussures </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||homme-sacs+et+bagages" class="category-label" data-v-0745f5da>Sacs &amp;Bagages </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||homme-montres" class="category-label" data-v-0745f5da>Montres </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||homme-sous-vetements+et+homewear-sous-vetements" class="category-label" data-v-0745f5da>Sous-vêtements </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||homme-accessoires" class="category-label" data-v-0745f5da>Accessoires </span>
																												 </li>
																										 </ul>
																										 <ul id="_507nn9dtc-sub-universe--2" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||soldes-soldes+enfant" class="title-row-label" data-v-0745f5da>Enfant </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes-soldes+enfant||ct||fille" class="category-label" data-v-0745f5da>Fille </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes-soldes+enfant||ct||garcon" class="category-label" data-v-0745f5da>Garçon </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes-soldes+enfant||ct||bebe" class="category-label" data-v-0745f5da>Bébé </span>
																												 </li>
																										 </ul>
																										 <ul id="_507nn9dtc-sub-universe--3" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||beaute" class="title-row-label" data-v-0745f5da>Beauté </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||beaute-parfum" class="category-label" data-v-0745f5da>Parfums </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||beaute-soin" class="category-label" data-v-0745f5da>Soins </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||beaute-maquillage" class="category-label" data-v-0745f5da>Maquillage </span>
																												 </li>
																										 </ul>
																										 <ul id="_507nn9dtc-sub-universe--4" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||maison" class="title-row-label" data-v-0745f5da>Maison </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||maison-culinaire" class="category-label" data-v-0745f5da>Culinaire </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||maison-arts+de+la+table" class="category-label" data-v-0745f5da>Arts de la table </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||maison-linge+de+maison" class="category-label" data-v-0745f5da>Linge de maison </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||maison-meubles+et+canapes" class="category-label" data-v-0745f5da>Meubles et canapés </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||maison-decoration+-+senteurs" class="category-label" data-v-0745f5da>Décorations &amp;senteurs </span>
																												 </li>
																										 </ul>
																										 <ul id="_507nn9dtc-sub-universe--5" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||soldes" class="title-row-label" data-v-0745f5da>Marques </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||fm||a+p+c+" class="category-label" data-v-0745f5da>A.P.C. </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||fm||boss" class="category-label" data-v-0745f5da>Boss </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||fm||cristel" class="category-label" data-v-0745f5da>Cristel </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||fm||guerlain" class="category-label" data-v-0745f5da>Guerlain </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||fm||des+petits+hauts" class="category-label" data-v-0745f5da>Des petits hauts </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||fm||jonak" class="category-label" data-v-0745f5da>Jacquemus </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||fm||jonak" class="category-label" data-v-0745f5da>Jonak </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||ct||homme||fm||lacoste" class="category-label" data-v-0745f5da>Lacoste </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||fm||maje" class="category-label" data-v-0745f5da>Maje </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||fm||polo+ralph+lauren" class="category-label" data-v-0745f5da>Polo Ralph Lauren </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||fm||sandro" class="category-label" data-v-0745f5da>Sandro </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||fm||tommy+hilfiger" class="category-label" data-v-0745f5da>Tommy Hilfiger </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||soldes||fm||zadig+voltaire" class="category-label" data-v-0745f5da>Zadig &amp;Voltaire </span>
																												 </li>
																										 </ul>
																								 </div>
																								 <div class="sidebar" data-v-0745f5da>
																										 <a href="https://www.galerieslafayette.com/c/soldes" class="sidebar__title" data-v-0745f5da>Sélections </a>
																										 <a href="https://www.galerieslafayette.com/c/soldes-coups+de+coeur+soldes" class="sidebar__link" data-v-0745f5da>Coups de coeur soldes </a>
																										 <span data-href="||c||soldes" class="sidebar__link" data-v-0745f5da>
																												 <img alt="" class="link-image" data-v-0745f5da>
																										 </span>
																								 </div>
																						 </div>
																				 </div>
																				 <div data-test-id="layer-femme" class="mega-menu" style="display:none;" data-v-0745f5da>
																						 <div class="mega-menu__content" data-v-0745f5da>
																								 <div class="sub-universes" data-v-0745f5da>
																										 <ul id="_javh1m1s6-sub-universe--0" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/h/createurs" class="title-row-label" data-v-0745f5da>Luxe &amp;Créateurs </a>
																												 </li>
																										 </ul>
																										 <ul id="_javh1m1s6-sub-universe--1" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/vetements" class="title-row-label" data-v-0745f5da>Vêtements </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/vetements/manteaux" class="category-label" data-v-0745f5da>Manteaux </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/vetements/vestes" class="category-label" data-v-0745f5da>Vestes </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/vetements/hauts/pulls" class="category-label" data-v-0745f5da>Pulls </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/vetements/hauts/gilets" class="category-label" data-v-0745f5da>Gilets </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/vetements/robes" class="category-label" data-v-0745f5da>Robes </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/vetements/hauts/sweat-shirts" class="category-label" data-v-0745f5da>Sweat-shirts </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/vetements/hauts/tee-shirts" class="category-label" data-v-0745f5da>T-shirts </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/vetements/hauts/chemises" class="category-label" data-v-0745f5da>Chemises </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/vetements/bas/pantalons" class="category-label" data-v-0745f5da>Pantalons </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/vetements/bas/jeans" class="category-label" data-v-0745f5da>Jeans </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/vetements/combinaisons" class="category-label" data-v-0745f5da>Combinaisons </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/vetements/bas/jupes" class="category-label" data-v-0745f5da>Jupes </a>
																												 </li>
																										 </ul>
																										 <ul id="_javh1m1s6-sub-universe--2" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/lingerie-de-jour" class="title-row-label" data-v-0745f5da>Lingerie </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/lingerie-de-jour/hauts/soutien-gorges" class="category-label" data-v-0745f5da>Soutien-gorges </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/lingerie-de-jour/bas" class="category-label" data-v-0745f5da>Culottes </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/lingerie-de-jour/chaussettes-collants" class="category-label" data-v-0745f5da>Chaussettes &amp;Collants </a>
																												 </li>
																										 </ul>
																										 <ul id="_javh1m1s6-sub-universe--3" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/bain" class="title-row-label" data-v-0745f5da>Maillots de bain </a>
																												 </li>
																										 </ul>
																										 <ul id="_javh1m1s6-sub-universe--4" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/homewear" class="title-row-label" data-v-0745f5da>Homewear </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/homewear/pyjamas" class="category-label" data-v-0745f5da>Pyjamas </a>
																												 </li>
																										 </ul>
																										 <ul id="_javh1m1s6-sub-universe--5" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/sacs-bagages" class="title-row-label" data-v-0745f5da>Sacs &amp;Bagages </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/sacs-bagages/sacs" class="category-label" data-v-0745f5da>Sacs </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/sacs-bagages/petite-maroquinerie" class="category-label" data-v-0745f5da>Petite maroquinerie </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/sacs-bagages/bagages" class="category-label" data-v-0745f5da>Bagages </a>
																												 </li>
																										 </ul>
																										 <ul id="_javh1m1s6-sub-universe--6" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/chaussures" class="title-row-label" data-v-0745f5da>Chaussures </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/chaussures/bottes-bottines" class="category-label" data-v-0745f5da>Bottes &amp;Bottines </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/chaussures/baskets" class="category-label" data-v-0745f5da>Baskets </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/chaussures/chaussures-de-ville/mocassins" class="category-label" data-v-0745f5da>Mocassins </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/chaussures/chaussures-de-ville/ballerines-babies" class="category-label" data-v-0745f5da>Ballerines &amp;Babies </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/chaussures/chaussures-de-ville/derbies-richelieus" class="category-label" data-v-0745f5da>Derbies &amp;Richelieus </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/chaussures/chaussures-de-ville/escarpins" class="category-label" data-v-0745f5da>Escarpins </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/chaussures/chaussures-ete/sandales" class="category-label" data-v-0745f5da>Sandales </a>
																												 </li>
																										 </ul>
																										 <ul id="_javh1m1s6-sub-universe--7" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/accessoires/bijoux" class="title-row-label" data-v-0745f5da>Bijouterie </a>
																												 </li>
																										 </ul>
																										 <ul id="_javh1m1s6-sub-universe--8" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/accessoires/joaillerie" class="title-row-label" data-v-0745f5da>Joaillerie </a>
																												 </li>
																										 </ul>
																										 <ul id="_javh1m1s6-sub-universe--9" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/accessoires/montres" class="title-row-label" data-v-0745f5da>Montres </a>
																												 </li>
																										 </ul>
																										 <ul id="_javh1m1s6-sub-universe--10" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/sport/ct/femme" class="title-row-label" data-v-0745f5da>Sport </a>
																												 </li>
																										 </ul>
																										 <ul id="_javh1m1s6-sub-universe--11" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/accessoires" class="title-row-label" data-v-0745f5da>Accessoires </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/accessoires/gants-mitaines-mouffles" class="category-label" data-v-0745f5da>Gants </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/accessoires/chapeaux-bonnets-casquettes/bonnets" class="category-label" data-v-0745f5da>Bonnets </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/accessoires/foulards-echarpes" class="category-label" data-v-0745f5da>Echarpes &amp;Foulards </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/accessoires/ceintures/ceintures" class="category-label" data-v-0745f5da>Ceintures </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/femme/accessoires/lunettes" class="category-label" data-v-0745f5da>Lunettes </a>
																												 </li>
																										 </ul>
																										 <ul id="_javh1m1s6-sub-universe--12" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/go+for+good/ct/femme" class="title-row-label" data-v-0745f5da>Mode Responsable </a>
																												 </li>
																										 </ul>
																										 <ul id="_javh1m1s6-sub-universe--13" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/marques" class="title-row-label" data-v-0745f5da>Top marques </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/prada/ct/femme" class="category-label" data-v-0745f5da>Prada </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/sandro" class="category-label" data-v-0745f5da>Sandro </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/maje" class="category-label" data-v-0745f5da>Maje </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/jacquemus" class="category-label" data-v-0745f5da>Jacquemus </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/the+kooples" class="category-label" data-v-0745f5da>The Kooples </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/zadig+voltaire/ct/femme" class="category-label" data-v-0745f5da>Zadig &amp;Voltaire </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/a+p+c+" class="category-label" data-v-0745f5da>A.P.C </a>
																												 </li>
																										 </ul>
																										 <ul id="_javh1m1s6-sub-universe--14" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/marques/d/femme-accessoires/femme-chaussures/femme-lingerie/femme-maillots-de-bain/femme-sacs-et-maroquinerie/femme-vetements/" class="title-row-label" data-v-0745f5da>Toutes les marques </a>
																												 </li>
																										 </ul>
																										 <ul id="_javh1m1s6-sub-universe--15" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/h/femme" class="title-row-label" data-v-0745f5da>Tout l'Univers Femme </a>
																												 </li>
																										 </ul>
																								 </div>
																								 <div class="sidebar" data-v-0745f5da>
																										 <span class="sidebar__title" data-v-0745f5da>Sélections </span>
																										 <a href="https://www.galerieslafayette.com/evt/animations/prada-collection-ss21" class="sidebar__link" data-v-0745f5da>PRADA : Collection SS21 </a>
																										 <a href="https://www.galerieslafayette.com/c/soldes-coups+de+coeur+soldes/ct/femme" class="sidebar__link" data-v-0745f5da>Coups de cœur Soldes </a>
																										 <a href="https://www.galerieslafayette.com/c/happy+winter/ct/femme" class="sidebar__link" data-v-0745f5da>Voyage au grand froid </a>
																										 <a href="https://www.galerieslafayette.com/c/manteau+doudou" class="sidebar__link" data-v-0745f5da>Les manteaux doudoux </a>
																										 <a href="https://www.galerieslafayette.com/c/nouveautes/ct/femme" class="sidebar__link" data-v-0745f5da>Nouveautés </a>
																										 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance" class="sidebar__link" data-v-0745f5da>Le nouveau service : Shopping à distance </a>
																										 <a href="https://www.galerieslafayette.com/page-offre-galeries-lafayette" class="sidebar__link" data-v-0745f5da>Les offres du moment </a>
																										 <span data-href="https:||||www.galerieslafayette.com||c||soldes||ct||femme" class="sidebar__link" data-v-0745f5da>
																												 <img alt="" class="link-image" data-v-0745f5da>
																										 </span>
																								 </div>
																						 </div>
																				 </div>
																				 <div data-test-id="layer-homme" class="mega-menu" style="display:none;" data-v-0745f5da>
																						 <div class="mega-menu__content" data-v-0745f5da>
																								 <div class="sub-universes" data-v-0745f5da>
																										 <ul id="_j2fvfbjb7-sub-universe--0" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||h||createurs" class="title-row-label" data-v-0745f5da>Luxe &amp;Créateurs </span>
																												 </li>
																										 </ul>
																										 <ul id="_j2fvfbjb7-sub-universe--1" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||homme" class="title-row-label" data-v-0745f5da>Mode Responsable </span>
																												 </li>
																										 </ul>
																										 <ul id="_j2fvfbjb7-sub-universe--2" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||homme||vetements" class="title-row-label" data-v-0745f5da>Vêtements </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||vetements||manteaux" class="category-label" data-v-0745f5da>Manteaux </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||vetements||vestes" class="category-label" data-v-0745f5da>Vestes </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||vetements||hauts||pulls" class="category-label" data-v-0745f5da>Pulls </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||vetements||hauts||sweat-shirts" class="category-label" data-v-0745f5da>Sweat-shirts </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||vetements||hauts||chemises" class="category-label" data-v-0745f5da>Chemises </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||vetements||hauts||polos" class="category-label" data-v-0745f5da>Polos </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||vetements||hauts||tee-shirts" class="category-label" data-v-0745f5da>T-shirts </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||vetements||bas||jeans" class="category-label" data-v-0745f5da>Jeans </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||vetements||bas||pantalons" class="category-label" data-v-0745f5da>Pantalons </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||vetements||costumes" class="category-label" data-v-0745f5da>Costumes </span>
																												 </li>
																										 </ul>
																										 <ul id="_j2fvfbjb7-sub-universe--3" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||homme||sous-vetements" class="title-row-label" data-v-0745f5da>Sous-vêtements </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||sous-vetements||bas" class="category-label" data-v-0745f5da>Boxers &amp;Caleçons </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||sous-vetements||chaussettes" class="category-label" data-v-0745f5da>Chaussettes </span>
																												 </li>
																										 </ul>
																										 <ul id="_j2fvfbjb7-sub-universe--4" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||homme||homewear" class="title-row-label" data-v-0745f5da>Homewear </span>
																												 </li>
																										 </ul>
																										 <ul id="_j2fvfbjb7-sub-universe--5" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||sport||ct||homme" class="title-row-label" data-v-0745f5da>Sport </span>
																												 </li>
																										 </ul>
																										 <ul id="_j2fvfbjb7-sub-universe--6" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||homme||sacs-bagages" class="title-row-label" data-v-0745f5da>Sacs &amp;Bagages </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||sacs-bagages||sacs" class="category-label" data-v-0745f5da>Sacs </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||sacs-bagages||bagages" class="category-label" data-v-0745f5da>Bagages </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||sacs-bagages||petite-maroquinerie" class="category-label" data-v-0745f5da>Petite Maroquinerie </span>
																												 </li>
																										 </ul>
																										 <ul id="_j2fvfbjb7-sub-universe--7" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||homme||chaussures" class="title-row-label" data-v-0745f5da>Chaussures </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||chaussures||bottes-bottines" class="category-label" data-v-0745f5da>Bottes &amp;Bottines </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||chaussures||baskets" class="category-label" data-v-0745f5da>Baskets </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||chaussures||chaussures-de-ville||mocassins" class="category-label" data-v-0745f5da>Mocassins </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||chaussures||chaussures-de-ville||derbies-richelieus" class="category-label" data-v-0745f5da>Derbies &amp;Richelieus </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||chaussures||chaussures-d-ete" class="category-label" data-v-0745f5da>Mules &amp;Espadrilles </span>
																												 </li>
																										 </ul>
																										 <ul id="_j2fvfbjb7-sub-universe--8" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||homme||accessoires" class="title-row-label" data-v-0745f5da>Accessoires </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||accessoires||chapeaux-bonnets-casquettes" class="category-label" data-v-0745f5da>Bonnets &amp;Casquettes </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||accessoires||foulards-echarpes" class="category-label" data-v-0745f5da>Écharpes &amp;Foulards </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||accessoires||gants-mitaines-mouffles" class="category-label" data-v-0745f5da>Gants </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||accessoires||ceintures" class="category-label" data-v-0745f5da>Ceintures </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||accessoires||cravates-noeuds-papillon" class="category-label" data-v-0745f5da>Cravates &amp;Nœuds Papillons </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||accessoires||lunettes" class="category-label" data-v-0745f5da>Lunettes de soleil </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||homme||accessoires||bijoux" class="category-label" data-v-0745f5da>Bijoux </span>
																												 </li>
																										 </ul>
																										 <ul id="_j2fvfbjb7-sub-universe--9" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||homme||bain" class="title-row-label" data-v-0745f5da>Bain </span>
																												 </li>
																										 </ul>
																										 <ul id="_j2fvfbjb7-sub-universe--10" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||homme||accessoires||montres" class="title-row-label" data-v-0745f5da>Montres </span>
																												 </li>
																										 </ul>
																										 <ul id="_j2fvfbjb7-sub-universe--11" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||marques||d||homme-chaussures||homme-sacs-et-accessoires||homme-sous-vetements||homme-vetements||" class="title-row-label" data-v-0745f5da>Top Marques </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||a+p+c+||ct||homme" class="category-label" data-v-0745f5da>A.P.C. </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||boss||ct||homme" class="category-label" data-v-0745f5da>BOSS </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||calvin+klein||ct||homme" class="category-label" data-v-0745f5da>Calvin Klein </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||comptoir+gl" class="category-label" data-v-0745f5da>Comptoir GL </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||galeries+lafayette||ct||homme" class="category-label" data-v-0745f5da>Galeries Lafayette </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||ikks||ct||homme" class="category-label" data-v-0745f5da>IKKS </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||lacoste||ct||homme" class="category-label" data-v-0745f5da>Lacoste </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||levi+s||ct||homme" class="category-label" data-v-0745f5da>Levi's </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||polo+ralph+lauren||ct||homme" class="category-label" data-v-0745f5da>Polo Ralph Lauren </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||prada" class="category-label" data-v-0745f5da>Prada </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||sandro||ct||homme" class="category-label" data-v-0745f5da>Sandro </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||the+kooples||ct||homme" class="category-label" data-v-0745f5da>The Kooples </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||tommy+hilfiger||ct||homme" class="category-label" data-v-0745f5da>Tommy Hilfiger </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||zadig+voltaire||ct||homme" class="category-label" data-v-0745f5da>Zadig &amp;Voltaire </span>
																												 </li>
																										 </ul>
																										 <ul id="_j2fvfbjb7-sub-universe--12" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||marques||d||homme-chaussures||homme-sacs-et-accessoires||homme-sous-vetements||homme-vetements||" class="title-row-label" data-v-0745f5da>Toutes les marques </span>
																												 </li>
																										 </ul>
																										 <ul id="_j2fvfbjb7-sub-universe--13" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||h||homme" class="title-row-label" data-v-0745f5da>Tout l'univers homme </span>
																												 </li>
																										 </ul>
																								 </div>
																								 <div class="sidebar" data-v-0745f5da>
																										 <span class="sidebar__title" data-v-0745f5da>Sélections </span>
																										 <a href="https://www.galerieslafayette.com/c/nouveautes/ct/homme" class="sidebar__link" data-v-0745f5da>Nouveautés </a>
																										 <a href="https://www.galerieslafayette.com/c/saint+valentin-pour+lui+saint+valentin" class="sidebar__link" data-v-0745f5da>Saint-Valentin </a>
																										 <a href="https://www.galerieslafayette.com/c/happy+winter/ct/homme" class="sidebar__link" data-v-0745f5da>Vestiaire Grand Froid </a>
																										 <a href="https://www.galerieslafayette.com/page-offre-galeries-lafayette" class="sidebar__link" data-v-0745f5da>Les offres du moment </a>
																										 <a href="https://www.galerieslafayette.com/c/looks+comfy+en+soldes" class="sidebar__link" data-v-0745f5da>Look Comfy en Soldes </a>
																										 <a href="https://www.galerieslafayette.com/c/l+hiver+en+soldes" class="sidebar__link" data-v-0745f5da>Look d'hiver en Soldes </a>
																										 <a href="https://www.galerieslafayette.com/c/soldes-coups+de+coeur+soldes/ct/homme" class="sidebar__link" data-v-0745f5da>Coups de coeur Soldes </a>
																										 <a href="https://www.galerieslafayette.com/c/street+chic+en+soldes" class="sidebar__link" data-v-0745f5da>Street Chic en Soldes </a>
																										 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance" class="sidebar__link" data-v-0745f5da>Le nouveau service : Shopping à distance </a>
																										 <span data-href="||c||soldes||ct||homme" class="sidebar__link" data-v-0745f5da>
																												 <img alt="" class="link-image" data-v-0745f5da>
																										 </span>
																								 </div>
																						 </div>
																				 </div>
																				 <div data-test-id="layer-beaute" class="mega-menu" style="display:none;" data-v-0745f5da>
																						 <div class="mega-menu__content" data-v-0745f5da>
																								 <div class="sub-universes" data-v-0745f5da>
																										 <ul id="_tuk22xm10-sub-universe--0" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||beaute-parfum#ebp=SmMvxkqDL6S1a502afB.oriza4eZMe9i_9I0uXSDLTg3" class="title-row-label" data-v-0745f5da>Parfum </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-parfum||ct||beaute-parfum-parfum+femme-coffrets+femme#ebp=SmMvxkqDL6S1a502afB.oiZnizw.GK2mSzG7Zr6hH8j5" class="category-label" data-v-0745f5da>Coffrets Femme </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-parfum-parfum+femme#ebp=SmMvxkqDL6S1a502afB.opmNxAZ.SwRBmDpDOs2vImKV" class="category-label" data-v-0745f5da>Parfums Femme </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-parfum||ct||beaute-parfum-parfum+homme-coffrets+homme#ebp=SmMvxkqDL6S1a502afB.ooBBy3oMOfk1DKOgxvh4Tb2O" class="category-label" data-v-0745f5da>Coffrets Homme </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-parfum-parfum+homme#ebp=SmMvxkqDL6S1a502afB.opV0fXeX2e0IBDrMsgzemvR_" class="category-label" data-v-0745f5da>Parfums Homme </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||boutique+parfums+exception" class="category-label" data-v-0745f5da>Parfums d'exception </span>
																												 </li>
																										 </ul>
																										 <ul id="_tuk22xm10-sub-universe--1" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||beaute-maquillage#ebp=SmMvxkqDL6S1a502afB.olE4rdJOhxXjGOJrWx9sOHLp" class="title-row-label" data-v-0745f5da>Maquillage </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-maquillage-levres#ebp=SmMvxkqDL6S1a502afB.ohIuPTqm0hJYA1xbuLeuYNCb" class="category-label" data-v-0745f5da>Lèvres </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-maquillage-teint#ebp=SmMvxkqDL6S1a502afB.oqX0RZehnjSoHrorNin5Ghq8" class="category-label" data-v-0745f5da>Teint </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-maquillage-yeux#ebp=SmMvxkqDL6S1a502afB.ojA2NYmmayf66HRbxBBjiPmQ" class="category-label" data-v-0745f5da>Yeux </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-maquillage-yeux-sourcils#ebp=SmMvxkqDL6S1a502afB.oqiKOeJBfiasb67nYZMWEDK8" class="category-label" data-v-0745f5da>Sourcil </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-maquillage-ongles#ebp=SmMvxkqDL6S1a502afB.orR371idy.qvBm6iAfv2yq7I" class="category-label" data-v-0745f5da>Ongles </span>
																												 </li>
																										 </ul>
																										 <ul id="_tuk22xm10-sub-universe--2" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||beauty+lab" class="title-row-label" data-v-0745f5da>Green Beauty </span>
																												 </li>
																										 </ul>
																										 <ul id="_tuk22xm10-sub-universe--3" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||evt||animations||diagnostic-peau" class="title-row-label" data-v-0745f5da>Routine sur mesure </span>
																												 </li>
																										 </ul>
																										 <ul id="_tuk22xm10-sub-universe--4" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||beaute-soin#ebp=SmMvxkqDL6S1a502afB.okPVNWTIJRAMtBoLowJgdfz5" class="title-row-label" data-v-0745f5da>Soin </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-soin-coffrets+soin#ebp=SmMvxkqDL6S1a502afB.ooaHIm9VvfgfdD.1UGvYaTo1" class="category-label" data-v-0745f5da>Coffrets soin </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-soin-soin+visage-demaquillant+nettoyant#ebp=SmMvxkqDL6S1a502afB.ooax2bsR4rAow2LBMojsy5NL" class="category-label" data-v-0745f5da>Démaquillant &amp;Nettoyant </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-soin-soin+visage||f||hydratant+nourrissant#ebp=SmMvxkqDL6S1a502afB.ogrjqA2ngTNvHx9zqv2YSnIU" class="category-label" data-v-0745f5da>Hydratant visage </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-soin-soin+visage-anti-age+anti-rides#ebp=SmMvxkqDL6S1a502afB.oivyYfcFjMiSqVWHUyjAYjh7" class="category-label" data-v-0745f5da>Anti-âge &amp;Anti-rides </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-soin||f||bb+cc+cremes#ebp=SmMvxkqDL6S1a502afB.oj_e1eItDsE2DrEXqqi7pkRS" class="category-label" data-v-0745f5da>BB &amp;CC Crèmes </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-soin-soin+visage-exfoliant+masque#ebp=SmMvxkqDL6S1a502afB.otuxr_HuLgfnjfT9fJaqIn7y" class="category-label" data-v-0745f5da>Exfoliant &amp;Masque </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-soin-soin+visage-serums#ebp=SmMvxkqDL6S1a502afB.ohZWXCzL1Gvr6.LGF_RkrXk6" class="category-label" data-v-0745f5da>Sérums </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute||ct||beaute-soin-soin+corps#ebp=SmMvxkqDL6S1a502afB.ovxQMqjL.zl7hsY_Z.AUJwbw" class="category-label" data-v-0745f5da>Corps &amp;Bain </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-soin-soin+solaire" class="category-label" data-v-0745f5da>Solaires &amp;Autobronzants </span>
																												 </li>
																										 </ul>
																										 <ul id="_tuk22xm10-sub-universe--5" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||beaute-soin-soin+cheveux#ebp=SmMvxkqDL6S1a502afB.or6pRoPENyKsoA6UqTYU635h" class="title-row-label" data-v-0745f5da>Cheveux </span>
																												 </li>
																										 </ul>
																										 <ul id="_tuk22xm10-sub-universe--6" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||beaute+homme#ebp=SmMvxkqDL6S1a502afB.oskC_BJ3Y6ZNSfCYspz1t358" class="title-row-label" data-v-0745f5da>Homme </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute+homme||ct||beaute-soin||f||rasage#ebp=SmMvxkqDL6S1a502afB.oqX4TmfPCtDYQqtDk2Qtcdqe" class="category-label" data-v-0745f5da>Rasage </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-soin-soin+homme-hydratant+nourrissant" class="category-label" data-v-0745f5da>Hydratant &amp;Nourissant </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||beaute-soin-soin+homme-anti-age+anti-rides#ebp=SmMvxkqDL6S1a502afB.oqVMtl8l_WGEXl50NdLD7W3C" class="category-label" data-v-0745f5da>Anti-ride &amp;Anti-âge </span>
																												 </li>
																										 </ul>
																										 <ul id="_tuk22xm10-sub-universe--7" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||marques#ebp=SmMvxkqDL6S1a502afB.oifmfvOjs.munDkhbWv6pNbg" class="title-row-label" data-v-0745f5da>Top Marques </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||chanel#ebp=SmMvxkqDL6S1a502afB.ovR8da2tCGrCHZBkIkscXxKq" class="category-label" data-v-0745f5da>CHANEL </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||hermes" class="category-label" data-v-0745f5da>HERMÈS </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||dior#ebp=SmMvxkqDL6S1a502afB.olPYuqlpCD7SZXLcvZz_2T6k" class="category-label" data-v-0745f5da>DIOR </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||guerlain" class="category-label" data-v-0745f5da>GUERLAIN </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||the+ordinary#ebp=SmMvxkqDL6S1a502afB.osvSbDKw09RLn5HUWHbaUjn9" class="category-label" data-v-0745f5da>The Ordinary </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||nuxe#ebp=SmMvxkqDL6S1a502afB.oqoFlGkN3bKdQdR3h_rqtjc." class="category-label" data-v-0745f5da>Nuxe </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||yves+saint+laurent#ebp=SmMvxkqDL6S1a502afB.op9380BRAgkuhpgr3JyCdctA" class="category-label" data-v-0745f5da>Yves Saint Laurent </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||lancome#ebp=SmMvxkqDL6S1a502afB.ovrCHSnoioKte1BKJ5Xu2LN9" class="category-label" data-v-0745f5da>Lancôme </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||estee+lauder#ebp=SmMvxkqDL6S1a502afB.okDUEYOfVRpkor.15eUeuTm6" class="category-label" data-v-0745f5da>Estée Lauder </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||clarins#ebp=SmMvxkqDL6S1a502afB.otQyL1LB52v.IvjGPBx6cKxy" class="category-label" data-v-0745f5da>Clarins </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||sisley+paris#ebp=SmMvxkqDL6S1a502afB.orAbNyl20FB5akMFSO_UvWmf" class="category-label" data-v-0745f5da>Sisley Paris (Beauté) </span>
																												 </li>
																										 </ul>
																										 <ul id="_tuk22xm10-sub-universe--8" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||marques||d||beaute||#ebp=SmMvxkqDL6S1a502afB.orBoxFOl2uuo4KnKALFecfpL" class="title-row-label" data-v-0745f5da>Toutes les marques </span>
																												 </li>
																										 </ul>
																										 <ul id="_tuk22xm10-sub-universe--9" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||h||beaute" class="title-row-label" data-v-0745f5da>Tout l'univers beauté </span>
																												 </li>
																										 </ul>
																								 </div>
																								 <div class="sidebar" data-v-0745f5da>
																										 <a href="https://www.galerieslafayette.com/c/la+beaute" class="sidebar__title" data-v-0745f5da>Sélections </a>
																										 <a href="https://www.galerieslafayette.com/evt/animations/diagnostic-peau" class="sidebar__link" data-v-0745f5da>Routine sur mesure </a>
																										 <a href="https://www.galerieslafayette.com/c/beauty+galerie" class="sidebar__link" data-v-0745f5da>Sélection responsable </a>
																										 <a href="https://www.galerieslafayette.com/c/nouveautes/ct/beaute#ebp=SmMvxkqDL6S1a502afB.oo3mCWwNuPVR4Nh2dfQaSnXt" class="sidebar__link" data-v-0745f5da>Nouveautés </a>
																										 <a href="https://www.galerieslafayette.com/c/boutique+parfums+d+exception" class="sidebar__link" data-v-0745f5da>Les parfums d'exception </a>
																										 <a href="https://www.galerieslafayette.com/c/selection+cocooning/ct/beaute" class="sidebar__link" data-v-0745f5da>Sélection Cocooning </a>
																										 <a href="https://www.galerieslafayette.com/c/les+coffrets+beaute" class="sidebar__link" data-v-0745f5da>Les coffrets beauté </a>
																										 <span data-href="https:||||www.galerieslafayette.com||c||soldes||ct||beaute" class="sidebar__link" data-v-0745f5da>
																												 <img alt="" class="link-image" data-v-0745f5da>
																										 </span>
																								 </div>
																						 </div>
																				 </div>
																				 <div data-test-id="layer-enfant" class="mega-menu" style="display:none;" data-v-0745f5da>
																						 <div class="mega-menu__content" data-v-0745f5da>
																								 <div class="sub-universes" data-v-0745f5da>
																										 <ul id="_4evaheod7-sub-universe--0" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||enfant-bebe+0-2+ans+-puericulture||f||coffrets+cadeaux||carnets+de+sante||timbales+et+tirelires" class="title-row-label" data-v-0745f5da>Cadeaux de naissance </span>
																												 </li>
																										 </ul>
																										 <ul id="_4evaheod7-sub-universe--1" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||jeux+et+jouets" class="title-row-label" data-v-0745f5da>Jeux &amp;Jouets </span>
																												 </li>
																										 </ul>
																										 <ul id="_4evaheod7-sub-universe--2" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||go+for+good-enfant" class="title-row-label" data-v-0745f5da>Mode Responsable </span>
																												 </li>
																										 </ul>
																										 <ul id="_4evaheod7-sub-universe--3" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||enfant-bebe+0-2+ans+" class="title-row-label" data-v-0745f5da>Bébé de 0 à 2 ans </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-bebe+0-2+ans+-manteaux+vestes" class="category-label" data-v-0745f5da>Vestes &amp;Manteaux </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-bebe+0-2+ans+-pulls+gilets+sweats" class="category-label" data-v-0745f5da>Pulls &amp;Gilets </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-bebe+0-2+ans+-combinaisons+ensembles" class="category-label" data-v-0745f5da>Ensembles &amp;Combinaisons </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-bebe+0-2+ans+-polos+t-shirts+chemises" class="category-label" data-v-0745f5da>Polos, T-Shirts &amp;Chemises </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-bebe+0-2+ans+-pyjamas+sous-vetements" class="category-label" data-v-0745f5da>Pyjamas &amp;Gigoteuses </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-bebe+0-2+ans+-robes+jupes" class="category-label" data-v-0745f5da>Robes &amp;Jupes </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-bebe+0-2+ans+-jeans+pantalons" class="category-label" data-v-0745f5da>Pantalons &amp;Jeans </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||accessoires-accessoires+bebe" class="category-label" data-v-0745f5da>Accessoires </span>
																												 </li>
																										 </ul>
																										 <ul id="_4evaheod7-sub-universe--4" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||enfant-bebe+0-2+ans+-puericulture" class="title-row-label" data-v-0745f5da>Puériculture </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-bebe+0-2+ans+-puericulture||f||groupe+0+0+0+a+13kg+||portes-bebes+ventraux||autres+sieges+auto||groupe+2+3+15+a+36kg+||groupe+1+2+9+a+36kg+||autres+poussettes||autres+promenade+sortie||poussettes+4+roues||poussettes+multi-places||poussettes+combinees" class="category-label" data-v-0745f5da>Poussettes &amp;Transport </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-bebe+0-2+ans+-puericulture||f||trousses+de+toilettes||bain+de+bebe||matelas+a+langer+et+housse||produits+de+soins+pour+bebes||sacs+a+langer||sets+de+toilette||toilette+et+soins||carnets+de+sante" class="category-label" data-v-0745f5da>Toilette &amp;Soin </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-bebe+0-2+ans+-puericulture||f||ciels+de+lits+voilages||couvertures+edredons+et+couettes||tours+de+lits||couvres+lit||lits+pliants+papillons||couffins||gigoteuses+et+douillettes||veilleuses||barrieres+de+protection||luminaires+et+veilleuses" class="category-label" data-v-0745f5da>Chambre </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-bebe+0-2+ans+-puericulture||f||biberons+sans+bisphenol+a||assiettes+enfants||bavoirs||boites+a+dejeuner+et+gourdes||assiettes+et+bols||pots" class="category-label" data-v-0745f5da>Repas </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-bebe+0-2+ans+-puericulture||f||attaches+et+ranges+sucettes||moulages||anneaux+de+dentition||accroches+et+ranges+tetines" class="category-label" data-v-0745f5da>Eveil </span>
																												 </li>
																										 </ul>
																										 <ul id="_4evaheod7-sub-universe--5" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||enfant-fille+3-16+ans+" class="title-row-label" data-v-0745f5da>Fille de 3 à 16 ans </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-fille+3-16+ans+-manteaux+vestes" class="category-label" data-v-0745f5da>Vestes &amp;Manteaux </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-fille+3-16+ans+-pulls+gilets+sweats" class="category-label" data-v-0745f5da>Pulls &amp;Gilets </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-fille+3-16+ans+-tops+t-shirts+chemises" class="category-label" data-v-0745f5da>Blouses &amp;T-shirts </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-fille+3-16+ans+-pyjamas+sous-vetements" class="category-label" data-v-0745f5da>Pyjamas &amp;Sous-vêtements </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-fille+3-16+ans+-robes+jupes" class="category-label" data-v-0745f5da>Robes &amp;Jupes </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-fille+3-16+ans+-jeans+pantalons" class="category-label" data-v-0745f5da>Pantalons &amp;Jeans </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-fille+3-16+ans+-shorts+pantacourts" class="category-label" data-v-0745f5da>Shorts &amp;Bermudas </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-fille+3-16+ans+-maillots+de+bain" class="category-label" data-v-0745f5da>Maillots de bain </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||accessoires-accessoires+fille" class="category-label" data-v-0745f5da>Accessoires </span>
																												 </li>
																										 </ul>
																										 <ul id="_4evaheod7-sub-universe--6" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||enfant-garcon+3-16+ans+" class="title-row-label" data-v-0745f5da>Garçon de 3 à 16 ans </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-garcon+3-16+ans+-manteaux+vestes" class="category-label" data-v-0745f5da>Vestes &amp;Manteaux </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-garcon+3-16+ans+-pulls+gilets+sweats" class="category-label" data-v-0745f5da>Pulls &amp;Gilets </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-garcon+3-16+ans+-polos+t-shirts+chemises||f||chemises||chemisettes" class="category-label" data-v-0745f5da>Chemises </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-garcon+3-16+ans+-polos+t-shirts+chemises||f||polos||t-shirts||debardeurs" class="category-label" data-v-0745f5da>T-shirts &amp;Polos </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-garcon+3-16+ans+-pyjamas+sous-vetements" class="category-label" data-v-0745f5da>Pyjamas &amp;Sous-vêtements </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-garcon+3-16+ans+-jeans+pantalons" class="category-label" data-v-0745f5da>Pantalons &amp;Jeans </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-garcon+3-16+ans+-shorts+bermudas" class="category-label" data-v-0745f5da>Shorts &amp;Bermudas </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-garcon+3-16+ans+-maillots+de+bain" class="category-label" data-v-0745f5da>Maillots de bain </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||accessoires-accessoires+garcon" class="category-label" data-v-0745f5da>Accessoires </span>
																												 </li>
																										 </ul>
																										 <ul id="_4evaheod7-sub-universe--7" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||chaussures-enfant" class="title-row-label" data-v-0745f5da>Chaussures </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||enfant-bebe+0-2+ans+-chaussures" class="category-label" data-v-0745f5da>Chaussures Bébé </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||chaussures-enfant||ct||fille" class="category-label" data-v-0745f5da>Chaussures Fille </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||chaussures-enfant||ct||garcon" class="category-label" data-v-0745f5da>Chaussures Garçon </span>
																												 </li>
																										 </ul>
																										 <ul id="_4evaheod7-sub-universe--8" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||marques||d||enfant||enfant-jeux-et-jouets||enfant-vetements||" class="title-row-label" data-v-0745f5da>Top Marques </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||billieblush" class="category-label" data-v-0745f5da>Billieblush </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||boss||c||enfant" class="category-label" data-v-0745f5da>BOSS </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||evt||fr||shoppingadistance||burberry" class="category-label" data-v-0745f5da>Burberry </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||cadet+rousselle" class="category-label" data-v-0745f5da>Cadet Rousselle </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||cyrillus||c||enfant" class="category-label" data-v-0745f5da>Cyrillus </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||galeries+lafayette+and+kids" class="category-label" data-v-0745f5da>Galeries Lafayette </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||ikks||c||enfant" class="category-label" data-v-0745f5da>IKKS </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||karl+lagerfeld+kids" class="category-label" data-v-0745f5da>Karl Lagerfeld Kids </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||polo+ralph+lauren||c||enfant" class="category-label" data-v-0745f5da>Polo Ralph Lauren </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||timberland||c||enfant" class="category-label" data-v-0745f5da>Timberland </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||zadig+voltaire+kids" class="category-label" data-v-0745f5da>Zadig &amp;Voltaire Kids </span>
																												 </li>
																										 </ul>
																										 <ul id="_4evaheod7-sub-universe--9" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||marques||d||enfant||enfant-jeux-et-jouets||enfant-vetements||" class="title-row-label" data-v-0745f5da>Toutes les marques </span>
																												 </li>
																										 </ul>
																										 <ul id="_4evaheod7-sub-universe--10" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||h||enfant" class="title-row-label" data-v-0745f5da>Tout l'univers enfant </span>
																												 </li>
																										 </ul>
																								 </div>
																								 <div class="sidebar" data-v-0745f5da>
																										 <span class="sidebar__title" data-v-0745f5da>Sélections </span>
																										 <a href="https://www.galerieslafayette.com/c/nouveautes-nouveautes+enfant" class="sidebar__link" data-v-0745f5da>Nouveautés </a>
																										 <a href="https://www.galerieslafayette.com/c/pulls+et+mailles+enfant" class="sidebar__link" data-v-0745f5da>Maille d'hiver </a>
																										 <a href="https://www.galerieslafayette.com/c/selection+chaussures+enfant" class="sidebar__link" data-v-0745f5da>Chaussures d'hiver </a>
																										 <a href="https://www.galerieslafayette.com/c/accessoires+de+l+hiver" class="sidebar__link" data-v-0745f5da>Accessoires d'hiver </a>
																										 <a href="https://www.galerieslafayette.com/c/la+panoplie+de+l+hiver+enfant" class="sidebar__link" data-v-0745f5da>Vestiaire Grand Froid </a>
																										 <a href="https://www.galerieslafayette.com/page-offre-galeries-lafayette" class="sidebar__link" data-v-0745f5da>Les offres du moment </a>
																										 <a href="https://www.galerieslafayette.com/c/l+hiver+en+soldes+enfant" class="sidebar__link" data-v-0745f5da>Look d'hiver en Soldes </a>
																										 <a href="https://www.galerieslafayette.com/c/soldes-coups+de+coeur+soldes-coups+de+coeur+soldes+enfant" class="sidebar__link" data-v-0745f5da>Coups de coeur Soldes </a>
																										 <span data-href="||c||soldes-soldes+enfant" class="sidebar__link" data-v-0745f5da>
																												 <img alt="" class="link-image" data-v-0745f5da>
																										 </span>
																								 </div>
																						 </div>
																				 </div>
																				 <div data-test-id="layer-maison" class="mega-menu" style="display:none;" data-v-0745f5da>
																						 <div class="mega-menu__content" data-v-0745f5da>
																								 <div class="sub-universes" data-v-0745f5da>
																										 <ul id="_kvz59cq59-sub-universe--0" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||maison-linge+de+maison" class="title-row-label" data-v-0745f5da>Linge de maison </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-linge+de+maison-linge+de+lit" class="category-label" data-v-0745f5da>Linge de lit </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-linge+de+maison-linge+de+toilette" class="category-label" data-v-0745f5da>Linge de toilette </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-linge+de+maison-couettes+et+oreillers" class="category-label" data-v-0745f5da>Couettes et oreillers </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-linge+de+maison-linge+de+table" class="category-label" data-v-0745f5da>Linge de table </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-linge+de+maison-linge+de+cuisine" class="category-label" data-v-0745f5da>Linge de cuisine </span>
																												 </li>
																										 </ul>
																										 <ul id="_kvz59cq59-sub-universe--1" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||maison-meubles+et+canapes" class="title-row-label" data-v-0745f5da>Meubles &amp;canapés </span>
																												 </li>
																										 </ul>
																										 <ul id="_kvz59cq59-sub-universe--2" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||maison||ct||maison-luminaires" class="title-row-label" data-v-0745f5da>Luminaire </span>
																												 </li>
																										 </ul>
																										 <ul id="_kvz59cq59-sub-universe--3" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||maison-rangement" class="title-row-label" data-v-0745f5da>Rangement </span>
																												 </li>
																										 </ul>
																										 <ul id="_kvz59cq59-sub-universe--4" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||maison-decoration+de+la+maison" class="title-row-label" data-v-0745f5da>Décoration &amp;senteurs </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-decoration+de+la+maison||ct||maison-rideaux+et+quincaillerie+deco" class="category-label" data-v-0745f5da>Rideaux et voilages </span>
																												 </li>
																										 </ul>
																										 <ul id="_kvz59cq59-sub-universe--5" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||maison-cuisine+et+arts+de+la+table-oenologie" class="title-row-label" data-v-0745f5da>Oenologie </span>
																												 </li>
																										 </ul>
																										 <ul id="_kvz59cq59-sub-universe--6" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||maison-literie" class="title-row-label" data-v-0745f5da>Literie </span>
																												 </li>
																										 </ul>
																										 <ul id="_kvz59cq59-sub-universe--7" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||maison-maison+idees+cadeaux" class="title-row-label" data-v-0745f5da>Coffrets &amp;Idées cadeaux </span>
																												 </li>
																										 </ul>
																										 <ul id="_kvz59cq59-sub-universe--8" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||maison-cuisine+et+arts+de+la+table" class="title-row-label" data-v-0745f5da>Cuisine et arts de la table </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-cuisine+et+arts+de+la+table-culinaire" class="category-label" data-v-0745f5da>Culinaire </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-cuisine+et+arts+de+la+table-arts+de+la+table" class="category-label" data-v-0745f5da>Arts de la table </span>
																												 </li>
																										 </ul>
																										 <ul id="_kvz59cq59-sub-universe--9" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||maison-multimedia" class="title-row-label" data-v-0745f5da>Multimédia </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-multimedia-informatique" class="category-label" data-v-0745f5da>Informatique </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-multimedia-smartphone+et+gps" class="category-label" data-v-0745f5da>Smartphone et GPS </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-multimedia-tv+image+et+son" class="category-label" data-v-0745f5da>TV Image et Son </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-multimedia-jeu+video" class="category-label" data-v-0745f5da>Jeu vidéo </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-multimedia-objet+connecte" class="category-label" data-v-0745f5da>Objet connecté </span>
																												 </li>
																										 </ul>
																										 <ul id="_kvz59cq59-sub-universe--10" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||maison-electromenager-gros+electromenager" class="title-row-label" data-v-0745f5da>Gros électroménager </span>
																												 </li>
																										 </ul>
																										 <ul id="_kvz59cq59-sub-universe--11" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||maison-electromenager||ct||maison-petit+electromenager" class="title-row-label" data-v-0745f5da>Petit électroménager </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-electromenager||ct||maison-petit+electromenager-preparation+culinaire" class="category-label" data-v-0745f5da>Préparation culinaire </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-electromenager||ct||maison-petit+electromenager-petit+dejeuner" class="category-label" data-v-0745f5da>Petit déjeuner </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-electromenager||ct||maison-petit+electromenager-aspirateurs+vapeur" class="category-label" data-v-0745f5da>Aspirateur, vapeur </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-electromenager||ct||maison-petit+electromenager-soin+du+corps" class="category-label" data-v-0745f5da>Soin du corps </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-electromenager||ct||maison-petit+electromenager-soin+du+cheveu" class="category-label" data-v-0745f5da>Soin du cheveu </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||maison-electromenager||ct||maison-petit+electromenager" class="category-label" data-v-0745f5da>&gt;tous les produits </span>
																												 </li>
																										 </ul>
																										 <ul id="_kvz59cq59-sub-universe--12" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||maison" class="title-row-label" data-v-0745f5da>Maison responsable </span>
																												 </li>
																										 </ul>
																										 <ul id="_kvz59cq59-sub-universe--13" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||marques||d||maison-bricolage||maison-cuisine-et-arts-de-la-table||maison-electromenager||maison-high-tech||maison-librairie-papeterie-et-loisirs-creatifs||maison-linge-de-maison||maison-mobilier-et-decoration||" class="title-row-label" data-v-0745f5da>Top Marques </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||absolument+maison||ct||maison" class="category-label" data-v-0745f5da>Absolument maison </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||am+pm+||ct||maison" class="category-label" data-v-0745f5da>AMPM </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||apple" class="category-label" data-v-0745f5da>Apple </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||cristel" class="category-label" data-v-0745f5da>Cristel </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||degrenne" class="category-label" data-v-0745f5da>Degrenne </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||drouault" class="category-label" data-v-0745f5da>Drouault </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||boss||ct||maison" class="category-label" data-v-0745f5da>Hugo Boss Home </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||kenzo||ct||maison" class="category-label" data-v-0745f5da>Kenzo </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||lacoste||ct||maison" class="category-label" data-v-0745f5da>Lacoste </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||lagostina" class="category-label" data-v-0745f5da>Lagostina </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||la+redoute+interieurs||ct||maison" class="category-label" data-v-0745f5da>La Redoute Intérieurs </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||mauviel" class="category-label" data-v-0745f5da>Mauviel </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||ralph+lauren||ct||maison" class="category-label" data-v-0745f5da>Ralph Lauren </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||b||villeroy+boch" class="category-label" data-v-0745f5da>Villeroy &amp;Boch </span>
																												 </li>
																										 </ul>
																										 <ul id="_kvz59cq59-sub-universe--14" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||marques||d||maison-bricolage||maison-cuisine-et-arts-de-la-table||maison-electromenager||maison-high-tech||maison-librairie-papeterie-et-loisirs-creatifs||maison-linge-de-maison||maison-mobilier-et-decoration||" class="title-row-label" data-v-0745f5da>Les marques de A à Z </span>
																												 </li>
																										 </ul>
																										 <ul id="_kvz59cq59-sub-universe--15" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||maison||ct||maison" class="title-row-label" data-v-0745f5da>Tout l'univers maison </span>
																												 </li>
																										 </ul>
																								 </div>
																								 <div class="sidebar" data-v-0745f5da>
																										 <span class="sidebar__title" data-v-0745f5da>Sélections </span>
																										 <a href="https://www.galerieslafayette.com/c/soldes-coups+de+coeur+soldes/ct/maison" class="sidebar__link" data-v-0745f5da>Coups de coeur des soldes </a>
																										 <a href="https://www.galerieslafayette.com/c/offre+electromenager+multimedia/tri/meilleures-ventes" class="sidebar__link" data-v-0745f5da>Bons plans électro Multimédia </a>
																										 <a href="https://www.galerieslafayette.com/c/top+gamers" class="sidebar__link" data-v-0745f5da>Sélection Gaming </a>
																										 <a href="https://www.galerieslafayette.com/c/teletravail" class="sidebar__link" data-v-0745f5da>Sélection télétravail </a>
																										 <a href="https://www.galerieslafayette.com/c/maison/ct/maison/tri/nouveautes" class="sidebar__link" data-v-0745f5da>Nouveautés </a>
																										 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance" class="sidebar__link" data-v-0745f5da>Le nouveau service : Shopping à distance </a>
																										 <span data-href="||c||soldes||ct||maison" class="sidebar__link" data-v-0745f5da>
																												 <img alt="" class="link-image" data-v-0745f5da>
																										 </span>
																								 </div>
																						 </div>
																				 </div>
																				 <div data-test-id="layer-luxe-et-createurs" class="mega-menu" style="display:none;" data-v-0745f5da>
																						 <div class="mega-menu__content" data-v-0745f5da>
																								 <div class="sub-universes" data-v-0745f5da>
																										 <ul id="_1ushqia82-sub-universe--0" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/marques+luxe+et+createurs" class="title-row-label" data-v-0745f5da>Nouveaux créateurs </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/balmain" class="category-label" data-v-0745f5da>Balmain </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/bottega-veneta" class="category-label" data-v-0745f5da>Bottega Veneta </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/cartier" class="category-label" data-v-0745f5da>Cartier </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/celine" class="category-label" data-v-0745f5da>Celine </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/dolce-gabbana" class="category-label" data-v-0745f5da>Dolce Gabbana </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/etudes" class="category-label" data-v-0745f5da>Etudes </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/eres" class="category-label" data-v-0745f5da>Eres </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/fred" class="category-label" data-v-0745f5da>Fred </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/givenchy" class="category-label" data-v-0745f5da>Givenchy </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/homme+plisse" class="category-label" data-v-0745f5da>Homme Plissé </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/isabel-marant" class="category-label" data-v-0745f5da>Isabel Marant </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/jimmy-choo" class="category-label" data-v-0745f5da>Jimmy Choo </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/weston" class="category-label" data-v-0745f5da>J.M. Weston </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/kenzo" class="category-label" data-v-0745f5da>Kenzo </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/messika" class="category-label" data-v-0745f5da>Messika </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/roger-vivier" class="category-label" data-v-0745f5da>Roger Vivier </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex" class="category-label" data-v-0745f5da>Rolex </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/tods" class="category-label" data-v-0745f5da>Tod's </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/valentino" class="category-label" data-v-0745f5da>Valentino </a>
																												 </li>
																										 </ul>
																										 <ul id="_1ushqia82-sub-universe--1" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/marques+luxe+et+createurs" class="title-row-label" data-v-0745f5da>Créateurs incontournables </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/alexander-mcqueen" class="category-label" data-v-0745f5da>Alexander McQueen </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/a+p+c+" class="category-label" data-v-0745f5da>A.P.C </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/burberry" class="category-label" data-v-0745f5da>Burberry </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/by+far" class="category-label" data-v-0745f5da>By Far </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/casablanca" class="category-label" data-v-0745f5da>Casablanca </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/chaumet" class="category-label" data-v-0745f5da>Chaumet </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/danse+lente" class="category-label" data-v-0745f5da>Danse Lente </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/dior" class="category-label" data-v-0745f5da>Dior </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/etoile" class="category-label" data-v-0745f5da>Etoile </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/fendi" class="category-label" data-v-0745f5da>Fendi </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/heron+preston" class="category-label" data-v-0745f5da>Heron Preston </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/jacquemus" class="category-label" data-v-0745f5da>Jacquemus </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/lemaire" class="category-label" data-v-0745f5da>Lemaire </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/loewe" class="category-label" data-v-0745f5da>Loewe </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/miu-miu" class="category-label" data-v-0745f5da>Miu Miu </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/off+white" class="category-label" data-v-0745f5da>Off White </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/prada" class="category-label" data-v-0745f5da>Prada </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/saint-laurent" class="category-label" data-v-0745f5da>Saint Laurent </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/simone+rocha" class="category-label" data-v-0745f5da>Simone Rocha </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/staud" class="category-label" data-v-0745f5da>Staud </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/versace" class="category-label" data-v-0745f5da>Versace </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/vince" class="category-label" data-v-0745f5da>Vince </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/wandler" class="category-label" data-v-0745f5da>Wandler </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/b/y-3" class="category-label" data-v-0745f5da>Y-3 </a>
																												 </li>
																										 </ul>
																										 <ul id="_1ushqia82-sub-universe--2" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/marques+luxe+et+createurs" class="title-row-label" data-v-0745f5da>Top catégories </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/marques+luxe+et+createurs/ct/femme-vetements" class="category-label" data-v-0745f5da>Vêtements femme </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/marques+luxe+et+createurs/ct/homme-vetements" class="category-label" data-v-0745f5da>Vêtements homme </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/marques+luxe+et+createurs/ct/femme-sacs+et+bagages" class="category-label" data-v-0745f5da>Maroquinerie </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/chaussures+marques+luxe+et+createurs" class="category-label" data-v-0745f5da>Chaussures </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/sneakers+marques+luxe+et+createurs" class="category-label" data-v-0745f5da>Sneakers </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/marques+luxe+et+createurs/ct/femme-bijouterie+et+joaillerie" class="category-label" data-v-0745f5da>Bijoux </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/marques+luxe+et+createurs/ct/beaute" class="category-label" data-v-0745f5da>Beauté </a>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/c/marques+luxe+et+createurs" class="category-label" data-v-0745f5da>Tout voir </a>
																												 </li>
																										 </ul>
																										 <ul id="_1ushqia82-sub-universe--3" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/index-luxe-createurs" class="title-row-label" data-v-0745f5da>Toutes les marques </a>
																												 </li>
																										 </ul>
																										 <ul id="_1ushqia82-sub-universe--4" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <a href="https://www.galerieslafayette.com/h/createurs" class="title-row-label" data-v-0745f5da>Tout l'univers créateurs </a>
																												 </li>
																										 </ul>
																								 </div>
																								 <div class="sidebar" data-v-0745f5da>
																										 <span class="sidebar__title" data-v-0745f5da>Nouveau service </span>
																										 <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance" class="sidebar__link" data-v-0745f5da>Découvrir le shopping à distance </a>
																										 <span data-href="https:||||www.galerieslafayette.com||evt||animations||prada-collection-ss21" class="sidebar__link" data-v-0745f5da>
																												 <img alt="" class="link-image" data-v-0745f5da>
																										 </span>
																								 </div>
																						 </div>
																				 </div>
																				 <div data-test-id="layer-mode-responsable" class="mega-menu" style="display:none;" data-v-0745f5da>
																						 <div class="mega-menu__content" data-v-0745f5da>
																								 <div class="sub-universes" data-v-0745f5da>
																										 <ul id="_ybhq8n7jf-sub-universe--0" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||femme" class="title-row-label" data-v-0745f5da>Femme </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||femme-vetements" class="category-label" data-v-0745f5da>Vêtements </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||femme-sacs+et+bagages" class="category-label" data-v-0745f5da>Sacs et Bagages </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||femme-chaussures" class="category-label" data-v-0745f5da>Chaussures </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||femme-bijouterie+et+joaillerie" class="category-label" data-v-0745f5da>Bijouterie et Joaillerie </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||accessoires-accessoires+femme-montres||fgfg||autres+criteres||cuir+tannage+vegetal+ou+sans+chrome||fabrique+en+france" class="category-label" data-v-0745f5da>Montres </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||femme-lingerie" class="category-label" data-v-0745f5da>Lingerie </span>
																												 </li>
																										 </ul>
																										 <ul id="_ybhq8n7jf-sub-universe--1" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||maison" class="title-row-label" data-v-0745f5da>Maison </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||maison-arts+de+la+table" class="category-label" data-v-0745f5da>Arts de la table </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||maison-culinaire" class="category-label" data-v-0745f5da>Culinaire </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||maison-decoration+-+senteurs" class="category-label" data-v-0745f5da>Décoration </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||maison-linge+de+maison" class="category-label" data-v-0745f5da>Linge de Maison </span>
																												 </li>
																										 </ul>
																										 <ul id="_ybhq8n7jf-sub-universe--2" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||beaute" class="title-row-label" data-v-0745f5da>Beauté </span>
																												 </li>
																										 </ul>
																										 <ul id="_ybhq8n7jf-sub-universe--3" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||homme" class="title-row-label" data-v-0745f5da>Homme </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||homme-vetements" class="category-label" data-v-0745f5da>Vêtements </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||homme-sacs+et+bagages" class="category-label" data-v-0745f5da>Sacs et bagages </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||homme-chaussures" class="category-label" data-v-0745f5da>Chaussures </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||homme-sous-vetements+et+homewear" class="category-label" data-v-0745f5da>Sous-vêtements </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||accessoires-accessoires+homme-montres||fgfg||fabrique+en+france||autres+criteres" class="category-label" data-v-0745f5da>Montres </span>
																												 </li>
																										 </ul>
																										 <ul id="_ybhq8n7jf-sub-universe--4" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||go+for+good-enfant" class="title-row-label" data-v-0745f5da>Enfant </span>
																												 </li>
																										 </ul>
																										 <ul id="_ybhq8n7jf-sub-universe--5" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||go+for+good" class="title-row-label" data-v-0745f5da>Top Critères responsables </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||fgfg||coton+bio" class="category-label" data-v-0745f5da>Coton Bio </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||fgfg||cosmetique+naturelle" class="category-label" data-v-0745f5da>Cosmétique Naturelle </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||fgfg||fabrique+en+france" class="category-label" data-v-0745f5da>Made in France </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||fgfg||matiere+recyclee" class="category-label" data-v-0745f5da>Matière Recyclée </span>
																												 </li>
																										 </ul>
																										 <ul id="_ybhq8n7jf-sub-universe--6" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||c||go+for+good" class="title-row-label" data-v-0745f5da>Top Marques responsables </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||fm||agnes+b+" class="category-label" data-v-0745f5da>Agnes B. </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||fm||girlfriend+collective" class="category-label" data-v-0745f5da>Girlfriend collective </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||fm||maison+labiche" class="category-label" data-v-0745f5da>Maison labiche </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||fm||cuisse+de+grenouille" class="category-label" data-v-0745f5da>Cuisse de Grenouille </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||fm||galeries+lafayette" class="category-label" data-v-0745f5da>Galeries Lafayette </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||fm||labienhecha" class="category-label" data-v-0745f5da>Labienhecha </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||fm||sanoflore" class="category-label" data-v-0745f5da>Sanoflore </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||fm||axiology" class="category-label" data-v-0745f5da>Axiology </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||ct||maison||fm||absolument+maison" class="category-label" data-v-0745f5da>Absolument maison </span>
																												 </li>
																												 <li class="sub-universe__category" style="display:;" data-v-0745f5da>
																														 <span data-href="||c||go+for+good||fm||tommy+hilfiger" class="category-label" data-v-0745f5da>Tommy Hilfiger </span>
																												 </li>
																										 </ul>
																								 </div>
																								 <div class="sidebar" data-v-0745f5da>
																										 <a href="https://www.galerieslafayette.com/c/go+for+good#ebp=SmMvxkqDL6S1a502afB.oqOZKtX4NwKAbXJr0qQv5cJD" class="sidebar__title" data-v-0745f5da>Go For Good </a>
																										 <a href="https://www.galerieslafayette.com/evt/animations/changeons-de-mode" class="sidebar__link" data-v-0745f5da>Changeons de mode </a>
																										 <a href="https://www.galerieslafayette.com/evt/animations/go-for-good" class="sidebar__link" data-v-0745f5da>Découvrir nos engagements </a>
																										 <a href="https://renouveau.galerieslafayette.com/" class="sidebar__link" data-v-0745f5da>Renouveau : le site d'abonnement de sacs des Galeries Lafayette </a>
																										 <span data-href="||c||go+for+good" class="sidebar__link" data-v-0745f5da>
																												 <img alt="" class="link-image" data-v-0745f5da>
																										 </span>
																								 </div>
																						 </div>
																				 </div>
																				 <div data-test-id="layer-marques" class="mega-menu" style="display:none;" data-v-0745f5da>
																						 <div class="mega-menu__content" data-v-0745f5da>
																								 <div class="sub-universes" data-v-0745f5da></div>
																						 </div>
																				 </div>
																				 <div data-test-id="layer-nos-services" class="mega-menu" style="display:none;" data-v-0745f5da>
																						 <div class="mega-menu__content" data-v-0745f5da>
																								 <div class="sub-universes" data-v-0745f5da>
																										 <ul id="_enibgrwyz-sub-universe--0" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="https:||||www.galerieslafayette.com||evt||faq" class="title-row-label" data-v-0745f5da>Infos COVID </span>
																												 </li>
																										 </ul>
																										 <ul id="_enibgrwyz-sub-universe--1" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="https:||||galerieslafayette.epticahosting.com||selfgalerieslafayette||fr-fr||37||retour||44||comment-faire-le-retour-de-ma-commande-livree-en-france-metropolitaine" class="title-row-label" data-v-0745f5da>Retours prolongés </span>
																												 </li>
																										 </ul>
																										 <ul id="_enibgrwyz-sub-universe--2" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||evt||fr||shoppingadistance" class="title-row-label" data-v-0745f5da>Shopping à distance </span>
																												 </li>
																										 </ul>
																										 <ul id="_enibgrwyz-sub-universe--3" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="https:||||galerieslafayette.epticahosting.com||selfgalerieslafayette||fr-fr||13||livraison||67||comment-faire-livrer-ma-commande-en-magasin-click-collect" class="title-row-label" data-v-0745f5da>Click and collect </span>
																												 </li>
																										 </ul>
																										 <ul id="_enibgrwyz-sub-universe--4" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="http:||||www.galerieslafayette.com||i||nos-magasins||" class="title-row-label" data-v-0745f5da>Infos magasins </span>
																												 </li>
																										 </ul>
																										 <ul id="_enibgrwyz-sub-universe--5" data-cs-override-id="sub-universe" class="sub-universe" data-v-0745f5da>
																												 <li class="sub-universe__title-row" data-v-0745f5da>
																														 <span data-href="||evt||faq" class="title-row-label" data-v-0745f5da>Tous nos services </span>
																												 </li>
																										 </ul>
																								 </div>
																						 </div>
																				 </div>
																		 </div>
																 </div>
														 </div>
												 </div>
										 </header>
								 </div>
