<?php include ('../../pages-defaults/header.php'); ?>
<script>
  document.title = "RDV Mag";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
  
<!-- <link href="../../../media/LP/src/css/2020/monrendezvousmagasin.css" rel="stylesheet" type="text/css"> -->
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/monrendezvousmagasin.min.css" rel="stylesheet" type="text/css" />
<div class="lp-content">
  <section class="lp-body">
    <div class="container">
      
      <div class="luxe-row">
        <div class="luxe-col-mobile-12">
          <article>
            <div id="apwidget"></div>
            <script src="https://retailwidgets.appointedd.com/widget/5fbbe3f1d5a92a327b74293c/widget.js"></script>
            <script>appointeddWidget.render('apwidget');</script>
          </article>
        </div>
      </div>

    </div>
  </section>

</div>
<!--=========================== FIN LANDING PAGE ========================-->

  
<?php include ('../../pages-defaults/footer.php'); ?>
