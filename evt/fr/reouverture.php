<?php include ('../pages-defaults/header.php'); ?>
<script>
  document.title = "Reouverture";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../media/LP/src/css/2021/reouverture.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/reouverture.min.v02.css" rel="stylesheet" type="text/css" />
<div class="reouverture">
  <section class="hero">
    <div class="container">
      <div class="luxe-row no-gutter">
        <div class="luxe-col-mobile-12">
          <div class="content text-left">
            <h1 class="is-uppercase">Vous revoir, enfin&nbsp;!</h1>
            <p>Découvrez les coulisses de notre magasin iconique qui n’attend que vous.</p>
          </div>
        </div>
        <div class="luxe-col-mobile-12">
          <a href="#" class="image" data-reveal-id="popin-video" target="_self">
            <div class="date">19.05.2021</div>
          </a>
        </div>
      </div>
    </div>
  </section>
  
  
  <section class="body mag">
    <div class="container">

      <div class="luxe-row luxe-middle-mobile luxe-around-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-image">
            <a href="https://www.galerieslafayette.com/m/nos-magasins/">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/gl-mag.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/gl-mag@2x.jpg"
                  alt="Nos magasins près de chez vous - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/gl-mag.jpg" alt="Nos magasins près de chez vous - Galeries Lafayette" width="767" height="279" /></noscript>
              </figure>
            </a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <h2>Nos magasins près de chez&nbsp;vous</h2>
            <p>Toutes nos équipes sont mobilisées pour vous accueillir dans les meilleures conditions et nos magasins vous réservent encore bien des surprises.</p>
            <a class="button primary" href="https://www.galerieslafayette.com/m/nos-magasins/" style="margin-bottom:0;">Trouver votre magasin</a>
          </div>
        </div>
      </div>
      
    </div>
  </section>
  
  
  <section class="body promo">
    <div class="container">

      <div class="luxe-row luxe-middle-mobile luxe-reverse">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-image">
            <a href="https://www.galerieslafayette.com/c/3j">
              <figure class="image">
                <img class="b-lazy" 
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/promo-3j.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/promo-3j@2x.jpg"
                  alt="Nos magasins près de chez vous - Galeries Lafayette">
                  <!-- Fallback for non JavaScript browsers -->
                  <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/promo-3j.jpg" alt="Nos magasins près de chez vous - Galeries Lafayette" width="767" height="418" /></noscript>
              </figure>
            </a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="content">
            <h2>Nous sommes de retour.<br> Les 3J aussi&nbsp;!<br>
              Jusqu'à -50% sur une sélection d'articles</h2>
            <p>Du 19 au 30 mai 2021, en ligne et dans tous nos magasins sur les collections femme, homme, enfant, maison et beauté.</p>
            <a class="button primary" href="https://www.galerieslafayette.com/c/3j" style="margin-bottom:0;">Voir la sélection</a>
          </div>
        </div>
      </div>
      
    </div>
  </section>
    
    
    
  <section class="body services">
    <div class="container">

      <div class="luxe-row luxe-middle-mobile">
        <div class="luxe-col-mobile-12 luxe-col-tablet-5 luxe-col-widescreen-6">
          <div class="content">
            <h2>Nos services pour<br class="is-hidden-tablet" /> simplifier vos&nbsp;achats</h2>
          </div>
        </div>
        <div class="luxe-col-mobile-12">
          <div class="luxe-row slider-services slider-flickity">
            
            
            <div class="luxe-col-mobile-12 luxe-col-tablet-6 service">
              <div class="article-image">
                <a href="https://www.galerieslafayette.com/evt/footer/livraison-et-retour">
                  <span class="is-uppercase">Click&Collect</span>
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/click-and-collect.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/click-and-collect@2x.jpg"
                      alt="Click & Collect - Galeries Lafayette">
                      <!-- Fallback for non JavaScript browsers -->
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/click-and-collect.jpg" alt="Click & Collect - Galeries Lafayette" width="767" height="245" /></noscript>
                  </figure>
                </a>
              </div>
              <div class="content">
                <h3>La livraison gratuite en Click&Collect</h3>
                <p>Vous êtes livrés le lendemain (jour ouvré) dans votre magasin Galeries Lafayette en France métropolitaine pour toute commande passée avant 14h.</p>
                <a class="is-uppercase is-text" href="https://www.galerieslafayette.com/evt/footer/livraison-et-retour">En savoir plus</a>
              </div>
            </div>
            
            <div class="luxe-col-mobile-12 luxe-col-tablet-6 service">
              <div class="article-image">
                <a href="https://galerieslafayette.epticahosting.com/selfgalerieslafayette/fr-fr/32/e-reservation/116/quelles-sont-les-etapes-de-la-e-reservation">
                  <span class="is-uppercase">E-réservation</span>
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/e-reservation.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/e-reservation@2x.jpg"
                      alt="E-réservation - Galeries Lafayette">
                      <!-- Fallback for non JavaScript browsers -->
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/e-reservation.jpg" alt="E-réservation - Galeries Lafayette" width="767" height="245" /></noscript>
                  </figure>
                </a>
              </div>
              <div class="content">
                <h3>E-réservation : réservez, essayez, craquez</h3>
                <p>Où que vous soyez, repérez vos articles favoris en ligne et réservez-les. Rendez-vous en magasin pour les essayer et craquez à votre guise !</p>
                <a class="is-uppercase is-text" href="https://galerieslafayette.epticahosting.com/selfgalerieslafayette/fr-fr/32/e-reservation/116/quelles-sont-les-etapes-de-la-e-reservation">En savoir plus</a>
              </div>
            </div>
            
            <div class="luxe-col-mobile-12 luxe-col-tablet-6 service">
              <div class="article-image">
                <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance#marques">
                  <span class="is-uppercase">Shopping à Distance</span>
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/sad.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/sad@2x.jpg"
                      alt="Shopping à distance - Galeries Lafayette">
                      <!-- Fallback for non JavaScript browsers -->
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/sad.jpg" alt="Shopping à distance - Galeries Lafayette" width="767" height="245" /></noscript>
                  </figure>
                </a>
              </div>
              <div class="content">
                <h3>Les marques Luxe & Créateurs en <br class="is-hidden-mobile" />Shopping à Distance</h3>
                <p>Les conseillers de vos marques préférées sont à votre disposition pour vous faire découvrir les collections du magasin en live vidéo.</p>
                <a class="is-uppercase is-text" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance#marques">Contacter une marque</a>
              </div>
            </div>
            
            <div class="luxe-col-mobile-12 luxe-col-tablet-6 service">
              <div class="article-image">
                <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance#personalshopper">
                  <span class="is-uppercase">Personal Shopper</span>
                  <figure class="image">
                    <img class="b-lazy" 
                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                      data-src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/personal-shopper.jpg|https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/personal-shopper@2x.jpg"
                      alt="Personal Shopper - Galeries Lafayette">
                      <!-- Fallback for non JavaScript browsers -->
                      <noscript><img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/reouverture/personal-shopper.jpg" alt="Personal Shopper - Galeries Lafayette" width="767" height="245" /></noscript>
                  </figure>
                </a>
              </div>
              <div class="content">
                <h3>L’expertise de nos Personal Shoppers<br class="is-hidden-mobile" /> en vidéo</h3>
                <p>Un conseil mode offert, sur rendez-vous ou en instantané, avec un de nos personal shoppers en live vidéo.</p>
                <a class="is-uppercase is-text" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance#personalshopper">Contacter un Personal Shopper</a>
              </div>
            </div>
            
            
          </div>
        </div>
        
        <div class="luxe-col-mobile-12 text-center">
          <a class="button primary" href="https://www.galerieslafayette.com/services">Voir tous nos services</a>
        </div>
      </div>
      
    </div>
  </section>
  
</div>


<div class="reveal-modal opened video-modal" data-reveal id="popin-video" role="dialog">
  <div>
      <a class="close-reveal-modal" data-close aria-label="Close">Fermer</a>
      <div class="modal-content">
        <iframe class="is-hidden-tablet" id="video-m" src="https://player.vimeo.com/video/551464103" width="767" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
        <iframe class="is-hidden-mobile" id="video-d" src="https://player.vimeo.com/video/551458430" width="1118" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
      </div>
  </div>
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2021/reouverture.min.v02.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->
    
<!-- build:js /media/LP/src/js/2021/reouverture.min.v02.js
  <script src="../../assets/js/flickity.pkgd.min.js"></script>
  <script src="../../assets/js/blazy.min.js"></script>
  <script src="../src/js/2021/reouverture.js"></script>
<!-- endbuild -->
  
<?php include ('../pages-defaults/footer.php'); ?>
