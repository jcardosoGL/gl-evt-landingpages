<?php include ('../../pages-defaults/header.php'); ?>
<script>
  document.title = "Service";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../../media/LP/src/css/2020/shoppingadistance-service.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance-service.min.v02.css" rel="stylesheet" type="text/css" />
<style>
  html {
    scroll-behavior: smooth;
  }
</style>
<div class="luxe-augmente" data-bg-color="">
  <section class="luxe-augmente-body" style="border-top: none;">
    <div class="container">
      <!-- Fil d'Ariane -->
      <nav class="breadcrumb" aria-label="Fil d'Ariane">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com">
              <span itemprop="name">Accueil</span>
            </a>
            <meta itemprop="position" content="1">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance" aria-current="page">
              <span itemprop="name">Shopping à Distance</span>
            </a>
            <meta itemprop="position" content="2">
          </li>
          <li  class="is-active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" aria-current="page">
              <span itemprop="name">FAQ</span>
            </a>
            <meta itemprop="position" content="3">
          </li>
        </ul>
      </nav>
      
      <div class="luxe-row">
        <div class="luxe-col-mobile-12">
          <article class="faq">
            <h1>FAQ — Shopping à Distance</h1>
            <div class="accordions text-left">
              
              <div class="accordion">
                <div class="accordion-header">
                  <p>Qu’est ce que le Shopping à Distance&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Depuis 125 ans, les Galeries Lafayette Paris Haussmann accueillent le monde et font vibrer la mode. Aujourd’hui, le plus iconique des grands magasins parisiens sort de ses murs, à la rencontre de tous ses clients. Avec le service SHOPPING À DISTANCE, accédez aux plus belles marques Luxe & Créateurs du grand magasin, où que vous soyez.<br /> 
                      Pour ce faire, trois services s’offrent à vous :<br />
                      Le Live Shopping Vidéo immédiat avec la marque de votre choix ou l’un de nos Personal Shoppers<br />
                      Le Live Shopping Vidéo sur rendez-vous avec la marque de votre choix ou l’un de nos Personal Shoppers<br />
                      La Wishlist, un formulaire vous permettant d’envoyer en toute simplicité votre liste d’envies à notre équipe qui vous répondra par téléphone sous 48H. <br />
                      Ces services sont entièrement gratuits, et sans obligation d’achat.</p>
                      <p>Accédez à l’ensemble de ces services sur notre page <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance">Shopping à Distance</a></p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Comment fonctionne le Live Shopping Vidéo&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Le Live Shopping vous permet de faire vos achats aux Galeries Lafayette Haussmann depuis chez vous.
                      Vous pouvez échanger en live vidéo avec un Conseiller de Marque ou un Personal Shopper, et effectuer votre shopping parmi nos plus belles marques Luxe & Créateurs.<br  />
                      Comment ? En choisissant une marque disponible avec ce service <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance#marques">https://www.galerieslafayette.com/evt/fr/shoppingadistance#marques</a> ou bien en contactant un Personal Shopper directement <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance#personalshopper">https://www.galerieslafayette.com/evt/fr/shoppingadistance#personalshopper</a>. Cliquez sur “Acheter en Live Vidéo”, vous pourrez alors vous connecter directement en live, ou bien prendre rendez-vous pour plus tard.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Quel matériel faut-il pour réaliser un Live Shopping Vidéo&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Un ordinateur ou un smartphone avec une connexion internet suffisent, le live vidéo se fait directement sans avoir à télécharger d’application.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Y a-t-il un montant minimum d’achat pour bénéficier de ce service&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Aucun montant minimum d’achat, ce service est totalement gratuit.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Quel est le coût de ce service&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Ce service est totalement gratuit.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Comment fonctionne la prise de rendez-vous&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Le rendez-vous Live Shopping Vidéo vous permet de prévoir un créneau pour un échange personnalisé avec un Personal Shopper ou le Conseiller d’une Marque, permettant de vivre une expérience de shopping premium en étant confortablement installé chez vous.<br  />
                       En choisissant une marque disponible avec ce service <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance#marques">https://www.galerieslafayette.com/evt/fr/shoppingadistance#marques</a> ou bien en contactant un Personal Shopper directement <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance#personalshopper">https://www.galerieslafayette.com/evt/fr/shoppingadistance#personalshopper</a>. Vous pourrez choisir de prendre rendez-vous en cliquant sur “ Acheter en Live Vidéo”, puis “Prendre un rendez-vous Video”.<br  /> 
                      Vous pourrez choisir le créneau de votre choix et vous recevrez une confirmation par e-mail qui contiendra un bouton de connexion. Ce bouton vous permettra d’accéder à votre rendez-vous Live Shopping le moment venu.</p>
                      
                    <p>Vous pouvez également vous rendre dans votre magasin Galeries Lafayette habituel, et découvrir ce service avec un conseiller de vente qui vous accompagnera dans la démarche de prise de rendez-vous.</p>
                      
                    <p>Vous recevrez un email de rappel 2 heures avant votre rendez-vous.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Puis-je annuler ou modifier mon rendez-vous&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Vous pouvez modifier ou annuler votre rendez-vous à tout moment, à partir de votre e-mail de confirmation de rendez-vous.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>J’ai eu un problème technique lors de mon Live Shopping Vidéo, que faire&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>En cas de déconnexion imprévue durant un Live Vidéo, votre interlocuteur fera le maximum pour vous recontacter en se reconnectant immédiatement au Live Vidéo, ou en vous contactant par téléphone si vous avez communiqué votre numéro.</p>
                    <p>En cas d’échec, n’hésitez pas à prendre un nouveau rendez-vous ou remplir votre Wishlist.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Quels sont les horaires du Live Shopping Vidéo&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>La prise de rendez-vous est possible à tout moment sur galerieslafayette.com, pour des RDV entre 11h et 19h du lundi au samedi, et dimanche et jours fériés de 12h à 19h.</p>
                    <p>Les conseillers sont également disponibles pour les appels live de 11h à 16h30 du lundi au samedi.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Qu’est ce que la Wishlist&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Vous savez exactement ce que vous voulez ? Qu’il s’agisse d’un grand classique de votre marque de Luxe favorite, ou d’un produit phare du dernier Créateur en vogue, n’hésitez pas à compléter votre wishlist : un formulaire simple et rapide qui sera envoyé directement à nos équipes aux Galeries Lafayette Haussmann.<br />
                      Nos équipes vous recontacterons en 48H par téléphone pour vous confirmer la disponibilité du ou des produits demandés, vous pourrez alors effectuer un paiement à distance et choisir votre mode de livraison.
                      </p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Qu’est-ce qu’un Personal Shopper&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Notre équipe de Personal Shoppers regroupe des experts, passionnés de mode, qui vous aident à constituer la garde-robe qui vous ressemble en vous apportant un conseil en style totalement adapté à vos besoins. Ils sauront trouver la pièce idéale, pour vous ou pour offrir.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Quelles sont les marques disponibles&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Vous trouverez l’ensemble des marques disponibles en Live Shopping Vidéo en cliquant sur ce lien : <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance#marques">https://www.galerieslafayette.com/evt/fr/shoppingadistance#marques</a><br />
                      La marque Luxe ou Créateurs que vous désirez n'apparaît pas dans cette liste ? N’hésitez pas à remplir une wishlist <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance#wishlist">https://www.galerieslafayette.com/evt/fr/shoppingadistance#wishlist</a> et nos équipes vous contacteront pour vous proposer une solution.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Comment fonctionne le paiement&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>— Une fois la vente validée, vous recevrez un devis avec les produits demandés et les frais de livraison, ainsi qu'un lien de paiement. En cliquant sur ce lien, vous arriverez sur une plateforme de paiement sécurisée.<br /> 
                      — Ce lien est valable durant 48H, durée pendant laquelle nos équipes garantissent la disponibilité des produits que vous avez sélectionnés.
                      — Une fois le paiement réalisé, vous recevrez une confirmation de commande.</p>
                      <p class="is-bold">Moyens de paiement :</p>
                      <ul>
                        <li><p>Paiement par carte bancaire via un lien de paiement sécurisé : cartes du réseau CB, Visa, Eurocard/Mastercard/American Express</p></li>
                        <li><p>Paiement par virement bancaire grâce au RIB adressé par le service Shopping à Distance par e-mail</p></li>
                        <li><p>Paiement par Alipay ou Wechat pay</p></li>
                        <li><p>Paiement par carte cadeau Galeries Lafayette ou points fidélité</p></li>
                        <li><p>Le paiement en plusieurs fois sans frais n’est pas possible pour le moment, mais nous faisons notre maximum pour le développer rapidement !</p></li>
                      </ul>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Peut-on cumuler des points fidélité&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Absolument, grâce à votre carte de fidélité, vous cumulez des points lors de chaque achat, y compris ceux réalisés en Shopping à Distance.</p>
                    <p>Il suffit de donner votre numéro de carte de fidélité à votre Personal Shopper, au Conseiller de Marque ou dans votre demande en Wishlist.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Puis-je modifier ou annuler ma commande&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Vous pouvez modifier la sélection de Produits en indiquant les quantités et/ou Produit(s) à modifier directement auprès service client Shopping à Distance, par retour d’email suite à votre devis ou paiement.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Quelles sont les modalités de livraison et retour du Shopping à Distance&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Les frais de livraisons à domicile en France et Union Européenne sont de 15 € pour la France et 30€ pour l’Union Européenne.</p>
                      
                    <p class="is-bold">Les frais de livraisons sont offerts :</p>
                    <ul>
                      <li><p>aux clients porteurs de carte de fidélité des Galeries Lafayette au statut  Exception  & Premium</p></li>
                      <li><p>à tous clients au-delà d’un seuil d’achat de 600€</p></li>
                      <li><p>pour toute livraison réalisée en Click & Collect dans un magasin Galeries Lafayette</p></li>
                    </ul>
                    <p>Les livraisons à l'international hors Union Européenne et les livraisons de produits d’horlogerie, bijouterie, joaillerie sont payantes pour tous les clients, veuillez contacter notre service Shopping à Distance pour en savoir plus.</p>
                    
                    <p class="is-bold">Modes de livraison :</p>
                    <p>Notre transporteur de confiance Chronopost se chargera de la livraison de vos colis Shopping à Distance à domicile, au créneau de votre choix y compris le soir et le weekend, dès le lendemain de la confirmation de votre commande.</p>
                    <p>Vous pouvez également récupérer votre colis en Click & Collect dans le magasin Galeries Lafayette de votre choix.</p>
                    <p>Dans les deux cas, vous recevrez un email confirmant l’envoi de votre commande, avec toutes les modalités vous permettant de suivre votre colis.</p>
                    <p class="is-bold">Retours :</p>
                    <p>Pour organiser la reprise d'un article, veuillez contacter le service client par e-mail à l’adresse : <a href="mailto:shoppingadistance@galerieslafayette.com">shoppingadistance@galerieslafayette.com</a> afin de déclarer votre intention de retour produit en indiquant votre n° de commande.</p> 
                    <p>Le service client vous communiquera la marche à suivre pour retourner votre produit en toute simplicité.</p>
                    
                    <p>Les frais de retour sont offerts depuis la France et l’Union Européenne.<br />
                    Les délais de retour sont de 30 jours.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Comment se passe le Live Shopping Vidéo&nbsp;? Peut-on me voir pendant le live&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Lors d’un appel en live vidéo, sur rendez-vous ou en direct, vous pourrez voir la caméra de votre conseiller de marque ou personal shopper. De cette manière, il vous présentera les produits susceptibles de vous intéresser.<br />
                      Vous aurez également la possibilité d’activer ou non votre vidéo avant de démarrer l'appel, et ensuite de modifier ce choix tout au long de l'échange pour permettre à votre conseiller de vous voir.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Les Lives Shopping Vidéos sont-ils enregistrés&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Non, nous ne gardons aucune trace de vos échanges, qui sont confidentiels et resterons entre votre conseiller de vente ou personal shopper et vous.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Que se passe-t-il avec les données partagées dans le cadre du Shopping à Distance&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Si vous souhaitez acheter un article en Shopping à Distance ou prendre rendez-vous, vos données personnelles sont collectées pour permettre l’accès au service.</p>
                      
                    <p>Dans le cadre d’une connexion au Live Shopping Vidéo en direct, aucune données personnelles ne sont nécessaires.</p>
                      
                    <p>Pour en savoir plus : <a href="https://www.galerieslafayette.com/service/service-confidence">https://www.galerieslafayette.com/service/service-confidence</a></p>
                  </div>
                </div>
              </div>
              
            </div>
            
            <div class="text-left">
              <p>Pour toute autre question, ou si vous rencontrez le moindre problème, vous pouvez contacter notre service client&nbsp;:</p>
                <ul>
                  <li><p>- En remplissant notre <a href="https://www.galerieslafayette.com/my-account/app/contact">formulaire de contact</a></p></li>
                  <li><p>- Ou en nous écrivant à l’adresse e-mail : <a href="mailto:shoppingadistance@galerieslafayette.com">shoppingadistance@galerieslafayette.com</a></p></li>
                </ul>
            </div>

          </article>
        </div>
      </div>

    </div>
  </section>
  
  <!-- CGUtilisation -->
  <section class="luxe-augmente-body" id="conditions">
    <div class="container">
      <div class="luxe-row">
        <div class="luxe-col-mobile-12">
          <article class="cgu">
            <h2>Condition Générale d’Utilisation — Shopping à Distance</h2>
            <div class="text-left">
              <p class="is-bold">1 - Préambule</p>
              <p>Soucieuse de toujours mieux répondre aux attentes de ses clients, la société 44 GALERIES LAFAYETTE – 44 GL (ci-après, les « Galeries Lafayette »), Société par Actions Simplifiée, au capital de 147 184 338 euros, dont le siège social est sis 44-48, rue de Châteaudun – 75 009 PARIS, identifiée sous le numéro 552 116 329 RCS PARIS, propose d’accéder à une grande partie de l’offre présentée par le magasin Galeries Lafayette Haussmann grâce à un service de vente à distance (ci-après, le « <span class="is-bold">Service</span> »).</p>
              <p>Les présentes conditions générales d’utilisation (ci-après les « <span class="is-bold">CGU</span> ») ont pour objet de définir les droits et obligations des Galeries Lafayette et de leurs clients dans le cadre du fonctionnement du Service.</p>
              
              <br />
              <p class="is-bold">2 - Champ d’application</p>
              <p>Les présentes CGU définissent les modalités et les conditions dans lesquelles le client accède et bénéficie au Service.</p> 
              <p>Toute utilisation du Service par le client implique l’acceptation pleine et sans réserve des CGU. Le client  reconnaît en avoir parfaite connaissance et renonce de ce fait à se prévaloir de tout document contradictoire.</p>
              <p>La relation entre le client et les Galeries Lafayette sera toujours régie par les dernières conditions en vigueur au jour de l’utilisation par le client du Service. En cas de modification des CGU, le client sera invité à accepter les nouvelles conditions, qui lui seront applicables au jour de l’acceptation. Le client est invité à consulter régulièrement les CGU afin de prendre connaissance des évolutions éventuelles. L’utilisation du Service vaut acceptation des CGU dans leur dernière version au jour de l’utilisation du Service.</p>
               
              <br />
              <p class="is-bold">3 - Service</p>
              <p>Le Service est accessible par l’intermédiaire de trois (3) canaux différents, tels que définis ci-après :</p>
              <p>Le client pourra avoir accès au service directement sur la page <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance">https://www.galerieslafayette.com/evt/fr/shoppingadistance</a>, il aura alors faculté de :</p>
              <ul>
                <li><p>Contacter directement le conseiller de vente d’une marque en Live Shopping Vidéo, pour avoir des informations sur un produit ou une marque, et ce du lundi au samedi de 11h à 16h30.</p></li>  
                <li><p>Prendre rendez-vous à une date/heure ultérieure avec le conseiller de vente d’une marque ou un Personal Shopper afin de programmer cet appel Live Shopping Vidéo.</p></li>
                <li><p>Remplir une Wishlist (liste d’envie) lui permettant de demander la disponibilité d’un ou plusieurs produits. Il sera contacté dès que possible par téléphone pour répondre à cette demande.</p></li>
              </ul>
              <p>Les dates et horaires indiqués sont susceptibles d’être modifiés en raison du contexte lié à la Covid-19.</p> 
              
              <br />
              <p class="is-bold">4 - Obligations liées à l’utilisation du Service</p>
              <p>Les clients s’engagent dans le cadre de l'utilisation du Service&nbsp;à&nbsp;:</p>
              <ul>
                <li><p>déclarer des informations complètes et exactes lors de son inscription, et à les mettre à jour en cas de changement ;</p></li>
                <li><p>ne pas utiliser le Service à des fins frauduleuses ou non prévues par les CGU ;</p></li>
                <li><p>ne pas usurper d’identité, ni tenter d’utiliser un compte autre que le sien ;</p></li>
                <li><p>à ne pas utiliser des pseudonymes ou utiliser des expressions propres à être reçues comme des agressions verbales par les Galeries Lafayette et son personnel de vente et/ou démonstrateurs ;</p></li> 
                <li><p>à ne pas avoir un comportement déplacé, ou qui serait perçu comme tel par les Galeries Lafayette, et son personnel de vente et/ou démonstrateurs .</p></li>
              </ul>
              <p>En tout état de cause, le client s’interdit de capturer et/ou enregistrer et/ou diffuser toute capture des écrans conversationnels avec le personnel de vente et/ou les démonstrateurs sur un quelconque moyen de communication électronique ou téléphonique.</p>
              
              <p>Si un comportement tel que mentionné précédemment est caractérisé, les Galeries Lafayette se réservent le droit de mettre un terme discrétionnairement à la réalisation du Service à tout moment, sans que sa responsabilité ne puisse être recherchée d’une quelconque façon.</p> 
              
              <br />
              <p class="is-bold">5 - Politique vie privée</p>
              <p>Les données personnelles collectées (ci-après, les «&nbsp;Données&nbsp;»), dans le cadre de l’accès au Service sont obligatoires et nécessaires pour permettre le fonctionnement du Service.</p> 
              <p>Galeries Lafayette est responsable du traitement des Données.</p> 
              <p>Galeries Lafayette garantit qu’aucune des images vidéos des clients collectées pour l’utilisation du Service ne seront conservées.</p> 
              <p>Pour tout connaître de la Politique de Vie privée des Galeries Lafayette, vous pouvez suivre ce lien :  <a href="https://www.galerieslafayette.com/service/service-confidence">https://www.galerieslafayette.com/service/service-confidence</a> ou demander à consulter la Politique Vie Privée en magasin.</p>
              <p>Galeries Lafayette garantit que l’accès au Service est sécurisé, notamment que l’accès aux images live est protégé et que ne peuvent y avoir accès uniquement les clients et le personnel de Galeries Lafayette ou des marques.</p> 
              
              <br />
              <p class="is-bold">6 - Responsabilité</p>
              <p>La responsabilité des Galeries Lafayette ne saurait être engagée si l’inexécution ou la mauvaise exécution de ses obligations est imputable au client, ou à un cas de force majeure conformément à l’article 1218 du Code civil.</p>
              <p>Dans l’hypothèse où des raisons impératives (notamment contexte d’incertitude lié à l’épidémie de Coronavirus et ses conséquences) et/ou un cas fortuit se présenteraient ou dans le cas d’une décision gouvernementale ou administrative et/ou une décision d’entreprise impérative, et notamment l’impossibilité d’ouvrir le magasin, empêchant les Galeries Lafayette ou le Client d’exécuter ses obligations, ces deux derniers devront s’en informer sans délai.</p> 
              <p>Les Galeries Lafayette ne sont pas responsables des dommages de toutes natures pouvant être subis par le Client à l'occasion d'une visite des sites internet et/ou usage des services, applications ou d’outils numériques.</p> 
              <p>L'utilisation de la page du Service et/ou d’outils numériques implique la connaissance et l'acceptation par le Client des caractéristiques ainsi que des limites de l'Internet et des technologies qui y sont liées, l'absence de protection de certaines données contre des détournements éventuels ou piratage et risques de contamination par d'éventuels virus circulants sur le réseau. Les Galeries Lafayette déclinent toute responsabilité en cas de mauvaise utilisation ou d'incident lié à l'utilisation de l'ordinateur, de l'accès à Internet, de la maintenance ou du dysfonctionnement des serveurs, de la ligne téléphonique ou de toute autre connexion technique, et de l'envoi des formulaires à une adresse erronée ou incomplète, d'erreurs informatiques quelconques ou défauts constatés sur la page du Service.</p>
              <p>L’utilisation du Service est susceptible d’entraîner la consommation d’une partie du forfait de transmission de données souscrit auprès de son opérateur mobile et/ou internet par le client. Toute utilisation du Service de depuis l’étranger est susceptible d’entraîner des coûts considérablement plus élevés que ceux d’une utilisation depuis la France. L’obtention des informations correspondantes et le paiement d’éventuels frais, notamment d’itinérance, facturés par l’opérateur du client, lui incombent entièrement.</p>
              
              <br />
              <p class="is-bold">7 - Achat via le Service</p>
              <p>Tout achat par le biais du Service implique une acceptation pleine et entière de la politique de vie privée Galerie Lafayette et des Conditions Générales de Vente du Service.</p> 
            </div>
          </article>
        </div>
      </div>
    </div>
  </section>
  
  <!-- CGVente -->
    <section class="luxe-augmente-body marques" id="vente">
      <div class="container">
        <div class="luxe-row">
          <div class="luxe-col-mobile-12">
            <article class="cgv">
              <h2>Conditions générales de vente - Galeries Lafayette Haussmann — Shopping à Distance</h2>
              <p class="is-uppercase is-bold">Mise à jour- OCTOBRE 2020</p>
              <div class="text-left">
                <p class="is-bold">1 - Identification des parties</p>
                <p class="is-bold">Le vendeur :</p> 
                
                <p>En l’absence de précision contraire, l’auteur de l’offre est le vendeur des produits et services proposés en magasin est :</p> 
                
                <p>La Société Galeries Lafayette Haussmann - GL HAUSSMANN Société par actions simplifiée au capital de 217.404.572 euros, dont le siège social est à Paris 9ème 27, rue de la Chaussée d’Antin, inscrite au Registre de Commerce et des Sociétés de Paris sous le numéro 572 062 594</p>
                
                <p><span class="is-bold">Galeries Lafayette Haussmann – Shopping à Distance<br />
                TSA 44100<br />
                75446  PARIS CEDEX 09<br />
                FRANCE</span><br />
                Tél : 01 42 82 38 08</p>
                
                <p>Les Galeries Lafayette proposent à la vente des produits et services (ci-après “Produits”) destinés aux personnes physiques faisant l’acquisition d’un ou plusieurs Produit(s) pour leurs besoins personnels et non destinés à la revente. Les Galeries Lafayette se réservent la possibilité de refuser la vente en cas de suspicion d’achats n’entrant pas dans ce cadre.</p> 
                
                <p class="is-bold">Le Client :</p> 
                
                <p>Seules les personnes physiques juridiquement capables de souscrire des contrats concernant les Produits proposés à la vente peuvent passer commande.<br />
                Lors de la passation de la commande, le Client garantit avoir la pleine capacité juridique pour adhérer aux présentes conditions générales de vente et ainsi conclure la vente.</p>
                
                <p>Certains Produits proposés à la vente peuvent être réservés aux personnes majeures, le Client s'engage, lors de la commande de ce type de Produits, à avoir dix-huit (18) ans révolus à la date de la commande. Les Galeries Lafayette se réservent la possibilité de demander lors de la livraison un justificatif d’identité.</p>
                
                <p class="is-bold">2 - Objet</p>
                
                <p>Les présentes conditions générales de vente ont pour objet de définir les droits et obligations des Galeries Lafayette Haussmann et de leurs Clients dans le cadre de la vente à distance, de produits proposés en magasin. Sauf dispositions contraires précisées au Client, les Produits éligibles sont les Produits vendus au sein du magasin Galeries Lafayette Haussmann. Le Client est informé que certains Produits ou certaines marques peuvent ne pas être éligibles à un achat à distance.</p> 
                
                <p>Tout achat implique l’acceptation pure et simple des présentes conditions générales de vente, qui doivent être lues par le Client qui reconnaît les accepter sans réserve avant toute validation de commande par email.<br />
                Les présentes conditions générales de vente seront adressées sur un support informatique durable au Client. </p>
                
                <p class="is-bold">3 - Prix des Produits</p>
                
                <p>Le prix du ou des Produit(s) est indiqué en Euros toutes taxes comprises, et confirmé lors de la validation de la commande. Le prix des Produits présentés peut varier en fonction des offres promotionnelles en cours et peut être différent des prix pratiqués dans les autres magasins Galeries Lafayette et/ou site e-commerce. Le magasin n’est pas non plus tenu d’appliquer les prix constatés dans d’autres magasins et sur Internet.</p>
                
                <p>Sauf indication contraire, les promotions et avantages ne sont pas cumulables entre eux. Certaines promotions peuvent être réservées à une partie de la clientèle, les conditions d’accès à ces offres sont disponibles en magasin.</p> 
                
                <p>Des frais de livraison en sus pourront être ajoutés en fonction du type et du montant de la commande.Les frais de livraison sont communiqués, au plus tard avant la passation de la confirmation de la commande.</p> 
                
                <p>Le paiement s’effectue au comptant selon les moyens de paiement et devises acceptés et signalés.</p> 
                
                <p>La présentation de justificatifs d’identité officiels et en cours de validité pourra être requise, le cas échéant, lors du paiement.</p>
                
                
                <p class="is-bold">4 - Commande de produit</p> 
                
                <p>Le présent article détaille les modalités de commande du ou des Produit(s).</p> 
                
                <p class="is-bold">Parcours de commande :</p> 
                
               <p>Toute commande pourra être passée par le Client via le service de commande à distance “Shopping à Distance” mis à sa disposition.</p> 
                
                <p>Le Client accepte de fournir les informations qui lui sont demandées et s'engage sur la véracité de ces dernières :</p>
                <ul>
                  <li><p>nom et prénom : information nécessaire pour identifier le propriétaire de la commande</p></li>
                  <li><p>adresse postale : information nécessaire pour la livraison à domicile</p></li>
                  <li><p>téléphone : information nécessaire pour le suivi de la livraison</p></li>
                  <li><p>adresse électronique : information nécessaire pour l’envoi des emails confirmation de commande et suivi 	de livraison</p></li></ul>
                
                <p>Le Client, après avoir opéré sa sélection de Produit(s) auprès du personnel de vente du magasin Galeries Lafayette, reçoit un email de confirmation de commande récapitulant l’ensemble de sa sélection, le prix du ou des Produit(s) et le détail des frais d’expédition.</p>  
                <p>Le Client peut, soit :</p> 
                <ul>
                  <li><p>Confirmer sa commande en procédant au paiement.</p></li>
                  <li><p>Modifier la sélection de Produits  en indiquant les quantités et/ou Produit(s) à modifier directement auprès de la marque ou du service Client des Galeries Lafayette.  Dans ce cas, il recevra un nouvel email de confirmation de commande.</p></li> 
                </ul>
                <p>Dès lors que la commande du Client est confirmée, le Client reçoit un email permettant de procéder au paiement de la commande.</p> 
                
                <p class="is-bold">Paiement :</p> 
                
                <p>Le Client consent au paiement intégral du prix du Produit au moment de la commande. En procédant au paiement, le Client valide définitivement sa commande et s’engage à en acquitter le prix intégral en une seule fois.</p>
                
                <p>Au choix du Client, trois modes de paiement sont possibles :</p> 
                <ul>
                  <li><p>Paiement par virement bancaire grâce au RIB adressé par le service vente à distance.</p></li>
                  <li><p>Paiement par carte bancaire via un lien de paiement sécurisé.</p></li>
                  <li><p>Paiement par Alipay ou Wechat pay.</p></li> 
                </ul>
                <p>Aucun autre mode de paiement n’est autorisé. Le paiement s’effectue en Euros</p>
                
                <p>L’ordre de paiement effectué par le Client ne pourra être annulé. Le paiement de la commande par le Client est irrévocable, sans préjudice pour le Client d’exercer son droit de rétractation.</p>
                
                <p>Le règlement de la commande pourra être effectué par le Client par carte bancaire (cartes du réseau CB, Visa, Eurocard/Mastercard/American Express).</p> 
                
                <p>Le Client confirme et garantit qu'il est le titulaire du moyen de paiement utilisé pour le paiement et que ce dernier donne accès à des fonds suffisants pour couvrir tous les coûts nécessaires au règlement de la commande. Le compte du Client sera débité dès lors que la commande est prête à être expédiée.</p> 
                
                <p class="is-bold">5 - Livraison à domicile</p> 
                
                <p>Les frais de livraisons à domicile en France et Union Européenne sont :</p>
                <ul>
                  <li><p>15 € pour la France</p></li>
                  <li><p>30€ pour l’Union Européenne</p></li>
                </ul>
                <p>Par dérogation, les frais de livraison sont offerts:</p>
                <ul>
                  <li><p>aux Clients porteurs de carte de fidélité des Galeries  Lafayette au statut  Exception  & Premium</p></li>
                  <li><p>à tous Clients au-delà d’un seuil d’achat de 600€</p></li>
                </ul>
                <p>Les livraisons à l'international hors Union Européenne sont payantes pour tous les Clients selon le barème d’achat suivant :</p>
                <ul>
                  <li><p>moins de 2000 € d'achat =  60 € de frais de livraison</p></li>
                  <li><p>de 2000 €  à 4000 € d'achat = 100 € de frais de livraison</p></li>
                  <li><p>de 4000 €  à 8000 € d'achat = 150 €  de frais de livraison</p></li>
                  <li><p>de 8000 €  à 15000 € d'achat = 250 €  de frais de livraison</p></li>
                  <li><p>de 15 000€  à 20000 € d'achat = 400 €  de frais de livraison</p></li>
                  <li><p>au delà de 20000 €  d'achat = 500 €  de frais de livraison</p></li>
                </ul>
                <p>Pour les envois de marchandises de haute-valeur (haute-horlogerie, haute-joaillerie, maroquinerie réalisée avec des peaux précieuses ou issues des espèces protégées)  nécessitant un transport sécurisé un devis est établi pour chaque envoi par le service vente à distance à la demande des marques . Le montant des frais est communiqué au Client par la marque concernée avant acceptation de la commande.</p> 
                
                
                
                <p>a) Territoires couverts par la livraison</p>
                
                <p>Les livraisons sont possibles en France et à l’international dans les pays éligibles à la livraison.</p> 
                
                <p>Les livraisons au Brésil et en Russie sont exclues.</p> 
                
                <p>Les livraisons en Chine sont restreintes et il appartient au Client de se rapprocher du personnel du magasin pour connaître les territoires couverts par la livraison.</p> 
                
                <p>b) Cas particuliers de certains Produits</p>
                
                <p>En fonction des législations propres à certains pays, le Client est informé que certains Produits sont interdits d’exportation. Ces derniers ne pourront pas faire l’objet d’une livraison dans les territoires interdisant leur entrée sur leur sol. Il s’agit notamment mais non exclusivement des peaux d’animaux, cuirs d’animaux, fourrures, produits de parfumerie, alcool…<br />
                Le Client est invité à se rapprocher du personnel du magasin pour connaître les restrictions appliquées à la livraison desdits Produits.</p> 
                
                <p>c) Livraison à domicile</p>
                
                <p>Le Produit sera livré à l’adresse que le Client aura indiquée au moment de la commande. Le délai indicatif de livraison sera communiqué au Client et dans tous les cas il ne pourra pas excéder 30 jours à compter de la date de la commande sauf dispositions particulières ou cas fortuit.  Les colis sont remis contre signature.</p> 
                
                <p>Le transfert des risques intervient à la livraison, au moment de la remise du Produit.</p>
                
                <p>Il est vivement recommandé au Client de vérifier l'état et la conformité du colis dès la livraison. Si le colis paraît abîmé ou ouvert, il est vivement conseillé au Client de formuler des réserves sur récépissé de livraison délivré par le livreur. Si le colis paraît vide, le Client doit refuser la livraison de celui-ci et contacter le service Client des Galeries Lafayette. Toute réclamation relative à un défaut apparent ou à l’endommagement lors de la livraison d'un Produit, devra, pour être valable vis-à-vis du transporteur, être adressée sans délai à compter de la réception du ou des Produits, par e-mail ou courrier adressé au service Clients  des Galeries Lafayette dont l’adresse en entête du présent document. En cas de dommages, le Client les détaillera précisément.</p>
                
                <p>Par ailleurs, un dépôt plainte pourra être nécessaire pour justifier de la non réception du Produit.</p> 
                
                <p>Dans le cas de plusieurs présentations infructueuses à l’adresse du Client, la commande sera retournée au magasin. Il appartient au Client de contacter le service Client  pour reprogrammer une nouvelle livraison à ses frais.</p> 
                
                <p class="is-bold">6 - Livraison en Click and Collect</p>
                 
                <p>Le Produit sera livré gratuitement au service Click and Collect du magasin Galeries Lafayette de son choix et sous réserve que le service soit ouvert en magasin.</p>  
                
                <p>Le Client sera informé de la date de mise à disposition de ses colis au service Click & Collect de son magasin.</p> 
                
                <p>Au moment du retrait, le Client devra présenter une pièce d’identité originale (passeport, carte d’identité, permis de conduire ou titre de séjour).</p>
                
                <p>Si le retrait du colis est effectué par une tierce personne au nom du Client, elle devra présenter sa pièce d’identité, celle du Client, ainsi qu’une lettre de procuration.</p>
                
                <p>L’intégralité de la commande doit être vérifiée avec le Client : elle sera considérée comme conforme et complète après signature par le Client.</p>
                
                <p>Pour des raisons sanitaires, les essayages en magasin pourront être aménagés ou rendus impossibles. Le Client sera alors informé sur place des mesures d'hygiène à respecter.</p> 
                
                <p class="is-bold">7 - Droit de rétractation</p>
                
                <p>Tout Client, qui ne serait pas satisfait des Produits commandés peut exercer son droit de rétractation. Le délai du droit de rétractation légal est de quatorze (14) jours francs à compter de la réception du Produit, conformément aux articles L 221-18 et suivants du Code de la consommation, sauf exceptions et limitations exposées aux points b) et c) du présent article. <span class="is-bold">Les Produits ayant fait l’objet d’une commande spéciale ou retouchés ou ayant été personnalisés ne sont pas éligibles à la rétractation.</span></p> 
                
                <p>Le droit de rétractation peut être exercé par le Client par toute déclaration exprimant sa volonté de se rétracter.</p> 
                
                <p class="is-bold is-uppercase">MODÈLE DE FORMULAIRE DE RÉTRACTATION</p> 
                
                <p>Je vous notifie par la présente ma rétractation du contrat portant sur la vente (*) du bien / prestation de service  suivant………………………...<br />
                Commande reçue le ……………………..<br />
                n° de ma commande: <br />
                Nom: <br />
                Adresse: <br />
                Date: <br />
                Signature.</p> 
                
               <p> Dans le cas d’une commande portant sur plusieurs Produits livrés séparément ou sur un Produit composé de lots ou de pièces multiples dont la livraison est échelonnée, le délai court à compter de la réception du dernier Produit ou lot ou pièce.</p>
                
                <p>La preuve de l’exercice effectif du droit de rétractation pèse sur le Client.</p>
                
                <p>a) délai légal de 14 jours</p> 
                
                <p>Le Client doit renvoyer le Produit sans retard excessif et au plus tard dans les quatorze (14) jours suivants la communication de sa décision de se rétracter.</p>
                
                <p>Le Client retournant le Produit dans ce délai dispose du droit au remboursement du prix du Produit commandé et au remboursement des frais de livraison aller sur la base du coût d’une livraison standard, quel que soit le mode de livraison sélectionné par le Client lors de sa commande.</p> 
                
                <p>Les retours sont gratuits pour les achats livrés en France et Union Européenne.</p> 
                
                <p>Pour les livraisons internationales hors Union Européenne, les frais de retour, taxes gouvernementales et  formalités douanières sont à la charge du Client.</p>
                
                <p>Avant tout retour, le Client doit adresser un mail au service Client qui lui indiquera les modalités de retour.</p>
                
                <p>Les retours directement dans les magasins du réseau Galeries Lafayette  ne sont pas possibles.</p> 
                
                <p>Le remboursement des Produits retournés au prix facturé, y compris les frais de livraison aller, s'effectuera en utilisant le même moyen de paiement que celui ayant été utilisé pour régler la Commande. Le remboursement interviendra dans les quatorze (14) jours suivant la réception de l’information du Client de sa décision de se rétracter, les Galeries Lafayette se réservant la possibilité de différer le remboursement jusqu'à la récupération du Produit ou de la preuve d’expédition du Produit par le Client, la date retenue étant celle du premier de ces faits.</p> 
                
                <p>En cas d’usage du droit de rétractation pour une partie seulement de la commande, seul le prix facturé pour les Produits retournés sera remboursé. Les frais de livraison aller de la commande resteront à la charge du Client.</p>
                
                <p>En cas de rétractation partielle de la commande, le Client qui aurait bénéficié, lors de la commande initiale, de la gratuité de la livraison du fait du dépassement d'un certain montant de commande, pourra se voir refacturer les frais de livraison correspondant à sa commande effective, si cette dernière passait sous le seuil de gratuité de livraison.</p>
                
                <p>b) Délai supplémentaire de retour</p>
                
                <p>A titre commercial, le délai de rétractation légal susvisé est complété d’une possibilité de retour de seize(16) jours francs supplémentaires, avec remboursement du Produit commandé.<br />
                Le remboursement des Produits retournés au prix facturé s'effectuera par crédit sur le compte bancaire du Client correspondant au moyen de paiement de la commande d'origine, dans les quatorze jours suivant la réception par les Galeries Lafayette des Produits retournés. En cas d’usage de la possibilité de retour pour une partie seulement de la commande, seul le prix facturé pour les Produits retournés sera remboursé.<br />
                En cas de retour partiel d’une commande ayant bénéficié d’une livraison gratuite liée à son montant, le Client pourra se voir refacturer les frais de livraison correspondant à la commande effective, si cette dernière passait sous le seuil de gratuité de livraison. Seuls seront acceptés le retour des Produits dans leur emballage d'origine, complet (accessoires, notice, étiquette...), en parfait état, non utilisés, non lavés, non abîmés, non usés, non descellés pour les Produits Gourmet et Beauté, et accompagnés du bon de retour.</p> 
                 
                <p>c) Conditions et modalités de retour :</p>
                
                <p>Un Produit peut être essayé mais non porté. Tout Produit incomplet, contrefaisant, porté par le Client, abîmé, utilisé, endommagé, détérioré, sali ou encore consommé même en partie ne pourra pas faire l’objet d’un échange ou d’un remboursement.<br />
                Pour des raisons d'hygiène les Galeries Lafayette ne reprennent et ne remboursent pas les Produits de petit électroménager maison et soin de la personne (tondeuse, épilateur, lisseur...), lingerie portée (sous vêtement bas, maillot de bain, lingerie autoadhésive).<br />
                Les Produits dont les emballages ont été ouverts ne sont pas repris, échangés ou remboursés.<br />
                Pour être repris, les semelles de chaussures doivent être lisses, neuves et ne doivent comporter aucune rayure qui pourrait supposer qu'elles aient été portées. Les boîtes contenant les chaussures ne doivent pas être abîmées et ne doivent en aucun cas servir de carton de réexpédition. Les boîtes à chaussures ne doivent comporter ni ruban adhésif ni aucune inscription.<br />
                Les conditionnements de la beauté (crème, maquillage ...) ne doivent pas être descellés.<br />
                Les écrins des montres et des bijoux sont indispensables et le cadran doit être muni de l'autocollant protecteur d'origine.</p>
                
                <p class="is-bold">8 - Garanties</p>
                
                <p class="is-bold">Garantie Légale :</p> 
                
                <p>Tous les Produits vendus dans le magasin bénéficient de la garantie légale de conformité.</p> 
                
                
                <p>Les Galeries Lafayette sont tenues des défauts de conformité des Produits vendus dans le magasin dans les conditions de l’article L. 217-4 et suivants du code de la consommation et des défauts cachés de la chose vendue dans les conditions prévues aux articles 1641 et suivants du code civil.</p>
                
                <p>Lorsque le Client agit en garantie légale de conformité :</p>
                <ul>
                  <li><p>Il bénéficie d’un délai de 24 mois à compter de la délivrance du produit pour agir.</p></li> 
                  <li><p>Il peut choisir entre la réparation ou le remplacement du bien, sous réserve de certaines conditions de coûts prévues par l’article L 217-9 du Code de la consommation.</p></li> 
                  <li><p>Il est dispensé de rapporter la preuve de l’existence du défaut de conformité durant les 24 mois suivant la délivrance du bien.</p></li> 
                </ul>
                <p>La garantie légale de conformité s’applique indépendamment de la garantie commerciale éventuellement consentie.
                Il est rappelé que le Client peut décider de mettre en œuvre la garantie contre les défauts de la chose vendue au sens de l’article 1641 du Code civil ; dans ce cas, le consommateur peut choisir entre la résolution de la vente ou une réduction du prix.</p> 
                
                
                
                
                <p class="is-bold">Garanties Commerciales :</p> 
                
                <p>En plus de la garantie légale, des garanties commerciales peuvent être proposées pour certains Produits par les marques directement et auxquelles les Galeries Lafayette ne sont pas parties. L’étendue et la durée de ces garanties commerciales varie selon les marques concernées . Vous pouvez consulter les conditions de ces garanties commerciales sur le site Internet du fabricant et/ou dans la notice du Produit.<br />
                Pour pouvoir bénéficier de ces garanties commerciales le Client doit se rapprocher des marques ayant octroyé lesdites garanties commerciales.<br />
                Les Galeries Lafayette ne sauraient être tenues pour responsables en cas de refus desdites marques d’appliquer leurs garanties commerciales.</p> 
                
                <p class="is-bold">9 - Pièces détachées</p> 
                
                <p>En l’absence de précision expresse sur le Produit, les Galeries Lafayette n’ont pas été informées de la disponibilité d’éventuelles pièces détachées.</p> 
                
                <p class="is-bold">Reprise des produits électriques – 1 pour 1</p> 
                
                <p>En application des dispositions de l’article R 543-180 du Code de l’Environnement, si un Client achète un Produit électrique ou électronique ménager, les Galeries Lafayette reprennent gratuitement l’équipement électrique ou électronique usagé dont le Client se défait, dans la limite de quantité et du type d’équipement vendu.<br />
                Les Galeries Lafayette peuvent refuser de reprendre l’équipement électrique et électronique qui, à la suite d’une contamination, présente un risque pour la sécurité et la santé de son personnel.<br />
                Dans ce cas, une solution alternative pourra être proposée par les Galeries Lafayette.</p>
                
                <p class="is-bold">11 - Cas fortuit et Force Majeure</p>
                 
                <p>Ni le Client ni les Galeries Lafayette ne pourront être tenus pour responsables de la non-exécution totale ou partielle de leurs obligations, si cette non-exécution est due à un cas fortuit ou de force majeure qui en gênerait ou en retarderait l'exécution. Sont considérés comme tels, notamment, sans que cette liste soit limitative, la guerre, les émeutes, l'insurrection, les troubles sociaux, les grèves de toutes natures, l'inondation, l'incendie, la tempête, le manque de matières premières, le lock-out, hospitalisation (pour le Client notamment), pandémie et épidémie, décisions gouvernementales ou administratives impératives ordonnant la fermeture des commerces et les livraisons, ces différents éléments étant appréciés par rapport à la jurisprudence applicable.<br />
                Les Galeries Lafayette informeront le Client d'un semblable cas fortuit ou de force majeure dans les 7 (sept jours) de sa survenance.</p> 
                 
                <p>Dans l’hypothèse où des raisons impératives (notamment contexte d’incertitude lié à l’épidémie de Coronavirus et ses conséquences) et/ou un cas fortuit se présenteraient ou dans le cas d’une décision gouvernementale ou administrative et/ou une décision d’entreprise impérative, et notamment l’impossibilité d’ouvrir le magasin, empêchant les Galeries Lafayette ou le Client d’exécuter ses obligations, l’autre Partie devra en être informée sans délai.</p> 
                
                <p>Dans un premier temps, la commande sera suspendue, si la durée de suspension se maintient au-delà de 5 jours, la commande pourra être résiliée par les Galeries Lafayette ou le Client. Le Client sera alors remboursé de sa commande et ne pourra prétendre à aucune autre indemnité.</p>
                 
                <p class="is-bold">12 - Réclamations et service de médiation</p> 
                
                <p>Les éventuelles réclamations doivent être adressées au service du magasin à l’adresse précisée ci-dessous, qui en assure le traitement et le suivi.</p> 
                
                <p>Galeries Lafayette Haussmann - Service Relation Clientèle<br />
                40 Bld Haussmann<br />
                TSA 44100<br />
                75446 PARIS Cedex 9<br />
                Tél : 01.42.82.38.08<br />
                Courriel : <a href="mailto:shoppingadistance@galerieslafayette.com">shoppingadistance@galerieslafayette.com</a></p>
                
                <p>Conformément aux dispositions du Code de la consommation concernant le règlement amiable des litiges, Galeries Lafayette adhère au service de médiateur du e-commerce de la FEVAD (Fédération du e-commerce et de la vente à distance) dont les coordonnées sont les suivantes : Médiation FEVAD, 60 rue la Boëtie, 75008 Paris.<br />
                Après démarche préalable écrite des Clients auprès du service Client des Galeries Lafayette, le service médiateur peut être saisi pour tout litige de consommation dont le règlement n’aurait pas abouti. Pour connaître les modalités de saisine, il faut se connecter au site suivant : <a href="http://www.mediateurfevad.fr">http://www.mediateurfevad.fr</a> </p>
                
                
                <p class="is-bold">13 - Données à caractère personnel et démarchage téléphonique</p> 
                
                
                <p>Des données personnelles sont collectées pour permettre l’accès au service et pourront être utilisées par les Galeries Lafayette à des fins statistiques pour leur permettre de mieux connaître leur clientèle et d’améliorer la qualité de service.</p> 
                
                <p>Conformément à la loi informatique et liberté,  toute personne dispose d'un droit d'accès , de rectification et d’opposition aux données personnelles la concernant, en écrivant à 44 GALERIES LAFAYETTE – 44 GL -  Correspondant CNIL – TSA 23 000 44-48 rue Châteaudun – 75 009 PARIS ou par email à l'adresse <a href="mailto:relaiscil@galerieslafayette.com">relaiscil@galerieslafayette.com</a></p>
                
                <p>Pour en savoir plus, <a href="https://www.galerieslafayette.com/service/service-confidence">https://www.galerieslafayette.com/service/service-confidence</a></p>
                
                <p class="is-bold">14 - Responsabilité</p>
                
                <p>En dehors des cas expressément prévus par les textes en vigueur, la responsabilité des Galeries Lafayette est limitée aux dommages directs et prévisibles pouvant résulter de l'utilisation par le Client des Produits vendus et expédiés par ses soins.</p> 
                
                <p>Les Galeries Lafayette ne sauraient engager leur responsabilité pour des dommages résultant d'une faute du Client dans le cadre de l'usage des Produits.</p>
                
                <p>Ni la responsabilité des Galeries Lafayette ni celle des marques et/ou partenaires, ne saurait être engagée si l'inexécution ou la mauvaise exécution de leurs obligations sont imputables au Client, au fait imprévisible et insurmontable d'un tiers étranger à la fourniture des prestations prévues aux présentes conditions générales de vente, un cas fortuit ou à un cas de force majeure imprévisible, irrésistible et extérieur.</p>
                
                <p>Les Galeries Lafayette ne sont pas responsables des dommages de toutes natures pouvant être subis par le Client à l'occasion d'une visite les sites Internet et/ou usage d’applications ou d’outils numériques. L'utilisation de la page du service de vente à distance et/ou d’outils numériques implique la connaissance et l'acceptation par le Client des caractéristiques ainsi que des limites de l'Internet et des technologies qui y sont liées, l'absence de protection de certaines données contre des détournements éventuels ou piratage et risques de contamination par d'éventuels virus circulants sur le réseau. Les Galeries Lafayette déclinent toute responsabilité en cas de mauvaise utilisation ou d'incident lié à l'utilisation de l'ordinateur, de l'accès à Internet, de la maintenance ou du dysfonctionnement des serveurs, de la ligne téléphonique ou de toute autre connexion technique, et de l'envoi des formulaires à une adresse erronée ou incomplète, d'erreurs informatiques quelconques ou défauts constatés sur la page du service de vente à distance.</p>
                
                
                <p class="is-bold">15 - Propriété Intellectuelle</p>
                
                <p>L’ensemble des éléments édités au sein du site internet des Galeries Lafayette, tels que sons, images, photographies, vidéos, écrits, animations, programmes, modèles, charte graphique, bases de données, logiciel, et autre technologie sous-jacente est protégé par les dispositions du Code de propriété intellectuelle et appartiennent aux Galeries Lafayette ou le cas échéant, leur ont été dûment concédées. La marque “Galeries Lafayette”, ainsi que l'ensemble des marques figuratives ou non et plus généralement toutes les autres marques, illustrations, images et logotypes figurant sur les articles, leurs accessoires ou leurs emballages, qu'ils soient déposés ou non, sont et demeureront la propriété exclusive des Galeries Lafayette, à l'exception des droits détenus par les fournisseurs et les partenaires des Galeries Lafayette sur les visuels de leurs Produits, sur leurs marques et logos présentés.</p>
                
                <p>Le Client s’interdit de capturer et/ou enregistrer et/ou diffuser toute capture des écrans conversationnel avec le personnel de vente des Galeries Lafayette sur un quelconque moyen de communication électronique ou téléphonique.</p>
                
                <p>Toute reproduction totale ou partielle, modification ou utilisation de ces marques, illustrations, images et logotypes, pour quelque motif et sur quelque support que ce soit, sans accord exprès et préalable des Galeries Lafayette, est strictement interdite. Il en est de même de toute combinaison ou conjonction avec toute autre marque, symbole, logotype et plus généralement tout signe distinctif destiné à former un logo composite, à l’exception des logos et des signes descriptifs appartenant aux marques présentes sur le site. Il en ira de même pour tous droits d'auteur, dessins et modèles, brevets qui sont la propriété des Galeries Lafayette.</p>
                
                <p>Un utilisateur souhaitant placer sur son site internet personnel un lien simple renvoyant directement à la page du service de vente à distance du magasin Galeries Lafayette Haussmann, devra obligatoirement en demander au préalable l'autorisation expresse aux Galeries Lafayette.</p>
                
                <p class="is-bold">16 - Invalidité partielle</p> 
                
                <p>Si une ou plusieurs stipulations des présentes conditions générales de vente étaient jugées illicites ou nulles, cette nullité n'aurait pas pour effet d'entraîner la nullité des autres dispositions de ces conditions.</p>
                
                <p class="is-bold">17 - Droit applicable et juridictions compétentes</p>
                
                <p>Les présentes conditions générales de vente sont soumises à l'application du droit français.</p>
                
                <p>Seule la version française des présentes conditions générales de vente fait foi.</p>
                
                <p>En cas de difficulté survenant à l'occasion de la commande ou de la livraison des Produits, le Client aura la possibilité, avant toute action en justice, de rechercher une solution amiable, avec le service de médiation de la FEVAD ou notamment avec l'aide d'une association de consommateurs ou de tout autre conseil de son choix.</p>
                
                <p>Tout litige susceptible de résulter de l'interprétation ou de l'exécution des présentes conditions générales de vente et de ses suites sera porté devant les tribunaux compétents.</p>
                
              </div>
            </article>
          </div>
        </div>
      </div>
    </section>

</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-service.min.v01.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->
  
<!-- build:js /media/LP/src/js/2020/shoppingadistance-service.min.v01.js 
  <script src="../../../assets/js/accordion.js"></script>
  <script src="../../src/js/2020/shoppingadistance-service.js"></script>
endbuild -->
  
<?php include ('../../pages-defaults/footer.php'); ?>
