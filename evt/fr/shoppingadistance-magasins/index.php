<?php include ('../../pages-defaults/header.php'); ?>
<script>
  document.title = "SAD MAGS";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
  
<!-- <link href="../../../media/LP/src/css/2020/shoppingadistance-hub.css" rel="stylesheet" type="text/css"> -->

<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance-hub.min.v10.css" rel="stylesheet" type="text/css">
<style>
  html {
    scroll-behavior: smooth;
  }
  .ab684263--first-banner {
    margin-top: 0 !important;
  }
  .ab684263 {
    display: none !important; 
  }
  .gis-cta-reset{
    display: none !important;
  } 
</style>
<div class="luxe-augmente" data-bg-color="">
  <section class="luxe-augmente-hero" style="background-color: #000000;">
    <div class="container">
      <article class="luxe-row no-gutter luxe-middle-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          
          <div class="hero-image is-hidden-table">
            <figure class="image">
              <iframe class="hero-bg-image hero-video is-hidden-tablet" src="https://player.vimeo.com/video/477237166?autoplay=1&amp;muted=1&amp;autopause=0&amp;background=1" width="767" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
              <iframe class="hero-bg-image hero-video is-hidden-mobile" src="https://player.vimeo.com/video/477262284?autoplay=1&amp;muted=1&amp;autopause=0&amp;background=1" width="574" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
              <!-- <img class="is-hidden-tablet" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/hero-garde-robe-carre_M.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/hero-garde-robe-carre_M@2x.jpg 2x" 
                alt="Le prêt-à-porter en shopping à distance - Galeries Lafayette" width="767" height="767"> -->
              <!-- <img class="is-hidden-mobile" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/hero-garde-robe_D.jpg"
              srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/hero-garde-robe_D@2x.jpg 2x" 
              alt="Le prêt-à-porter en shopping à distance - Galeries Lafayette" width="978" height="910"> -->
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <h1 class="is-uppercase has-text-white">Shopping à<br>— distance<br><span style="font-size:50%; letter-spacing: -2px;"> Vos magasins restent ouverts</span></h1>
            <br />
            <p class="has-text-white text-left"><strong class="is-bold">Et si vous faisiez votre shopping aux Galeries Lafayette les plus proches de chez vous, depuis votre salon ?</strong><br /><br />
            Avec le Shopping à Distance, les portes de votre grand magasin restent ouvertes virtuellement pour se faire plaisir en toute simplicité ! Retrouvez vos conseillers personnels préférés en live vidéo sur rendez-vous.<br>
            Si vous savez ce que vous voulez : optez pour la wishlist !
            </p>
            <div class="buttons text-left">
              <a href="#marques" class="has-smoothscroll button primary outlined has-arrow-down">contacter un magasin</a>
              <a href="#wishlist" class="has-smoothscroll button primary outlined has-arrow-down">renseigner votre wishlist</a>
            </div>
          </div>
        </div>
      </article>
    </div>
  </section>
  
  <section class="luxe-augmente-body" style="background-color: #F1EFEC;">
    <div class="container">
      <!-- Fil d'Ariane -->
      <nav class="breadcrumb" aria-label="Fil d'Ariane">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com">
              <span itemprop="name">Accueil</span>
            </a>
            <meta itemprop="position" content="1">
          </li>
          <li  class="is-active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-magasins" aria-current="page">
              <span itemprop="name">Shopping à Distance Magasins</span>
            </a>
            <meta itemprop="position" content="2">
          </li>
        </ul>
      </nav>
        
      <!-- SERVICES -->
      <article class="services luxe-row">
        <div class="luxe-col-mobile-12">
          <h2>Le shopping digital<br class="is-hidden-tablet" /> personnalisé et sublimé</h2>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-3">
          <div class="service luxe-row">
            <div class="luxe-col-mobile-3 luxe-col-tablet-12">
              <div class="service-icon">
                <img class="icon" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/icon-cintre.svg" 
                alt="icone marques - Galeries Lafayette">
              </div>
            </div>
            <div class="luxe-col-mobile-9 luxe-col-tablet-12">
              <p class="service-txt"><strong>Vos marques</strong>
              Accédez à toutes vos marques préférées depuis chez vous</p>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-3">
          <div class="service luxe-row">
            <div class="luxe-col-mobile-3 luxe-col-tablet-12">
              <div class="service-icon">
                <img class="icon" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/icon-conseil.svg" 
                alt="icone luxe - Galeries Lafayette">
              </div>
            </div>
            <div class="luxe-col-mobile-9 luxe-col-tablet-12">
              <p class="service-txt"><strong>Le Conseil</strong>
              En live vidéo, laissez-vous inspirer par nos experts en mode lors d’un rendez-vous très privé</p>
            </div>
          </div>
        </div>  
        <div class="luxe-col-mobile-12 luxe-col-tablet-3">  
          <div class="service luxe-row">
            <div class="luxe-col-mobile-3 luxe-col-tablet-12">
              <div class="service-icon">
                <img class="icon" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/icon-simplicite.svg" 
                alt="icone luxe - Galeries Lafayette">
              </div>
            </div>
            <div class="luxe-col-mobile-9 luxe-col-tablet-12">
              <p class="service-txt"><strong>La simplicité</strong>
              Confirmez votre commande par e-mail et procédez au paiement sécurisé</p>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-3">  
          <div class="service luxe-row">
            <div class="luxe-col-mobile-3 luxe-col-tablet-12">
              <div class="service-icon">
                <img class="icon" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/icon-confort.svg" 
                alt="icone luxe - Galeries Lafayette">
              </div>
            </div>
            <div class="luxe-col-mobile-9 luxe-col-tablet-12">
              <p class="service-txt"><strong>Le confort</strong>
              Recevez votre commande à domicile ou en click&collect</p>
            </div>
          </div>
        </div>
      </article>
    </div>
  </section>
  
  <!-- MARQUES -->
    <section class="luxe-augmente-body marques" id="marques">
      <div class="container">
        <article class="luxe-row no-gutter luxe-middle-mobile text-left">
          <div class="luxe-col-mobile-12 luxe-col-tablet-5">
            <div class="article-body">
              <div class="tag is-uppercase">Live Shopping</div>
              <h2 class="is-uppercase">Contactez votre<br class="is-hidden-mobile" /> magasin en live<br class="is-hidden-mobile" /> shopping</h2>
              <p>Plus besoin de vous déplacer en magasin : les conseillers de vos Galeries Lafayette préférées sont à votre disposition pour vous faire découvrir les collections en live shopping vidéo sur rendez-vous.</p>
              <p><strong class="is-bold">Lundi au samedi : 11H — 19H</strong><br />
                <strong class="is-bold">Dimanche et jours féries : 12H — 19H</strong></p>
            </div>
          </div>
          <div class="luxe-col-mobile-12 luxe-col-tablet-7">
            
            <div class="live-brands-list top16 show-brands" id="brands-top16"></div>
            
          </div>

          <div class="luxe-col-mobile-12 luxe-col-tablet-5">
            <div class="notice">
              <p><small>Cliquez sur ce symbole pour contacter un Conseiller en vidéo.
              Vous savez déjà ce que vous désirez ? Optez pour la wishlist : renseignez dans le formulaire en ligne les pièces dont vous avez envie.</small></p>
            </div>
          </div>
          
        </article>
      </div>
    </section>
  
  <!-- PERSONAL SHOPPER -->
  <section class="luxe-augmente-body" id="personalshopper">
    <div class="container">
      <!-- colonnes inversées avec la class luxe-reverse -->
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse text-left">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <div class="tag is-uppercase">Personal shopper</div>
            <h2 class="is-uppercase">L’expertise de nos personal shoppers en&nbsp;vidéo</h2>
            <p>Vous souhaitez être inspiré pour un cadeau ou pour compléter votre vestiaire ? Laissez-vous conseiller par nos conseillers personnels et découvrez en live shopping vidéo une sélection personnalisée de marques, de pièces et de styles. Un conseil mode offert sur rendez-vous.</p>
            <a href="" class="button primary trigger_liveshopping" onclick="clickedBrand='Personal shopper'; cleanBrandName='default'; appointedWidgetId='5f8876b66a0d547bea7db8e4'; GL_GIS.LiveShopping.checkPopinClerkAvailability('ps1,ps2,ps3'); return false;">Contacter un personal shopper<span class="icon"><i class="microphone"></i></span></a>
            <div id="jouets"></div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/personal-shopper-main.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/personal-shopper-main@2x.jpg 2x" 
                alt="L’expertise de nos personal shoppers en vidéo - Galeries Lafayette" width="574" height="652">
            </figure>
          </div>
        </div>
      </article>
      
    </div>
  </section>
      
  <!-- WISHLIST -->
  <section class="luxe-augmente-body" id="wishlist">
    <div class="container">
      
      <article class="luxe-row no-gutter luxe-middle-mobile text-left">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <div class="tag is-uppercase">Wishlist</div>
            <h2 class="is-uppercase">Vous savez ce que vous<br /> désirez&nbsp;? Optez pour<br /> la wishlist</h2>
            <p>N'hésitez pas à nous partager vos souhaits en remplissant notre formulaire pour être livré directement chez vous.</p>
            <a href="" class="button primary trigger_liveshopping" onclick="clickedBrand=' '; cleanBrandName='default'; return false;" data-reveal-id="popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/wishlist-reseau.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/wishlist-reseau@2x.jpg 2x" 
                alt="Vous savez ce que vous désirez ? Optez pour la wishlist - Galeries Lafayette" width="574" height="624">
            </figure>
          </div>
        </div>
      </article>
      <hr />
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse text-left secondary">
        <div class="luxe-col-mobile-12 luxe-col-tablet-8 luxe-col-desktop-9">
          <div class="article-body">
            <h3>Montres et bijoux d'exception</h3>
            <p>Indiquez dans votre wishlist la montre ou le bijou de vos rêves parmi les plus prestigieuses marques de haute horlogerie et de joaillerie : Bvlgari, Chaumet, Messika, Mauboussin, Djula, Jaeger Lecoultre, Omega, Bell & Ross, Tag Heuer, Montblanc.</p>
            <a href="" class="button primary trigger_liveshopping" onclick="clickedBrand=' '; cleanBrandName='default'; return false;" data-reveal-id="popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-4 luxe-col-desktop-3 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/wishlist-bagues.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/wishlist-bagues@2x.jpg 2x" 
                alt="L’expertise de nos personal shoppers en vidéo - Galeries Lafayette" width="280" height="280">
            </figure>
          </div>
        </div>
      </article>
    </div>
  </section>
  
  <!-- SAD Luxe -->
  <section class="luxe-augmente-body">
    <div class="container">
      <!-- colonnes inversées avec la class luxe-reverse -->
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse text-left">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <div class="tag is-uppercase is-hidden">Personal shopper</div>
            <h2 class="is-uppercase">Vous souhaitez retrouver directement vos marques de luxe préférées ?</h2>
            <p>Accédez aux plus belles marques Luxe & Créateurs du grand magasin iconique d’Haussmann en live shopping, sur rendez-vous ou en instantané.</p>
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance" class="button primary">Découvrir les marques</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/coupole-hsm.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/coupole-hsm@2x.jpg 2x" 
                alt="L’expertise de nos personal shoppers en vidéo - Galeries Lafayette" width="574" height="652">
            </figure>
          </div>
        </div>
      </article>
      
    </div>
  </section>
  
  <!-- FAQ -->
  <section class="luxe-augmente-body" style="background-color: #F1EFEC;">
    <div class="container">
      <div class="luxe-row text-left faq">
        <div class="luxe-col-mobile-12 luxe-col-tablet-5">
          <h2>Avec le Shopping à<br /> Distance, toujours plus<br /> d’avantages</h2>
          <p>Vous avez des questions ? Voir notre <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">FAQ</a></p>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-7 luxe-col-desktop-6">
          <ul>
            <li><p>Un service de personal shopping en vidéo, exclusif et gratuit.</p></li>
              
            <li><p>Une expérience en toute intimité sur-mesure : vous entendez et voyez votre conseiller ! Ce dernier vous entend et sera en capacité de vous voir uniquement si vous autorisez la vidéo lors de la connexion.</p></li>
              
            <li><p>La livraison des commandes partout en France, à votre domicile et dans l’espace click & collect des grands magasins Galeries Lafayette.</p></li>
              
            <li><p>Un paiement sécurisé, en ligne ou par virement.</p></li>
              
            <li><p>Le cumul des points fidélité et des avantages du programme Mes Galeries.</p></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  
  <!-- CONDTIONS -->
  <section class="luxe-augmente-body">
    <div class="container">
      <div class="luxe-row conditions">
        <div class="luxe-col-mobile-12">
          <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service#conditions" class="is-text">Conditions générales du Shopping à Distance</a>
        </div>
      </div>
    </div>
  </section>
</div>


<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-hub.min.v02.js"></script>

<div id="goinstore_modals" style="height:0;"></div>
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/shoppingadistance-goinstore-v2.min.css" rel="stylesheet" type="text/css">
<script src="https://static.galerieslafayette.com/media/LP/src/js/2021/shoppingadistance-goinstore-v2.min.v03.js"></script>

<script>
  var clickedBrand = '';
  var appointedWidgetId = '';
  var marques = [
    {"marque":"Haussmann","secteur":"","cleanbrandname":"","top":true,"visio":true,"key":"glps1","apwidget":"5f7c90eed2f4b3243d01d389"},
    {"marque":"Nice Masséna","secteur":"","cleanbrandname":"","top":true,"visio":true,"key":"","apwidget":""},
    {"marque":"Cap 3000","secteur":"","cleanbrandname":"","top":true,"visio":true,"key":"","apwidget":""},
    {"marque":"Bordeaux","secteur":"","cleanbrandname":"","top":true,"visio":true,"key":"","apwidget":""},
    {"marque":"Strasbourg","secteur":"","cleanbrandname":"","top":true,"visio":true,"key":"","apwidget":""},
    {"marque":"Nantes","secteur":"","cleanbrandname":"","top":true,"visio":true,"key":"","apwidget":""},
    {"marque":"Toulouse","secteur":"","cleanbrandname":"","top":true,"visio":true,"key":"","apwidget":""},
    {"marque":"Lyon Bron","secteur":"","cleanbrandname":"","top":true,"visio":true,"key":"","apwidget":""},
    {"marque":"Lyon Part-Dieu","secteur":"","cleanbrandname":"","top":true,"visio":true,"key":"","apwidget":""},
    {"marque":"Montpellier","secteur":"","cleanbrandname":"","top":true,"visio":true,"key":"","apwidget":""},
    {"marque":"Marseille Prado","secteur":"","cleanbrandname":"","top":true,"visio":true,"key":"","apwidget":""},
    {"marque":"Annecy","secteur":"","cleanbrandname":"","top":true,"visio":true,"key":"","apwidget":""},
    {"marque":"Metz","secteur":"","cleanbrandname":"","top":true,"visio":true,"key":"","apwidget":""}
  ];

  function getList(el, top = false) {
    const bloc = document.createElement('div');
    bloc.classList.add('brands-container');
    let wrapper_letter = null;
    let wrapper = top ? bloc : wrapper_letter;
    const lst = top ? marques.filter(m => m.top) : marques;
    let letter = null;
    lst.forEach(m => {
        const l = m.marque.substring(0, 1);
        const line = document.createElement('a');
        
        if (m.marque === "") {
          line.setAttribute('href', "https://www.galerieslafayette.com/evt/fr/shoppingadistance/");
          
        } else {
          line.setAttribute('href', "");
          if (m.visio) {
            line.setAttribute("onclick", "clickedBrand='" + m.marque.replace(/'/g, "\\'") + "'; cleanBrandName='" + m.cleanbrandname + "'; appointedWidgetId='" + m.apwidget + "'; GL_GIS.LiveShopping.checkPopinClerkAvailability('" + m.key + "'); return false;");
            line.classList.add('has-visio');
          } else {
              line.setAttribute("onclick", "clickedBrand='" + m.marque.replace(/'/g, "\\'") + "'; cleanBrandName='" + m.cleanbrandname + "'; return false;");
              line.dataset.revealId = "popin-live-shopping-form";
          }
        }
        
        line.innerText = m.marque;
        line.classList.add('marque', 'trigger_liveshopping');

        if (m.secteur) {
            const univers = document.createElement('span');
            univers.innerText = m.secteur;
            line.appendChild(univers);
        }
        wrapper.appendChild(line);
    });
    document.getElementById(el).appendChild(bloc);
  }

  getList('brands-top16', true);
</script>
<!--=========================== FIN LANDING PAGE ========================-->
    
<!-- build:js /media/LP/src/js/2021/shoppingadistance-goinstore-v2.min.v03.js 
  <script src="../../src/js/2021/shoppingadistance-goinstore-v2.js"></script>
<!-- endbuild -->
  
<!-- build:js /media/LP/src/js/2020/shoppingadistance-hub.min.v02.js 
  <script src="../../../assets/js/flickity.pkgd.min.js"></script>
  <script src="../../src/js/2020/shoppingadistance-hub.js"></script>
endbuild -->
  
<?php include ('../../pages-defaults/footer.php'); ?>
