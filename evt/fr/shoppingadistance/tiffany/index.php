<?php include ('../../../pages-defaults/header.php'); ?>
<script>
  document.title = "Tiffany & Co.";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
<!-- <link href="../../../../media/LP/src/css/2020/shoppingadistance.css" rel="stylesheet" type="text/css"> -->
  
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance.min.v05.css" rel="stylesheet" type="text/css">
<style>
  .back-top {
    display: none !important;
  }
  
  @media screen and (min-width: 768px) {
    .collection-main-text {
      transform-origin: top left;
      margin-right: -50px;
      left: -40px;
    }
    .luxe-augmente .collections .has-text-vertical {
      top: 50%;
    }
  }
  @media screen and (min-width: 1025px) {
    .luxe-augmente .collections .has-text-vertical {
      top: 40%;
    }
  }
  @media screen and (min-width: 1250px) {
    .collection-main-text {
      left: 0;
    }
    .luxe-augmente .collections .has-text-vertical {
      top: 30%;
    }
  }
</style>
<div class="luxe-augmente template-vad" data-bg-color="">
  <section class="luxe-augmente-hero" style="background-color: #000000;">
    <div class="hero-bg-image" style="background-image:url('https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/tiffany/tiffany-hero.jpg'); background-position:center top;">&nbsp;</div>
    <div class="container">
      <div class="luxe-row">
        <div class="luxe-col-mobile-12">
          <h1>Tiffany & Co.</h1>
          <a href="#collections" class="is-arrow-down has-smoothscroll">
            <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/circled-arrow-down.svg" alt="" width="31" height="31" />
          </a>
        </div>
      </div>
    </div>
  </section>
  
  <section class="luxe-augmente-body">
    <div class="container">
      <!-- Fil d'Ariane -->
      <nav class="breadcrumb" aria-label="Fil d'Ariane">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com">
              <span itemprop="name">Accueil</span>
            </a>
            <meta itemprop="position" content="1">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/h/createurs">
              <span itemprop="name">Luxe & Créateurs</span>
            </a>
            <meta itemprop="position" content="2">
          </li>
          <li  class="is-active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/tiffany" aria-current="page">
              <span itemprop="name">Tiffany & Co.</span>
            </a>
            <meta itemprop="position" content="3">
          </li>
        </ul>
      </nav>
      
      <!-- INTRO -->
      <article class="intro">
        <div class="luxe-row luxe-center-mobile">
          <div class="luxe-col-tablet-7 luxe-col-desktop-5 luxe-col-fullhd-4">
            <h3 class="is-uppercase is-hidden">La maison Tiffany & Co.</h3>
            <div class="intro-txt">
              <p>En 1837, Charles Lewis Tiffany fonda sa société à New York où son magasin fut vite acclamé pour la qualité exceptionnelle de ses pierres précieuses. Dès lors, Tiffany & Co. est devenu synonyme &nbsp;
                <span class="read-more"><span class="read-more-toggle">&hellip; suite</span></span>
                <span class="read-more-content is-hidden"> d’élégance, de design innovant, de savoir-faire raffiné et d’excellence créative. La Maison devint une référence culturelle comme l’illustre le roman de Truman Capote Breakfast at Tiffany’s et le film incarné par Audrey Hepburn.&nbsp;
                  <span class="read-less-toggle">Fermer</span>
                </span>  
              </p>
            </div>
          </div>
        </div>
        <div class="luxe-row luxe-center-mobile" id="collections">
          <div class="luxe-col-mobile-12 luxe-col-tablet-9 has-text-smaller">
            <h2>Collections à découvrir exclusivement<br class="is-hidden-mobile" /> en&nbsp;Shopping à&nbsp;Distance</h2>
            <br />
            <div class="intro-links">
              <a href="" class="button primary" onclick="initWhislist(); return false;" data-reveal-id="blkr-popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>  
              <br />
              <a href="#shopping-a-distance" class="is-text is-pink is-bold has-smoothscroll">Qu'est-ce que le Shopping à Distance ?</a>
            </div>
          </div>
        </div>
      </article>
      
      <!-- ARTICLE COLLECTIONS -->
      <article class="luxe-row no-gutter luxe-center-mobile text-left collections">
        
        <div class="luxe-col-mobile-12 luxe-col-fullhd-11">
          <div class="tabs-container">
            <div class="tabs is-centered is-hidden" role="tablist">
              <ul>
                <li role="presentation">
                  <a href="#pret-a-porter" aria-controls="pret-a-porter" role="tab" class="is-active" aria-selected="true">Prêt-à-porter</a>
                </li>
                <li role="presentation">
                  <a href="#beaute" aria-controls="beaute" role="tab" aria-selected="false" tabindex="-1">Beauté</a>
                </li>
              </ul>
            </div>
            
            <!-- Onglet Prët-à-porter -->
            <div class="tab-pane is-active" id="pret-a-porter" role="tabpanel" tabindex="0" aria-labelledby="pret-a-porter-tab">
              
              <div class="luxe-row no-gutter">
                <div class="luxe-col-mobile-12 luxe-col-tablet-10">
                  <div class="animated-article article1">
                    <figure class="image article-image">
                      <a href="" onclick="initWhislist(); return false;" data-reveal-id="blkr-popin-live-shopping-form">
                        <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/tiffany/tiffany-t.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/tiffany/tiffany-t@2x.jpg 2x" 
                        alt="Tiffany & Co. Tiffany T - Galeries Lafayette">
                      </a>
                      <figcaption class="text-right">Tiffany T</figcaption>
                    </figure>
                  </div>
                </div>
                
                <div class="luxe-col-mobile-10 luxe-col-mobile-offset-2 luxe-col-tablet-3">
                  <div class="has-text-fat text-left animated-article">
                    <p class="article-txt is-uppercase collection-main-text">Créations <br />iconiques</p>
                  </div>
                </div>
                
                <div class="luxe-col-mobile-2 is-hidden-tablet">
                  <div class="has-text-big has-text-vertical">
                    <p class="is-uppercase">Tiffany&nbsp;&&nbsp;Co.</p>
                  </div>
                </div>
                <div class="luxe-col-mobile-10 luxe-col-tablet-7 text-right">
                  <div class="animated-article article2">
                    <figure class="image article-image">
                      <a href="" onclick="initWhislist(); return false;" data-reveal-id="blkr-popin-live-shopping-form">
                        <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/tiffany/tiffany-amour-et-fiancailles.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/tiffany/tiffany-amour-et-fiancailles@2x.jpg 2x" 
                        alt="Tiffany & Co. Amour et fiançailles - Galeries Lafayette">
                      </a>
                      <figcaption class="text-right">Amour et fiançailles</figcaption>
                    </figure>
                  </div>
                </div>
              </div>
              
              <div class="luxe-row no-gutter is-pulled-row">
                <div class="luxe-col-mobile-8 luxe-col-mobile-offset-2 luxe-col-tablet-4 luxe-col-tablet-offset-0">
                  <div class="animated-article article3">
                    <figure class="image article-image" style="margin-top:-25px;">
                      <a href="" onclick="initWhislist(); return false;" data-reveal-id="blkr-popin-live-shopping-form">
                        <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/tiffany/tiffany-elsa-peretti.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/tiffany/tiffany-elsa-peretti@2x.jpg 2x" 
                        alt="Tiffany & Co. Elsa Peretti® - Galeries Lafayette">
                      </a>
                      <figcaption class="text-left has-text-right-tablet">Elsa Peretti&reg;</figcaption>
                    </figure>
                  </div>
                </div>
                <div class="luxe-col-tablet-1 is-hidden-mobile">
                  <div class="has-text-big has-text-vertical">
                    <p class="is-uppercase">Tiffany&nbsp;&&nbsp;Co.</p>
                  </div>
                </div>
              </div>              
              
            </div>
            
            
            
            <!-- Onglet beauté -->
            <div class="tab-pane" id="beaute" role="tabpanel" tabindex="0" aria-labelledby="beaute-tab" hidden>              
            </div>
            
            
            
          </div>
        </div>
      </article>
      
      <!-- ARTICLE SHOPPING À DISTANCE -->
      <article class="distance-shopping luxe-row no-gutter text-left" id="shopping-a-distance">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 animated-article">
          <div class="article-body has-text-huge has-text-right-tablet">
            <h3>SHOPPING À<br /> — DISTANCE</h3>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-col-fullhd-5 animated-article">
          <div class="article-body article-txt">
            <h4>Un conseiller Tiffany & Co. à votre&nbsp;disposition</h4>
            <p>Avec le Shopping à Distance, accédez aux collections Tiffany & Co. des Galeries Lafayette Paris Haussmann et effectuez vos achats où que vous soyez, en renseignant votre wishlist avec les pièces dont vous avez envie. <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">En savoir plus</a></p>
            <a href="" class="button primary" onclick="initWhislist(); return false;" data-reveal-id="blkr-popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>            
          </div>
        </div>
      </article>
      
      <div class="luxe-row conditions">
        <div class="luxe-col-mobile-12">
          <hr class="is-marginless" />
          <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service#conditions" class="is-text">Conditions générales du Shopping à Distance</a>
        </div>
      </div>
    </div>
  </section>
  
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-vad.min.v03.js"></script>

<div id="live_modals" style="height:0;"></div>
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/shoppingadistance-goinstore-v2.min.css" rel="stylesheet" type="text/css">
<script src="https://static.galerieslafayette.com/media/LP/src/js/2022/shoppingadistance-bleeker.min.v02.js"></script>

<script>
  (function() {
    let scripts = Array
    .from(document.querySelectorAll('script'))
    .map(scr => scr.src);
    const url = new URL(window.location.href);
    var search = url.search;
    var paramsStr = search.substr(1);
    if (!scripts.includes('https://widget.bleeker-st.com/gl/embed-dev.js')) { 
      var scriptNode = document.createElement('script');
      scriptNode['id'] = 'bleeker-script';
      scriptNode['src'] = 'https://widget.bleeker-st.com/gl/embed-dev.js'; scriptNode['type'] = 'text/javascript'; document.documentElement.appendChild(scriptNode);
    }
    window.onload = function() {
      initBleekerLiveShopping({
        params: paramsStr,
        vendorID: 'GL',
        uaGoogle : 'UA-4094055-1',
        widget: true,
        type: 'overlay', 
      });
    } 
  })();
  
  var brandID='Tiffany';
  var brand='Tiffany & Co.';
  var posterName='tiffany-v1';
  function initWhislist() {
    brand; 
    posterName;
    GL_BLKR.LiveShopping.displaySadBrand();
  }
</script>
<!--=========================== FIN LANDING PAGE ========================-->
 
<!-- build:js /media/LP/src/js/2020/shoppingadistance-goinstore.min.v01.js
  <script src="../../../src/js/2020/shoppingadistance-goinstore.js"></script>
endbuild -->

<!-- build:js /media/LP/src/js/2020/shoppingadistance-vad.min.v01.js
<script src="../../../../assets/js/tabs.js"></script>
<script src="../../../../assets/js/ScrollMagic.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
<script src="../../../src/js/2020/shoppingadistance-vad.js"></script>
endbuild -->

<?php include ('../../../pages-defaults/footer.php'); ?>
