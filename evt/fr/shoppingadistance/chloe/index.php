<?php include ('../../../pages-defaults/header.php'); ?>
<script>
  document.title = "Chloé";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
<!-- <link href="../../../../media/LP/src/css/2020/shoppingadistance.css" rel="stylesheet" type="text/css"> -->

<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance.min.v05.css" rel="stylesheet" type="text/css">
<style>
  .back-top {
    display: none !important;
  }
  .gis-cta-logo {
    height: 65px !important;
    width: 65px !important; 
  }
  .gis-cta-indicator {
    height: 15px !important;
    width: 15px !important;
  }
  @media screen and (max-width: 1024px) {
    .gis-cta-logo {
      height: 60px !important;
      width: 60px !important; 
    }
  }
</style>
<div class="luxe-augmente template-marque" data-bg-color="">
  <section class="luxe-augmente-hero" style="background-color: #000000;">
    <div class="hero-bg-image" style="background-image:url('https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/chloe/chloe-hero.jpg'); background-position:center top;">&nbsp;</div>
    <div class="container">
      <div class="luxe-row">
        <div class="luxe-col-mobile-12">
          <h1>Chloé</h1>
          <a href="#collections" class="button primary has-arrow-down has-smoothscroll">Découvrir les collections</a>
        </div>
      </div>
    </div>
  </section>
  
  <section class="luxe-augmente-body">
    <div class="container">
      <!-- Fil d'Ariane -->
      <nav class="breadcrumb" aria-label="Fil d'Ariane">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com">
              <span itemprop="name">Accueil</span>
            </a>
            <meta itemprop="position" content="1">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/h/createurs">
              <span itemprop="name">Luxe & Créateurs</span>
            </a>
            <meta itemprop="position" content="2">
          </li>
          <li  class="is-active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/chloe" aria-current="page">
              <span itemprop="name">Chloé</span>
            </a>
            <meta itemprop="position" content="3">
          </li>
        </ul>
      </nav>
        
      <!-- INTRO -->
      <article class="intro luxe-row  luxe-center-mobile">
        <div class="luxe-col-tablet-7 luxe-col-desktop-5 luxe-col-fullhd-4">
          <h3 class="is-uppercase">La maison Chloé</h3>
          <div class="intro-txt">
            <p>En 1952, Gaby Aghion conçoit sa première collection et donne naissance à Chloé. Un vestiaire élégant tout en silhouettes décontractées, telle est l'alternative &nbsp;
              <span class="read-more"><span class="read-more-toggle">&hellip; suite</span></span>
              <span class="read-more-content is-hidden"> au chic rigide que propose la marque. Karl Lagerfeld, Stella MCartney, Pheobe Philo et Clare Waight Keller se succèdent à la tête de Chloé offrant leur vision unique de la femme moderne. Aujourd'hui, sous la direction de Natasha Ramsay-Levi, allure boyish, silhouettes bohèmes, tailoring et moderne féminité font toujours battre le coeur de la femme Chloé. &nbsp;
                <span class="read-less-toggle">Fermer</span>
              </span>  
            </p>
          </div>
        </div>
      </article>
      
      <div id="collections"></div>
      
      <div id="sac"></div>
      <!-- ARTICLE 1 -->
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse animated-article article1">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
          <div class="article-body">
            <h2>Les sacs en Shopping à Distance</h2>
            <p>Avec le Shopping à Distance, découvrez les collections Chloé sacs des Galeries Lafayette Paris Haussmann et effectuez vos achats où que vous soyez, en live shopping vidéo avec un conseiller de la marque ou en renseignant votre wishlist avec les pièces dont vous avez envie. <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">En savoir +</a></p>
            <a href="" class="button outlined" onclick="initSAD(); return false;">Acheter en live vidéo<span class="icon"><i class="microphone"></i></span></a>
            <a href="" class="button outlined" onclick="initWhislist(); return false;" data-reveal-id="popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/chloe/chloe-sacs-pe.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/chloe/chloe-sacs-pe@2x.jpg 2x" 
                alt="Les sacs Chloé en Shopping à Distance - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      <!-- ARTICLE 2 -->
      <article class="luxe-row no-gutter luxe-middle-mobile animated-article article2">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
          <div class="article-body">
            <h2>Les chaussures en Shopping à Distance</h2>
            <p>Avec le Shopping à Distance, découvrez les collections Chloé chaussures des Galeries Lafayette Paris Haussmann et effectuez vos achats où que vous soyez, en live shopping vidéo avec un conseiller de la marque ou en renseignant votre wishlist avec les pièces dont vous avez envie. <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">En savoir +</a></p>
            <a href="" class="button outlined" onclick="initSAD(); return false;">Acheter en live vidéo<span class="icon"><i class="microphone"></i></span></a>
            <a href="" class="button outlined" onclick="initWhislist(); return false;" data-reveal-id="popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/chloe/chloe-chaussures.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/chloe/chloe-chaussures@2x.jpg 2x" 
                alt="Les chaussures Chloé en Shopping à Distance - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
            
      <!-- ARTICLE 2 -->
      <article class="luxe-row no-gutter luxe-middle-mobile  luxe-reverse text-left animated-article article3">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <h2>Les parfums en ligne</h2>
            <p>Véritables distillations d’élégance et de liberté, les parfums Chloé incarnent tous une facette de la femme Chloé, dans de délicats flacons inspirés des pièces iconiques de la Maison.</p>
            <a href="https://www.galerieslafayette.com/b/chloe" class="button outlined is-marginless">Découvrir la sélection</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/chloe/chloe-parfums-sad.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/chloe/chloe-parfums-sad@2x.jpg 2x" 
                alt="La sélection Chloé lunettes en ligne - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      
      <!-- SUR LE PODIUM -->
      <article class="podium">
        <div class="luxe-row no-gutter text-center">
          <div class="luxe-col-mobile-12 has-text-fatt">
            <h3 class="is-uppercase">Sur le Podium</h3>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center podium-items" style="background-color: #f0f0f0;">
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/chloe+-+eau+de+parfum-chloe/19689488/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/chloe/chloe-parfum.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/chloe/chloe-parfum@2x.jpg 2x" 
                    alt="Lunettes de Soleil polarisées BE4216 - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>CHLOÉ <br />Eau de Parfum</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/chloe+nomade+-+eau+de+parfum-chloe/52625776/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/chloe/chloe-parfum-nomade.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/chloe/chloe-parfum-nomade@2x.jpg 2x" 
                    alt="Lunettes de Soleil BE4251Q - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>CHLOÉ NOMADE <br />Eau de Parfum</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/chloe+love+story+eau+de+parfum-chloe/65576761/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/chloe/chloe-parfum-love-story.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/chloe/chloe-parfum-love-story@2x.jpg 2x" 
                    alt="Lunettes de Soleil BE4291 - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>CHLOÉ LOVE STORY <br />Eau de Parfum</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/atelier+des+fleurs+magnolia+alba+-+eau+de+parfum-chloe/66193687/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/chloe/chloe-parfum-atelier-des-fleurs.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/chloe/chloe-parfum-atelier-des-fleurs@2x.jpg 2x" 
                    alt="Lunettes de Soleil polarisées BE4286 - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>ATELIER DES FLEURS <br />Magnolia Alba - Eau de Parfum</p></div>
            </a>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center">
          <div class="luxe-col-mobile-12">
            <a href="https://www.galerieslafayette.com/b/chloe/ct/beaute-parfum-parfum+femme-parfum" class="podium-cta"><span class="plus"></span>Voir tous les articles</a>
          </div>
        </div>
      </article>
      
      
      <div class="luxe-row conditions">
        <div class="luxe-col-mobile-12">
          <hr class="is-marginless" />
          <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service#conditions" class="is-text">Conditions générales du Shopping à Distance</a>
        </div>
      </div>
      
    </div>
  </section>
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-mix.min.v03.js"></script>

<div id="goinstore_modals" style="height:0;"></div>
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/shoppingadistance-goinstore-v2.min.css" rel="stylesheet" type="text/css">
<script src="https://static.galerieslafayette.com/media/LP/src/js/2021/shoppingadistance-goinstore-v2.min.v03.js"></script>

<script>
  var routingKey = "brand8";
  var clickedBrand='Chloé'; 
  var cleanBrandName='chloe'; 
  function initWhislist() {
    clickedBrand; 
    cleanBrandName; 
    GL_GIS.LiveShopping.displaySadBrand();
  }
  function initSAD() {
    clickedBrand; 
    cleanBrandName; 
    appointedWidgetId='5f8879096a0d547bea7db900'; 
    GL_GIS.LiveShopping.displaySadBrand();  
    GL_GIS.LiveShopping.checkPopinClerkAvailability(routingKey);
  }

  window.onload = function() {
    GL_GIS.LiveShopping.init(routingKey);    
    
    var gisCTA = document.getElementById('gis-cta');
    if (gisCTA) {
      gisCTA.addEventListener('click', function(e) {
        e.stopPropagation();
        initSAD();
        GL_GIS.LiveShopping.displaySadBrand();
        return false;  
      }, true);
    }
    
    var gisMsgContainer = document.getElementById('gis-awareness-msg-container');
    if (gisMsgContainer) {
      gisMsgContainer.lastChild.addEventListener('click', function(e) { 
        e.stopPropagation();
        initSAD();
        GL_GIS.LiveShopping.displaySadBrand();
        return false;
      }, true);
    }
  }; 
</script>
<!--=========================== FIN LANDING PAGE ========================-->
  
<!-- build:js /media/LP/src/js/2020/shoppingadistance-goinstore.min.v03.js
  <script src="../../../src/js/2020/shoppingadistance-goinstore.js"></script>
endbuild -->

<!-- build:js /media/LP/src/js/2020/shoppingadistance-mix.min.v02.js 
<script src="../../../../assets/js/ScrollMagic.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
<script src="../../../src/js/2020/shoppingadistance-mix.js"></script>
endbuild -->
  
<?php include ('../../../pages-defaults/footer.php'); ?>
