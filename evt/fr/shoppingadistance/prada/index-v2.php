<?php include ('../../../pages-defaults/header.php'); ?>
<script>
  document.title = "Prada";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
  
<!-- <link href="../../../../media/LP/src/css/2020/shoppingadistance.css" rel="stylesheet" type="text/css"> -->
  
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance.min.v03.css" rel="stylesheet" type="text/css">
<div class="luxe-augmente template-marque" data-bg-color="">
  <section class="luxe-augmente-hero" style="background-color: #000000;">
    <div class="hero-bg-image" style="background-image:url('https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-hero-ss21.jpg'); background-position:center top;">&nbsp;</div>
    <div class="container">
      <div class="luxe-row">
        <div class="luxe-col-mobile-12">
          <h1 class="is-uppercase">Prada</h1>
          <a href="#collections" class="button inverted hide-small has-smoothscroll">Découvrir les collections</a>
        </div>
      </div>
    </div>
  </section>
  
  <section class="luxe-augmente-body">
    <div class="container">
      <!-- Fil d'Ariane -->
      <nav class="breadcrumb" aria-label="Fil d'Ariane">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com">
              <span itemprop="name">Accueil</span>
            </a>
            <meta itemprop="position" content="1">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/h/createurs">
              <span itemprop="name">Luxe & Créateurs</span>
            </a>
            <meta itemprop="position" content="2">
          </li>
          <li  class="is-active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/prada" aria-current="page">
              <span itemprop="name">Prada</span>
            </a>
            <meta itemprop="position" content="3">
          </li>
        </ul>
      </nav>
        
      <!-- INTRO -->
      <article class="intro luxe-row  luxe-center-mobile">
        <div class="luxe-col-tablet-7 luxe-col-desktop-5">
          <h3 class="is-uppercase">La maison Prada</h3>
          <div class="intro-txt">
            <p>L’histoire de Prada commence en 1913 lorsque Mario Prada, le grand-père de Miuccia Prada, ouvre la première boutique dans la prestigieuse Galleria Vittorio  
              <span class="read-more"><span class="show">&hellip;</span> <span class="read-more-toggle">Lire la suite</span>
                <span class="read-more-body"> Emanuele II à Milan. Tel un miroir de la société, la marque évolue, réinvente les codes esthétiques, réinterprète la réalité à travers des perspectives inhabituelles. Une approche innovante qui aboutit à des créations incitant l’expression de soi, libérée de toute contrainte.</span>
              </span>
            </p>
            
          </div>
        </div>
      </article>
      
      <div  id="collections"></div>
      
      <!-- ARTICLE 1 -->
      <article class="luxe-row no-gutter luxe-middle-mobile animated-article article1">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
          <div class="article-body">
            <h2>La mode femme en Shopping à Distance</h2>
            <p>Avec le Shopping à Distance, découvrez les collections Prada prêt-à-porter des Galeries Lafayette Paris Haussmann et effectuez vos achats où que vous soyez, en live shopping vidéo avec un conseiller de la marque ou en renseignant votre wishlist avec les pièces dont vous avez envie.</p>
            <a href="" class="button outlined trigger_liveshopping" onclick="clickedBrand='Prada'; appointedWidgetId='5f8878d26a0d547bea7db8f8'; GL_GIS.LiveShopping.checkClerkAvailability('brand15'); return false;">Acheter en live vidéo<span class="icon"><i class="microphone"></i></span></a>
            <a href="" class="button outlined trigger_liveshopping" onclick="clickedBrand='Prada';" data-reveal-id="popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
            <br /><br />
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">En savoir plus</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-mode-femme-ss21.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-mode-femme-ss21@2x.jpg 2x" 
                alt="Les collections Prada mode femme en Shopping à Distance - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
        
      <!-- ARTICLE 2 -->
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse animated-article article2">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
          <div class="article-body">
            <h2>La mode homme en Shopping à Distance</h2>
            <p>Avec le Shopping à Distance, découvrez les collections Prada prêt-à-porter des Galeries Lafayette Paris Haussmann et effectuez vos achats où que vous soyez, en live shopping vidéo avec un conseiller de la marque ou en renseignant votre wishlist avec les pièces dont vous avez envie.</p>
            <a href="" class="button outlined trigger_liveshopping" onclick="clickedBrand='Prada'; appointedWidgetId='5f8878d26a0d547bea7db8f8'; GL_GIS.LiveShopping.checkClerkAvailability('brand15'); return false;">Acheter en live vidéo<span class="icon"><i class="microphone"></i></span></a>
            <a href="" class="button outlined trigger_liveshopping" onclick="clickedBrand='Prada';" data-reveal-id="popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
            <br /><br />
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">En savoir plus</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-mode-homme-ss21.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-mode-homme-ss21@2x.jpg 2x" 
                alt="Les collections Prada mode homme en Shopping à Distance - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      <!-- ARTICLE 3 -->
      <article class="luxe-row no-gutter luxe-middle-mobile text-left animated-article article3">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <h2>Les lunettes en ligne</h2>
            <p>Pionnier dans le concept des lunettes comme véritable accessoire mode, Prada crée des montures à l’élégance moderne et avant-gardiste. Dès 2003, le Groupe s’allie avec Luxottica, leader mondial dans le domaine, pour donner naissance à des collections emblématiques telles que Prada Minimal Baroque et Prada Swing.</p>
            <a href="https://www.galerieslafayette.com/b/prada/f/lunettes+aviateur/lunettes+papillon/lunettes+arrondies/lunettes+rectangulaires" class="button outlined is-marginless">Découvrir la sélection</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-lunettes-ss21.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-lunettes-ss21@2x.jpg 2x" 
                alt="Les lunettes Prada en ligne - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      
      <!-- SUR LE PODIUM -->
      <article class="podium">
        <div class="luxe-row no-gutter text-center">
          <div class="luxe-col-mobile-12 has-text-fatt">
            <h3 class="is-uppercase">Sur le Podium</h3>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center podium-items" style="background-color: #f0f0f0;">
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/lunettes+de+soleil+conceptual+pr+02vs-prada/300405865872/278" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-lunettes-conceptual-pr02vs.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-lunettes-conceptual-pr02vs@2x.jpg 2x" 
                    alt="LUNETTES DE SOLEIL CONCEPTUAL PR 02VS - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>LUNETTES DE SOLEIL <br />CONCEPTUAL PR 02VS</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/lunettes+de+soleil+conceptual+pr+01vs-prada/300405865856/320" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-lunettes-conceptual-pr01vs.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-lunettes-conceptual-pr01vs@2x.jpg 2x" 
                    alt="LUNETTES DE SOLEIL CONCEPTUAL PR 01VS - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>LUNETTES DE SOLEIL <br />CONCEPTUAL PR 01VS</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/lunettes+de+soleil+pr+17us-prada/300405248599/320" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-lunettes-pr17us.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-lunettes-pr17us@2x.jpg 2x" 
                    alt="LUNETTES DE SOLEIL PR 17US - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>LUNETTES DE SOLEIL <br />PR 17US</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/lunettes+de+soleil+millennials+pr+12vs-prada/300406119428/286" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-lunettes-millenials-pr12vs.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-lunettes-millenials-pr12vs@2x.jpg 2x" 
                    alt="LUNETTES DE SOLEIL MILLENNIALS PR 12VS - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>LUNETTES DE SOLEIL <br />MILLENNIALS PR 12VS</p></div>
            </a>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center">
          <div class="luxe-col-mobile-12">
            <a href="https://www.galerieslafayette.com/b/prada/f/lunettes+aviateur/lunettes+papillon/lunettes+arrondies/lunettes+rectangulaires" class="podium-cta"><span class="plus"></span>Voir tous les articles</a>
          </div>
        </div>
      </article>
      
      <!-- ARTICLE 4 -->
      <article class="luxe-row no-gutter luxe-middle-mobile text-left animated-article article4">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <h2>Les parfums en ligne</h2>
            <p>Sous licence et direction créative de Miuccia Prada, la marque a lancé au fil des années une série de parfums qui restent à ce jour des incontournables. À l'image de sa mode, chacune des fragrances se caractérise par une élégance incomparable, avant-gardiste, rendant ses compositions intemporelles.</p>
            <a href="https://www.galerieslafayette.com/b/prada/f/parfum" class="button outlined is-marginless">Découvrir la sélection</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-parfums.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-parfums@2x.jpg 2x" 
                alt="Les parfums Prada en ligne - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      
      <!-- SUR LE PODIUM -->
      <article class="podium">
        <div class="luxe-row no-gutter text-center">
          <div class="luxe-col-mobile-12 has-text-fatt">
            <h3 class="is-uppercase">Sur le Podium</h3>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center podium-items" style="background-color: #f0f0f0;">
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/prada+luna+rossa+carbon+eau+de+toilette-prada/44014996/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-parfums-luna-rossa-carbon.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-parfums-luna-rossa-carbon@2x.jpg 2x" 
                    alt="PRADA LUNA ROSSA CARBON Eau de toilette - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>PRADA LUNA ROSSA CARBON <br />Eau de toilette</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/prada+l+homme-prada/39027155/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-parfums-l-homme.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-parfums-l-homme@2x.jpg 2x" 
                    alt="PRADA L'HOMME - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>PRADA L'HOMME<br /></p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/prada+la+femme-prada/39027142/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-parfums-la-femme.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-parfums-la-femme@2x.jpg 2x" 
                    alt="PRADA LA FEMME - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>PRADA LA FEMME<br /></p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/eau+de+parfum+prada+candy+kiss-prada/36836155/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-parfums-candy-kiss.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/prada/prada-parfums-candy-kiss@2x.jpg 2x" 
                    alt="PRADA CANDY KISS Eau de parfum - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>PRADA CANDY KISS <br />Eau de parfum</p></div>
            </a>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center">
          <div class="luxe-col-mobile-12">
            <a href="https://www.galerieslafayette.com/b/prada/f/parfum" class="podium-cta"><span class="plus"></span>Voir tous les articles</a>
          </div>
        </div>
      </article>
      
      
      <div class="luxe-row conditions">
        <div class="luxe-col-mobile-12">
          <hr class="is-marginless" />
          <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service#conditions" class="is-text">Conditions générales du Shopping à Distance</a>
        </div>
      </div>
      
    </div>
  </section>
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-mix.min.v02.js"></script> 
<div id="goinstore_modals"></div>
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance-goinstore.min.v02.css" rel="stylesheet" type="text/css">
<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-goinstore.min.v07.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->
  
<!-- build:js /media/LP/src/js/2020/shoppingadistance-goinstore.min.v03.js
  <script src="../../../src/js/2020/shoppingadistance-goinstore.js"></script>
endbuild -->

<!-- build:js /media/LP/src/js/2020/shoppingadistance-mix.min.v02.js 
<script src="../../../../assets/js/ScrollMagic.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
<script src="../../../src/js/2020/shoppingadistance-mix.js"></script>
endbuild -->
  
<?php include ('../../../pages-defaults/footer.php'); ?>
