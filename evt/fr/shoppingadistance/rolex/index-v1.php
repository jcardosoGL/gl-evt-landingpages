<?php include ('../../../pages-defaults/header.php'); ?>
<script>
  document.title = "Rolex";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../../../media/LP/src/css/2020/shoppingadistance.css" rel="stylesheet" type="text/css"> -->
  
<!-- =========================== LANDING PAGE ========================== -->
<script src="//assets.adobedtm.com/7e3b3fa0902e/7ba12da1470f/launch-5de25e657d80.min.js" async></script>  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance.min.v03b.css" rel="stylesheet" type="text/css">
<style>
  .luxe-augmente-hero .hero-bg-image {
    opacity: 1;
  }
</style>
<div class="luxe-augmente template-spe" data-bg-color="">
  
  <section class="luxe-augmente-hero" style="background-color: #000000;">
    <div class="hero-bg-image" style="background-image:url('https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/rolex/rolex-hero-v2.jpg'); background-position:center center;">&nbsp;</div>
    <div class="container">
      
    </div>
  </section>
  
  <section class="luxe-augmente-body">
    <div class="container">
      <!-- Fil d'Ariane -->
      <nav class="breadcrumb" aria-label="Fil d'Ariane">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com">
              <span itemprop="name">Accueil</span>
            </a>
            <meta itemprop="position" content="1">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/h/createurs">
              <span itemprop="name">Luxe & Créateurs</span>
            </a>
            <meta itemprop="position" content="2">
          </li>
          <li  class="is-active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex" aria-current="page">
              <span itemprop="name">Rolex</span>
            </a>
            <meta itemprop="position" content="3">
          </li>
        </ul>
      </nav>
        
      <!-- INTRO -->
      <article class="intro luxe-row  luxe-center-mobile">
        <div class="luxe-col-tablet-7 luxe-col-desktop-5 luxe-col-desktop-offset-3">
          <h1 class="is-uppercase">Rolex</h1>
          <div class="intro-txt">
            <p>Les montres Rolex sont réalisées à partir des meilleurs matériaux et sont assemblées avec une minutieuse attention au détail. Chaque composant est conçu 
              <span class="read-more"><span class="show">&hellip;</span> <span class="read-more-toggle">Lire la suite</span>
                <span class="read-more-body"> et produit selon les normes les plus rigoureuses.</span>
              </span>
            </p>
            
          </div>
        </div>
        <div class="luxe-col-tablet-7 luxe-col-desktop-2 luxe-col-desktop-offset-1">
          <a class="retailer" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex/corner" style="display:block">
            <div class="rolex-retailer-clock"></div>
            </a>
            <script src="https://static.rolex.com/retailers/clock/retailercall.js"></script>
            <script>
            var rdp = new RolexRetailerClock();
            var rdpConfig = {
            dealerAPIKey: '09bf86c7a6b216244c782507e41b374a',
            lang: 'fr',
            colour: 'gold'
            };
            try {
            rdp.getRetailerClock(rdpConfig);
            } catch (err) {}
            </script>
        </div>
      </article>

      
      <!-- SUR LE PODIUM -->
      <article class="podium">
        <div class="luxe-row luxe-middle-mobile text-center podium-items">
          <div class="luxe-col-mobile-6 luxe-col-tablet-4" style="background-color: #f0f0f0;">
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex/corner?cmplp=rolex-watches&cmpfa=datejust" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/rolex/rolex-datejust.jpg" alt="Rolex Datejust - Galeries Lafayette" width="414" height="496">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>Datejust</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-4" style="background-color: #f0f0f0;">
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex/corner?cmplp=rolex-watches&cmpfa=gmt-master-ii" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/rolex/rolex-gmt-master-II.jpg" alt="Rolex GMT-Master-II - Galeries Lafayette" width="414" height="496">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>GMT-Master-II</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-4" style="background-color: #f0f0f0;">
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex/corner?cmplp=rolex-watches&cmpfa=day-date" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/rolex/rolex-day-date.jpg" alt="Rolex Day-Date - Galeries Lafayette" width="414" height="496">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>Day-Date</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-4" style="background-color: #f0f0f0;">
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex/corner?cmplp=rolex-watches&cmpfa=submariner" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/rolex/rolex-submariner.jpg" alt="Rolex Submariner - Galeries Lafayette" width="414" height="496">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>Submariner</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-4" style="background-color: #f0f0f0;">
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex/corner?cmplp=rolex-watches&cmpfa=oyster-perpetual" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/rolex/rolex-oyster-perpetual.jpg" alt="Rolex Oyster-Perpetual - Galeries Lafayette" width="414" height="496">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>Oyster-Perpetual</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-4" style="background-color: #f0f0f0;">
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex/corner?cmplp=rolex-watches&cmpfa=cosmograph-daytona" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/rolex/rolex-cosmograph-daytona.jpg" alt="Rolex Cosmograph-Daytona - Galeries Lafayette" width="414" height="496">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>Cosmograph-Daytona</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-4" style="background-color: #f0f0f0;">
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex/corner?cmplp=rolex-watches&cmpfa=yacht-master" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/rolex/rolex-yacht-master.jpg" alt="Rolex Yacht-Master - Galeries Lafayette" width="414" height="496">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>Yacht-Master</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-4" style="background-color: #f0f0f0;">
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex/corner?cmplp=rolex-watches&cmpfa=lady-datejust#" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/rolex/rolex-lady-datejust.jpg" alt="Rolex Lady-Datejust - Galeries Lafayette" width="414" height="496">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>Lady-Datejust</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-4" style="background-color: #f0f0f0;">
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex/corner?cmplp=rolex-watches&cmpfa=sea-dweller" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/rolex/rolex-sea-dweller.jpg" alt="Rolex Sea-Dweller - Galeries Lafayette" width="414" height="496">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>Sea-Dweller</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-4" style="background-color: #f0f0f0;">
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex/corner?cmplp=rolex-watches&cmpfa=explorer" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/rolex/rolex-explorer.jpg" alt="Rolex Explorer - Galeries Lafayette" width="414" height="496">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>Explorer</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-4" style="background-color: #f0f0f0;">
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex/corner?cmplp=rolex-watches&cmpfa=sky-dweller" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/rolex/rolex-sky-dweller.jpg" alt="Rolex Sky-Dweller - Galeries Lafayette" width="414" height="496">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>Sky-Dweller</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-4" style="background-color: #f0f0f0;">
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex/corner?cmplp=rolex-watches&cmpfa=milgauss" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/rolex/rolex-milgauss.jpg" alt="Rolex Milgauss - Galeries Lafayette" width="414" height="496">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>Milgauss</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-4" style="background-color: #f0f0f0;">
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex/corner?cmplp=rolex-watches&cmpfa=cellini" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/rolex/rolex-cellini.jpg" alt="Rolex Cellini - Galeries Lafayette" width="414" height="496">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>Cellini</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-4" style="background-color: #f0f0f0;">
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex/corner?cmplp=rolex-watches&cmpfa=pearlmaster" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/rolex/rolex-pearlmaster.jpg" alt="Rolex Pearlmaster - Galeries Lafayette" width="414" height="496">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>Pearlmaster</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-4" style="background-color: #f0f0f0;">
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex/corner?cmplp=rolex-watches&cmpfa=air-king" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/rolex/rolex-air-king.jpg" alt="Rolex Air-king - Galeries Lafayette" width="414" height="496">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>Air-king</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 is-hidden-tablet"></div>
        </div>
      </article>
    </div>
  </section>
      
  <!-- ARTICLE -->
  <!-- colonnes inversées avec la class luxe-reverse -->
  <section class="luxe-augmente-body">
    <div class="container">
      <article class="txt-article luxe-row no-gutter luxe-middle-mobile luxe-reverse text-left animated-article">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <h2>Rolex aux Galeries<br /> Lafayette Paris<br /> Haussmann</h2>
            <p>Les Galeries Lafayette Haussmann ont l’honneur de faire partie du réseau mondial de détaillants officiels Rolex autorisés à vendre et à entretenir des montres Rolex. Grâce à nos compétences, notre savoir-faire technique et un équipement spécifique, nous garantissons l’authenticité de chacune des pièces de votre Rolex. Parcourez la collection Rolex ci-dessus ou contactez-nous pour prendre rendez-vous avec le personnel spécialisé qui vous aidera à choisir une montre qui durera toute votre vie.</p>
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex/corner?cmplp=contact" class="button primary">Contacter un conseiller Rolex</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/rolex/rolex-galeries-lafayette.jpg"
                alt="Rolex aux Galeries Lafayette Paris Haussmann - Galeries Lafayette" width="560" height="599" style="object-position: right;">
            </figure>
          </div>
        </div>
      </article>
    </div>
  </section>
  
</div>

<script>
  window.coBrandVersion = "Hybrid"
</script>
<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-spe.min.v01.js"></script>
<script type="text/javascript">_satellite.pageBottom();</script>
<!--=========================== FIN LANDING PAGE ========================-->
  
<!-- build:js /media/LP/src/js/2020/shoppingadistance-spe.min.v01.js
  <script src="../../../../assets/js/ScrollMagic.min.js"></script>
  <script src="../../../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
  <script src="../../../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
  <script src="../../../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
  <script src="../../../src/js/2020/shoppingadistance-spe.js"></script>
<!--  endbuild -->
  
<?php include ('../../../pages-defaults/footer.php'); ?>
