<?php include ('../../pages-defaults/header.php'); ?>
<script>
  document.title = "Index Luxe";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
  
<!-- <link href="../../../media/LP/src/css/2020/shoppingadistance-index.css" rel="stylesheet" type="text/css" /> -->
  
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2022/shoppingadistance-index.min.v03.css" rel="stylesheet" type="text/css">
<style>
  .main-nav.ab684263--inline--css--header,
  .ab684263--first-banner {
    margin-top: 0 !important;
  }
  .ab684263 {
    display: none !important; 
  }
  body .header-unified ul.index-nav {
    z-index: 10000;
    position: fixed;
  }
  ul.index-nav {
    margin-left: 0;
  }
  @media screen and (min-width: 1024px) {
    .index-nav.is-fixed-top {
        top: 100px;
    }
  }
</style>
<div class="luxe-augmente index">
  <section class="luxe-augmente-hero" >
    <div class="container">
      <div class="luxe-row no-gutter">
        <div class="luxe-col-mobile-12">
          <h1>Nos marques Luxe & Créateurs</h1>
        </div>
        <div class="luxe-col-mobile-12">
          <ul class="index-nav" data-scroll-header>
            <li><a href="#a" class="has-smoothscroll">A</a></li>
            <li><a href="#b" class="has-smoothscroll">B</a></li>
            <li><a href="#c" class="has-smoothscroll">C</a></li>
            <li><a href="#d" class="has-smoothscroll">D</a></li>
            <li><a href="#e" class="has-smoothscroll">E</a></li>
            <li><a href="#f" class="has-smoothscroll">F</a></li>
            <li><a href="#g" class="has-smoothscroll">G</a></li>
            <li><a href="#h" class="has-smoothscroll">H</a></li>
            <li><a href="#i" class="has-smoothscroll">I</a></li>
            <li><a href="#j" class="has-smoothscroll">J</a></li>
            <li><a href="#k" class="has-smoothscroll">K</a></li>
            <li><a href="#l" class="has-smoothscroll">L</a></li>
            <li><a href="#m" class="has-smoothscroll">M</a></li>
            <li><a href="#n" class="has-smoothscroll">N</a></li>
            <li><a href="#o" class="has-smoothscroll">O</a></li>
            <li><a href="#p" class="has-smoothscroll">P</a></li>
            <li><a href="#q" class="has-smoothscroll">Q</a></li>
            <li><a href="#r" class="has-smoothscroll">R</a></li>
            <li><a href="#s" class="has-smoothscroll">S</a></li>
            <li><a href="#t" class="has-smoothscroll">T</a></li>
            <li class=" is-hidden"><a href="#u" class="has-smoothscroll">U</a></li>
            <li><a href="#v" class="has-smoothscroll">V</a></li>
            <li><a href="#w" class="has-smoothscroll">W</a></li>
            <li class="is-hidden"><a href="#x" class="has-smoothscroll">X</a></li>
            <li><a href="#y" class="has-smoothscroll">Y</a></li>
            <li><a href="#z" class="has-smoothscroll">Z</a></li>
            <li class="is-hidden"><a href="#autres" class="has-smoothscroll">#</a></li>
          </ul>
          <span class="sticky-spacer"></span>
        </div>
      </div>
    </div>
  </section>
  
  <section class="luxe-augmente-body">
    <div class="container">
      <div class="luxe-row">
        <div class="luxe-col-mobile-12 luxe-col-tablet-4 is-hidden">
          <div class="top-brand">
            <div class="luxe-row" style="background-color: #b2040e;">
              <div class="luxe-col-mobile-6 luxe-col-tablet-10 luxe-col-tablet-offset-2">
                <div class="top-brand-image">
                  <figure class="image">
                    <div class="hide-xmedium" style="height: 100%;">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/shoppingadistance/index/top-brand-balenciaga_M.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/shoppingadistance/index/top-brand-balenciaga_M@2x.jpg 2x" 
                        alt="Nouvelle Collection Bottega Veneta - Galeries Lafayette" width="181" height="185">
                    </div>
                    <div class="hide-small hide-medium">
                      <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/shoppingadistance/index/top-brand-balenciaga_D.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/shoppingadistance/index/top-brand-balenciaga_D@2x.jpg 2x" 
                        alt="Nouvelle Collection Bottega Veneta - Galeries Lafayette" width="344" height="440">
                    </div>
                  </figure>
                </div>
              </div>
              <div class="luxe-col-mobile-6 luxe-col-tablet-10 luxe-col-tablet-offset-2">
                <div class="top-brand-text">
                  <p class="boxed-text">
                    <span>Balenciaga : </span>
                    <span>La nouvelle</span>
                    <span>collection est ici</span>
                  </p>
                  <a href="https://www.galerieslafayette.com/b/balenciaga/ct/femme" class="button primary hide-small hide-medium">DÉCOUVRIR</a>
                  <div class="cta-mobile hide-xmedium">DÉCOUVRIR<div class="icon">
                      <svg class="icon-arrow-up-linear" viewBox="0 0 32 32">
                        <title>arrow-up-linear</title>
                        <path d="M15.999 23.707v-13.792l-3.146 3.146-0.707-0.707 4.353-4.354 4.354 4.354-0.708 0.707-3.145-3.146v13.792z"></path>
                        </svg>
                    </div>
                  </div>
                </div>
              </div>
              <a href="https://www.galerieslafayette.com/b/balenciaga/ct/femme" class="global_link"></a>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-8 luxe-col-tablet-offset-2">
          <div class="brands-container">
            <div class="luxe-row letter-section" id="a">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">a</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/a+p+c+">A.P.C.</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/aape+by+a+bathing+ape">AAPE by A Bathing Ape</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/acheval-pampa">Acheval Pampa</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/acne-studios">Acné Studios</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/adieu">Adieu</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/aeyde">AEYDE</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/alexander+mcqueen">Alexander McQueen</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/alighieri">ALIGHIERI</a></li>
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/ambush">Ambush</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/amelie+pichard">Amélie Pichard</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/ami-paris">AMI Paris</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/amiri">Amiri</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/ancient+greek+sandals">Ancient Greek Sandals</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/anine-bing">Anine Bing</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/anna-october">Anna October</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/aries">Aries</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/arizona+love">Arizona Love</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/aurelie+bidermann">Aurélie Bidermann</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/avnier">AVNIER</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/axel+arigato">Axel Arigato</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="b">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">b</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/balenciaga">Balenciaga</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/balmain">Balmain</a></li>
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/berluti">Berluti</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/bettina-vermillon">Bettina Vermillon</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/bohemian+rhapsodie">Bohemian Rhapsodie</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/bottega+veneta">Bottega Veneta</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/burberry">Burberry</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/by+far">By Far</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/by+malene+birger">By Malene Birger</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="c">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">c</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/carel">Carel</a></li>
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/cartier">Cartier</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/casablanca">Casablanca</a></li>
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/cecilie-bahnsen">Cecilie Bahnsen</a></li>
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/celine">Celine</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/cesta+collective">Cesta Collective</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/chaumet">Chaumet</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/chloe">Chloé</a></li>
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/chopard">Chopard</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/clergerie">Clergerie</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/co">Co</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/comme+des+garcons+play">Comme des Garçons PLAY</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/comme+des+garcons+wallets">Comme des Garçons WALLETS</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/coperni">Coperni</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/courreges">Courrèges</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/craig-green">Craig Green</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="d">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">d</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/danse+lente">DANSE LENTE</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/de+beers+jewellers">De Beers Jewellers</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/demellier">Demellier</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/designers+remix">Designers Remix</a></li>
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/dior">Dior</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/djula">Djula</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/dodo+jewels"> DoDo Jewels</a></li>
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/dolce-gabbana">Dolce & Gabbana</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/dsquared2">Dsquared2</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/dries+van+noten">DRIES VAN NOTEN</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="e">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">e</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/elleme">Elleme</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/eres">Eres</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/erl">ERL</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/ester-manas">Ester Manas</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/etudes">Etudes</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/eytys">Eytys</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="f">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">f</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/faure-lepage">Fauré Le Page</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/fabrizio-viti">Fabrizio Viti</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/fear+of+god">Fear of God</a></li>
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/fendi">Fendi</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/filling+pieces">Filling Pieces</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/forte-forte">Forte_Forte</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/frame">Frame</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/fred">Fred</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="g">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">g</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/ganni">Ganni</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/gestuz">Gestuz</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/gianvito+rossi">Gianvito Rossi</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/givenchy">Givenchy</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/golden+goose">Golden Goose</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/goossens">Goossens</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="h">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">h</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/heron+preston">Heron Preston</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/hogan">Hogan</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/holzweiler">Holzweiler</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/homecore">Homecore</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/homme+plisse">Homme Plissé</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/house+of+dagmar">House of Dagmar</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="i">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">i</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/iindaco">Iindaco</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/isabel+marant">Isabel Marant</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/isabel+marant+etoile">Isabel Marant Etoile</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="j">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">j</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/jacquemus">Jacquemus</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/jem">JEM</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/jerome-dreyfuss">Jérôme Dreyfuss</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/jil+sander">Jil Sander</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/jimmy+choo">Jimmy Choo</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/j+m+weston">J.M. WESTON</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/justine+clenquet">Justine Clenquet</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/j+w+anderson">JW Anderson</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="k">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">k</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/kassl-editions">Kassl Editions</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/kenzo">Kenzo</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/kitri">Kitri</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/khrisjoy">Khrisjoy</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="l">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">l</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/la+brune+la+blonde">La Brune & La Blonde</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/la+perla">La Perla</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/lanvin">LANVIN</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/laurence+bras">Laurence Bras</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/l-autre-chose">L'Autre Chose</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/lemaire">Lemaire</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/letrange">Létrange</a></li>
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/loewe">Loewe</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/loulou+studio">Loulou Studio</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/l-uniform">L/UNIFORM</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="m">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">m</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/mach-mach">Mach & Mach</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/magali-pascal">Magali Pascal</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/maison+kitsune">Maison Kitsuné</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/maison-margiela">Maison Margiela</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/maison+michel">Maison Michel</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/marcelo+burlon">Marcelo Burlon</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/maria+luca">Maria Luca</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/marine+serre">Marine Serre</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/marni">Marni</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/martin-martin">Martin Martin</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/mcm">MCM</a></li>
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/messika">Messika</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/michel-vivien">Michel Vivien</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/mi-mai">MI/MAI</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/miu+miu">Miu Miu</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/mm6+maison+margiela">MM6 Maison Margiela</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/moea">Moea</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/moschino+couture">Moschino Couture</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/mother">Mother</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/mother-of-pearl">Mother of Pearl</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/mugler">Mugler</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="n">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">n</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/nanushka">Nanushka</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/neous">Neous</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/nina-ricci">Nina Ricci</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/nodaleto">Nodaleto</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="o">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">o</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/off+white">Off White</a></li>
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/orris-london">ORRIS LONDON</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="p">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">p</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/paco+rabanne">Paco Rabanne</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/palm+angels">Palm Angels</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/paladini">Paladini</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/paloma+stella">Paloma Stella</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/paris+texas">Paris Texas</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/patou">Patou</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/persee">Persee</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/pierre+hardy">Pierre Hardy</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/poiray">Poiray</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/pomellato">Pomellato</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/prada">Prada</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/proenza+schouler">Proenza Schouler</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="q">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">q</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/qeelin">Qeelin</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="r">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">r</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/raf+simons">Raf Simons</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/reception">Reception</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/rejina+pyo">Rejina Pyo</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/rochas">Rochas</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/rodebjer">Rodebjer</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/roger+vivier">Roger Vivier</a></li>
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rolex">Rolex</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/rombaut">Rombaut</a></li>
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/rosie-assoulin">Rosie Assoulin</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="s">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">s</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/saint-laurent">Saint Laurent</a></li>
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/salvatore-ferragamo">Salvatore Ferragamo</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/sarlane">Sarlane</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/sea-new-york">Sea New York</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/shourouk">SHOUROUK</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/souliers-martinez">Souliers Martinez</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/staud">Staud</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/stella+mccartney">Stella McCartney</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/stella+nova">Stella Nova</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/stella-pardo">Stella Pardo</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/stine+goya">Stine Goya</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/stone+island">Stone Island</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/stuart-weitzman">Stuart Weitzman</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/studio+max+mara">Studio Max Mara</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/studio-reco">Studio Reco</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/+s+max+mara">'S Max Mara</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="t">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">t</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/t-by-alexander-wang">T by Alexander Wang</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/the+attico">The Attico</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/the-frankie-shop">The Frankie Shop</a></li>
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/tiffany">Tiffany & Co.</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/tod+s">Tod's</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/toteme">Toteme</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section is-hidden" id="u">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">u</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li class="is-hidden"><a href="https://www.galerieslafayette.com/b/uniforme">Uniforme</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="v">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">v</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/valentino">Valentino</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/valentino+garavani">Valentino Garavani</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/valextra">Valextra</a></li>
                  <li><a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/van-cleef-arpels">Van Cleef & Arpels</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/vanrycke">Vanrycke</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/versace">Versace</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/vince">Vince</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="w">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">w</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/wandler">Wandler</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/weekend+max+mara">Weekend Max Mara</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section is-hidden" id="x">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">x</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href=""></a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="y">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">y</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/y+project">Y/Project</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/y-3">Y-3</a></li>
                  <li><a href="https://www.galerieslafayette.com/b/youyou">Youyou</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section" id="z">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">z</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href="https://www.galerieslafayette.com/b/zoe+chicco">Zoe Chicco</a></li>
                </ul>
              </div>
            </div>
            
            <div class="luxe-row letter-section is-hidden" id="autres">
              <div class="luxe-col-mobile-12">
                <span class="index-letter">#</span>
              </div>
              <div class="luxe-col-mobile-12">
                <ul class="brands">
                  <li><a href=""></a></li>
                </ul>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </section>
  
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-index.min.v02.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->
  
<!-- build:js /media/LP/src/js/2020/shoppingadistance-index.min.v01.js
<script src="../../../assets/js/gumshoe-scrollspy.polyfills.min.js"></script>  
<script src="../../../assets/js/ScrollMagic.min.js"></script>
<script src="../../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
<script src="../../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
<script src="../../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
<script src="../../src/js/2020/shoppingadistance-index.js"></script>
endbuild -->

<?php include ('../../pages-defaults/footer.php'); ?>
