<?php include ('../../../pages-defaults/header.php'); ?>
<script>
  document.title = "Dolce&Gabbana";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
<!-- <link href="../../../../media/LP/src/css/2020/shoppingadistance.css" rel="stylesheet" type="text/css"> -->
  
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance.min.v05.css" rel="stylesheet" type="text/css">
<style>
  .back-top {
    display: none !important;
  }
  .gis-cta-logo {
    height: 65px !important;
    width: 65px !important; 
  }
  .gis-cta-indicator {
    height: 15px !important;
    width: 15px !important;
  }
  @media screen and (max-width: 1024px) {
    .gis-cta-logo {
      height: 60px !important;
      width: 60px !important; 
    }
  }
</style>
<div class="luxe-augmente template-marque" data-bg-color="">
  <section class="luxe-augmente-hero" style="background-color: #000000;">
    <div class="hero-bg-image" style="background-image:url('https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-hero.jpg'); background-position:center top;">&nbsp;</div>
    <div class="container">
      <div class="luxe-row">
        <div class="luxe-col-mobile-12">
          <h1>Dolce&Gabbana</h1>
          <a href="#collections" class="button primary has-arrow-down has-smoothscroll">Découvrir les collections</a>
        </div>
      </div>
    </div>
  </section>
  
  <section class="luxe-augmente-body">
    <div class="container">
      <!-- Fil d'Ariane -->
      <nav class="breadcrumb" aria-label="Fil d'Ariane">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com">
              <span itemprop="name">Accueil</span>
            </a>
            <meta itemprop="position" content="1">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/h/createurs">
              <span itemprop="name">Luxe & Créateurs</span>
            </a>
            <meta itemprop="position" content="2">
          </li>
          <li  class="is-active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/dolce-gabbana" aria-current="page">
              <span itemprop="name">Dolce&Gabbana</span>
            </a>
            <meta itemprop="position" content="3">
          </li>
        </ul>
      </nav>
        
      <!-- INTRO -->
      <article class="intro luxe-row  luxe-center-mobile">
        <div class="luxe-col-tablet-7 luxe-col-desktop-5 luxe-col-fullhd-4">
          <h3 class="is-uppercase">La maison Dolce&Gabbana</h3>
          <div class="intro-txt">
            <p>Fondée en 1985, Dolce&Gabbana figure parmi les leaders internationaux du secteur de la mode et des produits de luxe. Les fondateurs, Domenico Dolce et Stefano&nbsp;
              <span class="read-more"><span class="read-more-toggle">&hellip; suite</span></span>
              <span class="read-more-content is-hidden"> Gabbana, ont toujours été les sources créatives et stylistiques des activités de la marque.<br />
                  Vêtements haut de gamme, maroquinerie, chaussures, accessoires, joaillerie, lunettes ou encore cosmétique, l'univers Dolce&Gabbana gagne à être découvert.&nbsp;
                  <span class="read-less-toggle">Fermer</span>
              </span>
            </p>
          </div>
        </div>
      </article>
      
      <div  id="collections"></div>
      
      <!-- ARTICLE 1 -->
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse text-left animated-article article1">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <h2>K by Dolce&Gabbana<br />
              Eau de Parfum</h2>
            <p>Cette nouvelle fragrance dévoile l'âme cachée d'un roi moderne dont la force est contrebalancée par un cœur toujours bienveillant.</p>
            <a href="https://www.galerieslafayette.com/b/dolce+gabbana/ct/beaute" class="button outlined is-marginless">Découvrir la sélection</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-parfum-main.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-parfum-main@2x.jpg 2x" 
                alt="K by Dolce&Gabbana - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      
      <!-- SUR LE PODIUM -->
      <article class="podium">
        <div class="luxe-row no-gutter text-center">
          <div class="luxe-col-mobile-12 has-text-fatt">
            <h3 class="is-uppercase">Sur le Podium</h3>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center podium-items" style="background-color: #f0f0f0;">
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/k+by+dolce+gabbana+-+eau+de+parfum-dolce+gabbana/72773722/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-k-eau-de-parfum.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-k-eau-de-parfum@2x.jpg 2x" 
                    alt="K by Dolce&Gabbana Eau de Parfum - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>K BY DOLCE&GABBANA <br />Eau de Parfum</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/k+by+dolce+gabbana+-+eau+de+toilette-dolce+gabbana/65364833/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-k-eau-de-toilette.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-k-eau-de-toilette@2x.jpg 2x" 
                    alt="K by Dolce&Gabbana Eau de Toilette - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>K BY DOLCE&GABBANA <br />Eau de Toilette</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/the+only+one+-+eau+de+parfum-dolce+gabbana/57096211/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-the-only-one-parfum.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-the-only-one-parfum@2x.jpg 2x" 
                    alt="Dolce&Gabbana THE ONLY ONE Eau de parfum - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>THE ONLY ONE <br />Eau de Parfum</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/the+only+one+-+eau+de+parfum+intense-dolce+gabbana/70274878/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-the-only-one-parfum-intense.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-the-only-one-parfum-intense@2x.jpg 2x" 
                    alt="Dolce&Gabbana THE ONLY ONE Eau de parfum Intense - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>THE ONLY ONE<br />Eau de Parfum Intense</p></div>
            </a>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center">
          <div class="luxe-col-mobile-12">
            <a href="https://www.galerieslafayette.com/b/dolce+gabbana/ct/beaute" class="podium-cta"><span class="plus"></span>Voir tous les articles</a>
          </div>
        </div>
      </article>
      
      <!-- ARTICLE 2 -->
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse animated-article article2">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
          <div class="article-body">
            <h2>La mode femme et homme en Shopping à Distance</h2>
            <p>Avec le Shopping à Distance, découvrez les collections Dolce&Gabbana femme et homme des Galeries Lafayette Paris Haussmann et effectuez vos achats où que vous soyez, en live shopping vidéo avec un conseiller de la marque ou en renseignant votre wishlist avec les pièces dont vous avez envie. <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">En savoir plus</a></p>
            <a href="" class="button outlined" onclick="initSAD(); return false;">Acheter en live vidéo<span class="icon"><i class="microphone"></i></span></a>
            <a href="" class="button outlined" onclick="initWhislist(); return false;" data-reveal-id="popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-mode-femme-homme.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-mode-femme-homme@2x.jpg 2x" 
                alt="La mode femme et homme Dolce&Gabbana en Shopping à Distance - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
          
      <!-- ARTICLE 3 -->
      <article class="luxe-row no-gutter luxe-middle-mobile animated-article article3">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
          <div class="article-body">
            <h2>La mode enfant en Shopping à Distance</h2>
            <p>Avec le Shopping à Distance, découvrez les collections Dolce&Gabbana enfant des Galeries Lafayette Paris Haussmann et effectuez vos achats où que vous soyez, en live shopping vidéo avec un conseiller de la marque ou en renseignant votre wishlist avec les pièces dont vous avez envie. <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">En savoir plus</a></p>
            <a href="" class="button outlined" onclick="initSAD(); return false;">Acheter en live vidéo<span class="icon"><i class="microphone"></i></span></a>
            <a href="" class="button outlined" onclick="initWhislist(); return false;" data-reveal-id="popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-mode-enfant.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-mode-enfant@2x.jpg 2x" 
                alt="La mode enfant Dolce&Gabbana en Shopping à Distance - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      <!-- ARTICLE 4 -->
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse text-left animated-article article4">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <h2>Les lunettes</h2>
            <p>Découvrez l'univers unique de la marque et retrouvez ici une sélection de must have ainsi que des pièces issues de la collection automne-hiver 2020-2021.</p>
            <a href="https://www.galerieslafayette.com/b/dolce+gabbana/ct/femme" class="button outlined is-marginless">Découvrir la sélection</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-lunettes.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-lunettes@2x.jpg 2x" 
                alt="Les lunettes Dolce&Gabbana - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      
      <!-- SUR LE PODIUM -->
      <article class="podium">
        <div class="luxe-row no-gutter text-center">
          <div class="luxe-col-mobile-12 has-text-fatt">
            <h3 class="is-uppercase">Sur le Podium</h3>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center podium-items" style="background-color: #f0f0f0;">
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/lunettes+de+soleil+dg4270-dolce+gabbana/300403723964/320" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-lunettes-de-soleil-dg4270.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-lunettes-de-soleil-dg4270@2x.jpg 2x" 
                    alt="Dolce&Gabbana LUNETTES DE SOLEIL - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>LUNETTES DE SOLEIL <br />DG4270</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/lunettes+de+soleil+dg4268-dolce+gabbana/300403725425/320" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-lunettes-de-soleil-dg4268.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-lunettes-de-soleil-dg4268@2x.jpg 2x" 
                    alt="Dolce&Gabbana LUNETTES DE SOLEIL - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>LUNETTES DE SOLEIL <br />DG4268</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/lunettes+de+soleil+dg2213-dolce+gabbana/300405866166/320" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-lunettes-de-soleil-dg2213.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-lunettes-de-soleil-dg2213@2x.jpg 2x" 
                    alt="Dolce&Gabbana LUNETTES DE SOLEIL - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>LUNETTES DE SOLEIL <br />DG2213</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/lunettes+de+soleil+dg4354-dolce+gabbana/300406119754/278" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-lunettes-de-soleil-dg4354.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dolce-gabbana/dolce-gabbana-lunettes-de-soleil-dg4354@2x.jpg 2x" 
                    alt="Dolce&Gabbana LUNETTES DE SOLEIL - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>LUNETTES DE SOLEIL <br />DG4354</p></div>
            </a>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center">
          <div class="luxe-col-mobile-12">
            <a href="https://www.galerieslafayette.com/b/dolce+gabbana/ct/femme" class="podium-cta"><span class="plus"></span>Voir tous les articles</a>
          </div>
        </div>
      </article>
      
      
      <div class="luxe-row conditions">
        <div class="luxe-col-mobile-12">
          <hr class="is-marginless" />
          <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service#conditions" class="is-text">Conditions générales du Shopping à Distance</a>
        </div>
      </div>
      
    </div>
  </section>
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-mix.min.v03.js"></script>

<div id="goinstore_modals" style="height:0;"></div>
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/shoppingadistance-goinstore-v2.min.css" rel="stylesheet" type="text/css">
<script src="https://static.galerieslafayette.com/media/LP/src/js/2021/shoppingadistance-goinstore-v2.min.v03.js"></script>

<script>
  var routingKey = "brand20";
  var clickedBrand='Dolce&Gabbana'; 
  var cleanBrandName='dolce-gabbana-v1'; 
  function initWhislist() {
    clickedBrand; 
    cleanBrandName; 
    GL_GIS.LiveShopping.displaySadBrand();
  }
  function initSAD() {
    clickedBrand; 
    cleanBrandName; 
    appointedWidgetId='5f88787a6a0d547bea7db8ee'; 
    GL_GIS.LiveShopping.displaySadBrand();  
    GL_GIS.LiveShopping.checkPopinClerkAvailability(routingKey);
  }

  window.onload = function() {
    GL_GIS.LiveShopping.init(routingKey);    
    
    var gisCTA = document.getElementById('gis-cta');
    if (gisCTA) {
      gisCTA.addEventListener('click', function(e) {
        e.stopPropagation();
        initSAD();
        GL_GIS.LiveShopping.displaySadBrand();
        return false;  
      }, true);
    }
    
    var gisMsgContainer = document.getElementById('gis-awareness-msg-container');
    if (gisMsgContainer) {
      gisMsgContainer.lastChild.addEventListener('click', function(e) { 
        e.stopPropagation();
        initSAD();
        GL_GIS.LiveShopping.displaySadBrand();
        return false;
      }, true);
    }
  }; 
</script>
<!--=========================== FIN LANDING PAGE ========================-->
  
<!-- build:js /media/LP/src/js/2020/shoppingadistance-goinstore.min.v03.js
  <script src="../../../src/js/2020/shoppingadistance-goinstore.js"></script>
endbuild -->

<!-- build:js /media/LP/src/js/2020/shoppingadistance-mix.min.v02.js 
<script src="../../../../assets/js/ScrollMagic.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
<script src="../../../src/js/2020/shoppingadistance-mix.js"></script>
endbuild -->
  
<?php include ('../../../pages-defaults/footer.php'); ?>
