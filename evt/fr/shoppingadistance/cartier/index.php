<?php include ('../../../pages-defaults/header.php'); ?>
<script>
  document.title = "Cartier";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
<!-- <link href="../../../../media/LP/src/css/2020/shoppingadistance.css" rel="stylesheet" type="text/css"> -->
  
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance.min.v05.css" rel="stylesheet" type="text/css">
<style>
  .back-top {
    display: none !important;
  }
  .luxe-augmente-hero .hero-video {
    width: 208%;
    height: 100%;
    margin: 0 auto; 
    left: -54%;
  }
  @media screen and (min-width: 768px) {
    .luxe-augmente-hero .hero-video {
      width: 100%;
      height: 146%;
      left: 0%;
      top: -19%;
    }
  }
  @media screen and (min-width: 1250px) {
    .luxe-augmente-hero .hero-video {
      height: 124%;
      top: -7%;  
    }
  }
</style>
<div class="luxe-augmente template-marque" data-bg-color="">
  <section class="luxe-augmente-hero" style="background-color: #000000;">
    <div class="hero-bg-image" style="background-image:url('https://static.galerieslafayette.com/media/LP/src/img/2022/landing/shoppingadistance/cartier/hero_D.jpg'); background-position:center bottom;">&nbsp;</div>
    <div class="container">
      <div class="luxe-row">
        <div class="luxe-col-mobile-12">
          <h1>Cartier</h1>
          <a href="#collections" class="button primary has-arrow-down has-smoothscroll">Découvrir les collections</a>
        </div>
      </div>
    </div>
  </section>
  
  <section class="luxe-augmente-body">
    <div class="container">
      <!-- Fil d'Ariane -->
      <nav class="breadcrumb" aria-label="Fil d'Ariane">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com">
              <span itemprop="name">Accueil</span>
            </a>
            <meta itemprop="position" content="1">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/h/createurs">
              <span itemprop="name">Luxe & Créateurs</span>
            </a>
            <meta itemprop="position" content="2">
          </li>
          <li  class="is-active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/cartier" aria-current="page">
              <span itemprop="name">Cartier</span>
            </a>
            <meta itemprop="position" content="3">
          </li>
        </ul>
      </nav>
        
      <!-- INTRO -->
      <article class="intro luxe-row  luxe-center-mobile">
        <div class="luxe-col-tablet-7 luxe-col-desktop-5 luxe-col-fullhd-4">
          <h3 class="is-uppercase">La maison Cartier</h3>
          <div class="intro-txt">
            <p>Cartier révèle la beauté partout où elle se trouve. De la joaillerie à la haute joaillerie, de l’horlogerie aux parfums, à la maroquinerie et aux accessoires,&nbsp;
              <span class="read-more"><span class="read-more-toggle">&hellip; suite</span></span>
              <span class="read-more-content is-hidden"> les créations de Cartier symbolisent la rencontre d’un savoir-faire d’exception et d’une signature atemporelle.&nbsp;
                <span class="read-less-toggle">Fermer</span>
              </span>
            </p>
          </div>
        </div>
      </article>
      
      <div  id="collections"></div>
      
      <!-- ARTICLE 1 -->
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse animated-article article1">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
          <div class="article-body">
            <h2>L'Horlogerie en<br /> Shopping à Distance</h2>
            <p>Chez Cartier, tout commence par le dessin. La recherche de la ligne pure préside à la naissance de montres de forme qui constituent le patrimoine horloger de Cartier. Des montres pensées comme des objets de design, réalisés à la Manufacture Cartier de La Chaux-de-Fonds et 
              dans les ateliers de la Maison. <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">En savoir plus</a></p>
            <a href="" class="button outlined" onclick="initWhislist(); return false;" data-reveal-id="blkr-popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/cartier/cartier-horlogerie.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/cartier/cartier-horlogerie@2x.jpg 2x" 
                alt="L'Horlogerie Cartier en Shopping à Distance - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
        
      <!-- ARTICLE 2 -->
      <article class="luxe-row no-gutter luxe-middle-mobile animated-article article2">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
          <div class="article-body">
            <h2>La Joaillerie en<br /> Shopping à Distance</h2>
            <p>Pierres d’exception, savoir-faire virtuose, les collections de joaillerie de Cartier naissent d’une vision singulière, ancrée dans l’histoire de la Maison et sublimée au fil d’une constante évolution. <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">En savoir plus</a></p>
            <a href="" class="button outlined" onclick="initWhislist(); return false;" data-reveal-id="blkr-popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/cartier/cartier-joaillerie.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/cartier/cartier-joaillerie@2x.jpg 2x" 
                alt="La Joaillerie Cartier en Shopping à Distance - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      <!-- ARTICLE 3 -->
      <article class="luxe-row no-gutter luxe-middle-mobile  luxe-reverse text-left animated-article article3">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <h2>Les parfums</h2>
            <p>Pour elle ou pour lui, les parfums de Cartier révèlent une vision olfactive libre, ancrée dans l’histoire de la parfumerie et de l’art du parfum - celle du Parfumeur Maison, Mathilde Laurent.</p>
            <a href="https://www.galerieslafayette.com/b/cartier" class="button outlined is-marginless">Découvrir la sélection</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/cartier/cartier-parfums.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/cartier/cartier-parfums@2x.jpg 2x" 
                alt="Les parfums Cartier - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      
      <!-- SUR LE PODIUM -->
      <article class="podium">
        <div class="luxe-row no-gutter text-center">
          <div class="luxe-col-mobile-12 has-text-fatt">
            <h3 class="is-uppercase">Sur le Podium</h3>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center podium-items" style="background-color: #f0f0f0;">
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/l+envol+-+eau+de+parfum+-+non+rechargeable-cartier/39712733/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/cartier/cartier-parfum-l-envol.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/cartier/cartier-parfum-l-envol@2x.jpg 2x" 
                    alt="Cartier parfums L'envol - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>L'ENVOL <br />Eau de Parfum</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/parfum-cartier/52628490/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/cartier/cartier-parfum-declaration.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/cartier/cartier-parfum-declaration@2x.jpg 2x" 
                    alt="Cartier parfums Déclaration - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>CARTIER <br />Parfum</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/edition+soir+-+eau+de+parfum-cartier/40046866/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/cartier/cartier-parfum-edition-soir.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/cartier/cartier-parfum-edition-soir@2x.jpg 2x" 
                    alt="Cartier parfums Edition soir - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>CARTIER — EDITION SOIR <br />Eau de Parfum</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/eau+de+toilette-cartier/22948406/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/cartier/cartier-parfum-pasha.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/cartier/cartier-parfum-pasha@2x.jpg 2x" 
                    alt="Cartier Eau de Toilette Pasha - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>CARTIER <br />Eau de Toilette</p></div>
            </a>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center">
          <div class="luxe-col-mobile-12">
            <a href="https://www.galerieslafayette.com/b/cartier" class="podium-cta"><span class="plus"></span>Voir tous les articles</a>
          </div>
        </div>
      </article>
      
      
      <!-- ARTICLE 4 -->
        <article class="luxe-row no-gutter luxe-middle-mobile animated-article article4">
          <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
            <div class="article-body">
              <h2>La Maroquinerie en<br /> Shopping à Distance</h2>
              <p>Formes signées, matières nobles, détails précieux – les collections de maroquinerie Cartier célèbrent la richesse de son patrimoine autour d’objets d’allure et de style, la signature Cartier. <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">En savoir plus</a></p>
              <a href="" class="button outlined" onclick="initWhislist(); return false;" data-reveal-id="blkr-popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
            </div>
          </div>
          <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
            <div class="article-image">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/shoppingadistance/cartier/cartier-maroquinerie.jpg"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/shoppingadistance/cartier/cartier-maroquinerie@2x.jpg 2x" 
                  alt="La Maroquinerie Cartier en Shopping à Distance - Galeries Lafayette" width="620" height="620">
              </figure>
            </div>
          </div>
        </article>
          
        <!-- ARTICLE 5 -->
        <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse animated-article article5">
          <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
            <div class="article-body">
              <h2>Les Accessoires en<br /> Shopping à Distance</h2>
              <p>Des objets qui ont du style, c’est ainsi que Cartier définit ses accessoires. Parce qu’ils invitent le beau auprès de soi, ils subliment le quotidien. <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">En savoir plus</a></p>
              <a href="" class="button outlined" onclick="initWhislist(); return false;" data-reveal-id="blkr-popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
            </div>
          </div>
          <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
            <div class="article-image">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/cartier/cartier-accessoires.jpg"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/cartier/cartier-accessoires@2x.jpg 2x" 
                  alt="Les Accessoires Cartier en Shopping à Distance - Galeries Lafayette" width="620" height="620">
              </figure>
            </div>
          </div>
        </article>
      
      
      <div class="luxe-row conditions">
        <div class="luxe-col-mobile-12">
          <hr class="is-marginless" />
          <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service#conditions" class="is-text">Conditions générales du Shopping à Distance</a>
        </div>
      </div>
      
    </div>
  </section>
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-mix.min.v03.js"></script>

<div id="live_modals" style="height:0;"></div>
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/shoppingadistance-goinstore-v2.min.css" rel="stylesheet" type="text/css">
<script src="https://static.galerieslafayette.com/media/LP/src/js/2022/shoppingadistance-bleeker.min.v02.js"></script>

<script>
(function() {
  let scripts = Array
  .from(document.querySelectorAll('script'))
  .map(scr => scr.src);
  const url = new URL(window.location.href);
  var search = url.search;
  var paramsStr = search.substr(1);
  if (!scripts.includes('https://widget.bleeker-st.com/gl/embed-dev.js')) { 
    var scriptNode = document.createElement('script');
    scriptNode['id'] = 'bleeker-script';
    scriptNode['src'] = 'https://widget.bleeker-st.com/gl/embed-dev.js'; scriptNode['type'] = 'text/javascript'; document.documentElement.appendChild(scriptNode);
  }
  window.onload = function() {
    initBleekerLiveShopping({
      params: paramsStr,
      vendorID: 'GL',
      uaGoogle : 'UA-4094055-1',
      widget: true,
      type: 'overlay', 
    });
  } 
})();

var brandID='Cartier';
var brand='Cartier';
var posterName='cartier-ah22';
function initWhislist() {
  brand; 
  posterName;
  GL_BLKR.LiveShopping.displaySadBrand();
}
</script>
<!--=========================== FIN LANDING PAGE ========================-->
  
<!-- build:js /media/LP/src/js/2020/shoppingadistance-goinstore.min.v03.js
  <script src="../../../src/js/2020/shoppingadistance-goinstore.js"></script>
endbuild -->

<!-- build:js /media/LP/src/js/2020/shoppingadistance-mix.min.v02.js 
<script src="../../../../assets/js/ScrollMagic.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
<script src="../../../src/js/2020/shoppingadistance-mix.js"></script>
endbuild -->
  
<?php include ('../../../pages-defaults/footer.php'); ?>
