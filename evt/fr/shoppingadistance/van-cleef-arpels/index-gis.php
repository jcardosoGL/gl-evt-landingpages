<?php include ('../../../pages-defaults/header.php'); ?>
<script>
  document.title = "Van Cleef & Arpels";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
<!-- <link href="../../../../media/LP/src/css/2020/shoppingadistance.css" rel="stylesheet" type="text/css"> -->
  
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance.min.v05.css" rel="stylesheet" type="text/css">
<style>
  .back-top {
    display: none !important;
  }
  .gis-cta-logo {
    height: 65px !important;
    width: 65px !important; 
  }
  .gis-cta-indicator {
    height: 15px !important;
    width: 15px !important;
  }
  @media screen and (max-width: 1024px) {
    .gis-cta-logo {
      height: 60px !important;
      width: 60px !important; 
    }
  }
</style>
<div class="luxe-augmente template-marque" data-bg-color="">
  <section class="luxe-augmente-hero" style="background-color: #000000;">
    <div class="hero-bg-image is-hidden-mobile" style="background-image:url('https://static.galerieslafayette.com/media/LP/src/img/2022/landing/shoppingadistance/van-cleef-arpels/hero_D.jpg'); background-position:center top;">&nbsp;</div>
    <div class="hero-bg-image is-hidden-tablet" style="background-image:url('https://static.galerieslafayette.com/media/LP/src/img/2022/landing/shoppingadistance/van-cleef-arpels/hero_M.jpg'); background-position:center top;">&nbsp;</div>
    <div class="container">
      <div class="luxe-row">
        <div class="luxe-col-mobile-12">
          <h1>Van Cleef & Arpels</h1>
          <a href="#collections" class="button primary has-arrow-down has-smoothscroll">Découvrir les collections</a>
        </div>
      </div>
    </div>
  </section>
  
  <section class="luxe-augmente-body">
    <div class="container">
      <!-- Fil d'Ariane -->
      <nav class="breadcrumb" aria-label="Fil d'Ariane">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com">
              <span itemprop="name">Accueil</span>
            </a>
            <meta itemprop="position" content="1">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/h/createurs">
              <span itemprop="name">Luxe & Créateurs</span>
            </a>
            <meta itemprop="position" content="2">
          </li>
          <li  class="is-active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/van-cleef-arpels" aria-current="page">
              <span itemprop="name">Van Cleef & Arpels</span>
            </a>
            <meta itemprop="position" content="3">
          </li>
        </ul>
      </nav>
        
      <!-- INTRO -->
      <article class="intro luxe-row  luxe-center-mobile">
        <div class="luxe-col-tablet-7 luxe-col-desktop-5 luxe-col-fullhd-4">
          <h3 class="is-uppercase is-hidden">La maison Van Cleef & Arpels</h3>
          <div class="intro-txt">
            <p>L’histoire de Van Cleef & Arpels débute en 1895 lorsqu’Estelle Arpels épouse Alfred Van Cleef. En 1906, naît au 22, place Vendôme la Maison Van Cleef & Arpels, réunissant ces deux noms en
              <span class="read-more"><span class="read-more-toggle">&hellip; suite</span></span>
              <span class="read-more-content is-hidden">une même destinée joaillière. Entre inventivité et poésie, la Maison perpétue au fil des décennies un style reconnaissable entre tous, qui a donné naissance à de nombreuses signatures.&nbsp;
                  <span class="read-less-toggle">Fermer</span>
              </span>
            </p>
          </div>
        </div>
      </article>
      
      <div  id="collections"></div>
      
      <!-- ARTICLE 1 -->
      <article class="luxe-row no-gutter luxe-middle-mobile text-left animated-article article1">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <h2 class="is-uppercase">La joaillerie en shopping à distance</h2>
            <p>Avec le service SHOPPING À DISTANCE, découvrez les collections Van Cleef & Arpels des Galeries Lafayette Paris Haussmann et effectuez vos achats simplement, où que vous soyez, en live vidéo avec un conseiller de la marque ou sur rendez-vous.</p>
            <a href="" class="button outlined" onclick="initSAD(); return false;">Acheter en live vidéo<span class="icon"><i class="microphone"></i></span></a>
            <a href="" class="button outlined" onclick="initWhislist(); return false;" data-reveal-id="popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/shoppingadistance/van-cleef-arpels/content_1.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2022/landing/shoppingadistance/van-cleef-arpels/content_1@2x.jpg 2x" 
                alt="La joaillerie Van Cleef & Arpels - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      <!-- ARTICLE 2 -->
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse animated-article article2">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
          <div class="article-body">
            <h2 class="is-uppercase">La collection ALHAMBRA</h2>
            <p>En 1968, la Maison crée le premier sautoir Alhambra® inspiré de la forme du trèfle à quatre feuilles. Tel un porte-bonheur, ce symbole connaît un succès immédiat, s’établissant à travers le monde comme une icône de chance, emblématique de Van Cleef & Arpels.</p>
            <a href="" class="button outlined" onclick="initSAD(); return false;">Acheter en live vidéo<span class="icon"><i class="microphone"></i></span></a>
            <a href="" class="button outlined" onclick="initWhislist(); return false;" data-reveal-id="popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/van-cleef-arpels/content_2.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/van-cleef-arpels/content_2@2x.jpg 2x" 
                alt="La collection ALHAMBRA Van Cleef & Arpels - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
          
      <!-- ARTICLE 3 -->
      <article class="luxe-row no-gutter luxe-middle-mobile animated-article article3">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
          <div class="article-body">
            <h2 class="is-uppercase">La collection PERLÉE</h2>
            <p>Née en 2008, la collection Perlée® emprunte son esthétique et ses savoir-faire à l’histoire de Van Cleef & Arpels. Magnifiant les créations de délicates perles d’or, les lignes graphiques des bagues, colliers, bracelets et motifs d’oreilles se rencontrent au gré d’une infinité de combinaisons, permettant d’exprimer un style unique et personnel.</p>
            <a href="" class="button outlined" onclick="initSAD(); return false;">Acheter en live vidéo<span class="icon"><i class="microphone"></i></span></a>
            <a href="" class="button outlined" onclick="initWhislist(); return false;" data-reveal-id="popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/van-cleef-arpels/content_3.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/van-cleef-arpels/content_3@2x.jpg 2x" 
                alt="La collection PERLÉE Van Cleef & Arpels - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      <!-- ARTICLE 4 -->
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse text-left animated-article article4">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <h2 class="is-uppercase">La collection FRIVOLE</h2>
            <p>Van Cleef & Arpels célèbre la vitalité de la nature en lui rendant hommage avec poésie. La collection Frivole® se distingue par l’esthétique graphique et aérienne de ces motifs de fleurs. Les pétales en forme de cœur, en or poli miroir ou pavé, suscitent des jeux de lumière et de reflets.</p>
            <a href="" class="button outlined" onclick="initSAD(); return false;">Acheter en live vidéo<span class="icon"><i class="microphone"></i></span></a>
            <a href="" class="button outlined" onclick="initWhislist(); return false;" data-reveal-id="popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/van-cleef-arpels/content_4.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/van-cleef-arpels/content_4@2x.jpg 2x" 
                alt="La collection FRIVOLE Van Cleef & Arpels - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      
      <!-- ARTICLE 5 -->
        <article class="luxe-row no-gutter luxe-middle-mobile animated-article article5">
          <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
            <div class="article-body">
              <h2 class="is-uppercase">La haute parfumerie</h2>
              <p>À travers des fragrances uniques, la Collection Extraordinaire réinterprète les ingrédients naturels les plus nobles de la parfumerie.La combinaison de notes d’agrumes, de fleurs printanières, exotiques et boisées, rend hommage à la poésie de la nature.</p>
              <a href="https://www.galerieslafayette.com/b/van+cleef+arpels" class="button outlined is-marginless">Découvrir la sélection</a>
            </div>
          </div>
          <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
            <div class="article-image">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/van-cleef-arpels/content_5.jpg"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/van-cleef-arpels/content_5@2x.jpg 2x" 
                  alt="La mode enfant Van Cleef & Arpels en Shopping à Distance - Galeries Lafayette" width="620" height="620">
              </figure>
            </div>
          </div>
        </article>
      
      
      <!-- SUR LE PODIUM -->
      <article class="podium">
        <div class="luxe-row no-gutter text-center">
          <div class="luxe-col-mobile-12 has-text-fatt">
            <h3 class="is-uppercase">Sur le Podium</h3>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center podium-items" style="background-color: #f0f0f0;">
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/rose+rouge-van+cleef+arpels/57086490/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/van-cleef-arpels/product_1.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/van-cleef-arpels/product_1@2x.jpg 2x" 
                    alt="Van Cleef & Arpels COLLECTION EXTRAORDINAIRE - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>COLLECTION&nbsp;EXTRAORDINAIRE <br />Rose Rouge</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/collection+extraordinaire-van+cleef+arpels/65154825/5952" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/van-cleef-arpels/product_2.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/van-cleef-arpels/product_2@2x.jpg 2x" 
                    alt="Van Cleef & Arpels COLLECTION EXTRAORDINAIRE - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>COLLECTION&nbsp;EXTRAORDINAIRE <br />Orchidée Vanille</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/neroli+amara-van+cleef+arpels/54063468/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/van-cleef-arpels/product_3.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/van-cleef-arpels/product_3@2x.jpg 2x" 
                    alt="Van Cleef & Arpels COLLECTION EXTRAORDINAIRE - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>COLLECTION&nbsp;EXTRAORDINAIRE <br />Néroli Amara</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/first+-+eau+de+toilette-van+cleef+arpels/88971726/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/van-cleef-arpels/product_4.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/shoppingadistance/van-cleef-arpels/product_4@2x.jpg 2x" 
                    alt="Van Cleef & Arpels First - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>FIRST <br />Eau de toilette</p></div>
            </a>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center">
          <div class="luxe-col-mobile-12">
            <a href="https://www.galerieslafayette.com/b/van+cleef+arpels" class="podium-cta"><span class="plus"></span>Voir tous les articles</a>
          </div>
        </div>
      </article>
      
      
      <div class="luxe-row conditions">
        <div class="luxe-col-mobile-12">
          <hr class="is-marginless" />
          <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service#conditions" class="is-text">Conditions générales du Shopping à Distance</a>
        </div>
      </div>
      
    </div>
  </section>
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-mix.min.v03.js"></script>

<div id="goinstore_modals" style="height:0;"></div>
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/shoppingadistance-goinstore-v2.min.css" rel="stylesheet" type="text/css">
<script src="https://static.galerieslafayette.com/media/LP/src/js/2021/shoppingadistance-goinstore-v2.min.v03.js"></script>

<script>
  var routingKey = "brand44";
  var clickedBrand='Van Cleef & Arpels'; 
  var cleanBrandName='van-cleef-arpels-pe22'; 
  function initWhislist() {
    clickedBrand; 
    cleanBrandName; 
    GL_GIS.LiveShopping.displaySadBrand();
  }
  function initSAD() {
    clickedBrand; 
    cleanBrandName; 
    appointedWidgetId='5fbcfaa1c773203df7231fbb'; 
    GL_GIS.LiveShopping.displaySadBrand();  
    GL_GIS.LiveShopping.checkPopinClerkAvailability(routingKey);
  }

  window.onload = function() {
    GL_GIS.LiveShopping.init(routingKey);    
    
    var gisCTA = document.getElementById('gis-cta');
    if (gisCTA) {
      gisCTA.addEventListener('click', function(e) {
        e.stopPropagation();
        initSAD();
        GL_GIS.LiveShopping.displaySadBrand();
        return false;  
      }, true);
    }
    
    var gisMsgContainer = document.getElementById('gis-awareness-msg-container');
    if (gisMsgContainer) {
      gisMsgContainer.lastChild.addEventListener('click', function(e) { 
        e.stopPropagation();
        initSAD();
        GL_GIS.LiveShopping.displaySadBrand();
        return false;
      }, true);
    }
  }; 
</script>
<!--=========================== FIN LANDING PAGE ========================-->
  
<!-- build:js /media/LP/src/js/2020/shoppingadistance-goinstore.min.v03.js
  <script src="../../../src/js/2020/shoppingadistance-goinstore.js"></script>
endbuild -->

<!-- build:js /media/LP/src/js/2020/shoppingadistance-mix.min.v02.js 
<script src="../../../../assets/js/ScrollMagic.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
<script src="../../../src/js/2020/shoppingadistance-mix.js"></script>
endbuild -->
  
<?php include ('../../../pages-defaults/footer.php'); ?>
