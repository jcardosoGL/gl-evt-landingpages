<?php include ('../../pages-defaults/header.php'); ?>
<script>
  document.title = "FR SAD BLEEKER";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
  
<!-- <link href="../../../media/LP/src/css/2020/shoppingadistance-hub.css" rel="stylesheet" type="text/css"> -->

<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance-hub.min.v10.css" rel="stylesheet" type="text/css">
<style>
  html {
    scroll-behavior: smooth;
  }
  .stores-open .is-hidden-stores-open {
    display: none;
  }
  .stores-closed span.is-hidden-stores-open {
    display: inline;
  }
  .stores-closed section.is-hidden-stores-open, .stores-closed div.is-hidden-stores-open {
    display: block;
  } 
</style>
<div class="luxe-augmente stores-open" data-bg-color="">
  <section class="luxe-augmente-hero" style="background-color: #000000;">
    <div class="container">
      <div class="language"><a href="https://www.galerieslafayette.com/evt/en/remote-personalshopping">EN</a> &nbsp;|&nbsp; FR</div>
      <article class="luxe-row no-gutter luxe-middle-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          
          <div class="hero-image is-hidden-table">
            <figure class="image">
              <iframe class="hero-bg-image hero-video is-hidden-tablet" src="https://player.vimeo.com/video/477237166?autoplay=1&amp;muted=1&amp;autopause=0&amp;background=1" width="767" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
              <iframe class="hero-bg-image hero-video is-hidden-mobile" src="https://player.vimeo.com/video/477262284?autoplay=1&amp;muted=1&amp;autopause=0&amp;background=1" width="574" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <h1 class="is-uppercase has-text-white">Shopping à<br>— distance<br><span class="is-hidden-stores-open" style="font-size:35%; letter-spacing: -1.5px;">Vos plus belles marques Luxe & Créateurs</span></h1>
            <br />
            <p class="has-text-white text-left"><strong class="is-bold">Et si, vous faisiez votre shopping aux Galeries Lafayette Paris Haussmann, depuis chez vous ?</strong><br /><br />
            Avec le Shopping à Distance, les portes du grand magasin iconique restent ouvertes virtuellement pour se faire plaisir en toute simplicité ! Accédez aux plus belles marques Luxe & Créateurs en live vidéo avec les conseillers de vente de vos marques préférées ou les personal shoppers des Galeries Lafayette, et aussi sur wishlist.
            </p>
            <div class="buttons text-left">
              <a href="#marques" class="has-smoothscroll button primary outlined has-arrow-down">contacter une marque</a>
              <a href="#personalshopper" class="has-smoothscroll button primary outlined has-arrow-down">contacter un personal shopper</a>
              <a href="#wishlist" class="has-smoothscroll button primary outlined has-arrow-down">renseigner votre wishlist</a>
            </div>
          </div>
        </div>
      </article>
    </div>
  </section>
  
  <section class="luxe-augmente-body" style="background-color: #F1EFEC;">
    <div class="container">
      <!-- Fil d'Ariane -->
      <nav class="breadcrumb" aria-label="Fil d'Ariane">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com">
              <span itemprop="name">Accueil</span>
            </a>
            <meta itemprop="position" content="1">
          </li>
          <li  class="is-active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance" aria-current="page">
              <span itemprop="name">Shopping à Distance</span>
            </a>
            <meta itemprop="position" content="2">
          </li>
        </ul>
      </nav>
        
      <!-- SERVICES -->
      <article class="services luxe-row">
        <div class="luxe-col-mobile-12">
          <h2>Le shopping digital<br class="is-hidden-tablet" /> personnalisé et sublimé</h2>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-3">
          <div class="service luxe-row">
            <div class="luxe-col-mobile-3 luxe-col-tablet-12">
              <div class="service-icon">
                <img class="icon" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/icon-luxe.svg" 
                alt="icone luxe - Galeries Lafayette">
              </div>
            </div>
            <div class="luxe-col-mobile-9 luxe-col-tablet-12">
              <p class="service-txt"><strong>Le luxe</strong>
              Accédez à nos plus belles marques Luxe & Créateurs depuis chez vous</p>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-3">
          <div class="service luxe-row">
            <div class="luxe-col-mobile-3 luxe-col-tablet-12">
              <div class="service-icon">
                <img class="icon" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/icon-conseil.svg" 
                alt="icone luxe - Galeries Lafayette">
              </div>
            </div>
            <div class="luxe-col-mobile-9 luxe-col-tablet-12">
              <p class="service-txt"><strong>Le Conseil</strong>
              En live vidéo, laissez-vous inspirer par nos experts en mode lors d’un rendez-vous très privé</p>
            </div>
          </div>
        </div>  
        <div class="luxe-col-mobile-12 luxe-col-tablet-3">  
          <div class="service luxe-row">
            <div class="luxe-col-mobile-3 luxe-col-tablet-12">
              <div class="service-icon">
                <img class="icon" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/icon-simplicite.svg" 
                alt="icone luxe - Galeries Lafayette">
              </div>
            </div>
            <div class="luxe-col-mobile-9 luxe-col-tablet-12">
              <p class="service-txt"><strong>La simplicité</strong>
              Confirmez votre commande par e-mail et procédez au paiement sécurisé</p>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-3">  
          <div class="service luxe-row">
            <div class="luxe-col-mobile-3 luxe-col-tablet-12">
              <div class="service-icon">
                <img class="icon" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/icon-confort.svg" 
                alt="icone luxe - Galeries Lafayette">
              </div>
            </div>
            <div class="luxe-col-mobile-9 luxe-col-tablet-12">
              <p class="service-txt"><strong>Le confort</strong>
              Recevez votre commande à domicile ou en click&collect</p>
            </div>
          </div>
        </div>
      </article>
    </div>
  </section>
  
  <!-- MARQUES -->
    <section class="luxe-augmente-body marques" id="marques">
      <div class="container">
        <article class="luxe-row no-gutter luxe-middle-mobile text-left">
          <div class="luxe-col-mobile-12 luxe-col-tablet-5">
            <div class="article-body">
              <div class="tag is-uppercase">Live Shopping</div>
              <h2 class="is-uppercase">Contactez une<br class="is-hidden-mobile" /> marque en live<br class="is-hidden-mobile" /> shopping</h2>
              <p>Plus besoin de vous déplacer en magasin : les conseillers de vos marques préférées sont à votre disposition pour vous faire découvrir les collections en live shopping vidéo, en direct ou sur rendez-vous.</p>
              <p><strong class="is-bold">Lundi au samedi : 11H — 19H</strong><br />
                <strong class="is-bold">Dimanche et jours féries : 12H — 19H</strong></p>
            </div>
          </div>
          <div class="luxe-col-mobile-12 luxe-col-tablet-7">
            
            <div class="live-brands-list top16 show-brands" id="brands-top16"></div>
            
          </div>
                    
          <div class="luxe-col-tablet-4 is-hidden-mobile">
            <div class="notice">
              <p><small>Cliquez sur ce symbole pour contacter un Conseiller en vidéo.
              Vous savez déjà ce que vous désirez ? Optez pour la wishlist : renseignez dans le formulaire en ligne les pièces dont vous avez envie.</small></p>
            </div>
          </div>
          <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-col-tablet-offset-1">
            <a href="#all-brands" class="button outlined show-more">
              <span class="plus">Voir toutes les marques +</span>
              <span class="moins">Fermer -</span>
            </a>
          </div>
          <div class="luxe-col-mobile-12 is-hidden-tablet">
            <div class="notice">
              <p><small>Cliquez sur ce symbole pour contacter un Conseiller en vidéo.
              Vous savez déjà ce que vous désirez ? Optez pour la wishlist : renseignez dans le formulaire en ligne les pièces dont vous avez envie.</small></p>
            </div>
          </div>
          
          <div class="luxe-col-mobile-12">
            <div class="section-content-brands-list show-brands" id="all-brands">
              <div class=" all-hide">
                <div class="live-brands-list" id="brands-list"></div>
                <div class="luxe-col-mobile-12 text-center">
                  <br />
                  <a href="#marques" class="button outlined show-more">
                    <span class="plus">Voir toutes les marques +</span>
                    <span class="moins">Fermer -</span>
                  </a>
                  <br /><br class="is-hidden-tablet" />
                </div>
              </div>
            </div>
          </div>
          
        </article>
        
        <hr />
        
        <article class="luxe-row luxe-middle-mobile secondary">
          <div class="luxe-col-mobile-12">
            <h3>Les nouveautés de vos marques préférées</h3>
          </div>
          <div class="luxe-col-mobile-12 text-left">
            <div class="luxe-row slider-flickty slider-marques">
              
              <div class="slider-col luxe-col-desktop-3">
                <div class="luxe-row no-gutter box">
                  <div class="luxe-col-desktop-12">
                    <a class="article-image" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/prada">
                      <figure class="image">
                        <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/marques-prada.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/marques-prada@2x.jpg 2x" 
                        alt="Les nouveautés Prada - Galeries Lafayette" width="272" height="264">
                      </figure>
                    </a>
                  </div>
                  <div class="luxe-col-desktop-12">
                    <div class="article-body">
                      <p class="title is-uppercase">Prada</p>
                      <p>En exclusivité pour les Galeries Lafayette, Prada présente "Prada Symbols".</p>
                      <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/prada" class="is-text is-bold">Voir les collections</a>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="slider-col luxe-col-desktop-3">
                <div class="luxe-row no-gutter box">
                  <div class="luxe-col-desktop-12">
                    <a class="article-image" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/saint-laurent">
                      <figure class="image">
                        <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/marques-saint-laurent.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/marques-saint-laurent@2x.jpg 2x" 
                        alt="Les nouveautés Saint Laurent - Galeries Lafayette" width="272" height="264">
                      </figure>
                    </a>
                  </div>
                  <div class="luxe-col-desktop-12">
                    <div class="article-body">
                      <p class="title is-uppercase">Saint Laurent</p>
                      <p>Sublime essence du luxe à la française.</p>
                      <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/saint-laurent" class="is-text is-bold">Voir les collections</a>
                    </div>
                  </div>
                </div>
              </div>
                            
              <div class="slider-col luxe-col-desktop-3">
                <div class="luxe-row no-gutter box">
                  <div class="luxe-col-desktop-12">
                    <a class="article-image" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/jacquemus">
                      <figure class="image">
                        <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/marques-jacquemus.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/marques-jacquemus@2x.jpg 2x" 
                        alt="Les nouveautés Gucci - Galeries Lafayette" width="272" height="264">
                      </figure>
                    </a>
                  </div>
                  <div class="luxe-col-desktop-12">
                    <div class="article-body">
                      <p class="title is-uppercase">Jacquemus</p>
                      <p>Quand la mode rencontre la poésie.</p>
                      <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/jacquemus" class="is-text is-bold">Voir les collections</a>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="slider-col luxe-col-desktop-3">
                <div class="luxe-row no-gutter box">
                  <div class="luxe-col-desktop-12">
                    <a class="article-image" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/chloe">
                      <figure class="image">
                        <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/marques-chloe.jpg"
                          srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/marques-chloe@2x.jpg 2x" 
                          alt="Les nouveautés Chanel - Galeries Lafayette" width="272" height="264">
                      </figure>
                    </a>
                  </div>
                  <div class="luxe-col-desktop-12">
                    <div class="article-body">
                      <p class="title is-uppercase">Chloé</p>
                      <p>La douce expression d'une féminité assumée.</p>
                      <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/chloe" class="is-text is-bold">Voir les collections</a>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
        </article>
      </div>
    </section>
  
  <!-- PERSONAL SHOPPER -->
  <section class="luxe-augmente-body" id="personalshopper">
    <div class="container">
      <!-- colonnes inversées avec la class luxe-reverse -->
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse text-left">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <div class="tag is-uppercase">Personal shopper</div>
            <h2 class="is-uppercase">L’expertise de nos personal shoppers en&nbsp;vidéo</h2>
            <p>Vous souhaitez être inspiré pour un cadeau ou pour compléter votre vestiaire ? Laissez-vous conseiller par nos personal shoppers et découvrez en live shopping vidéo une sélection personnalisée de marques, de pièces et de styles. Un conseil mode offert, sur rendez-vous ou en instantané.</p>
            <a href="" class="button primary trigger_liveshopping" onclick="brand='Personal Shopper'; posterName='default'; appointedWidgetId='5f8876b66a0d547bea7db8e4'; checkPopinPSAvailability(brand); return false;">Contacter un personal shopper<span class="icon"><i class="microphone"></i></span></a>
            <div id="jouets"></div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/personal-shopper-main.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/personal-shopper-main@2x.jpg 2x" 
                alt="L’expertise de nos personal shoppers en vidéo - Galeries Lafayette" width="574" height="652">
            </figure>
          </div>
        </div>
      </article>      
    </div>
  </section>
      
  <!-- WISHLIST -->
  <section class="luxe-augmente-body" id="wishlist">
    <div class="container">
      
      <article class="luxe-row no-gutter luxe-middle-mobile text-left">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <div class="tag is-uppercase">Wishlist</div>
            <h2 class="is-uppercase">Vous savez ce que vous<br /> désirez&nbsp;? Optez pour<br /> la wishlist</h2>
            <p>N'hésitez pas à nous partager vos souhaits en remplissant notre formulaire pour être livré directement chez vous.</p>
            <a href="" class="button primary trigger_liveshopping" onclick="brand=' '; posterName='default'; return false;" data-reveal-id="blkr-popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/wishlist-main.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/wishlist-main@2x.jpg 2x" 
                alt="Vous savez ce que vous désirez ? Optez pour la wishlist - Galeries Lafayette" width="574" height="624">
            </figure>
          </div>
        </div>
      </article>
      <hr />
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse text-left secondary">
        <div class="luxe-col-mobile-12 luxe-col-tablet-8 luxe-col-desktop-9">
          <div class="article-body">
            <h3>Montres et bijoux d'exception</h3>
            <p>Indiquez dans votre wishlist la montre ou le bijou de vos rêves parmi les plus prestigieuses marques de haute horlogerie et de joaillerie : Bvlgari, Chaumet, Messika, Mauboussin, Djula, Jaeger Lecoultre, Omega, Bell & Ross, Tag Heuer, Montblanc.</p>
            <a href="" class="button primary trigger_liveshopping" onclick="brand=' '; posterName='default'; return false;" data-reveal-id="blkr-popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-4 luxe-col-desktop-3 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/wishlist-bagues.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/wishlist-bagues@2x.jpg 2x" 
                alt="L’expertise de nos personal shoppers en vidéo - Galeries Lafayette" width="280" height="280">
            </figure>
          </div>
        </div>
      </article>
    </div>
  </section>
  
  <!-- SAD Magasins Réseau -->
  <section class="luxe-augmente-body is-hidden-stores-open">
    <div class="container">
      <!-- colonnes inversées avec la class luxe-reverse -->
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse text-left">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <div class="tag is-uppercase is-hidden">Personal shopper</div>
            <h2 class="is-uppercase">Vous souhaitez retrouver directement vos marques de luxe préférées ?</h2>
            <p>Accédez aux plus belles marques Luxe & Créateurs du grand magasin iconique d’Haussmann en live shopping, sur rendez-vous ou en instantané.</p>
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-magasins" class="button primary">Découvrir les marques</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/coupole-hsm.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2021/landing/bottega-veneta/coupole-hsm@2x.jpg 2x" 
                alt="L’expertise de nos personal shoppers en vidéo - Galeries Lafayette" width="574" height="652">
            </figure>
          </div>
        </div>
      </article>
    </div>
  </section>
  
  <!-- FAQ -->
  <section class="luxe-augmente-body" style="background-color: #F1EFEC;">
    <div class="container">
      <div class="luxe-row text-left faq">
        <div class="luxe-col-mobile-12 luxe-col-tablet-5">
          <h2>Avec le Shopping à<br /> Distance, toujours plus<br /> d’avantages</h2>
          <p>Vous avez des questions ? Voir notre <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">FAQ</a></p>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-7 luxe-col-desktop-6">
          <ul>
            <li><p>Un service de personal shopping en vidéo, exclusif et gratuit.</p></li>
              
            <li><p>Une expérience en toute intimité sur-mesure : vous entendez et voyez votre conseiller ! Ce dernier vous entend et sera en capacité de vous voir uniquement si vous autorisez la vidéo lors de la connexion.</p></li>
              
            <li><p>La livraison des commandes partout en France, à votre domicile et dans l’espace click & collect des grands magasins Galeries Lafayette.</p></li>
              
            <li><p>Un paiement sécurisé, en ligne ou par virement.</p></li>
              
            <li><p>Le cumul des points fidélité et des avantages du programme Mes Galeries.</p></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  
  <!-- CONDTIONS -->
  <section class="luxe-augmente-body">
    <div class="container">
      <div class="luxe-row conditions">
        <div class="luxe-col-mobile-12">
          <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service#conditions" class="is-text">Conditions générales du Shopping à Distance</a>
        </div>
      </div>
    </div>
  </section>
</div>


<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-hub.min.v02.js"></script>

<div id="live_modals" style="height:0;"></div>
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/shoppingadistance-goinstore-v2.min.css" rel="stylesheet" type="text/css">
<script src="https://static.galerieslafayette.com/media/LP/src/js/2022/shoppingadistance-bleeker.min.v03.js"></script>


<script>
  (function() {
    let scripts = Array
    .from(document.querySelectorAll('script'))
    .map(scr => scr.src);
    const url = new URL(window.location.href);
    var search = url.search;
    var paramsStr = search.substr(1);
    if (!scripts.includes('https://widget.bleeker-st.com/gl/embed-dev.js')) { 
      var scriptNode = document.createElement('script');
      scriptNode['id'] = 'bleeker-script';
      scriptNode['src'] = 'https://widget.bleeker-st.com/gl/embed-dev.js'; scriptNode['type'] = 'text/javascript'; document.documentElement.appendChild(scriptNode);
    }
    window.onload = function() {
      initBleekerLiveShopping({
        params: paramsStr,
        vendorID: 'GL',
        uaGoogle : 'UA-4094055-1',
        widget: true,
        type: 'overlay', 
      });
    } 
  })();
  
  function checkPopinPSAvailability(psBrand) {
    console.log('BLKR: Checking Personal Shopper availability.');
    var answers = [];
    
    async function fetchPersonalShoppers() {
      const [response1, response2] = await Promise.all([
        fetch('https://5y48dswgch.execute-api.eu-west-2.amazonaws.com/prod/getvendorisavailable?name='+psBrand+' 1'),
        fetch('https://5y48dswgch.execute-api.eu-west-2.amazonaws.com/prod/getvendorisavailable?name='+psBrand+' 2')
      ]);
      const ps1 = await response1.json();
      const ps2 = await response2.json();
      return [ps1, ps2];
    }
    
    fetchPersonalShoppers().then(([ps1, ps2]) => {
      const isPSAvailable1 = ps1.isAvailable;
      const isPSAvailable2 = ps2.isAvailable;
      if (isPSAvailable1 === true ) {
        answers.push("true"); 
      } else {
        answers.push("false"); 
      }
      if (isPSAvailable2 === true ) {
        answers.push("true"); 
      } else {
        answers.push("false"); 
      }
      console.log("is Personal Shopper 1 available ==> ", isPSAvailable1);
      console.log("is Personal Shopper 2 available ==> ", isPSAvailable2);
      
      if (answers.includes('true') === true) {
        console.log("BLKR: A Personal Shopper is available.");
        $('#blkr-popin-live-shopping-open').foundation('reveal', 'open');
        var startLiveButton = document.querySelector('#blkr-popin-live-shopping-open .live-shopping-cta.video');
        startLiveButton.classList.add("is-available");
        startLiveButton.id = "live-shopping-bleeker-start-chat";
        startLiveButton.setAttribute('onclick', 'initPersonalShoppers("Personal Shopper 1", "Personal Shopper 2", "", "GL");');
      } else {
        console.log("BLKR: No Personal Shopper is available.");
        $('#blkr-popin-live-shopping-closed').foundation('reveal', 'open');
      }
    });
    
    
    var existingAPScript = document.getElementById('blkr-apwidget');
    if (!existingAPScript) {
      var appointedIframe = document.createElement('div');
      appointedIframe.id = "blkr-apwidget";
      
      var appointedScript = document.createElement('script'); 
      appointedScript.src = "https://retailwidgets.appointedd.com/widget/"+ appointedWidgetId +"/widget.js";
      
      var apContainer = document.getElementById("blkr-apwidget-container");
      apContainer.appendChild(appointedIframe);
      apContainer.appendChild(appointedScript);
    }
  }

  
  var brandID = '';
  var brand = '';
  var appointedWidgetId = '';
  var marques = [
    {"marque":"Aeyde","secteur":"","postername":"default","top":false,"visio":true,"key":"LABO 2","apwidget":"5f88795e6a0d547bea7db90c"},
    {"marque":"Alexander McQueen","secteur":"","postername":"alexander-mcqueen-pe22","top":true,"visio":true,"key":"Alexander McQueen","apwidget":"5f8878346a0d547bea7db8e5"},
    {"marque":"Balenciaga","secteur":"","postername":"balenciaga-pe22","top":true,"visio":true,"key":"Balenciaga","apwidget":"5f8878426a0d547bea7db8e6"},
    {"marque":"Berluti","secteur":"","postername":"berluti-pe22","top":false,"visio":true,"key":"Berluti","apwidget":"5f8878496a0d547bea7db8e7"},
    {"marque":"Bottega Veneta","secteur":"","postername":"bottega-veneta-pe22","top":true,"visio":true,"key":"Bottega Veneta","apwidget":"5f8878506a0d547bea7db8e8"},
    {"marque":"Burberry","secteur":"","postername":"burberry-pe22","top":true,"visio":true,"key":"Burberry","apwidget":"5f8878566a0d547bea7db8e9"},
    {"marque":"Cartier","secteur":"","postername":"cartier","top":false,"visio":true,"key":"Cartier","apwidget":"5f8879036a0d547bea7db8ff"},
    {"marque":"Celine","secteur":"","postername":"celine-v1","top":true,"visio":true,"key":"Celine","apwidget":"5f88785e6a0d547bea7db8ea"},
    {"marque":"Chaumet","secteur":"","postername":"chaumet-pe22","top":false,"visio":true,"key":"Chaumet","apwidget":"5f8878646a0d547bea7db8eb"},
    {"marque":"Chloé","secteur":"","postername":"chloe-pe22","top":true,"visio":true,"key":"Chloé","apwidget":"5f8879096a0d547bea7db900"},
    {"marque":"Chopard","secteur":"","postername":"chopard-v1","top":false,"visio":true,"key":"Chopard","apwidget":"5ff4979f6bbac15fbc76ec32"},
    {"marque":"Comme des Garçons","secteur":"","postername":"default","top":false,"visio":true,"key":"Comme des Garçons","apwidget":"5f8878fc6a0d547bea7db8fe"},
    {"marque":"Dior","secteur":"","postername":"dior-v1","top":true,"visio":true,"key":"Dior","apwidget":"5f8878736a0d547bea7db8ed"},
    {"marque":"Dior beauté","secteur":"","postername":"dior-v1","top":true,"visio":true,"key":"Dior beauté","apwidget":"5fbcfaa6c773203df7231fbc"},
    {"marque":"Dolce & Gabbana","secteur":"","postername":"dolce-gabbana-fw21","top":false,"visio":true,"key":"Dolce - Gabbana","apwidget":"5f88787a6a0d547bea7db8ee"},
    {"marque":"Eres","secteur":"","postername":"eres-v1","top":false,"visio":true,"key":"Eres","apwidget":"5f8879176a0d547bea7db902"},
    {"marque":"Etoile","secteur":"","postername":"default","top":false,"visio":true,"key":"Isabel Marant","apwidget":"5f8878806a0d547bea7db8ef"},
    {"marque":"Fauré Lepage","secteur":"","postername":"faure-lepage-v1","top":true,"visio":true,"key":"Fauré Lepage","apwidget":"5f88791d6a0d547bea7db903"},
    {"marque":"Fendi","secteur":"","postername":"fendi-homme-pe22","top":true,"visio":true,"key":"Fendi","apwidget":"5f8878966a0d547bea7db8f0"},
    {"marque":"Flattered","secteur":"","postername":"default","top":false,"visio":true,"key":"LABO 2","apwidget":"5f88795e6a0d547bea7db90c"},
    {"marque":"Fred","secteur":"","postername":"fred-pe22","top":false,"visio":true,"key":"Fred","apwidget":"5f88789d6a0d547bea7db8f1"},
    {"marque":"Givenchy","secteur":"","postername":"givenchy-v1","top":false,"visio":true,"key":"Givenchy","apwidget":"5f8878a36a0d547bea7db8f2"},
    {"marque":"Isabel Marant","secteur":"Femme","postername":"isabel-marant-pe22","top":true,"visio":true,"key":"Isabel Marant","apwidget":"5f8878806a0d547bea7db8ef"},
    {"marque":"Jacquemus","secteur":"","postername":"jacquemus-pe22","top":true,"visio":true,"key":"Jacquemus","apwidget":"5f8879246a0d547bea7db904"},
    {"marque":"Jimmy Choo","secteur":"","postername":"jimmy-choo-v2","top":false,"visio":true,"key":"Jimmy Choo","apwidget":"5f88792c6a0d547bea7db905"},
    {"marque":"Jw Anderson","secteur":"","postername":"jw-anderson","top":false,"visio":true,"key":"LABO 3","apwidget":"5f8879626a0d547bea7db90d"},
    {"marque":"Kenzo","secteur":"","postername":"kenzo-pe22","top":false,"visio":true,"key":"Kenzo","apwidget":"5fa00f84849cbd530ae374aa"},
    {"marque":"L Autre Chose","secteur":"","postername":"default","top":false,"visio":true,"key":"LABO 2","apwidget":"5f88795e6a0d547bea7db90c"},
    {"marque":"La Perla ","secteur":"","postername":"la-perla-pe22","top":false,"visio":true,"key":"La Perla","apwidget":"5f8879346a0d547bea7db906"},
    {"marque":"Loewe","secteur":"","postername":"loewe","top":true,"visio":true,"key":"Loewe","apwidget":"5f8878bb6a0d547bea7db8f5"},
    {"marque":"Manu Atelier","secteur":"","postername":"default","top":false,"visio":true,"key":"LABO 2","apwidget":"5f88795e6a0d547bea7db90c"},
    {"marque":"Marine Serre","secteur":"","postername":"default","top":false,"visio":true,"key":"LABO 3","apwidget":"5f8879626a0d547bea7db90d"},
    {"marque":"Marni ","secteur":"","postername":"default","top":false,"visio":true,"key":"Marni","apwidget":"5f8878c46a0d547bea7db8f6"},
    {"marque":"Materiel","secteur":"","postername":"default","top":false,"visio":true,"key":"LABO 3","apwidget":"5f8879626a0d547bea7db90d"},
    {"marque":"Mauboussin","secteur":"","postername":"default","top":false,"visio":true,"key":"Mauboussin","apwidget":"5fbcfaafc773203df7231fbe"},
    {"marque":"Max Mara","secteur":"","postername":"default","top":false,"visio":true,"key":"Max Mara","apwidget":"5f88793b6a0d547bea7db907"},
    {"marque":"Messika","secteur":"","postername":"messika-v1","top":false,"visio":true,"key":"Messika","apwidget":"5f8879436a0d547bea7db908"},
    {"marque":"Miista","secteur":"","postername":"default","top":false,"visio":true,"key":"LABO 2","apwidget":"5f88795e6a0d547bea7db90c"},
    {"marque":"Miu Miu","secteur":"","postername":"miu-miu-fw21","top":false,"visio":true,"key":"Miu Miu","apwidget":"5f8878d96a0d547bea7db8f9"},
    {"marque":"Moncler","secteur":"","postername":"default","top":false,"visio":true,"key":"Moncler","apwidget":"5f8878cb6a0d547bea7db8f7"},
    {"marque":"Off-White","secteur":"Homme","postername":"off-white-pe22","top":false,"visio":true,"key":"Off-White","apwidget":"5fa3b8f448dc683d829fac1d"},
    {"marque":"Prada","secteur":"","postername":"prada-pe22-homme","top":true,"visio":true,"key":"Prada","apwidget":"5f8878d26a0d547bea7db8f8"},
    {"marque":"Roger Vivier","secteur":"","postername":"roger-vivier-pe22","top":false,"visio":true,"key":"Roger Vivier","apwidget":"5f8878aa6a0d547bea7db8f3"},
    {"marque":"Saint Laurent","secteur":"","postername":"saint-laurent-v3","top":true,"visio":true,"key":"Saint Laurent","apwidget":"5f8878e86a0d547bea7db8fb"},
    {"marque":"Salvatore Ferragamo","secteur":"","postername":"salvatore-ferragamo-pe22","top":false,"visio":true,"key":"Salvatore Ferragamo","apwidget":"5f8878ee6a0d547bea7db8fc"},
    {"marque":"The Attico","secteur":"","postername":"default","top":false,"visio":true,"key":"LABO 3","apwidget":"5f8879626a0d547bea7db90d"},
    {"marque":"Tiffany & Co.","secteur":"","postername":"tiffany-v1","top":false,"visio":true,"key":"Tiffany","apwidget":"5f88794a6a0d547bea7db909"},
    {"marque":"Tod's","secteur":"","postername":"tods-femme-pe22","top":false,"visio":true,"key":"Tod's","apwidget":"5f8878b16a0d547bea7db8f4"},
    {"marque":"Valentino","secteur":"","postername":"valentino-ah21","top":true,"visio":true,"key":"Valentino","apwidget":"5f8879506a0d547bea7db90a"},
    {"marque":"Van Cleef & Arpels","secteur":"","postername":"van-cleef-arpels-pe22","top":false,"visio":true,"key":"Van Cleef - Arpels","apwidget":"5fbcfaa1c773203df7231fbb"},
    {"marque":"Yuul Yi","secteur":"","postername":"default","top":false,"visio":true,"key":"LABO 2","apwidget":"5f88795e6a0d547bea7db90c"}
  ];

  function getList(el, top = false) {
    const bloc = document.createElement('div');
    bloc.classList.add('brands-container');
    let wrapper_letter = null;
    let wrapper = top ? bloc : wrapper_letter;
    const lst = top ? marques.filter(m => m.top) : marques;
    let letter = null;
    lst.forEach(m => {
        const l = m.marque.substring(0, 1);
        if (!top) {
            if (letter === null || letter !== l) {
                letter = l;
                wrapper_letter = document.createElement('div');
                wrapper_letter.classList.add('wrapper');
                bloc.appendChild(wrapper_letter);
                const line = document.createElement('div');
                line.innerText = l;
                line.classList.add('letter');
                wrapper_letter.appendChild(line);
                wrapper = wrapper_letter;
            }
        }
        const line = document.createElement('a');
        
        if (m.marque === "") {
          line.setAttribute('href', "https://www.galerieslafayette.com/evt/fr/shoppingadistance/");
        } else {
          line.setAttribute('href', "");
          if (m.visio) {
            line.setAttribute("onclick", "brand='" + m.marque.replace(/'/g, "\\'") + "'; brandID='" + m.key + "'; posterName='" + m.postername + "'; appointedWidgetId='" + m.apwidget + "'; GL_BLKR.LiveShopping.checkPopinAvailability('" + m.key + "'); return false;");
            line.classList.add('has-visio');
          } else {
            
          }
        }
        
        line.innerText = m.marque;
        line.classList.add('marque', 'trigger_liveshopping');

        if (m.secteur) {
            const univers = document.createElement('span');
            univers.innerText = m.secteur;
            line.appendChild(univers);
        }
        wrapper.appendChild(line);
    });
    document.getElementById(el).appendChild(bloc);
  }

  getList('brands-top16', true);
  getList('brands-list');
</script>
<!--=========================== FIN LANDING PAGE ========================-->
    
      

<!-- build:js /media/LP/src/js/2022/shoppingadistance-bleeker.min.dev04.js
  <script src="../../src/js/2022/shoppingadistance-bleeker-dev.js"></script>
<!-- endbuild -->
  
<!-- build:js /media/LP/src/js/2022/shoppingadistance-bleeker.min.v04.js
  <script src="../../src/js/2022/shoppingadistance-bleeker.js"></script>
<!-- endbuild -->
  
<!-- build:js /media/LP/src/js/2020/shoppingadistance-hub.min.v02.js 
  <script src="../../../assets/js/flickity.pkgd.min.js"></script>
  <script src="../../src/js/2020/shoppingadistance-hub.js"></script>
endbuild -->
  
<?php include ('../../pages-defaults/footer.php'); ?>
