<?php include ('../../../pages-defaults/header.php'); ?>
<script>
  document.title = "LIVE BLEEKER";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
  
<!-- =========================== LANDING PAGE ========================== -->  
<div id="bleeeker-liveshopping-content"></div>


<script>
  (function() {
    let scripts = Array
    .from(document.querySelectorAll('script'))
    .map(scr => scr.src);
    const url = new URL(window.location.href);
    var search = url.search;
    var paramsStr = search.substr(1);
    if (!scripts.includes('https://widget.bleeker-st.com/gl/embed-dev.js')) { 
      var scriptNode = document.createElement('script');
      scriptNode['id'] = 'bleeker-script';
      scriptNode['src'] = 'https://widget.bleeker-st.com/gl/embed-dev.js'; scriptNode['type'] = 'text/javascript'; document.documentElement.appendChild(scriptNode);
    }
    window.onload = function() {
      initBleekerLiveShopping({
        params: paramsStr,
        vendorID: 'GL',
        uaGoogle : 'UA-4094055-1',
        widget: true,
        type: 'overlay', 
      });
    } 
  })();
</script>
<!--=========================== FIN LANDING PAGE ========================-->

<?php include ('../../../pages-defaults/footer.php'); ?>
