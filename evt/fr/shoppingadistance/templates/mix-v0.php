<?php include ('../../../pages-defaults/header.php'); ?>
<script>
  document.title = "Mix";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>

<!-- =========================== LANDING PAGE ========================== -->  
<!-- https://static.galerieslafayette.com/ -->
<link href="../../../../media/LP/src/css/2020/shoppingadistance.css" rel="stylesheet" type="text/css">


<div class="luxe-augmente template-marque" data-bg-color="#ffede5">
  <section class="luxe-augmente-hero" style="background-color: #000000;">
    <div class="hero-bg-image hide-for-xmedium-up" style="background-image:url('../../../../media/LP/src/img/2020/landing/luxe-augmente/brand_hero.jpg'); background-position:center center;">&nbsp;</div>
    <div class="hero-bg-image show-for-xmedium-up" style="background-image:url('../../../../media/LP/src/img/2020/landing/luxe-augmente/brand_hero_b_v02.jpg'); background-position:center top;">&nbsp;</div>
    <div class="container">
      <div class="luxe-row">
        <div class="luxe-col-mobile-12">
          <h1>DOLCE & GABBANA</h1>
          <a href="#collections" class="button inverted hide-small has-smoothscroll">Découvrir les collections</a>
        </div>
      </div>
    </div>
  </section>
  
  <section class="luxe-augmente-body" id="collections">
    <div class="container">
      
      <!-- INTRO -->
      <article class="intro luxe-row  luxe-center-mobile">
        <div class="luxe-col-tablet-7 luxe-col-desktop-5">
          <h3>LE STYLE À L'ITALIENNE</h3>
          <div class="intro-txt">
            <p>Depuis les années 80, Domenico Dolce et Stefano Gabbana, l’un des duos les plus influents du monde la mode, rendent hommage, à travers leurs créations, à la culture et au glamour italiens. Leurs collections opulentes et thématiques transcendent les saisons            
              <span class="read-more is-hidden-widescreen"><span class="show">&hellip;</span> <span class="read-more-toggle">Lire la suite</span>
                <span class="read-more-body"> et se caractérisent par une abondance d’ornements, une féminité rétro et des imprimés éclatants.</span>
              </span>
              <span class="is-hidden-touch is-hidden-desktop-only">et se caractérisent par une abondance d’ornements, une féminité rétro et des imprimés éclatants.</span class="is-hidden-touch">
            </p>
          </div>
        </div>
      </article>
      
      <!-- ARTICLE 1 -->
      <article class="animated-article article1">
        <div class="luxe-row no-gutter luxe-middle-mobile">
          <div class="luxe-col-mobile-12 luxe-col-tablet-6">
            <div class="article-body">
              <h2>LA BEAUTÉ<br /> & PARFUMS</h2>
              <p>Accompagné en live vidéo par un conseiller de la marque Rolex ou en échangeant par e-mail, réalisez votre shopping de votre grand magasin Galeries Lafayette depuis chez vous.</p>
              <a href="#" class="button outlined">Découvrir la sélection</a>
            </div>
          </div>
          <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
            <div class="article-image">
              <figure class="image">
                <img src="../../../../media/LP/src/img/2020/landing/luxe-augmente/image-article-1.jpg"
                  srcset="../../../../media/LP/src/img/2020/landing/luxe-augmente/image-article-1@2x.jpg 2x" 
                  alt="La beauté & parfums - Galeries Lafayette" width="620" height="620">
              </figure>
            </div>
          </div>
        </div>
      </article>
      
      
      
      <!-- SUR LE PODIUM -->
      <article class="podium">
        <div class="luxe-row no-gutter text-center">
          <div class="luxe-col-mobile-12 has-text-fat">
            <p class="is-uppercase">— Sur le Podium</p>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center podium-items" style="background-color: #f0f0f0;">
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="#" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="../../../../media/LP/src/img/2020/landing/luxe-augmente/dior/dior-diorshow-mono.jpg"
                    srcset="../../../../media/LP/src/img/2020/landing/luxe-augmente/dior/dior-diorshow-mono@2x.jpg 2x" 
                    alt="DIORSHOW MONO Fard à paupières - Galeries Lafayette" width="251" height="244">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>DIORSHOW MONO <br />Fard à paupières</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="#" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="../../../../media/LP/src/img/2020/landing/luxe-augmente/dior/dior-j-adore.jpg"
                    srcset="../../../../media/LP/src/img/2020/landing/luxe-augmente/dior/dior-j-adore@2x.jpg 2x" 
                    alt="J'ADORE Eau de parfum infinissime - Galeries Lafayette" width="238" height="307">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>J'ADORE<br />Eau de parfum infinissime</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="#" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="../../../../media/LP/src/img/2020/landing/luxe-augmente/dior/dior-diorshow-pump-n-volume-hd.jpg"
                    srcset="../../../../media/LP/src/img/2020/landing/luxe-augmente/dior/dior-diorshow-pump-n-volume-hd@2x.jpg 2x" 
                    alt="MASCARA DIORSHOW PUMP 'N' VOLUME HD - Galeries Lafayette" width="202" height="282">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>MASCARA DIORSHOW PUMP 'N'<br class="is-hidden-touch" /> VOLUME HD</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="#" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="../../../../media/LP/src/img/2020/landing/luxe-augmente/dior/dior-sauvage.jpg"
                    srcset="../../../../media/LP/src/img/2020/landing/luxe-augmente/dior/dior-sauvage@2x.jpg 2x" 
                    alt="SAUVAGE Lotion après-rasage - Galeries Lafayette" width="266" height="313">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>SAUVAGE <br />Lotion après-rasage</p></div>
            </a>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center">
          <div class="luxe-col-mobile-12 has-text-fat">
            <a href="#" class="podium-cta"><span class="plus"></span>Voir tous les articles</a>
          </div>
        </div>
      </article>
      
      
      
      <!-- ARTICLE 2 -->
      <!-- colonnes inversées avec la class luxe-reverse -->
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse animated-article article2">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <h2>LA MODE EN SHOPPING<br /> À DISTANCE</h2>
            <p>Avec le service SHOPPING À DISTANCE, découvrez les collections Dolce & Gabbana des Galeries Lafayette Paris Haussmann et effectuez vos achats simplement, où que vous soyez, en live vidéo avec un conseiller de la marque ou sur rendez-vous.</p>
            <a href="#" class="button outlined">Acheter en live vidéo</a>
            <a href="#" class="button outlined">Prendre rendez-vous</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="../../../../media/LP/src/img/2020/landing/luxe-augmente/image-article-2.jpg"
                srcset="../../../../media/LP/src/img/2020/landing/luxe-augmente/image-article-2@2x.jpg 2x" 
                alt="La mode en shopping personnalisé - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      <!-- ARTICLE 3 -->
      <article class="luxe-row no-gutter luxe-middle-mobile animated-article article3">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <h2>LES LUNETTES &<br /> ACCESSOIRES</h2>
            <p>Accompagné en live vidéo par un conseiller de la marque Rolex ou en échangeant par e-mail, réalisez votre shopping de votre grand magasin Galeries Lafayette depuis chez vous.</p>
            <a href="#" class="button outlined">Découvrir la sélection</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="../../../../media/LP/src/img/2020/landing/luxe-augmente/image-article-3.jpg"
                srcset="../../../../media/LP/src/img/2020/landing/luxe-augmente/image-article-3@2x.jpg 2x" 
                alt="Les lunettes & Accessoires - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
    </div>
  </section>
  
</div>


<script src="../../../../media/LP/src/js/2020/shoppingadistance-mix.min.v01.js"></script>
  
<!--=========================== FIN LANDING PAGE ========================-->
  
<!-- build:js /media/LP/src/js/2020/shoppingadistance-goinstore.min.v01.js
  <script src="../../../src/js/2020/shoppingadistance-goinstore.js"></script>
endbuild -->

<!-- build:js /media/LP/src/js/2020/shoppingadistance-mix.min.v01.js 
<script src="../../../../assets/js/ScrollMagic.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
<script src="../../../src/js/2020/shoppingadistance-mix.js"></script>
endbuild -->

<?php include ('../../../pages-defaults/footer.php'); ?>
