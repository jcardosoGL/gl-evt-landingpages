<?php include ('../../../pages-defaults/header.php'); ?>
<script>
  document.title = "MIX BLEEKER";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../../../media/LP/src/css/2020/shoppingadistance.css" rel="stylesheet" type="text/css"> -->
  
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance.min.v05.css" rel="stylesheet" type="text/css">
<style>
  .back-top {
    display: none !important;
  }
</style>
<div class="luxe-augmente template-marque" data-bg-color="">
  <section class="luxe-augmente-hero" style="background-color: #000000;">
    <div class="hero-bg-image" style="background-image:url('https://static.galerieslafayette.com/media/LP/src/img/2020/landing/luxe-augmente/dior/dior-hero.jpg'); background-position:center top;">&nbsp;</div>
    <div class="container">
      <div class="luxe-row">
        <div class="luxe-col-mobile-12">
          <h1>Dior</h1>
          <a href="#collections" class="button primary has-arrow-down has-smoothscroll">Découvrir les collections</a>
        </div>
      </div>
    </div>
  </section>
  
  <section class="luxe-augmente-body">
    <div class="container">
      <!-- Fil d'Ariane -->
      <nav class="breadcrumb" aria-label="Fil d'Ariane">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com">
              <span itemprop="name">Accueil</span>
            </a>
            <meta itemprop="position" content="1">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/h/createurs">
              <span itemprop="name">Luxe & Créateurs</span>
            </a>
            <meta itemprop="position" content="2">
          </li>
          <li  class="is-active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/" aria-current="page">
              <span itemprop="name">Dior</span>
            </a>
            <meta itemprop="position" content="3">
          </li>
        </ul>
      </nav>
        
      <!-- INTRO -->
      <article class="intro luxe-row  luxe-center-mobile">
        <div class="luxe-col-tablet-7 luxe-col-desktop-5 luxe-col-fullhd-4">
          <h3 class="is-uppercase">La maison Dior</h3>
          <div class="intro-txt">
            <p>Autobiographie, autoportrait, récit. Associer des lieux, des images, des mots. Librement, avec nos yeux d'aujourd'hui. Pour sa collection prêt-à-porter automne-hiver 2020-2021, Maria Grazia Chiuri dessine son atlas des émotions à travers son journal intime&nbsp;
              <span class="read-more"><span class="read-more-toggle">&hellip; suite</span></span>
              <span class="read-more-content is-hidden"> et se caractérisent par une abondance d’ornements, une féminité rétro et des imprimés éclatants.&nbsp;
                <span class="read-less-toggle">Fermer</span>
              </span>             
            </p>
          </div>
        </div>
      </article>
      
      <div  id="collections"></div>
      
      <!-- ARTICLE 1 -->
      <article class="luxe-row no-gutter luxe-middle-mobile text-left animated-article article1">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <h2>Le prêt-à-porter en <br />shopping à distance</h2>
            <p>Avec le Shopping à Distance, découvrez les collections Dior maroquinerie des Galeries Lafayette Paris Haussmann et effectuez vos achats où que vous soyez, en live shopping vidéo avec un conseiller de la marque ou en renseignant votre wishlist avec les pièces dont vous avez envie. <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">En savoir +</a></p>
            <a href="" class="button outlined" onclick="initSAD(); return false;">Acheter en live vidéo<span class="icon"><i class="microphone"></i></span></a>
            <a href="" class="button outlined" onclick="initWhislist(); return false;" data-reveal-id="blkr-popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
            <br /><br />
            
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/luxe-augmente/dior/dior-pret-a-porter.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/luxe-augmente/dior/dior-pret-a-porter@2x.jpg 2x" 
                alt="Le prêt-à-porter en shopping à distance - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      
      <!-- ARTICLE 2 -->
      <!-- colonnes inversées avec la class luxe-reverse -->
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse animated-article article2">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
          <div class="article-body">
            <h2>La Beauté</h2>
            <p>Avec le service SHOPPING À DISTANCE, découvrez les collections Dolce & Gabbana des Galeries Lafayette Paris Haussmann et effectuez vos achats simplement, où que vous soyez, en live vidéo avec un conseiller de la marque ou sur rendez-vous.</p>
            <a href="#" class="button outlined is-marginless">Découvrir la sélection</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/luxe-augmente/dior/dior-parfums.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/luxe-augmente/dior/dior-parfums@2x.jpg 2x" 
                alt="La beauté - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      
      <!-- SUR LE PODIUM -->
      <article class="podium">
        <div class="luxe-row no-gutter text-center">
          <div class="luxe-col-mobile-12 has-text-fatt">
            <h3 class="is-uppercase">Sur le Podium</h3>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center podium-items" style="background-color: #f0f0f0;">
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="#" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/luxe-augmente/dior/dior-diorshow-mono.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/luxe-augmente/dior/dior-diorshow-mono@2x.jpg 2x" 
                    alt="DIORSHOW MONO Fard à paupières - Galeries Lafayette" width="251" height="244">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>DIORSHOW MONO <br />Fard à paupières</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="#" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/luxe-augmente/dior/dior-j-adore.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/luxe-augmente/dior/dior-j-adore@2x.jpg 2x" 
                    alt="J'ADORE Eau de parfum infinissime - Galeries Lafayette" width="238" height="307">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>J'ADORE<br />Eau de parfum infinissime</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="#" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/luxe-augmente/dior/dior-diorshow-pump-n-volume-hd.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/luxe-augmente/dior/dior-diorshow-pump-n-volume-hd@2x.jpg 2x" 
                    alt="MASCARA DIORSHOW PUMP 'N' VOLUME HD - Galeries Lafayette" width="202" height="282">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>MASCARA DIORSHOW PUMP 'N'<br class="is-hidden-touch" /> VOLUME HD</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="#" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/luxe-augmente/dior/dior-sauvage.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/luxe-augmente/dior/dior-sauvage@2x.jpg 2x" 
                    alt="SAUVAGE Lotion après-rasage - Galeries Lafayette" width="266" height="313">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>SAUVAGE <br />Lotion après-rasage</p></div>
            </a>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center">
          <div class="luxe-col-mobile-12">
            <a href="#" class="podium-cta"><span class="plus"></span>Voir tous les articles</a>
          </div>
        </div>
      </article>
      
      
      
      <!-- ARTICLE 3 -->
      <article class="luxe-row no-gutter luxe-middle-mobile animated-article article3">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
          <div class="article-body">
            <h2>La maroquinerie en shopping à distance</h2>
            <p>Avec le Shopping à Distance, découvrez les collections Dior maroquinerie des Galeries Lafayette Paris Haussmann et effectuez vos achats où que vous soyez, en live shopping vidéo avec un conseiller de la marque ou en renseignant votre wishlist avec les pièces dont vous avez envie. <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">En savoir plus</a></p>
            <a href="" class="button outlined" onclick="initSAD(); return false;">Acheter en live vidéo<span class="icon"><i class="microphone"></i></span></a>
            <a href="" class="button outlined" onclick="initWhislist(); return false;" data-reveal-id="blkr-popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
            <br /><br />
            
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/luxe-augmente/dior/dior-maroquinerie.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/luxe-augmente/dior/dior-maroquinerie@2x.jpg 2x" 
                alt="La maroquinerie en shopping à distance - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      <div class="luxe-row conditions">
        <div class="luxe-col-mobile-12">
          <hr class="is-marginless" />
          <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service#conditions" class="is-text">Conditions générales du Shopping à Distance</a>
        </div>
      </div>
      
    </div>
  </section>
</div>


<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-mix.min.v03.js"></script>

<div id="live_modals" style="height:0;"></div>
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/shoppingadistance-goinstore-v2.min.css" rel="stylesheet" type="text/css">
<script src="https://static.galerieslafayette.com/media/LP/src/js/2022/shoppingadistance-bleeker.min.v02.js"></script>

<script>
  (function() {
    let scripts = Array
    .from(document.querySelectorAll('script'))
    .map(scr => scr.src);
    const url = new URL(window.location.href);
    var search = url.search;
    var paramsStr = search.substr(1);
    if (!scripts.includes('https://widget.bleeker-st.com/gl/embed-dev.js')) { 
      var scriptNode = document.createElement('script');
      scriptNode['id'] = 'bleeker-script';
      scriptNode['src'] = 'https://widget.bleeker-st.com/gl/embed-dev.js'; scriptNode['type'] = 'text/javascript'; document.documentElement.appendChild(scriptNode);
    }
    window.onload = function() {
      initBleekerLiveShopping({
        params: paramsStr,
        vendorID: 'GL',
        uaGoogle : 'UA-4094055-1',
        widget: true,
        type: 'overlay', 
      });
    } 
  })();
  
  var brandID='Fred';
  var brand='Fred';
  var posterName='fred';
  function initWhislist() {
    brand; 
    posterName;
    GL_BLKR.LiveShopping.displaySadBrand();
  }
  function initSAD() {
    brandID;
    brand;
    posterName; 
    appointedWidgetId='5f88789d6a0d547bea7db8f1'; 
    GL_BLKR.LiveShopping.displaySadBrand();  
    GL_BLKR.LiveShopping.checkPopinAvailability(brandID);
  }
</script>
<!-- =========================== FIN LANDING PAGE ======================== -->

<!-- build:js /media/LP/src/js/2022/shoppingadistance-bleeker.min.v01.js 
  <script src="../../../src/js/2022/shoppingadistance-bleeker.js"></script>
 endbuild -->
    
<!-- build:js /media/LP/src/js/2022/shoppingadistance-bleeker.min.v01.js 
  <script src="../../../src/js/2022/shoppingadistance-bleeker.js"></script>
<!-- endbuild -->  

<!-- build:js /media/LP/src/js/2020/shoppingadistance-mix.min.v01.js 
<script src="../../../../assets/js/ScrollMagic.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
<script src="../../../src/js/2020/shoppingadistance-mix.js"></script>
endbuild -->
  
<?php include ('../../../pages-defaults/footer.php'); ?>
