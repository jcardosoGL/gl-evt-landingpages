<?php include ('../../../pages-defaults/header.php'); ?>
<script>
  document.title = "Saint Laurent";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- =========================== LANDING PAGE ========================== -->  
<!-- <link href="../../../../media/LP/src/css/2020/shoppingadistance.css" rel="stylesheet" type="text/css"> -->
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance.min.v03.css" rel="stylesheet" type="text/css">
<style>
  @media screen and (min-width: 768px) {
    .collection-main-text {
      margin-right: -220px;
      left: -90px;
    }
  }
  @media screen and (min-width: 1024px) {
    .collection-main-text {
      left: -50px;
    }
  }
</style>
<div class="luxe-augmente template-vad" data-bg-color="">
  <section class="luxe-augmente-hero" style="background-color: #000000;">
    <div class="hero-bg-image" style="background-image:url('https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/saint-laurent/saint-laurent-hero.jpg'); background-position:center top;">&nbsp;</div>
    <div class="container">
      <div class="luxe-row">
        <div class="luxe-col-mobile-12">
          <h1 class="is-uppercase">Saint Laurent</h1>
          <a href="#collections" class="is-arrow-down has-smoothscroll">
            <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/circled-arrow-down.svg" alt="" width="31" height="31" />
          </a>
        </div>
      </div>
    </div>
  </section>
  
  <section class="luxe-augmente-body">
    <div class="container">
      <!-- Fil d'Ariane -->
      <nav class="breadcrumb" aria-label="Fil d'Ariane">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com">
              <span itemprop="name">Accueil</span>
            </a>
            <meta itemprop="position" content="1">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/h/createurs">
              <span itemprop="name">Luxe & Créateurs</span>
            </a>
            <meta itemprop="position" content="2">
          </li>
          <li  class="is-active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/saint-laurent" aria-current="page">
              <span itemprop="name">Saint Laurent</span>
            </a>
            <meta itemprop="position" content="3">
          </li>
        </ul>
      </nav>
      
      <!-- INTRO -->
      <article class="intro">
        <div class="luxe-row luxe-center-mobile">
          <div class="luxe-col-tablet-7 luxe-col-desktop-5">
            <h3 class="is-uppercase">La maison Saint Laurent</h3>
            <div class="intro-txt">
              <p>Fondée en 1961, Yves Saint Laurent fut la première maison de couture à lancer le prêt-à-porter de luxe avec la ligne “Saint Laurent Rive Gauche”. Depuis,
                <span class="read-more"><span class="show">&hellip;</span> <span class="read-more-toggle">Lire la suite</span>
                  <span class="read-more-body"> Yves Saint Laurent, s’est imposé comme l’un des créateurs majeurs de sa génération. Membre du groupe Kering, la Maison est restée fidèle à son identité́. Sous la direction créative d’Anthony Vaccarello depuis avril 2016, elle offre aujourd’hui des collections de prêt-à-porter féminin et masculin, de maroquinerie, de chaussures, de bijoux et de lunettes.</span>
                </span>
              </p>
            </div>
          </div>
        </div>
        <div class="luxe-row luxe-center-mobile" id="collections">
          <div class="luxe-col-mobile-12 luxe-col-tablet-9 has-text-smaller">
            <h2>Collections à découvrir exclusivement<br class="is-hidden-mobile" /> en&nbsp;Shopping à&nbsp;Distance</h2>
            <br />
            <div class="intro-links">
              <a href="#" class="button primary trigger_liveshopping" onclick="clickedBrand='Saint Laurent'; appointedWidgetId='5f8878e86a0d547bea7db8fb'; GL_GIS.LiveShopping.checkClerkAvailability('brand19'); return false;">Acheter en live vidéo<span class="icon"><i class="microphone"></i></span></a>
              <br />
              <a href="" class="button primary trigger_liveshopping" onclick="clickedBrand='Saint Laurent';" data-reveal-id="popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>  
              <br />
              <a href="#shopping-a-distance" class="is-text is-pink is-bold has-smoothscroll">Qu'est-ce que le Shopping à Distance ?</a>
            </div>
          </div>
        </div>
      </article>
      
      <!-- ARTICLE COLLECTIONS -->
      <article class="luxe-row no-gutter luxe-center-mobile text-left collections">
        
        <div class="luxe-col-mobile-12">
          <div class="tabs-container">
            <div class="tabs is-centered is-hidden" role="tablist">
              <ul>
                <li role="presentation">
                  <a href="#pret-a-porter" aria-controls="pret-a-porter" role="tab" class="is-active" aria-selected="true">Prêt-à-porter</a>
                </li>
                <li role="presentation">
                  <a href="#beaute" aria-controls="beaute" role="tab" aria-selected="false" tabindex="-1">Beauté</a>
                </li>
              </ul>
            </div>
            
            <!-- Onglet Prët-à-porter -->
            <div class="tab-pane is-active" id="pret-a-porter" role="tabpanel" tabindex="0" aria-labelledby="pret-a-porter-tab">
              
              <div class="luxe-row no-gutter">
                <div class="luxe-col-mobile-12 luxe-col-tablet-10">
                  <div class="animated-article article1">
                    <figure class="image article-image">
                      <a href="" class="trigger_liveshopping" onclick="clickedBrand='Saint Laurent'; appointedWidgetId='5f8878e86a0d547bea7db8fb'; GL_GIS.LiveShopping.checkClerkAvailability('brand19'); return false;">
                        <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/saint-laurent/saint-laurent-accessoires.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/saint-laurent/saint-laurent-accessoires@2x.jpg 2x" 
                        alt="La Collection Saint Laurent, Accessoires - Galeries Lafayette">
                      </a>
                      <figcaption class="text-right">Accessoires</figcaption>
                    </figure>
                  </div>
                </div>
                
                <div class="luxe-col-mobile-10 luxe-col-mobile-offset-2 luxe-col-tablet-3">
                  <div class="has-text-fat text-left animated-article">
                    <p class="article-txt is-uppercase collection-main-text">La Collection <br />Saint Laurent</p>
                  </div>
                </div>
                
                <div class="luxe-col-mobile-2 is-hidden-tablet">
                  <div class="has-text-big has-text-vertical">
                    <p>FW20</p>
                  </div>
                </div>
                <div class="luxe-col-mobile-10 luxe-col-tablet-7 text-right">
                  <div class="animated-article article2">
                    <figure class="image article-image">
                      <a href="" class="trigger_liveshopping" onclick="clickedBrand='Saint Laurent'; appointedWidgetId='5f8878e86a0d547bea7db8fb'; GL_GIS.LiveShopping.checkClerkAvailability('brand19'); return false;">
                        <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/saint-laurent/saint-laurent-pret-a-porter.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/saint-laurent/saint-laurent-pret-a-porter@2x.jpg 2x" 
                          alt="La Collections Saint Laurent, Prêt-à-porter - Galeries Lafayette">
                      </a>
                      <figcaption class="text-right">Prêt-à-porter</figcaption>
                    </figure>
                  </div>
                </div>
              </div>
              
              <div class="luxe-row no-gutter is-pulled-row">
                <div class="luxe-col-mobile-8 luxe-col-mobile-offset-2 luxe-col-tablet-4 luxe-col-tablet-offset-0">
                  <div class="animated-article article3">
                    <figure class="image article-image" style="margin-top:-25px;">
                      <a href="" class="trigger_liveshopping" onclick="clickedBrand='Saint Laurent'; appointedWidgetId='5f8878e86a0d547bea7db8fb'; GL_GIS.LiveShopping.checkClerkAvailability('brand19'); return false;">
                        <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/saint-laurent/saint-laurent-sacs.jpg"
                        srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/saint-laurent/saint-laurent-sacs@2x.jpg 2x" 
                        alt="La Collection Saint Laurent, Sacs - Galeries Lafayette">
                      </a>
                      <figcaption class="text-left has-text-right-tablet">Sacs</figcaption>
                    </figure>
                  </div>
                </div>
                <div class="luxe-col-tablet-1 is-hidden-mobile">
                  <div class="has-text-big has-text-vertical">
                    <p>FW20</p>
                  </div>
                </div>
              </div>              
              
            </div>
            
            
            
            <!-- Onglet beauté -->
            <div class="tab-pane" id="beaute" role="tabpanel" tabindex="0" aria-labelledby="beaute-tab" hidden>              
            </div>
            
            
            
          </div>
        </div>
      </article>
      
      <!-- ARTICLE SHOPPING À DISTANCE -->
      <article class="distance-shopping luxe-row no-gutter text-left" id="shopping-a-distance">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 animated-article">
          <div class="article-body has-text-huge has-text-right-tablet">
            <h3>SHOPPING À<br /> — DISTANCE</h3>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 animated-article">
          <div class="article-body article-txt">
            <h4>Un conseiller Saint Laurent à votre&nbsp;disposition</h4>
            <p>Avec le Shopping à Distance, découvrez les collections Saint Laurent des Galeries Lafayette Paris Haussmann et effectuez vos achats où que vous soyez, en live shopping vidéo avec un conseiller de la marque ou en renseignant votre wishlist avec les pièces dont vous avez envie.</p>
            <a href="#" class="button primary trigger_liveshopping" onclick="clickedBrand='Saint Laurent'; appointedWidgetId='5f8878e86a0d547bea7db8fb'; GL_GIS.LiveShopping.checkClerkAvailability('brand19'); return false;">Acheter en live vidéo<span class="icon"><i class="microphone"></i></span></a>
            <a href="" class="button primary trigger_liveshopping" onclick="clickedBrand='Saint Laurent';" data-reveal-id="popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
            <br /><br />
            <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">En savoir plus</a>
          </div>
        </div>
      </article>
      
      <div class="luxe-row conditions">
        <div class="luxe-col-mobile-12">
          <hr class="is-marginless" />
          <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service#conditions" class="is-text">Conditions générales du Shopping à Distance</a>
        </div>
      </div>
    </div>
  </section>
  
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-vad.min.v01.js"></script>
<div id="goinstore_modals"></div>
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance-goinstore.min.v02.css" rel="stylesheet" type="text/css">
<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-goinstore.min.v07.js"></script>

<!--=========================== FIN LANDING PAGE ========================-->
 
<!-- build:js /media/LP/src/js/2020/shoppingadistance-goinstore.min.v01.js
  <script src="../../../src/js/2020/shoppingadistance-goinstore.js"></script>
endbuild -->

<!-- build:js /media/LP/src/js/2020/shoppingadistance-vad.min.v01.js
<script src="../../../../assets/js/tabs.js"></script>
<script src="../../../../assets/js/ScrollMagic.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
<script src="../../../src/js/2020/shoppingadistance-vad.js"></script>
endbuild -->

<?php include ('../../../pages-defaults/footer.php'); ?>
