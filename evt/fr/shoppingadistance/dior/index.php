<?php include ('../../../pages-defaults/header.php'); ?>
<script>
  document.title = "Dior";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
<!-- <link href="../../../../media/LP/src/css/2020/shoppingadistance.css" rel="stylesheet" type="text/css"> -->
  
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance.min.v05.css" rel="stylesheet" type="text/css">
<style>
  .back-top {
    display: none !important;
  }
  .luxe-augmente {
    margin: 0 auto !important;
  }
  .luxe-augmente .podium-items div[class*=luxe-col-] {
    height: 376px;
  }
  @media screen and (min-width: 768px), print {
    .luxe-augmente .podium-items div[class*=luxe-col-] {
      height: 416px;
    }
  }
  @media screen and (min-width: 1250px) {
    .luxe-augmente .podium-items div[class*=luxe-col-] {
      height: 456px;
    }
  }
</style>
<div class="luxe-augmente template-marque" data-bg-color="">
  <section class="luxe-augmente-hero" style="background-color: #000000;">
    <div class="hero-bg-image" style="background-image:url('https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dior-mix/dior-hero.jpg'); background-position:center top;">&nbsp;</div>
    <div class="container">
      <div class="luxe-row">
        <div class="luxe-col-mobile-12">
          <h1>Dior</h1>
          <a href="#collections" class="button primary has-arrow-down has-smoothscroll">Découvrir les collections</a>
        </div>
      </div>
    </div>
  </section>
  
  <section class="luxe-augmente-body">
    <div class="container">
      <!-- Fil d'Ariane -->
      <nav class="breadcrumb" aria-label="Fil d'Ariane">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com">
              <span itemprop="name">Accueil</span>
            </a>
            <meta itemprop="position" content="1">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/h/createurs">
              <span itemprop="name">Luxe & Créateurs</span>
            </a>
            <meta itemprop="position" content="2">
          </li>
          <li  class="is-active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance/dior" aria-current="page">
              <span itemprop="name">Dior</span>
            </a>
            <meta itemprop="position" content="3">
          </li>
        </ul>
      </nav>
        
      <!-- INTRO -->
      <article class="intro luxe-row  luxe-center-mobile">
        <div class="luxe-col-tablet-7 luxe-col-desktop-5 luxe-col-fullhd-4">
          <h3 class="is-uppercase">La maison Dior</h3>
          <div class="intro-txt">
            <p>Christian Dior fut le couturier du rêve. Dès la fondation de sa Maison en 1946, consacrée par la révolution du New Look, son esprit visionnaire n’a eu de &nbsp;
              <span class="read-more"><span class="read-more-toggle">&hellip; suite</span></span>
              <span class="read-more-content is-hidden"> cesse de magnifier les femmes du monde entier. « Le tissu est le seul véhicule de nos rêves (…). La mode, en somme, est issue d’un rêve, et le rêve, c’est une évasion » écrivait-il. Au gré des saisons, cet héritage d’exception est réinventé par la passion créative des différents directeurs artistiques qui – de la couture aux parfums – font perdurer la magie Dior.&nbsp;
                <span class="read-less-toggle">Fermer</span>
              </span>
            </p>
          </div>
        </div>
      </article>
      
      <div  id="collections"></div>
      
      <!-- ARTICLE 1 -->
      <article class="luxe-row no-gutter luxe-middle-mobile animated-article article1">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
          <div class="article-body">
            <h2>La mode femme en Shopping à Distance</h2>
            <p>Féerique célébration des arts traditionnels des Pouilles, la collection croisière 2021 fait rayonner et réinvente l’excellence de savoir-faire inestimables. De la beauté onirique des Luminarie, sublimée en de flamboyants motifs, aux fleurs sauvages esquissées par Pietro Ruffo, en passant par les accessoires, sacs et souliers au cuir finement ajouré ou sculpté, les créations de Maria Grazia Chiuri sont autant d’objets de désir façonnés par un artisanat d’exception. <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">En savoir plus</a></p>
            <a href="" class="button outlined" onclick="initWhislist(); return false;" data-reveal-id="blkr-popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dior-mix/dior-mode-femme.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dior-mix/dior-mode-femme@2x.jpg 2x" 
                alt="Les collections Dior mode femme en Shopping à Distance - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
        
      <!-- ARTICLE 2 -->
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse animated-article article2">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
          <div class="article-body">
            <h2>La mode homme en Shopping à Distance</h2>
            <p>Judy Blame était un alchimiste. Iconoclaste, il se réappropriait les objets les plus élémentaires pour imaginer des bijoux captivants devenant de véritables œuvres d’art. Aujourd’hui, Kim Jones, Directeur Artistique des collections homme de Dior, rend hommage à son univers passionnant, à sa créativité insatiable, en les conjuguant à l’héritage haute couture de Christian Dior, à l’occasion de sa collection Hiver 2020-2021. Un dialogue riche de sens et de modernité, qui transcende les modes et les frontières. <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">En savoir plus</a></p>
            <a href="" class="button outlined" onclick="initWhislist(); return false;" data-reveal-id="blkr-popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dior-mix/dior-mode-homme.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dior-mix/dior-mode-homme@2x.jpg 2x" 
                alt="Les collections Dior mode homme en Shopping à Distance - Galeries Lafayette" width="620" height="620">
            </figure>
          </div>
        </div>
      </article>
      
      <!-- ARTICLE 3 -->
        <article class="luxe-row no-gutter luxe-middle-mobile animated-article article3">
          <div class="luxe-col-mobile-12 luxe-col-tablet-6 text-left">
            <div class="article-body">
              <h2>La collection privée Maison Christian Dior en Shopping à Distance</h2>
              <p>Maison Christian Dior est une invitation à parfumer tous les moments de la vie. Pour soi comme pour offrir, les collections de parfums, de bougies, de savons et d’accessoires composent un véritable art de vivre parfumé. L’occasion pour François Demachy, parfumeur-créateur Dior, d’exprimer une large palette olfactive inspirée du savoir-faire unique de Dior, des fleurs d’exceptions et d’ingrédients précieux. <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service" class="is-text">En savoir plus</a></p>
              <a href="" class="button outlined" onclick="initWhislist(); return false;" data-reveal-id="blkr-popin-live-shopping-form" target="_self">Renseigner votre wishlist</a>
            </div>
          </div>
          <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
            <div class="article-image">
              <figure class="image">
                <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dior-mix/dior-collection-privee.jpg"
                  srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dior-mix/dior-collection-privee@2x.jpg 2x" 
                  alt="La collection privée Dior en Shopping à Distance - Galeries Lafayette" width="620" height="620">
              </figure>
            </div>
          </div>
        </article>
      
      
      <!-- SUR LE PODIUM -->
      <article class="podium">
        <div class="luxe-row no-gutter text-center">
          <div class="luxe-col-mobile-12 has-text-fatt">
            <h3 class="is-uppercase">La beauté en ligne</h3>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center podium-items" style="background-color: #f0f0f0;">
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/j+adore+eau+de+parfum+infinissime-dior/73257427/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dior-mix/dior-j-adore.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dior-mix/dior-j-adore@2x.jpg 2x" 
                    alt="Dior J’adore Eau de Parfum Infinissime - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>DIOR <br />J’adore Eau de Parfum Infinissime</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/sauvage+parfum-dior/65272621/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dior-mix/dior-sauvage.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dior-mix/dior-sauvage@2x.jpg 2x" 
                    alt="Dior Sauvage Parfum - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>DIOR <br />Sauvage Parfum</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/rouge+diorrouge+a+levres+rechargeable+couleur+couture+4+finis+satin+mat+metallique+et+velours-dior/74998650/190002" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dior-mix/dior-rouge-a-levre-rechargeable.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dior-mix/dior-rouge-a-levre-rechargeable@2x.jpg 2x" 
                    alt="DIOR Dior Rouge à Lèvres Rechargeable Couleur Couture - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>DIOR <br />Rouge Dior Rouge à Lèvres <br />Rechargeable Couleur Couture</p></div>
            </a>
          </div>
          <div class="luxe-col-mobile-6 luxe-col-tablet-3">
            <a href="https://www.galerieslafayette.com/p/capture+totale+super+potent+rich+cremecreme+riche+anti-age+global-dior/74997309/378" class="animated-article">
              <div class="podium-image luxe-middle-mobile">
                <figure class="image">
                  <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dior-mix/dior-capture-totale.jpg"
                    srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/dior-mix/dior-capture-totale@2x.jpg 2x" 
                    alt="DIOR Capture Totale Super Potent Rich Creme Anti-âge Global - Galeries Lafayette" width="249" height="292">
                </figure>
              </div>
              <br />
              <div class="luxe-middle-mobile podium-txt"><p>DIOR<br />Capture Totale Super Potent Rich <br class="is-hidden-touch" />Creme Anti-âge Global</p></div>
            </a>
          </div>
        </div>
        
        <div class="luxe-row luxe-middle-mobile text-center">
          <div class="luxe-col-mobile-12">
            <a href="https://www.galerieslafayette.com/b/dior" class="podium-cta"><span class="plus"></span>Voir tous les articles</a>
          </div>
        </div>
      </article>
      
      
      <div class="luxe-row conditions">
        <div class="luxe-col-mobile-12">
          <hr class="is-marginless" />
          <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance-service#conditions" class="is-text">Conditions générales du Shopping à Distance</a>
        </div>
      </div>
      
    </div>
  </section>
</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-mix.min.v03.js"></script>

<div id="live_modals" style="height:0;"></div>
<link href="https://static.galerieslafayette.com/media/LP/src/css/2021/shoppingadistance-goinstore-v2.min.css" rel="stylesheet" type="text/css">
<script src="https://static.galerieslafayette.com/media/LP/src/js/2022/shoppingadistance-bleeker.min.v03.js"></script>

<script>
  (function() {
    let scripts = Array
    .from(document.querySelectorAll('script'))
    .map(scr => scr.src);
    const url = new URL(window.location.href);
    var search = url.search;
    var paramsStr = search.substr(1);
    if (!scripts.includes('https://widget.bleeker-st.com/gl/embed-dev.js')) { 
      var scriptNode = document.createElement('script');
      scriptNode['id'] = 'bleeker-script';
      scriptNode['src'] = 'https://widget.bleeker-st.com/gl/embed-dev.js'; scriptNode['type'] = 'text/javascript'; document.documentElement.appendChild(scriptNode);
    }
    window.onload = function() {
      initBleekerLiveShopping({
        params: paramsStr,
        vendorID: 'GL',
        uaGoogle : 'UA-4094055-1',
        widget: true,
        type: 'overlay', 
      });
    } 
  })();
  
  var brandID='Dior';
  var brand='Dior';
  var posterName='dior-v1';
  function initWhislist() {
    brand; 
    posterName;
    GL_BLKR.LiveShopping.displaySadBrand();
  }
</script>
<!--=========================== FIN LANDING PAGE ========================-->
  
<!-- build:js /media/LP/src/js/2020/shoppingadistance-goinstore.min.v03.js
  <script src="../../../src/js/2020/shoppingadistance-goinstore.js"></script>
endbuild -->

<!-- build:js /media/LP/src/js/2020/shoppingadistance-mix.min.v02.js 
<script src="../../../../assets/js/ScrollMagic.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/animation.gsap.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/TweenMax.min.js"></script>
<script src="../../../../assets/js/ScrollMagic-plugins/debug.addIndicators.min.js"></script>
<script src="../../../src/js/2020/shoppingadistance-mix.js"></script>
endbuild -->
  
<?php include ('../../../pages-defaults/footer.php'); ?>
