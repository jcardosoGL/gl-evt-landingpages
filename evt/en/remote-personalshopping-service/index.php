<?php include ('../../pages-defaults/header.php'); ?>
<script>
  document.title = "EN Service";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->

<!-- <link href="../../../media/LP/src/css/2020/shoppingadistance-service.css" rel="stylesheet" type="text/css"> -->
    
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance-service.min.v02.css" rel="stylesheet" type="text/css" />
<style>
  html {
    scroll-behavior: smooth;
  }
</style>
<div class="luxe-augmente" data-bg-color="">
  <section class="luxe-augmente-body" style="border-top: none;">
    <div class="container">
      <!-- Fil d'Ariane -->
      <nav class="breadcrumb" aria-label="Fil d'Ariane">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com">
              <span itemprop="name">Home</span>
            </a>
            <meta itemprop="position" content="1">
          </li>
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/evt/en/remote-personalshopping" aria-current="page">
              <span itemprop="name">Remote Personal Shopping</span>
            </a>
            <meta itemprop="position" content="2">
          </li>
          <li  class="is-active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/evt/en/remote-personalshopping-service" aria-current="page">
              <span itemprop="name">Q&A</span>
            </a>
            <meta itemprop="position" content="3">
          </li>
        </ul>
      </nav>
      
      <div class="luxe-row">
        <div class="luxe-col-mobile-12">
          <article class="faq">
            <h1>Q&A — Remote Personal Shopping</h1>
            <div class="accordions text-left">
              
              <div class="accordion">
                <div class="accordion-header">
                  <p>What is Remote Personal Shopping&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>For 125 years, Galeries Lafayette Paris Haussmann has celebrated creativity and curated an inspiring selection of fashion brands. Today, the most iconic of Parisian department stores leaves its physical walls behind to meet all its customers across the world. With the REMOTE PERSONAL SHOPPING service, access the finest Luxury & Designer brands, wherever you are.</p>
                    <p class="is-bold">Two options are available :</p> 
                    <ul>
                      <li><p>The Live Video Shopping by appointment with the brand of your choice or one of our Personal Shoppers.</p></li>
                      <li><p>The Wishlist, a form allowing you to easily request a list of items you would like to our team, who will contact you by phone within 48 hours.</p></li>
                    </ul>
                    <p>This service is completely free and without any purchasing obligation.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>How does Live Video Shopping work&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Live Shopping allows you to shop at Galeries Lafayette Haussmann from home.
                      You can chat via Live Video with a Brand Advisor or a Personal Shopper, and shop among our finest Luxury & Designer brands.</p>
                    <p>How? By choosing a brand available with this service or by contacting a Personal Shopper directly [ajouter lien]. Click on “Make a Live Video Appointment”, you can then make an appointment at your convenience.
                      </p> 
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>What equipment do you need to make a Live Shopping Video&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>A computer or a smartphone with an internet connection is sufficient, the Live Video Shopping is done directly without having to download an application.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Is there a minimum purchase amount to benefit from this service&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>No minimum purchase amount, this service is completely free.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>How much does this service cost&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>This service is completely free.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>I had a technical problem during my Live Video Shopping, what can I do&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>In case of unexpected disconnection during a Live Video, your contact person will do his best to recontact you by immediately logging back into the Live Video, or by contacting you by phone if you have given your phone number.</p>
                    <p>In case of failure, don't hesitate to make a new appointment or fill in your Wishlist.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>What is the Wishlist&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Do you know exactly what you want? Whether it’s a classic from your favorite Luxury brand, or a trendy piece from the latest fashionable designer, do not hesitate to fill out a wishlist: a simple and quick form allowing you to request a set of items directly to our teams at Galeries Lafayette Haussmann.</p>
                    <p>Our teams will get back to you within 48 hours by phone to confirm the availability of the requested product(s). You can then make a remote payment and choose your delivery method.
                    </p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>What is a Personal Shopper&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Our team of Personal Shoppers are passionate fashion experts dedicated to help you put together a wardrobe reflecting your style and provide personalised advice. They will know how to find the ideal piece, for yourself or as a gift.
                    </p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>How does the payment work&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>— Once the purchase is validated, you will receive a quote with the requested products and delivery costs, as well as a payment link.<br /> 
                      — If you reside outside of Europe, the quote will include a tax-free total for the order.<br />
                      — By clicking on this link, you will be directed to a secure payment platform.<br />
                      — This link is valid for 48 hours, during which the availability of the selected products is guaranteed.<br />
                      — Once your payment has been processed, you will receive an order confirmation.
                    </p>
                    <p class="is-bold">Means of payment :</p>
                    <ul>
                      <li><p>Payment by credit card via a secure payment link: cards from the CB network, Visa, Eurocard / Mastercard / American Express</p></li>
                      <li><p>Payment by bank transfer using the bank account details sent via email by the Remote Personal Shopping service</p></li>
                      <li><p>Payment by Alipay or Wechat pay</p></li>
                      <li><p>Payment by Galeries Lafayette gift card or loyalty points</p></li>
                      <li><p>Payment in several installments free of charge is not available at the moment, but we are doing our best to develop it quickly!</p></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>Can I modify or cancel my order&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>You can modify the selection of Products by indicating the quantities and / or Product (s) to be modified directly to the Remote Personal Shopping customer service via email following your quote or payment confirmation.</p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>What are the delivery methods and costs and return policy for Remote Personal Shopping&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>Home delivery costs 15€ in France and 30€ in the European Union.</p>
                      
                    <p class="is-bold">Shipping costs are free :</p>
                    <ul>
                      <li><p>for customers holding a Galeries Lafayette loyalty card with Exception & Premium status</p></li>
                      <li><p>for all customers with an order value above 600€</p></li>
                      <li><p>for any Click & Collect delivery in a Galeries Lafayette store</p></li>
                    </ul>
                    <p>International delivery prices (outside the European Union) vary according to order value for all Customers</p>
                    <ul>
                      <li><p>less than 2000€ of purchases = 60 € of delivery costs</p></li>
                      <li><p>from 2000€ to 4000€ of purchases = 100 € of delivery costs</p></li>
                      <li><p>from 4000€ to 8000€ of purchases = 150 € of delivery costs</p></li>
                      <li><p>from 8,000€ to 15,000€ of purchases = 250€ delivery costs</p></li>
                      <li><p>from 15,000€ to 20,000€ of purchases = 400€ delivery costs</p></li>
                      <li><p>over 20,000€ of purchases = 500€ delivery costs</p></li>
                    </ul>

                    <p>For orders placed outside of the European Union, please note that depending on the destination country, the customer may be required to pay all applicable taxes and custom duties upon receipt of the products in the destination country.</p>
                    
                    <p>Please contact our Remote Personal Shopping customer service for more information about jewelry and watches delivery fees.</p>
                    
                    <p class="is-bold">Regions covered by the delivery</p>
                    <p>Deliveries are available in France, the European Union and worldwide with a few exceptions : <br />
                      Deliveries to Brazil and Russia are unfortunately not available.<br  />
                      Deliveries to China have specific restrictions, please contact our Remote Personal Shopping customer service for more information.
                    </p> 
                    
                    <p class="is-bold">Returns :</p>
                    <p>We hope you love your purchase but if you’ve changed your mind and wish to return an item, we invite you to contact customer service by e-mail at: <a href="mailto:shoppingadistance@galerieslafayette.com">shoppingadistance@galerieslafayette.com</a>. Please indicate your order number when expressing your intention to return a product.</p> 
                    <p>Customer service will communicate the next steps to take for an easy return.</p>
                    <p>Return fees are free from France and the European Union.<br />
                    Return fees from outside of the European Union are the responsibility of the customer.<br />
                    Returns must be made within 30 days.
                    </p>
                  </div>
                </div>
              </div>
              <div class="accordion">
                <div class="accordion-header">
                  <p>What happens with the data shared in the context of Remote Personal Shopping&nbsp;?</p>
                  <button class="toggle" aria-label="toggle"></button>
                </div>
                <div class="accordion-body">
                  <div class="accordion-content">
                    <p>If you wish to make an appointment or purchase an item through our Remote Personal Shopping service, certain personal data must be collected for you to access the service.</p>
                      
                    <p>When connecting to Live Shopping Video, no personal data is required.</p>
                      
                    <p>To find out more, feel free to visit : <a href="https://haussmann.galerieslafayette.com/en/privacy-policy-for-export-sales-service/">https://haussmann.galerieslafayette.com/en/privacy-policy-for-export-sales-service/</a></p>
                  </div>
                </div>
              </div>
              
            </div>
            
            <div class="text-left">
              <p>If you have any problems or questions, you can contact our customer service <a href="https://haussmann.galerieslafayette.com/en/your-customer-service/">here</a> or via email at &nbsp;:&nbsp;<a href="mailto:shoppingadistance@galerieslafayette.com">shoppingadistance@galerieslafayette.com</a></p>
            </div>

          </article>
        </div>
      </div>

    </div>
  </section>
  
  <!-- CGUtilisation -->
  <section class="luxe-augmente-body" id="conditions">
    <div class="container">
      <div class="luxe-row">
        <div class="luxe-col-mobile-12">
          <article class="cgu">
            <h2>General terms and conditions of use — Remote Personal Shopping</h2>
            <div class="text-left">
              <p class="is-bold">1 - Foreword</p>
              <p>Always seeking exceed customer expectations, the company 44 GALERIES LAFAYETTE - 44 GL (hereinafter called “Galeries Lafayette”), a simplified share company (SAS) incorporated in France with capital of 217,404,572 euros, whose registered office is located in the 9th district of Paris, 27 rue de la Chaussée d'Antin, registered under the number 552 116 329 RCS PARIS, offers to access a large part of the Galeries Lafayette Haussmann store selection through a remote personal shopping service ( hereinafter, the "Service").</p>
              <p>The purpose of these general terms and conditions of use (hereinafter the "T&Cs") is to define the rights and obligations of Galeries Lafayette and their customers with regards to the operation of the Service.</p>
              
              <br />
              <p class="is-bold">2 - Scope of application</p>
              <p>These T&Cs define the terms and conditions under which the customer accesses and benefits from the remote personal shopping Service.</p> 
              <p>Any use of the Service by the customer implies full and unreserved acceptance of the T&Cs. The customer acknowledges having full knowledge of it and thereby waives the right to rely on any contradictory document.</p>
              <p>The relationship between the customer and Galeries Lafayette will always be governed by the latest conditions in force on the day of the customer's use of the Service. In the event of modification of the T&Cs, the customer will be invited to accept the new conditions, which will apply to him on the day of acceptance. The customer is invited to regularly consult the T&Cs in order to be aware of any changes. Use of the Service constitutes acceptance of the T&Cs in their latest version on the day of use of the Service.</p>
               
              <br />
              <p class="is-bold">3 - Service</p>
              <p>The Service is accessible through three (3) different channels, as defined below :</p>
              <p>Clients can directly access the service through the webpage <a href="https://www.galerieslafayette.com/evt/en/remote-personalshopping">https://www.galerieslafayette.com/evt/en/remote-personalshopping</a>, and will then be able to :</p>
              <ul>
                <li><p>Complete a Wishlist allowing to request the availability of one or more products. The client will be contacted as soon as possible by phone to respond to this request.</p></li>
              </ul>
              <p>The dates and times indicated are subject to change in the context of Covid-19.</p> 
              
              <br />
              <p class="is-bold">4 - Obligations related to the use of the Service</p>
              <p>In order to use the Service, Customers commit&nbsp;:</p>
              <ul>
                <li><p>to declare complete and accurate information when registering, and update this information if there is any change ;</p></li>
                <li><p>not to use the Service for purposes that are fraudulent or not provided for in the T&Cs ;</p></li>
                <li><p>not to impersonate or attempt to use an account other than their own ;</p></li>
                <li><p>not to use pseudonyms or use expressions suitable for being received as verbal attacks by Galeries Lafayette and its sales staff and / or demonstrators ;</p></li> 
                <li><p>not to behave inappropriately, or in any way which would be perceived as such by Galeries Lafayette, and its sales staff and / or demonstrators.</p></li>
              </ul>
              <p>In all circumstances, the customer will refrain from capturing and / or recording and / or broadcasting any screenshot of conversations with sales staff and / or demonstrators on any means of electronic or telephone communication.</p>
              
              <p>If behavior as mentioned above is noticed, Galeries Lafayette reserves the right to terminate the performance of the Service at its discretion at any time, free from any liability.</p> 
              
              <br />
              <p class="is-bold">5 - Privacy policy</p>
              <p>Collection of personal data (hereinafter called "Data"), is mandatory and necessary to allow the Service to function and be accessed.</p> 
              <p>Galeries Lafayette is responsible for data processing.</p> 
              <p>Galeries Lafayette guarantees that none of the customer video images collected for the use of the Service will be kept.</p> 
              <p>To find out more about Galeries Lafayette's Privacy Policy, you can click on this link :  <a href="https://haussmann.galerieslafayette.com/en/privacy-policy-for-export-sales-service">https://haussmann.galerieslafayette.com/en/privacy-policy-for-export-sales-service</a> or ask to consult the Privacy Policy in store.</p>
              
              <p>Galeries Lafayette guarantees that access to the Service is secure, in particular that access to live images is strictly protected and can only be accessed by customers, Galeries Lafayette staff or brands.</p> 
              
              <br />
              <p class="is-bold">6 - Responsibility</p>
              <p>Galeries Lafayette cannot be held liable if the non-performance or improper performance of its obligations is attributable to the customer, or to a case of force majeure in accordance with article 1218 of the French Civil Code.</p>
              <p>In the event that imperative reasons (in particular context of uncertainty related to the Coronavirus epidemic and its consequences) and / or a fortuitous event arise or in the case of a government or administrative decision and / or an imperative company decision (in particular the impossibility of opening a store), preventing Galeries Lafayette or the Customer from fulfilling their obligations, the other Party must be informed without delay.</p> 
              <p>Galeries Lafayette is not responsible for any damage of any kind that may be suffered by the Customer during a visit to websites and / or use of services, applications or digital tools.</p> 
              <p>The use of the Service page and / or digital tools implies the acknowledgement and acceptance by the Customer of the characteristics as well as the limits of the Internet and related technologies, the absence protection of certain data against possible misappropriation or pirating and risks of contamination by possible viruses circulating on the network. Galeries Lafayette declines all responsibility in the event of misuse or incidents related to the use of the computer, Internet access, maintenance or malfunction of servers, telephone lines or any other technical connection, and in the event of forms being sent to incorrect or incomplete address, of any computer errors or faults on the page of the Service page.</p>
              <p>Use of the Service is likely to result in the consumption of part of the customer’s data transmission subscription plan with a mobile and / or internet operator. Any use of the Service from abroad is likely to incur considerably higher costs than those of use from France. Being informed of the relevant charges billed by the customer's operator, including roaming charges, is the customer’s responsibility.</p>
              
              <br />
              <p class="is-bold">7 - Purchase via the Service</p>
              <p>Any purchase through the Service implies full acceptance of the Galerie Lafayette privacy policy and the General Terms and Conditions of Sale of the Service.</p> 
            </div>
          </article>
        </div>
      </div>
    </div>
  </section>
  
  <!-- CGVente -->
    <section class="luxe-augmente-body marques" id="vente">
      <div class="container">
        <div class="luxe-row">
          <div class="luxe-col-mobile-12">
            <article class="cgv">
              <h2>General terms and conditions of sale - Galeries Lafayette Haussmann — Remote Personal Shopping</h2>
              <p class="is-uppercase is-bold">Updated - OCTOBER 2020</p>
              <div class="text-left">
                <p class="is-bold">1 - Identification of parties</p>
                <p class="is-bold">Vendor :</p> 
                
                <p>Unless otherwise specified, the vendor of the products and services offered in the store is :</p> 
                
                <p>La Société Galeries Lafayette Haussmann - GL HAUSSMANN a simplified share company (SAS) incorporated in France with share capital of 217,404,572 euros, whose registered office is located in the 9th district of Paris, 27 rue de la Chaussée d'Antin, registered with the Trade and Companies Registry of Paris under the number 572 062 594</p>
                
                <p><span class="is-bold">Galeries Lafayette Haussmann – Shopping à Distance<br />
                TSA 44100<br />
                75446  PARIS CEDEX 09<br />
                FRANCE</span><br />
                Tél : 01 42 82 38 08</p>
                
                <p>Galeries Lafayette offers products and services for sale (hereinafter “Products”) intended for individuals purchasing one or more Product(s) for personal purposes and not for resale. Galeries Lafayette reserves the right to refuse any sale if it is suspected of not to comply with these terms.</p> 
                
                <p class="is-bold">1 - The customer :</p> 
                
                <p>Only individuals that are legally capable of entering into contracts concerning the Products for sale may place an order. When placing the order, the Customer guarantees to have full legal capacity to adhere to these general conditions of sale and thus conclude the sale.</p>
                
                <p>Certain Products offered for sale can be restricted for adults only. When ordering this type of Products, the Customer undertakes to be eighteen (18) years old at the date of the order. Galeries Lafayette reserves the right to request proof of identity upon delivery.</p>
                
                <p class="is-bold">2 - Purpose</p>
                
                <p>The purpose of these general terms and conditions of sale is to define the rights and obligations of Galeries Lafayette Haussmann and their Customers in the context of remote selling of products offered in stores. Unless otherwise specified to the Customer, the eligible Products are the Products sold in the Galeries Lafayette Haussmann store. The Customer is informed that certain Products or certain brands may not be eligible for remote purchases.</p> 
                
                <p>Any purchase implies the acceptance of these general terms and conditions of sale, which must be read by the Customer who acknowledges their unconditional acceptance before validating any order by email.</p>
                
                <p>The present general terms and conditions of sale will be addressed to the Customer on a lasting electronic medium.</p>
                
                <p class="is-bold">3 - Product Prices</p>
                
                <p>The price of the Product (s) is indicated in Euros all taxes included, and confirmed during the validation of the order. The price of the displayed Products may vary depending on current promotional offers and may be different from the prices charged in other Galeries Lafayette stores and / or e-commerce websites. The store does not have any obligations to apply prices found in other stores or on the Internet.<br />
                  For all Customers residing outside of the European Union, the quote will include a tax-free total.</p>
                
                <p>Unless otherwise indicated, promotions and benefits cannot be added up together. Certain promotions may be reserved to a specific clientele; the conditions of access to these offers are available in store.</p> 
                
                <p>Additional delivery costs may be added depending on the type and amount of the order.The delivery costs are communicated, at the latest before the order confirmation is placed.</p> 
                
                <p>Payment in cash is made according to the means of payment and currencies accepted and reported.</p> 
                
                <p>The presentation of official and valid proof of identity may be required, if applicable, upon payment.</p>
                
                
                <p class="is-bold">4 - Product order</p> 
                
                <p>This article details the methods of ordering the Product (s).</p> 
                
                <p class="is-bold">Order process :</p> 
                
               <p>Any order may be placed by the Customer via the “Remote Personal Shopping” ordering service.</p> 
                
                <p>The Customer agrees to provide the following requested information and guarantees its veracity :</p>
                <ul>
                  <li><p>first and last name : information necessary to identify the owner of the order</p></li>
                  <li><p>postal address : information required for home delivery</p></li>
                  <li><p>telephone: information necessary for delivery tracking</p></li>
                  <li><p>email address : information required for sending order confirmation emails and delivery tracking</p></li></ul>
                
                <p>After having made a selection of Product(s) with the sales staff of the Galeries Lafayette store, the Customer receives an order confirmation email summarizing the selection, price of the Product(s) and shipping cost details.</p>  
                <p>The Customer can either :</p> 
                <ul>
                  <li><p>Confirm the order by proceeding to payment.</p></li>
                  <li><p>Modify the selection of Products by indicating the quantities and / or Product(s) to be changed directly with the brand or Galeries Lafayette Customer Service. In this case, the Customer will receive a new order confirmation email.</p></li> 
                </ul>
                <p>As soon as the Customer's order is confirmed, the Customer receives an email to proceed with the payment for the order.</p> 
                
                <p class="is-bold">Payment :</p> 
                
                <p>The Customer agrees to pay the full price of the Product at the time of the order. By proceeding to the payment, the Customer definitively validates his order and agrees to pay the full price in one go.</p>
                
                <p>Three payment methods are available for the Customer :</p> 
                <ul>
                  <li><p>Payment by bank transfer using the bank account details sent by the remote shopping service.</p></li>
                  <li><p>Payment by credit/debit card via a secure payment link (cards from the CB network, Visa, Eurocard, Mastercard, American Express).</p></li>
                  <li><p>Payment by Alipay or Wechat pay.</p></li> 
                </ul>
                <p>No other payment method is allowed. Payment is made in Euros (€).</p>
                
                <p>The payment order made by the Customer cannot be canceled. Payment of the order by the Customer is irrevocable, without prejudice to the Customer to exercise his right of withdrawal.</p>
                
                <p>The Customer confirms and guarantees that he is the holder of the means of payment used for payment and that the latter provides access to sufficient funds to cover the full amount necessary for the payment of the order. The Customer's account will be debited as soon as the order is ready to be shipped.</p> 
                
                <p class="is-bold">5 - Home delivery</p> 
                
                <p>The costs of home deliveries in France and the European Union are :</p>
                <ul>
                  <li><p>15€ for France</p></li>
                  <li><p>30€ for the European Union</p></li>
                </ul>
                <p>By derogation, the delivery costs are gifted to :</p>
                <ul>
                  <li><p>Customers holding a Galeries Lafayette loyalty card with Exception & Premium status</p></li>
                  <li><p>All Customers with an order value above the purchase threshold of 600€ in France or EU</p></li>
                </ul>
                <p>International delivery prices (outside the European Union) vary according to order value for all Customers :</p>
                <ul>
                  <li><p>less than 2000€ of purchases = 60 € of delivery costs</p></li>
                  <li><p>from 2000€ to 4000€ of purchases = 100 € of delivery costs</p></li>
                  <li><p>from 4000€ to 8000€ of purchases = 150 € of delivery costs</p></li>
                  <li><p>from 8,000€ to 15,000€ of purchases = 250€ delivery costs</p></li>
                  <li><p>from 15,000€ to 20,000€ of purchases = 400€ delivery costs</p></li>
                  <li><p>over 20,000€ of purchases = 500€ delivery costs</p></li>
                </ul>
                <p>For orders placed outside of the European Union, depending on the destination country, the customer may be required to pay all applicable taxes and custom duties upon receipt of the products in the destination country.</p> 
                
                <p>For shipments of high-value goods (fine watchmaking, fine jewelry, leather goods made with precious skins or from protected species) that require secure transport, a quote for each shipment is made by the remote personal shopping service upon the request of brands. The amount of the fee is communicated to the Customer by the involved brand before acceptance of the order.</p>
                
                <p>a) Regions covered by the delivery</p>
                
                <p>Deliveries are available in France and worldwide in eligible countries.</p> 
                <p>Deliveries to Brazil and Russia are unfortunately not available.</p> 
                <p>Deliveries to China are restricted and it is the Customer’s responsibility to contact the Remote Personal Shopping customer service to find out eligible regions for delivery.</p> 
                
                <p>b) Special cases of certain Products</p>
                
                <p>Depending on the laws specific to certain countries, the Customer is informed that certain Products are prohibited for export. These Products cannot be delivered in the regions prohibiting their entry. This especially but not exclusively regards animal skins, animal leathers, furs, perfume products, alcohol, etc...<br />
                In addition, perfumery products are not shipped internationally.<br />
                The Customer is invited to contact the Remote Personal Shopping customer service to find out more about restrictions applied to the delivery of selected Products.</p> 
                
                <p>c) Home delivery</p>
                
                <p>The Product will be delivered to the address specified by the Customer when ordering. An indicative delivery time will be communicated to the Customer and in any case it may not exceed 30 days from the date of the order, except for special provisions or fortuitous events. Packages are delivered upon a signature.</p> 
                
                <p>The transfer of risk occurs upon delivery, when the Product is handed over.</p>
                
                <p>It is strongly recommended that the Customer check the condition and conformity of the package upon delivery. If the package appears damaged or open, the Customer is advised to formulate remarks to express dissatisfaction on the delivery receipt issued by the deliverer. If the package appears empty, the Customer must refuse delivery and contact Galeries Lafayette Customer Service. In order to be valid with regards to the carrier, any complaint relating to an apparent defect or damage during the delivery of a Product must be communicated without delay upon receipt of the Product (s), by e-mail at: <a href="mailto:shoppingadistance@galerieslafayette.com">shoppingadistance@galerieslafayette.com</a> or letter addressed to the Galeries Lafayette Customer Service. Customer Service contact details can be found at the top of this document. In the event of damage, the Customer must detail the damages precisely.</p>
                
                <p>In addition, a formal complaint filing may be necessary to justify the non-receipt of the Product.</p> 
                
                <p>In the event of several unsuccessful delivery attempts to the Customer's address, the order will be returned to the store. It is the Customer's responsibility to contact Customer Service to reschedule a new delivery at the Customer’s own expense.</p> 
                
                <p class="is-bold">6 - Click and Collect delivery</p>
                 
                <p>The Product will be delivered free of charge to the Click and Collect counter of the Galeries Lafayette store of the Customer’s choice in France and subject to the service being available in the store.</p>  
                
                <p>The Customer will be informed of the date of availability of his parcels at the Click & Collect counter of his chosen store.</p> 
                
                <p>At the time of withdrawal, the Customer must present an original identity document (passport, identity card, driver's license or residence permit).</p>
                
                <p>If the package is collected by a third party individual on behalf of the Customer, this individual must present their original identity document, the original identity document of the Customer, as well as a letter of proxy.</p>
                
                <p>The entire order must be verified with the Customer: it will be considered complete and compliant after signature by the Customer.</p>
                
                <p>For health reasons, in-store fittings may be allowed or not. The Customer will be informed on the spot of the hygiene measures to be followed.</p> 
                
                <p class="is-bold">7 - Cancellation right</p>
                
                <p>Any Customer who is not satisfied with the Products ordered may exercise their right of withdrawal. In accordance with articles L 221-18 et seq. of the French Consumer Code, the Customer has fourteen (14) clear days from the receipt of the parcel containing the ordered Product to exercise the right of withdrawal, with the exceptions and limitations set out in points b) and c) of this article. <span class="is-bold">Products that are special orders or have been altered or have been personalized are not eligible for withdrawal.</span></p> 
                
                <p>The right of withdrawal can be exercised by the Customer by any formal notice expressing his willingness to withdraw.</p> 
                
                <p class="is-bold is-uppercase">WITHDRAWAL STATEMENT TEMPLATE</p> 
                
                <p>I hereby notify you of my withdrawal from the contract relating to the sale (*) of the following good / service………………………...<br />
                Order received on ……………………..<br />
                n° of my order : <br />
                Last name: <br />
                Address: <br />
                Date: <br />
                Signature.</p> 
                
               <p>In the case of an order for several Products delivered separately or for a Product made up of multiple parts requiring multiple deliveries, the withdrawal period starts from receipt of the last Product or part.</p>
                
                <p>The proof of the effective exercise of the right of withdrawal is the Customer’s responsibility.</p>
                
                <p>a) legal 14 day period</p> 
                
                <p>The Customer must return the Product without undue delay and at the latest within fourteen (14) days following the communication of his decision to withdraw.</p>
                
                <p>The Customer returning the Product within this period has the right to a refund of the price of the Product ordered and to the reimbursement of outgoing delivery costs on the basis of a standard delivery fee, regardless of the delivery method selected by the Customer for his order.</p> 
                
                <p>Returns are free for purchases delivered in France and the European Union.</p> 
                
                <p>For international deliveries outside the European Union, return costs, government taxes and customs formalities are at the Customer’s expense.</p>
                
                <p>Before any return, the Customer must send an email to Customer Service, which will indicate the terms of return.</p>
                
                <p>Direct return to a Galeries Lafayette store is not possible.</p> 
                
                <p>Reimbursement of returned Products at the invoiced price, including outbound delivery costs, will be made using the same means of payment as that used to pay for the Order. The reimbursement will be made within fourteen (14) days following receipt of the Customer's formal notice expressing the decision to withdraw. Galeries Lafayette reserves the right to defer the reimbursement until retrieval of the Product or receipt of proof of shipment of the Product by the Customer, in which case the receipt date will be considered the first day of the 14 day period.</p> 
                
                <p>If the Customer only withdraws from a part of the order, the price invoiced for the returned Products will be refunded, but the delivery fee will not. </p>
                
                <p>In the event of partial withdrawal of the order, the Customer who had benefited from free delivery thanks to an initial order amount exceeding a certain threshold, may be charged for the delivery costs corresponding to his final actual order, if the latter fell below the threshold for free delivery.</p>
                
                <p>b) Additional return period</p>
                
                <p>As a commercial gesture, the aforementioned legal withdrawal period is supplemented by a possibility of return of sixteen (16) additional clear days, with reimbursement of the Product ordered.
                  The reimbursement of returned Products at the invoiced price will be made by crediting the Customer's bank account corresponding to the means of payment of the original order, within fourteen days of receipt by Galeries Lafayette of the returned Products. If only part of the order is returned, only the price invoiced for the returned Products will be refunded.<br />
                  In the event of a partial return of an order that has benefited from free delivery linked to its amount, the Customer may be charged for the delivery costs corresponding to the final actual order, if the latter amount falls below the free delivery threshold. To be accepted for return, Products must be in their original packaging, whole (accessories, instructions, label, etc.), in perfect condition, unused, unwashed, undamaged, unworn, unsealed for Gourmet and Beauty Products , and accompanied by the return slip.
                  </p> 
                 
                <p>c) Conditions and modalities of return :</p>
                
                <p>A Product can be tried on but not worn. Any Product that is incomplete, counterfeit, worn, spoiled, used, damaged, deteriorated, soiled or even partially consumed by the Customer cannot be the subject of an exchange or a refund.
                  For hygienic reasons, Galeries Lafayette does not take back or reimburse products for small household appliances and personal care (trimmer, epilator, straightener, etc.), lingerie worn (underwear, swimsuits, self-adhesive lingerie).<br />
                  Products whose packaging has been opened are not returned, exchanged or reimbursed.<br />
                  To be taken back, the soles of the shoes must be smooth, new and must not have any scratches that could indicate that they have been worn. The boxes containing the shoes must not be damaged and must not under any circumstances be used as a return box. Shoe boxes must not contain adhesive tape or any inscriptions.<br />
                  Beauty packaging (cream, makeup, etc.) must not be unsealed.<br />
                  Watch and jewelry boxes must also be returned in perfect condition and the dial must have the original protective sticker.
                  </p>
                
                <p class="is-bold">8 - Guarantees</p>
                
                <p class="is-bold">Legal Guarantee :</p> 
                
                <p>All Products sold in the store benefit from the legal guarantee of conformity</p> 
                
                
                <p>Galeries Lafayette is liable for any lack of conformity of the Products sold in the store in accordance with the conditions of Article L. 217-4 et seq. of the French Consumer Code, and for hidden defects in the item sold in accordance with the conditions provided for in Articles 1641 et seq. of the French Civil Code.</p>
                
                <p>When the Customer claims the legal guarantee of conformity :</p>
                <ul>
                  <li><p>He has a period of 24 months from the delivery of the product to act.</p></li> 
                  <li><p>He can choose between repairing or replacing the item, subject to certain cost conditions provided for in Article L 217-9 of the French Consumer Code.</p></li> 
                  <li><p>It is not necessary to provide proof of the existence of the lack of conformity during the 24 months following delivery of the goods.</p></li> 
                </ul>
                <p>The legal guarantee of conformity applies regardless of any commercial guarantee granted.<br />
                  The Customer may appeal to the Article 1641 of the French Civil Code, in this case the Consumer may choose between canceling the sale or reducing its price.
                  </p> 
                
                
                
                
                <p class="is-bold">Commercial guarantees :</p> 
                
                <p>In addition to the legal guarantee, commercial guarantees to which Galeries Lafayette is not a party may be offered for certain Products directly by the brands. The scope and duration of these commercial guarantees varies according to the brands concerned. You can consult the conditions of these commercial guarantees on the manufacturer's website and / or in the Product manual.<br />
                  To benefit from these commercial guarantees, the Customer must contact the brands which have granted them.<br />
                  Galeries Lafayette cannot be held responsible if brands refuse to apply their commercial guarantees.
                  </p> 
                
                <p class="is-bold">9 - Spare parts</p> 
                
                <p>In the absence of explicit mention on the Product, Galeries Lafayette has not been informed of the availability of any spare parts.</p> 
                
                <p class="is-bold">10 - Take-back of electrical products - 1 for 1</p> 
                
                <p>Pursuant to the provisions of article R 543-180 of the French Environmental Code, if a Customer purchases an electrical or electronic household product, Galeries Lafayette will take back the used electrical or electronic equipment that the Customer disposes of free of charge, within the limit of quantity and type of equipment sold.<br />
                  Galeries Lafayette may refuse to take back electrical and electronic equipment which, following contamination, presents a risk to the safety and health of its staff.<br />
                  In this case, an alternative solution may be offered by Galeries Lafayette.
                  </p>
                
                <p class="is-bold">11 - Fortuitous event and Force Majeure</p>
                 
                <p>Neither the Customer nor Galeries Lafayette can be held responsible for the total or partial non-performance of their obligations, if this non-performance is due to a fortuitous event or force majeure which would hinder or delay the performance. Are considered as such, in particular, without this list being exhaustive: war, riots, insurrections, social disturbances, strikes of all kinds, floods, fires, storms, lack of raw materials, lockouts, hospitalizations (for the Customer in particular), pandemics and epidemics, imperative government or administrative decisions ordering the closure of businesses and deliveries; these various elements being assessed in relation to the applicable law.<br />
                  Galeries Lafayette will inform the Customer of such a fortuitous event or force majeure within 7 (seven days) of its occurrence.
                  </p> 
                 
                <p>In the event that imperative reasons (in particular context of uncertainty related to the Coronavirus epidemic and its consequences) and / or a fortuitous event arise or in the case of a government or administrative decision and / or an imperative company decision (in particular the impossibility of opening a store), preventing Galeries Lafayette or the Customer from fulfilling their obligations, the other Party must be informed without delay.</p> 
                
                <p>Initially, the order will be suspended, and if the duration of the suspension continues beyond 5 days, the order may be cancelled by Galeries Lafayette or the Customer. The Customer will then be reimbursed for his order and will not be able to claim any other compensation.</p>
                 
                <p class="is-bold">12 - Complaints and mediation</p> 
                
                <p>Should there be any complaint, Customer Relations will ensure processing and follow-up. Please address any such complaints to the following address :</p> 
                
                <p>Galeries Lafayette Haussmann - Service Relation Clientèle<br />
                40 Bld Haussmann<br />
                TSA 44100<br />
                75446 PARIS Cedex 9<br />
                Tél : 01.42.82.38.08<br />
                Courriel : <a href="mailto:shoppingadistance@galerieslafayette.com">shoppingadistance@galerieslafayette.com</a></p>
                
                <p>In accordance with the provisions of the French Consumer Code concerning the amicable settlement of disputes, Galeries Lafayette adheres to the e-commerce mediator service of FEVAD (Fédération du e-commerce et de la vente à distance). Their contact details are as follows: Mediation FEVAD, 60 rue la Boëtie, 75008 Paris.<br />
                  After having gone through a written request to the Galeries Lafayette Customer Service which has not been satisfied, the Customer may also refer to mediator services for any consumer dispute. To find out how to submit a request to the Mediator, the Customer can go to : <a href="http://www.mediateurfevad.fr">http://www.mediateurfevad.fr</a></p>
                
                
                <p class="is-bold">13 - Personal data and telephone solicitation</p> 
                
                <p>Personal data is collected to allow access to the service and may be used by Galeries Lafayette for statistical purposes to enable a better understanding of customers and improve the quality of service.</p> 
                
                <p>In accordance with the French Data Protection and Freedom of Information Law (Loi Informatique et Liberté), everyone has the right to access, rectify and oppose personal data concerning them, by writing to 44 GALERIES LAFAYETTE - 44 GL - Correspondent CNIL - TSA 23 000 44-48 rue Châteaudun - 75 009 PARIS or by email at <a href="mailto:relaiscil@galerieslafayette.com">relaiscil@galerieslafayette.com</a></p>
                
                <p>For more information, please visit <a href="https://haussmann.galerieslafayette.com/en/privacy-policy-for-export-sales-service/">https://haussmann.galerieslafayette.com/en/privacy-policy-for-export-sales-service/</a></p>
                
                <p class="is-bold">14 - Responsibility</p>
                
                <p>Aside from cases expressly laid down by the current texts in force, Galeries Lafayette's liability is limited to direct and foreseeable damage that may result from the use by the Customer of the sold and shipped Products.</p> 
                
                <p>Galeries Lafayette cannot be held liable for damage resulting from faulty use of the Products by the Customer.</p>
                
                <p>Neither the responsibility of Galeries Lafayette nor that of the brands and / or partners can be engaged if the non-performance or poor performance of their obligations are attributable to the Customer, to the unforeseeable and insurmountable event of a third party failing to perform and uphold these general conditions of sale, a fortuitous event or an unforeseeable, unstoppable and external force majeure event.</p>
                
                <p>Galeries Lafayette is not responsible for any kind of damages that may be suffered by the Customer during the use of the websites and / or applications or digital tools. The use of the page of the remote personal shopping service and / or digital tools implies the acknowledgement and acceptance by the Customer of the characteristics as well as the limits of the Internet and related technologies, the absence protection of certain data against possible misappropriation or pirating and risks of contamination by possible viruses circulating on the network. Galeries Lafayette declines all responsibility in the event of misuse or incidents related to the use of the computer, Internet access, maintenance or malfunction of servers, telephone lines or any other technical connection, and in the event of forms being sent to incorrect or incomplete address, of any computer errors or faults on the page of the remote personal shopping service.</p>
                
                
                <p class="is-bold">15 - Intellectual property</p>
                
                <p>All the elements published on the Galeries Lafayette website, such as sounds, images, photographs, videos, writings, animations, programs, models, graphic charters, databases, software, and other underlying technology are protected by the provisions of the French Intellectual Property Code and belong to Galeries Lafayette or, where applicable, have been duly granted to the Galeries Lafayette. The “Galeries Lafayette” brand, as well as all figurative and non-figurative brands and more generally all other brands, illustrations, images and logotypes appearing on the articles, accessories or packaging, whether filed or not, are and will remain the exclusive property of Galeries Lafayette, with the exception of the rights held by suppliers and partners of Galeries Lafayette on the visuals of their Products, on their brands and logos presented.</p>
                
                <p>The Customer shall not capture and / or record and / or broadcast any screenshots of conversations with the sales staff of Galeries Lafayette on any means of electronic or telephone communication.</p>
                
                <p>Any total or partial reproduction, modification or use of these brands, photos, illustrations, images and logotypes, for any reason and on any medium whatsoever, without the explicit prior consent of Galeries Lafayette, is strictly prohibited. The same applies to any combination or conjunction with any other brand, symbol, logotypes and more generally any distinctive sign intended to form a composite logo, with the exception of logos and descriptive signs belonging to the brands present on the site. The same will apply to all copyrights, designs and patents which are the property of Galeries Lafayette.</p>
                
                <p>A user wishing to place on his personal website a simple link directing to the page of the distance selling service of the Galeries Lafayette Haussmann store, must first request express authorization from Galeries Lafayette.</p>
                
                <p class="is-bold">16 - Partial invalidity</p> 
                
                <p>If one or more stipulations of these general terms and conditions of sale were deemed unlawful or invalid, this nullity would not have the effect of rendering the other provisions of these terms and conditions null and void.</p>
                
                <p class="is-bold">17 - Applicable Law and Jurisdiction</p>
                
                <p>These general terms and conditions of sale are subject to the application of French law.</p>
                
                <p>Only the French version of these general conditions of sale is deemed valid.</p>
                
                <p>In the event of a complaint arising from an order of Products or its delivery, the Customer will have the possibility, prior to any legal action, to seek an amicable solution with the help of the mediation service of FEVAD, or a consumer association or any other chosen council.</p>
                
                <p>Any dispute that may result from the interpretation or execution of these general terms and conditions of sale and its consequences will be brought before qualified courts.</p>
                
              </div>
            </article>
          </div>
        </div>
      </div>
    </section>

</div>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-service.min.v01.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->
  
<!-- build:js /media/LP/src/js/2020/shoppingadistance-service.min.v01.js 
  <script src="../../../assets/js/accordion.js"></script>
  <script src="../../src/js/2020/shoppingadistance-service.js"></script>
endbuild -->
  
<?php include ('../../pages-defaults/footer.php'); ?>
