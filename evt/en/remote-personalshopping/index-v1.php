<?php include ('../../pages-defaults/header.php'); ?>
<script>
	document.title = "EN SAD";
</script>
<div class="row header__product-list">
	<div class="header__product-list--first-line">
		<div class="columns large-12 medium-24"></div>
		<div class="columns large-12 show-for-large-up"></div>
	</div>
</div>
<!-- https://static.galerieslafayette.com/ -->
	
<!-- <link href="../../../media/LP/src/css/2020/shoppingadistance-hub.css" rel="stylesheet" type="text/css"> -->
	
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance-hub.min.v07.css" rel="stylesheet" type="text/css">
<style>
	html {
		scroll-behavior: smooth;
	}
	.main-nav {
		padding-top: 0 !important;
	}
	.ab_widget_container_promotional-banner {
		display: none; 
	}
	@media screen and (max-width: 767px) {
		.luxe-augmente h1 {
			font-size: 56px;
		}
	}
	@media screen and (min-width: 1024px) {
		.menu-wrapper {
			height: 0;
			border-bottom: none;
			overflow: hidden;
		}
		.header--shadow {
			height: 112px !important; 
		}
	}
</style>
<div class="luxe-augmente" data-bg-color="">
	<section class="luxe-augmente-hero" style="background-color: #000000;">
		<div class="container">
			<div class="language">EN &nbsp;|&nbsp; <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance">FR</a></div>
			<article class="luxe-row no-gutter luxe-middle-tablet">
				<div class="luxe-col-mobile-12 luxe-col-tablet-6">
					
					<div class="hero-image is-hidden-table">
						<figure class="image">
							<iframe class="hero-bg-image hero-video is-hidden-tablet" src="https://player.vimeo.com/video/477237166?autoplay=1&amp;muted=1&amp;autopause=0&amp;background=1" width="767" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
							<iframe class="hero-bg-image hero-video is-hidden-mobile" src="https://player.vimeo.com/video/477262284?autoplay=1&amp;muted=1&amp;autopause=0&amp;background=1" width="574" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
							<!-- <img class="is-hidden-tablet" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/hero-garde-robe-carre_M.jpg"
								srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/hero-garde-robe-carre_M@2x.jpg 2x" 
								alt="Le prêt-à-porter en shopping à distance - Galeries Lafayette" width="767" height="767"> -->
							<!-- <img class="is-hidden-mobile" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/hero-garde-robe_D.jpg"
							srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/hero-garde-robe_D@2x.jpg 2x" 
							alt="Le prêt-à-porter en shopping à distance - Galeries Lafayette" width="978" height="910"> -->
						</figure>
					</div>
				</div>
				<div class="luxe-col-mobile-12 luxe-col-tablet-6">
					<div class="article-body text-left">
						<h1 class="is-uppercase has-text-white">Remote Personal Shopping</h1>
						<br />
						<p class="has-text-white"><strong class="is-bold">What if you could do your Christmas shopping at Galeries Lafayette from the comfort of your home?</strong><br /><br />                        
						With the Remote Personal Shopping service, the doors of our iconic department store remain virtually open to help you prepare for the holiday season! Access the finest Luxury & Designer brands of Galeries Lafayette Paris Haussmann through your Wishlist.
						</p>
						<div class="buttons">
							<a href="#wishlist" class="has-smoothscroll button primary outlined has-arrow-down">Fill out your wishlist</a>
						</div>
					</div>
				</div>
			</article>
		</div>
	</section>
	
	<section class="luxe-augmente-body" style="background-color: #F1EFEC;">
		<div class="container">
			<!-- Fil d'Ariane -->
			<nav class="breadcrumb" aria-label="Fil d'Ariane">
				<ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
					<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="https://www.galerieslafayette.com">
							<span itemprop="name">Home</span>
						</a>
						<meta itemprop="position" content="1">
					</li>
					<li  class="is-active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance" aria-current="page">
							<span itemprop="name">Remote Personal Shopping</span>
						</a>
						<meta itemprop="position" content="2">
					</li>
				</ul>
			</nav>
				
			<!-- SERVICES -->
			<article class="services luxe-row">
				<div class="luxe-col-mobile-12">
					<h2>A personalised and elevated digital shopping experience</h2>
				</div>
				<div class="luxe-col-mobile-12 luxe-col-tablet-4">
					<div class="service luxe-row">
						<div class="luxe-col-mobile-3 luxe-col-tablet-12">
							<div class="service-icon">
								<img class="icon" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/icon-luxe.svg" 
								alt="icone luxe - Galeries Lafayette">
							</div>
						</div>
						<div class="luxe-col-mobile-9 luxe-col-tablet-12">
							<p class="service-txt"><strong>Luxury</strong>
							Access the best Luxury & Designer brands from the comfort of your home</p>
						</div>
					</div>
				</div>
				<div class="luxe-col-mobile-12 luxe-col-tablet-4">  
					<div class="service luxe-row">
						<div class="luxe-col-mobile-3 luxe-col-tablet-12">
							<div class="service-icon">
								<img class="icon" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/icon-simplicite.svg" 
								alt="icone luxe - Galeries Lafayette">
							</div>
						</div>
						<div class="luxe-col-mobile-9 luxe-col-tablet-12">
							<p class="service-txt"><strong>Simplicity</strong>
							Confirm your order via email and proceed to a secure payment</p>
						</div>
					</div>
				</div>
				<div class="luxe-col-mobile-12 luxe-col-tablet-4">  
					<div class="service luxe-row">
						<div class="luxe-col-mobile-3 luxe-col-tablet-12">
							<div class="service-icon">
								<img class="icon" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/icon-confort.svg" 
								alt="icone luxe - Galeries Lafayette">
							</div>
						</div>
						<div class="luxe-col-mobile-9 luxe-col-tablet-12">
							<p class="service-txt"><strong>Worldwide delivery</strong>
							A convenient way to make tax-free purchases (for customers residing outside of the European Union)</p>
						</div>
					</div>
				</div>
			</article>
		</div>
	</section>
			
	<!-- WISHLIST -->
	<section class="luxe-augmente-body" id="wishlist">
		<div class="container">
			
			<article class="luxe-row no-gutter luxe-middle-mobile text-left">
				<div class="luxe-col-mobile-12 luxe-col-tablet-6">
					<div class="article-body">
						<div class="tag is-uppercase">Wishlist</div>
						<h2 class="is-uppercase">Already have your mind<br /> set on something? Use<br class="is-hidden-mobile" /> our wishlist</h2>
						<p>Share with us your dream purchase to get delivered straight at home.</p>
						<a href="" class="button primary trigger_liveshopping" onclick="clickedBrand=''; return false;" data-reveal-id="popin-live-shopping-form" target="_self">Fill out your wishlist</a>
					</div>
				</div>
				<div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
					<div class="article-image">
						<figure class="image">
							<img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/wishlist-main.jpg"
								srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/wishlist-main@2x.jpg 2x" 
								alt="Already have your mind set on something? Use our wishlist - Galeries Lafayette" width="574" height="624">
						</figure>
					</div>
				</div>
			</article>
			
		</div>
	</section>
	
	<!-- FAQ -->
	<section class="luxe-augmente-body" style="background-color: #F1EFEC;">
		<div class="container">
			<div class="luxe-row luxe-middle-mobile text-left faq">
				<div class="luxe-col-mobile-12 luxe-col-tablet-5">
					<h2>With the Remote Personal Shopping service, access the finest Luxury & Designer brands, wherever you are.</h2>
					<p>See all <a href="https://www.galerieslafayette.com/evt/en/remote-personalshopping-service" class="is-text">Q&As</a></p>
				</div>
				<div class="luxe-col-mobile-12 luxe-col-tablet-7 luxe-col-desktop-6">
					<ul>
						<li><p>Worldwide delivery</p></li>
							
						<li><p>Tax-free purchase outside of Europe</p></li>
							
						<li><p>No minimum purchase amount, this service is completely free</p></li>
							
						<li><p>Payment method of your choice*, via a secure link</p></li>
					</ul>
					<p><em>*Among the available payment methods outlined in our Terms and Conditions of Sale</em></p>
				</div>
			</div>
		</div>
	</section>
	
	<!-- CONDTIONS -->
	<section class="luxe-augmente-body">
		<div class="container">
			<div class="luxe-row conditions">
				<div class="luxe-col-mobile-12">
					<a href="https://www.galerieslafayette.com/evt/en/remote-personalshopping-service#conditions" class="is-text">Terms and conditions</a>
				</div>
			</div>
		</div>
	</section>
</div>

<script>
	window.onload = function() {
		var magLink = document.querySelector('.header-mag-link');
		var magTxt = document.querySelector('.header-mag-link .picto__map__label');
		
		magLink.setAttribute('href', 'https://haussmann.galerieslafayette.com/en');
		magTxt.innerText = "Galeries Lafayette Paris Haussmann";
	};
</script>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-hub-en.min.js"></script>
	
<div id="goinstore_modals"></div>
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance-goinstore.min.v02.css" rel="stylesheet" type="text/css">
<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-goinstore-en.min.v01.js"></script>
<!--=========================== FIN LANDING PAGE ========================-->
		
<!-- build:js /media/LP/src/js/2020/shoppingadistance-goinstore-en.min.js 
	<script src="../../src/js/2020/shoppingadistance-goinstore-en.js"></script>
<!-- endbuild -->
	
<!-- build:js /media/LP/src/js/2020/shoppingadistance-hub-en.min.js 
	<script src="../../src/js/2020/shoppingadistance-hub-en.js"></script>
<!-- endbuild --> 
	
<?php include ('../../pages-defaults/footer.php'); ?>
