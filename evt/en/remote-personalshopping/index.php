<?php include ('../../pages-defaults/header.php'); ?>
<script>
  document.title = "EN SAD";
</script>
<div class="row header__product-list">
  <div class="header__product-list--first-line">
    <div class="columns large-12 medium-24"></div>
    <div class="columns large-12 show-for-large-up"></div>
  </div>
</div>
<!-- https://static.galerieslafayette.com/ -->
  
<!-- <link href="../../../media/LP/src/css/2020/shoppingadistance-hub.css" rel="stylesheet" type="text/css"> -->
  
<!-- =========================== LANDING PAGE ========================== -->  
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance-hub.min.v10.css" rel="stylesheet" type="text/css">
<style>
  html {
   scroll-behavior: smooth;
  }
  .main-nav {
   padding-top: 0 !important;
  }
  .ab_widget_container_promotional-banner {
   display: none !important; 
  }
  .luxe-augmente {
   margin: 0 auto !important;
  }
  .header__secondary,
  .footer {
    display: none !important;
  }
  .header:not(.header--prehome) .header__wrapper[data-v-6ea22a7f] {
    padding-bottom: 0 !important;
  }
  @media screen and (max-width: 374px) {
    .luxe-augmente .luxe-augmente-hero .button, .luxe-augmente #personalshopper .button{
      font-size: 15px;
      padding-left: 30px;
      padding-right: 30px;
    }
  }
  @media screen and (max-width: 767px) {
    .luxe-augmente h1 {
      font-size: 56px;
    }
  }
  @media only screen and (min-width: 1025px) {
    .header:not(.header--prehome) .header__content[data-v-6ea22a7f],
    .header[data-v-6ea22a7f]:not(.header--prehome) {
      height: 7.0625rem !important;
    }
  }
</style>
<div class="luxe-augmente" data-bg-color="">
  <section class="luxe-augmente-hero" style="background-color: #000000;">
    <div class="container">
      <div class="language">EN &nbsp;|&nbsp; <a href="https://www.galerieslafayette.com/evt/fr/shoppingadistance">FR</a></div>
      <article class="luxe-row no-gutter luxe-middle-tablet">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          
          <div class="hero-image is-hidden-table">
            <figure class="image">
              <iframe class="hero-bg-image hero-video is-hidden-tablet" src="https://player.vimeo.com/video/477237166?autoplay=1&amp;muted=1&amp;autopause=0&amp;background=1" width="767" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
              <iframe class="hero-bg-image hero-video is-hidden-mobile" src="https://player.vimeo.com/video/477262284?autoplay=1&amp;muted=1&amp;autopause=0&amp;background=1" width="574" frameborder="0" allow="autoplay" allowfullscreen mozallowfullscreen="" webkitallowfullscreen=""></iframe>
            </figure>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body text-left">
            <h1 class="is-uppercase has-text-white">Remote Personal Shopping</h1>
            <br />
            <p class="has-text-white"><strong class="is-bold">What if you could do your shopping at Galeries Lafayette Paris Haussmann from the comfort of your home?</strong><br /><br />                        
            With the Remote Personal Shopping service, the doors of our iconic department store remain virtually open for you. Access the finest Luxury & Designer brands of Galeries Lafayette Paris Haussmann via Live Video with sale advisors from your favourite brands or Galeries Lafayette personal shoppers, as well as through your Wishlist.</p>
            <div class="buttons">
              <a href="#brands" class="has-smoothscroll button primary outlined has-arrow-down">Connect with a brand stylist</a>
              <a href="#personalshopper" class="has-smoothscroll button primary outlined has-arrow-down">Connect with a Personal Shopper</a>
              <a href="#wishlist" class="has-smoothscroll button primary outlined has-arrow-down">Fill out your wishlist</a>
            </div>
          </div>
        </div>
      </article>
    </div>
  </section>
  
  <section class="luxe-augmente-body" style="background-color: #F1EFEC;">
    <div class="container">
      <!-- Fil d'Ariane -->
      <nav class="breadcrumb" aria-label="Fil d'Ariane">
        <ul itemscope="" itemtype="http://schema.org/BreadcrumbList">
          <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com">
              <span itemprop="name">Home</span>
            </a>
            <meta itemprop="position" content="1">
          </li>
          <li  class="is-active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="https://www.galerieslafayette.com/evt/fr/shoppingadistance" aria-current="page">
              <span itemprop="name">Remote Personal Shopping</span>
            </a>
            <meta itemprop="position" content="2">
          </li>
        </ul>
      </nav>
        
      <!-- SERVICES -->
      <article class="services luxe-row">
        <div class="luxe-col-mobile-12">
          <h2>A personalised and elevated digital shopping experience</h2>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-3">
          <div class="service luxe-row">
            <div class="luxe-col-mobile-3 luxe-col-tablet-12">
              <div class="service-icon">
                <img class="icon" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/icon-luxe.svg" 
                alt="icone luxe - Galeries Lafayette">
              </div>
            </div>
            <div class="luxe-col-mobile-9 luxe-col-tablet-12">
              <p class="service-txt"><strong>Luxury</strong>
              Access the best Luxury & Designer brands from the comfort of your home</p>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-3">
          <div class="service luxe-row">
            <div class="luxe-col-mobile-3 luxe-col-tablet-12">
              <div class="service-icon">
                <img class="icon" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/icon-conseil.svg" 
                alt="icone luxe - Galeries Lafayette">
              </div>
            </div>
            <div class="luxe-col-mobile-9 luxe-col-tablet-12">
              <p class="service-txt"><strong>Inspiration</strong>
              Be inspired by our fashion experts during a very private live video appointment</p>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-3">  
          <div class="service luxe-row">
            <div class="luxe-col-mobile-3 luxe-col-tablet-12">
              <div class="service-icon">
                <img class="icon" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/icon-simplicite.svg" 
                alt="icone luxe - Galeries Lafayette">
              </div>
            </div>
            <div class="luxe-col-mobile-9 luxe-col-tablet-12">
              <p class="service-txt"><strong>Simplicity</strong>
              Confirm your order via email and proceed to a secure payment</p>
            </div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-3">  
          <div class="service luxe-row">
            <div class="luxe-col-mobile-3 luxe-col-tablet-12">
              <div class="service-icon">
                <img class="icon" src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/icon-confort.svg" 
                alt="icone luxe - Galeries Lafayette">
              </div>
            </div>
            <div class="luxe-col-mobile-9 luxe-col-tablet-12">
              <p class="service-txt"><strong>Worldwide delivery</strong>
              A convenient way to make tax-free purchases (for customers residing outside of the European Union)</p>
            </div>
          </div>
        </div>
      </article>
    </div>
  </section>
  
  <!-- BRANDS -->
  <section class="luxe-augmente-body marques" id="brands">
    <div class="container">
      <article class="luxe-row no-gutter luxe-middle-mobile text-left">
        <div class="luxe-col-mobile-12 luxe-col-tablet-5">
          <div class="article-body">
            <div class="tag is-uppercase">Live Shopping</div>
            <h2 class="is-uppercase">Appointment<br class="is-hidden-mobile" /> with a brand Stylist</h2>
            <p>No more need to go to the store : the sales advisors of your favorite brands are at your disposal by appointment. Brand representatives help you browse the collections in Live Video Shopping and enjoy an in-store experience from the comfort of your own home.</p>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-7">
          
          <div class="live-brands-list top16 show-brands" id="brands-top16"></div>
          
        </div>
        
      </article>
    </div>
  </section>
  
  <!-- PERSONAL SHOPPER -->
  <section class="luxe-augmente-body" id="personalshopper">
    <div class="container">
      <!-- colonnes inversées avec la class luxe-reverse -->
      <article class="luxe-row no-gutter luxe-middle-mobile luxe-reverse text-left">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <div class="tag is-uppercase">Personal shopper</div>
            <h2 class="is-uppercase">Personal shopping via live&nbsp;video</h2>
            <p>Looking for a gift or to complete your dressing room?<br />
              Get inspiration with the expert assistance of our Personal Shoppers, on hand to help you find just the right style or item you are looking for. Let our personal shoppers help you browse and select items through live video shopping with a personalised selection of brands, pieces and styles. Free fashion advice available on appointment.</p>
            <a href="" class="button primary trigger_liveshopping" onclick="clickedBrand=''; appointedWidgetId='6006ad4d111720330ccf23b0'; GL_GIS.LiveShopping.createAppointedWidget('ps1,ps2,ps3'); return false;" data-reveal-id="popin-live-shopping-rdv">Connect with a Personal Shopper</span></a>
            <div id="jouets"></div>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/personal-shopper-main.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/personal-shopper-main@2x.jpg 2x" 
                alt="L’expertise de nos personal shoppers en vidéo - Galeries Lafayette" width="574" height="652">
            </figure>
          </div>
        </div>
      </article>
    </div>
  </section>
  
  <!-- WISHLIST -->
  <section class="luxe-augmente-body" id="wishlist">
    <div class="container">
      
      <article class="luxe-row no-gutter luxe-middle-mobile text-left">
        <div class="luxe-col-mobile-12 luxe-col-tablet-6">
          <div class="article-body">
            <div class="tag is-uppercase">Wishlist</div>
            <h2 class="is-uppercase">Already have your mind<br /> set on something? Use<br class="is-hidden-mobile" /> our wishlist</h2>
            <p>Share with us your dream purchase to get delivered straight at home.</p>
            <a href="" class="button primary trigger_liveshopping" onclick="clickedBrand=''; return false;" data-reveal-id="popin-live-shopping-form" target="_self">Fill out your wishlist</a>
          </div>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-6 luxe-first-mobile luxe-last-tablet">
          <div class="article-image">
            <figure class="image">
              <img src="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/wishlist-main.jpg"
                srcset="https://static.galerieslafayette.com/media/LP/src/img/2020/landing/shoppingadistance/hub/wishlist-main@2x.jpg 2x" 
                alt="Already have your mind set on something? Use our wishlist - Galeries Lafayette" width="574" height="624">
            </figure>
          </div>
        </div>
      </article>
      
    </div>
  </section>
  
  <!-- FAQ -->
  <section class="luxe-augmente-body" style="background-color: #F1EFEC;">
    <div class="container">
      <div class="luxe-row luxe-middle-mobile text-left faq">
        <div class="luxe-col-mobile-12 luxe-col-tablet-5">
          <h2>With the Remote Personal Shopping service, access the finest Luxury & Designer brands, wherever you are.</h2>
          <p>See all <a href="https://www.galerieslafayette.com/evt/en/remote-personalshopping-service" class="is-text">Q&As</a></p>
        </div>
        <div class="luxe-col-mobile-12 luxe-col-tablet-7 luxe-col-desktop-6">
          <ul>
            <li><p>Exclusive  assistance of our Personal Shoppers on hand to help you find just the right style or item you are looking for.</p></li>
            
            <li><p>Worldwide delivery</p></li>
              
            <li><p>Tax-free purchase outside of Europe</p></li>
              
            <li><p>No minimum purchase amount, this service is completely free</p></li>
              
            <li><p>Payment method of your choice*, via a secure link</p></li>
          </ul>
          <p><small><em>*Among the available payment methods outlined in our Terms and Conditions of Sale</em></small></p>
        </div>
      </div>
    </div>
  </section>
  
  <!-- CONDTIONS -->
  <section class="luxe-augmente-body">
    <div class="container">
      <div class="luxe-row conditions">
        <div class="luxe-col-mobile-12">
          <a href="https://www.galerieslafayette.com/evt/en/remote-personalshopping-service#conditions" class="is-text">Terms and conditions</a>
        </div>
      </div>
    </div>
  </section>
</div>

<script>
  window.onload = function() {
    var magLink = document.querySelector('.primary-header__store-locator');
    var magLinkContent = magLink.lastElementChild.outerHTML;
    
    magLink.setAttribute('href', 'https://haussmann.galerieslafayette.com/en');
    magLink.innerHTML = magLinkContent + " Galeries Lafayette Paris Haussmann ";
  };
</script>

<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-hub-en.min.v01.js"></script>
  
<div id="goinstore_modals"></div>
<link href="https://static.galerieslafayette.com/media/LP/src/css/2020/shoppingadistance-goinstore.min.v02.css" rel="stylesheet" type="text/css">
<script src="https://static.galerieslafayette.com/media/LP/src/js/2020/shoppingadistance-goinstore-en.min.v03.js"></script>
  
<script>
  var clickedBrand = '';
  var appointedWidgetId = '';
  var marques = [
    {"marque":"Alexander McQueen","secteur":"","top":true,"visio":false,"key":"brand28","apwidget":"602bad24da6f8c5da61813c7"},
    {"marque":"Chloé","secteur":"","top":true,"visio":false,"key":"brand8","apwidget":"602bad09da6f8c5da61813c0"},
    {"marque":"Fauré Lepage","secteur":"","top":true,"visio":false,"key":"brand4","apwidget":"602bacedda6f8c5da61813b9"},
    {"marque":"Fendi","secteur":"","top":true,"visio":false,"key":"brand30","apwidget":"602bace9da6f8c5da61813b8"},
    {"marque":"Loewe","secteur":"","top":true,"visio":false,"key":"brand29","apwidget":"602bacc2da6f8c5da61813ae"},
    {"marque":"Max Mara","secteur":"","top":true,"visio":false,"key":"brand1","apwidget":"602bacb8da6f8c5da61813ac"},
    {"marque":"Prada","secteur":"","top":true,"visio":false,"key":"brand15","apwidget":"602baca5da6f8c5da61813a7"},
    {"marque":"Valentino","secteur":"","top":true,"visio":false,"key":"brand38","apwidget":"602bac8cda6f8c5da61813a1"}
  ];

  function getList(el, top = false) {
    const bloc = document.createElement('div');
    bloc.classList.add('brands-container', 'is-disabled');
    let wrapper_letter = null;
    let wrapper = top ? bloc : wrapper_letter;
    const lst = top ? marques.filter(m => m.top) : marques;
    let letter = null;
    lst.forEach(m => {
        const l = m.marque.substring(0, 1);
        if (!top) {
            if (letter === null || letter !== l) {
                letter = l;
                wrapper_letter = document.createElement('div');
                wrapper_letter.classList.add('wrapper');
                bloc.appendChild(wrapper_letter);
                const line = document.createElement('div');
                line.innerText = l;
                line.classList.add('letter');
                wrapper_letter.appendChild(line);
                wrapper = wrapper_letter;
            }
        }
        const line = document.createElement('a');
        
        if (m.marque === "") {
          line.setAttribute('href', "https://www.galerieslafayette.com/evt/fr/shoppingadistance/");
          
        } else {
          line.setAttribute('href', "");
          if (m.visio) {
            line.setAttribute("onclick", "clickedBrand='" + m.marque.replace(/'/g, "\\'") + "'; appointedWidgetId='" + m.apwidget + "'; GL_GIS.LiveShopping.checkClerkAvailability('" + m.key + "'); return false;");
            line.classList.add('has-visio');
          } else {
              line.setAttribute("onclick", "clickedBrand='" + m.marque.replace(/'/g, "\\'") + "'; appointedWidgetId='" + m.apwidget + "'; GL_GIS.LiveShopping.createAppointedWidget('" + m.key + "'); return false;");
              line.dataset.revealId = "popin-live-shopping-rdv";
          }
        }
        
        line.innerText = m.marque;
        line.classList.add('marque', 'trigger_liveshopping');

        if (m.secteur) {
            const univers = document.createElement('span');
            univers.innerText = m.secteur;
            line.appendChild(univers);
        }
        wrapper.appendChild(line);
    });
    document.getElementById(el).appendChild(bloc);
  }

  getList('brands-top16', true);
</script>
<!--=========================== FIN LANDING PAGE ========================-->

  
<!-- build:js /media/LP/src/js/2020/shoppingadistance-goinstore-en.min.v03.js 
  <script src="../../src/js/2020/shoppingadistance-goinstore-en-v2.js"></script>
<!-- endbuild -->
  
<!-- build:js /media/LP/src/js/2020/shoppingadistance-hub-en.min.js 
  <script src="../../src/js/2020/shoppingadistance-hub-en.js"></script>
<!-- endbuild --> 
  
<?php include ('../../pages-defaults/footer.php'); ?>
