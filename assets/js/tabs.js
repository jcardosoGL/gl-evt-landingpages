var getClosest = function (elem, selector) {
	// Element.matches() polyfill
	if (!Element.prototype.matches) {
	    Element.prototype.matches =
	        Element.prototype.matchesSelector ||
	        Element.prototype.mozMatchesSelector ||
	        Element.prototype.msMatchesSelector ||
	        Element.prototype.oMatchesSelector ||
	        Element.prototype.webkitMatchesSelector ||
	        function(s) {
	            var matches = (this.document || this.ownerDocument).querySelectorAll(s),
	                i = matches.length;
	            while (--i >= 0 && matches.item(i) !== this) {}
	            return i > -1;
	        };
	}
	// Get the closest matching element
	for ( ; elem && elem !== document; elem = elem.parentNode ) {
		if ( elem.matches( selector ) ) return elem;
	}
	return null;
};

var allTabs = document.querySelectorAll(".tabs > ul > li > a");
function myTabClicks(tabClickEvent) {
	// Get the parent with the `.tabs` class
	var myTabsParent = getClosest(event.target, '.tabs-container');
	
	// ajoute/supprime .is-active sur l'onglet
	var myTabs = myTabsParent.querySelectorAll(".tabs > ul > li > a");
	for (var i = 0; i < myTabs.length; i++) {
		myTabs[i].classList.remove("is-active");
		myTabs[i].setAttribute("aria-selected", "false");
		myTabs[i].setAttribute("tabindex", "-1");
		
	}
	var clickedTab = tabClickEvent.currentTarget;
	clickedTab.classList.add("is-active");
	clickedTab.setAttribute("aria-selected", "true");
	clickedTab.setAttribute("tabindex", "0");
	
	tabClickEvent.preventDefault();
	
	// ajoute/supprime .is-active sur le panneau à afficher
	var myContentPanes = myTabsParent.querySelectorAll(".tab-pane");
	for (i = 0; i < myContentPanes.length; i++) {
		myContentPanes[i].classList.remove("is-active");
		myContentPanes[i].setAttribute("hidden", "");
	}
	
	var anchorReference = tabClickEvent.target;
	var activePaneId = anchorReference.getAttribute("href");
	var activePane = document.querySelector(activePaneId);
	activePane.classList.add("is-active");
	activePane.removeAttribute("hidden");
	
	//permet de charger les images lazyloadées avec bLazy dans les onglets
	if (typeof bLazy != 'undefined') { 
		bLazy.revalidate();
	}
	
}

for (i = 0; i < allTabs.length; i++) {
	allTabs[i].addEventListener("click", myTabClicks)
}