//Permet de gérer les accordéons
document.addEventListener("DOMContentLoaded", function() {

    [].forEach.call(document.querySelectorAll('div.accordion-header'), function(el) {
        el.addEventListener('click', function() {
            if (this.parentElement.classList.contains('is-active')){
                this.parentElement.classList.remove('is-active');
            }    
            else{
                var parents = this.parentElement.parentElement;
                var els = parents.children;
                for (i = 0; i < els.length ; i++){
                    els[i].classList.remove('is-active');
                }
                this.parentElement.classList.add('is-active');
                var parent = this.parentElement;
            }
        })
    })

});