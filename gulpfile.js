var gulp      	= require('gulp'),
	changed 			= require('gulp-changed'),
	sass 					= require('gulp-sass'),
	tildeImporter = require('node-sass-tilde-importer'),
	browserSync 	= require('browser-sync').create(),
	php  					= require('gulp-connect-php'),
	useref 				= require('gulp-useref'),
	uglifyEs 			= require('gulp-uglify-es').default,
	gulpIf 				= require('gulp-if'),
	cleanCSS 			= require('gulp-clean-css'),
	postcss				= require('gulp-postcss'),
	autoprefixer	= require('autoprefixer'),
	rename       	= require('gulp-rename'),
	imagemin 			= require('gulp-imagemin'),
	tinypng 			= require('gulp-tinypng-compress');

	sass.compiler = require('node-sass');

// Compile les scss et met à jour uniquement les fichiers css qui ont été modifiés
gulp.task('styles-dev', function(done) {
	return gulp.src('evt/**/*.scss')
	.pipe(sass({ importer: tildeImporter }).on('error', sass.logError))
	.pipe(changed('media/LP/', {hasChanged: changed.compareSha1Digest}))
	.pipe(gulp.dest('media/LP/'))
	.pipe(browserSync.stream())
	done();
});

// Optimise les gif et svg et les déplace dans le dossier media automatiquement dés qu'une nouvelle image est ajoutée ou modifiée
gulp.task('gif-svg', function(done) {
	return gulp.src(['evt/**/*.gif', 'evt/**/*.svg'])
	.pipe(changed('media/LP/'))
	.pipe(imagemin([
		imagemin.gifsicle({interlaced: true}),
		imagemin.svgo({
			plugins: [
					{removeViewBox: true},
					{cleanupIDs: true}
			]
		})
	], {
		verbose: true
	}))
	.pipe(gulp.dest('media/LP/'))
	.pipe(browserSync.stream())
	done();
});


// Optimise les png et jpeg et les déplace dans le dossier media automatiquement dés qu'une nouvelle image est ajoutée ou modifiée
// Utilise l'API de TinyPNG pour une compression beaucoup plus efficace qu'imagemin, à voir si la limitation a 500 images/mois est suffisante
// https://tinypng.com/developers/subscription

gulp.task('tiny-images', function(done) {
	return gulp.src('evt/**/*.+(png|jpg|jpeg)')
	.pipe(tinypng({
				key: 'kmTN7zypdGc0fKWRFX8d80SftQRDQdWw',
				sigFile: '.tinypng-sigs',
				summarize: true,
				log: false
		}))
	.pipe(gulp.dest('media/LP/'))
	.pipe(browserSync.stream())
	done();
});


// Crée un serveur PHP pour que BrowserSync prenne en charge les fichiers .php
gulp.task('php', function(done) {
		php.server({  hostname: 'localhost', base: '.', port: 8010, keepalive: true });
		done();
});

// BROWSER SYNC - Options -> browsersync.io/docs/options/#option-files
gulp.task('browserSync', function(done) {
	browserSync.init({
		//port: 8000,
		proxy: 'localhost:8888',
		open: false,
		reloadOnRestart: true,
		browser: "google chrome",
		startPath: "/galerieslafayette/evt",
		//open: true,
		notify: false
	});
	done();
});

gulp.task('reload', function(done){
	browserSync.reload();
	done();
});

// Watcher pour le travail en local
gulp.task('watch-src', gulp.series('php', 'browserSync', 'styles-dev', function(done) {
	gulp.watch('evt/**/*.scss', gulp.series('styles-dev'));
	gulp.watch(['evt/**/*.gif', 'evt/**/*.svg'], gulp.series('gif-svg'));
	gulp.watch(['evt/**/*.+(png|jpg|jpeg)'], gulp.series('tiny-images'));
	gulp.watch(['evt/**/*.html', 'evt/**/*.php', 'evt/**/*.js']).on('change', gulp.series('reload'));
	done();
}));


// JS - PROD - concatenation et minification des JS pour la Prod
gulp.task('scripts', function(done) {
	return gulp.src(['./**/*.html', './**/*.php'])
	.pipe(useref())
	.pipe(gulpIf('*.js', uglifyEs()))
	.pipe(gulp.dest('./'))
	done();
});

// SCSS TO CSS - PROD - Compile, prefixe, clean et minifie uniquement les fichiers qui ont été modifiés
gulp.task('styles', function(done) {
	return gulp.src('evt/**/*.scss')
	.pipe(sass({ importer: tildeImporter }).on('error', sass.logError))
	.pipe(postcss([ autoprefixer() ]))
	.pipe(cleanCSS())
	.pipe(rename({suffix: '.min'}))
	.pipe(changed('media/LP/', {hasChanged: changed.compareSha1Digest}))
	.pipe(gulp.dest('media/LP/'))
	done();
});