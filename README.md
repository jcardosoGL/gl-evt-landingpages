## PREREQUIS POUR L'UTILISATION DU REPO
Versions sur lesquelles le repo tourne sans problèmes :

Yarn 	-> v1.21.1  
Node 	-> v14.2.0  
NPM 	-> v6.14yarngu.4  
GULP 	-> CLI version: 2.2.0  
			-> Local version: 4.0.2


## INSTALLATION DU REPO
Cloner le repo 

Une fois le dossier cloné lancer la commande "yarn", ce qui devrait récupérer tous les node_modules répertoriés dans package.json
 
 
## COMMANDES UTILES POUR FAIRE TOURNER LE REPO
Vous pouvez retrouver toutes les commandes dans le fichier package.json

yarn start :  
- lance le repo en mode dev avec browsersync  
- rafraichissement des navigateurs sur l'url http://localhost:3000/galerieslafayette/..  
- rafraichissement a chaque modif des fichiers .sass .html .php .js  
- compilation du sass vers css dans le dossier  
- minification des images et svg et déplacement de evt/src/ vers media/

yarn css		-> compilation et minification des sass vers media/

yarn js 		-> compilation et minification des js vers media/ 